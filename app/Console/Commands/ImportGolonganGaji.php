<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Pegawai;
use App\Model\Golongan;

class ImportGolonganGaji extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:golgaji';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "1024M");
        $file = public_path('import.txt');
        $fres = public_path('import-result.txt');
        $handle = fopen($file,"r");
        $golongan = Golongan::lists('gol_id','xxx_kd_gol');
        $golongan[''] = null;
        $result = "";
        $nf = 0;
        $nu = 0;
        $u = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data = explode("\t",str_replace(["\r","\n"], "", $line));
                if (count($data) != 3) continue;
                $nama = $data[0];
                $nip = $str = ltrim($data[1], '0');
                $gol_id = $golongan[strtolower($data[2])];
                $pegawai = Pegawai::where('peg_nip','like',"%$nip")->orWhere('peg_nip_lama','like',"%$nip")->first();
                if (!$pegawai) {
                    $result .= "NF\t$nama\t$nip\r\n";
                    $nf++;
                } else {
                    if ($pegawai->gol_id_akhir == $gol_id || !$gol_id) {
                        $result .= "NU\t$nama\t{$pegawai->gol_id_akhir}\t$gol_id\r\n";
                        $nu++;
                    } else {
                        $result .= "U\t$nama\t{$pegawai->gol_id_akhir}\t$gol_id\r\n";
                        $pegawai->gol_id_akhir = $gol_id;
                        $pegawai->save();
                        $u++;
                    }
                }
            }
            fclose($handle);
        }
        file_put_contents($fres, "Updated: $u\r\nNot Updated: $nu\r\nNot Found: $nf\r\n\r\n".$result);
    }
}
