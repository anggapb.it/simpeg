<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Pegawai;
use App\Model\Pegawai2;
use App\Model\Jabatan;
use App\Model\UnitKerja;

class ImportJabatanPegawai extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:jabatanpegawai';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "1024M");
        $file = public_path('import-japeg.txt');
        $fres = public_path('import-japeg-result.txt');
        $handle = fopen($file,"r");
        $result = "";
        $nf = 0;
        $nu = 0;
        $u = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data = explode("\t",str_replace(["\r","\n"], "", $line));
                if (count($data) != 4) continue;
                $nip = $data[0];
                $unit_kerja_id = $data[1];
                $jf_id = $data[2];
                $jfu_id = $data[3];
                if ($unit_kerja_id == 'null') $unit_kerja_id = null;
                if ($jf_id == 'null') $jf_id = null;
                if ($jfu_id == 'null') $jfu_id = null;
                $pegawai = Pegawai::where('peg_nip',"$nip")->first();
                $pegawai2 = Pegawai2::where('peg_nip',"$nip")->first();
                if (!$pegawai) {
                    $result .= "NF\t$nip\r\n";
                    $nf++;
                } else {
                    $pegawai->unit_kerja_id = $unit_kerja_id;
                    $jabatan = Jabatan::where('unit_kerja_id',$unit_kerja_id)->where('jf_id',$jf_id)->where('jfu_id',$jfu_id)->first();
                    if (!$jabatan) {
                        $uker = UnitKerja::find($unit_kerja_id);
                        $jabatan = new Jabatan;
                        $jabatan->satuan_kerja_id = $uker ? $uker->satuan_kerja_id : 21;
                        $jabatan->unit_kerja_id = $unit_kerja_id;
                        $jabatan->jf_id = $jf_id;
                        $jabatan->jfu_id = $jfu_id;
                        if ($jf_id) {
                            $jabatan->jabatan_jenis = 3;
                        } else if ($jfu_id) {
                            $jabatan->jabatan_jenis = 4;
                        }
                        $jabatan->save();
                    }
                    $pegawai->jabatan_id = $jabatan->jabatan_id;
                    $pegawai->save();
                    if ($pegawai2) {
                        $pegawai2->unit_kerja_id = $unit_kerja_id;
                        $pegawai2->jabatan_id = $jabatan->jabatan_id;
                        $pegawai2->save();
                    }
                }
            }
            fclose($handle);
        }
        file_put_contents($fres, "Updated: $u\r\nNot Updated: $nu\r\nNot Found: $nf\r\n\r\n".$result);
    }
}
