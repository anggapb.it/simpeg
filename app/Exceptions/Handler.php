<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyDisplayer;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected function convertExceptionToResponse(Exception $e)
    {
        $debug = config('app.debug', false);

        if ($debug) {
            return (new SymfonyDisplayer($debug))->createResponse($e);
        }

        return response()->view('errors.default', ['exception' => $e], 500);
    }

	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		if (app()->bound('sentry') && $this->shouldReport($e)) {
			app('sentry')->captureException($e);
		}
		parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if ($e instanceof NotFoundHttpException) {
			return view('errors.404');
		}
		if ($e instanceof TokenMismatchException) {
			return redirect('auth/login')->withErrors([
                'username' => "Mohon ulangi login",
            ]);
		}

		if($e instanceof TokenInvalidException) {
            return response()->json(['Token is invalid'], $e->getStatusCode());
        } elseif ($e instanceof TokenExpiredException) {
            return response()->json(['Token has expired'], $e->getStatusCode());
        } elseif ($e instanceof TokenBlacklistedException) {
            return response()->json(['Token is blacklisted'], $e->getStatusCode());
		}
		
		return parent::render($request, $e);
	}

}
