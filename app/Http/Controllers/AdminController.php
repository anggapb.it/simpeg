<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Keahlian;
use App\Model\KeahlianLevel;
use App\Model\MasterPengajuan;
use Auth;
use Illuminate\Http\Request;
use Redirect;

class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * Fungsi untuk menampilkan view list keahlian
     * 
     * @return \Illuminate\Http\Response
     */
	public function dataKeahlian(){
		if(Auth::user()->role_id == 1){
			$keahlian = Keahlian::with('allChildKeahlian')->where('keahlian_id','<>','1')->where('keahlian_id','<>','0')->where('spesifik_dari_keahlian_id', '0')->get();

			return view('pages.list_keahlian', compact('keahlian'));
		}else{
			return Redirect::back();
		}
	}

	/**
     * Fungsi untuk menampilkan view list keahlian bahasa
     *
     * 
     * @return \Illuminate\Http\Response
     */
	public function dataBahasa(){
		if(Auth::user()->role_id == 1){
			$bahasa = Keahlian::where('spesifik_dari_keahlian_id', 1)->get();
			return view('pages.list_bahasa', compact('bahasa'));
		}else{
			return Redirect::back();
		}
	}

	/**
     * Fungsi untuk menyimpan data keahlian
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function addDataKeahlian(Request $req){
		$data = $req->except('_token');
		$cek = Keahlian::where('keahlian_nama', $req->input('keahlian_nama'))->count();
		if($req->input('keahlian_nama') != null && $cek==0){
			$bahasa = new Keahlian();
			$bahasa->keahlian_nama = $req->input('keahlian_nama'); 
			$bahasa->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
			$bahasa->keahlian_tipe = $req->input('keahlian_tipe'); 
			$bahasa->spesifik_dari_keahlian_id = 0;
			$bahasa->save();
			logAction('Tambah Keahlian',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Isian Tidak boleh kosong dan nama tidak boleh duplicate');
		}

		return Redirect::back()->with('message','Data berhasil ditambahkan');
	}

	/**
     * Fungsi untuk menyimpan data keahlian baru
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function dataKeahlianNew(Request $req){
		$data = $req->except('_token');
		if($req->input('keahlian_nama') != null){
			$bahasa = new Keahlian();
			$bahasa->keahlian_nama = $req->input('keahlian_nama'); 
			$bahasa->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
			$bahasa->keahlian_tipe = 'default'; 
			$bahasa->spesifik_dari_keahlian_id = 0;
			if($bahasa->save()){
				logAction('Tambah Keahlian',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);
				$k = Keahlian::select('keahlian_id','keahlian_nama')->where('spesifik_dari_keahlian_id', '<>',1)->where('keahlian_id','<>',1)->get();
				echo json_encode($k);
			}
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Isian Tidak boleh kosong');
		}
	}

	public function dataBahasaNew(Request $req){
		$data = $req->except('_token');
		$bahasa = new Keahlian();
		$bahasa->keahlian_nama = $req->input('keahlian_nama'); 
		$bahasa->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
		$bahasa->keahlian_tipe = 'bahasa'; 
		$bahasa->spesifik_dari_keahlian_id = 1;
		if($bahasa->save()){
			logAction('Tambah Keahlian Bahasa',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);
			$k = Keahlian::select('keahlian_id','keahlian_nama')->where('spesifik_dari_keahlian_id',1)->where('keahlian_id','<>',1)->get();
			echo json_encode($k);
		}
	}

	public function addDataKeahlianChild($id,Request $req){
		$data = $req->except('_token');
		$bahasa = new Keahlian();
		$bahasa->keahlian_nama = $req->input('keahlian_nama'); 
		$bahasa->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
		$bahasa->keahlian_tipe = $req->input('keahlian_tipe'); 
		$bahasa->spesifik_dari_keahlian_id = $id;
		$bahasa->save();
		logAction('Tambah Keahlian',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);	
		return Redirect::back()->with('message','Data berhasil ditambahkan');
	}

	public function updateDataKeahlian($id, Request $req){
		$data = $req->except('_token');
		$keahlian = Keahlian::find($id);
		$keahlian->keahlian_nama = $req->input('keahlian_nama'); 
		$keahlian->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
		$keahlian->keahlian_tipe = $req->input('keahlian_tipe');
		$keahlian->save();
		logAction('Edit Keahlian',json_encode($keahlian),$keahlian->keahlian_id,Auth::user()->username);
		return Redirect::back()->with('message','Data telah diupdate');
	}

	public function addDataBahasa(Request $req){
		$data = $req->except('_token');
		$cek = Keahlian::where('keahlian_nama', $req->input('keahlian_nama'))->count();
		if($req->input('keahlian_nama') != null && $cek==0){
			$bahasa = new Keahlian();
			$bahasa->keahlian_nama = $req->input('keahlian_nama'); 
			$bahasa->keahlian_deskripsi = $req->input('keahlian_deskripsi'); 
			$bahasa->keahlian_tipe = 'bahasa';
			$bahasa->spesifik_dari_keahlian_id = 1;
			$bahasa->save();
			logAction('Tambah Keahlian Bahasa',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Isian Tidak boleh kosong dan Nama tidak boleh duplicate');
		}

		return Redirect::back()->with('message','Data berhasil ditambahkan');
	}

	public function updateDataBahasa($id, Request $req){
		$data = $req->except('_token');
		$keahlian = Keahlian::find($id);
		$keahlian->keahlian_nama = $req->input('keahlian_nama'); 
		$keahlian->keahlian_deskripsi = $req->input('keahlian_deskripsi');
		$keahlian->save();
		logAction('Edit Keahlian Bahasa',json_encode($bahasa),$bahasa->keahlian_id,Auth::user()->username);
		return Redirect::back()->with('message','Data telah diupdate');
	}

	public function deleteDataKeahlian($id){
		if (Auth::user()->role_id == 1) {
			Keahlian::find($id)->delete();
			logAction('Hapus Keahlian','',$id,Auth::user()->username);
			return Redirect::back()->with('message','Delete Sukses');
		}else{
			return Redirect::back();
		}
	}

	public function deleteDataBahasa($id){
		if (Auth::user()->role_id == 1) {
			Keahlian::find($id)->delete();
			logAction('Hapus Keahlian Bahasa','',$id,Auth::user()->username);
			return Redirect::back()->with('message','Delete Sukses');
		}else{
			return Redirect::back();
		}
	}

	public function dataPengajuan(){
		if(Auth::user()->role_id == 1){
			$pengajuan = MasterPengajuan::get();
			return view('pages.list_pengajuan', compact('pengajuan'));
		}else{
			return Redirect::back();
		}
	}

	public function addDataPengajuan(Request $req){
		$data = $req->except('_token');
		
		if($req->input('pengajuan') != null){
			$bahasa = new MasterPengajuan;
			$bahasa->nama_pengajuan = $req->input('pengajuan'); 
			$bahasa->deskripsi = $req->input('deskripsi'); 
			$bahasa->save();
			logAction('Tambah Pengajuan',json_encode($bahasa),$bahasa->id,Auth::user()->username);
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Isian Tidak boleh kosong');
		}

		return Redirect::back()->with('message','Data berhasil ditambahkan');
	}

	public function updateDataPengajuan($id, Request $req){
		$data = $req->except('_token');
		$keahlian = MasterPengajuan::find($id);
		$keahlian->nama_pengajuan = $req->input('pengajuan'); 
		$keahlian->deskripsi = $req->input('deskripsi');
		$keahlian->save();
		logAction('Edit Pengajuan',json_encode($keahlian),$keahlian->id,Auth::user()->username);
		return Redirect::back()->with('message','Data telah diupdate');
	}

	public function deleteDataPengajuan($id){
		if (Auth::user()->role_id == 1) {
			MasterPengajuan::find($id)->delete();
			logAction('Hapus Pengajuan','',$id,Auth::user()->username);
			return Redirect::back()->with('message','Delete Sukses');
		}else{
			return Redirect::back();
		}
	}

}