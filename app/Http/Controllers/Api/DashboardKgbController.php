<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use Auth; 
use DB;

use App\Model\Pegawai;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Model\SatuanKerja;
use Redirect;

class DashboardKgbController extends Controller
{
    public function dashboard($id = ''){
        if(!$id)
        $id = Auth::user()->satuan_kerja_id;

        $title = 'KGB';

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
       
        return view('pages.kgb.dashboard_kgb', compact('satker','id','title'));
    }

    
    public function index(Request $req)
    {
        $tgl_awal=$req->periode_awal; 
        $tgl_akhir=$req->periode_akhir;
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }
     
        $tgl_awal = date('Y-m');
        $plus_3month = date('Y-m-d', strtotime('+3 months', strtotime($tgl_awal)));
        $tgl_akhir = date('Y-m');
      
        $search_satker = $req->get('search_satker_dashkgb',false);
       

        $where = '';                              
        
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
            if(!empty($search_satker)) {
                $where .= ' AND satuan_kerja_id = '.$search_satker;
            }
        } else {
            if(!empty(Auth::user()->satuan_kerja_id)) {
                $where .= ' AND satuan_kerja_id = '.Auth::user()->satuan_kerja_id;
            }
        }
        
       
    

        $page = $req->get('page');
        $perpage = $req->get('perpage');

        $limit = '';

        if($page && $perpage)
        $limit = "LIMIT ".$perpage." OFFSET ".($perpage * ($page-1));
$sql = 
<<<EOT
SELECT msu.nama_status 
, (select count(status) 
from spg_usulan where status = msu.status
$where) as jumlah
from m_status_usulan msu 
ORDER BY msu.status ASC
$limit
EOT;

        $list_status_usulan = DB::connection('pgsql2')->select($sql);
    
        // if($page && $perpage)
        //     $list_status_usulan = $list_status_usulan->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_pegawai_simpeg.peg_nama')->toSql();
        // else
        //     $list_status_usulan = $list_status_usulan->orderBy('spg_pegawai_simpeg.peg_nama', 'asc')->get();

        // var_dump($list_status_usulan);die();
        return response()->json(compact('list_status_usulan'))->setStatusCode(200);                            
    }
}