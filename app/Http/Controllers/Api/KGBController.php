<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use Auth; 
use DB;

use App\Model\Pegawai;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;

class KGBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index($tgl_awal=null, $tgl_akhir=null)
    /* public function index(Request $req)
    {
        $tgl_awal=$req->periode_awal; 
        $tgl_akhir=$req->periode_akhir;
        // try {
        // $user = JWTAuth::parseToken()->toUser();
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }
        

        if(!$tgl_awal) {
            $tgl_awal = date('Y-m', strtotime('-2 years'));
        } else {   
            $tgl_awal = '01-'.$tgl_awal;
            $tgl_awal = date("Y-m", strtotime('-2 years', strtotime($tgl_awal)));
        }

        if(!$tgl_akhir) {
            $tgl_akhir = date('Y-m', strtotime('-2 years'));
        } else {   
            $tgl_akhir = '01-'.$tgl_akhir;
            $tgl_akhir = date("Y-m", strtotime('-2 years', strtotime($tgl_akhir)));    
        }

        $search_satker = $req->get('search_satker',false);
        $search_uker = $req->get('search_uker',false);
        $search_golongan = $req->get('search_golongan',false);
        $search_eselon = $req->get('search_eselon',false);
        $search_kelas_jabatan = $req->get('search_kelas_jabatan',false);
        $search_jenjab = $req->get('search_jenjab',false);

        $list_pegawai_kgb = Pegawai::select('spg_pegawai_simpeg.*',
                                        DB::raw("EXTRACT(year FROM age(to_char($tgl_awal, 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as tahun_lama_kerja"),
                                        DB::raw("EXTRACT(month FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as bulan_lama_kerja"),
                                        DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),
                                        'm_spg_satuan_kerja.satuan_kerja_nama',
                                        'gol.nm_gol', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama', 'spg_kgb.kgb_status')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    leftJoin('spg_kgb', 'spg_kgb.peg_id', '=','spg_pegawai_simpeg.peg_id', 'and', 'spg_kgb.kgb_status = 0')->
                                    where('spg_pegawai_simpeg.peg_status', 'TRUE')->
                                  //  whereRaw('EXTRACT(year FROM age(to_char(now(), \'YYYY-MM-01\')::date,spg_pegawai_simpeg.peg_cpns_tmt)) < 32')->
                                    whereRaw('TO_CHAR( spg_pegawai_simpeg.peg_tmt_kgb, \'YYYY-MM\' ) >= \''.$tgl_awal.'\' 
                                            AND TO_CHAR( spg_pegawai_simpeg.peg_tmt_kgb, \'YYYY-MM\' ) <= \''.$tgl_akhir.'\'');

        if($search = strtoupper(\Request::get('lookup_key'))) {
            $list_pegawai_kgb = $list_pegawai_kgb->where(function($query) use ($search) {
                $query->WhereRaw('upper(peg_nama) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(peg_nip) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(gol.nm_gol) LIKE \'%'.$search.'%\'');
            });//->with('celupanJob')->paginate(10);
        }                                    
        
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
            if(!empty($search_satker)) {
                $list_pegawai_kgb = $list_pegawai_kgb->where('spg_pegawai_simpeg.satuan_kerja_id',$search_satker);
            }

            if(!empty($search_uker)) {
                $list_pegawai_kgb = $list_pegawai_kgb->where('spg_pegawai_simpeg.unit_kerja_id',$search_uker);
            }
        } else {
            if(!empty(Auth::user()->satuan_kerja_id)) {
                $list_pegawai_kgb = $list_pegawai_kgb->where('spg_pegawai_simpeg.satuan_kerja_id',Auth::user()->satuan_kerja_id);
            }

            if(!empty(Auth::user()->unit_kerja_id)) {
                $list_pegawai_kgb = $list_pegawai_kgb->where('spg_pegawai_simpeg.unit_kerja_id',Auth::user()->unit_kerja_id);
            }
        }
        
        if(!empty($search_golongan)) {
            $list_pegawai_kgb = $list_pegawai_kgb->whereIn('spg_pegawai_simpeg.gol_id_akhir',$search_golongan);
        }
        if(!empty($search_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::whereRaw("jf_nama ilike '%$search_jabatan%'")->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::whereRaw("jfu_nama ilike '%$search_jabatan%'")->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::whereRaw("jabatan_nama ilike '%$search_jabatan%'")->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $list_pegawai_kgb = $list_pegawai_kgb->whereIn('spg_pegawai_simpeg.jabatan_id', $id_jabatan);
            }
        }

        if(!empty($search_kelas_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::where('jf_kelas',$search_kelas_jabatan)->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::where('jfu_kelas',$search_kelas_jabatan)->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::where('jabatan_kelas',$search_kelas_jabatan)->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $list_pegawai_kgb = $list_pegawai_kgb->whereIn('spg_pegawai_simpeg.jabatan_id', $id_jabatan);
            }
        }

        if(!empty($search_jenjab)){
            $jab_id = [];
            $jabid = Jabatan::where('jabatan_jenis',$search_jenjab)->get();
            
            foreach ($jabid as $ji) {
                $jab_id[] = $ji->jabatan_id;
            }

            $list_pegawai_kgb = $list_pegawai_kgb->whereIn('spg_pegawai_simpeg.jabatan_id',$jab_id);
        }

        $page = $req->get('page');
        $perpage = $req->get('perpage');

        // $count = $list_pegawai_kgb->count();
        if($page && $perpage)
            $list_pegawai_kgb = $list_pegawai_kgb->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_pegawai_simpeg.peg_nama')->toSql();
        else
            $list_pegawai_kgb = $list_pegawai_kgb->orderBy('spg_pegawai_simpeg.peg_nama', 'asc')->get();

        // var_dump($list_pegawai_kgb);die();
        return response()->json(compact('list_pegawai_kgb', 'count'))->setStatusCode(200);                            
    } */

    public function index(Request $req)
    {
        $tgl_awal=$req->periode_awal; 
        $tgl_akhir=$req->periode_akhir;
        // try {
        // $user = JWTAuth::parseToken()->toUser();
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }
        /* } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
            return abort(403,'Something went wrong');
        } */

        if(!$tgl_awal) {
            $tgl_awal = date('Y-m');
        } else {   
            $tgl_awal = '01-'.$tgl_awal;
            $tgl_awal = date("Y-m", strtotime($tgl_awal));
        }

        if(!$tgl_akhir) {
            $tgl_akhir = date('Y-m');
        } else {   
            $tgl_akhir = '01-'.$tgl_akhir;
            $tgl_akhir = date("Y-m", strtotime($tgl_akhir));    
        }

        $search_satker = $req->get('search_satker',false);
        $search_uker = $req->get('search_uker',false);
        $search_golongan = $req->get('search_golongan',false);
        $search_eselon = $req->get('search_eselon',false);
        $search_kelas_jabatan = $req->get('search_kelas_jabatan',false);
        $search_jenjab = $req->get('search_jenjab',false);

        $where = '';
        if($search = strtoupper(\Request::get('lookup_key'))) {            
                $where .= " AND (upper(peg_nama) LIKE '%$search%'
                        OR upper(peg_nip) LIKE '%$search%'
                        OR upper(gol.nm_gol) LIKE '%$search%')";
        }                                    
        
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
            if(!empty($search_satker)) {
                $where .= ' AND spg_pegawai_simpeg.satuan_kerja_id = '.$search_satker;
            }

            if(!empty($search_uker)) {
                $where .= ' AND spg_pegawai_simpeg.unit_kerja_id = '.$search_uker;
            }
        } else {
            if(!empty(Auth::user()->satuan_kerja_id)) {
                $where .= ' AND spg_pegawai_simpeg.satuan_kerja_id = '.Auth::user()->satuan_kerja_id;
            }

            if(!empty(Auth::user()->unit_kerja_id)) {
                $where .= ' AND spg_pegawai_simpeg.unit_kerja_id = '.Auth::user()->unit_kerja_id;
            }
        }
        
        if(!empty($search_golongan)) {
            $where .= ' AND spg_pegawai_simpeg.gol_id_akhir in('.implode(',', $search_golongan).')';
        }
        if(!empty($search_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::whereRaw("jf_nama ilike '%$search_jabatan%'")->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::whereRaw("jfu_nama ilike '%$search_jabatan%'")->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::whereRaw("jabatan_nama ilike '%$search_jabatan%'")->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $where .= ' AND spg_pegawai_simpeg.jabatan_id in('.implode(',', $id_jabatan).')';
            }
        }

        if(!empty($search_kelas_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::where('jf_kelas',$search_kelas_jabatan)->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::where('jfu_kelas',$search_kelas_jabatan)->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::where('jabatan_kelas',$search_kelas_jabatan)->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $where .= ' AND spg_pegawai_simpeg.jabatan_id in('.implode(',', $id_jabatan).')';
            }
        }

        if(!empty($search_jenjab)){
            $jab_id = [];
            $jabid = Jabatan::where('jabatan_jenis',$search_jenjab)->get();
            
            foreach ($jabid as $ji) {
                $jab_id[] = $ji->jabatan_id;
            }

            $where .= ' AND spg_pegawai_simpeg.jabatan_id in('.implode(',', $jab_id).')';
        }

        if(!empty($search_eselon)) {
            $id_jabatan = array();
            $jabatan = Jabatan::whereIn('eselon_id', $search_eselon)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }
                $where .= ' AND spg_pegawai_simpeg.jabatan_id in('.implode(',', $id_jabatan).')';
            }
        }

        $page = $req->get('page');
        $perpage = $req->get('perpage');

        $limit = '';

        if($page && $perpage)
        $limit = "LIMIT ".$perpage." OFFSET ".($perpage * ($page-1));
$sql = 
<<<EOT
SELECT DISTINCT
    COALESCE(dms.total_dms, 0) total_dms,
    hukdis.mhukum_id,
    simpeg_kgb.*,    
	"spg_pegawai_simpeg".*,
	gol_lama.nm_gol AS gol_awal,
	"gol"."nm_gol" AS gol_akhir,
--spg_pegawai_simpeg.peg_kerja_tahun, spg_pegawai_simpeg.peg_kerja_bulan--	TO_CHAR( spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD' ) AS tgl_kenaikan,
	"m_spg_satuan_kerja"."satuan_kerja_nama",
	"jab"."jabatan_nama",
	"jf"."jf_nama",
	"jfu"."jfu_nama",
	"spg_kgb"."kgb_status" 
FROM
spg_pegawai_simpeg
LEFT JOIN (
    SELECT
        spg_pegawai_simpeg.peg_id,
        substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' AS tgl_awal_masuk,
        spg_simpeg.tahun_lama_kerja AS masa_kerja_tahun,
        spg_simpeg.bulan_lama_kerja AS masa_kerja_bulan,
        spg_simpeg.jumlah_tahun_cuti AS tahun_lama_kerja,
        spg_simpeg.jumlah_bulan_cuti AS bulan_lama_kerja,
        (
    CASE
        
        WHEN spg_pegawai_simpeg.gol_id_awal <> 5 THEN
        (
    CASE
    
    WHEN MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja ) :: INT, 24 ) <> 0 THEN--ELSE--statement_list
        to_char(
            CURRENT_DATE + ((
                    24 - MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja ) :: INT, 24 )) :: TEXT || ' month' 
            ) :: INTERVAL,
            'YYYY-MM-01' 
        ) :: DATE ELSE to_char( CURRENT_DATE, 'YYYY-MM-01' ) :: DATE 
    END 
        ) 
        WHEN spg_pegawai_simpeg.gol_id_awal = 5 THEN
        (
        CASE
                
                WHEN MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja - 12 ) :: INT, 24 ) <> 0 THEN--ELSE--statement_list
                to_char(
                    CURRENT_DATE + ((
                            24 - MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja - 12 ) :: INT, 24 )) :: TEXT || ' month' 
                    ) :: INTERVAL,
                    'YYYY-MM-01' 
                ) :: DATE ELSE to_char( CURRENT_DATE, 'YYYY-MM-01' ) :: DATE 
            END 
            ) 
        END 
        ) AS tgl_kenaikan 
    FROM
        "spg_pegawai_simpeg"
        LEFT JOIN (
        SELECT
            spg_pegawai_simpeg.peg_id,
            spg_simpeg.tahun_lama_kerja,
            spg_simpeg.bulan_lama_kerja,
            (
            CASE
                    
                    WHEN spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) < 0 THEN
                    12 + (
                    spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 )) ELSE spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) 
                END 
                ) AS jumlah_bulan_cuti,
                (
                CASE
                        
                        WHEN spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) < 0 THEN
                        FLOOR ((
                                spg_simpeg.tahun_lama_kerja * 12- COALESCE (( SUM ( EXTRACT ( YEAR FROM age( cuti_selesai, cuti_mulai ))) * 12 ), 0 ) + (
                                spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ))) / 12 
                        ) ELSE spg_simpeg.tahun_lama_kerja - COALESCE ( SUM ( EXTRACT ( YEAR FROM age( cuti_selesai, cuti_mulai ))), 0 ) 
                    END 
                    ) AS jumlah_tahun_cuti 
                FROM
                    spg_pegawai_simpeg
                    LEFT JOIN spg_riwayat_cuti cuti on cuti.peg_id = spg_pegawai_simpeg.peg_id
                    LEFT JOIN (
                    SELECT
                        spg_pegawai_simpeg.peg_id,
                        EXTRACT (
                        YEAR 
                        FROM
                            age(
                                to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                            )) - (
                        CASE
                                
                                WHEN spg_pegawai_simpeg.gol_id_awal >= 5 
                                AND spg_pegawai_simpeg.gol_id_awal <= 8 
                                AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                                    5 
                                    WHEN spg_pegawai_simpeg.gol_id_awal >= 1 
                                    AND spg_pegawai_simpeg.gol_id_awal <= 4 
                                    AND spg_pegawai_simpeg.gol_id_akhir > 4 
                                    AND spg_pegawai_simpeg.gol_id_akhir <= 8 THEN
                                        6 
                                        WHEN spg_pegawai_simpeg.gol_id_awal >= 1 
                                        AND spg_pegawai_simpeg.gol_id_awal <= 4 
                                        AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                                            11 ELSE 0 
                                        END 
                                        ) -- gol 2B sd 2C
                                        + ( CASE WHEN spg_pegawai_simpeg.gol_id_awal >= 6 AND spg_pegawai_simpeg.gol_id_awal <= 8 AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN 3 ELSE 0 END ) + (
                                            COALESCE ( pmk.pmk_tahun, 0 ) + (
                                            CASE
                                                    
                                                    WHEN COALESCE ( pmk.pmk_bulan, 0 ) > 0 
                                                    AND EXTRACT (
                                                    MONTH 
                                                    FROM
                                                        age(
                                                            to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                            ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                        )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) > 12 THEN
                                                        FLOOR ((
                                                                EXTRACT (
                                                                MONTH 
                                                                FROM
                                                                    age(
                                                                        to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                        ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                    )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) 
                                                                ) / 12 
                                                        ) ELSE 0 
                                                    END 
                                                    )) AS tahun_lama_kerja,
                                                (
                                                    (
                                                    CASE
                                                            
                                                            WHEN COALESCE ( pmk.pmk_bulan, 0 ) > 0 
                                                            AND EXTRACT (
                                                            MONTH 
                                                            FROM
                                                                age(
                                                                    to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                    ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) > 12 THEN
                                                                MOD (
                                                                    EXTRACT (
                                                                    MONTH 
                                                                    FROM
                                                                        age(
                                                                            to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                            ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                        )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ),
                                                                    12 
                                                                    ) ELSE EXTRACT (
                                                                MONTH 
                                                                FROM
                                                                    age(
                                                                        to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                        ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                    )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) 
                                                            END 
                                                            )) AS bulan_lama_kerja 
                                                    FROM
                                                        spg_pegawai_simpeg
                                                        LEFT JOIN spg_pegawai_pmk pmk ON pmk.peg_id = spg_pegawai_simpeg.peg_id 
                                                    WHERE
                                                        spg_pegawai_simpeg.peg_status = TRUE 
                                                    ) spg_simpeg ON spg_simpeg.peg_id = spg_pegawai_simpeg.peg_id
                                                GROUP BY
                                                    spg_pegawai_simpeg.peg_id,
                                                    spg_simpeg.tahun_lama_kerja,
                                                    spg_simpeg.bulan_lama_kerja 
                                                ) spg_simpeg ON spg_simpeg.peg_id = spg_pegawai_simpeg.peg_id 
                                            WHERE
                                                "spg_pegawai_simpeg"."peg_status" = TRUE 
                                            ) simpeg_kgb ON simpeg_kgb.peg_id = spg_pegawai_simpeg.peg_id
	LEFT JOIN "m_spg_satuan_kerja" ON "m_spg_satuan_kerja"."satuan_kerja_id" = "spg_pegawai_simpeg"."satuan_kerja_id"
	LEFT JOIN "m_spg_golongan" AS "gol" ON "gol"."gol_id" = "spg_pegawai_simpeg"."gol_id_akhir"
	LEFT JOIN "m_spg_golongan" AS "gol_lama" ON "gol_lama"."gol_id" = "spg_pegawai_simpeg"."gol_id_awal"
	LEFT JOIN "m_spg_jabatan" AS "jab" ON "jab"."jabatan_id" = "spg_pegawai_simpeg"."jabatan_id"
	LEFT JOIN "m_spg_referensi_jf" AS "jf" ON "jf"."jf_id" = "jab"."jf_id"
	LEFT JOIN "m_spg_referensi_jfu" AS "jfu" ON "jfu"."jfu_id" = "jab"."jfu_id"
    LEFT JOIN "spg_kgb" ON "spg_kgb"."peg_id" = "spg_pegawai_simpeg"."peg_id" and spg_kgb.kgb_status != 50 
    LEFT JOIN (
        SELECT
            peg_id, mhukum_id 
        FROM
            spg_riwayat_hukuman 
        WHERE
            (
                mhukum_id = 20 
                AND to_char( now(), 'YYYY-MM-DD' ) :: DATE >= riw_hukum_tmt 
                AND to_char( now(), 'YYYY-MM-DD' ) :: DATE <= riw_hukum_sd
            )) hukdis ON hukdis.peg_id = spg_pegawai_simpeg.peg_id 
    LEFT JOIN ( SELECT count( peg_id ) total_dms, peg_id FROM spg_file_pegawai GROUP BY peg_id ) dms ON dms.peg_id = spg_pegawai_simpeg.peg_id 
WHERE
	"spg_pegawai_simpeg"."peg_status" = TRUE 
	AND TO_CHAR( simpeg_kgb.tgl_kenaikan, 'YYYY-MM' ) >= '$tgl_awal' 
    AND TO_CHAR( simpeg_kgb.tgl_kenaikan, 'YYYY-MM' ) <= '$tgl_akhir' 
    $where
    AND (
		CASE
				
				WHEN spg_pegawai_simpeg.gol_id_akhir >= 1 
				AND spg_pegawai_simpeg.gol_id_akhir <= 4 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 27 
                WHEN spg_pegawai_simpeg.gol_id_akhir >= 4 
					AND spg_pegawai_simpeg.gol_id_akhir <= 8 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 33 
                WHEN spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 32 END)
ORDER BY
"spg_pegawai_simpeg"."peg_nama" ASC
$limit
EOT;

        $list_pegawai_kgb = DB::connection('pgsql2')->select($sql);

        $sql_count = 
<<<EOT
SELECT DISTINCT
    count(spg_pegawai_simpeg.peg_id)  
FROM
spg_pegawai_simpeg
LEFT JOIN (
    SELECT
        spg_pegawai_simpeg.peg_id,
        substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' AS tgl_awal_masuk,
        spg_simpeg.tahun_lama_kerja AS masa_kerja_tahun,
        spg_simpeg.bulan_lama_kerja AS masa_kerja_bulan,
        spg_simpeg.jumlah_tahun_cuti AS tahun_lama_kerja,
        spg_simpeg.jumlah_bulan_cuti AS bulan_lama_kerja,
        (
    CASE
        
        WHEN spg_pegawai_simpeg.gol_id_awal <> 5 THEN
        (
    CASE
    
    WHEN MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja ) :: INT, 24 ) <> 0 THEN--ELSE--statement_list
        to_char(
            CURRENT_DATE + ((
                    24 - MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja ) :: INT, 24 )) :: TEXT || ' month' 
            ) :: INTERVAL,
            'YYYY-MM-01' 
        ) :: DATE ELSE to_char( CURRENT_DATE, 'YYYY-MM-01' ) :: DATE 
    END 
        ) 
        WHEN spg_pegawai_simpeg.gol_id_awal = 5 THEN
        (
        CASE
                
                WHEN MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja - 12 ) :: INT, 24 ) <> 0 THEN--ELSE--statement_list
                to_char(
                    CURRENT_DATE + ((
                            24 - MOD (( COALESCE ( spg_simpeg.tahun_lama_kerja, 1 ) * 12+ spg_simpeg.bulan_lama_kerja - 12 ) :: INT, 24 )) :: TEXT || ' month' 
                    ) :: INTERVAL,
                    'YYYY-MM-01' 
                ) :: DATE ELSE to_char( CURRENT_DATE, 'YYYY-MM-01' ) :: DATE 
            END 
            ) 
        END 
        ) AS tgl_kenaikan 
    FROM
        "spg_pegawai_simpeg"
        LEFT JOIN (
        SELECT
            spg_pegawai_simpeg.peg_id,
            spg_simpeg.tahun_lama_kerja,
            spg_simpeg.bulan_lama_kerja,
            (
            CASE
                    
                    WHEN spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) < 0 THEN
                    12 + (
                    spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 )) ELSE spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) 
                END 
                ) AS jumlah_bulan_cuti,
                (
                CASE
                        
                        WHEN spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ) < 0 THEN
                        FLOOR ((
                                spg_simpeg.tahun_lama_kerja * 12- COALESCE (( SUM ( EXTRACT ( YEAR FROM age( cuti_selesai, cuti_mulai ))) * 12 ), 0 ) + (
                                spg_simpeg.bulan_lama_kerja - COALESCE ( SUM ( EXTRACT ( MONTH FROM age( cuti_selesai, cuti_mulai ))), 0 ))) / 12 
                        ) ELSE spg_simpeg.tahun_lama_kerja - COALESCE ( SUM ( EXTRACT ( YEAR FROM age( cuti_selesai, cuti_mulai ))), 0 ) 
                    END 
                    ) AS jumlah_tahun_cuti 
                FROM
                    spg_pegawai_simpeg
                    LEFT JOIN spg_riwayat_cuti cuti on cuti.peg_id = spg_pegawai_simpeg.peg_id
                    LEFT JOIN (
                    SELECT
                        spg_pegawai_simpeg.peg_id,
                        EXTRACT (
                        YEAR 
                        FROM
                            age(
                                to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                            )) - (
                        CASE
                                
                                WHEN spg_pegawai_simpeg.gol_id_awal >= 5 
                                AND spg_pegawai_simpeg.gol_id_awal <= 8 
                                AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                                    5 
                                    WHEN spg_pegawai_simpeg.gol_id_awal >= 1 
                                    AND spg_pegawai_simpeg.gol_id_awal <= 4 
                                    AND spg_pegawai_simpeg.gol_id_akhir > 4 
                                    AND spg_pegawai_simpeg.gol_id_akhir <= 8 THEN
                                        6 
                                        WHEN spg_pegawai_simpeg.gol_id_awal >= 1 
                                        AND spg_pegawai_simpeg.gol_id_awal <= 4 
                                        AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                                            11 ELSE 0 
                                        END 
                                        ) -- gol 2B sd 2C
                                        + ( CASE WHEN spg_pegawai_simpeg.gol_id_awal >= 6 AND spg_pegawai_simpeg.gol_id_awal <= 8 AND spg_pegawai_simpeg.gol_id_akhir > 8 THEN 3 ELSE 0 END ) + (
                                            COALESCE ( pmk.pmk_tahun, 0 ) + (
                                            CASE
                                                    
                                                    WHEN COALESCE ( pmk.pmk_bulan, 0 ) > 0 
                                                    AND EXTRACT (
                                                    MONTH 
                                                    FROM
                                                        age(
                                                            to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                            ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                        )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) > 12 THEN
                                                        FLOOR ((
                                                                EXTRACT (
                                                                MONTH 
                                                                FROM
                                                                    age(
                                                                        to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                        ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                    )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) 
                                                                ) / 12 
                                                        ) ELSE 0 
                                                    END 
                                                    )) AS tahun_lama_kerja,
                                                (
                                                    (
                                                    CASE
                                                            
                                                            WHEN COALESCE ( pmk.pmk_bulan, 0 ) > 0 
                                                            AND EXTRACT (
                                                            MONTH 
                                                            FROM
                                                                age(
                                                                    to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                    ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) > 12 THEN
                                                                MOD (
                                                                    EXTRACT (
                                                                    MONTH 
                                                                    FROM
                                                                        age(
                                                                            to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                            ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                        )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ),
                                                                    12 
                                                                    ) ELSE EXTRACT (
                                                                MONTH 
                                                                FROM
                                                                    age(
                                                                        to_char( now(), 'YYYY-MM-01' ) :: DATE,
                                                                        ( substr( spg_pegawai_simpeg.peg_nip, 9, 4 ) || '-' || substr( spg_pegawai_simpeg.peg_nip, 13, 2 ) || '-01' ) :: DATE 
                                                                    )) :: INT + COALESCE ( pmk.pmk_bulan, 0 ) 
                                                            END 
                                                            )) AS bulan_lama_kerja 
                                                    FROM
                                                        spg_pegawai_simpeg
                                                        LEFT JOIN spg_pegawai_pmk pmk ON pmk.peg_id = spg_pegawai_simpeg.peg_id 
                                                    WHERE
                                                        spg_pegawai_simpeg.peg_status = TRUE 
                                                    ) spg_simpeg ON spg_simpeg.peg_id = spg_pegawai_simpeg.peg_id
                                                GROUP BY
                                                    spg_pegawai_simpeg.peg_id,
                                                    spg_simpeg.tahun_lama_kerja,
                                                    spg_simpeg.bulan_lama_kerja 
                                                ) spg_simpeg ON spg_simpeg.peg_id = spg_pegawai_simpeg.peg_id 
                                            WHERE
                                                "spg_pegawai_simpeg"."peg_status" = TRUE 
                                            ) simpeg_kgb ON simpeg_kgb.peg_id = spg_pegawai_simpeg.peg_id
	LEFT JOIN "m_spg_satuan_kerja" ON "m_spg_satuan_kerja"."satuan_kerja_id" = "spg_pegawai_simpeg"."satuan_kerja_id"
	LEFT JOIN "m_spg_golongan" AS "gol" ON "gol"."gol_id" = "spg_pegawai_simpeg"."gol_id_akhir"
	LEFT JOIN "m_spg_golongan" AS "gol_lama" ON "gol_lama"."gol_id" = "spg_pegawai_simpeg"."gol_id_awal"
	LEFT JOIN "m_spg_jabatan" AS "jab" ON "jab"."jabatan_id" = "spg_pegawai_simpeg"."jabatan_id"
	LEFT JOIN "m_spg_referensi_jf" AS "jf" ON "jf"."jf_id" = "jab"."jf_id"
	LEFT JOIN "m_spg_referensi_jfu" AS "jfu" ON "jfu"."jfu_id" = "jab"."jfu_id"
    LEFT JOIN "spg_kgb" ON "spg_kgb"."peg_id" = "spg_pegawai_simpeg"."peg_id" and spg_kgb.kgb_status != 50 
    LEFT JOIN (
        SELECT
            peg_id, mhukum_id 
        FROM
            spg_riwayat_hukuman 
        WHERE
            (
                mhukum_id = 20 
                AND to_char( now(), 'YYYY-MM-DD' ) :: DATE >= riw_hukum_tmt 
                AND to_char( now(), 'YYYY-MM-DD' ) :: DATE <= riw_hukum_sd
            )) hukdis ON hukdis.peg_id = spg_pegawai_simpeg.peg_id 
    LEFT JOIN ( SELECT count( peg_id ) total_dms, peg_id FROM spg_file_pegawai GROUP BY peg_id ) dms ON dms.peg_id = spg_pegawai_simpeg.peg_id 
WHERE
	"spg_pegawai_simpeg"."peg_status" = TRUE 
	AND TO_CHAR( simpeg_kgb.tgl_kenaikan, 'YYYY-MM' ) >= '$tgl_awal' 
    AND TO_CHAR( simpeg_kgb.tgl_kenaikan, 'YYYY-MM' ) <= '$tgl_akhir' 
    $where
    AND (
		CASE
				
				WHEN spg_pegawai_simpeg.gol_id_akhir >= 1 
				AND spg_pegawai_simpeg.gol_id_akhir <= 4 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 27 
                WHEN spg_pegawai_simpeg.gol_id_akhir >= 4 
					AND spg_pegawai_simpeg.gol_id_akhir <= 8 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 33 
                WHEN spg_pegawai_simpeg.gol_id_akhir > 8 THEN
                    simpeg_kgb.tahun_lama_kerja > 0 and simpeg_kgb.tahun_lama_kerja <= 32 END)
EOT;
        $count = DB::connection('pgsql2')->select($sql_count);
        
        // if($page && $perpage)
        //     $list_pegawai_kgb = $list_pegawai_kgb->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_pegawai_simpeg.peg_nama')->toSql();
        // else
        //     $list_pegawai_kgb = $list_pegawai_kgb->orderBy('spg_pegawai_simpeg.peg_nama', 'asc')->get();

        // var_dump($list_pegawai_kgb);die();
        return response()->json(compact('list_pegawai_kgb', 'count'))->setStatusCode(200);                            
    }
}
