<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use Auth;
use DB;
use App\Model\Pegawai;
use App\Model\KGB;
use App\Model\GajiTahun;
use App\Model\TTDKGB;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProsesKgbController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $periode_awal=$req->periode_awal; 
        $periode_akhir=$req->periode_akhir;
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }

        if($periode_awal)  {
            $periode_awal = date("Y-m", strtotime($periode_awal));
        }

        if(!$periode_akhir) {
            $periode_akhir = date('Y-m');
        } else{
            $periode_akhir = date("Y-m", strtotime($periode_akhir));
        }
        $search_satker = $req->get('search_satker',false);
        $search_uker = $req->get('search_uker',false);
        $search_golongan = $req->get('search_golongan',false);
        $search_eselon = $req->get('search_eselon',false);
        $search_kelas_jabatan = $req->get('search_kelas_jabatan',false);
        $search_jenjab = $req->get('search_jenjab',false);
        $search_nousulan = $req->get('search_nousulan',false);
        $search_ttd = $req->get('search_ttd',false);

        $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
        $list_proses_kgb = KGB::select('spg_pegawai_simpeg.*',
                                    DB::raw("EXTRACT(year FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as tahun_lama_kerja"),
                                    DB::raw("EXTRACT(month FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as bulan_lama_kerja"),
                                    DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),  
                                    DB::raw("(SELECT gaji_pokok FROM m_spg_gaji 
                                    where mgaji_id = $gaji_thn->mgaji_id AND gol_id = spg_kgb.gol_id AND gaji_masakerja = spg_pegawai_simpeg.peg_kerja_tahun) as gaji_pokok"),     
                                    'm_spg_satuan_kerja.satuan_kerja_nama','m_spg_unit_kerja.unit_kerja_nama',
                                    'gol.nm_gol', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama', 'spg_kgb.kgb_status',
                                    'spg_kgb.kgb_id','spg_kgb.gol_id','spg_kgb.gaji_id','spg_kgb.kgb_nosk','spg_kgb.kgb_tglsk',
                                    'spg_kgb.kgb_tmt','spg_kgb.kgb_status_peg','spg_kgb.kgb_thn','spg_kgb.kgb_bln','spg_kgb.kgb_gapok',
                                    'spg_kgb.kgb_tglsurat','spg_kgb.kgb_nosurat','spg_kgb.kgb_status','spg_kgb.user_id',
                                    'spg_kgb.kgb_kerja_tahun','spg_kgb.kgb_kerja_bulan', 'spg_kgb.kgb_status_pdf', 'spg_kgb.kgb_tmt',
                                    'spg_pegawai_simpeg.peg_kerja_tahun','spg_pegawai_simpeg.peg_kerja_bulan', 'su.no_usulan')->
                                    leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_kgb.peg_id')->
                                  //  leftJoin('m_spg_gaji as gj', 'gj.gaji_masakerja', '=','spg_pegawai_simpeg.peg_kerja_tahun','and','gj.gol_id', '=','spg_kgb.gol_id', 'and', 'gj.mgaji_id = \''.$gaji_thn->mgaji_id.'\'')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_unit_kerja', 'm_spg_unit_kerja.unit_kerja_id', '=', 'spg_pegawai_simpeg.unit_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    leftJoin('spg_usulan_detail as sud', 'sud.peg_id', '=','spg_kgb.peg_id')->
                                    leftJoin('spg_usulan as su', 'su.usulan_id', '=','sud.usulan_id')->
                                    leftJoin('m_ttd_kgb', function($join){
                                        $join->on('spg_pegawai_simpeg.gol_id_akhir','>=','m_ttd_kgb.gol_awal_id');
                                         $join->on('spg_pegawai_simpeg.gol_id_akhir','<=','m_ttd_kgb.gol_akhir_id');
                                    });
                                    //leftJoin('m_ttd_kgb as ttd',  'spg_pegawai_simpeg.gol_id_akhir', '>=', 'ttd.gol_awal_id', 'and' ,'spg_pegawai_simpeg.gol_id_akhir' ,'<=', 'ttd.gol_akhir_id');

        if($periode_awal) {
            $list_proses_kgb = $list_proses_kgb->whereRaw('concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) >= \''.$periode_awal.'\'
                                AND concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) <= \''.$periode_akhir.'\'');
        }
        
        if($search = strtoupper(\Request::get('lookup_key'))) {
            $list_proses_kgb = $list_proses_kgb->where(function($query) use ($search) {
                $query->WhereRaw('upper(spg_pegawai_simpeg.peg_nama) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(spg_pegawai_simpeg.peg_nip) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(gol.nm_gol) LIKE \'%'.$search.'%\'');
            });//->with('celupanJob')->paginate(10);
        }  
        
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
            if(!empty($search_satker)) {
                $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.satuan_kerja_id',$search_satker);
            }

            if(!empty($search_uker)) {
                $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.unit_kerja_id',$search_uker);
            }
        } else {
            if(!isset($req->par)) {
                if(!empty(Auth::user()->satuan_kerja_id)) {
                    $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.satuan_kerja_id',Auth::user()->satuan_kerja_id);
                }

                if(!empty(Auth::user()->unit_kerja_id)) {
                    $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.unit_kerja_id',Auth::user()->unit_kerja_id);
                }
            }
        }

        $list_proses_kgb->where('status_data', '<>', 'manual');
        
        if(!empty($search_golongan)) {
            $list_proses_kgb = $list_proses_kgb->whereIn('spg_pegawai_simpeg.gol_id_akhir',$search_golongan);
        }
       
        if(!empty($search_ttd)) {
            $list_proses_kgb = $list_proses_kgb->where('m_ttd_kgb.peg_id_ttd',$search_ttd);
        }

        if($search_nousulan != '') {
            $list_proses_kgb = $list_proses_kgb->where('su.no_usulan',$search_nousulan);
        }
       

        if(!empty($search_kelas_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::where('jf_kelas',$search_kelas_jabatan)->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::where('jfu_kelas',$search_kelas_jabatan)->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::where('jabatan_kelas',$search_kelas_jabatan)->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $list_proses_kgb = $list_proses_kgb->whereIn('spg_pegawai_simpeg.jabatan_id', $id_jabatan);
            }
        }

        if(!empty($search_jenjab)){
            $jab_id = [];
            $jabid = Jabatan::where('jabatan_jenis',$search_jenjab)->get();
            
            foreach ($jabid as $ji) {
                $jab_id[] = $ji->jabatan_id;
            }

            $list_proses_kgb = $list_proses_kgb->whereIn('spg_pegawai_simpeg.jabatan_id',$jab_id);
        }
        
        if(isset($req->par) && !empty($req->par)) {
            $list_proses_kgb = $list_proses_kgb->where('kgb_status',  1);

            if($req->par == 'ttd_digital') {
                $pegawai = pegawai::where('peg_nip', Auth::user()->username)->first();
                
                $peg_id = 0;
                if($pegawai) {
                    $peg_id = $pegawai->peg_id;
                }

                $ttd_kgb = TTDKGB::where('peg_id_ttd', $peg_id)->first();
                
                $gol_awal_id = 0;
                $gol_akhir_id = 0;
                
                if($ttd_kgb) {
                    $gol_awal_id = $ttd_kgb->gol_awal_id;
                    $gol_akhir_id = $ttd_kgb->gol_akhir_id;
                }

                $list_proses_kgb = $list_proses_kgb->whereNull('ttd_digital');
                $list_proses_kgb = $list_proses_kgb->
                                    where('spg_kgb.gol_id', '>=', $gol_awal_id)->
                                    where('spg_kgb.gol_id', '<=', $gol_akhir_id);
            }
        }

        $page = $req->get('page');
        $perpage = $req->get('perpage');

        $count = $list_proses_kgb->count();
        if($page && $perpage)
            $list_proses_kgb = $list_proses_kgb->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_pegawai_simpeg.peg_nama')->get();
            // print_r($list_proses_kgb);
            // die;
         else
             $list_proses_kgb = $list_proses_kgb->orderBy('spg_pegawai_simpeg.peg_tmt_kgb', 'asc')->get();

        // var_dump($list_proses_kgb);die();
        return response()->json(compact('list_proses_kgb', 'count'))->setStatusCode(200);                            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
