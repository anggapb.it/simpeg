<?php

namespace App\Http\Controllers\Api; 

use Illuminate\Http\Request;
use App\Model\UsulanKgb;
use App\Model\UsulanDetailKgb;

use Auth;
use DB;
use Storage;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsulanKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function usulan(Request $req)
    {
        $tgl_awal=$req->periode_awal; 
        $tgl_akhir=$req->periode_akhir;
        // try {
        // $user = JWTAuth::parseToken()->toUser();
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }
        /* } catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
            return abort(403,'Something went wrong');
        } */

        if(!$tgl_awal) {
            $tgl_awal = date('Y-m-d', strtotime('first day of this month', time()));
        } else {   
            $tgl_awal = date("Y-m-d", strtotime($tgl_awal));
        }

        if(!$tgl_akhir) {
            $tgl_akhir = date('Y-m-t');
        } else {   
            $tgl_akhir = date("Y-m-d", strtotime($tgl_akhir));    
        }
        
        $par = $req->get('par',false);
        $search_satker = $req->get('search_satker',false);

        $kgb_detail = usulanDetailKgb::select('usulan_id', DB::raw('count(peg_id) as total_pegawai'))->
                                        groupBy('usulan_id');

        $list = UsulanKgb::select('spg_usulan.*', 'm_spg_satuan_kerja.satuan_kerja_nama', 'm_status_usulan.nama_status', 'kgb_detail.total_pegawai')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_usulan.satuan_kerja_id')->
                                    leftJoin('m_status_usulan', 'm_status_usulan.status', '=', 'spg_usulan.status')->
                                    leftJoin(DB::raw("(SELECT count(peg_id) total_pegawai, usulan_id from spg_usulan_detail GROUP BY usulan_id) AS kgb_detail"), 'spg_usulan.usulan_id', '=', 'kgb_detail.usulan_id')->
                                    whereRaw('TO_CHAR( spg_usulan.tgl_usulan, \'YYYY-MM-DD\' ) >= \''.$tgl_awal.'\' 
                                            AND TO_CHAR( spg_usulan.tgl_usulan, \'YYYY-MM-DD\' ) <= \''.$tgl_akhir.'\'');

        if($search = strtoupper(\Request::get('lookup_key'))) {
            $list = $list->where(function($query) use ($search) {
                $query->WhereRaw('upper(no_usulan) LIKE \'%'.$search.'%\'');
            });//->with('celupanJob')->paginate(10);
        }                                    
           
        
        if(!empty($par)) {
            $list = $list->where('spg_usulan.status', '>=','10');
        } else {
            if(!empty($search_satker)) {
                $list = $list->where('spg_usulan.satuan_kerja_id',$search_satker);
            }else {
                $list = $list->where('spg_usulan.satuan_kerja_id',Auth::user()->satuan_kerja_id);
            }
        }

        $page = $req->get('page');
        $perpage = $req->get('perpage');

        $count = $list->count();
        if($page && $perpage)
            $list = $list->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_usulan.no_usulan')->get();
        else
            $list = $list->orderBy('spg_usulan.no_usulan', 'asc')->get();

        // var_dump($list);die();
        return response()->json(compact('list', 'count'))->setStatusCode(200);    
    }


    public function usulanDetail(Request $req,$usulan_id=null)
    {
        $usulan_id= $usulan_id ?: 0; 
        
        // try {
        // $user = JWTAuth::parseToken()->toUser();
        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }
                
        $list = UsulanDetailKgb::select('spg_usulan_detail.*', 
                                        'spg_pegawai_simpeg.peg_gelar_depan',
                                        'spg_pegawai_simpeg.peg_gelar_belakang',
                                        'spg_pegawai_simpeg.peg_nama',
                                        'spg_pegawai_simpeg.peg_nip',
                                        'spg_pegawai_simpeg.peg_lahir_tanggal',
                                        'spg_pegawai_simpeg.peg_lahir_tempat',
                                        'spg_pegawai_simpeg.peg_tmt_kgb',
                                        'spg_pegawai_simpeg.peg_kerja_tahun',
                                        'spg_pegawai_simpeg.peg_kerja_bulan',
                                        DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),
                                        'm_spg_satuan_kerja.satuan_kerja_nama',
                                        'gol.nm_gol', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama')->
                                    leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_usulan_detail.peg_id')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    where('spg_usulan_detail.usulan_id', $usulan_id);

        if($search = strtoupper(\Request::get('lookup_key'))) {
            $list = $list->where(function($query) use ($search) {
                $query->WhereRaw('upper(peg_nama) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(peg_nip) LIKE \'%'.$search.'%\'')
                        ->orWhereRaw('upper(gol.nm_gol) LIKE \'%'.$search.'%\'');
            });//->with('celupanJob')->paginate(10);
        }                                    
           
        $page = $req->get('page');
        $perpage = $req->get('perpage');

        $count = $list->count();
        if($page && $perpage)
            $list = $list->skip(($page-1) * $perpage)->take($perpage)->orderBy('spg_pegawai_simpeg.peg_nama')->get();
        else
            $list = $list->orderBy('spg_pegawai_simpeg.peg_nama', 'asc')->get();
        
        return response()->json(compact('list', 'count'))->setStatusCode(200);    
    }
    
    public function getDocument($id)
    {
        $usulan = UsulanKgb::findOrFail($id);

        if(!Auth::check()) {
            $user = JWTAuth::parseToken()->authenticate();

            if(! $user) {
                return abort(404, 'User Not Found');  
            }
        }

        $filePath = storage_path('surat_pengantar_kgb\\'.$usulan->surat_pengantar);

        $headers = array(
            'Content-Type: application/pdf',
        ); 
          
        return Response::download($filePath, $usulan->surat_pengantar, $headers);
    }
}
