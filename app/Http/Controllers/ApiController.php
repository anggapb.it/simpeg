<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Model\UserDevice;
use App\Model\User;

use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\Golongan;
use App\Model\Eselon;
use App\Model\TingkatPendidikan;
use App\Model\Jurusan;
use App\Model\Universitas;
use App\Model\Notification;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('api_auth',['except' => ['authenticateDevice','sendNotificationAdminSimpegAdm', 'authenticate', 'getToken']]);
    }

    //AuthenticateDevice
    public function authenticateDevice(Request $request)
    {
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => null,
            'message' => 'Authentication success'
        ];
        $credentials = $request->only('username', 'password');
        $device_id = $request->get('device_id',false);
        $device_type = $request->get('device_type','');
        $cloud_key = $request->get('cloud_key','');
        $gcm_key = $request->get('gcm_key','');
        $apns_key = $request->get('apns_key','');
        $device_description = $request->get('device_description','[]');
        if (!$device_id)
        {
            $response = [
                'response' => 'FAILED',
                'statusCode' => 203,
                'message' => 'Parameter `device_id` unspecified'
            ];
            return response()->json($response);
        }
        $peg = Pegawai::where('peg_nip',$credentials['username'])->where('peg_status',true)->where('satuan_kerja_id','<>',999999)->first();
        if ($peg) {
            if (!User::where('username',$credentials['username'])->exists()) {
                $user = new User;
                $user->username = $credentials['username'];
                $user->password = bcrypt('123456');
                $user->role_id = 4;
                $user->status_aktif = 1;
                $user->satuan_kerja_id = $peg->satuan_kerja_id;
                $user->email = $peg->peg_email;
                $user->save();
            }
        }
        if (Auth::once($credentials)) {
            $user = Auth::user();
            $api_key = str_random(64);
            $user_device = UserDevice::findOrNew($device_id);
            $user_device->id = $device_id;
            $user_device->fill([
                'user_id'=>$user->id,
                'api_key'=>$api_key,
                'device_type'=>$device_type,
                'device_description'=>$device_description,
                'cloud_key'=>$cloud_key,
                'gcm_key'=>$gcm_key,
                'apns_key'=>$apns_key
            ]);
            $user_device->save();
            $response['result'] = [
                'api_key' => $api_key,
                'device_id' => $device_id
            ];
        } else {
            $response['message'] = 'Authentication failed';
            $response['statusCode'] = 203;
            $response['response'] = 'FAILED';
        }
        return response()->json($response);
    }

    public function deauthenticate(Request $request)
    {
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => null,
            'message' => 'De-Authentication success'
        ];
        $device_id = $request->get('device_id',false);
        UserDevice::find($device_id)->delete();
        return response()->json($response);
    }

    public function profile()
    {
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => null,
            'message' => 'Get profile success'
        ];
        if (Auth::user()->role_id == 4) {
            $peg = Pegawai::where('peg_nip',Auth::user()->username)->where('peg_status',true)->where('satuan_kerja_id','<>',999999)->first();
            $nama = $peg ? (!empty($peg->peg_gelar_depan) ? $peg->peg_gelar_depan." ":"").$peg->peg_nama.(!empty($peg->peg_gelar_belakang) ? ", ".$peg->peg_gelar_belakang:"") : "-";
            $jabatan = $peg ? $peg->getJabatan()['jabatan']: "Pelaksana";
            $response['result'] = [
                'name' => $nama,
                'jabatan' => $jabatan,
                'role_id' => Auth::user()->role ? Auth::user()->role->role_name : '-',
                'profile_picture' => $peg ? $peg->getPhotoUrl() : null,
                'profile_picture_thumb' => $peg ? $peg->getPhotoUrlResize() : null,
                'satuan_kerja_id' => $peg ? intval($peg->satuan_kerja_id) : 0,
                'peg_id' => $peg ? $peg->peg_id : null,
            ];
        } else {
            $response['result'] = [
                'name' => Auth::user()->nama,
                'jabatan' => Auth::user()->nama,
                'role_id' => Auth::user()->role ? Auth::user()->role->role_name : '-',
                'profile_picture' => null,
                'satuan_kerja_id' => Auth::user()->satuan_kerja_id,
                'peg_id' => null
            ];
        }
        return response()->json($response);
    }
    
    public function getPegawai(Request $request, $peg_id) {
        $pegawai = Pegawai::with([
            'golongan_darah',
            'golongan_awal',
            'golongan_akhir',
            'pendidikan_awal',
            'pendidikan_akhir',
            'unit_kerja',
            'kecamatan.kabupaten.propinsi',
            'jabatan',
            'jabatan.jabatan_fungsional',
            'jabatan.jabatan_fungsional_umum',
            'agama',
            'satuan_kerja',
            'riwayat_diklat',
            'riwayat_diklat.diklat_fungsional',
            'riwayat_diklat.diklat_struktural',
            'riwayat_diklat.diklat_teknis',
            'riwayat_jabatan',
            'riwayat_jabatan.golongan',
            'riwayat_keluarga',
            'riwayat_non_formal',
            'riwayat_pangkat',
            'riwayat_pendidikan',
            'riwayat_penghargaan',
        ])->find($peg_id);
        if (!$pegawai) {
            $response = [
                'response' => 'FAILED',
                'statusCode' => 403,
                'message' => 'Pegawai not found'
            ];
            return response()->json($response);
        }
        $pegawai->peg_nama = (!empty($pegawai->peg_gelar_depan) ? $pegawai->peg_gelar_depan." ":"").$pegawai->peg_nama.(!empty($pegawai->peg_gelar_belakang) ? ", ".$pegawai->peg_gelar_belakang:"");
        $pegawai->satuan_kerja_nama = $pegawai->satuan_kerja ? $pegawai->satuan_kerja->satuan_kerja_nama : '-';
        $pegawai->unit_kerja_nama = $pegawai->unit_kerja ? $pegawai->unit_kerja->unit_kerja_nama : '-';
        $pegawai->peg_jenis_kelamin = $pegawai->peg_jenis_kelamin;
        $pegawai->peg_rumah_alamat = $pegawai->peg_rumah_alamat . ($pegawai->kecamatan ? (" ".$pegawai->kecamatan->kecamatan_nm.($pegawai->kecamatan->kabupaten ? " ".$pegawai->kecamatan->kabupaten->kabupaten_nm : '')) : '');
        $pegawai->peg_foto = $pegawai->getPhotoUrl();
        $pegawai->jabatan_nama = $pegawai->jabatan ? ($pegawai->jabatan->jabatan_jenis == 2 ? $pegawai->jabatan->jabatan_nama: ($pegawai->jabatan->jabatan_fungsional ? $pegawai->jabatan->jabatan_fungsional->jf_nama : ($pegawai->jabatan->jabatan_fungsional_umum ? $pegawai->jabatan->jabatan_fungsional_umum->jfu_nama : 'Pelaksana'))) : '-';
        switch(intval($pegawai->peg_status_perkawinan)) {
            case 1:
                $pegawai->peg_status_perkawinan = "Kawin";
                break;
            case 2:
                $pegawai->peg_status_perkawinan = "Belum Kawin";
                break;
            case 3:
                $pegawai->peg_status_perkawinan = "Janda";
                break;
            case 4:
                $pegawai->peg_status_perkawinan = "Duda";
                break;
            default:
                $pegawai->peg_status_perkawinan = "-";
                break;
        }
        $tmp = $pegawai->agama ? $pegawai->agama->nm_agama : '-';
        unset($pegawai->agama);
        $pegawai->agama = $tmp;
        $tmp = $pegawai->golongan_darah ? $pegawai->golongan_darah->nm_goldar : '-';
        unset($pegawai->golongan_darah);
        $pegawai->golongan_darah = $tmp;
        $tmp = $pegawai->golongan_awal ? $pegawai->golongan_awal->nm_gol.' '.$pegawai->golongan_awal->nm_pkt : '-';
        unset($pegawai->golongan_awal);
        $pegawai->golongan_awal = $tmp;
        $tmp = $pegawai->golongan_akhir ? $pegawai->golongan_akhir->nm_gol.' '.$pegawai->golongan_akhir->nm_pkt : '-';
        unset($pegawai->golongan_akhir);
        $pegawai->golongan_akhir = $tmp;
        $tmp = $pegawai->pendidikan_awal ? $pegawai->pendidikan_awal->nm_pend : '-';
        unset($pegawai->pendidikan_awal);
        $pegawai->pendidikan_awal = $tmp;
        $tmp = $pegawai->pendidikan_akhir ? $pegawai->pendidikan_akhir->nm_pend : '-';
        unset($pegawai->pendidikan_akhir);
        $pegawai->pendidikan_akhir = $tmp;
        if ($pegawai->kecamatan) {
            if ($pegawai->kecamatan->kabupaten) {
                if ($pegawai->kecamatan->kabupaten->propinsi) {
                    $pegawai->propinsi = $pegawai->kecamatan->kabupaten->propinsi->propinsi_nm;
                } else {
                    $pegawai->propinsi = '-';
                }
                $pegawai->kabupaten = $pegawai->kecamatan->kabupaten->kabupaten_nm;
            } else {
                $pegawai->kabupaten = '-';
                $pegawai->propinsi = '-';
            }
            $tmp = $pegawai->kecamatan->kecamatan_nm;
            unset($pegawai->kecamatan);
            $pegawai->kecamatan = $tmp;
        } else {
            unset($pegawai->kecamatan);
            $pegawai->kecamatan = '-';
            $pegawai->kabupaten = '-';
            $pegawai->propinsi = '-';
        }
        foreach ($pegawai->riwayat_jabatan as &$rj) {
            $tmp = $rj->golongan ? $rj->golongan->nm_gol.' '.$rj->golongan->nm_pkt : '-';
            unset($rj->golongan);
        }
        foreach ($pegawai->riwayat_diklat as &$rd) {
            $tmp = $rd->diklat_fungsional ? $rd->diklat_fungsional->diklat_fungsional_nm : '-';
            unset($rd->diklat_fungsional);
            $rd->diklat_fungsional = $tmp;
            $tmp = $rd->diklat_struktural ? $rd->diklat_struktural->kategori_nama : '-';
            unset($rd->diklat_struktural);
            $rd->diklat_struktural = $tmp;
            $tmp = $rd->diklat_teknis ? $rd->diklat_teknis->diklat_teknis_nm : '-';
            unset($rd->diklat_teknis);
            $rd->diklat_teknis = $tmp;
        }
        unset($pegawai->satuan_kerja);
        unset($pegawai->unit_kerja);
        unset($pegawai->jabatan);
        $rps = $pegawai->riwayat_pendidikan;
        $rps_arr = [];
        foreach ($rps as $n => $rp) {
            if (empty($rp->riw_pendidikan_nm)) {
                if (!$rp->tingkat_pendidikan) $rp->tingkat_pendidikan = new TingkatPendidikan;
                if (!$rp->jurusan) $rp->jurusan = new Jurusan;
                if (!$rp->universitas) $rp->universitas = new Universitas;
                $rps_arr[] = "{$rp->tingkat_pendidikan->nm_tingpend} {$rp->jurusan->jurusan_nm} {$rp->universitas->univ_nmpti}";
            } else {
                $rps_arr[] = $rp->riw_pendidikan_nm;
            }
        }
        unset($pegawai->riwayat_pendidikan);
        $pegawai->riwayat_pendidikan = $rps_arr;
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => [],
            'message' => 'Retrieve success'
        ];
        $response['result'] = $pegawai;
        return response()->json($response);
    }
    
    public function getPegawais(Request $request, $offset = 0, $limit = 20) {
        $pegawais = Pegawai::skip($offset)->take($limit)->orderBy('peg_id')->get();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => [],
            'message' => 'Retrieve success'
        ];
        $response['result'] = $pegawais;
        return response()->json($response);
    }
    
    public function getData(Request $request) {
        $pegawais = Pegawai::with(['unit_kerja','jabatan','jabatan.jabatan_fungsional','jabatan.jabatan_fungsional_umum','satuan_kerja','kecamatan.kabupaten'])->orderBy('peg_id')->where('peg_status',true)->where('satuan_kerja_id','<>',999999);
        $params = $request->get('params',false);

        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'updated_at':
                        $pegawais = $pegawais->where($key,'>',$val);
                        break;
                    default:
                        $pegawais = $pegawais->where($key,$val);
                        break;
                }
            }
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',100);

        $pegawais = $pegawais->skip(($page-1) * $perpage)->take($perpage)->get();
        $data = [];
        foreach ($pegawais as $pegawai) {
            $data[] = [
                'peg_id' => $pegawai->peg_id,
                'peg_nama' => (!empty($pegawai->peg_gelar_depan) ? $pegawai->peg_gelar_depan." ":"").$pegawai->peg_nama.(!empty($pegawai->peg_gelar_belakang) ? ", ".$pegawai->peg_gelar_belakang:""),
                'satuan_kerja_nama' => $pegawai->satuan_kerja ? $pegawai->satuan_kerja->satuan_kerja_nama : '-',
                'unit_kerja_nama' => $pegawai->unit_kerja ? $pegawai->unit_kerja->unit_kerja_nama : '-',
                'peg_jenis_kelamin' => $pegawai->peg_jenis_kelamin,
                'peg_rumah_alamat' => $pegawai->peg_rumah_alamat . ($pegawai->kecamatan ? (" ".$pegawai->kecamatan->kecamatan_nm.($pegawai->kecamatan->kabupaten ? " ".$pegawai->kecamatan->kabupaten->kabupaten_nm : '')) : ''),
                'peg_nip' => $pegawai->peg_nip,
                'peg_telp' => $pegawai->peg_telp,
                'peg_telp_hp' => $pegawai->peg_telp_hp,
                'peg_foto' => $pegawai->getPhotoUrl(),
                'jabatan_nama' => $pegawai->jabatan ? ($pegawai->jabatan->jabatan_jenis == 2 ? $pegawai->jabatan->jabatan_nama: ($pegawai->jabatan->jabatan_fungsional ? $pegawai->jabatan->jabatan_fungsional->jf_nama : ($pegawai->jabatan->jabatan_fungsional_umum ? $pegawai->jabatan->jabatan_fungsional_umum->jfu_nama : 'Pelaksana'))) : '-',
                'tingkat_pendidikan' => $pegawai->pendidikan_akhir ? (($pegawai->pendidikan_akhir->kategori_pendidikan ? $pegawai->pendidikan_akhir->kategori_pendidikan->tingkat_pendidikan->nm_tingpend : '').' - '.$pegawai->pendidikan_akhir->nm_pend) : '',
                'golongan_nama' => $pegawai->golongan_akhir ? $pegawai->golongan_akhir->nm_gol . ' ' . $pegawai->golongan_akhir->nm_pkt : '',
                'eselon_nama' => $pegawai->jabatan ? ($pegawai->jabatan->eselon ? $pegawai->jabatan->eselon->eselon_nm : '') : '',
            ];
        }
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $data,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function getBawahan(Request $request) {
        $nip = Auth::user()->username;
        if (strlen($nip) != 18) {
            return response()->json([
                'response' => 'OK',
                'statusCode' => 200,
                'result' => [],
                'message' => 'Retrieve success'
            ]);
        }
        $peg = Pegawai::with('jabatan')->where('peg_nip',$nip)->first();
        if (!$peg || $peg->jabatan->jabatan_jenis != 2) {
            return response()->json([
                'response' => 'OK',
                'statusCode' => 200,
                'result' => [],
                'message' => 'Retrieve success'
            ]);
        }
        $pegawais = Pegawai::with(['unit_kerja','jabatan','jabatan.jabatan_fungsional','jabatan.jabatan_fungsional_umum','satuan_kerja','kecamatan.kabupaten'])
            ->orderBy('peg_id')->where('peg_status',true)
            ->where('satuan_kerja_id','<>',999999)
            ->whereIn('unit_kerja_id',getAllUnitKerjaChildren($peg->unit_kerja_id))
            ->where('peg_nip','<>',$nip);
        $params = $request->get('params',false);

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',100);

        $pegawais = $pegawais->skip(($page-1) * $perpage)->take($perpage)->get();
        $data = [];
        foreach ($pegawais as $pegawai) {
            $data[] = [
                'peg_id' => $pegawai->peg_id,
                'peg_nama' => (!empty($pegawai->peg_gelar_depan) ? $pegawai->peg_gelar_depan." ":"").$pegawai->peg_nama.(!empty($pegawai->peg_gelar_belakang) ? ", ".$pegawai->peg_gelar_belakang:""),
                'satuan_kerja_nama' => $pegawai->satuan_kerja ? $pegawai->satuan_kerja->satuan_kerja_nama : '-',
                'unit_kerja_nama' => $pegawai->unit_kerja ? $pegawai->unit_kerja->unit_kerja_nama : '-',
                'peg_jenis_kelamin' => $pegawai->peg_jenis_kelamin,
                'peg_rumah_alamat' => $pegawai->peg_rumah_alamat . ($pegawai->kecamatan ? (" ".$pegawai->kecamatan->kecamatan_nm.($pegawai->kecamatan->kabupaten ? " ".$pegawai->kecamatan->kabupaten->kabupaten_nm : '')) : ''),
                'peg_nip' => $pegawai->peg_nip,
                'peg_telp' => $pegawai->peg_telp,
                'peg_telp_hp' => $pegawai->peg_telp_hp,
                'peg_foto' => $pegawai->getPhotoUrl(),
                'jabatan_nama' => $pegawai->jabatan ? ($pegawai->jabatan->jabatan_jenis == 2 ? $pegawai->jabatan->jabatan_nama: ($pegawai->jabatan->jabatan_fungsional ? $pegawai->jabatan->jabatan_fungsional->jf_nama : ($pegawai->jabatan->jabatan_fungsional_umum ? $pegawai->jabatan->jabatan_fungsional_umum->jfu_nama : 'Pelaksana'))) : '-',
            ];
        }
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $data,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function masterSkpd(Request $request) {
        $models = SatuanKerja::where('satuan_kerja_nama','not like','%--%')->whereNotIn('satuan_kerja_nama',['REKON2014','REKNN','PNDHKLR','PNDHINTRN'])->where('status',1)->orderBy('satuan_kerja_nama')->get();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $models,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function masterGolongan(Request $request) {
        $models = Golongan::select(DB::raw('gol_id, concat(nm_gol, concat(\' \', nm_pkt)) as golongan_nama'))->orderBy('gol_id')->get();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $models,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function masterEselon(Request $request) {
        $models = Eselon::select(DB::raw('eselon_id, eselon_nm as eselon_nama'))->orderBy('eselon_id')->get();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $models,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function masterPendidikan(Request $request) {
        $models = TingkatPendidikan::orderBy('kode_urut_pend')->get();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $models,
            'message' => 'Retrieve success'
        ];
        return response()->json($response);
    }
    
    public function sendNotificationAdminSimpegAdm(Request $request) {
        $model = new Notification;
        $model->title = $request->get('title');
        $model->content = $request->get('content');
        $model->sender = 'Simpeg Administratif';
        $model->role_ids = ['1','3'];
        $model->time = time();
        $model->save();
        $response = [
            'response' => 'OK',
            'statusCode' => 200,
            'result' => $model,
            'message' => 'Notification saved'
        ];
        return response()->json($response);
    }
    
    // use \Response;

    public function authenticate(\Request $request){
        $credentials = $request::only('username', 'password');

        try {
            if(! $token = JWTAuth::attempt($credentials)) {
                return abort(403, 'Unauthorized action.');    
            }
        } catch (JWTException $ex) {
            return abort(500, 'Something went wrong');
        }

        return response()->json(compact('token'))->setStatusCode(200);
    }

    public function getToken() {
        $token = JWTAuth::getToken();

        if(!$token) {
            return abort(403, 'Token is invalid.');    
        }

        try {
            $refreshedToken = JWTAuth::refresh($token);
        } catch (JWTException $ex){
            return abort(500, 'Something went wrong');
        }

        return response()->json(compact('refreshedToken'))->setStatusCode(200);
    }
}
