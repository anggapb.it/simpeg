<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Auth;
use Redirect;
use Validator;
use Excel;
use DB;
use DateTime;

use App\Model\Pegawai;
use App\Model\Golongan;
use App\Model\RiwayatAssessment;
use App\Model\MatriksAssessment;

class AssessmentController extends Controller {

    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('bkd');

    }

    public function import(PegawaiSapkImport $import){
        $import->noHeading();
        $import->ignoreEmpty();
        $results = $import->toArray();

        $i=1;
        $messages = "";
        $processed = 0;
        $processed_fail = 0;
        for($i=1; $i<count($results); $i++){
            $processed = $i;
        }
        $messages .= "$processed data telah diproses";
        return Redirect::back()->with('message',$messages);
    }
    
    public function UploadAssessment(){
        return view('pages.assessment.index');
    }

    public function UploadAssessmentPost(PegawaiSapkImport $import, Request $req){
        $import->noHeading();
        $import->ignoreEmpty();
        $tanggal = $req->input('tanggal');
        $results = $import->toArray();
        $total = 0;
        $processed = 0;
        $nipNotFound= 0;
        $invalidGolongan = 0;
        $invalidMatriks = 0;
        $messages = "";
        
        for($i=6; $i<count($results); $i++){
            $total++;
            $peserta = $results[$i][1];
            $golongan = $results[$i][2];
            $jabatan = $results[$i][3];
            $kompetensi = $results[$i][4];
            $potensi = $results[$i][5];
            $matriks_nm = $results[$i][6];
            
            $arr_peserta = explode("\n", $peserta);
            $nip = $arr_peserta[1];
            $pegawai = Pegawai::where('peg_nip', $nip)->first();
            if ($pegawai){
                $arr_golongan = explode(",", $golongan);
                $nm_gol = trim($arr_golongan[1]);
                $gol = Golongan::where('nm_gol', $nm_gol)->first();
                if ($gol){
                    $matriksAssessment = MatriksAssessment::where('matriks_nm', $matriks_nm)->first();
                    if ($matriksAssessment){
                        $riwayatAssessment = new RiwayatAssessment();
                        $riwayatAssessment->tanggal = $tanggal;
                        $riwayatAssessment->peg_id = $pegawai->peg_id;
                        $riwayatAssessment->jabatan = $jabatan;
                        $riwayatAssessment->gol_id = $gol->gol_id;
                        $riwayatAssessment->kompetensi = $kompetensi;
                        $riwayatAssessment->potensi = $potensi;
                        $riwayatAssessment->matriks_id = $matriksAssessment->matriks_id;
                        if($riwayatAssessment->save()){
                            $processed++;
                        }
                    } else {
                        $invalidMatriks++;
                    }
                } else {
                    $invalidGolongan++;
                }
            } else {
                $nipNotFound++;
            }            
        }
        
        $messages .= $processed." dari ".$total." data telah berhasil diproses.";
        if($nipNotFound > 0) $messages .= "<br />NIP Tidak Ditemukan = $nipNotFound. ";
        if($invalidGolongan > 0) $messages .= "<br />Golongan Tidak Valid = $invalidGolongan. ";
        if($invalidMatriks > 0) $messages .= "<br />Matriks Tidak Valid = $invalidMatriks. ";
        return Redirect::back()->with('message',$messages);
    }

}

?>