<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use Redirect;
use Validator;
use Input;
use File;
use DB;
use Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_PageSetup;

use App\Model\Pegawai;
use App\Model\Golongan;
use App\Model\JabatanFungsionalUmum;
use App\Model\JfuBaruTerisi;
use App\Model\PegawaiEvjab;
use App\Model\PegawaiEvjabLengkap;
use App\Model\PegawaiEvjabUjikom;
use App\Model\PegawaiErk;
use App\Model\PegawaiNaikTurun;
use App\Model\PegawaiBelum;
use App\Model\ListSpgPegawai;
use App\Model\ListSpgPensiun;
use App\Model\JfuBaru;
use App\Model\PegawaiPensiun;
use App\Model\Pegawai2;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Penghargaan;
use App\Model\Jabatan;
use App\Model\RiwayatPenghargaan;
use App\Model\RiwayatPenghargaan2;
use App\Model\RiwayatPendidikan;
use App\Model\RiwayatPendidikan2;
use App\Model\RiwayatNonFormal;
use App\Model\RiwayatNonFormal2;
use App\Model\RiwayatPangkat;
use App\Model\RiwayatPangkat2;
use App\Model\KGB;
use App\Model\Kgb_skpd;
use App\Model\Pmk;
use App\Model\Pmk_skpd;
use App\Model\RiwayatJabatan;
use App\Model\RiwayatJabatan2;
use App\Model\RiwayatDiklat;
use App\Model\RiwayatDiklat2;
use App\Model\RiwayatKeluarga;
use App\Model\RiwayatKeluarga2;
use App\Model\RiwayatKeahlian;
use App\Model\RiwayatKeahlian2;
use App\Model\RiwayatKeahlianRel;
use App\Model\RiwayatKeahlianRel2;
use App\Model\StatusEditPegawai;
use App\Model\StatusEditPegawaiLog;
use App\Model\MStatus;
use App\Model\TingkatPendidikan;
use App\Model\Pendidikan;
use App\Model\KategoriPendidikan;
use App\Model\Kecamatan;
use App\Model\Kabupaten;
use App\Model\LogEditPegawai;
use App\Model\RiwayatHukuman;
use App\Model\RiwayatHukuman2;
use App\Model\FilePegawai;
use App\Model\FilePegawai2;

class BkdController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('bkd');

	}

	public function viewStaging(){
		$listPegawai = ListSpgPegawai::where('status_record', false)->orderby('added_at')->get();
		if(Auth::user()->role_id==1) {
			return view('pages.list_pns_staging', compact('listPegawai'));
		} else {
			return Redirect::back();
		}
	}

	public function viewStagingPensiun(){
		$listPegawaiPensiun = ListSpgPensiun::where('status_record', false)->orderBy('added_at')->get();
		if(Auth::user()->role_id==1) {
			return view('pages.list_pns_staging_pensiun', compact('listPegawaiPensiun'));
		} else {
			return Redirect::back();
		}
	}

	public function pushData($id){
		$listSpgPegawai = ListSpgPegawai::where('peg_id', $id)->where('status_record', false)->first();
		$listSpgPegawai->status_record = true;
		$listSpgPegawai->pushed_by = Auth::user()->id;

		$pSpg = Pegawai::where('peg_id', $id)->first();
		$pErk = PegawaiErk::where('peg_id', $id)->first();
		$listSpgPegawai->pushed_at = date("Y-m-d H:i:s");

		$pErk->unit_kerja_id = $pSpg->unit_kerja_id;
		$pErk->jabatan_id = $pSpg->jabatan_id;
		$pErk->satuan_kerja_id = $pSpg->satuan_kerja_id;
		$pErk->peg_jabatan_tmt = $pSpg->peg_jabatan_tmt;

		$pErk->save();
		$listSpgPegawai->save();

		return Redirect::to('pushdata/jabatan')->with('message', 'Data Perubahan Jabatan Sudah Di-Push');
	}

	public function pushDataPensiun($id){
		$listSpgPensiun = ListSpgPensiun::where('peg_id', $id)->where('status_record', false)->first();
		$listSpgPensiun->status_record = true;
		$listSpgPensiun->pushed_by = Auth::user()->id;

		$pSpg = Pegawai::where('peg_id', $id)->first();
		$pErk = PegawaiErk::where('peg_id', $id)->first();
		$listSpgPensiun->pushed_at = date("Y-m-d H:i:s");

		$pErk->peg_status = $pSpg->peg_status;
		$pErk->peg_ketstatus = $pSpg->peg_ketstatus;

		$pErk->save();
		$listSpgPensiun->save();

		return Redirect::to('pushdata/pensiun')->with('message', 'Data Perubahan Pensiun Sudah Di-Push');
	}

	public function pushAllData(){
		$listSpgPegawai = ListSpgPegawai::where('status_record', false)->get();
		foreach ($listSpgPegawai as $lsp) {
			$this->pushData($lsp->peg_id);
		}
		return Redirect::to('home')->with('message', 'Semua Data Perubahan Jabatan Sudah Di-Push');
	}

	public function pushAllDataPensiun(){
		$listSpgPensiun = ListSpgPensiun::where('status_record', false)->get();
		foreach ($listSpgPensiun as $lsp) {
			$this->pushDataPensiun($lsp->peg_id);
		}
		return Redirect::to('home')->with('message', 'Semua Data Perubahan Pensiun Sudah Di-Push');
	}

	public function evjabIndexJfu(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$jabatan = JfuBaru::where('is_deleted', false)->get();
		return view('pages.jfubaru.list',compact('jabatan'));
	}

	public function evjabEditJfu($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$model = JfuBaru::find($id);
		$jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->whereRaw("kelas ~ '^\d+$'")->orderBy('nilai_jabatan','desc')->get();

		return view('pages.jfubaru.edit', compact('model','jfkelas'));
	}

	public function evjabUpdateJfu($id,Request $request){
			$jft = JfuBaru::find($id);
			if(!$jft){
					return Redirect::to('/evjab/jfu/edit/'.$id)->with('message', 'Data Tidak Ditemukan');
			}
			$jft->jfu_nama = $request->namajab;
			$jft->jfu_pangkat_awal = $request->gol_awal;
			$jft->jfu_pangkat_puncak = $request->gol_akhir;
			$jft->jfu_bup = $request->bup;
			$jft->jfu_syarat = $request->syarat;
			$jft->jfu_kelas = $request->jabatan_kelas;
			$jft->kode_jabatan = $request->kojab;

			if($jft->save()){
					logAction('Edit EvjabJFU',json_encode($jft),$jft->jfu_id,Auth::user()->username);

					return Redirect::to('/evjab/jfu')->with('message', 'Data Telah Diupdate');
			}
	}

	public function evjabDeleteJfu($id,Request $request){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$jft = JfuBaru::find($id);
		if(!$jft){
				return Redirect::to('/evjab/jfu')->with('message', 'Data Tidak Ditemukan');
		}

		$jabatan = Jabatan::where('jfu_id',$id)->lists('jabatan_id');
		$jumpeg = PegawaiEvjab::whereIn('jabatan_id_evjab',$jabatan)->where('peg_status',true)->count();

		if($jumpeg > 0){
				return Redirect::to('/evjab/jfu')->with('message', 'Tidak Dapat Menghapus Data Karena Ada Pegawai Yang Terikat Dengan Jabatan Tersebut');
		}

		$jft->is_deleted = true;
		if($jft->save()){
				logAction('Hapus EvjabJFU','',$id,Auth::user()->username);
				return Redirect::to('/evjab/jfu')->with('message', 'Data Telah Dihapus');
		}
	}

	public function evjabSearchKojabJfu(Request $request){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$jabkel = (int) $request->jabatan_kelas;

		if(strlen($jabkel) == 1){
			$jabkel = '0'.$jabkel;
		}

		$jft = JfuBaru::where('kode_jabatan','like','2.000.'.$jabkel.'%')->orderBy('kode_jabatan','desc')->first();

		if(!$jft){
			return '2.000.'.$jabkel.'.001';
		}

		$kojab = explode('.', $jft->kode_jabatan);
		$kojabs = (int) $kojab[3] + 1;

		if(strlen($kojabs) == 1){
			$kojabs = '00'.$kojabs;
		}elseif (strlen($kojabs) == 2) {
			$kojabs = '0'.$kojabs;
		}

		return '2.000.'.$jabkel.'.'.$kojabs;
	}

	public function evjabCreateJfu(Request $request){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$rumpun = DB::connection('pgsql2')->table('m_spg_jabatan_rumpun')->lists('rumpun_nm','rumpun_id');
		$kategori = DB::connection('pgsql2')->table('m_spg_jabatan_kategori')->lists('jabkat_nm','jabkat_id');
		$jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->whereRaw("kelas ~ '^\d+$'")->orderBy('nilai_jabatan','desc')->get();

		return view('pages.jfubaru.create',compact('rumpun','kategori','jfkelas'));
	}

	public function evjabStoreJfu(Request $request){
		$lastid = JfuBaru::where('jfu_nama','<>','Pelaksana')->orderBy('jfu_id','desc')->first();
		$jft = new JfuBaru;
		$jft->jfu_id = $lastid->jfu_id + 1;
		$jft->jfu_nama = $request->namajab;
		$jft->jfu_pangkat_awal = $request->gol_awal;
		$jft->jfu_pangkat_puncak = $request->gol_akhir;
		$jft->jfu_bup = $request->bup;
		$jft->jfu_syarat = $request->syarat;
		$jft->jfu_kelas = $request->jabatan_kelas;
		$jft->kode_jabatan = $request->kojab;
		logAction('Tambah EvjabJFU',json_encode($jft),$jft->jfu_id,Auth::user()->username);
		if($jft->save()){
			return Redirect::to('/evjab/jfu')->with('message', 'Data Telah Ditambahkan');
		}
	}

	public function evjabGetPegawaiJfu(Request $request){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$jfu_id = $request->jfu_id;
		$jabatan = Jabatan::where('jfu_id',$jfu_id)->lists('jabatan_id');
		$data = PegawaiEvjab::leftJoin('m_spg_satuan_kerja','m_spg_satuan_kerja.satuan_kerja_id','=','spg_pegawai_evjab.satuan_kerja_id')->whereIn('jabatan_id_evjab',$jabatan)->where('peg_status', 'TRUE')->get();
		return response()->json($data);
	}

	public function evjabSearch(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$url = url('evjab/get-data');
        $columns = [
            'peg_nama' => ['Nama','peg_nama'],
            'peg_nip' => ['NIP','peg_nip'],
            'satuan_kerja_id' => ['Satuan Kerja','satuan_kerja.satuan_kerja_nama'],
            'peg_status' => ['Status Pegawai','peg_status'],
            'status_id' => ['Status Edit','status_edit.status_id'],
        ];
		return view('pages.jfubaru.all_pns',compact('url','columns'));
	}

	public function evjabGetDataRender(Request $request) {
    $models = PegawaiEvjab::where('is_deleted', false)->with(['satuan_kerja','status_edit']);
    $params = $request->get('params',false);
    $search = $request->get('search',false);
    $search = addslashes(strtolower($search));
    $order  = $request->get('order' ,false);

    if ($order) {
      $order_direction = $request->get('order_direction','asc');
        switch ($order) {
          default:
            $models = $models->orderBy($order,$order_direction);
            break;
          }
    }

    if ($params) {
      foreach ($params as $key => $search) {
        if ($search == '') continue;
          switch($key) {
            default:
            break;
          }
        }
    }

  	if ($search != '') {
      $models = $models->whereRaw("lower(peg_nama) like '%$search%'")->orWhere('peg_nip','like',"%$search%");
    }

    $page = $request->get('page',1);
    $perpage = $request->get('perpage',20);

    $count = count($models->get());
    $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

    foreach ($models as $key => $value) {
      if($models[$key]['peg_status']=='true'){
        if($models[$key]['peg_status_kepegawaian']==1){
          $models[$key]['peg_status'] = 'Aktif - PNS';
        }elseif($models[$key]['peg_status_kepegawaian']==2){
          $models[$key]['peg_status'] = 'Aktif - CPNS';
        }else{
          $models[$key]['peg_status'] = 'Aktif';
        }
      }else{
        if($models[$key]['peg_ketstatus'] != null){
          $models[$key]['peg_status'] = $models[$key]['peg_ketstatus'];
        }else{
          $models[$key]['peg_status'] = 'Pegawai Pensiun';
        }
      }
			$models[$key]['status_edit']['status_id'] = 'Sudah Verifikasi BKPP';
    }

    $result = [
      'data' => $models,
      'count' => $count
    ];

    return response()->json($result);
  }

	public function evjabGetProfilePegawai($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$pegawai = PegawaiEvjab::where('peg_id', $id)->first();
		if($pegawai == null){
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();

		if(Auth::user()->role_id==1 || Auth::user()->role_id==3){
			return view('pages.jfubaru.profile_pegawai', compact('pegawai','satker'));
		}else{
			return Redirect::back();
		}

	}

	public function evjabGetProfileUjikomPegawai($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$pegawai = PegawaiEvjab::where('peg_id', $id)->first();
		if($pegawai == null){
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();

		if(Auth::user()->role_id==1 || Auth::user()->role_id==3){
			return view('pages.jfubaru.profile_pegawai_ujikom', compact('pegawai','satker'));
		}else{
			return Redirect::back();
		}

	}

	public function evjabEditPegawai($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$pegawai = PegawaiEvjabLengkap::where('peg_id', $id)->first();
		if($pegawai == null){
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
		$uker = UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id)->first();
		$tempjfu = Jabatan::where('jabatan_id', $pegawai->jabatan_id_evjab)->first();
		if ($tempjfu){
			$jfu_id = $tempjfu->jfu_id;
			$tempkelas = JfuBaru::where('jfu_id', $jfu_id)->first();
			$kelas = $tempkelas->jfu_kelas;
		}else{
			$kelas = "0";
		}
		return view('pages.jfubaru.edit_pegawai', compact('pegawai', 'satker', 'uker', 'kelas'));
	}

	public function evjabEditUjikomPegawai($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$pegawai = PegawaiEvjabLengkap::where('peg_id', $id)->first();
		if($pegawai == null){
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
		$uker = UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id)->first();
		$tempjfu = Jabatan::where('jabatan_id', $pegawai->jabatan_id_evjab)->first();
		if ($tempjfu){
			$jfu_id = $tempjfu->jfu_id;
			$tempkelas = JfuBaru::where('jfu_id', $jfu_id)->first();
			$kelas = $tempkelas->jfu_kelas;
		}else{
			$kelas = "0";
		}
		return view('pages.jfubaru.edit_pegawai_ujikom', compact('pegawai', 'satker', 'uker', 'kelas'));
	}

	public function evjabDeletePegawai($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$pegawai = PegawaiEvjab::where('peg_id', $id)->first();
		if(!$pegawai){
			return Redirect::to('/evjab/list-pegawai-belum')->with('message', 'Data Tidak Ditemukan');
		}else{
			$pegawai->is_deleted = true;
			if($pegawai->save()){
				logAction('Hapus Pegawai from spg_pegawai_evjab','',$id,Auth::user()->username);
				return Redirect::to('/evjab/list-pegawai-belum')->with('message', 'Data Telah Dihapus');
			} else {
				return Redirect::to('/evjab/list-pegawai-belum')->with('message', 'Gagal Menghapus Pegawai');
			}
		}
	}

	public function evjabUpdatePegawai($id, Request $req){
		$data = $req->except('_token');
		date_default_timezone_set("Asia/Bangkok");
		$time = date("Y-m-d");

		$pegawai = PegawaiEvjab::find($id);
		if($pegawai){
			if(empty($req->input('satuan_kerja_id')) || $req->input('satuan_kerja_id') == null){
				return Redirect::to('/evjab/search/edit-pegawai/'.$pegawai->peg_id)->with('message', 'Gagal! Silahkan Pilih Satuan Kerja Terlebih Dahulu.');
			}
		}

		$jabs = Jabatan::where('jabatan_id',$req->input('jabatan_id'))->first();
		$cekriwjab = RiwayatJabatan::where('peg_id',$pegawai->peg_id)->orderBy('riw_jabatan_tmt','desc')->first();

		if($pegawai->jabatan_id_evjab != (int) $req->input('jabatan_id')){
			if($cekriwjab && $cekriwjab->riw_jabatan_tmt == saveDate($req->input('peg_jabatan_tmt'))){
				$rw_jab = RiwayatJabatan::find($cekriwjab->riw_jabatan_id);
			}else{
				$rw_jab = new RiwayatJabatan;
				$id_rw_jab = RiwayatJabatan::select('riw_jabatan_id')->max('riw_jabatan_id');
				$find_rpid5 = RiwayatJabatan::where('riw_jabatan_id', $id_rw_jab)->first();
				if($find_rpid5){
					$id_rw_jab++;
				}
				$rw_jab->riw_jabatan_id = $id_rw_jab;
				$rw_jab->peg_id = $pegawai->peg_id;
				$rw_jab->riw_jabatan_tmt = saveDate($req->input('peg_jabatan_tmt'));
			}
			if($req->input('satuan_kerja_id') != 3){
				$idkec = SatuanKerja::where('satuan_kerja_nama','ilike','kecamatan%')->lists('satuan_kerja_id')->toArray();
				$allkec = UnitKerja::whereIn('satuan_kerja_id',$idkec)->lists('unit_kerja_id')->toArray();
				$kel = UnitKerja::where('unit_kerja_nama','ilike','kelurahan%')->lists('unit_kerja_id')->toArray();
				$allkel = UnitKerja::whereIn('unit_kerja_parent',$kel)->lists('unit_kerja_id')->toArray();
				$kelid = array_merge($kel,$allkel);

				if(in_array($req->input('unit_kerja_id'), $idkec)){
					$searchunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
					$nmkec = SatuanKerja::where('satuan_kerja_id',$searchunit->satuan_kerja_id)->first();
					$rw_jab->riw_jabatan_unit = $nmkec->satuan_kerja_nama;
				}elseif (in_array($req->input('unit_kerja_id'), $kelid)){
					$searchunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
					$nmkel = $searchunit->unit_kerja_parent ? UnitKerja::where('unit_kerja_id',$searchunit->unit_kerja_parent)->first() : $searchunit;
					$rw_jab->riw_jabatan_unit = $nmkel->unit_kerja_nama;
				}else{
					$nmskpd = SatuanKerja::where('satuan_kerja_id',$req->input('satuan_kerja_id'))->first();
					$rw_jab->riw_jabatan_unit = $nmskpd->satuan_kerja_nama;
				}
			}else{
				$searchunit = UnitKerja::where('unit_kerja_nama','ilike','bagian%')->lists('unit_kerja_id')->toArray();
				$bagian = UnitKerja::whereIn('unit_kerja_parent',$searchunit)->lists('unit_kerja_id')->toArray();
				if(in_array($req->input('unit_kerja_id'), $bagian)){
					$nmunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
					$namaunit = $nmunit->unit_kerja_parent ? unitKerja::where('unit_kerja_id',$nmunit->unit_kerja_parent)->first() : $nmunit;
					$rw_jab->riw_jabatan_unit = $nmunit->unit_kerja_nama;
				}else{
					$nmunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
					$rw_jab->riw_jabatan_unit = $nmunit->unit_kerja_nama;
				}
			}
			$rw_jab->riw_jabatan_nm = $req->input('jabatan_nama');
			$rw_jab->riw_jabatan_no = $req->input('no_sk_jabatan');
			$rw_jab->riw_jabatan_tgl = saveDate($req->input('tanggal_sk_jabatan'));
			$rw_jab->riw_jabatan_pejabat = $req->input('jabatan_penandatangan_jab');
			$rw_jab->gol_id = $req->input('gol_id_akhir');
			$rw_jab->save();

			$pegawai2 = Pegawai2::find($id);
			if($pegawai2){
				$cekriwjab2 = RiwayatJabatan2::where('peg_id',$pegawai->peg_id)->orderBy('riw_jabatan_tmt','desc')->first();
				if($cekriwjab2 && $cekriwjab2->riw_jabatan_tmt == saveDate($req->input('peg_jabatan_tmt'))){
					$rw_jab2 = RiwayatJabatan2::find($cekriwjab2->riw_jabatan_id);
					$rw_jab2->riw_jabatan_nm = $req->input('jabatan_nama');
				}else{
					$rw_jab2 = new RiwayatJabatan2;
					$rw_jab2->riw_jabatan_id = $rw_jab->riw_jabatan_id;
					$rw_jab2->peg_id = $rw_jab->peg_id;
					$rw_jab2->riw_jabatan_tmt = $rw_jab->riw_jabatan_tmt;
					$rw_jab2->riw_jabatan_unit = $rw_jab->riw_jabatan_unit;
					$rw_jab2->riw_jabatan_nm = $rw_jab->riw_jabatan_nm;
					$rw_jab2->riw_jabatan_no = $rw_jab->riw_jabatan_no;
					$rw_jab2->riw_jabatan_tgl = $rw_jab->riw_jabatan_tgl;
					$rw_jab2->riw_jabatan_pejabat = $rw_jab->riw_jabatan_pejabat;
					$rw_jab2->gol_id = $rw_jab->gol_id;
				}
				$rw_jab2->save();
			}

			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : ($jabs ? $jabs->unit_kerja_id : null);
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

			if($pegawai->jabatan_id_evjab != $req->input('jabatan_id')){
				if($req->input('jenis_jabatan')==2){
					$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
				}else if ($req->input('jenis_jabatan')==4){
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jfu_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 4;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
						$jab->jfu_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id_evjab = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id_evjab = $jab->jabatan_id;
					}
				}elseif ($req->input('jenis_jabatan')==3) {
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jf_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 3;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
						$jab->jf_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}
			}
			if($pegawai->save()){
				return Redirect::to('/evjab/search/profile/edit/'.$pegawai->peg_id)->with('message', 'Data Berhasil Diupdate!');
			}
		}else{
			return Redirect::to('/evjab/search/profile/edit/'.$pegawai->peg_id)->with('message', 'Data Berhasil Diupdate!');
		}
	}

	public function evjabUpdateUjikomPegawai($id, Request $req){
		$data = $req->except('_token');
		date_default_timezone_set("Asia/Bangkok");
		$time = date("Y-m-d");

		$pegawai = PegawaiEvjab::find($id);
		if($pegawai){			
			if(empty($req->input('jabatan_id')) || $req->input('jabatan_id') == null){
				return Redirect::to('/evjab/search/edit-pegawai-ujikom/'.$pegawai->peg_id)->with('message', 'Gagal! Silahkan Pilih Jabatan Baru Terlebih Dahulu.');
			}
			if(empty($req->input('unit_kerja_id_evjab')) || $req->input('unit_kerja_id_evjab') == null){
				return Redirect::to('/evjab/search/edit-pegawai-ujikom/'.$pegawai->peg_id)->with('message', 'Gagal! Silahkan Pilih Unit Kerja Baru Terlebih Dahulu.');
			}

		}
		$pegawai->unit_kerja_id_evjab = $req->input('unit_kerja_id_evjab');

		$jabs = Jabatan::where('jabatan_id',$req->input('jabatan_id'))->first();

		if($pegawai->jabatan_id_evjab != (int) $req->input('jabatan_id')){																
			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : ($jabs ? $jabs->unit_kerja_id : null);
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

			if($pegawai->jabatan_id_evjab != $req->input('jabatan_id')){
				if($req->input('jenis_jabatan')==2){
					$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
				}else if ($req->input('jenis_jabatan')==4){
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jfu_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 4;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id_evjab') ? $req->input('unit_kerja_id_evjab') : null;
						$jab->jfu_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id_evjab = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id_evjab = $jab->jabatan_id;
					}
				}elseif ($req->input('jenis_jabatan')==3) {
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jf_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 3;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
						$jab->jf_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}
			}
			if($pegawai->save()){
				return Redirect::to('/evjab/search/profile-ujikom/edit/'.$pegawai->peg_id)->with('message', 'Data Berhasil Diupdate!');
			}
		}else{
			return Redirect::to('/evjab/search/profile-ujikom/edit/'.$pegawai->peg_id)->with('message', 'Data Berhasil Diupdate!');
		}
	}

	public function evjabIndex(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		ini_set('max_execution_time', 10000);
    	ini_set('memory_limit','2048M');
		$id = Auth::user()->satuan_kerja_id;
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();		
		$pegawai = PegawaiEvjabLengkap::where('satuan_kerja_id', $id)->where('is_deleted', false)->where('is_hidden', false)->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama')->get();
		return view('pages.jfubaru.list_pegawai', compact('id', 'satker','pegawai'));
	}

	public function evjabListUnit($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$pegawai = PegawaiEvjabLengkap::where('satuan_kerja_id', $id)->where('is_hidden', false)->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama')->get();
		return view('pages.jfubaru.list_pegawai', compact('id', 'satker','pegawai'));
	}

	public function evjabListPegawaiNaik(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		$pegawai = PegawaiNaikTurun::whereRaw('jfu_kelas_2019 > jfu_kelas')->orderBy('satuan_kerja_id')->orderBy('peg_nip')->get();
		return view('pages.jfubaru.list_pegawai_naik', compact('pegawai'));
	}

	public function evjabDownloadPegawaiNaik(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', 10000);

		$all = false;
		$data = array();
		$header = array();
		$query = PegawaiNaikTurun::whereRaw('jfu_kelas_2019 > jfu_kelas')->orderBy('satuan_kerja_id')->orderBy('peg_nip');

		$query = $query->get();
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
			$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
			$jabatan2019 = Jabatan::where('jabatan_id', $a->jabatan_id_evjab)->first();
			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.' '.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.' '.$a->peg_nama;
			}
			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
			if($jfu){
				$nama_jabatan = $jfu['jfu_nama'];
			}else{
				$nama_jabatan = 'Pelaksana';
			}
			$nama_jabatan_2019 = '';
			if($jabatan2019){
				$jfu2019 = JfuBaru::where('jfu_id', $jabatan2019->jfu_id)->first();
				if($jfu2019){
					$nama_jabatan_2019 = $jfu2019['jfu_nama'];
				}else{
					$nama_jabatan_2019 = '';
				}
			}			
			$unit_kerja = UnitKerja::where('unit_kerja_id', $a->unit_kerja_id)->first();

			$d = [
				$i+1,
				$a->peg_nip,
				$nama,				
				$golongan ? $golongan->nm_gol.', '.$golongan->nm_pkt : '',
				$unit_kerja->unit_kerja_nama,
				$nama_jabatan,
				$nama_jabatan_2019,                
			];			
			$data[] = $d;			
		}

		$h= [
			'NO',	// A
			'NIP',	// B
			'NAMA',	// C
			'GOL',	// D
			'UNIT KERJA',	// E
			'JABATAN LAMA',	// F
			'JABATAN 2019',	// G
		];
		$header = $h;		
		
		Excel::create('Daftar Pegawai Naik Kelas', function($excel) use ($data, $header) {
			$excel->sheet('Daftar Pegawai Naik Kelas', function($sheet) use ($data, $header) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(11);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:G2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->row(2, ['DAFTAR PEGAWAI NAIK KELAS ']);				

				$sheet->cells('A4:G4', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#cccccc');
				});

				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);

				$sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'C' => '@',
				    'D' => '@',
				    'E' => '@',
				    'F' => '@',
				    'G' => '@',				    		    
				));
				$sheet->row(4, $header);
				$sheet->setAutoFilter('A4:G4');
				$sheet->setBorder('A4:G4', 'thin');
				$sheet->fromArray($data, "", "A5", true, false);

				for ($j = 5; $j < count($data) + 5; $j++) {
					$sheet->setBorder('A'.$j.':G'.$j, 'thin');
				}
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});

		})->download('xlsx');
	}

	public function evjabListPegawaiTurun(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		$pegawai = PegawaiNaikTurun::whereRaw('jfu_kelas_2019 < jfu_kelas')->orderBy('satuan_kerja_id')->orderBy('peg_nip')->get();
		return view('pages.jfubaru.list_pegawai_turun', compact('pegawai'));
	}

	public function evjabListPegawaiBelum(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		$pegawai = PegawaiBelum::orderBy('satuan_kerja_id')->orderBy('peg_nip')->get();
		return view('pages.jfubaru.list_pegawai_belum', compact('pegawai'));
	}

	public function evjabListPegawaiUjikom(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		$pegawai = PegawaiEvjabUjikom::where('is_hidden', true)->orderBy('satuan_kerja_id')->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama')->get();
		return view('pages.jfubaru.list_pegawai_ujikom', compact('pegawai'));
	}

	public function evjabDownloadPegawaiUjikom(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}

		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', 10000);

		$all = false;
		$data = array();
		$header = array();
		$query = PegawaiEvjabUjikom::where('is_hidden', true)->orderBy('satuan_kerja_id')->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama');

		$query = $query->get();
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
			$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
			$jabatan2019 = Jabatan::where('jabatan_id', $a->jabatan_id_evjab)->first();
			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
			}
			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
			if($jfu){
				$nama_jabatan = $jfu['jfu_nama'];
				$kelas_lama = $jfu['jfu_kelas'];
			}else{
				$nama_jabatan = 'Pelaksana';
				$kelas_lama = 5;
			}
			$nama_jabatan_2019 = '';
			if($jabatan2019){
				$jfu2019 = JfuBaru::where('jfu_id', $jabatan2019->jfu_id)->first();
				if($jfu2019){
					$nama_jabatan_2019 = $jfu2019['jfu_nama'];
					$kelas_baru = $jfu2019['jfu_kelas'];
				}else{
					$nama_jabatan_2019 = 'Pelaksana';
					$kelas_baru = 5;
				}
			}			
			$unit_kerja = UnitKerja::where('unit_kerja_id', $a->unit_kerja_id)->first();
			$satuan_kerja = SatuanKerja::where('satuan_kerja_id', $a->satuan_kerja_id)->first();
			$unit_kerja_evjab = UnitKerja::where('unit_kerja_id', $a->unit_kerja_id_evjab)->first();

			$d = [
				$i+1,
				$a->peg_nip,
				$nama,				
				$golongan ? $golongan->nm_gol.', '.$golongan->nm_pkt : '',
				$satuan_kerja->satuan_kerja_nama,
				$nama_jabatan,
				$kelas_lama,
				$unit_kerja->unit_kerja_nama,
				$nama_jabatan_2019,
				$kelas_baru,
				$unit_kerja_evjab->unit_kerja_nama,
			];			
			$data[] = $d;			
		}

		$h= [
			'NO',	// A
			'NIP',	// B
			'NAMA',	// C
			'GOL',	// D
			'OPD',	// E
			'NAMA JABATAN',	// F
			'KELAS', // G
			'UNIT KERJA',	// H
			'NAMA JABATAN BARU',	// I
			'KELAS', // J
			'UNIT KERJA BARU', // K
		];
		$header = $h;		
		
		Excel::create('Daftar Pegawai Naik Kelas', function($excel) use ($data, $header) {
			$excel->sheet('Daftar Pegawai Naik Kelas', function($sheet) use ($data, $header) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(11);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:K2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->row(2, ['DAFTAR PEGAWAI LULUS UJI KOMPETENSI ']);				

				$sheet->cells('A4:K4', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#cccccc');
				});

				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);

				$sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'C' => '@',
				    'D' => '@',
				    'E' => '@',
				    'F' => '@',
					'G' => '@',				    		    
					'H' => '@',
					'I' => '@',
					'J' => '@',
					'K' => '@',
				));
				$sheet->row(4, $header);
				$sheet->setAutoFilter('A4:K4');
				$sheet->setBorder('A4:K4', 'thin');
				$sheet->fromArray($data, "", "A5", true, false);

				for ($j = 5; $j < count($data) + 5; $j++) {
					$sheet->setBorder('A'.$j.':K'.$j, 'thin');
				}
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});

		})->download('xlsx');
	}

	public function revisiPegawai($id, Request $req){
		$input = $req->except('_token');
		$pegawai = Pegawai2::where('peg_id', $id)->first();
		$log=StatusEditPegawai::where('peg_id', $id)->orderBy('id', 'desc')->first();
		if($log){
			$log->action ="revisi_data";
			if(isDisdik()){
				$log->status_id =6;
			}
			else{
				$log->status_id =3;
			}
			$log->editor_id = Auth::user()->id;
			$log->save();
		}else{
			$log = New StatusEditPegawai;
			$log->peg_id = $id;
			$log->peg_nip = $pegawai->peg_nip;
			$log->action ="revisi_data";
			if(isDisdik()){
				$log->status_id =6;
			}
			else{
				$log->status_id =3;
			}
			$log->editor_id = Auth::user()->id;
			$log->save();
		}

		$pensiun = PegawaiPensiun::where('peg_id',$id)->first();
		if($pensiun && $pegawai->peg_status == false){
			$pensiun->delete();

			$pegawai->peg_status = true;
			$pegawai->peg_ketstatus = null;
			$pegawai->save();
		}

		$revisi = new LogEditPegawai;
		$revisi->keterangan =$req->input('keterangan');
		$revisi->peg_id =$id;
		$revisi->status_id = 3;
		$revisi->user_id = Auth::user()->id;
		$revisi->save();

		return Redirect::to('/pegawai/profile/edit/'.$id)->with('message', 'keterangan revisi telah diterima');
	}

	public function approvePegawaiBaru($id){
		DB::beginTransaction();
		try{
			$temp_id = $id;
			$pegawai = new Pegawai;
			$p = Pegawai2::where('peg_id', $id)->first();
			$pgid=Pegawai::select('peg_id')->max('peg_id');
			$findpgid = Pegawai::where('peg_id', $pgid)->first();
			if($findpgid){
				$pgid = $pgid+1;
				$pegawai->peg_id = $pgid;
				$temp_id = $pgid;
			}else{
				$pegawai->peg_id = $pgid;
				$temp_id = $pgid;
			}

			$pegawai->id_goldar=$p['id_goldar'];
			$pegawai->gol_id_awal=$p['gol_id_awal'];
			$pegawai->id_pend_awal=$p['id_pend_awal'];
			$pegawai->unit_kerja_id=$p['unit_kerja_id'];
			$pegawai->kecamatan_id=$p['kecamatan_id'];
			$pegawai->gol_id_akhir=$p['gol_id_akhir'];
			$pegawai->jabatan_id=$p['jabatan_id'];
			$pegawai->id_pend_akhir=$p['id_pend_akhir'];
			$pegawai->id_agama=$p['id_agama'];
			$pegawai->peg_nip=$p['peg_nip'];
			$pegawai->peg_nama=$p['peg_nama'];
			$pegawai->peg_gelar_depan=$p['peg_gelar_depan'];
			$pegawai->peg_gelar_belakang=$p['peg_gelar_belakang'];
			$pegawai->peg_lahir_tempat=$p['peg_lahir_tempat'];
			$pegawai->peg_lahir_tanggal=$p['peg_lahir_tanggal'];
			$pegawai->peg_jenis_kelamin=$p['peg_jenis_kelamin'];
			$pegawai->peg_status_perkawinan=$p['peg_status_perkawinan'];
			$pegawai->peg_karpeg=$p['peg_karpeg'];
			$pegawai->peg_karsutri=$p['peg_karsutri'];
			$pegawai->peg_status_kepegawaian=$p['peg_status_kepegawaian'];
			$pegawai->peg_cpns_tmt=$p['peg_cpns_tmt'];
			$pegawai->peg_pns_tmt=$p['peg_pns_tmt'];
			$pegawai->peg_gol_awal_tmt=$p['peg_gol_awal_tmt'];
			$pegawai->peg_kerja_tahun=$p['peg_kerja_tahun'];
			$pegawai->peg_kerja_bulan=$p['peg_kerja_bulan'];
			$pegawai->peg_jabatan_tmt=$p['peg_jabatan_tmt'];
			$pegawai->peg_no_askes=$p['peg_no_askes'];
			$pegawai->peg_npwp=$p['peg_npwp'];
			$pegawai->peg_bapertarum=$p['peg_bapertarum'];
			$pegawai->peg_rumah_alamat=$p['peg_rumah_alamat'];
			$pegawai->peg_kel_desa=$p['peg_kel_desa'];
			$pegawai->peg_kodepos=$p['peg_kodepos'];
			$pegawai->peg_rumah_alamat=$p['peg_rumah_alamat'];
			$pegawai->peg_telp=$p['peg_telp'];
			$pegawai->peg_telp_hp=$p['peg_telp_hp'];
			$pegawai->peg_tmt_kgb=$p['peg_tmt_kgb'];
			$pegawai->peg_pend_awal_th=$p['peg_pend_awal_th'];
			$pegawai->peg_pend_akhir_th=$p['peg_pend_akhir_th'];
			$pegawai->peg_gol_akhir_tmt=$p['peg_gol_akhir_tmt'];
			$pegawai->tgl_entry=$p['tgl_entry'];
			$pegawai->satuan_kerja_id=$p['satuan_kerja_id'];
			$pegawai->peg_status=$p['peg_status'];
			$pegawai->peg_nip_lama=$p['peg_nip_lama'];
			$pegawai->peg_ktp=$p['peg_ktp'];
			$pegawai->peg_instansi_dpk=$p['peg_instansi_dpk'];
			$pegawai->peg_ak=$p['peg_ak'];
			$pegawai->peg_punya_kpe=$p['peg_punya_kpe'];
			$pegawai->peg_punya_kpe_tgl=$p['peg_punya_kpe_tgl'];
			$pegawai->peg_email=$p['peg_email'];
			$pegawai->peg_status_asn =  $p['peg_status_asn'];
			$pegawai->peg_status_gaji = $p['peg_status_gaji'];
			$pegawai->id_status_kepegawaian = $p['id_status_kepegawaian'];
			$pegawai->peg_jenis_asn=$p['peg_jenis_asn'];
			$pegawai->peg_status_calon=$p['peg_status_calon'];
			$pegawai->peg_jenis_pns=$p['peg_jenis_pns'];
			$pegawai->jenis_guru = $p['jenis_guru'];
			$pegawai->status_tkd=$p['status_tkd'];
			$pegawai->kedudukan_pegawai=$p['kedudukan_pegawai'];

			if($p['peg_foto']!=null){
				if(File::exists('uploads/'.$p['peg_foto'])){
					$file = 'uploads/'.$p['peg_foto'];
					$data = File::get($file);
					//$new = '/var/www/simpeg/var/uploads/foto/'.$p->peg_foto;
					//file_put_contents($new, $data);
					$pegawai->peg_foto=$p['peg_foto'];
				}
			}else{
				$pegawai->peg_foto='none.gif';
			}

			if($pegawai->save()){
				Pegawai2::where('peg_id', $id)->delete();
				$pErk = new PegawaiErk;
				$pErk->peg_id = $pegawai->peg_id;
				$pErk->id_goldar = $pegawai->id_goldar;
				$pErk->gol_id_awal = $pegawai->gol_id_awal;
				$pErk->id_pend_awal = $pegawai->id_pend_awal;
				$pErk->unit_kerja_id = $pegawai->unit_kerja_id;
				$pErk->kecamatan_id = $pegawai->kecamatan_id;
				$pErk->gol_id_akhir = $pegawai->gol_id_akhir;
				$pErk->jabatan_id = $pegawai->jabatan_id;
				$pErk->id_pend_akhir = $pegawai->id_pend_akhir;
				$pErk->id_agama = $pegawai->id_agama;
				$pErk->peg_nip = $pegawai->peg_nip;
				$pErk->peg_nama = $pegawai->peg_nama;
				$pErk->peg_gelar_depan = $pegawai->peg_gelar_depan;
				$pErk->peg_gelar_belakang = $pegawai->peg_gelar_belakang;
				$pErk->peg_lahir_tempat = $pegawai->peg_lahir_tempat;
				$pErk->peg_lahir_tanggal = $pegawai->peg_lahir_tanggal;
				$pErk->peg_jenis_kelamin = $pegawai->peg_jenis_kelamin;
				$pErk->peg_status_perkawinan = $pegawai->peg_status_perkawinan;
				$pErk->peg_karpeg = $pegawai->peg_karpeg;
				$pErk->peg_karsutri = $pegawai->peg_karsutri;
				$pErk->peg_status_kepegawaian = $pegawai->peg_status_kepegawaian;
				$pErk->peg_cpns_tmt = $pegawai->peg_cpns_tmt;
				$pErk->peg_pns_tmt = $pegawai->peg_pns_tmt;
				$pErk->peg_gol_awal_tmt = $pegawai->peg_gol_awal_tmt;
				$pErk->peg_kerja_tahun = $pegawai->peg_kerja_tahun;
				$pErk->peg_kerja_bulan = $pegawai->peg_kerja_bulan;
				$pErk->peg_jabatan_tmt = $pegawai->peg_jabatan_tmt;
				$pErk->peg_no_askes = $pegawai->peg_no_askes;
				$pErk->peg_npwp = $pegawai->peg_npwp;
				$pErk->peg_bapertarum = $pegawai->peg_bapertarum;
				$pErk->peg_rumah_alamat = $pegawai->peg_rumah_alamat;
				$pErk->peg_kel_desa = $pegawai->peg_kel_desa;
				$pErk->peg_kodepos = $pegawai->peg_kodepos;
				$pErk->peg_rumah_alamat = $pegawai->peg_rumah_alamat;
				$pErk->peg_telp = $pegawai->peg_telp;
				$pErk->peg_telp_hp = $pegawai->peg_telp_hp;
				$pErk->peg_tmt_kgb = $pegawai->peg_tmt_kgb;
				$pErk->peg_pend_awal_th = $pegawai->peg_pend_awal_th;
				$pErk->peg_pend_akhir_th = $pegawai->peg_pend_akhir_th;
				$pErk->peg_gol_akhir_tmt = $pegawai->peg_gol_akhir_tmt;
				$pErk->tgl_entry = $pegawai->tgl_entry;
				$pErk->satuan_kerja_id = $pegawai->satuan_kerja_id;
				$pErk->peg_status = $pegawai->peg_status;
				$pErk->peg_nip_lama = $pegawai->peg_nip_lama;
				$pErk->peg_ktp = $pegawai->peg_ktp;
				$pErk->peg_instansi_dpk = $pegawai->peg_instansi_dpk;
				$pErk->peg_ak = $pegawai->peg_ak;
				$pErk->peg_punya_kpe = $pegawai->peg_punya_kpe;
				$pErk->peg_punya_kpe_tgl = $pegawai->peg_punya_kpe_tgl;
				$pErk->peg_email = $pegawai->peg_email;
				$pErk->peg_status_asn = $pegawai->peg_status_asn;
				$pErk->peg_status_gaji = $pegawai->peg_status_gaji;
				$pErk->id_status_kepegawaian = $pegawai->id_status_kepegawaian;
				$pErk->peg_jenis_asn = $pegawai->peg_jenis_asn;
				$pErk->peg_status_calon = $pegawai->peg_status_calon;
				$pErk->peg_jenis_pns = $pegawai->peg_jenis_pns;
				$pErk->jenis_guru = $pegawai->jenis_guru;
				$pErk->status_tkd = $pegawai->status_tkd;
				$pErk->kedudukan_pegawai = $pegawai->kedudukan_pegawai;
				$pErk->peg_foto = $pegawai->peg_foto;
				$pErk->save();
			}

			$pg = RiwayatPenghargaan2::where('peg_id', $id)->get();
			foreach ($pg as $pg2) {
				$id_pg2 = $pg2->riw_penghargaan_id;
				$rp = new RiwayatPenghargaan;
				$id_rp=RiwayatPenghargaan::select('riw_penghargaan_id')->max('riw_penghargaan_id');
				$find_rpid = RiwayatPenghargaan::where('riw_penghargaan_id', $id_rp)->first();
				if($find_rpid){
					$id_rp = $id_rp+1;
					$rp->riw_penghargaan_id = $id_rp;
				}else{
					$rp->riw_penghargaan_id = $id_rp;
				}
				$rp->peg_id = $temp_id;
				$rp->penghargaan_id = $pg2->penghargaan_id;
				$rp->riw_penghargaan_instansi= $pg2->riw_penghargaan_instansi;
				$rp->riw_penghargaan_sk = $pg2->riw_penghargaan_sk;
				$rp->riw_penghargaan_tglsk = saveDate($pg2->riw_penghargaan_tglsk);
				$rp->riw_penghargaan_jabatan = $pg2->riw_penghargaan_jabatan;
				$rp->riw_penghargaan_lokasi = $pg2->riw_penghargaan_lokasi;
				if($rp->save()){
					RiwayatPenghargaan2::where('riw_penghargaan_id',$id_pg2)->delete();
				}
			}

			$pf = RiwayatPendidikan2::where('peg_id', $id)->get();
			foreach ($pf as $pf2) {
				$id_pf2 = $pf2->riw_pendidikan_id;
				$rp2=new RiwayatPendidikan;
				$id_rp2=RiwayatPendidikan::select('riw_pendidikan_id')->max('riw_pendidikan_id');
				$find_rpid2 = RiwayatPendidikan::where('riw_pendidikan_id', $id_rp2)->first();
				if($find_rpid2){
					$id_rp2 = $id_rp2+1;
					$rp2->riw_pendidikan_id = $id_rp2;
				}else{
					$rp2->riw_pendidikan_id = $id_rp2;
				}
				$rp2->peg_id =  $temp_id;
				$rp2->id_pend = $pf2->id_pend;
				$rp2->tingpend_id= $pf2->tingpend_id;
				$rp2->riw_pendidikan_fakultas= $pf2->riw_pendidikan_fakultas;
				$rp2->jurusan_id= $pf2->jurusan_id;
				$rp2->univ_id = $pf2->univ_id;
				$rp2->riw_pendidikan_sttb_ijazah= $pf2->riw_pendidikan_sttb_ijazah;
				$rp2->riw_pendidikan_tgl = saveDate($pf2->riw_pendidikan_tgl);
				$rp2->riw_pendidikan_pejabat = $pf2->riw_pendidikan_pejabat;
				$rp2->riw_pendidikan_nm = $pf2->riw_pendidikan_nm;
				$rp2->riw_pendidikan_lokasi = $pf2->riw_pendidikan_lokasi;
				$rp2->fakultas_id = $pf2->fakultas_id;
				if($rp2->save()){
					RiwayatPendidikan2::where('riw_pendidikan_id', $id_pf2)->delete();
				}
			}

			$pn = RiwayatNonFormal2::where('peg_id', $id)->get();
			foreach ($pn as $pn2) {
				$id_pn2 = $pn2->non_id;
				$rp3=new  RiwayatNonFormal;
				$id_rp3=RiwayatNonFormal::select('non_id')->max('non_id');
				$find_rpid3 = RiwayatNonFormal::where('non_id', $id_rp3)->first();
				if($find_rpid3){
					$id_rp3 = $id_rp3+1;
					$rp3->non_id = $id_rp3;
				}else{
					$rp3->non_id = $id_rp3;
				}
				$rp3->peg_id =  $temp_id;
				$rp3->non_nama= $pn2->non_nama;
				$rp3->non_tgl_mulai = saveDate($pn2->non_tgl_mulai);
				$rp3->non_tgl_selesai = saveDate($pn2->non_tgl_selesai);
				$rp3->non_sttp = $pn2->non_sttp;
				$rp3->non_sttp_tanggal = saveDate($pn2->non_sttp_tanggal);
				$rp3->non_sttp_pejabat = $pn2->non_sttp_pejabat;
				$rp3->non_penyelenggara = $pn2->non_penyelenggara;
				$rp3->non_tempat = $pn2->non_tempat;
				if($rp3->save()){
					RiwayatNonFormal2::where('non_id', $id_pn2)->delete();
				}
			}

			$pp = RiwayatPangkat2::where('peg_id', $id)->get();
			foreach ($pp as $pp2) {
				$id_pp2 = $pp2->riw_pangkat_id;
				$rp4=new RiwayatPangkat;
				$id_rp4=RiwayatPangkat::select('riw_pangkat_id')->max('riw_pangkat_id');
				$find_rpid4 = RiwayatPangkat::where('riw_pangkat_id', $id_rp4)->first();
				if($find_rpid4){
					$id_rp4 = $id_rp4+1;
					$rp4->riw_pangkat_id = $id_rp4;
				}else{
					$rp4->riw_pangkat_id = $id_rp4;
				}

				$rp4->peg_id =  $temp_id;
				$rp4->riw_pangkat_thn= $pp2->riw_pangkat_thn;
				$rp4->riw_pangkat_bln = $pp2->riw_pangkat_bln;
				$rp4->riw_pangkat_gapok = $pp2->riw_pangkat_gapok;
				$rp4->riw_pangkat_sk = $pp2->riw_pangkat_sk;
				$rp4->riw_pangkat_sktgl =  saveDate($pp2->riw_pangkat_sktgl);
				$rp4->riw_pangkat_tmt =  saveDate($pp2->riw_pangkat_tmt);
				$rp4->riw_pangkat_pejabat = $pp2->riw_pangkat_pejabat;
				$rp4->riw_pangkat_unit_kerja = $pp2->riw_pangkat_unit_kerja;
				$rp4->gol_id = $pp2->gol_id;
				if($rp4->save()){
					RiwayatPangkat2::where('riw_pangkat_id', $id_pp2)->delete();
				}
			}

			/* $kgb = Kgb_skpd::where('peg_id', $id)->where('status_data', 'manual')->get();
			foreach ($kgb as $pp2) {
				$id_pp2 = $pp2->kgb_id;
				$rp4=new KGB;
				$id_rp4=KGB::select('kgb_id')->max('kgb_id');
				$find_rpid4 = KGB::where('kgb_id', $id_rp4)->first();
				if($find_rpid4){
					$id_rp4 = $id_rp4+1;
					$rp4->kgb_id = $id_rp4;
				}else{
					$rp4->kgb_id = $id_rp4;
				}

				$rp4->peg_id =  $pp2->peg_id;
				$rp4->kgb_kerja_tahun= $pp2->kgb_kerja_tahun;
				$rp4->kgb_kerja_bulan = $pp2->kgb_kerja_bulan;
				$rp4->kgb_gapok = $pp2->kgb_gapok;
				$rp4->kgb_nosk = $pp2->kgb_nosk;
				$rp4->kgb_tglsk =  saveDate($pp2->kgb_tglsk);
				$rp4->kgb_tmt =  saveDate($pp2->kgb_tmt);
				$rp4->ttd_pejabat = $pp2->ttd_pejabat;
				$rp4->unit_kerja = $pp2->unit_kerja;
				$rp4->gol_id = $pp2->gol_id;
				$rp4->kgb_thn = date('Y');
				$rp4->kgb_bln = date('m');
				$rp4->kgb_status = 50;
				$rp4->user_id = Auth::user()->id;
				$rp4->ttd_digital = 50;
				$rp4->status_data = 'manual';
				if($rp4->save()){
					Kgb_skpd::where('kgb_id', $pp2->kgb_id)->delete();
				}
			}

			$pmk = Pmk_skpd::where('peg_id', $id)->get();
			foreach ($pmk as $pp2) {
				// $id_pp2 = $pp2->kgb_id;
				$rp4=new Pmk_skpd;
				
				$rp->peg_id = $pp2->peg_id;
				$rp->created_by = $pp2->created_by;
				$rp->updated_by = $pp2->updated_by;
				
				$rp->pmk_tahun = $pp2->pmk_tahun;
				$rp->pmk_bulan = $pp2->pmk_bulan;
				$rp->masa_kerja_lama_thn = $pp2->masa_kerja_lama_thn;
				$rp->masa_kerja_lama_bln = $pp2->masa_kerja_lama_bln;
				$rp->pmk_gapok_lama = $pp2->pmk_gapok_lama;
		
				$rp->masa_kerja_baru_thn = $pp2->masa_kerja_baru_thn;
				$rp->masa_kerja_baru_bln = $pp2->masa_kerja_baru_bln;
				$rp->pmk_gapok_baru = $pp2->pmk_gapok_baru;
		
				$rp->pejabat_id = $pp2->pejabat_id;
				$rp->pmk_jabatan = $pp2->pmk_jabatan;
				
				$rp->pmk_nosk = $pp2->pmk_nosk;
				$rp->pmk_sktgl =  $pp2->pmk_sktgl;
				$rp->pmk_tmt_lama =  $pp2->pmk_tmt_lama;
				$rp->pmk_tmt_baru =  $pp2->pmk_tmt_baru;
				$rp->pmk_ttd_pejabat = $pp2->pmk_ttd_pejabat;
				$rp->pmk_unit_kerja = $pp2->pmk_unit_kerja;
				$rp->gol_id = $pp2->gol_id;
				if($rp4->save()){
					Pmk_skpd::where('peg_id', $pp2->peg_id)->delete();
				}
			} */

			$pj = RiwayatJabatan2::where('peg_id', $id)->get();
			foreach ($pj as $pj2) {
				$id_pj2 = $pj2->riw_jabatan_id;
				$rp5=new RiwayatJabatan;
				$id_rp5=RiwayatJabatan::select('riw_jabatan_id')->max('riw_jabatan_id');
				$find_rpid5 = RiwayatJabatan::where('riw_jabatan_id', $id_rp5)->first();
				if($find_rpid5){
					$id_rp5 = $id_rp5+1;
					$rp5->riw_jabatan_id = $id_rp5;
				}else{
					$rp5->riw_jabatan_id = $id_rp5;
				}
				$rp5->peg_id =  $temp_id;
				$rp5->riw_jabatan_nm= $pj2->riw_jabatan_nm;
				$rp5->riw_jabatan_no = $pj2->riw_jabatan_no;
				$rp5->riw_jabatan_pejabat = $pj2->riw_jabatan_pejabat;
				$rp5->riw_jabatan_tgl = saveDate($pj2->riw_jabatan_tgl);
				$rp5->riw_jabatan_tmt =  saveDate($pj2->riw_jabatan_tmt);
				$rp5->riw_jabatan_unit = $pj2->riw_jabatan_unit;
				$rp5->gol_id = $pj2->gol_id;
				if($rp5->save()){
					RiwayatJabatan2::where('riw_jabatan_id', $id_pj2)->delete();
				}
			}

			$pd = RiwayatDiklat2::where('peg_id',$id)->get();
			foreach ($pd as $pd2) {
				$id_pd2 = $pd2->diklat_id;
				$rp6=new RiwayatDiklat;
				$id_rp6=RiwayatDiklat::select('diklat_id')->max('diklat_id');
				$find_rpid6 = RiwayatDiklat::where('diklat_id', $id_rp6)->first();
				if($find_rpid6){
					$id_rp6 = $id_rp6+1;
					$rp6->diklat_id = $id_rp6;
				}else{
					$rp6->diklat_id = $id_rp6;
				}
				$rp6->peg_id =  $temp_id;
				$rp6->kategori_id= $pd2->kategori_id;
				$rp6->diklat_fungsional_id= $pd2->diklat_fungsional_id;
				$rp6->diklat_teknis_id= $pd2->diklat_teknis_id;
				$rp6->diklat_jenis = $pd2->diklat_jenis;
				$rp6->diklat_mulai = saveDate($pd2->diklat_mulai);
				$rp6->diklat_selesai = saveDate($pd2->diklat_selesai);
				$rp6->diklat_penyelenggara = $pd2->diklat_penyelenggara;
				$rp6->diklat_tempat = $pd2->diklat_tempat;
				$rp6->diklat_jumlah_jam = $pd2->diklat_jumlah_jam;
				$rp6->diklat_sttp_no = $pd2->diklat_sttp_no;
				$rp6->diklat_sttp_tgl =  saveDate($pd2->diklat_sttp_tgl);
				$rp6->diklat_sttp_pej = $pd2->diklat_sttp_pej;
				$rp6->diklat_usul_no = $pd2->diklat_usul_no;
				$rp6->diklat_usul_tgl = $pd2->diklat_usul_tgl;
				$rp6->pejabat_id = $pd2->pejabat_id;
				$rp6->diklat_nama = $pd2->diklat_nama;
				$rp6->diklat_tipe = $pd2->diklat_tipe;
				if($rp6->save()){
					RiwayatDiklat2::where('diklat_id', $id_pd2)->delete();
				}
			}

			$pr = RiwayatKeluarga2::where('peg_id', $id)->get();
			foreach ($pr as $pr2) {
				$id_pr2 = $pr2->riw_id;
				$rp7=new RiwayatKeluarga;
				$id_rp7=RiwayatKeluarga::select('riw_id')->max('riw_id');
				$find_rpid7 = RiwayatKeluarga::where('riw_id', $id_rp7)->first();
				if($find_rpid7){
					$id_rp7 = $id_rp7+1;
					$rp7->riw_id = $id_rp7;
				}else{
					$rp7->riw_id = $id_rp7;
				}
				$rp7->peg_id =  $temp_id;
				$rp7->riw_status = $pr2->riw_status;
				$rp7->nik= $pr2->nik;
				$rp7->riw_nama= $pr2->riw_nama;
				$rp7->riw_tgl_lahir =saveDate($pr2->riw_tgl_lahir);
				$rp7->riw_tempat_lahir = $pr2->riw_tempat_lahir;
				$rp7->riw_kelamin = $pr2->riw_kelamin;
				$rp7->riw_pendidikan = $pr2->riw_pendidikan;
				$rp7->riw_pekerjaan = $pr2->riw_pekerjaan;
				$rp7->riw_ket = $pr2->riw_ket;
				$rp7->riw_tgl_ket = $pr2->riw_tgl_ket;
				$rp7->riw_status_tunj = $pr2->riw_status_tunj;
				$rp7->riw_status_sutri = $pr2->riw_status_sutri;
				$rp7->riw_status_perkawinan = $pr2->riw_status_perkawinan;
				if($rp7->save()){
					RiwayatKeluarga2::where('riw_id', $id_pr2)->delete();
				}
			}

			$pk = RiwayatKeahlian2::where('peg_id', $id)->get();
			foreach ($pk as $pk2) {
				$id_pk2 = $pk2->riw_keahlian_id;
				$rp8=new RiwayatKeahlian;
				$id_rp8=RiwayatKeahlian::select('riw_keahlian_id')->max('riw_keahlian_id');
				$find_rpid8 = RiwayatKeahlian::where('riw_keahlian_id', $id_rp8)->first();
				if($find_rpid8){
					$id_rp8 = $id_rp8+1;
					$rp8->riw_keahlian_id = $id_rp8;
				}else{
					$rp8->riw_keahlian_id = $id_rp8;
				}
				$rp8->peg_id =  $temp_id;
				$rp8->keahlian_id = $pk2->keahlian_id;
				$rp8->keahlian_level_id = $pk2->keahlian_level_id;
				$rp8->riw_keahlian_sejak= $pk2->riw_keahlian_sejak;
				if($rp8->save()){
					$pkr = RiwayatKeahlianRel2::where('riw_keahlian_id',$id_pk2)->first();
					$id_pkr = $pkr->riw_keahlian_rel_id;
					$rp9 = new RiwayatKeahlianRel;
					$id_rp9=RiwayatKeahlianRel::select('riw_keahlian_rel_id')->max('riw_keahlian_rel_id');
					$find_rpid9 = RiwayatKeahlianRel::where('riw_keahlian_rel_id', $id_rp9)->first();
					if($find_rpid9){
						$id_rp9 = $id_rp9+1;
						$rp9->riw_keahlian_rel_id = $id_rp9;
					}else{
						$rp9->riw_keahlian_rel_id = $id_rp9;
					}
					$rp9->riw_keahlian_id = $rp8->riw_keahlian_id;
					$rp9->non_id = $pkr['non_id'];
	  				$rp9->riw_pendidikan_id = $pkr['riw_pendidikan_id'];
	  				$rp9->diklat_id = $pkr['diklat_id'];
	  				$rp9->predikat = $pkr['predikat'];

					RiwayatKeahlian2::where('riw_keahlian_id', $id_pk2)->delete();
					if($rp9->save()){
						RiwayatKeahlianRel2::where('riw_keahlian_rel_id', $id_pkr)->delete();
					}

				}
			}

			$ph = RiwayatHukuman2::where('peg_id', $id)->get();
			foreach ($ph as $ph2) {
				$id_ph2 = $ph2->riw_hukum_id;
				$rp9=new RiwayatHukuman;
				$id_rp9=RiwayatHukuman::select('riw_hukum_id')->max('riw_hukum_id');
				$find_rpid9 = RiwayatHukuman::where('riw_hukum_id', $id_rp9)->first();
				if($find_rpid9){
					$id_rp9 = $id_rp9+1;
					$rp9->riw_hukum_id = $id_rp9;
				}else{
					$rp9->riw_hukum_id = $id_rp9;
				}
				$rp9->peg_id =  $temp_id;
				$rp9->mhukum_id = $ph2->mhukum_id;
				$rp9->riw_hukum_tmt= saveDate($ph2->riw_hukum_tmt);
				$rp9->riw_hukum_sd =saveDate($ph2->riw_hukum_sd);
				$rp9->riw_hukum_sk = $ph2->riw_hukum_sk;
				$rp9->riw_hukum_tgl = saveDate($ph2->riw_hukum_tgl);
				$rp9->riw_hukum_ket = $ph2->riw_hukum_ket;
				if($rp9->save()){
					RiwayatHukuman2::where('riw_hukum_id', $id_ph2)->delete();
				}
			}

			$pfile = FilePegawai2::where('peg_id', $id)->get();
			foreach ($pfile as $pfile2) {
				$id_pfile2 = $pfile2->file_id;
				$rp11=new FilePegawai;
				$id_rp11=FilePegawai::select('file_id')->max('file_id');
				$find_rpid11 = FilePegawai::where('file_id', $id_rp11)->first();
				if($find_rpid11){
					$id_rp11 = $id_rp11+1;
					$rp11->file_id = $id_rp11;
				}else{
					$rp11->file_id = $id_rp11;
				}
				$rp11->peg_id =  $temp_id;
				$rp11->file_nama = $pfile2->file_nama;
				$rp11->file_ket = $pfile2->file_ket;
				$rp11->file_tgl = $pfile2->file_tgl;
				if($pfile2->file_lokasi!=null){
					if(File::exists('uploads/file/'.$pfile2->file_lokasi)){
						$file = 'uploads/file/'.$pfile2->file_lokasi;
						$data = File::get($file);
						$rp11->file_lokasi=$pfile2->file_lokasi;
					}
				}
				if($rp11->save()){
					FilePegawai2::where('file_id', $id_pfile2)->delete();
				}
			}


			$log=StatusEditPegawai::where('peg_id', $id)->orderBy('id', 'desc')->first();
			if($log){
				$log->peg_id = $pegawai->peg_id;
				$log->action ="verifikasi_data";
				$log->status_id = 4;
				$log->editor_id = Auth::user()->id;
				$log->save();

			}else{
				$log = new StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action ="verifikasi_data";
				$log->status_id = 4;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Approve Pegawai Baru','NIP : '.$pegawai->peg_nip,$pegawai->peg_id,Auth::user()->username);
		}catch(\Exception $e){
			DB::rollBack();
			return Redirect::to('home')->with('message', 'data gagal diverifikasi '.$e);
		}
		DB::commit();
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
		return Redirect::to('home')->with('message', 'data telah berhasil diverifikasi');
	}

	public function approvePegawai($id){
		$statusedit=StatusEditPegawai::where('peg_id', $id)->orderBy('id', 'desc')->first();
		if($statusedit->status_id == 5 && isDisdik() || $statusedit->status_id == 6 && isDisdik()){
			$statusedit->status_id = 2;
			$statusedit->editor_id = Auth::user()->id;
			$statusedit->action = 'usul_perubahan';
			$statusedit->save();
			return Redirect::to('pegawai/profile/edit/'.$id)->with('message', 'data telah berhasil diverifikasi');
		}
		else{
		$pegawai = Pegawai::where('peg_id', $id)->first();
		$p = Pegawai2::where('peg_id', $id)->first();

		if($p){
			if($pegawai == null){
				$pegawai = Pegawai::where('peg_nip',$p->peg_nip)->first();
			}
		}

		if($p){
			if($pegawai==null){
				return $this->approvePegawaiBaru($id);
			}else{
				DB::beginTransaction();
				try{
				$pegawai->id_goldar=$p['id_goldar'];
				$pegawai->gol_id_awal=$p['gol_id_awal'];
				$pegawai->id_pend_awal=$p['id_pend_awal'];
				$pegawai->unit_kerja_id=$p['unit_kerja_id'];
				$pegawai->kecamatan_id=$p['kecamatan_id'];
				$pegawai->gol_id_akhir=$p['gol_id_akhir'];
				$pegawai->jabatan_id=$p['jabatan_id'];
				$pegawai->id_pend_akhir=$p['id_pend_akhir'];
				$pegawai->id_agama=$p['id_agama'];
				$pegawai->peg_nip=$p['peg_nip'];
				$pegawai->peg_nama=$p['peg_nama'];
				$pegawai->peg_gelar_depan=$p['peg_gelar_depan'];
				$pegawai->peg_gelar_belakang=$p['peg_gelar_belakang'];
				$pegawai->peg_lahir_tempat=$p['peg_lahir_tempat'];
				$pegawai->peg_lahir_tanggal=$p['peg_lahir_tanggal'];
				$pegawai->peg_jenis_kelamin=$p['peg_jenis_kelamin'];
				$pegawai->peg_status_perkawinan=$p['peg_status_perkawinan'];
				$pegawai->peg_karpeg=$p['peg_karpeg'];
				$pegawai->peg_karsutri=$p['peg_karsutri'];
				$pegawai->peg_status_kepegawaian=$p['peg_status_kepegawaian'];
				$pegawai->peg_cpns_tmt=$p['peg_cpns_tmt'];
				$pegawai->peg_pns_tmt=$p['peg_pns_tmt'];
				$pegawai->peg_gol_awal_tmt=$p['peg_gol_awal_tmt'];
				$pegawai->peg_kerja_tahun=$p['peg_kerja_tahun'];
				$pegawai->peg_kerja_bulan=$p['peg_kerja_bulan'];
				$pegawai->peg_jabatan_tmt=$p['peg_jabatan_tmt'];
				$pegawai->peg_no_askes=$p['peg_no_askes'];
				$pegawai->peg_npwp=$p['peg_npwp'];
				$pegawai->peg_bapertarum=$p['peg_bapertarum'];
				$pegawai->peg_rumah_alamat=$p['peg_rumah_alamat'];
				$pegawai->peg_kel_desa=$p['peg_kel_desa'];
				$pegawai->peg_kodepos=$p['peg_kodepos'];
				$pegawai->peg_rumah_alamat=$p['peg_rumah_alamat'];
				$pegawai->peg_telp=$p['peg_telp'];
				$pegawai->peg_telp_hp=$p['peg_telp_hp'];
				$pegawai->peg_tmt_kgb=$p['peg_tmt_kgb'];
				$pegawai->peg_pend_awal_th=$p['peg_pend_awal_th'];
				$pegawai->peg_pend_akhir_th=$p['peg_pend_akhir_th'];
				$pegawai->peg_gol_akhir_tmt=$p['peg_gol_akhir_tmt'];
				$pegawai->tgl_entry=$p['tgl_entry'];
				$pegawai->satuan_kerja_id=$p['satuan_kerja_id'];
				$pegawai->peg_jenis_asn=$p['peg_jenis_asn'];
				$pegawai->peg_status_calon=$p['peg_status_calon'];
				$pegawai->peg_jenis_pns=$p['peg_jenis_pns'];

				$peg_pensiun = PegawaiPensiun::where('peg_id',$p['peg_id'])->where('pensiun_id',6)->first();
				if($peg_pensiun && (!$pegawai->peg_status)){
					$pegawai->peg_status = true;
					$pegawai->peg_ketstatus= null;
				}else{
					$pegawai->peg_status=$p['peg_status'];
					$pegawai->peg_ketstatus=$p['peg_ketstatus'];
				}

				$pegawai->peg_nip_lama=$p['peg_nip_lama'];
				$pegawai->peg_ktp=$p['peg_ktp'];
				$pegawai->peg_instansi_dpk=$p['peg_instansi_dpk'];
				$pegawai->peg_ak=$p['peg_ak'];
				$pegawai->peg_punya_kpe=$p['peg_punya_kpe'];
				$pegawai->peg_punya_kpe_tgl=$p['peg_punya_kpe_tgl'];
				$pegawai->peg_email=$p['peg_email'];
				$pegawai->peg_status_asn =  $p['peg_status_asn'];
				$pegawai->peg_status_gaji = $p['peg_status_gaji'];
				$pegawai->id_status_kepegawaian = $p['id_status_kepegawaian'];
				$pegawai->jenis_guru = $p['jenis_guru'];
				$pegawai->status_tkd=$p['status_tkd'];
				$pegawai->kedudukan_pegawai=$p['kedudukan_pegawai'];

				if(file_exists('/uploads/'.$p['peg_foto'])){
					$file = '/uploads/'.$p['peg_foto'];
					$data = file_get_contents($file);
				}
				$pegawai->peg_foto=$p['peg_foto'];
				if($pegawai->save()){
					Pegawai2::where('peg_id', $id)->delete();
					$pErk = PegawaiErk::where('peg_id', $id)->first();
					if ($pegawai->jabatan_id != $pErk->jabatan_id){
						/* ada perubahan jabatan */
						$lsp = ListSpgPegawai::where('peg_id', $id)->where('status_record', false)->first();
						if (!$lsp){
							$lsp = new ListSpgPegawai;
						}
						$lsp->peg_id = $pegawai['peg_id'];
						$lsp->status_record = 0;
						$lsp->added_by = Auth::user()->id;
						$lsp->save();
					} else {
						/* tidak ada perubahan jabatan */
						$pErk->unit_kerja_id = $pegawai->unit_kerja_id;
						$pErk->jabatan_id = $pegawai->jabatan_id;
						$pErk->satuan_kerja_id = $pegawai->satuan_kerja_id;
						$pErk->peg_jabatan_tmt = $pegawai->peg_jabatan_tmt;
					}

					$temp_peg_pensiun = PegawaiPensiun::where('peg_id',$p['peg_id'])->whereIn('pensiun_id',[1,3])->first();
					if (($pegawai->peg_status != $pErk->peg_status) && ($temp_peg_pensiun)){
						/* ada perubahan status menjadi pensiun BUP / APS */
						$pErk->peg_status = true;
						$pErk->peg_ketstatus= null;
						/* yang perlu di-save: added_at(pk), peg_id, pensiun_id, status_record, added_by. optional: pushed_by, pushed_at						*/
						$ls_pensiun = new ListSpgPensiun;
						$ls_pensiun->peg_id = $pegawai['peg_id'];
						$ls_pensiun->pensiun_id = $temp_peg_pensiun->pensiun_id;
						$ls_pensiun->status_record = 0;
						$ls_pensiun->added_by = Auth::user()->id;
						$ls_pensiun->save();
					}	else {
						/* tidak ada perubahan status */
						$pErk->peg_status = $pegawai->peg_status;
						$pErk->peg_ketstatus = $pegawai->peg_ketstatus;
					}

					$pErk->id_goldar = $pegawai->id_goldar;
					$pErk->gol_id_awal = $pegawai->gol_id_awal;
					$pErk->id_pend_awal = $pegawai->id_pend_awal;
					$pErk->kecamatan_id = $pegawai->kecamatan_id;
					$pErk->gol_id_akhir = $pegawai->gol_id_akhir;
					$pErk->id_pend_akhir = $pegawai->id_pend_akhir;
					$pErk->id_agama = $pegawai->id_agama;
					$pErk->peg_nip = $pegawai->peg_nip;
					$pErk->peg_nama = $pegawai->peg_nama;
					$pErk->peg_gelar_depan = $pegawai->peg_gelar_depan;
					$pErk->peg_gelar_belakang = $pegawai->peg_gelar_belakang;
					$pErk->peg_lahir_tempat = $pegawai->peg_lahir_tempat;
					$pErk->peg_lahir_tanggal = $pegawai->peg_lahir_tanggal;
					$pErk->peg_jenis_kelamin = $pegawai->peg_jenis_kelamin;
					$pErk->peg_status_perkawinan = $pegawai->peg_status_perkawinan;
					$pErk->peg_karpeg = $pegawai->peg_karpeg;
					$pErk->peg_karsutri = $pegawai->peg_karsutri;
					$pErk->peg_status_kepegawaian = $pegawai->peg_status_kepegawaian;
					$pErk->peg_cpns_tmt = $pegawai->peg_cpns_tmt;
					$pErk->peg_pns_tmt = $pegawai->peg_pns_tmt;
					$pErk->peg_gol_awal_tmt = $pegawai->peg_gol_awal_tmt;
					$pErk->peg_kerja_tahun = $pegawai->peg_kerja_tahun;
					$pErk->peg_kerja_bulan = $pegawai->peg_kerja_bulan;
					$pErk->peg_no_askes = $pegawai->peg_no_askes;
					$pErk->peg_npwp = $pegawai->peg_npwp;
					$pErk->peg_bapertarum = $pegawai->peg_bapertarum;
					$pErk->peg_rumah_alamat = $pegawai->peg_rumah_alamat;
					$pErk->peg_kel_desa = $pegawai->peg_kel_desa;
					$pErk->peg_kodepos = $pegawai->peg_kodepos;
					$pErk->peg_rumah_alamat = $pegawai->peg_rumah_alamat;
					$pErk->peg_telp = $pegawai->peg_telp;
					$pErk->peg_telp_hp = $pegawai->peg_telp_hp;
					$pErk->peg_tmt_kgb =$pegawai->peg_tmt_kgb;
					$pErk->peg_pend_awal_th = $pegawai->peg_pend_awal_th;
					$pErk->peg_pend_akhir_th = $pegawai->peg_pend_akhir_th;
					$pErk->peg_gol_akhir_tmt = $pegawai->peg_gol_akhir_tmt;
					$pErk->tgl_entry = $pegawai->tgl_entry;
					$pErk->peg_jenis_asn = $pegawai->peg_jenis_asn;
					$pErk->peg_status_calon = $pegawai->peg_status_calon;
					$pErk->peg_jenis_pns = $pegawai->peg_jenis_pns;
					$pErk->peg_nip_lama = $pegawai->peg_nip_lama;
					$pErk->peg_ktp = $pegawai->peg_ktp;
					$pErk->peg_instansi_dpk = $pegawai->peg_instansi_dpk;
					$pErk->peg_ak = $pegawai->peg_ak;
					$pErk->peg_punya_kpe = $pegawai->peg_punya_kpe;
					$pErk->peg_punya_kpe_tgl = $pegawai->peg_punya_kpe_tgl;
					$pErk->peg_email = $pegawai->peg_email;
					$pErk->peg_status_asn = $pegawai->peg_status_asn;
					$pErk->peg_status_gaji = $pegawai->peg_status_gaji;
					$pErk->id_status_kepegawaian = $pegawai->id_status_kepegawaian;
					$pErk->jenis_guru = $pegawai->jenis_guru;
					$pErk->status_tkd = $pegawai->status_tkd;
					$pErk->kedudukan_pegawai = $pegawai->kedudukan_pegawai;
					$pErk->peg_foto = $pegawai->peg_foto;
					$pErk->save();
				}

				$rp = RiwayatPenghargaan::where('peg_id', $id)->delete();
				$pg = RiwayatPenghargaan2::where('peg_id', $id)->get();
				foreach ($pg as $pg2) {
					$id_pg2 = $pg2->riw_penghargaan_id;
					$find_id = RiwayatPenghargaan::where('riw_penghargaan_id',  $pg2->riw_penghargaan_id)->first();
					$rp = new RiwayatPenghargaan;
					if(!$find_id){
						$rp->riw_penghargaan_id = $pg2->riw_penghargaan_id;
					}else{
						$id_rp=RiwayatPenghargaan::select('riw_penghargaan_id')->max('riw_penghargaan_id');
						$find_rpid = RiwayatPenghargaan::where('riw_penghargaan_id', $id_rp)->first();
						if($find_rpid){
							$id_rp= $id_rp+1;
							$rp->riw_penghargaan_id = $id_rp;
						}else{
							$rp->riw_penghargaan_id = $id_rp;
						}
					}
					$rp->peg_id = $pg2->peg_id;
					$rp->penghargaan_id = $pg2->penghargaan_id;
					$rp->riw_penghargaan_instansi= $pg2->riw_penghargaan_instansi;
					$rp->riw_penghargaan_sk = $pg2->riw_penghargaan_sk;
					$rp->riw_penghargaan_tglsk = saveDate($pg2->riw_penghargaan_tglsk);
					$rp->riw_penghargaan_jabatan = $pg2->riw_penghargaan_jabatan;
					$rp->riw_penghargaan_lokasi = $pg2->riw_penghargaan_lokasi;
					if($rp->save()){
						RiwayatPenghargaan2::where('riw_penghargaan_id',$id_pg2)->delete();
					}
				}

				$rp2 = RiwayatPendidikan::where('peg_id', $id)->delete();
				$pf = RiwayatPendidikan2::where('peg_id', $id)->get();
				foreach ($pf as $pf2) {
					$id_pf2 = $pf2->riw_pendidikan_id;
					$find_id2 = RiwayatPendidikan::where('riw_pendidikan_id',  $pf2->riw_pendidikan_id)->first();
					$rp2=new RiwayatPendidikan;

					if(!$find_id2){
						$rp2->riw_pendidikan_id = $pf2->riw_pendidikan_id;
					}else{
						$id_rp2=RiwayatPendidikan::select('riw_pendidikan_id')->max('riw_pendidikan_id');
						$find_rpid2 = RiwayatPendidikan::where('riw_pendidikan_id', $id_rp2)->first();
						if($find_rpid2){
							$id_rp2= $id_rp2+1;
							$rp2->riw_pendidikan_id = $id_rp2;
						}else{
							$rp2->riw_pendidikan_id = $id_rp2;
						}
					}
					$rp2->peg_id = $pf2->peg_id;
					$rp2->id_pend = $pf2->id_pend;
					$rp2->tingpend_id= $pf2->tingpend_id;
					$rp2->riw_pendidikan_fakultas= $pf2->riw_pendidikan_fakultas;
					$rp2->jurusan_id= $pf2->jurusan_id;
					$rp2->univ_id = $pf2->univ_id;
					$rp2->riw_pendidikan_sttb_ijazah= $pf2->riw_pendidikan_sttb_ijazah;
					$rp2->riw_pendidikan_tgl = saveDate($pf2->riw_pendidikan_tgl);
					$rp2->riw_pendidikan_pejabat = $pf2->riw_pendidikan_pejabat;
					$rp2->riw_pendidikan_nm = $pf2->riw_pendidikan_nm;
					$rp2->riw_pendidikan_lokasi = $pf2->riw_pendidikan_lokasi;
					$rp2->fakultas_id = $pf2->fakultas_id;
					if($rp2->save()){
						RiwayatPendidikan2::where('riw_pendidikan_id', $id_pf2)->delete();
					}
				}

				$rp3 = RiwayatNonFormal::where('peg_id', $id)->delete();
				$pn = RiwayatNonFormal2::where('peg_id', $id)->get();
				foreach ($pn as $pn2) {
					$id_pn2 = $pn2->non_id;
					$find_id3 = RiwayatNonFormal::where('non_id', $id_pn2)->first();
					$rp3=new  RiwayatNonFormal;
					if(!$find_id3){
						$rp3->non_id = $pn2->non_id;
					}else{
						$id_rp3=RiwayatNonFormal::select('non_id')->max('non_id');
						$find_rpid3 = RiwayatNonFormal::where('non_id', $id_rp3)->first();
						if($find_rpid3){
							$id_rp3 = $id_rp3+1;
							$rp3->non_id = $id_rp3;
						}else{
							$rp3->non_id = $id_rp3;
						}
					}
					$rp3->peg_id = $pn2->peg_id;
					$rp3->non_nama= $pn2->non_nama;
					$rp3->non_tgl_mulai = saveDate($pn2->non_tgl_mulai);
					$rp3->non_tgl_selesai = saveDate($pn2->non_tgl_selesai);
					$rp3->non_sttp = $pn2->non_sttp;
					$rp3->non_sttp_tanggal = saveDate($pn2->non_sttp_tanggal);
					$rp3->non_sttp_pejabat = $pn2->non_sttp_pejabat;
					$rp3->non_penyelenggara = $pn2->non_penyelenggara;
					$rp3->non_tempat = $pn2->non_tempat;
					if($rp3->save()){
						RiwayatNonFormal2::where('non_id', $id_pn2)->delete();
					}
				}

				$rp4 = RiwayatPangkat::where('peg_id',$id)->delete();
				$pp = RiwayatPangkat2::where('peg_id', $id)->get();
				foreach ($pp as $pp2) {
					$id_pp2 = $pp2->riw_pangkat_id;
					$find_id4 = RiwayatPangkat::where('riw_pangkat_id', $id_pp2)->first();
					$rp4=new RiwayatPangkat;
					if(!$find_id4){
						$rp4->riw_pangkat_id = $pp2->riw_pangkat_id;
					}else{
						$id_rp4=RiwayatPangkat::select('riw_pangkat_id')->max('riw_pangkat_id');
						$find_rpid4 = RiwayatPangkat::where('riw_pangkat_id', $id_rp4)->first();
						if($find_rpid4){
							$id_rp4 = $id_rp4+1;
							$rp4->riw_pangkat_id = $id_rp4;
						}else{
							$rp4->riw_pangkat_id = $id_rp4;
						}
					}
					$rp4->peg_id = $pp2->peg_id;
					$rp4->riw_pangkat_thn= $pp2->riw_pangkat_thn;
					$rp4->riw_pangkat_bln = $pp2->riw_pangkat_bln;
					$rp4->riw_pangkat_gapok = $pp2->riw_pangkat_gapok;
					$rp4->riw_pangkat_sk = $pp2->riw_pangkat_sk;
					$rp4->riw_pangkat_sktgl =  saveDate($pp2->riw_pangkat_sktgl);
					$rp4->riw_pangkat_tmt =  saveDate($pp2->riw_pangkat_tmt);
					$rp4->riw_pangkat_pejabat = $pp2->riw_pangkat_pejabat;
					$rp4->riw_pangkat_unit_kerja = $pp2->riw_pangkat_unit_kerja;
					$rp4->gol_id = $pp2->gol_id;
					if($rp4->save()){
						RiwayatPangkat2::where('riw_pangkat_id', $id_pp2)->delete();
					}
				}

				/* KGB::where('peg_id',$id)->where('status_data', 'manual')->delete();
				$kgb = Kgb_skpd::where('peg_id', $id)->where('status_data', 'manual')->get();
				foreach ($kgb as $pp2) {
					$id_pp2 = $pp2->kgb_id;
					$find_id4 = KGB::where('kgb_id', $id_pp2)->first();
					$rp4=new KGB;
					if(!$find_id4){
						$rp4->kgb_id = $pp2->kgb_id;
					}else{
						$id_rp4=KGB::select('kgb_id')->max('kgb_id');
						$find_rpid4 = KGB::where('kgb_id', $id_rp4)->first();
						if($find_rpid4){
							$id_rp4 = $id_rp4+1;
							$rp4->kgb_id = $id_rp4;
						}else{
							$rp4->kgb_id = $id_rp4;
						}
					}

					$rp4->peg_id =  $pp2->peg_id;
					$rp4->kgb_kerja_tahun= $pp2->kgb_kerja_tahun;
					$rp4->kgb_kerja_bulan = $pp2->kgb_kerja_bulan;
					$rp4->kgb_gapok = $pp2->kgb_gapok;
					$rp4->kgb_nosk = $pp2->kgb_nosk;
					$rp4->kgb_tglsk =  saveDate($pp2->kgb_tglsk);
					$rp4->kgb_tmt =  saveDate($pp2->kgb_tmt);
					$rp4->ttd_pejabat = $pp2->ttd_pejabat;
					$rp4->unit_kerja = $pp2->unit_kerja;
					$rp4->gol_id = $pp2->gol_id;
					$rp4->kgb_thn = date('Y');
					$rp4->kgb_bln = date('m');
					$rp4->kgb_status = 50;
					$rp4->user_id = Auth::user()->id;
					$rp4->ttd_digital = 50;
					$rp4->status_data = 'manual';
					if($rp4->save()){
						Kgb_skpd::where('kgb_id', $pp2->kgb_id)->delete();
					}
				}

				Pmk::where('peg_id',$id)->delete();
				$pmk = Pmk_skpd::where('peg_id', $id)->get();
				foreach ($pmk as $pp2) {
					$rp=new Pmk;
				
					$rp->peg_id = $pp2->peg_id;
					$rp->created_by = $pp2->created_by;
					$rp->updated_by = $pp2->updated_by;
				// }
				
					$rp->pmk_tahun = $pp2->pmk_tahun;
					$rp->pmk_bulan = $pp2->pmk_bulan;
					$rp->masa_kerja_lama_thn = $pp2->masa_kerja_lama_thn;
					$rp->masa_kerja_lama_bln = $pp2->masa_kerja_lama_bln;
					$rp->pmk_gapok_lama = $pp2->pmk_gapok_lama;
			
					$rp->masa_kerja_baru_thn = $pp2->masa_kerja_baru_thn;
					$rp->masa_kerja_baru_bln = $pp2->masa_kerja_baru_bln;
					$rp->pmk_gapok_baru = $pp2->pmk_gapok_baru;
			
					$rp->pejabat_id = $pp2->pejabat_id;
					$rp->pmk_jabatan = $pp2->pmk_jabatan;
					
					$rp->pmk_nosk = $pp2->pmk_nosk;
					$rp->pmk_sktgl =  $pp2->pmk_sktgl;
					$rp->pmk_tmt_lama =  $pp2->pmk_tmt_lama;
					$rp->pmk_tmt_baru =  $pp2->pmk_tmt_baru;
					$rp->pmk_ttd_pejabat = $pp2->pmk_ttd_pejabat;
					$rp->pmk_unit_kerja = $pp2->pmk_unit_kerja;
					$rp->gol_id = $pp2->gol_id;
					if($rp->save()){
						Pmk_skpd::where('peg_id', $pp2->peg_id)->delete();
					}
				} */

				$rp5 =  RiwayatJabatan::where('peg_id', $id)->delete();
				$pj = RiwayatJabatan2::where('peg_id', $id)->get();
				foreach ($pj as $pj2) {
					$id_pj2 = $pj2->riw_jabatan_id;
					$find_id5 = RiwayatJabatan::where('riw_jabatan_id', $id_pj2)->first();
					$rp5=new RiwayatJabatan;
					if(!$find_id5){
						$rp5->riw_jabatan_id = $pj2->riw_jabatan_id;
					}else{
						$id_rp5=RiwayatJabatan::select('riw_jabatan_id')->max('riw_jabatan_id');
						$find_rpid5 = RiwayatJabatan::where('riw_jabatan_id', $id_rp5)->first();
						if($find_rpid5){
							$id_rp5 = $id_rp5+1;
							$rp5->riw_jabatan_id = $id_rp5;
						}else{
							$rp5->riw_jabatan_id = $id_rp5;
						}
					}
					$rp5->peg_id = $pj2->peg_id;
					$rp5->riw_jabatan_nm= $pj2->riw_jabatan_nm;
					$rp5->riw_jabatan_no = $pj2->riw_jabatan_no;
					$rp5->riw_jabatan_pejabat = $pj2->riw_jabatan_pejabat;
					$rp5->riw_jabatan_tgl = saveDate($pj2->riw_jabatan_tgl);
					$rp5->riw_jabatan_tmt =  saveDate($pj2->riw_jabatan_tmt);
					$rp5->riw_jabatan_unit = $pj2->riw_jabatan_unit;
					$rp5->gol_id = $pj2->gol_id;
					if($rp5->save()){
						RiwayatJabatan2::where('riw_jabatan_id', $id_pj2)->delete();
					}
				}

				$rp6 = RiwayatDiklat::where('peg_id', $id)->delete();
				$pd = RiwayatDiklat2::where('peg_id',$id)->get();
				foreach ($pd as $pd2) {
					$id_pd2 = $pd2->diklat_id;
					$find_id6 = RiwayatDiklat::where('diklat_id', $id_pd2)->first();
					$rp6=new RiwayatDiklat;
					if(!$find_id6){
						$rp6->diklat_id = $pd2->diklat_id;
					}else{
						$id_rp6=RiwayatDiklat::select('diklat_id')->max('diklat_id');
						$find_rpid6 = RiwayatDiklat::where('diklat_id', $id_rp6)->first();
						if($find_rpid6){
							$id_rp6 = $id_rp6+1;
							$rp6->diklat_id = $id_rp6;
						}else{
							$rp6->diklat_id = $id_rp6;
						}
					}
					$rp6->peg_id = $pd2->peg_id;
					$rp6->kategori_id= $pd2->kategori_id;
					$rp6->diklat_fungsional_id= $pd2->diklat_fungsional_id;
					$rp6->diklat_teknis_id= $pd2->diklat_teknis_id;
					$rp6->diklat_jenis = $pd2->diklat_jenis;
					$rp6->diklat_mulai = saveDate($pd2->diklat_mulai);
					$rp6->diklat_selesai = saveDate($pd2->diklat_selesai);
					$rp6->diklat_penyelenggara = $pd2->diklat_penyelenggara;
					$rp6->diklat_tempat = $pd2->diklat_tempat;
					$rp6->diklat_jumlah_jam = $pd2->diklat_jumlah_jam;
					$rp6->diklat_sttp_no = $pd2->diklat_sttp_no;
					$rp6->diklat_sttp_tgl =  saveDate($pd2->diklat_sttp_tgl);
					$rp6->diklat_sttp_pej = $pd2->diklat_sttp_pej;
					$rp6->diklat_usul_no = $pd2->diklat_usul_no;
					$rp6->diklat_usul_tgl = $pd2->diklat_usul_tgl;
					$rp6->pejabat_id = $pd2->pejabat_id;
					$rp6->diklat_nama = $pd2->diklat_nama;
					$rp6->diklat_tipe = $pd2->diklat_tipe;
					if($rp6->save()){
						RiwayatDiklat2::where('diklat_id', $id_pd2)->delete();
					}
				}

				$rp7 = RiwayatKeluarga::where('peg_id', $id)->delete();
				$pr = RiwayatKeluarga2::where('peg_id', $id)->get();
				foreach ($pr as $pr2) {
					$id_pr2 = $pr2->riw_id;
					$find_id7 = RiwayatKeluarga::where('riw_id', $id_pr2)->first();
					$rp7=new RiwayatKeluarga;
					if(!$find_id7){
						$rp7->riw_id = $pr2->riw_id;
					}else{
						$id_rp7=RiwayatKeluarga::select('riw_id')->max('riw_id');
						$find_rpid7 = RiwayatKeluarga::where('riw_id', $id_rp7)->first();
						if($find_rpid7){
							$id_rp7 = $id_rp7+1;
							$rp7->riw_id = $id_rp7;
						}else{
							$rp7->riw_id = $id_rp7;
						}
					}
					$rp7->peg_id = $id;
					$rp7->riw_status = $pr2->riw_status;
					$rp7->nik= $pr2->nik;
					$rp7->riw_nama= $pr2->riw_nama;
					$rp7->riw_tgl_lahir =saveDate($pr2->riw_tgl_lahir);
					$rp7->riw_tempat_lahir = $pr2->riw_tempat_lahir;
					$rp7->riw_kelamin = $pr2->riw_kelamin;
					$rp7->riw_pendidikan = $pr2->riw_pendidikan;
					$rp7->riw_pekerjaan = $pr2->riw_pekerjaan;
					$rp7->riw_ket = $pr2->riw_ket;
					$rp7->riw_tgl_ket = $pr2->riw_tgl_ket;
					$rp7->riw_status_tunj = $pr2->riw_status_tunj;
					$rp7->riw_status_sutri = $pr2->riw_status_sutri;
					$rp7->riw_status_perkawinan = $pr2->riw_status_perkawinan;

					if($rp7->save()){
						RiwayatKeluarga2::where('riw_id', $id_pr2)->delete();
					}
				}

				$rp8 = RiwayatKeahlian::where('peg_id', $id)->delete();
				$pk = RiwayatKeahlian2::where('peg_id', $id)->get();
				foreach ($pk as $pk2) {
					$id_pk2 = $pk2->riw_keahlian_id;
					$find_id8 = RiwayatKeahlian::where('riw_keahlian_id', $id_pk2)->first();
					$rp8=new RiwayatKeahlian;

					if(!$find_id8){
						$rp8->riw_keahlian_id = $pk2->riw_keahlian_id;
					}else{
						$id_rp8=RiwayatKeahlian::select('riw_keahlian_id')->max('riw_keahlian_id');
						$find_rpid8 = RiwayatKeahlian::where('riw_keahlian_id', $id_rp8)->first();
						if($find_rpid8){
							$id_rp8 = $id_rp8+1;
							$rp8->riw_keahlian_id = $id_rp8;
						}else{
							$rp8->riw_keahlian_id = $id_rp8;
						}
					}

					$rp8->peg_id =  $id;
					$rp8->keahlian_id = $pk2->keahlian_id;
					$rp8->keahlian_level_id = $pk2->keahlian_level_id;
					$rp8->riw_keahlian_sejak= $pk2->riw_keahlian_sejak;
					if($rp8->save()){
						$rp9 = RiwayatKeahlianRel::where('riw_keahlian_id', $id_pk2)->delete();
						$find_id9 = RiwayatKeahlianRel2::where('riw_keahlian_id',$id_pk2)->first();
						$id_pkr = $find_id9['riw_keahlian_rel_id'];
						$rp9 = new RiwayatKeahlianRel;

						if(!$find_id9){
							$rp9->riw_keahlian_rel_id = $find_id9['riw_keahlian_rel_id'];
						}else{
							$id_rp9=RiwayatKeahlianRel::select('riw_keahlian_rel_id')->max('riw_keahlian_rel_id');
							$find_rpid9 = RiwayatKeahlianRel::where('riw_keahlian_rel_id', $id_rp9)->first();
							if($find_rpid9){
								$id_rp9 = $id_rp9+1;
								$rp9->riw_keahlian_rel_id = $id_rp9;
							}else{
								$rp9->riw_keahlian_rel_id = $id_rp9;
							}
						}

						$rp9->riw_keahlian_id = $rp8->riw_keahlian_id;
						$rp9->non_id = $find_id9['non_id'];
		  				$rp9->riw_pendidikan_id = $find_id9['riw_pendidikan_id'];
		  				$rp9->diklat_id = $find_id9['diklat_id'];
		  				$rp9->predikat = $find_id9['predikat'];

						RiwayatKeahlian2::where('riw_keahlian_id', $id_pk2)->delete();
						if($rp9->save()){
							RiwayatKeahlianRel2::where('riw_keahlian_rel_id', $id_pkr)->delete();
						}
					}
				}

				$rp10 = RiwayatHukuman::where('peg_id', $id)->delete();
				$ph = RiwayatHukuman2::where('peg_id', $id)->get();
				foreach ($ph as $ph2) {
					$id_ph2 = $ph2->riw_hukum_id;
					$find_id10 = RiwayatHukuman::where('riw_hukum_id', $id_ph2)->first();
					$rp10=new RiwayatHukuman;
					if(!$find_id10){
						$rp10->riw_hukum_id = $ph2->riw_hukum_id;
					}else{
						$id_rp10=RiwayatHukuman::select('riw_hukum_id')->max('riw_hukum_id');
						$find_rpid10 = RiwayatHukuman::where('riw_hukum_id', $id_rp10)->first();
						if($find_rpid10){
							$id_rp10 = $id_rp10+1;
							$rp10->riw_hukum_id = $id_rp10;
						}else{
							$rp10->riw_hukum_id = $id_rp10;
						}
					}
					$rp10->peg_id = $id;
					$rp10->mhukum_id = $ph2->mhukum_id;
					$rp10->riw_hukum_tmt= saveDate($ph2->riw_hukum_tmt);
					$rp10->riw_hukum_sd =saveDate($ph2->riw_hukum_sd);
					$rp10->riw_hukum_sk = $ph2->riw_hukum_sk;
					$rp10->riw_hukum_tgl = saveDate($ph2->riw_hukum_tgl);
					$rp10->riw_hukum_ket = $ph2->riw_hukum_ket;
					if($rp10->save()){
						RiwayatHukuman2::where('riw_hukum_id', $id_ph2)->delete();
					}
				}

				$rp11 = FilePegawai::where('peg_id', $id)->delete();
				$pfile = FilePegawai2::where('peg_id', $id)->get();
				foreach ($pfile as $pfile2) {
					$id_pfile2 = $pfile2->file_id;
					$find_id11 = FilePegawai::where('file_id', $id_pfile2)->first();
					$rp11=new FilePegawai;
					if(!$find_id11){
						$rp11->file_id = $pfile2->file_id;
					}else{
						$id_rp11=FilePegawai::select('file_id')->max('file_id');
						$find_rpid11 = FilePegawai::where('file_id', $id_rp11)->first();
						if($find_rpid11){
							$id_rp11 = $id_rp11+1;
							$rp11->file_id = $id_rp11;
						}else{
							$rp11->file_id = $id_rp11;
						}
					}
					$rp11->peg_id =  $id;
					$rp11->file_nama = $pfile2->file_nama;
					$rp11->file_ket = $pfile2->file_ket;
					$rp11->file_tgl = $pfile2->file_tgl;
					if($pfile2->file_lokasi!=null){
						if(File::exists('uploads/file/'.$pfile2->file_lokasi)){
							$file = 'uploads/file/'.$pfile2->file_lokasi;
							$data = File::get($file);
							$rp11->file_lokasi=$pfile2->file_lokasi;
						}
					}
					if($rp11->save()){
						FilePegawai2::where('file_id', $id_pfile2)->delete();
					}
				}

					$log=StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
					if($log){
						$log->action ="verifikasi_data";
						$log->status_id = 4;
						$log->editor_id = Auth::user()->id;
						$log->save();
					}else{
						$log = new StatusEditPegawai;
						$log->peg_id = $pegawai->peg_id;
						$log->peg_nip = $pegawai->peg_nip;
						$log->action ="verifikasi_data";
						$log->status_id = 4;
						$log->editor_id = Auth::user()->id;
						$log->save();
					}
					logAction('Approve Pegawai','NIP : '.$pegawai->peg_nip,$pegawai->peg_id,Auth::user()->username);
				}catch(\Exception $e){
					DB::rollBack();
					return Redirect::to('home')->with('message', 'data gagal diverifikasi'.$e);
				}
				DB::commit();
				$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
				return Redirect::to('home')->with('message', 'data telah berhasil diverifikasi');
			}
		}else{
			return Redirect::to('home')->with('message', 'Data Sudah Di Approve');
		}
		}
	}

	public function approveAllPegawai($id){
		$str = parse_str($id);

		foreach ($peg_id as $key => $value) {
			$this->approvePegawai($value);
		}
		return Redirect::to('home')->with('message', 'data telah berhasil diverifikasi');

	}


	public function pensiunPegawai($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai2::find($id);
		$pegawai->peg_status = 'TRUE';
		if($pegawai->save()){
			$pSimpeg = Pegawai::find($id);
			$pSimpeg->peg_status = true;
			$pSimpeg->peg_ketstatus = null;
			$pSimpeg->save();

			$pErk = PegawaiErk::find($id);
			$pErk->peg_status = true;
			$pErk->peg_ketstatus = null;
			$pErk->save();

			$pegawaiPensiun = PegawaiPensiun::where('peg_id', $id)->delete();

			ListSpgPensiun::where('peg_id', $id)->where('status_record', false)->delete();

			$log=StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$l =new StatusEditPegawaiLog;
				$l->sed_id = $log->id;
				$l->action ="undo_pensiun_pegawai";
				$l->editor_id = Auth::user()->id;
				$l->save();
			}else{
				$log = New StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action ="undo_pensiun_pegawai";
				$log->status_id =4;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Update Status Pegawai','NIP : '.$pegawai->peg_nip,$pegawai->peg_id,Auth::user()->username);

			return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id)->with('message', 'Status Pegawai Menjadi Aktif');
		}
	}

	public function updateStatusPegawai($stat,$id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai2::find($id);
		$pegawai->peg_status = 'TRUE';
		$pegawai->peg_ketstatus = null;
		$status = '';
		if($stat == 'meninggal'){
			$status = 'undo_pegawai_meninggal';
		}else{
			$status = 'undo_pindah_pegawai';
		}
		if($pegawai->save()){
			$log=StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$l =new StatusEditPegawaiLog;
				$l->sed_id = $log->id;
				$l->action =$status;
				$l->editor_id = Auth::user()->id;
				$l->save();
			}else{
				$log = New StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action =$status;
				$log->status_id =4;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Update Status Pegawai','NIP : '.$pegawai->peg_nip,$pegawai->peg_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id)->with('message', 'Status Pegawai Menjadi Aktif');
		}
	}

	public function deletePegawai($id){
		RiwayatPenghargaan::where('peg_id', $id)->delete();
		RiwayatPenghargaan2::where('peg_id', $id)->delete();
		RiwayatPendidikan::where('peg_id', $id)->delete();
		RiwayatPendidikan2::where('peg_id', $id)->delete();
		RiwayatNonFormal::where('peg_id', $id)->delete();
		RiwayatNonFormal2::where('peg_id', $id)->delete();
		RiwayatPangkat::where('peg_id', $id)->delete();
		RiwayatPangkat2::where('peg_id', $id)->delete();
		KGB::where('peg_id', $id)->where('status_data', 'manual')->delete();
		Kgb_skpd::where('peg_id', $id)->where('status_data', 'manual')->delete();
		Pmk::where('peg_id', $id)->delete();
		Pmk_skpd::where('peg_id', $id)->delete();
		RiwayatJabatan::where('peg_id', $id)->delete();
		RiwayatJabatan2::where('peg_id', $id)->delete();
		RiwayatDiklat::where('peg_id', $id)->delete();
		RiwayatDiklat2::where('peg_id', $id)->delete();
		RiwayatKeluarga::where('peg_id', $id)->delete();
		RiwayatKeluarga2::where('peg_id', $id)->delete();
		$rk = RiwayatKeahlian::where('peg_id', $id)->first();
		if($rk){
			RiwayatKeahlianRel::where('riwayat_keahlian_id', $rk->riwayat_keahlian_id)->delete();
			$rk->delete();
		}
		$rk2=RiwayatKeahlian2::where('peg_id', $id)->first();
		if($rk2){
			RiwayatKeahlianRel2::where('riwayat_keahlian_id', $rk2->riwayat_keahlian_id)->delete();
			$rk2->delete();
		}
		LogEditPegawai::where('peg_id', $id)->delete();
		RiwayatHukuman::where('peg_id', $id)->delete();
		RiwayatHukuman2::where('peg_id', $id)->delete();
		$status = StatusEditPegawai::where('peg_id', $id)->first();
		if($status){
			StatusEditPegawaiLog::where('sed_id', $status->id)->delete();
			$status->delete();
		}
		Pegawai::where('peg_id', $id)->delete();
		Pegawai2::where('peg_id', $id)->delete();

		logAction('Hapus Pegawai','',$id,Auth::user()->username);

		return Redirect::to('home')->with('message', 'Pegawai Telah Dihapus');
	}

	public function evjabIndexClear(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		ini_set('max_execution_time', 10000);
    	ini_set('memory_limit','2048M');
		$id = Auth::user()->satuan_kerja_id;
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}
		$pegawai = PegawaiEvjab::where('satuan_kerja_id', $id)->where('is_deleted', false)->whereNotNull('jabatan_id_evjab')->orderBy('unit_kerja_id', 'peg_nama')->orderBy('gol_id_akhir', 'desc')->get();
		return view('pages.jfubaru.list_pegawai', compact('id', 'satker','pegawai'));
	}

	public function evjabListUnitClear($id){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$pegawai = PegawaiEvjab::where('satuan_kerja_id', $id)->whereNotNull('jabatan_id_evjab')->orderBy('unit_kerja_id', 'peg_nama')->orderBy('gol_id_akhir', 'desc')->get();
		return view('pages.jfubaru.list_pegawai', compact('id', 'satker','pegawai'));
	}

	public function evjabJfuTerisi(){
		if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
			return Redirect::back();
		}
		$jfuterisi = JfuBaruTerisi::orderBy('jfu_kelas', 'desc')->orderBy('jfu_nama')->get();
		return view('pages.jfubaru.jfu_terisi', compact('jfuterisi'));
	}

	public function evjabDownloadPegawaiSkpd($id,$status){
		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', 10000);

		$all = false;
		$data = array();
		$header = array();
		if($status == 'true'){
			$query = PegawaiEvjabLengkap::where('satuan_kerja_id', $id)->whereNotNull('jabatan_id_evjab')->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama');
		}else{
			$query = PegawaiEvjabLengkap::where('satuan_kerja_id', $id)->orderBy('unit_kerja_id')->orderBy('jfu_nama')->orderBy('peg_nama');
		}		
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$query = $query->get();
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
			$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
			$jabatan2019 = Jabatan::where('jabatan_id', $a->jabatan_id_evjab)->first();
			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
			}
			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
			if($jfu){
				$nama_jabatan = $jfu['jfu_nama'];
			}else{
				$nama_jabatan = 'Pelaksana';
			}
			$nama_jabatan_2019 = '';
			if($jabatan2019){
				$jfu2019 = JfuBaru::where('jfu_id', $jabatan2019->jfu_id)->first();
				if($jfu2019){
					$nama_jabatan_2019 = $jfu2019['jfu_nama'];
				}else{
					$nama_jabatan_2019 = '';
				}
			}			
			$unit_kerja = UnitKerja::where('unit_kerja_id', $a->unit_kerja_id)->first();

			$d = [
				$i+1,
				$a->peg_nip,
				$nama,				
				$golongan ? $golongan->nm_gol.', '.$golongan->nm_pkt : '',
				$unit_kerja->unit_kerja_nama,
				$nama_jabatan,
				$nama_jabatan_2019,                
			];			
			$data[] = $d;			
		}
		$h= [
			'NO',	// A
			'NIP',	// B
			'NAMA',	// C
			'GOL',	// D
			'UNIT KERJA',	// E
			'JABATAN LAMA',	// F
			'JABATAN 2019',	// G
		];
		$header = $h;
		$satuan_kerja = $satker->satuan_kerja_nama;
		
		Excel::create('Konversi Jabatan'.' '.$satuan_kerja, function($excel) use ($data, $header, $satuan_kerja, $status) {
			$excel->sheet('Konversi Jabatan', function($sheet) use ($data, $header, $satuan_kerja, $status) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(11);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:G2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$satuan_kerja = strtoupper($satuan_kerja);
				if($status == 'true'){
					$sheet->row(2, ['(CLEAN VERSION) DAFTAR PEGAWAI SATUAN KERJA '.$satuan_kerja]);
				}else{
					$sheet->row(2, ['DAFTAR PEGAWAI SATUAN KERJA '.$satuan_kerja]);
				}				

				$sheet->cells('A4:G4', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#cccccc');
				});

				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);

				$sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'C' => '@',
				    'D' => '@',
				    'E' => '@',
				    'F' => '@',
				    'G' => '@',				    		    
				));
				$sheet->row(4, $header);
				$sheet->setAutoFilter('A4:G4');
				$sheet->setBorder('A4:G4', 'thin');
				$sheet->fromArray($data, "", "A5", true, false);

				for ($j = 5; $j < count($data) + 5; $j++) {
					$sheet->setBorder('A'.$j.':G'.$j, 'thin');
				}
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});

		})->download('xlsx');
	}

	public function rekonUpload(){
		return view('pages.rekon-sapk.upload');
	}

	public function rekonUploadPost(Request $request){
		if($request->hasFile('datapns')){
			return view('pages.rekon-sapk.upload-post');
		} else {
			return view('pages.rekon-sapk.upload');
		}
	}

}
