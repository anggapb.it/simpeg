<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use Redirect;

use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Pegawai;

class ExecutiveController extends Controller {

  public function __construct(){
		$this->middleware('auth');
	}

  public function ulangTahunLocal(){
    if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)&&(Auth::user()->role_id != 6)) {
			return Redirect::back();
		}
    ini_set('max_execution_time', 10000);
    ini_set('memory_limit','2048M');

    $id = Auth::user()->satuan_kerja_id;
    $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}
    $satuan = "harian";

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->whereRaw('date_part(\'d\', peg_lahir_tanggal)=date_part(\'d\', CURRENT_DATE) and date_part(\'month\', peg_lahir_tanggal)=date_part(\'month\', CURRENT_DATE)')->orderBy('peg_nip')->get();
    return view('pages.list_ultah', compact('id', 'satker','pegawai', 'satuan'));
  }

  public function ulangTahunUnit($id){
    if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)&&(Auth::user()->role_id != 6)) {
      return Redirect::back();
    }
    ini_set('max_execution_time', 10000);
    ini_set('memory_limit','2048M');

    $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}
    $satuan = "harian";

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->whereRaw('date_part(\'d\', peg_lahir_tanggal)=date_part(\'d\', CURRENT_DATE) and date_part(\'month\', peg_lahir_tanggal)=date_part(\'month\', CURRENT_DATE)')->orderBy('peg_nip')->get();
    return view('pages.list_ultah', compact('id', 'satker','pegawai', 'satuan'));
	}

  public function ulangTahunBulananLocal(){
    if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)&&(Auth::user()->role_id != 6)) {
			return Redirect::back();
		}
    ini_set('max_execution_time', 10000);
    ini_set('memory_limit','2048M');

    $id = Auth::user()->satuan_kerja_id;
    $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}
    $satuan = "bulanan";

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->whereRaw('date_part(\'month\', peg_lahir_tanggal)=date_part(\'month\', CURRENT_DATE)')->orderBy('peg_nip')->get();
    return view('pages.list_ultah', compact('id', 'satker','pegawai', 'satuan'));
  }

  public function ulangTahunBulananUnit($id){
    if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)&&(Auth::user()->role_id != 6)) {
      return Redirect::back();
    }
    ini_set('max_execution_time', 10000);
    ini_set('memory_limit','2048M');

    $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}
    $satuan = "bulanan";

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->whereRaw('date_part(\'month\', peg_lahir_tanggal)=date_part(\'month\', CURRENT_DATE)')->orderBy('peg_nip')->get();
    return view('pages.list_ultah', compact('id', 'satker','pegawai', 'satuan'));
	}
}

?>
