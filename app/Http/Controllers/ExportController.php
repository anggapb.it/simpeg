<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Excel;
use PHPExcel_Style_Alignment;
use PDF;
use View;
use PHPExcel_Worksheet_PageSetup;

use App\Model\Pegawai;
use App\Model\Pegawai2;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Golongan;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Model\GajiTahun;
use App\Model\Keahlian;
use App\Model\StatusEditPegawai;
use App\Model\RiwayatPendidikan2;
use App\Model\RiwayatNonFormal2;
use App\Model\RiwayatPangkat2;
use App\Model\RiwayatJabatan2;
use App\Model\RiwayatDiklat2;
use App\Model\RiwayatPenghargaan2;
use App\Model\RiwayatKeluarga2;
use App\Model\RiwayatKeahlian2;
use App\Model\RiwayatKeahlianRel2;
use App\Model\RiwayatPendidikan;
use App\Model\RiwayatNonFormal;
use App\Model\RiwayatPangkat;
use App\Model\RiwayatJabatan;
use App\Model\RiwayatDiklat;
use App\Model\RiwayatPenghargaan;
use App\Model\RiwayatKeluarga;
use App\Model\RiwayatKeahlian;
use App\Model\RiwayatKeahlianRel;
use App\Model\RiwayatHukuman;
use App\Model\RiwayatHukuman2;
use App\Model\StatusKepegawaian;


class ExportController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function downloadPegawaiSkpd($id,$uker){
		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', 10000);
        // set_time_limit(0);

		$all = false;
		$data = array();
		$header = array();
		
		$query = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc');
		if($uker != 'all'){
			$query = $query->where('unit_kerja_id',$uker);
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$query = $query->get();
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
	     	$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
	     	if($a->peg_status_kepegawaian == '1'){
				$status = 'PNS';
				$tmt =  transformDate($a->peg_pns_tmt);
			}else if($a->peg_status_kepegawaian == '2'){
				$status = 'CPNS';
				$tmt = transformDate($a->peg_cpns_tmt);
			}else{
				$status = '-';
			}

			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
			}

			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			if($jabatan['jabatan_jenis'] == 2){
				$jab_jenis = $a['jabatan']['eselon']['eselon_nm'];
				$nama_jabatan = $jabatan->jabatan_nama;
			}elseif($jabatan['jabatan_jenis'] == 3){
				$jab_jenis = 'Fungsional Tertentu';
				$jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
				$nama_jabatan = $jf->jf_nama;
			}elseif($jabatan['jabatan_jenis'] == 4){
				$jab_jenis ='Fungsional Umum';
				$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
				if($jfu){
					$nama_jabatan = $jfu['jfu_nama'];
				}else{
					$nama_jabatan = 'Pelaksana';
				}
			}

			if($a->peg_jenis_kelamin == 'L'){
                $jenis_kelamin = 'Laki-laki';
            }else if($a->peg_jenis_kelamin == 'P'){
                $jenis_kelamin = 'Perempuan';
            }else{
                $jenis_kelamin='-';
            }

			if($a->peg_status_perkawinan == '1'){
				$peg_status_perkawinan = 'Kawin';
			}elseif($a->peg_status_perkawinan == '2'){
				$peg_status_perkawinan = 'Belum Kawin';
			}elseif($a->peg_status_perkawinan == '3'){
				$peg_status_perkawinan = 'Janda';
			}elseif($a->peg_status_perkawinan == '4'){
				$peg_status_perkawinan = 'Duda';
			}else{
				$peg_status_perkawinan = '-';
			}

			if($a['unit_kerja']['unit_kerja_level'] == 1){
				$uker = $a->unit_kerja->unit_kerja_nama;
			}else if($a['unit_kerja']['unit_kerja_level'] == 2){
				$parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
				$uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'];
			}else if($a['unit_kerja']['unit_kerja_level'] == 3){
				$parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
				$grandparent = UnitKerja::where('unit_kerja_id', $parent['unit_kerja_parent'])->first();
				$uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'].', '.$grandparent['unit_kerja_nama'];
			}else{
				$uker = '';
			}
			$kedudukan_pegawai=StatusKepegawaian::where('id_status_kepegawaian',$a->id_status_kepegawaian)->pluck('status');

			$d = [
				$i+1,
				$nama,
				$tempat.','.transformDate($a->peg_lahir_tanggal),
				$a->peg_nip,
				$uker,
				$golongan ? $golongan->nm_gol : '',
				transformDate($a->peg_gol_akhir_tmt),
				$jab_jenis ? : '',
				$nama_jabatan,
				transformDate($a->peg_jabatan_tmt),
				$status,
				$tmt,
				$a->peg_kerja_tahun,
				$a->peg_kerja_bulan,
				$jenis_kelamin,
				$a['agama']['nm_agama'],
				$peg_status_perkawinan,
				$a['pendidikan_awal']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_awal']['nm_pend'],
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],
                $a->peg_no_askes,
				$a->peg_npwp,
				$a->peg_ktp,
				$a->peg_rumah_alamat.' '. $a->peg_kel_desa.' '. $a['kecamatan']['kecamatan_nm'],
				$a->peg_telp,
				$a->peg_telp_hp,
				$a->peg_email,
				$kedudukan_pegawai,
			];
			$h= [
					'No',
					'Nama',
					'Tempat Tanggal Lahir',
					'NIP',
					'Unit Kerja',
					'Golongan Pangkat',
					'TMT Golongan',
					'Eselon',
					'Nama Jabatan',
					'TMT Jabatan',
					'Status Pegawai',
					'TMT Pegawai',
					'Masa Kerja Tahun',
					'Masa Kerja Bulan',
					'Jenis Kelamin',
					'Agama',
					'Status Perkawinan',
					'Pendidikan Awal',
					'Pendidikan Akhir',
					'No Askes',
					'No NPWP',
					'No KTP',
					'Alamat Rumah',
					'Telp',
					'HP',
					'E-mail',
					'Kedudukan Pegawai'
				];
			$data[] = $d;
			$header = $h;
			$satuan_kerja = strtoupper($satker->satuan_kerja_nama);
		}

		Excel::create('DATA PEGAWAI'.' '.$satuan_kerja, function($excel) use ($data, $header, $satuan_kerja) {
			$excel->sheet('DAFTAR PEGAWAI', function($sheet) use ($data, $header, $satuan_kerja) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(12);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:AA2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->row(2, ['DAFTAR PEGAWAI SATUAN KERJA'.' '.$satuan_kerja]);

				$sheet->cells('A4:AA4', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#b7dee8');
				});
				$sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'C' => '@',
				    'D' => '@',
				    'E' => '@',
				    'F' => '@',
				    'G' => '@',
				    'H' => '@',
				    'I' => '@',
				    'J' => '@',
				    'K' => '@',
				    'L' => '@',
				    'M' => '@',
				    'N' => '@',
				    'O' => '@',
				    'P' => '@',
				    'Q' => '@',
				    'R' => '@',
				    'S' => '@',
				    'T' => '@',
				    'U' => '@',
				    'V' => '@',
				    'W' => '@',
				    'X' => '@',
				    'Y' => '@',
				    'Z' => '@',
				    'AA' => '@',
				));
				$sheet->row(4, $header);
				$sheet->setAutoFilter('A4:AA4');
				$sheet->setBorder('A4:AA4', 'thin');
				$sheet->fromArray($data, "", "A5", true, false);

				for ($j = 5; $j < count($data) + 5; $j++) {
					$sheet->setBorder('A'.$j.':AA'.$j, 'thin');
				}
				
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});
		})->download('xlsx');
		
	}

	public function downloadDukSkpd($id){
		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', 10000);
        // set_time_limit(0);

		$all = false;
		$data = array();
		$header = array();
		
		$query = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('gol_id_akhir', 'desc')->orderBy('peg_gol_akhir_tmt')->orderBy('peg_nip');
		
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();

		$query = $query->get();
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
	     	$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();	     	

			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
			}

			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			if($jabatan['jabatan_jenis'] == 2){
				$nama_jabatan = $jabatan->jabatan_nama;
			}elseif($jabatan['jabatan_jenis'] == 3){
				$jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
				$nama_jabatan = $jf->jf_nama;
			}elseif($jabatan['jabatan_jenis'] == 4){
				$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
				if($jfu){
					$nama_jabatan = $jfu['jfu_nama'];
				}else{
					$nama_jabatan = 'Pelaksana';
				}
			}											

			$d = [
				$i+1,
				$a->peg_nip,
				$nama,
				$tempat.', '.transformDate($a->peg_lahir_tanggal),								
				$golongan ? $golongan->nm_gol : '',
				transformDate($a->peg_gol_akhir_tmt),
				$nama_jabatan,
				transformDate($a->peg_jabatan_tmt),
				$a->peg_kerja_tahun,
				$a->peg_kerja_bulan,								
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],                
			];
			$h= [
					'1',	// NO
					'2',	// NIP
					'3',	// Nama
					'4',	// TTL
					'5',	// Gol
					'6',	// TMT				
					'7',	// Nama Jab
					'8',	// TMT
					'9',	// MK THN
					'10',	// MK BLN		
					'11',	// PDDK AKHIR		
				];
			$data[] = $d;
			$header = $h;
			$satuan_kerja = strtoupper($satker->satuan_kerja_nama);
		}		

		Excel::create('DUK'.' '.$satuan_kerja, function($excel) use ($data, $header, $satuan_kerja) {
			$excel->sheet('DUK', function($sheet) use ($data, $header, $satuan_kerja) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(12);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:K2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->mergeCells('A3:K3');
				$sheet->cell('A3', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->mergeCells('A4:K4');
				$sheet->cell('A4', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->mergeCells('A5:K5');
				$sheet->cell('A5', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->row(2, ['DAFTAR URUTAN KEPANGKATAN']);
				$sheet->row(3, [$satuan_kerja]);
				$sheet->row(4, ['PEMERINTAH KOTA BANDUNG']);
				$sheet->row(5, [date("Y")]);

				$sheet->cells('A7:K9', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#cccccc');
				});

				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				);				

				$sheet->mergeCells('A7:A8');
				$sheet->getStyle('A7:A8')->applyFromArray($style);
				$sheet->setCellValue('A7', 'NO');

				$sheet->mergeCells('B7:B8');
				$sheet->getStyle('B7:B8')->applyFromArray($style);
				$sheet->setCellValue('B7', 'NIP');

				$sheet->mergeCells('C7:C8');
				$sheet->getStyle('C7:C8')->applyFromArray($style);
				$sheet->setCellValue('C7', 'NAMA');

				$sheet->mergeCells('D7:D8');	
				$sheet->getStyle('D7:D8')->applyFromArray($style);
				$sheet->setCellValue('D7', 'TEMPAT / TANGGAL LAHIR');

				$sheet->mergeCells('E7:F7');
				$sheet->getStyle('E7:F7')->applyFromArray($style);
				$sheet->setCellValue('E7', 'PANGKAT');

				$sheet->getStyle('E8')->applyFromArray($style);
				$sheet->setCellValue('E8', 'GOL');

				$sheet->getStyle('F8')->applyFromArray($style);
				$sheet->setCellValue('F8', 'TMT');

				$sheet->mergeCells('G7:H7');
				$sheet->getStyle('G7:H7')->applyFromArray($style);
				$sheet->setCellValue('G7', 'JABATAN');
				
				$sheet->getStyle('G8')->applyFromArray($style);
				$sheet->setCellValue('G8', 'NAMA');

				$sheet->getStyle('H8')->applyFromArray($style);
				$sheet->setCellValue('H8', 'TMT');

				$sheet->mergeCells('I7:J7');
				$sheet->getStyle('I7:J7')->applyFromArray($style);
				$sheet->setCellValue('I7', 'MASA KERJA');

				$sheet->getStyle('I8')->applyFromArray($style);
				$sheet->setCellValue('I8', 'THN');

				$sheet->getStyle('J8')->applyFromArray($style);
				$sheet->setCellValue('J8', 'BLN');

				$sheet->mergeCells('K7:K8');	
				$sheet->getStyle('K7:K8')->applyFromArray($style);
				$sheet->setCellValue('K7', 'PENDIDIKAN TERAKHIR');

				$sheet->setColumnFormat(array(
				    'A' => '@',
				    'B' => '@',
				    'C' => '@',
				    'D' => '@',
				    'E' => '@',
				    'F' => '@',
				    'G' => '@',
				    'H' => '@',
				    'I' => '@',
				    'J' => '@',
				    'K' => '@',				    
				));
				$sheet->row(9, $header);
				$sheet->setAutoFilter('A9:K9');
				$sheet->setBorder('A9:K9', 'thin');
				$sheet->fromArray($data, "", "A10", true, false);

				for ($j = 7; $j < count($data) + 10; $j++) {
					$sheet->setBorder('A'.$j.':K'.$j, 'thin');
				}
				
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});
		})->download('xlsx');
		
	}

	public function downloadPegawaiUnit($id){
		date_default_timezone_set('Asia/Jakarta');
		$all = false;
		$data = array();
		$header = array();
		
		$uker = UnitKerja::where('unit_kerja_id', $id)->first();
		$satker = SatuanKerja::where('satuan_kerja_id', $uker->satuan_kerja_id)->first(); 
		$query = Pegawai::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('gol_id_akhir', 'desc')->orderBy('jabatan_id')->orderBy('peg_nama')->get();
		
		foreach ($query as $i=>$a) {
			$golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
	     	$jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
	     	if($a->peg_status_kepegawaian == '1'){
				$status = 'PNS';
				$tmt =  transformDate($a->peg_pns_tmt);
			}else if($a->peg_status_kepegawaian == '2'){
				$status = 'CPNS';
				$tmt = transformDate($a->peg_cpns_tmt);
			}else{
				$status = '-';
			}

			if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
			}

			if($jabatan->jabatan_jenis == 2){
				$jab_jenis = 'Struktural';
				$nama_jabatan = $jabatan->jabatan_nama;
			}elseif($jabatan->jabatan_jenis == 3){
				$jab_jenis = 'Fungsional Tertentu';
				$jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
				$nama_jabatan = $jf->jf_nama;
			}elseif($jabatan->jabatan_jenis == 4){
				$jab_jenis ='Fungsional Umum';
				$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
				if($jfu){
					$nama_jabatan = $jfu['jfu_nama'];
				}else{
					$nama_jabatan = 'Pelaksana';
				}
			}

			$tempat = str_replace(' ', '', $a->peg_lahir_tempat);
			$d = [
				$i+1,
				$nama,
				$tempat.','.transformDate($a->peg_lahir_tanggal),
				$a->peg_nip,
				$golongan->nm_gol,
				transformDate($a->peg_gol_akhir_tmt),
				$nama_jabatan,
				transformDate($a->peg_jabatan_tmt),
				$status,
				$tmt,
				$a->peg_kerja_tahun,
				$a->peg_kerja_bulan,
			];
			$h= [
					'No',
					'Nama',
					'Tempat Tanggal Lahir',
					'NIP',
					'Golongan Pangkat',
					'TMT Golongan',
					'Nama Jabatan',
					'TMT Jabatan',
					'Status Pegawai',
					'TMT Pegawai',
					'Masa Kerja Tahun',
					'Masa Kerja Bulan',
				];
			$data[] = $d;
			$header = $h;
			$satuan_kerja = strtoupper($satker->satuan_kerja_nama);
			$unit_kerja =  strtoupper($uker->unit_kerja_nama);
		}

		Excel::create('DATA PEGAWAI SATUAN KERJA'.' '.$satuan_kerja.' UNIT KERJA '.$unit_kerja, function($excel) use ($data, $header, $satuan_kerja, $unit_kerja) {
			$excel->sheet('DAFTAR PEGAWAI', function($sheet) use ($data, $header, $satuan_kerja, $unit_kerja) {
				$sheet->setFontFamily('Calibri');
				$sheet->setFontSize(12);
				$sheet->setAutoSize(true);

				$sheet->mergeCells('A2:L2');
				$sheet->cell('A2', function($cell) {
					$cell->setAlignment('center');
					$cell->setFontWeight('bold');
				});
				$sheet->row(2, ['DAFTAR PEGAWAI SATUAN KERJA '.$satuan_kerja.' UNIT KERJA '.$unit_kerja]);

				$sheet->cells('A4:L4', function($cells) {
					$cells->setAlignment('center');
					$cells->setFontWeight('bold');
					$cells->setBackground('#b7dee8');
				});
				$sheet->row(4, $header);
				$sheet->setBorder('A4:L4', 'thin');
				$sheet->fromArray($data, "", "A5", true, false);

				for ($j = 5; $j < count($data) + 5; $j++) {
					$sheet->setBorder('A'.$j.':L'.$j, 'thin');
				}
				
				$sheet->setPageMargin(0.25);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
			});
		})->download('xlsx');
		
	}

	public function downloadProfilePegawai($id){
		$pegawai = Pegawai2::where('peg_id', $id)->first();
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first(); 
		$riwayat_pend = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->orderBy('riw_pendidikan_id')->get();
		$pend_non = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->orderBy('non_sttp_tanggal')->get();
		$riwayat_pangkat = RiwayatPangkat2::where('peg_id', $pegawai->peg_id)->orderBy('gol_id')->get();
		$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
		$riwayat_jabatan = RiwayatJabatan2::where('peg_id', $pegawai->peg_id)->orderBy('riw_jabatan_tmt')->get();
		$riwayat_diklat_s = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 1)->orderBy('diklat_selesai')->get();
		$riwayat_diklat_f = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 2)->orderBy('diklat_selesai')->get();
		$riwayat_diklat_t = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 3)->orderBy('diklat_selesai')->get();
		$penghargaan = RiwayatPenghargaan2::where('peg_id', $pegawai->peg_id)->get();
		$kel_p = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',4)->get();
		$kel_a = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',1)->orderBy('riw_tgl_lahir')->get();
		$kel_o = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',3)->orderBy('riw_tgl_lahir')->get();
		$kel_s = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',2)->orderBy('riw_tgl_lahir')->get();
		$hukuman = RiwayatHukuman2::where('peg_id', $pegawai->peg_id)->orderBy('riw_hukum_tmt')->get();
		$kedudukan_pegawai = StatusKepegawaian::where('id_status_kepegawaian',$pegawai->id_status_kepegawaian)->pluck('status');
		

		if(file_exists('uploads/'.$pegawai->peg_foto) && $pegawai->peg_foto != null){
			$foto = 'uploads/'.$pegawai->peg_foto;
		}else{
			$foto = base_path("public\\uploads\\none.png");
		}

		$imagehead = base_path("public/bdg.png");

		$pdf = PDF::loadView('pages.profile_pdf',compact('pegawai','satker','foto','riwayat_pend','pend_non', 'riwayat_pangkat', 'gaji_thn', 'riwayat_jabatan','riwayat_diklat_s','riwayat_diklat_f', 'riwayat_diklat_t', 'penghargaan','kel_p', 'kel_a','kel_o','kel_s', 'hukuman','imagehead','kedudukan_pegawai'));
		return $pdf->stream('Data Pegawai '.$pegawai->peg_nip.'.pdf')->header('Content-Type','application/pdf');
        return $pdf->setPaper('a4')->setWarnings(false)->download('Data Pegawai '.$pegawai->peg_nip.'.pdf');
	}

	public function downloadProfilePegawaiBkd($id){
		$pegawai = Pegawai::where('peg_id', $id)->first();
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first(); 
		$foto = 'uploads\\'.$pegawai->peg_foto;
		$riwayat_pend = RiwayatPendidikan::where('peg_id', $pegawai->peg_id)->orderBy('riw_pendidikan_id')->get();
		$pend_non = RiwayatNonFormal::where('peg_id', $pegawai->peg_id)->orderBy('non_sttp_tanggal')->get();
		$riwayat_pangkat = RiwayatPangkat::where('peg_id', $pegawai->peg_id)->orderBy('gol_id')->get();
		$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
		$riwayat_jabatan = RiwayatJabatan::where('peg_id', $pegawai->peg_id)->orderBy('riw_jabatan_tmt')->get();
		$riwayat_diklat_s = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 1)->orderBy('diklat_selesai')->get();
		$riwayat_diklat_f = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 2)->orderBy('diklat_selesai')->get();
		$riwayat_diklat_t = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 3)->orderBy('diklat_selesai')->get();
		$penghargaan = RiwayatPenghargaan::where('peg_id', $pegawai->peg_id)->get();
		$kel_p = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',4)->get();
		$kel_a = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',1)->orderBy('riw_tgl_lahir')->get();
		$kel_o = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',3)->orderBy('riw_tgl_lahir')->get();
		$kel_s = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',2)->orderBy('riw_tgl_lahir')->get();
		$hukuman = RiwayatHukuman::where('peg_id', $pegawai->peg_id)->orderBy('riw_hukum_tmt')->get();
		$imagehead = base_path("public\\bdg.png");

		$pdf = PDF::loadView('pages.profile_pdf',compact('pegawai','satker','foto','riwayat_pend','pend_non', 'riwayat_pangkat', 'gaji_thn', 'riwayat_jabatan','riwayat_diklat_s','riwayat_diklat_f', 'riwayat_diklat_t', 'penghargaan','kel_p', 'kel_a','kel_o','kel_s', 'hukuman', 'imagehead'));
		return $pdf->stream('Data Pegawai '.$pegawai->peg_nip.'.pdf')->header('Content-Type','application/pdf');
        return $pdf->setPaper('a4')->download('Data Pegawai '.$pegawai->peg_nip.'.pdf');
	}
}