<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\UnitKerja;
use App\Model\SatuanKerja;
use App\Model\Golongan;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use Validator;
use Input;
use Redirect;
use Auth;
use Excel;
use PHPExcel_Worksheet_PageSetup;

class GrafikController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function skpd(){
        return view('pages.grafik.skpd');
    }

	public function jabatan(){
		return view('pages.grafik.jabatan');
	}

    public function golongan(){
        return view('pages.grafik.golongan');
    }

    public function JenisKelamin(){
        return view('pages.grafik.jenis_kelamin');
    }

    public function pendidikan(){
        return view('pages.grafik.pendidikan');
    }

    public function usia(){
        return view('pages.grafik.kelompok_usia');
    }

    public function eselonJK(){
        return view('pages.grafik.eselon_jk');
    }

    public function getDataJabatan(Request $req, $id) {
        $rekaps = Pegawai::getRekapJabatan($id);
        
        $data[] = [ 'name' => 'Jabatan Struktural',
            		'y'=>$rekaps['total_s'],
            		];
	    $data[] = [ 'name' => 'Jabatan Fungsional Tertentu',
	        		'y'=>$rekaps['total_f'],
	        		];
        $data[] = [ 'name' => 'Jabatan Fungsional Umum',
                    'y'=>$rekaps['total_fu'],
                    ];
        if ($rekaps['total_uk']) {
        $data[] = [ 'name' => 'Data Jabatan Belum Lengkap',
                    'y'=>$rekaps['total_uk'],
                    ];
        }
            
        $result = [
            'data' => $data,
        ];

        return response()->json($result);
    }

    public function getDataJenisKelamin(Request $req, $id) {

        $rekaps = Pegawai::getRekapJenisKelamin($id);
        
        $data[] = [ 'name' => 'Laki-laki',
            		'y'=>$rekaps['l'],
            		];
	    $data[] = [ 'name' => 'Perempuan',
	        		'y'=>$rekaps['p'],
	        		];

        $result = [
            'data' => $data,
        ];

        return response()->json($result);
    }

    public function getDataGolongan(Request $req, $id) {
        
        $rekaps = Pegawai::getRekapGolongan($id);

        $data[] = ['I/A',$rekaps['1a']];
        $data[] = ['I/B',$rekaps['1b']];
		$data[] = ['I/C',$rekaps['1c']];
		$data[] = ['I/D',$rekaps['1d']];
		$data[] = ['II/A',$rekaps['2a']];
		$data[] = ['II/B',$rekaps['2b']];
		$data[] = ['II/C',$rekaps['2c']];
		$data[] = ['II/D',$rekaps['2d']];
		$data[] = ['III/A',$rekaps['3a']];
		$data[] = ['III/B',$rekaps['3b']];
		$data[] = ['III/C',$rekaps['3c']];
		$data[] = ['III/D',$rekaps['3d']];
		$data[] = ['IV/A',$rekaps['4a']];
		$data[] = ['IV/B',$rekaps['4b']];
		$data[] = ['IV/C',$rekaps['4c']];
		$data[] = ['IV/D',$rekaps['4d']];
		$data[] = ['IV/E',$rekaps['4e']];
        
        $result = [
            'data' => $data,
        ];

        return response()->json($result);
    }

    public function getDataPendidikan(Request $req, $id) {
        $rekaps = Pegawai::getRekapPendidikan($id);
        
        $data[] = ['SD',$rekaps['sd']];
        $data[] = ['SMP',$rekaps['smp']];
        $data[] = ['SMA',$rekaps['sma']];
        $data[] = ['D1',$rekaps['d1']];
        $data[] = ['D2',$rekaps['d2']];
        $data[] = ['D3',$rekaps['d3']];
        $data[] = ['D4',$rekaps['d4']];
        $data[] = ['S1',$rekaps['s1']];
        $data[] = ['S2',$rekaps['s2']];
        $data[] = ['S3',$rekaps['s3']];

        $result = [
            'data' => $data,
        ];

        return response()->json($result);
    }

    public function getDataUsia(Request $req, $id) {
        $rekaps = Pegawai::getRekapUsia($id);
        
        $data[] = ['16-25 Tahun',$rekaps['1']];
        $data[] = ['26-35 Tahun',$rekaps['2']];
        $data[] = ['36-45 Tahun',$rekaps['3']];
        $data[] = ['46-55 Tahun',$rekaps['4']];
        $data[] = ['56-65 Tahun',$rekaps['5']];
        $data[] = ['>65 Tahun',$rekaps['6']];
        $data[] = ['Tidak ada data umur',$rekaps['7']];

        $result = [
            'data' => $data,
        ];

        return response()->json($result);
    }

    public function getDataEselonJK(Request $req,$id) {
        $rekaps = Pegawai::getRekapEselonJK($id);
	    $d[] =  
	     	[ 	'name' => 'Laki-laki',
    			'data'=>[
			        $rekaps['eselon_2a_l'],
		            $rekaps['eselon_2b_l'],
		            $rekaps['eselon_3a_l'],
		            $rekaps['eselon_3b_l'],
		            $rekaps['eselon_4a_l'],
		            $rekaps['eselon_4b_l'],
		            $rekaps['eselon_5a_l'],
    			]
    		];
    	$d[] =  
	     	[ 	'name' => 'Perempuan',
    			'data'=>[
		            $rekaps['eselon_2a_p'],
		            $rekaps['eselon_2b_p'],
		            $rekaps['eselon_3a_p'],
		            $rekaps['eselon_3b_p'],
		            $rekaps['eselon_4a_p'],
		            $rekaps['eselon_4b_p'],
		            $rekaps['eselon_5a_p'],
    			]
    		];
        $result = [
            'd' => $d,
        ];

        return response()->json($result);
    }
}