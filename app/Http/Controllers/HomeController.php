<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\ReportKonversiJabatan;
use App\Model\UnitKerja;
use App\Model\Golongan;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Model\NotificationUserStatus;
use App\Model\Notification;
use Validator;
use Input;
use Redirect;
use Auth;
use File;
use Excel;
use PHPExcel_Worksheet_PageSetup;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

    public function daftarHadir()
    {
        return view('pages.absen.daftar_hadir');
    }

	public function search(){
		$url = url('pns/get-data');
        $columns = [
            'peg_nama' => ['Nama','peg_nama'],
            'peg_nip' => ['NIP','peg_nip'],
            'satuan_kerja_id' => ['Satuan Kerja','satuan_kerja.satuan_kerja_nama'],
            'peg_status' => ['Status Pegawai','peg_status'],
            'status_id' => ['Status Edit','status_edit.status_id'],
        ];
		return view('pages.all_pns',compact('url','columns'));
	}

	public function getDataRender(Request $request) {
        $models = Pegawai::with(['satuan_kerja','status_edit']);
        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }
        
        if ($search != '') {
            $models = $models->whereRaw("lower(peg_nama) like '%$search%'")
            		->orWhere('peg_nip','like',"%$search%");
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = count($models->get());
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
            if($models[$key]['peg_status']=='true'){
            	if($models[$key]['peg_status_kepegawaian']==1){
               	 	$models[$key]['peg_status'] = 'Aktif - PNS';
               	}elseif($models[$key]['peg_status_kepegawaian']==2){
               		$models[$key]['peg_status'] = 'Aktif - CPNS';
               	}else{
               		$models[$key]['peg_status'] = 'Aktif';
               	}
            }else{
                if($models[$key]['peg_ketstatus'] != null){
                    $models[$key]['peg_status'] = $models[$key]['peg_ketstatus'];
                }else{
                    $models[$key]['peg_status'] = 'Pegawai Pensiun';
                }
            }

            if($models[$key]['status_edit'] == null){
            	$models[$key]['status_edit'] = '-';
            }else{
            	if($models[$key]['status_edit']['status_id'] == 1){
            		$models[$key]['status_edit']['status_id'] = 'Draft';
            	}elseif($models[$key]['status_edit']['status_id'] ==2){
            		$models[$key]['status_edit']['status_id'] = 'Usul Perubahan';
            	}elseif($models[$key]['status_edit']['status_id'] == 3){
            		$models[$key]['status_edit']['status_id'] = 'Revisi';
            	}elseif($models[$key]['status_edit']['status_id'] == 4){
            		$models[$key]['status_edit']['status_id'] = 'Sudah Verifikasi BKD';
            	}elseif($models[$key]['status_edit']['status_id'] == 5){
                    $models[$key]['status_edit']['status_id'] = 'Usul Perubahan (Pegawai Unit Kerja)';
                }elseif($models[$key]['status_edit']['status_id'] == 6){
                    $models[$key]['status_edit']['status_id'] = 'Revisi Perubahan (Pegawai Unit Kerja)';
                }else{
            		$models[$key]['status_edit']['status_id'] = '-';
            	}
            }
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function exportData(Request $request){
        ini_set('max_execution_time', 6000);
        ini_set('memory_limit','2048M');
        date_default_timezone_set('Asia/Jakarta');
        $all = false;
        $data = array();
        $header = array();
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        
        $models = Pegawai::with(['satuan_kerja','status_edit']);
        if ($search != '') {
            $models = $models->whereRaw("lower(peg_nama) like '%$search%'")
                    ->orWhere('peg_nip','like',"%$search%");
        }else{
            if(File::exists(storage_path().'/exports/DATA SELURUH PEGAWAI KOTA BANDUNG.xlsx')){
                $file = storage_path().'/exports/DATA SELURUH PEGAWAI KOTA BANDUNG.xlsx';
                $data = File::get($file);
                
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        }
        $models = $models->get();
        foreach ($models as $i=>$a) {
            $golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
            $jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
            if($a->peg_status_kepegawaian == '1'){
                $status = 'PNS';
                $tmt =  transformDate($a->peg_pns_tmt);
            }else if($a->peg_status_kepegawaian == '2'){
                $status = 'CPNS';
                $tmt = transformDate($a->peg_cpns_tmt);
            }else{
                $status = '-';
            }

            if($a->peg_gelar_belakang != null){
                $nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
            }else{
                $nama= $a->peg_gelar_depan.$a->peg_nama;
            }

            $tempat = str_replace(' ', '', $a->peg_lahir_tempat);
            if($jabatan['jabatan_jenis'] == 2){
                $jab_jenis = $a['jabatan']['eselon']['eselon_nm'];
                $nama_jabatan = $jabatan->jabatan_nama;
            }elseif($jabatan['jabatan_jenis'] == 3){
                $jab_jenis = 'Fungsional Tertentu';
                $jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
                $nama_jabatan = $jf->jf_nama;
            }elseif($jabatan['jabatan_jenis'] == 4){
                $jab_jenis ='Fungsional Umum';
                $jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
                if($jfu){
                    $nama_jabatan = $jfu['jfu_nama'];
                }else{
                    $nama_jabatan = 'Pelaksana';
                }
            }else{
                $jab_jenis = '';
                $nama_jabatan = '';
            }

            if($a->peg_jenis_kelamin == 'L'){
                $jenis_kelamin = 'Laki-laki';
            }else if($a->peg_jenis_kelamin == 'P'){
                $jenis_kelamin = 'Perempuan';
            }else{
                $jenis_kelamin='-';
            }

            if($a->peg_status_perkawinan == '1'){
                $peg_status_perkawinan = 'Kawin';
            }elseif($a->peg_status_perkawinan == '2'){
                $peg_status_perkawinan = 'Belum Kawin';
            }elseif($a->peg_status_perkawinan == '3'){
                $peg_status_perkawinan = 'Janda';
            }elseif($a->peg_status_perkawinan == '4'){
                $peg_status_perkawinan = 'Duda';
            }else{
                $peg_status_perkawinan = '-';
            }

            if($a['unit_kerja']['unit_kerja_level'] == 1){
                $uker = $a->unit_kerja->unit_kerja_nama;
            }else if($a['unit_kerja']['unit_kerja_level'] == 2){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'];
            }else if($a['unit_kerja']['unit_kerja_level'] == 3){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                $grandparent = UnitKerja::where('unit_kerja_id', $parent['unit_kerja_parent'])->first();
                $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'].', '.$grandparent['unit_kerja_nama'];
            }else{
                $uker = '';
            }

            $d = [
                $i+1,
                $nama,
                $tempat.','.transformDate($a->peg_lahir_tanggal),
                $a->peg_nip,
                $a['satuan_kerja']['satuan_kerja_nama'],
                $uker,
                $golongan['nm_gol'],
                transformDate($a->peg_gol_akhir_tmt),
                $jab_jenis,
                $nama_jabatan,
                transformDate($a->peg_jabatan_tmt),
                $status,
                $tmt,
                $a->peg_kerja_tahun,
                $a->peg_kerja_bulan,
                $jenis_kelamin,
                $a['agama']['nm_agama'],
                $peg_status_perkawinan,
                $a['pendidikan_awal']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_awal']['nm_pend'],
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],
                $a->peg_no_askes,
                $a->peg_npwp,
                $a->peg_ktp,
                $a->peg_rumah_alamat.' '. $a->peg_kel_desa.' '. $a['kecamatan']['kecamatan_nm'],
                $a->peg_telp,
                $a->peg_telp_hp,
                $a->peg_email,
            ];
            $h= [
                    'No',
                    'Nama',
                    'Tempat Tanggal Lahir',
                    'NIP',
                    'Satuan Kerja',
                    'Unit Kerja',
                    'Golongan Pangkat',
                    'TMT Golongan',
                    'Eselon',
                    'Nama Jabatan',
                    'TMT Jabatan',
                    'Status Pegawai',
                    'TMT Pegawai',
                    'Masa Kerja Tahun',
                    'Masa Kerja Bulan',
                    'Jenis Kelamin',
                    'Agama',
                    'Status Perkawinan',
                    'Pendidikan Awal',
                    'Pendidikan Akhir',
                    'No Askes',
                    'No NPWP',
                    'No KTP',
                    'Alamat Rumah',
                    'Telp',
                    'HP',
                    'E-mail',
                ];
            $data[] = $d;
            $header = $h;
        }

        Excel::create('DATA SELURUH PEGAWAI KOTA BANDUNG', function($excel) use ($data, $header) {
            $excel->sheet('DAFTAR PEGAWAI', function($sheet) use ($data, $header) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:AA2');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->row(2, ['DAFTAR SELURUH PEGAWAI KOTA BANDUNG']);

                $sheet->cells('A4:AA4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                    'G' => '@',
                    'H' => '@',
                    'I' => '@',
                    'J' => '@',
                    'K' => '@',
                    'L' => '@',
                    'M' => '@',
                    'N' => '@',
                    'O' => '@',
                    'P' => '@',
                    'Q' => '@',
                    'R' => '@',
                    'S' => '@',
                    'T' => '@',
                    'U' => '@',
                    'V' => '@',
                    'W' => '@',
                    'X' => '@',
                    'Y' => '@',
                    'Z' => '@',
                    'AA' => '@',
                ));
                $sheet->setAutoFilter('A4:AA4');
                $sheet->row(4, $header);
                $sheet->setBorder('A4:AA4', 'thin');
                $sheet->fromArray($data, "", "A5", true, false);

                for ($j = 5; $j < count($data) + 5; $j++) {
                    $sheet->setBorder('A'.$j.':AA'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');
        
    }

    public function exportDataEvjab(Request $request){
        ini_set('max_execution_time', 6000);
        ini_set('memory_limit','2048M');
        date_default_timezone_set('Asia/Jakarta');
        $all = false;
        $data = array();
        $header = array();        
        
        $query = ReportKonversiJabatan::orderBy('satuan_kerja_nama')->orderBy('unit_kerja_nama')->orderBy('jfu_nama')->orderBy('peg_nama');
        $query = $query->get();
        foreach ($query as $i=>$a) {
            $golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
            if($a->peg_gelar_belakang != null){
				$nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
			}else{
				$nama= $a->peg_gelar_depan.$a->peg_nama;
            }
            
            $d = [
				$i+1,
				$a->peg_nip,
				$nama,				
                $golongan ? $golongan->nm_gol.', '.$golongan->nm_pkt : '',
                $a->pend_akhir,
                $a->satuan_kerja_nama,
                $a->unit_kerja_nama,
                $a->jfu_nama,
                $a->jfu_kelas,
                $a->unit_kerja_baru_nama,
                $a->jfu_nama_evjab,
                $a->jfu_kelas_evjab,				
			];			
			$data[] = $d;
        }
        $h= [
			'NO',	// A
			'NIP',	// B
			'NAMA',	// C
            'GOL',	// D
            'PENDIDIKAN TERAKHIR', // E
			'OPD',	// F
			'UNIT KERJA LAMA',	// G
            'JABATAN LAMA',	// H
            'KELAS LAMA',	// I
            'UNIT KERJA BARU',	// J
            'JABATAN BARU',	// K
            'KELAS BARU',	// L
		];
        $header = $h;
        
        Excel::create('Data Seluruh Konversi Jabatan', function($excel) use ($data, $header) {        
            $excel->sheet('DAFTAR PEGAWAI', function($sheet) use ($data, $header) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(false);
                $sheet->getColumnDimension('A')->setWidth("7");
                $sheet->getColumnDimension('B')->setWidth("19");
                $sheet->getColumnDimension('C')->setWidth("50");
                $sheet->getColumnDimension('D')->setWidth("22");
                $sheet->getColumnDimension('E')->setWidth("44");
                $sheet->getColumnDimension('F')->setWidth("60");
                $sheet->getColumnDimension('G')->setWidth("115");
                $sheet->getColumnDimension('H')->setWidth("44");
                $sheet->getColumnDimension('I')->setWidth("15");
                $sheet->getColumnDimension('J')->setWidth("115");
                $sheet->getColumnDimension('K')->setWidth("44");
                $sheet->getColumnDimension('L')->setWidth("15");

                $sheet->mergeCells('A2:L2');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->row(2, ['KONVERSI JABATAN PEGAWAI KOTA BANDUNG']);

                $sheet->cells('A4:L4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                    'G' => '@',
                    'H' => '@',
                    'I' => '@',
                    'J' => '@',
                    'K' => '@',    
                    'L' => '@',                
                ));
                $sheet->setAutoFilter('A4:L4');
                $sheet->row(4, $header);
                $sheet->setBorder('A4:L4', 'thin');
                $sheet->fromArray($data, "", "A5", true, false);

                for ($j = 5; $j < count($data) + 5; $j++) {
                    $sheet->setBorder('A'.$j.':L'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');
        
    }

    public function readNotif(Request $req)
    {
        $id = $req->get('id');
        $user_id = Auth::user()->id;
        $cek = NotificationUserStatus::where('notification_id',$id)->where('user_id',$user_id)->first();
        if(count($cek) > 0){
            $stat = NotificationUserStatus::find($cek->id);
        }else{
            $stat = new NotificationUserStatus;
            $stat->notification_id = $id;
            $stat->user_id = $user_id;
        }
        $stat->is_readed = 1;

        if($stat->save()){
            $cek = NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_deleted',1)->lists('notification_id')->toArray();
            $read = NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_readed',1)->where('is_deleted','<>',1)->lists('notification_id')->toArray();
            $jum = Notification::whereNotIn('id',array_merge($cek,$read))->whereRaw("exists (select * from jsonb_array_elements_text(role_ids) where value = '".Auth::user()->role_id."')")->count();

            return response()->json(['sisa' => $jum]);
        }
    }

    public function deleteNotif(Request $req)
    {
        $id = $req->get('id');
        $user_id = Auth::user()->id;
        $cek = NotificationUserStatus::where('notification_id',$id)->where('user_id',$user_id)->first();
        if(count($cek) > 0){
            $stat = NotificationUserStatus::find($cek->id);
        }else{
            $stat = new NotificationUserStatus;
            $stat->notification_id = $id;
            $stat->user_id = $user_id;
        }
        $stat->is_deleted = 1;

        if($stat->save()){
            $cek = NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_deleted',1)->lists('notification_id')->toArray();
            $read = NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_readed',1)->where('is_deleted','<>',1)->lists('notification_id')->toArray();
            $jum = Notification::whereNotIn('id',array_merge($cek,$read))->whereRaw("exists (select * from jsonb_array_elements_text(role_ids) where value = '".Auth::user()->role_id."')")->count();

            return response()->json(['sisa' => $jum]);
        }
    }

    public function getSatuanOrganisasi(Request $req){
        $input_data = $req->except('_');
        $id = $input_data['satuan_kerja_id'];

        $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->where('unit_kerja_level',1)->orderBy('unit_kerja_nama')->get();
        echo json_encode($unit_kerja);
    }
    
    public function getUnitKerja(Request $req){
        $input_data = $req->except('_');
        $id = $input_data['satuan_kerja_id'];
        $parent = $input_data['unit_kerja_parent'];

        if($parent != null){
            $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->where('unit_kerja_parent', $parent);
        }else{
            $unit_kerja = UnitKerja::where('satuan_kerja_id', $id);
        }
        $unit_kerja = $unit_kerja->where('unit_kerja_nama','NOT LIKE','%-%')->get();
        echo json_encode($unit_kerja);
    }

     public function getPegawai(Request $req){
        $input_data = $req->except('_');
        $id = $input_data['satuan_kerja_id'];
        $parent = $input_data['unit_kerja_parent'] ? $input_data['unit_kerja_parent'] : null;
        $uker = $input_data['unit_kerja_id'] ? $input_data['unit_kerja_id'] : null;

        $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->where('unit_kerja_parent', $parent)->get();
        $uk = array();

        foreach ($unit_kerja as $key => $value) {
            array_push($uk, $value->unit_kerja_id);
        }
        array_push($uk, $parent);

        $pegawai = Pegawai::select('peg_id','peg_nip','peg_nama')->where('peg_status','true')->where('satuan_kerja_id', $id);
        if($parent != null && $uker == null){
            if(($parent==2) || ($parent==3) || ($parent==4)){
                $pegawai = $pegawai->where('unit_kerja_id',$parent);
            }else{
                $pegawai = $pegawai->whereIn('unit_kerja_id',getAllUnitKerjaChildren($parent));
            }
        }

        if($uker != null){
            $pegawai = $pegawai->whereIn('unit_kerja_id',getAllUnitKerjaChildren($uker));
        }

        $pegawai= $pegawai->orderBy('peg_nama')->get();
        echo json_encode($pegawai);
    }
}
