<?php namespace App\Http\Controllers\Iframe;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\UnitKerja;
use App\Model\SatuanKerja;
use App\Model\Golongan;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Model\KategoriPendidikan;
use App\Model\Pendidikan;
use App\Model\RiwayatPendidikan;
use Validator;
use Input;
use Redirect;
use Auth;
use Excel;
use PHPExcel_Worksheet_PageSetup;
use DB;
use File;

class DaftarPegawaiController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

    public function index(){
        $columns = [
            'peg_nip' => ['NIP','peg_nip'],
            'peg_nama' => ['Nama','peg_nama'],
            // 'satuan_kerja_id' => ['Satuan Kerja','satuan_kerja.satuan_kerja_nama'],
            'unit_kerja_id' => ['Unit Kerja','unit_kerja.unit_kerja_nama'],
            'gol_id_akhir' => ['Golongan','golongan_akhir.nm_gol'],
            'eselon_id' => ['Eselon','jabatan.eselon.eselon_nm'],
            'jabatan_id' => ['Jabatan','jabatan.jabatan_nama'],
        ];
        return view('pages.iframe.daftar_pegawai',compact('columns','kelas_jabatan'));
    }

    public function searchParam(Request $request) {
        $models = Pegawai::select('spg_pegawai.*')->where('peg_status', true)->where('spg_pegawai.satuan_kerja_id','<>',999999)->with(['satuan_kerja','jabatan.eselon', 'golongan_akhir','unit_kerja']);
        $mandatory = false;
        $params = $request->get('params',false);
        $search_any = $request->get('search',false);
        $search_nama = $request->get('search_nama',false);
        $search_nip = $request->get('search_nip',false);
        $search_jabatan = $request->get('search_jabatan',false);
        $search_satker = $request->get('search_satker',false);
        $search_uker = $request->get('search_uker',false);
        $search_golongan = $request->get('search_golongan',false);
        $search_eselon = $request->get('search_eselon',false);
        $search_kelas_jabatan = $request->get('search_kelas_jabatan',false);
        $search_agama = $request->get('search_agama',false);
        $search_status_pegawai = $request->get('search_status_pegawai',false);
        $search_tingpend = $request->get('search_tingpend',false);
        $search_jurusan = $request->get('search_jurusan',false);
        $search_jk = $request->get('search_jk',false);
        $search_perkawinan = $request->get('search_perkawinan',false);
        $search_goldar = $request->get('search_goldar',false);
        $search_usia = $request->get('search_usia',false);
        $search_alamat = $request->get('search_alamat',false);
        $search_hp = $request->get('search_hp',false);
        $search_instansi = $request->get('search_instansi',false);
        $search_nama = addslashes(strtolower($search_nama));
        $search_nip = addslashes(strtolower($search_nip));
        $search_jabatan = addslashes(strtolower($search_jabatan));
        $search_alamat = addslashes(strtolower($search_alamat));
        $search_hp = addslashes(strtolower($search_hp));
        $search_instansi = addslashes(strtolower($search_instansi));
        $search_riwayat = $request->get('search_riwayat',false);
        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }
        
        if (!empty($search_any)) {
            $models = $models->leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_pegawai.jabatan_id')
                ->leftJoin('m_spg_referensi_jf','m_spg_jabatan.jf_id','=','m_spg_referensi_jf.jf_id')
                ->leftJoin('m_spg_eselon','m_spg_jabatan.eselon_id','=','m_spg_eselon.eselon_id')
                ->leftJoin('m_spg_referensi_jfu','m_spg_jabatan.jfu_id','=','m_spg_referensi_jfu.jfu_id')
                ->leftJoin('m_spg_golongan','spg_pegawai.gol_id_akhir','=','m_spg_golongan.gol_id')
                ->leftJoin('m_spg_satuan_kerja','spg_pegawai.satuan_kerja_id','=','m_spg_satuan_kerja.satuan_kerja_id')
                ->leftJoin('m_spg_unit_kerja','spg_pegawai.unit_kerja_id','=','m_spg_unit_kerja.unit_kerja_id')
                ->where(function($q) use ($search_any) {
                $q->where('peg_nama','ilike','%'.$search_any.'%')
                  ->orWhere('jabatan_nama','ilike','%'.$search_any.'%')
                  ->orWhere('eselon_nm','ilike',$search_any)
                  ->orWhere('nm_gol','ilike',$search_any)
                  ->orWhere('jf_nama','ilike','%'.$search_any.'%')
                  ->orWhere('jfu_nama','ilike','%'.$search_any.'%')
                  ->orWhere('satuan_kerja_nama','ilike','%'.$search_any.'%')
                  ->orWhere('unit_kerja_nama','ilike','%'.$search_any.'%')
                  ->orWhere('peg_nip','ilike','%'.$search_any.'%');
                if (strpos(strtolower($search_any), 'umum') !== false) {
                    $q->orWhere('jabatan_jenis',4);
                } else if (strpos(strtolower($search_any), 'tertentu') !== false) {
                    $q->orWhere('jabatan_jenis',3);
                } else if (strpos(strtolower($search_any), 'struktur') !== false) {
                    $q->orWhere('jabatan_jenis',2);
                } else if (strpos(strtolower($search_any), 'fungsional') !== false) {
                    $q->orWhere('jabatan_jenis',4)->orWhere('jabatan_jenis',3);
                }
            });
        }
        
        if (!empty($search_nama)) {
            $models = $models->whereRaw("peg_nama ilike '%$search_nama%'");
        }

        if(!empty($search_nip)) {
            $models = $models->where('peg_nip','like',"%$search_nip%");
        }

        if(!empty($search_satker)) {
            $satker = SatuanKerja::where('satuan_kerja_nama','ilike',"%$search_satker%")->get()->pluck('satuan_kerja_id');
            if ($satker->count()) {
                $models = $models->whereIn('spg_pegawai.satuan_kerja_id',$satker);
                $mandatory = true;
            }
        }

        if(!empty($search_uker)) {
            $uker = UnitKerja::where('unit_kerja_nama','ilike',$search_uker)->first();
            if ($uker) {
                $models = $models->whereIn('spg_pegawai.unit_kerja_id',getAllUnitKerjaChildren($uker->unit_kerja_id));
                $mandatory = true;
            }
        }

        if(!empty($search_golongan)) {
            $models = $models->whereIn('gol_id_akhir',$search_golongan);
        }
        if(!empty($search_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::whereRaw("jf_nama ilike '%$search_jabatan%'")->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::whereRaw("jfu_nama ilike '%$search_jabatan%'")->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::whereRaw("jabatan_nama ilike '%$search_jabatan%'")->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $models = $models->whereIn('spg_pegawai.jabatan_id', $id_jabatan);
            }
        }

        if(!empty($search_kelas_jabatan)) {
            $id_jabatan = array();
            $jf_ = array();
            $jfu_ = array();

            $jf = JabatanFungsional::where('jf_kelas',$search_kelas_jabatan)->get();
            foreach ($jf as $key => $value) {
                $jf_[] = $value->jf_id;
            }

            $jfu = JabatanFungsionalUmum::where('jfu_kelas',$search_kelas_jabatan)->get();
            foreach ($jfu as $key => $value) {
                $jfu_[] = $value->jfu_id;
            }

            $jabatan = Jabatan::where('jabatan_kelas',$search_kelas_jabatan)->orWhereIn('jf_id', $jf_)->orWhereIn('jfu_id', $jfu_)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }

                $models = $models->whereIn('spg_pegawai.jabatan_id', $id_jabatan);
            }
        }
        if(!empty($search_eselon)) {
            $id_jabatan = array();
            $jabatan = Jabatan::whereIn('eselon_id', $search_eselon)->get();
            if($jabatan->count()!=0){
                foreach ($jabatan as $key => $value) {
                    $id_jabatan[] = $value->jabatan_id;
                }
                $models = $models->whereIn('spg_pegawai.jabatan_id',$id_jabatan);
            }
        }

        if(!empty($search_agama)) {
            $models = $models->where('id_agama',$search_agama);
        }

        if(!empty($search_status_pegawai)) {
            $models = $models->where('peg_status_kepegawaian',$search_status_pegawai);
        }

        if(!empty($search_tingpend)) {
            $kategori = KategoriPendidikan::where('tingpend_id', $search_tingpend)->lists('kat_pend_id');
            $pendidikan = Pendidikan::whereIn('kat_pend_id', $kategori)->lists('id_pend');
            $models = $models->whereIn('id_pend_akhir',$pendidikan);
        }
        if(!empty($search_jurusan)) {
            $jurusan = RiwayatPendidikan::where('jurusan_id',$search_jurusan)->groupBy('peg_id')->get(['peg_id']);
            $models = $models->whereIn('spg_pegawai.peg_id',$jurusan);
        }
        if($search_jk){
            $models = $models->where('peg_jenis_kelamin',$search_jk);
        }

        if($search_perkawinan){
            $models = $models->where('peg_status_perkawinan',$search_perkawinan);
        }

        if($search_goldar){
            $models = $models->where('id_goldar',$search_goldar);
        }

        if (!empty($search_alamat)) {
            $models = $models->whereRaw("peg_rumah_alamat ilike '%$search_alamat%'");
        }
        if (!empty($search_hp)) {
            $models = $models->whereRaw("peg_telp_hp ilike '%$search_hp%'");
        }
        if (!empty($search_instansi)) {
            $models = $models->whereRaw("peg_instansi_dpk ilike '%$search_instansi%'");
        }
        if (!empty($search_riwayat)) {
            $caririwayat = RiwayatPendidikan::leftJoin('m_spg_jurusan','m_spg_jurusan.jurusan_id','=','spg_riwayat_pendidikan.jurusan_id')->where('jurusan_nm','ilike','%'.$search_riwayat.'%')->lists('peg_id')->toArray();
            $models = $models->whereIn('spg_pegawai.peg_id',$caririwayat);
        }

        if(!empty($search_usia)) {
            $usia = DB::table('spg_pegawai')
                     ->select(DB::raw("date_part('year',age(peg_lahir_tanggal)) as usia, peg_lahir_tanggal"))
                     ->where('peg_status', true)
                     ->get();
            $kel_1=[];
            $kel_2=[];
            $kel_3=[];
            $kel_4=[];
            $kel_5=[];
            $kel_6=[];

            foreach ($usia as $key => $value) {
                if($value->usia >= 16 && $value->usia <= 25){
                    $kel_1[] = [$value->peg_lahir_tanggal];
                }elseif($value->usia >= 26 && $value->usia <= 35){
                    $kel_2[]=[$value->peg_lahir_tanggal];
                }elseif($value->usia >= 36 && $value->usia <= 45){
                    $kel_3[]=[$value->peg_lahir_tanggal];
                }elseif($value->usia >= 46 && $value->usia <= 55){
                    $kel_4[]=[$value->peg_lahir_tanggal];
                }elseif($value->usia >= 56 && $value->usia <= 65){
                    $kel_5[]=[$value->peg_lahir_tanggal];
                }elseif($value->usia > 65){
                    $kel_6[]=[$value->peg_lahir_tanggal];
                }
            }

            if($search_usia ==1){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_1);
            }elseif($search_usia ==2){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_2);
            }elseif($search_usia ==3){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_3);
            }elseif($search_usia ==4){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_4);
            }elseif($search_usia ==5){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_5);
            }elseif($search_usia ==6){
                $models = $models->whereIn('peg_lahir_tanggal',$kel_6);
            }
        }

        if (!$mandatory) {
            return null;
        }
        return $models;
    }

    public function getDataRender(Request $request) {
        $models = $this->searchParam($request);
        if (!$models) {
            $result = [
                'data' => [],
                'count' => 0
            ];
            return response()->json($result);
        }
        $order  = $request->get('order' ,false);

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = $models->count();
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }

        $models = $models->skip(($page-1) * $perpage)->take($perpage)->orderBy('peg_nama')->get();

        foreach ($models as $key => $value) {
            if($models[$key]['jabatan'] == null){
                $models[$key]['jabatan'] = '-';
            }else{
                if($models[$key]['jabatan']['jabatan_jenis'] == 2){
                    $models[$key]['jabatan']['jabatan_nama'] = $models[$key]['jabatan']['jabatan_nama'];
                }elseif($models[$key]['jabatan']['jabatan_jenis'] ==3){
                    $jf = JabatanFungsional::where('jf_id', $models[$key]['jabatan']['jf_id'])->first();
                    if($jf){
                      $models[$key]['jabatan']['jabatan_nama'] = $jf->jf_nama;
                    }else{
                      $models[$key]['jabatan']['jabatan_nama'] = 'Pelaksana';
                    }
                }elseif($models[$key]['jabatan']['jabatan_jenis'] == 4){
                    $jfu = JabatanFungsionalUmum::where('jfu_id', $models[$key]['jabatan']['jfu_id'])->first();
                    if($jfu){
                        $models[$key]['jabatan']['jabatan_nama'] = $jfu['jfu_nama'];
                    }else{
                        $models[$key]['jabatan']['jabatan_nama'] = 'Pelaksana';
                    }
                }else{
                    $models[$key]['jabatan']['jabatan_nama'] = '-';
                }
            }
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function exportData(Request $request){
        set_time_limit (5000);
        ini_set('memory_limit','2048M');
        date_default_timezone_set('Asia/Jakarta');
        $models = $this->searchParam($request);

        if (!$models) {
            return redirect()->back()->with('message','Terjadi kesalahan');
        }

        $models = $models->orderBy('peg_nama')->get();

        $all = false;
        $data = array();
        $header = array();

        foreach ($models as $i => $a) {
            $golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
            $jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
            if($a->peg_status_kepegawaian == '1'){
                $status = 'PNS';
                $tmt =  transformDate($a->peg_pns_tmt);
            }else if($a->peg_status_kepegawaian == '2'){
                $status = 'CPNS';
                $tmt = transformDate($a->peg_cpns_tmt);
            }else{
                $status = '-';
            }

            if($a->peg_gelar_belakang != null){
                $nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
            }else{
                $nama= $a->peg_gelar_depan.$a->peg_nama;
            }

            $tempat = str_replace(' ', '', $a->peg_lahir_tempat);
            if($jabatan['jabatan_jenis'] == 2){
                $jab_jenis = $a['jabatan']['eselon']['eselon_nm'];
                $nama_jabatan = $jabatan->jabatan_nama;
                $jabatan_kelas = $jabatan->jabatan_kelas;
            }elseif($jabatan['jabatan_jenis'] == 3){
                $jab_jenis = 'Fungsional Tertentu';
                $jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
                $nama_jabatan = $jf->jf_nama;
                $jabatan_kelas = $jf->jf_kelas;
            }elseif($jabatan['jabatan_jenis'] == 4){
                $jab_jenis ='Fungsional Umum';
                $jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
                if($jfu){
                    $nama_jabatan = $jfu['jfu_nama'];
                }else{
                    $nama_jabatan = 'Pelaksana';
                }
                $jabatan_kelas = $jfu->jfu_kelas;
            }else{
                $jab_jenis = '';
                $nama_jabatan = '';
                $jabatan_kelas = '';
            }

            if($a->peg_jenis_kelamin == 'L'){
                $jenis_kelamin = 'Laki-laki';
            }else if($a->peg_jenis_kelamin == 'P'){
                $jenis_kelamin = 'Perempuan';
            }else{
                $jenis_kelamin='-';
            }

            if($a->peg_status_perkawinan == '1'){
                $peg_status_perkawinan = 'Kawin';
            }elseif($a->peg_status_perkawinan == '2'){
                $peg_status_perkawinan = 'Belum Kawin';
            }elseif($a->peg_status_perkawinan == '3'){
                $peg_status_perkawinan = 'Janda';
            }elseif($a->peg_status_perkawinan == '4'){
                $peg_status_perkawinan = 'Duda';
            }else{
                $peg_status_perkawinan = '-';
            }

            if($a['unit_kerja']['unit_kerja_level'] == 1){
                $uker = $a->unit_kerja->unit_kerja_nama;
            }else if($a['unit_kerja']['unit_kerja_level'] == 2){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                if (strpos(strtolower($a['satuan_kerja']['satuan_kerja_nama']),'kecamatan') !== false) {
                    $uker = $parent['unit_kerja_nama'].', '.$a->unit_kerja->unit_kerja_nama;
                } else {
                    $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'];
                }
            }else if($a['unit_kerja']['unit_kerja_level'] == 3){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                $grandparent = UnitKerja::where('unit_kerja_id', $parent['unit_kerja_parent'])->first();
                $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'].', '.$grandparent['unit_kerja_nama'];
            }else{
                $uker = '';
            }
 
            $d = [
                $i+1,
                $nama,
                $tempat.','.transformDate($a->peg_lahir_tanggal),
                $a->peg_nip,
                $a['satuan_kerja']['satuan_kerja_nama'],
                $uker,
                $golongan['nm_gol'],
                transformDate($a->peg_gol_akhir_tmt),
                $jab_jenis,
                $nama_jabatan,
                $jabatan_kelas,
                transformDate($a->peg_jabatan_tmt),
                $status,
                $tmt,
                $a->peg_kerja_tahun,
                $a->peg_kerja_bulan,
                $jenis_kelamin,
                $a['agama']['nm_agama'],
                $peg_status_perkawinan,
                $a['pendidikan_awal']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_awal']['nm_pend'],
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],
                $a->peg_no_askes,
                $a->peg_npwp,
                $a->peg_ktp,
                $a->peg_rumah_alamat.' '. $a->peg_kel_desa.' '. $a['kecamatan']['kecamatan_nm'],
                $a->peg_telp,
                $a->peg_telp_hp,
                $a->peg_email,
            ];
            $h= [
                    'No',
                    'Nama',
                    'Tempat Tanggal Lahir',
                    'NIP',
                    'Satuan Kerja',
                    'Unit Kerja',
                    'Golongan Pangkat',
                    'TMT Golongan',
                    'Eselon',
                    'Nama Jabatan',
                    'Jabatan Kelas',
                    'TMT Jabatan',
                    'Status Pegawai',
                    'TMT Pegawai',
                    'Masa Kerja Tahun',
                    'Masa Kerja Bulan',
                    'Jenis Kelamin',
                    'Agama',
                    'Status Perkawinan',
                    'Pendidikan Awal',
                    'Pendidikan Akhir',
                    'No Askes',
                    'No NPWP',
                    'No KTP',
                    'Alamat Rumah',
                    'Telp',
                    'HP',
                    'E-mail',
                ];
            $data[] = $d;
            $header = $h;
        }


        Excel::create('Data Pegawai', function($excel) use ($data, $header) {
            $excel->sheet('Data Pegawai', function($sheet) use ($data, $header) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);
                $sheet->mergeCells('A1:H1');

                $sheet->cell('A1', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(14);
                });
                $sheet->row(1, ['DATA PEGAWAI']);
                
                $sheet->cells('A3:AA3', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });
                $sheet->row(3, $header);
                $sheet->setAutoFilter('A3:AA3');

                $sheet->fromArray($data, "", "A4", true, false);

                for ($j = 3; $j < count($data) + 4; $j++) {
                    $sheet->setBorder('A'.$j.':AA'.$j, 'thin');
                }

                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');
        
    }

}
