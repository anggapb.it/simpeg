<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Excel;
use Input;
use File;

use App\Model\Pegawai;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;

class ImportController extends Controller
{
    public function importJfuPegawaiView(){
        return view('pages.import_pegawai_jfu');
    }
    
    public function importJfuPegawai(Request $request){
        $satuan_kerja_id = $request->get('satuan_kerja_id');
        
       // Import a user provided file
        $file = Input::file('excelfile',null);
        $destinationPath = 'uploads'; // upload path
        if ($file == null) {
            return false;
        }
        $extension = $file->getClientOriginalExtension(); // getting file extension
        $filename = $file->getClientOriginalName(); // getting file extension
        
        while (File::exists("$destinationPath/$filename")) {
            $extension_pos = strrpos($filename, '.');
            $filename = substr($filename, 0, $extension_pos) . str_random(5) . substr($filename, $extension_pos);
        }

        $upload_success = $file->move($destinationPath, $filename); // uploading file to given path

        if($upload_success){
            $file = public_path("uploads/".$filename);
        }
        $messages = "";
        $excel = new Excel($file);
        $data = $excel->toArray();
        $processed = 0;
        $processed_fail = 0;
        for ($row = 5; $row < count($data); $row++) {
            if ($data[$row][3]) {
                $nip = preg_replace("/[^0-9]/","",$data[$row][3]);
                $pegawai = Pegawai::where('peg_nip',$nip)->first();
                if (!$pegawai) {
                    $messages .= "Pegawai <b>{$data[$row][2]}</b> tidak proses karena NIP $nip tidak ditemukan<br>\n";
                    $processed_fail++;
                    continue;
                }
                $satuan_kerja_id = $pegawai->satuan_kerja_id;
                //$pegawai->satuan_kerja_id = $satuan_kerja_id;
                $unit_kerja_id = $pegawai->unit_kerja_id;
                $nama_jabatan = addslashes(strtolower($data[$row][1]));
                $nama_jabatan=preg_replace('/\s+/', '', $nama_jabatan);
                //dd($nama_jabatan);
                $jfu = JabatanFungsionalUmum::whereRaw("lower(regexp_replace(jfu_nama, ' ', '','g')) = '$nama_jabatan'")->first();
                
                if ($jfu) {
                    $jab = Jabatan::getOrCreateJabatan($satuan_kerja_id,$unit_kerja_id,$jfu->jfu_id,null);
                } else {
                    $jf = JabatanFungsional::whereRaw("lower(regexp_replace(jf_nama, ' ', '','g')) = '$nama_jabatan'")->first();
                    if (!$jf) {
                        $messages .= "Pegawai <b>{$data[$row][2]}</b> tidak proses karena Jabatan <b>{$data[$row][1]}</b> tidak ditemukan<br>\n";
                        $processed_fail++;
                        continue;
                    }
                    $jab = Jabatan::getOrCreateJabatan($satuan_kerja_id,$unit_kerja_id,null,$jf->jf_id);
                }
                $pegawai->jabatan_id = $jab->jabatan_id;
                $pegawai->save();
                $processed++;
            } else {
                if ($data[$row][2]) {
                    $messages .= "Pegawai <b>{$data[$row][2]}</b> tidak proses karena NIP tidak tersedia<br>\n";
                    $processed_fail++;
                }
                continue;
            }
        }
        $messages .= "$processed_fail data gagal diproses";
        $messages .= "$processed data telah diproses";
        return redirect()->back()->with('message',$messages);
    }
}
