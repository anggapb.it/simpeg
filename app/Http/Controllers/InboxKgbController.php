<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Pegawai;
use App\Model\UsulanKgb;
use App\Model\UsulanDetailKgb;

class InboxKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(!$id)
        $id = Auth::user()->satuan_kerja_id;

        // ini_set('max_execution_time', 10000);
        // ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $title = 'KGB';

        return view('pages.kgb.inbox_kgb.index', compact('satker', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($usulan_id='')
    {
        // if(!$id)
        $id = Auth::user()->satuan_kerja_id;

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        $usulan = UsulanKgb::where('usulan_id', $usulan_id)->with('statusUsulan')->first();
        
		$list_satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();


		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $id){
			return view('pages.kgb.inbox_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id == 6){
			return view('pages.kgb.inbox_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
		}else if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.inbox_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
			}else{
				return Redirect::back();
			}
		}else if(Auth::user()->role_id == 9){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai_induk = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE');
				$pegawai = Pegawai::whereRaw('unit_kerja_id in (select unit_kerja_id from m_spg_unit_kerja where unit_kerja_parent = '.Auth::user()->unit_kerja_id.')')->where('peg_status', 'TRUE')->union($pegawai_induk)->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.inbox_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
			}else{
				return Redirect::back();
			}
		}else{
			return Redirect::back();
		}
    }

    public function store(Request $req)
    {
        // $data = $req->except('_token');
        $usulan_id = $req->usulan_id?:null;
        $usulan_det = UsulanDetailKgb::where('usulan_id', $usulan_id)->
                        where('status_usulan_pegawai', '=', 10)->count();
        
		if($usulan_det > 0){
                $usulan = UsulanKgb::find($usulan_id);
                $usulan->status = 30;
                $usulan->save();
                
                logAction('Save Inbox Kgb',json_encode($usulan),$usulan_id,Auth::user()->username);
                return Redirect::to('kgb/inbox-kgb/detail/'.$usulan_id)->with('message','Data berhasil diupdate');
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Detail usulan belum ada yang diproses KGB');
		}

    }
    
    public function changeStatusUsulan(Request $req)
    {
        // $data = $req->except('_token');
        $usulan_id = $req->usulan_id?:null;
        // $cek = 0;

        $usulan = UsulanKgb::find($usulan_id);

        $usulan->status = $req->input('status');
        $usulan->save();
                
        logAction('Edit Data Usulan',json_encode($usulan),$usulan_id,Auth::user()->username);

        if($usulan->status == 20)
            $message = 'Data usulan berubah status menjadi Sedang verifikasi';
        else
            $message = 'Data usulan berubah status menjadi ditolak';

        return response()->json(['message' => $message,
                                        'error' => '',
                                        'usulan_id' => $usulan_id])->setStatusCode(200);  
    }
}
