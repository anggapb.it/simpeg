<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\Jabatan;
use App\Model\Eselon;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Model\UnitKerja;
use Validator;
use Input;
use DB;
use Redirect;
use Auth;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $satuan_kerja_id = $request->satuan_kerja_id;
        $satker = SatuanKerja::find($satuan_kerja_id);
        $jabatan = Jabatan::where('satuan_kerja_id',$satuan_kerja_id)->whereNull('unit_kerja_id')->where('jabatan_jenis',2)->first();
        return view('pages.jabatan.list',compact('satker','jabatan'));
    }

    public function indexJft(Request $request)
    {
        $jabatan = JabatanFungsional::where('is_deleted', false)->get();
        return view('pages.jft.list',compact('jabatan'));
    }

    public function indexJfu(Request $request)
    {
        $jabatan = JabatanFungsionalUmum::where('is_deleted', false)->get();
        return view('pages.jfu.list',compact('jabatan'));
    }

    public function create(Request $request){
            $satuan_kerja_id = $request->satuan_kerja_id;
            $satker = SatuanKerja::find($satuan_kerja_id);
            $unit_kerja_id = $request->unit_kerja_id;
            if($unit_kerja_id == "null"){
                $uker = null;
                $$unit_kerja_id = null;
            }elseif($unit_kerja_id != "null"){
                $uker = UnitKerja::find($unit_kerja_id);
            }
            $jenjab = $request->jenjab;
        return view('pages.jabatan.create',compact('satker','uker','jenjab','satuan_kerja_id','unit_kerja_id'));
    }

    public function createJft(Request $request){
        $rumpun = DB::connection('pgsql2')->table('m_spg_jabatan_rumpun')->lists('rumpun_nm','rumpun_id');
        $kategori = DB::connection('pgsql2')->table('m_spg_jabatan_kategori')->lists('jabkat_nm','jabkat_id');
        $jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->orderBy('nilai_jabatan','desc')->get();

        return view('pages.jft.create',compact('rumpun','kategori','jfkelas'));
    }

    public function createJfu(Request $request){
        $rumpun = DB::connection('pgsql2')->table('m_spg_jabatan_rumpun')->lists('rumpun_nm','rumpun_id');
        $kategori = DB::connection('pgsql2')->table('m_spg_jabatan_kategori')->lists('jabkat_nm','jabkat_id');
        $jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->whereRaw("kelas ~ '^\d+$'")->orderBy('nilai_jabatan','desc')->get();

        return view('pages.jfu.create',compact('rumpun','kategori','jfkelas'));
    }

    public function store(Request $request)
    {
        $jenjab = $request->jenjab;
        $uker = $request->uker == "null" ? null : $request->uker;
        if($jenjab == 2){
            $cek = Jabatan::where('satuan_kerja_id',$request->satker)->where('unit_kerja_id',$uker)->where('jabatan_jenis',2)->exists();
            if($cek){
                return Redirect::to('/data/jabatan/tambah?satuan_kerja_id='.$request->satker.'&unit_kerja_id='.($uker ? : 'null'))->with('message', 'Struktural Tidak Boleh Lebih dari 1');
            }
            $jabatan = new Jabatan;
            $jabatan->satuan_kerja_id = $request->satker;
            if($request->uker != "null"){
                $jabatan->unit_kerja_id = $request->uker;
            }
            $jabatan->eselon_id = empty($request->eselon) ? null : $request->eselon;
            $jabatan->jabatan_nama = $request->namajab;
            $jabatan->jabatan_jenis = $request->jenjab;
            $jabatan->jabatan_bup = $request->bup;
            $jabatan->kode_jabatan = $request->kodejab;
        }elseif($jenjab == 3){
            $jabatan = new Jabatan;
            $jabatan->satuan_kerja_id = $request->satker;
            if($request->uker != "null"){
                $jabatan->unit_kerja_id = $request->uker;
            }
            $jabatan->jf_id = $request->namajf;
            if (Jabatan::where('satuan_kerja_id',$jabatan->satuan_kerja_id)->where('jf_id',$jabatan->jf_id)
                ->where('unit_kerja_id',$jabatan->unit_kerja_id)->count() > 0) {
                return Redirect::back()->with('message', 'JFT Sudah Ada');
            }
            $jabatan->jabatan_jenis = $request->jenjab;
        }elseif ($jenjab == 4) {
            $jabatan = new Jabatan;
            $jabatan->satuan_kerja_id = $request->satker;
            if($request->uker != "null"){
                $jabatan->unit_kerja_id = $request->uker;
            }
            $jabatan->jfu_id = $request->namajfu;
            if (Jabatan::where('satuan_kerja_id',$jabatan->satuan_kerja_id)->where('jfu_id',$jabatan->jfu_id)
                ->where('unit_kerja_id',$jabatan->unit_kerja_id)->count() > 0) {
                return Redirect::back()->with('message', 'JFU Sudah Ada');
            }
            $jabatan->jabatan_jenis = $request->jenjab;
        }
        try {
            $jabatan->save();
        } catch (\Exception $e) {
            DB::connection('pgsql2')->statement("SELECT setval('\"public\".\"seqm_spg_jabatan\"', (select max(jabatan_id)+1 from public.m_spg_jabatan), true)");
            $jabatan->save();
        }
        logAction('Tambah Jabatan',json_encode($jabatan),$jabatan->jabatan_id,Auth::user()->username);
        return Redirect::to('/data/jabatan?satuan_kerja_id='.$request->satker)->with('message', 'Data Telah Ditambahkan');
    }

    public function storeJft(Request $request)
    {
        $lastid = JabatanFungsional::orderBy('jf_id','desc')->first();
        $jft = new JabatanFungsional;
        $jft->jf_id = $lastid->jf_id + 1;
        $jft->jf_nama = $request->namajab;
        $jft->rumpun_id = $request->rumpun;
        $jft->jabkat_id = $request->kategori;
        $jft->jf_gol_awal_id = $request->gol_awal;
        $jft->jf_gol_akhir_id = $request->gol_akhir;
        $jft->jf_bup = $request->bup;
        $jft->jf_skill = $request->skill;
        $jft->jf_kelas = $request->jabatan_kelas;
        $jft->kode_jabatan = $request->kojab;
        logAction('Tambah JFT',json_encode($jft),$jft->jf_id,Auth::user()->username);
        if($jft->save()){
            return Redirect::to('/data/jft')->with('message', 'Data Telah Ditambahkan');
        }
    }

    public function storeJfu(Request $request)
    {
        $lastid = JabatanFungsionalUmum::where('jfu_nama','<>','Pelaksana')->orderBy('jfu_id','desc')->first();
        $jft = new JabatanFungsionalUmum;
        $jft->jfu_id = $lastid->jfu_id + 1;
        $jft->jfu_nama = $request->namajab;
        $jft->jfu_pangkat_awal = $request->gol_awal;
        $jft->jfu_pangkat_puncak = $request->gol_akhir;
        $jft->jfu_bup = $request->bup;
        $jft->jfu_syarat = $request->syarat;
        $jft->jfu_kelas = $request->jabatan_kelas;
        $jft->kode_jabatan = $request->kojab;
        logAction('Tambah JFU',json_encode($jft),$jft->jfu_id,Auth::user()->username);
        if($jft->save()){
            return Redirect::to('/data/jfu')->with('message', 'Data Telah Ditambahkan');
        }
    }

    public function delete($id)
    {
        $uker = Jabatan::find($id);
        $satuan_kerja_id = $uker->satuan_kerja_id;
        logAction('Hapus Jabatan','Nama Jabatan : '.$uker->jabatan_nama,$id,Auth::user()->username);
        $uker->delete();
        return Redirect::to('/data/jabatan?satuan_kerja_id='.$satuan_kerja_id)->with('message', 'Data Telah Dihapus');
    }

    public function edit($id){
        $model = Jabatan::find($id);
        $eselon = Eselon::find($model->eselon_id);
        if(empty($model->unit_kerja_id)){
            $satker = SatuanKerja::find($model->satuan_kerja_id);
        }else{
            $uker = UnitKerja::find($model->unit_kerja_id);
        }

        return view('pages.jabatan.edit', compact('model','eselon','uker','satker'));
    }

    public function editJft($id){
        $model = JabatanFungsional::find($id);
        $rumpun = DB::connection('pgsql2')->table('m_spg_jabatan_rumpun')->lists('rumpun_nm','rumpun_id');
        $kategori = DB::connection('pgsql2')->table('m_spg_jabatan_kategori')->lists('jabkat_nm','jabkat_id');
        $jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->orderBy('nilai_jabatan','desc')->get();

        return view('pages.jft.edit', compact('model','rumpun','kategori','jfkelas'));
    }

    public function editJfu($id){
        $model = JabatanFungsionalUmum::find($id);
        $jfkelas = DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->whereRaw("kelas ~ '^\d+$'")->orderBy('nilai_jabatan','desc')->get();

        return view('pages.jfu.edit', compact('model','jfkelas'));
    }

    public function updateJft($id,Request $request)
    {
        $jft = JabatanFungsional::find($id);
        if(!$jft){
            return Redirect::to('/data/jft/edit/'.$id)->with('message', 'Data Tidak Ditemukan');
        }
        $jft->jf_nama = $request->namajab;
        $jft->rumpun_id = $request->rumpun;
        $jft->jabkat_id = $request->kategori;
        $jft->jf_gol_awal_id = $request->gol_awal;
        $jft->jf_gol_akhir_id = $request->gol_akhir;
        $jft->jf_bup = $request->bup;
        $jft->jf_skill = $request->skill;
        $jft->jf_kelas = $request->jabatan_kelas;
        $jft->kode_jabatan = $request->kojab;

        if($jft->save()){
            logAction('Edit JFT',json_encode($jft),$jft->jf_id,Auth::user()->username);

            return Redirect::to('/data/jft')->with('message', 'Data Telah Diupdate');
        }
    }

    public function updateJfu($id,Request $request)
    {
        $jft = JabatanFungsionalUmum::find($id);
        if(!$jft){
            return Redirect::to('/data/jfu/edit/'.$id)->with('message', 'Data Tidak Ditemukan');
        }
        $jft->jfu_nama = $request->namajab;
        $jft->jfu_pangkat_awal = $request->gol_awal;
        $jft->jfu_pangkat_puncak = $request->gol_akhir;
        $jft->jfu_bup = $request->bup;
        $jft->jfu_syarat = $request->syarat;
        $jft->jfu_kelas = $request->jabatan_kelas;
        $jft->kode_jabatan = $request->kojab;

        if($jft->save()){
            logAction('Edit JFU',json_encode($jft),$jft->jfu_id,Auth::user()->username);

            return Redirect::to('/data/jfu')->with('message', 'Data Telah Diupdate');
        }
    }

    public function deleteJft($id,Request $request)
    {
        $jft = JabatanFungsional::find($id);
        if(!$jft){
            return Redirect::to('/data/jft')->with('message', 'Data Tidak Ditemukan');
        }

        $jabatan = Jabatan::where('jf_id',$id)->lists('jabatan_id');
        $jumpeg = Pegawai::whereIn('jabatan_id',$jabatan)->where('peg_status',true)->count();

        if($jumpeg > 0){
            return Redirect::to('/data/jft')->with('message', 'Tidak Dapat Menghapus Data Karena Ada Pegawai Yang Terikat Dengan Jabatan Tersebut');
        }
        $jft->is_deleted = true;
        if($jft->save()){
            logAction('Hapus JFT','',$id,Auth::user()->username);
            return Redirect::to('/data/jft')->with('message', 'Data Telah Dihapus');
        }
    }

    public function deleteJfu($id,Request $request)
    {
        $jft = JabatanFungsionalUmum::find($id);
        if(!$jft){
            return Redirect::to('/data/jfu')->with('message', 'Data Tidak Ditemukan');
        }

        $jabatan = Jabatan::where('jfu_id',$id)->lists('jabatan_id');
        $jumpeg = Pegawai::whereIn('jabatan_id',$jabatan)->where('peg_status',true)->count();

        if($jumpeg > 0){
            return Redirect::to('/data/jfu')->with('message', 'Tidak Dapat Menghapus Data Karena Ada Pegawai Yang Terikat Dengan Jabatan Tersebut');
        }

        $jft->is_deleted = true;
        if($jft->save()){
            logAction('Hapus JFU','',$id,Auth::user()->username);
            return Redirect::to('/data/jfu')->with('message', 'Data Telah Dihapus');
        }
    }

    public function update($id,Request $request){
        $model = Jabatan::find($id);
        $model->jabatan_nama = $request->namajab;
        $model->eselon_id = empty($request->eselon_id) ? null : $request->eselon_id;
        $model->kode_jabatan = $request->kodejab;
        $model->jabatan_bup = $request->bup;
        $model->save();

        logAction('Edit Jabatan',json_encode($model),$model->jabatan_id,Auth::user()->username);
        // return Redirect::to('/data/jabatan?satuan_kerja_id='.$model->satuan_kerja_id)->with('message', 'Data Telah Ditambahkan');
        return Redirect::back()->with('message', 'Data Telah Ditambahkan');
    }

    public function getPegawai(Request $request)
    {
        $jabatan_id = $request->id;
        $data = Pegawai::where('jabatan_id',$jabatan_id)->where('peg_status', 'TRUE')->get();
        return response()->json($data);
    }

    public function getPegawaiJf(Request $request)
    {
        $jf_id = $request->jf_id;
        $jabatan = Jabatan::where('jf_id',$jf_id)->lists('jabatan_id');
        $data = Pegawai::leftJoin('m_spg_satuan_kerja','m_spg_satuan_kerja.satuan_kerja_id','=','spg_pegawai_simpeg.satuan_kerja_id')->whereIn('jabatan_id',$jabatan)->where('peg_status', 'TRUE')->get();
        return response()->json($data);
    }

    public function getPegawaiJfu(Request $request)
    {
        $jfu_id = $request->jfu_id;
        $jabatan = Jabatan::where('jfu_id',$jfu_id)->lists('jabatan_id');
        $data = Pegawai::leftJoin('m_spg_satuan_kerja','m_spg_satuan_kerja.satuan_kerja_id','=','spg_pegawai_simpeg.satuan_kerja_id')->whereIn('jabatan_id',$jabatan)->where('peg_status', 'TRUE')->get();
        return response()->json($data);
    }

    public function searchKojab(Request $request)
    {
        $jabkel = (int) $request->jabatan_kelas;

        if(strlen($jabkel) == 1){
            $jabkel = '0'.$jabkel;
        }

        $jft = JabatanFungsional::where('kode_jabatan','like','3.000.'.$jabkel.'%')->orderBy('kode_jabatan','desc')->first();

        if(!$jft){
            return '3.000.'.$jabkel.'.001';
        }

        $kojab = explode('.', $jft->kode_jabatan);
        $kojabs = (int) $kojab[3] + 1;

        if(strlen($kojabs) == 1){
            $kojabs = '00'.$kojabs;
        }elseif (strlen($kojabs) == 2) {
            $kojabs = '0'.$kojabs;
        }

        return '3.000.'.$jabkel.'.'.$kojabs;
    }

    public function searchKojabJfu(Request $request)
    {
        $jabkel = (int) $request->jabatan_kelas;

        if(strlen($jabkel) == 1){
            $jabkel = '0'.$jabkel;
        }

        $jft = JabatanFungsionalUmum::where('kode_jabatan','like','2.000.'.$jabkel.'%')->orderBy('kode_jabatan','desc')->first();

        if(!$jft){
            return '2.000.'.$jabkel.'.001';
        }

        $kojab = explode('.', $jft->kode_jabatan);
        $kojabs = (int) $kojab[3] + 1;

        if(strlen($kojabs) == 1){
            $kojabs = '00'.$kojabs;
        }elseif (strlen($kojabs) == 2) {
            $kojabs = '0'.$kojabs;
        }

        return '2.000.'.$jabkel.'.'.$kojabs;
    }
}
