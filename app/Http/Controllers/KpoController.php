<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use Redirect;

use App\Model\SatuanKerja;
use App\Model\Pegawai;
use App\Model\DaftarKpoPelaksana;

class KpoController extends Controller {

  public function __construct(){
		$this->middleware('auth');
	}

  public function indexKpoPelaksana($id1=null,$id2=null){
    if((Auth::user()->role_id != 1)&&(Auth::user()->role_id != 3)) {
      return Redirect::back();
    }    
    if ((!$id1) && (!$id2)){      
      $satker = SatuanKerja::where('satuan_kerja_id', $id1)->first();
      $periode = null;
      $daftarkpo = null;
      return view('pages.list_kpo_pelaksana', compact('satker', 'periode', 'daftarkpo'));
    }else{
      $satker = SatuanKerja::where('satuan_kerja_id', $id1)->first();
      $periode = ($id2 == 0) ? 'April' : 'Oktober';
      $mm = ($id2 ==0)? 4 : 10;
      $daftarkpo = DB::connection('pgsql2')->select("select tb1.*, tb2.nm_gol||', '||tb2.nm_pkt as golruang from
      (select * from
      (select
        peg_id,
        peg_nip,
        peg_gelar_depan,
        peg_nama,
        peg_gelar_belakang,
        gol_id_akhir,	
        peg_gol_akhir_tmt,
        peg_kerja_tahun,
        peg_kerja_bulan,
				kat_nama,
        make_date(
          date_part('year', tmt_pensi)::INT, 
          date_part('month', tmt_pensi)::INT,
          1
        ) as tmt_pensiun		
      from
        (select t1.*, t2.* from
          (SELECT
            peg_id,
            peg_nip,
            peg_gelar_depan,
            peg_nama,
            peg_gelar_belakang,
            gol_id_akhir,
            peg_gol_akhir_tmt,
            peg_kerja_tahun,
            peg_kerja_bulan,
            id_pend_akhir,			
            make_date(
                cast(COALESCE(substring(peg_nip from 1 for 4), '0') AS integer), 
                cast(COALESCE(substring(peg_nip from 5 for 2), '0') AS integer),
                1
            )+ INTERVAL '58 year 1 month' as tmt_pensi
          FROM
            spg_pegawai_simpeg
          WHERE
            peg_status = TRUE
          AND satuan_kerja_id = ".$id1."
          AND kedudukan_pegawai = 1
          AND id_status_kepegawaian not in (3,7)
          AND jabatan_id in (select jabatan_id from m_spg_jabatan where jabatan_jenis = 4)) t1
        JOIN
          (select * from tingpend) t2
        on t1.id_pend_akhir = t2.id_pend) tm1
      where gol_id_akhir < gol_puncak) as tam1 where tmt_pensiun > make_date (2019,".$mm.",1)	
      and peg_gol_akhir_tmt + interval '4 years' <= make_date (2019,".$mm.",1)) tb1
      left join
      (select gol_id, nm_gol, nm_pkt from m_spg_golongan) tb2
      on tb1.gol_id_akhir = tb2.gol_id order by 1");
      return view('pages.list_kpo_pelaksana', compact('satker', 'periode', 'daftarkpo'));
    }
  }   

  public function indexKpoSkpd(){
    if(Auth::user()->role_id != 2) {
      return Redirect::back();
    } 
    $id_satker = Auth::user()->satuan_kerja_id;
		$satker = SatuanKerja::where('satuan_kerja_id', $id_satker)->first();    
    $mm = 4;
    if (date("n") >= 8){
      $mm = 10;
    } else if (date("n") ==7){
      if (date("j") > 15){
        $mm = 10;
      }
    }
    $periode = ($mm == 4) ? 'April' : 'Oktober';
    $daftarkpo = DB::connection('pgsql2')->select("select tb1.*, tb2.nm_gol||', '||tb2.nm_pkt as golruang from
      (select * from
      (select
        peg_id,
        peg_nip,
        peg_gelar_depan,
        peg_nama,
        peg_gelar_belakang,
        gol_id_akhir,	
        peg_gol_akhir_tmt,
        peg_kerja_tahun,
        peg_kerja_bulan,
				kat_nama,
        make_date(
          date_part('year', tmt_pensi)::INT, 
          date_part('month', tmt_pensi)::INT,
          1
        ) as tmt_pensiun		
      from
        (select t1.*, t2.* from
          (SELECT
            peg_id,
            peg_nip,
            peg_gelar_depan,
            peg_nama,
            peg_gelar_belakang,
            gol_id_akhir,
            peg_gol_akhir_tmt,
            peg_kerja_tahun,
            peg_kerja_bulan,
            id_pend_akhir,			
            make_date(
                cast(COALESCE(substring(peg_nip from 1 for 4), '0') AS integer), 
                cast(COALESCE(substring(peg_nip from 5 for 2), '0') AS integer),
                1
            )+ INTERVAL '58 year 1 month' as tmt_pensi
          FROM
            spg_pegawai_simpeg
          WHERE
            peg_status = TRUE
          AND satuan_kerja_id = ".$id_satker."
          AND kedudukan_pegawai = 1
          AND id_status_kepegawaian not in (3,7)
          AND jabatan_id in (select jabatan_id from m_spg_jabatan where jabatan_jenis = 4)) t1
        JOIN
          (select * from tingpend) t2
        on t1.id_pend_akhir = t2.id_pend) tm1
      where gol_id_akhir < gol_puncak) as tam1 where tmt_pensiun > make_date (2019,".$mm.",1)	
      and peg_gol_akhir_tmt + interval '4 years' <= make_date (2019,".$mm.",1)) tb1
      left join
      (select gol_id, nm_gol, nm_pkt from m_spg_golongan) tb2
      on tb1.gol_id_akhir = tb2.gol_id order by 1");   
    return view('pages.list_kpo_pelaksana', compact('satker', 'periode', 'daftarkpo'));     
  }
}
