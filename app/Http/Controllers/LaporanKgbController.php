<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\StatusEditPegawai;
use App\Model\KGB;        
use App\Model\KGBExport;   
use App\Model\Gaji;
use App\Model\GajiTahun;
use App\Model\TTDKGB;
use App\Model\RiwayatPangkat;
use App\Model\UsulanKgb;
use App\Model\UsulanDetailKgb;
use App\Model\Golongan;     
use PDF;
use Excel;
use TC_PDF;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LaporanKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id='')
    {
        if(!$id)
            $id = Auth::user()->satuan_kerja_id;

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $title = 'KGB';
        $ttd_kgb = TTDKGB::select('spg_pegawai_simpeg.peg_id', 'spg_pegawai_simpeg.peg_nama','spg_pegawai_simpeg.peg_gelar_depan','spg_pegawai_simpeg.peg_gelar_belakang')->
        leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'm_ttd_kgb.peg_id_ttd')->orderBy('spg_pegawai_simpeg.peg_nama')->get();

        return view('pages.kgb.laporan_kgb', compact('satker', 'title','ttd_kgb'));
    }

    public function downloadPdf($id='') {
        if(!$id)
            $id = Auth::user()->satuan_kerja_id;

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        $satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->take(5)->get();
// var_dump($pegawai);die();
		if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				// return view('pages.kgb.index', compact('id', 'satker','pegawai'));
			}
		}else if(Auth::user()->role_id == 9){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai_induk = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE');
				$pegawai = Pegawai::whereRaw('unit_kerja_id in (select unit_kerja_id from m_spg_unit_kerja where unit_kerja_parent = '.Auth::user()->unit_kerja_id.')')->where('peg_status', 'TRUE')->union($pegawai_induk)->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				// return view('pages.kgb.index', compact('id', 'satker','pegawai'));
			}
		}
        
        $data = ['title' => 'Welcome to belajarphp.net',
                'satker' => $satker, 'pegawai' => $pegawai];
 
        $pdf = PDF::loadView('pages.kgb.pdf_kgb', $data)->setPaper('a4', 'landscape');
        // return $pdf->download('laporan-pdf.pdf');
        return $pdf->stream();
    }
    
    public function downloadExcel($type='xlsx') {
        $data = KGB::get()->toArray();
		return Excel::create('laporan KGB', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type); 
    }

    public function getPdf($periode_awal,$periode_akhir,$satker,$uker, $gol, $ttd)
    {   
        $decrypt_per_awal = base64_decode($periode_awal);
        $decrypt_per_akhir = base64_decode($periode_akhir);
        $thnbln_awal = date("Y-m", strtotime($decrypt_per_awal));    
        $thnbln_akhir = date("Y-m", strtotime($decrypt_per_akhir));    
        $decrypt_gol = base64_decode($gol);
        $decrypt_ttd = base64_decode($ttd);
        $arr_gol = explode("," , $decrypt_gol);
        TC_PDF::SetPrintHeader(false);
        TC_PDF::AddPage('L','A4');
        TC_PDF::SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		TC_PDF::SetFont('helvetica', 'B', 14);
        $x=0;$y=10;
		TC_PDF::writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
        TC_PDF::Cell(0, 0, 'BERITA ACARA LIST KGB', 0, 1, 'C', 0, '', 0);
        
        TC_PDF::SetFont('helvetica', '', 12);
        TC_PDF::Cell(0, 0, 'Periode : '.$decrypt_per_awal.' s/d '.$decrypt_per_akhir, 0, 1, 'C', 0, '', 0);
        $isi = '';  
        $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
        $list_proses_kgb = KGB::select('spg_pegawai_simpeg.*', 
                                    DB::raw("EXTRACT(year FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as tahun_lama_kerja"),
                                    DB::raw("EXTRACT(month FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as bulan_lama_kerja"),
                                    DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),  
                                    DB::raw("(SELECT gaji_pokok FROM m_spg_gaji 
                                    where mgaji_id = $gaji_thn->mgaji_id AND gol_id = spg_kgb.gol_id AND gaji_masakerja = spg_pegawai_simpeg.peg_kerja_tahun) as gaji_pokok"),     
                                    'm_spg_satuan_kerja.satuan_kerja_nama','m_spg_unit_kerja.unit_kerja_nama',
                                    'gol.nm_gol', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama', 'spg_kgb.kgb_status',
                                    'spg_kgb.kgb_id','spg_kgb.gol_id','spg_kgb.gaji_id','spg_kgb.kgb_nosk','spg_kgb.kgb_tglsk',
                                    'spg_kgb.kgb_tmt','spg_kgb.kgb_status_peg','spg_kgb.kgb_thn','spg_kgb.kgb_bln','spg_kgb.kgb_gapok',
                                    'spg_kgb.kgb_tglsurat','spg_kgb.kgb_nosurat','spg_kgb.kgb_status','spg_kgb.user_id',
                                    'spg_kgb.kgb_kerja_tahun','spg_kgb.kgb_kerja_bulan', 'spg_kgb.kgb_status_pdf',
                                    'spg_pegawai_simpeg.peg_kerja_tahun','spg_pegawai_simpeg.peg_kerja_bulan', 'su.no_usulan')->
                                    leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_kgb.peg_id')->
                                  //  leftJoin('m_spg_gaji as gj', 'gj.gaji_masakerja', '=','spg_pegawai_simpeg.peg_kerja_tahun','and','gj.gol_id', '=','spg_kgb.gol_id', 'and', 'gj.mgaji_id = \''.$gaji_thn->mgaji_id.'\'')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_unit_kerja', 'm_spg_unit_kerja.unit_kerja_id', '=', 'spg_pegawai_simpeg.unit_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    leftJoin('spg_usulan_detail as sud', 'sud.peg_id', '=','spg_kgb.peg_id')->
                                    leftJoin('spg_usulan as su', 'su.usulan_id', '=','sud.usulan_id')->
                                   // leftJoin('m_ttd_kgb as ttd',  "spg_pegawai_simpeg.gol_id_akhir", ">=", "ttd.gol_awal_id", "and" ,"spg_pegawai_simpeg.gol_id_akhir" ,"<=", "ttd.gol_akhir_id")->
                                    whereRaw('concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) >= \''.$thnbln_awal.'\'
                                    AND concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) <= \''.$thnbln_akhir.'\'')->
                                    where('status_data', '<>', 'manual');
        
        if(!empty($search_ttd)) {
            $list_proses_kgb = $list_proses_kgb->where('m_ttd_kgb.peg_id_ttd',$search_ttd);
        }
        if($satker != 'all'){
            $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.satuan_kerja_id',$satker);
        }

        if($uker != 'all') {
            $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.unit_kerja_id',$uker);
        } 
        if($gol != 'all') {
            $list_proses_kgb = $list_proses_kgb->whereIn('spg_pegawai_simpeg.gol_id_akhir',$arr_gol);
        } 

        $list_proses_kgb = $list_proses_kgb->orderBy('spg_kgb.kgb_id', 'asc')->get();    
        // print_r($list_proses_kgb);
        // die;                       
        
        foreach($list_proses_kgb AS $i=>$val)
        {
            if($val->jf_nama){
                $jabatan = $val->jf_nama;
            } else if($val->jfu_nama){
                $jabatan = $val->jfu_nama;
            }else{
                $jabatan = $val->jabatan_nama;
            }
            $isi .= "<tr>
					<td align=\"center\">". ($i+1) ."</td>
					<td>". $val->peg_nip ."</td>
					<td>". $val->peg_nama ."</td>
					<td>". $val->satuan_kerja_nama ."</td>
					<td>". $val->unit_kerja_nama ."</td>
					<td>". $jabatan ."</td>
					<td align=\"center\">". $val->nm_gol ."</td>
					<td>". date_format(date_create($val->peg_tmt_kgb), 'd-m-Y') ."</td>
					<td>". date_format(date_create($val->tgl_kenaikan), 'd-m-Y') ."</td>
					<td align=\"center\">". $val->kgb_kerja_tahun ."</td>
					<td align=\"center\">". $val->kgb_kerja_bulan ."</td>
					<td align=\"right\">". number_format(($val->kgb_gapok),0,',','.') ."</td>
					<td align=\"right\">". number_format(($val->gaji_pokok),0,',','.') ."</td>
			</tr>";
        }             
        
        $html = "<br/><br/><font size=\"7.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
		
				<tr align=\"center\">
					<td width=\"15\" rowspan=\"2\"><b>NO</b></td>
					<td width=\"78\" rowspan=\"2\"><b>NIP</b></td>
					<td width=\"100\" rowspan=\"2\"><b>NAMA</b></td>
					<td rowspan=\"2\"><b>SATUAN KERJA</b></td>
					<td rowspan=\"2\"><b>UNIT KERJA</b></td>
					<td rowspan=\"2\"><b>JABATAN</b></td>
					<td rowspan=\"2\"><b>GOLONGAN</b></td>
					<td colspan=\"2\"><b>KGB</b></td>
					<td colspan=\"2\"><b>MASA KERJA</b></td>
					<td colspan=\"2\"><b>GAJI POKOK</b></td>
                </tr>
                <tr align=\"center\">
                    <td>Terakhir</td>
                    <td>Selanjutnya</td>
                    <td>THN</td>
                    <td>BLN</td>
                    <td>Lama</td>
                    <td>Baru</td>
                </tr>
                ". $isi ."
				
			</table></font>
		";
		TC_PDF::writeHTML($html,true,false,false,false);
		TC_PDF::SetTitle('Berita Acara List KGB');
        TC_PDF::Output('berita_acara_list_kgb.pdf');
    }
}
