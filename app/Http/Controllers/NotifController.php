<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;

class NotifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function prediksi()
    {
        $url = url('notifikasi/get-prediksi');
        $columns = [
            'peg_nama' => ['Nama','peg_nama'],
            'peg_nip' => ['NIP','peg_nip'],
            'peg_lahir_tanggal' => ['Tanggal Lahir','peg_lahir_tanggal | formatDate'],
            'satuan_kerja_nama' => ['Satuan Kerja','satuan_kerja_nama'],
            'unit_kerja_nama' => ['Unit Kerja','unit_kerja_nama'],
            'nm_gol' => ['Golongan','nm_gol'],
            'eselon_nm' => ['Eselon','eselon_nm'],
            'jabatan_nama' => ['Jabatan','jabatan_nama'],
        ];
        return view('pages.notifikasi.prediksi',compact('url','columns'));
    }

    public function lebih()
    {
        $url = url('notifikasi/get-lebih');
        $columns = [
            'peg_nama' => ['Nama','peg_nama'],
            'peg_nip' => ['NIP','peg_nip'],
            'peg_lahir_tanggal' => ['Tanggal Lahir','peg_lahir_tanggal | formatDate'],
            'satuan_kerja_nama' => ['Satuan Kerja','satuan_kerja_nama'],
            'unit_kerja_nama' => ['Unit Kerja','unit_kerja_nama'],
            'nm_gol' => ['Golongan','nm_gol'],
            'eselon_nm' => ['Eselon','eselon_nm'],
            'jabatan_nama' => ['Jabatan','jabatan_nama'],
        ];
        return view('pages.notifikasi.lebih',compact('url','columns'));
    }

    public function prediksiPangkat()
    {
        $url = url('notifikasi/get-prediksipangkat');
        $columns = [
            'peg_nama' => ['Nama','peg_nama'],
            'peg_nip' => ['NIP','peg_nip'],
            'satuan_kerja_nama' => ['Satuan Kerja','satuan_kerja_nama'],
            'unit_kerja_nama' => ['Unit Kerja','unit_kerja_nama'],
            'nm_gol' => ['Golongan','nm_gol'],
            'peg_gol_akhir_tmt' => ['TMT Golongan Akhir','peg_gol_akhir_tmt'],
            'jabatan_nama' => ['Jabatan','jfu_nama'],
        ];
        return view('pages.notifikasi.lebihpangkat',compact('url','columns'));
    }

    public function getPrediksi(Request $request) {
        $models = Pegawai::select('*')->whereRaw('EXTRACT(YEAR from AGE(peg_lahir_tanggal)) = (peg_umur_pensiun - 1)')->where('peg_status',true)->leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_pegawai.jabatan_id')
                ->leftJoin('m_spg_referensi_jf','m_spg_jabatan.jf_id','=','m_spg_referensi_jf.jf_id')
                ->leftJoin('m_spg_eselon','m_spg_jabatan.eselon_id','=','m_spg_eselon.eselon_id')
                ->leftJoin('m_spg_referensi_jfu','m_spg_jabatan.jfu_id','=','m_spg_referensi_jfu.jfu_id')
                ->leftJoin('m_spg_golongan','spg_pegawai.gol_id_akhir','=','m_spg_golongan.gol_id')
                ->leftJoin('m_spg_satuan_kerja','spg_pegawai.satuan_kerja_id','=','m_spg_satuan_kerja.satuan_kerja_id')
                ->leftJoin('m_spg_unit_kerja','spg_pegawai.unit_kerja_id','=','m_spg_unit_kerja.unit_kerja_id');

        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('peg_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('peg_nip', 'ilike', '%'.$search.'%')
                    ->orWhere('unit_kerja_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('eselon_nm', 'ilike', '%'.$search.'%')
                    ->orWhere('nm_gol', 'ilike', '%'.$search.'%')
                    ->orWhere('jabatan_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('satuan_kerja_nama', 'ilike', '%'.$search.'%');
            });
        }

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    case 'satuan_kerja_id':
                        $models = $models->where('m_spg_jabatan.satuan_kerja_id',$search);
                        break;
                    default:
                        break;
                }
            }
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = count($models->get());
        $models = $models->orderBy('peg_nama', 'asc')->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function getLebih(Request $request) {
        $models = Pegawai::select('*')->whereRaw('EXTRACT(YEAR from AGE(peg_lahir_tanggal)) > peg_umur_pensiun')->leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_pegawai.jabatan_id')
                ->leftJoin('m_spg_referensi_jf','m_spg_jabatan.jf_id','=','m_spg_referensi_jf.jf_id')
                ->leftJoin('m_spg_eselon','m_spg_jabatan.eselon_id','=','m_spg_eselon.eselon_id')
                ->leftJoin('m_spg_referensi_jfu','m_spg_jabatan.jfu_id','=','m_spg_referensi_jfu.jfu_id')
                ->leftJoin('m_spg_golongan','spg_pegawai.gol_id_akhir','=','m_spg_golongan.gol_id')
                ->leftJoin('m_spg_satuan_kerja','spg_pegawai.satuan_kerja_id','=','m_spg_satuan_kerja.satuan_kerja_id')
                ->leftJoin('m_spg_unit_kerja','spg_pegawai.unit_kerja_id','=','m_spg_unit_kerja.unit_kerja_id')->where('peg_status',true);

        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('peg_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('peg_nip', 'ilike', '%'.$search.'%')
                    ->orWhere('unit_kerja_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('eselon_nm', 'ilike', '%'.$search.'%')
                    ->orWhere('nm_gol', 'ilike', '%'.$search.'%')
                    ->orWhere('jabatan_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('satuan_kerja_nama', 'ilike', '%'.$search.'%');
            });
        }

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = count($models->get());
        $models = $models->orderBy('peg_nama', 'asc')->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function getPrediksiPangkat(Request $request) {
        $models = Pegawai::select('*')->whereRaw("round(( ( ( EXTRACT ( 'years' FROM AGE(peg_gol_akhir_tmt)) * 12 ) + ( EXTRACT ( 'month' FROM AGE(peg_gol_akhir_tmt) ) ) + ( EXTRACT ( 'day' FROM AGE(peg_gol_akhir_tmt) ) / 30 ) ) / 12 ) :: NUMERIC, 1 ) > 3.5")->where('jabatan_jenis',4)->where('peg_status',true)->leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_pegawai.jabatan_id')
                ->leftJoin('m_spg_eselon','m_spg_jabatan.eselon_id','=','m_spg_eselon.eselon_id')
                ->leftJoin('m_spg_referensi_jfu','m_spg_jabatan.jfu_id','=','m_spg_referensi_jfu.jfu_id')
                ->leftJoin('m_spg_golongan','spg_pegawai.gol_id_akhir','=','m_spg_golongan.gol_id')
                ->leftJoin('m_spg_satuan_kerja','spg_pegawai.satuan_kerja_id','=','m_spg_satuan_kerja.satuan_kerja_id')
                ->leftJoin('m_spg_unit_kerja','spg_pegawai.unit_kerja_id','=','m_spg_unit_kerja.unit_kerja_id');

        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('peg_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('peg_nip', 'ilike', '%'.$search.'%')
                    ->orWhere('unit_kerja_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('eselon_nm', 'ilike', '%'.$search.'%')
                    ->orWhere('nm_gol', 'ilike', '%'.$search.'%')
                    ->orWhere('jabatan_nama', 'ilike', '%'.$search.'%')
                    ->orWhere('satuan_kerja_nama', 'ilike', '%'.$search.'%');
            });
        }

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = count($models->get());
        $models = $models->orderBy('peg_nama', 'asc')->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }
}
