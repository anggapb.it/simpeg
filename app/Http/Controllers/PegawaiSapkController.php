<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Auth;
use Redirect;
use Validator;
use Excel;
use DB;
use DateTime;

use App\Model\Pegawai;
use App\Model\Golongan;
use App\Model\RiwayatPangkat;
use App\Model\RiwayatPangkat2;

class PegawaiSapkController extends Controller
{

    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('bkd');

	}

    public function import(PegawaiSapkImport $import){
        $import->noHeading();
        $import->ignoreEmpty();
        $results = $import->toArray();

        $i=1;
        $messages = "";
        $processed = 0;
        $processed_fail = 0;
        for($i=1; $i<count($results); $i++){
            $processed = $i;
        }
        $messages .= "$processed data telah diproses";
        return Redirect::back()->with('message',$messages);
    }

    public function rekonUpload(){
		return view('pages.rekon-sapk.upload');
    }
    
    public function updateKpKolektif(){
        return view('pages.rekon-sapk.update_kolektif');
    }

	public function rekonUploadPost(PegawaiSapkImport $import){
		$import->noHeading();
		$import->ignoreEmpty();
		$results = $import->toArray();

		$messages = "";
		$processed = 0;
		$processed_fail = 0;
		DB::statement('create table spg_pegawai_sapk'.date("YmdHis").' as select * from spg_pegawai_sapk');
		DB::statement('truncate table spg_pegawai_sapk');
		for($i=1; $i<count($results); $i++){
			$model = new PegawaiSapk();
			$model->peg_nip = $results[$i][2];
			$model->peg_nama = $results[$i][4];
			$model->peg_gelar_depan = $results[$i][5];
			$model->peg_gelar_belakang = $results[$i][6];
			$model->peg_lahir_tempat = $results[$i][8];
			$model->peg_lahir_tanggal = $results[$i][9];
			$model->kd_gol_akhir = $results[$i][35];
			$model->peg_gol_akhir_tmt = $results[$i][37];
			$model->peg_kerja_tahun = $results[$i][38];
			$model->peg_kerja_bulan = $results[$i][39];
			$model->save();
			$processed++;
		}
		$messages .= "$processed_fail data gagal diproses, ";
		$messages .= "$processed data telah diproses";
		//return view('pages.rekon-sapk.upload-post');
		return Redirect::back()->with('message',$messages);
    }

    public function updateKpKolektifPost(PegawaiSapkImport $import, Request $req){
		// tinggal loging. klo bisa ke table
		$import->noHeading();
		$import->ignoreEmpty();
		$results = $import->toArray();
		$updatepokok = $req->input('update_pokok');
		$updateriwayat = $req->input('riwayat_kp');

        $messages = "";
        $total_data = 0;
		$updated = 0;
		$sk_belum_cetak = 0;
		$tms = 0;
		$btl = 0;
		$belum_diproses = 0;
		$status_lain = 0;
		$nip_notfound = 0;
		$nama_table = "update_kp_".date("YmdHis");
		DB::connection("pgsql2")->statement("create table ".$nama_table." as select * from update_kp_template");
		DB::connection("pgsql2")->statement("truncate table ".$nama_table);

		for($i=1; $i<count($results); $i++){
			$kondisi = $results[$i][9];
			$no_sk = $results[$i][7];
			if ($kondisi == "NP" || $kondisi == "Proses KP Selesai"){
				if($no_sk != ""){
					$tgl_sk = $results[$i][8];
					$nip = $results[$i][18];
					$gol = $results[$i][28];
					$tmt_gol = $results[$i][29];
					$mk_thn = $results[$i][30];
					$mk_bln = $results[$i][31];
					$unit_kerja = $results[$i][36];
					$pejabat = "Wali Kota Bandung";
					if ($gol == "IV/c" || $gol == "IV/d" || $gol == "IV/e") {
						$pejabat = "Presiden Republik Indonesia";
					} else if ($gol == "IV/a" || $gol == "IV/b") {
						$pejabat = "Gubernur Jawa Barat";
					}
					$pegawai = Pegawai::where('peg_nip', $nip)->first();					
					if ($pegawai){
						$golongan = Golongan::where('nm_gol', $gol)->first();
						$parsedTmtGol = explode("-", $tmt_gol);
						$parsedTglSk = explode("-", $tgl_sk);
						if ($updatepokok){
							$sql = "update spg_pegawai set gol_id_akhir = ".$golongan->gol_id.
							", peg_gol_akhir_tmt = '".trim($parsedTmtGol[1])."/".trim($parsedTmtGol[0])."/".trim($parsedTmtGol[2]).
							"', peg_kerja_tahun = '".$mk_thn."', peg_kerja_bulan = '".$mk_bln."' where peg_id = ".$pegawai->peg_id;
							$sql_simpeg = "update spg_pegawai_simpeg set gol_id_akhir = ".$golongan->gol_id.
							", peg_gol_akhir_tmt = '".trim($parsedTmtGol[1])."/".trim($parsedTmtGol[0])."/".trim($parsedTmtGol[2]).
							"', peg_kerja_tahun = '".$mk_thn."', peg_kerja_bulan = '".$mk_bln."' where peg_id = ".$pegawai->peg_id;
							DB::connection('pgsql2')->statement($sql);
							DB::connection('pgsql2')->statement($sql_simpeg);
							DB::connection('pgsql')->statement($sql);							
						}						
						$riwayatKp = RiwayatPangkat::where('peg_id', $pegawai->peg_id)->where('gol_id', $golongan->gol_id)->first();
						if($riwayatKp){
							if ($updateriwayat == "1") {
								$tanggalTmt = trim($parsedTmtGol[2])."/".trim($parsedTmtGol[1])."/".trim($parsedTmtGol[0]);
								$tanggalSk = trim($parsedTglSk[2])."/".trim($parsedTglSk[1])."/".trim($parsedTglSk[0]);
								$riwayatKp->riw_pangkat_tmt = new DateTime($tanggalTmt);
								$riwayatKp->riw_pangkat_sk = $no_sk;
								$riwayatKp->riw_pangkat_sktgl = new DateTime($tanggalSk);
								$riwayatKp->riw_pangkat_thn = $mk_thn;
								$riwayatKp->riw_pangkat_bln = $mk_bln;
								$riwayatKp->gol_id = $golongan->gol_id;
								$riwayatKp->peg_id = $pegawai->peg_id;
								$riwayatKp->riw_pangkat_pejabat = $pejabat;
								$riwayatKp->riw_pangkat_unit_kerja = $unit_kerja;																
								$riwayatKp->save();

								$riwayat_copy = RiwayatPangkat2::where('peg_id', $pegawai->peg_id)->where('gol_id', $golongan->gol_id)->first();
								if ($riwayat_copy){
									$riwayat_copy->riw_pangkat_tmt = new DateTime($tanggalTmt);
									$riwayat_copy->riw_pangkat_sk = $no_sk;
									$riwayat_copy->riw_pangkat_sktgl = new DateTime($tanggalSk);
									$riwayat_copy->riw_pangkat_thn = $mk_thn;
									$riwayat_copy->riw_pangkat_bln = $mk_bln;
									$riwayat_copy->gol_id = $golongan->gol_id;
									$riwayat_copy->peg_id = $pegawai->peg_id;
									$riwayat_copy->riw_pangkat_pejabat = $pejabat;
									$riwayat_copy->riw_pangkat_unit_kerja = $unit_kerja;								
									$riwayat_copy->save();
								}								
							}
						} else {							
							$rkp = new RiwayatPangkat();
							$tanggalTmt = trim($parsedTmtGol[2])."/".trim($parsedTmtGol[1])."/".trim($parsedTmtGol[0]);
							$tanggalSk = trim($parsedTglSk[2])."/".trim($parsedTglSk[1])."/".trim($parsedTglSk[0]);
							$rkp->riw_pangkat_tmt = new DateTime($tanggalTmt);
							$rkp->riw_pangkat_sk = $no_sk;
							$rkp->riw_pangkat_sktgl = new DateTime($tanggalSk);
							$rkp->riw_pangkat_thn = $mk_thn;
							$rkp->riw_pangkat_bln = $mk_bln;
							$rkp->gol_id = $golongan->gol_id;
							$rkp->peg_id = $pegawai->peg_id;
							$rkp->riw_pangkat_pejabat = $pejabat;
							$rkp->riw_pangkat_unit_kerja = $unit_kerja;							
							if($rkp->save()){								
								$rkp_copy = new RiwayatPangkat2();
								$rkp_copy->riw_pangkat_id = $rkp->riw_pangkat_id;
								$rkp_copy->riw_pangkat_tmt = new DateTime($tanggalTmt);
								$rkp_copy->riw_pangkat_sk = $no_sk;
								$rkp_copy->riw_pangkat_sktgl = new DateTime($tanggalSk);
								$rkp_copy->riw_pangkat_thn = $mk_thn;
								$rkp_copy->riw_pangkat_bln = $mk_bln;
								$rkp_copy->gol_id = $golongan->gol_id;
								$rkp_copy->peg_id = $pegawai->peg_id;
								$rkp_copy->riw_pangkat_pejabat = $pejabat;
								$rkp_copy->riw_pangkat_unit_kerja = $unit_kerja;																
								$rkp_copy->save();
							}							
						}						
						$updated++;
					} else {
						$nip_notfound++;
						$kondisi = "NIP Not Found";
					}
				}else{
					$sk_belum_cetak++;
					$kondisi = "Belum Cetak SK";
				}					
			} else if ($kondisi == "TMS") {
				$tms++;
			} else if ($kondisi == "BTL" || $kondisi == "Batal Berkas"){
				$btl++;
			} else if ($kondisi == "Belum Diproses"){
				$belum_diproses++;
			} else {
				$status_lain++;
			}
			// insert ke log table here
			$sql_log = "insert into ".$nama_table." values ('".$no_sk."', '".
						$results[$i][8]."', '".$kondisi."', '".$results[$i][18]."', '".
						$results[$i][28]."', '".$results[$i][29]."', ".$results[$i][30].", ".
						$results[$i][31].", '".$results[$i][36]."')";							
			DB::connection('pgsql2')->statement($sql_log);
			$total_data = $i;					
		}		
		
		$messages .= "Total Data = $total_data. Diupdate = $updated. ";
		if($sk_belum_cetak > 0) $messages .= " Belum Cetak SK = $sk_belum_cetak. ";
		if($tms > 0) $messages .= "Data TMS = $tms. ";
		if($btl > 0) $messages .= "Data BTL = $btl. ";
		if($belum_diproses > 0) $messages .= "Belum Diproses BKN = $belum_diproses. ";
		if($nip_notfound > 0) $messages .= "NIP Tidak Ditemukan pada SIMPEG = $nip_notfound. ";
		if($status_lain > 0) $messages .= "Statusnya Tidak Diketahui = $status_lain. ";
		return Redirect::back()->with('message',$messages);
    }
}
?>
