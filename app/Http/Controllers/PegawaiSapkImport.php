<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use File;

class PegawaiSapkImport extends \Maatwebsite\Excel\Files\ExcelFile {

    public function getFile(){
       // Import a user provided file
        $file = Input::file('excelfile');
        $destinationPath = 'uploads'; // upload path
        $extension = $file->getClientOriginalExtension(); // getting file extension
        $filename = $file->getClientOriginalName(); // getting file extension

        while (File::exists("uploads/$filename")) {
            $extension_pos = strrpos($filename, '.');
            $filename = substr($filename, 0, $extension_pos) . date("YmdHis") . substr($filename, $extension_pos);
        }

        $upload_success = $file->move($destinationPath, $filename); // uploading file to given path

        if($upload_success){
            return "uploads/".$filename;
        }
    }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }

}

?>