<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\FilePegawai;
use App\Model\MasterPengajuan;
use App\Model\Pegawai;
use App\Model\Pengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengajuanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();
        $file = FilePegawai::where('peg_id', $pegawai->peg_id)->lists('file_nama', 'file_id');
        $pengajuan = MasterPengajuan::lists('nama_pengajuan', 'nama_pengajuan');

        return view('pages.pengajuan.create', compact('file', 'pengajuan'));
    }

    public function store(Request $request)
    {
        $error = $this->validate($request, [
                'nama_pengajuan' => 'required',
                'lampiran' => 'required',
            ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
                ->withErrors($message);
        }

        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();

        $model = new Pengajuan;
        $model->nama_pengajuan = $request->nama_pengajuan;
        $model->status = 'belum verifikasi';
        $model->peg_id = $pegawai->peg_id;
        $model->lampiran = $request->lampiran;
        $model->tanggal_pengajuan = date('Y-m-d');
        $model->keterangan = '';
        $model->save();

        logAction('Tambah Pengajuan Pegawai','Nama Pengajuan : '.$request->nama_pengajuan,$model->pengajuan_id,Auth::user()->username);
        return redirect()->back()
            ->with('message', 'Pengajuan Telah Dikirim, Tunggu Verifikasi BKD');
    }

    public function edit($id)
    {
        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();
        $file = FilePegawai::where('peg_id', $pegawai->peg_id)->lists('file_nama', 'file_id');
        $pengajuan = MasterPengajuan::lists('nama_pengajuan', 'nama_pengajuan');

        $data = Pengajuan::find($id);

        return view('pages.pengajuan.edit', compact('file', 'pengajuan', 'data'));
    }

    public function update(Request $request)
    {
        $error = $this->validate($request, [
                'nama_pengajuan' => 'required',
                'lampiran' => 'required',
            ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
                ->withErrors($message);
        }

        $model = Pengajuan::find($request->pengajuan_id);
        $model->nama_pengajuan = $request->nama_pengajuan;
        $model->lampiran = $request->lampiran;
        $model->save();
        logAction('Edit Pengajuan Pegawai','Nama Pengajuan : '.$request->nama_pengajuan,$model->pengajuan_id,Auth::user()->username);
        return redirect()->back()
            ->with('message', 'Pengajuan Telah Diubah');
    }

    public function pengajuanSaya()
    {
        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();
        $pengajuan = Pengajuan::where('peg_id', $pegawai->peg_id)->get();

        $columns = [
            'nama_pengajuan' => ['Pengajuan','nama_pengajuan'],
            'status' => ['Status','status'],
            'keterangan' => ['Keterangan','keterangan'],
        ];

        return view('pages.pengajuan.pengajuan_saya', compact('pengajuan', 'columns'));
    }

    public function daftarPengajuan()
    {
        $pengajuan = Pengajuan::where('is_batal', 0)->get();

        $columns = [
            'tanggal_pengajuan' => ['Tanggal','tanggal_pengajuan'],
            'peg_nip' => ['NIP','peg_nip'],
            'peg_nama' => ['Nama','peg_nama'],
            'nama_pengajuan' => ['Pengajuan','nama_pengajuan'],
            'status' => ['Status','status'],
        ];

        return view('pages.pengajuan.daftar_pengajuan', compact('pengajuan', 'columns'));
    }

    public function detailPengajuan($id)
    {
        $data = Pengajuan::where('pengajuan_id', $id)->first();
        $lampiran = FilePegawai::where('peg_id', $data->peg_id)->get();

        return view('pages.pengajuan.detail_pengajuan', compact('lampiran', 'data'));
    }

    public function batal($id)
    {
        $model = Pengajuan::find($id);
        $model->keterangan = 'Pengajuan telah dibatalkan';
        $model->is_batal = 1;
        $model->save();
        logAction('Batal Pengajuan Pegawai','Nama Pengajuan : '.$model->nama_pengajuan,$model->pengajuan_id,Auth::user()->username);
        return redirect()->back()
            ->with('message', 'Pengajuan Telah Dibatalkan');
    }

    public function ubahStatus($id, Request $request)
    {
        $model = Pengajuan::find($id);
        $model->keterangan = $request->keterangan;
        $model->status = $request->status;
        $model->save();
        logAction('Ubah Status Pengajuan Pegawai','Nama Pengajuan : '.$model->nama_pengajuan,$model->pengajuan_id,Auth::user()->username);
        return redirect()->back()
            ->with('message', 'Status Pengajuan Telah Diubah');
    }

    public function getDataRender(Request $request)
    {
        $models = Pengajuan::leftJoin('spg_pegawai', 'spg_pengajuan.peg_id', '=', 'spg_pegawai.peg_id');

        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search_pengajuan = $request->get('search_pengajuan',false);
        $order  = $request->get('order' ,false);

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        $models->where($key,'like','%'.$search.'%');
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->orWhere('nama_pengajuan', 'like', '%'.$search.'%')
                    ->orWhere('peg_nip', 'like', '%'.$search.'%')
                    ->orWhere('peg_nama', 'like', '%'.$search.'%')
                    ->orWhere('status', 'like', '%'.$search.'%');
            });
        }

        if($search_pengajuan != '') {
            $models = $models->where('nama_pengajuan', 'like', '%'.$search_pengajuan.'%');
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

        $count = $models->count();
         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case 'tanggal_pengajuan':
                    $models = $models->orderBy('tanggal_pengajuan',$order_direction);
                    break;
                case 'peg_nip':
                    $models = $models->orderBy('peg_nip',$order_direction);
                    break;
                case 'peg_nama':
                    $models = $models->orderBy('peg_nama',$order_direction);
                    break;
                case 'nama_pengajuan':
                    $models = $models->orderBy('nama_pengajuan',$order_direction);
                    break;
                case 'status':
                    $models = $models->orderBy('status',$order_direction);
                    break;
                default:
                    $models = $models->orderBy('pengajuan_id',$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as &$model) {
            $model->tanggal_pengajuan = getFullDate($model->tanggal_pengajuan);
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function getPengajuanSaya(Request $request)
    {
        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();
        $models = Pengajuan::where('peg_id', $pegawai->peg_id);

        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search_pengajuan = $request->get('search_pengajuan',false);
        $order  = $request->get('order' ,false);

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        $models->where($key,'like','%'.$search.'%');
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->orWhere('nama_pengajuan', 'like', '%'.$search.'%')
                    ->orWhere('status', 'like', '%'.$search.'%')
                    ->orWhere('keterangan', 'like', '%'.$search.'%');
            });
        }

        if($search_pengajuan != '') {
            $models = $models->where('nama_pengajuan', 'like', '%'.$search_pengajuan.'%');
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

        $count = $models->count();
         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case 'nama_pengajuan':
                    $models = $models->orderBy('nama_pengajuan',$order_direction);
                    break;
                case 'status':
                    $models = $models->orderBy('status',$order_direction);
                    break;
                case 'keterangan':
                    $models = $models->orderBy('keterangan',$order_direction);
                    break;
                default:
                    $models = $models->orderBy('pengajuan_id',$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as &$model) {
            $model->tanggal_pengajuan = getFullDate($model->tanggal_pengajuan);
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

}
