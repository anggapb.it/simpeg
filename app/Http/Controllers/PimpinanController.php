<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai2;
use App\Model\StatusEditPegawai;
use App\Model\StatusEditPegawaiLog;
use App\Model\LogEditPegawai;
use App\Model\Walikota;
use App\Model\Pegawai;
use App\Model\WalikotaPeriode;
use Validator;
use Input;
use Redirect;
use Auth;
use File;
use Excel;
use PHPExcel_Worksheet_PageSetup;

class PimpinanController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('bkd');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        if(Auth::user()->role_id != '1' && Auth::user()->role_id != '3'){
			return redirect('/');
		}
        $url = url('walikota/get-data');
        $columns = [
            'walikota_nama' => ['Nama','walikota.walikota_nama'],
            'tgl_mulai' => ['Tanggal Mulai Menjabat','tgl_mulai | formatDate'],
            'tgl_selesai' => ['Tanggal Berakhir Menjabat','tgl_selesai | formatDate'],
            'periode' => ['Periode','periode'],
            'jenis' => ['Jabatan','jenis']
        ];
		return view('pages.pimpinan.index',compact('url','columns'));
	}

    public function getData(Request $request) {
        $models = WalikotaPeriode::with(['walikota']);
        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = $models->count();
        $models = $models->orderBy('tgl_mulai', 'desc')->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
            if($value->tgl_selesai){
                $thn_mulai = date('Y', strtotime($value->tgl_mulai));
                $thn_selesai = date('Y', strtotime($value->tgl_selesai));
                $periode = $thn_mulai." s.d. ".$thn_selesai;
                $value['attributes'] = array_merge($value['attributes'], ['periode'=>$periode]);
            }else{
                $thn_mulai = date('Y', strtotime($value->tgl_mulai));
                $periode = $thn_mulai." s.d. ".($thn_mulai+5);
                $value['attributes'] = array_merge($value['attributes'], ['periode'=>$periode]);
               
            }
            if(strlen($value->id) == 1){
                $angka = '200000000';
            }elseif(strlen($value->id) > 1){
                $jumlah = 2;
                $sisa = 8 - ($jumlah - 1);
                $angka = '2';
                for ($i=1; $i <= $sisa; $i++) { 
                    $angka = $angka.'0';
                }
            }
            $pegawai = Pegawai::where('peg_id',$angka.$value->walikota_id)->first();
            $value['attributes'] = array_merge($value['attributes'],['peg_id'=>$pegawai->peg_id]);
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function add(){
        return view('pages.pimpinan.create');
    }

    public function edit($peg_id,$id){
        $pegawai = Pegawai::where('peg_id',$peg_id)->first();
        $model = WalikotaPeriode::where('walikota_id',$id)->first();
        return view('pages.pimpinan.edit-profile', compact('pegawai','model'));
    }

    public function store(Request $req){
        $model = $req->all();
        
        if($model['walikota'] != null && $model['walikota_new'] != null){
            return Redirect::back()
            ->withInput($model)
            ->with(array('error'=>'Terjadi kesalahan input data, Pilih salah satu, nama yang sudah ada atau buat baru','info'=>'danger'))
            ->withErrors($validation);
        }else if($model['walikota'] != null){
            $max2 = WalikotaPeriode::max('id');
            $data = new WalikotaPeriode;
            $data->id=$max2+1;
            $data->walikota_id = $model['walikota'];
            $data->tgl_mulai = $model['tgl_mulai'] ? $model['tgl_mulai'] : null;
            $data->tgl_selesai = $model['tgl_selesai'] ? $model['tgl_selesai']: null;
            $data->jenis = $model['jenis'];
            $data->save();
        }else if($model['walikota_new'] != null){
            $max = Walikota::max('walikota_id');
            $data = new Walikota;
            $data->walikota_id = $max+1;
            $data->walikota_nama = $model['walikota_new'];

            if($data->save()){
                $max2 = WalikotaPeriode::max('id');
                $p = new WalikotaPeriode;
                $p->id = $max2+1;
                $p->walikota_id = $data->walikota_id;
                $p->tgl_mulai = $model['tgl_mulai'] ? $model['tgl_mulai'] : null;
                $p->tgl_selesai = $model['tgl_selesai'] ? $model['tgl_selesai']: null;
                $p->jenis = $model['jenis'];
                $p->save();
            }
        }
        logAction('Tambah Walikota',json_encode($data),$data->id,Auth::user()->username);
        return Redirect::to('/walikota')->with('message', 'Data Telah Ditambahkan');
    }

    public function update($id, Request $req){
        $model = $req->all();

        $data = WalikotaPeriode::find($id);
        $data->tgl_mulai = $model['tgl_mulai'] ? $model['tgl_mulai'] : null;
        $data->tgl_selesai = $model['tgl_selesai'] ? $model['tgl_selesai']: null;
        $data->jenis = $model['jenis'];
        if($data->save()){
            $w = Walikota::find($data->walikota_id);
            $w->walikota_nama = $model['walikota_nama'];
            $w->save();
        }
        logAction('Edit Walikota',json_encode($data),$data->id,Auth::user()->username);
        return Redirect::to('/walikota')->with('message', 'Data Telah Diperbarui');
    }

    public function updates($peg_id,$id,Request $req)
    {
        $data = $req->except('_token');
        date_default_timezone_set("Asia/Bangkok");
        $time = date("Y-m-d");
        $pegawai = Pegawai2::find($peg_id);
        if($pegawai){
            if($pegawai->gol_id_akhir != $req->input('gol_id_akhir')){
                $rp=new RiwayatPangkat2;
                //$rp->riw_pangkat_id = rand();
                $rp->peg_id = $pegawai->peg_id;
                $rp->riw_pangkat_thn= $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : 0;
                $rp->riw_pangkat_bln =$req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') : 0;
                $rp->riw_pangkat_sk = $req->input('no_sk');
                $rp->riw_pangkat_sktgl =  saveDate($req->input('tanggal_sk'));
                $rp->riw_pangkat_tmt =  saveDate($req->input('peg_gol_akhir_tmt'));
                $rp->riw_pangkat_pejabat = $req->input('jabatan_penandatangan');
                $rp->riw_pangkat_unit_kerja = $req->input('unit_kerja_gol');
                $rp->gol_id = $req->input('gol_id_akhir') ? : null;
                if($rp->save()){
                    $pegawai = Pegawai2::find($rp->peg_id);
                    $log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
                        if($log){
                            $log->action ="add_riwayat_pangkat";
                            $log->status_id = 1;
                            $log->editor_id = Auth::user()->id;
                            $log->save();
                        }else{
                            $log = new StatusEditPegawai;
                            $log->peg_nip = $pegawai['peg_nip'];
                            $log->peg_id = $rp->peg_id;
                            $log->action ="add_riwayat_pangkat";
                            $log->status_id = 1;
                            $log->editor_id = Auth::user()->id;
                            $log->save();
                        }
                }
            }

            $pegawai->id_goldar = $req->input('id_goldar') ? $req->input('id_goldar') : null;
            $pegawai->gol_id_awal = $req->input('gol_id_awal') ? $req->input('gol_id_awal') : null;
            $pegawai->id_pend_awal = $req->input('id_pend_awal') ? $req->input('id_pend_awal') : null;
            $pegawai->kecamatan_id = $req->input('kecamatan_id') ?  $req->input('kecamatan_id') : null;
            $pegawai->gol_id_akhir = $req->input('gol_id_akhir') ?  $req->input('gol_id_akhir') : null;
            $unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

            $pegawai->id_pend_akhir = $req->input('id_pend_akhir') ? $req->input('id_pend_akhir') : null;
            $pegawai->id_agama = $req->input('id_agama') ? $req->input('id_agama') : null;
            $pegawai->peg_nip = $req->input('peg_nip') ? $req->input('peg_nip') : null;
            $pegawai->peg_nama = $req->input('peg_nama') ? $req->input('peg_nama') : null;
            $pegawai->peg_gelar_depan = $req->input('peg_gelar_depan') ? $req->input('peg_gelar_depan') : null;
            $pegawai->peg_gelar_belakang = $req->input('peg_gelar_belakang') ? $req->input('peg_gelar_belakang') : null;
            $pegawai->peg_lahir_tempat = $req->input('peg_lahir_tempat') ?  $req->input('peg_lahir_tempat') : null;
            $pegawai->peg_lahir_tanggal = saveDate($req->input('peg_lahir_tanggal'));
            $pegawai->peg_jenis_kelamin = $req->input('peg_jenis_kelamin') ? $req->input('peg_jenis_kelamin') : null;
            $pegawai->peg_status_perkawinan = $req->input('peg_status_perkawinan') ? $req->input('peg_status_perkawinan') : null;
            $pegawai->peg_karpeg = $req->input('peg_karpeg') ? $req->input('peg_karpeg') : null;
            $pegawai->peg_karsutri = $req->input('peg_karsutri');
            $pegawai->peg_status_kepegawaian = $req->input('peg_status_kepegawaian');
            $pegawai->peg_cpns_tmt =saveDate($req->input('peg_cpns_tmt')) ;
            $pegawai->peg_pns_tmt = saveDate($req->input('peg_pns_tmt'));
            $pegawai->peg_gol_awal_tmt = saveDate($req->input('peg_gol_awal_tmt'));
            $pegawai->peg_kerja_tahun = $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : 0;
            $pegawai->peg_kerja_bulan = $req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') :0;
            $pegawai->peg_no_askes = $req->input('peg_no_askes');
            $pegawai->peg_npwp = $req->input('peg_npwp');
            $pegawai->peg_bapertarum = $req->input('peg_bapertarum') ? $req->input('peg_bapertarum') : null;
            $pegawai->peg_rumah_alamat = $req->input('peg_rumah_alamat');
            $pegawai->peg_kel_desa = $req->input('peg_kel_desa');
            $pegawai->peg_kodepos = $req->input('peg_kodepos') ? $req->input('peg_kodepos') : null;
            $pegawai->peg_telp = $req->input('peg_telp') ? $req->input('peg_telp') : null;
            $pegawai->peg_telp_hp = $req->input('peg_telp_hp') ? $req->input('peg_telp_hp') : null;
            $pegawai->peg_tmt_kgb = saveDate($req->input('peg_tmt_kgb'));
            $pegawai->peg_pend_awal_th = $req->input('peg_pend_awal_th');
            $pegawai->peg_pend_akhir_th = $req->input('peg_pend_akhir_th');
            $pegawai->peg_gol_akhir_tmt = saveDate($req->input('peg_gol_akhir_tmt'));
            $pegawai->tgl_entry = $time;
            $pegawai->satuan_kerja_id = $req->input('satuan_kerja_id');
            $pegawai->peg_status = 'TRUE';
            $pegawai->peg_nip_lama = $req->input('peg_nip_lama');
            $pegawai->peg_ktp = $req->input('peg_ktp');
            $pegawai->peg_instansi_dpk = $req->input('peg_instansi_dpk');
            $pegawai->peg_ak = $req->input('peg_ak');
            $pegawai->peg_email = $req->input('peg_email');
            $pegawai->peg_status_asn =  $req->input('peg_status_asn') ? $req->input('peg_status_asn') : null;
            $pegawai->peg_status_gaji = $req->input('peg_status_gaji') ? $req->input('peg_status_gaji') : null;
            $pegawai->id_status_kepegawaian = $req->input('id_status_kepegawaian') ? $req->input('id_status_kepegawaian') : null;

            if($req->file('peg_foto')){
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('peg_foto')->getClientOriginalExtension(); // getting file extension
                $filename = $req->input('peg_nip') . '.' . $extension; // renameing image
                $upload_success = Input::file('peg_foto')->move($destinationPath, $filename); // uploading file to given path

                if($upload_success){
                    $pegawai->peg_foto = $filename;
                }
            }

            if($pegawai->save()){
                $log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
                if($log){
                    $log->action ="update_pegawai";
                    $log->status_id = 1;
                    $log->editor_id = Auth::user()->id;
                    $log->save();
                }else{
                    $log = new StatusEditPegawai;
                    $log->peg_id = $pegawai->peg_id;
                    $log->peg_nip = $req->input('peg_nip');
                    $log->action ="update_pegawai";
                    $log->status_id = 1;
                    $log->editor_id = Auth::user()->id;
                    $log->save();
                }
                logAction('Update Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
                
                $model = $req->all();
                $data = WalikotaPeriode::find($id);
                $data->tgl_mulai = $model['tgl_mulai'] ? $model['tgl_mulai'] : null;
                $data->tgl_selesai = $model['tgl_selesai'] ? $model['tgl_selesai']: null;
                $data->jenis = $model['jenis'];
                if($data->save()){
                    $w = Walikota::find($data->walikota_id);
                    $w->walikota_nama = $model['peg_nama'];
                    $w->save();
                }
                logAction('Edit Walikota',json_encode($data),$data->id,Auth::user()->username);
                return Redirect::to('/walikota')->with('message', 'Data Telah Diperbarui');
            }
        }
    }
}
