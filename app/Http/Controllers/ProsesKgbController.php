<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\StatusEditPegawai;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\PegawaiBelum;
use App\Model\SettingTempSrt;
use App\Model\KGB;
use App\Model\Gaji;
use App\Model\GajiTahun;
use App\Model\TTDKGB;
use App\Model\RiwayatPangkat;
use App\Model\UsulanKgb;
use App\Model\UsulanDetailKgb;
use App\Model\Golongan;
use PDF;
use TCPDF;
use Dompdf\Dompdf;
use DB;
use \ConvertApi\ConvertApi;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use PDFMerger;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use NcJoes\OfficeConverter\OfficeConverter;


class ProsesKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataKgb(){
        $id = Auth::user()->satuan_kerja_id; 
        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        $list_satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
        }
        
        $ttd_kgb = TTDKGB::select('spg_pegawai_simpeg.peg_id', 'spg_pegawai_simpeg.peg_nama','spg_pegawai_simpeg.peg_gelar_depan','spg_pegawai_simpeg.peg_gelar_belakang')->
        leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'm_ttd_kgb.peg_id_ttd')->orderBy('spg_pegawai_simpeg.peg_nama')->get();


        $pegawai = PegawaiBelum::orderBy('satuan_kerja_id')->orderBy('peg_nip')->get();
        $title = 'KGB';

        return view('pages.kgb.proses-kgb', compact('pegawai','list_satker', 'title','ttd_kgb'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $req){
        $kgb_manual = new KGB;
        $kgb_manual->user_id = Auth::user()->id;
        $kgb_manual->peg_nip = $req->m_peg_nip ? $req->m_peg_nip : 0;
        $kgb_manual->tanggal_from = $req->tgl_kgb;
        $kgb_manual->tanggal_to = $req->tanggal_to;
        $files = 0;

        foreach ($req->lampiran as $filelampiran) {
            if ($filelampiran == null) continue;
            $files++;
            $destinationPath = 'uploads'; // upload path
            $extension = $filelampiran->getClientOriginalExtension(); // getting file extension
            if (!in_array($extension,['jpg','jpeg','pdf'])) {
                return redirect()->back()->with('message','File surat harus dalam bentuk PDF atau JPG');
            }
            $filename = $filelampiran->getClientOriginalName(); // getting file extension
            while (File::exists(public_path("$destinationPath/$filename"))) {
                $extension_pos = strrpos($filename, '.');
                $filename = substr($filename, 0, $extension_pos) . str_random(5) . substr($filename, $extension_pos);
            }
            $filelampiran->move(public_path("$destinationPath"), $filename);
            $lampiran[] = $filename;
        }
        if ($files) {
            $kgb_manual->lampiran = $lampiran;
        }
        $kgb_manual->save();
        return redirect()->back()->with('message','Pengajuan berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function testpdf()
    {
        $settemplate = SettingTempSrt::where('id',1)->first();
      //  $data = ['title' => 'tes'];
     //   $nama_pegawai = 'halo';
        $pdf = PDF::loadHtml('<table>
    <tbody><tr>
        <td width="70%"></td>
        <td><p>Bandung,</p></td>
    </tr>     
    <tr>
        <td></td>
        <td><p>Kepada</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>Yth. Kepala Badan Pengelolaan Keuangan</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>dan Aset Kota Bandung</p></td>
    </tr>    
    <tr>
        <td></td>
        <td><p>(up. Kepala Bidang Perbendaharaan)</p></td>
    </tr>    
    <tr>
        <td></td>
        <td><p>di</p></td>
    </tr>   
     <tr>
        <td></td>
        <td><center><p><u>B A N D U N G</u></p></center></td>
    </tr>   
</tbody></table>   
<br \="">
<center><b><u>SURAT PEMBERITAHUAN KENAIKAN GAJI BERKALA</u></b></center>
<center><p>NOMOR : ............................................</p></center>
<p>Dengan ini diberitahukan, bahwa telah dipenuhinya masa kerja syarat-syarat administrasi lainnya kepada :</p>
<table>
    <tbody><tr>
        <td>1.</td>    
        <td>Nama</td>  
        <td>:</td>   
        <td>{{$nama_pegawai}}</td>   
    </tr>    
     <tr>
        <td>2.</td>    
        <td>Tempat, Tanggal Lahir</td>  
        <td>:</td>   
        <td>{{$ttl}}</td>   
    </tr>    
     <tr>
        <td>3.</td>    
        <td>NIP / KARPEG</td>  
        <td>:</td>   
        <td>{{$nip}}</td>   
    </tr>    
    <tr>
        <td>4.</td>    
        <td>Pangkat / Jabatan</td>  
        <td>:</td>   
        <td>{{$pangkat_jabatan}}</td>   
    </tr>    
     <tr>
        <td>5.</td>    
        <td>Unit Kerja</td>  
        <td>:</td>   
        <td>{{$unit_kerja}}</td>   
    </tr>    
     <tr>
        <td>6.</td>    
        <td>Gaji Pokok Lama</td>  
        <td>:</td>   
        <td><b>Rp. {{$gapok_lama}} ({{$terbilang}})</b></td>   
    </tr>    
     <tr>
        <td></td>    
        <td><p>(Berdasarkan Surat Keputusan terakhir Wali Kota Bandung tentang gaji dan pangkat yang ditetapkan)</p></td>  
    </tr>   
     <tr>
        <td></td>    
        <td>a. oleh pejabat</td>  
        <td>:</td>   
        <td>Kepala Dinas Pendidikan dan Kebudayaan</td>   
    </tr>    
    <tr>
        <td></td>    
        <td>b. tanggal</td>  
        <td>:</td>   
        <td>05 Januari 2015</td>   
    </tr>   
     <tr>
        <td></td>    
        <td>c.&nbsp; nomor</td>  
        <td>:</td>   
        <td>822.4/01665/DISDIKBUD/2015</td>   
    </tr>    
     <tr>
        <td></td>    
        <td>d. tanggal mulai berlaku gaji tersebut</td>  
        <td>:</td>   
        <td>01 Maret 2015</td>   
    </tr>    
     <tr>
        <td></td>    
        <td>e. masa Kerja Gol. pada tanggal tersebut</td>  
        <td>:</td>   
        <td>24 tahun 00 bulan</td>   
    </tr>    
     <tr>
        <td></td>    
        <td><p><u style="background-color: transparent;"><br></u></p><p><u style="background-color: transparent;">Diberikan Kenaikan Gaji Berkala, hingga memperoleh :</u><br></p></td>  
    </tr>   
     <tr>
        <td>7.</td>    
        <td>&nbsp;Gaji Pokok Baru</td>  
        <td>:</td>   
        <td><b>Rp. {{$gapok_baru}} ({{$terbilang}})</b></td>   
    </tr>  
     <tr>
        <td>8.</td>    
        <td>&nbsp;Berdasarkan masa kerja</td>  
        <td>:</td>   
        <td><b>{{$masa_kerja}}</b></td>   
    </tr>  
     <tr>
        <td>9.</td>    
        <td>&nbsp;Dalam Golongan</td>  
        <td>:</td>   
        <td><b>{{$golongan}}</b></td>   
    </tr>   
     <tr>
        <td>10.</td>    
        <td>&nbsp;Mulai Tanggal</td>  
        <td>:</td>   
        <td><b>{{$mulai_tanggal}}</b></td>   
    </tr>   
</tbody></table>    
<br \="">
<p>KETERANGAN :</p>
<p>a. Yang bersangkutan adalah Pegawai Negeri Sipil Daerah</p>
<p>b. Kenaikan Gaji Berkala y.a.d pada tanggal : <b>01 Maret 2019</b></p>
<p>c. Jika memenuhi syarat - syarat yang diperlukan</p>
<br \="">
<p>Diharapkan agar kepada Pegawai tersebut dibayarkan penghasilan gaji pokok yang baru Berdasarkan Peraturan Pemerintah Nomor 30 Tahun 2015</p>
<br \="">

<table>
    <tbody><tr>
        <td width="70%"></td>
        <td><center><p>a.n WALIKOTA BANDUNG </p></center></td>
    </tr>    
   
     <tr>
        <td></td>
        <td><center><p>KEPALA BADAN KEPEGAWAIAN PENDIDIKAN</p></center></td>
    </tr>    
     <tr>
        <td></td>
        <td><center><p>DAN PELATIHAN</p></center></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>   
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>   
   
    <tr>
        <td></td>
        <td><center><p>{{$nama_pejabat_penandatangan}}</p></center></td>
    </tr>    
    <tr>
        <td></td>
        <td><center><p>{{$nama_pangkat_pejabat_penandatangan}}</p></center></td>
    </tr>   
     <tr>
        <td></td>
        <td><center><p>NIP : {{$nip_pejabat_penandatangan}}</p></center></td>
    </tr>   
</tbody></table>  
<br \="">
<p><b>Tembusan :</b></p>
<p>1. Yth. Sekretaris Daerah Kota Bandung (sebagai laporan);</p>
<p>2. Yth. Inspektur Kota Bandung</p>
<p>3. Yth. Kepala Dinas Pendidikan Kota Bandung</p>')->setPaper('A4', 'potrait');
        // return $pdf->download('laporan-pdf.pdf');
        return $pdf->stream();
    }

    public function samplePDF()
    {
        require_once __DIR__ . '/vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML('<h1>Hello world!</h1>');
        $mpdf->Output();
        
        // $html_content = '<h1>Generate a PDF using TCPDF in laravel </h1>
        // 		<h4>by<br/>Learn Infinity</h4>';
      
 
      
        // TCPDF::AddPage();
        // TCPDF::writeHTML($html_content, true, false, true, false, '');
 
        // //TCPDF::Output('SamplePDF.pdf');
        // TCPDF::Output(uniqid().'_SamplePDF.pdf', 'D');
    }

    function getNoSuratKgb(){
        $thn = date('Y');
        $kgb = KGB::select(DB::raw('SUBSTRING(MAX(spg_kgb.kgb_nosurat),14,1) as max_ns'))->whereRaw('spg_kgb.kgb_thn = \''.$thn.'\'')
                ->first();
        if(is_null($kgb->max_ns)) $max = 1;
		else $max = $kgb->max_ns + 1;
        return $max;
        
    }


    public function verifikasi(Request $req){
     
        $update_kgb = KGB::where('kgb_id', $req->kgb_id)->first();

        $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
        $gaji = Gaji::where('mgaji_id','=',$gaji_thn->mgaji_id)
                    ->where('gol_id','=',$update_kgb->gol_id)
                    ->where('gaji_masakerja','=',$req->kgb_kerja_tahun)
                    ->first();

        //GET GOLONGAN
        $row_gol = Golongan::where('gol_id', $update_kgb->gol_id)->first();
        $kdgol = substr($row_gol->xxx_kd_gol,0,1);

        $nourutsurat = $this->getNoSuratKgb();
        $nourutsurat = str_pad($nourutsurat, 4, "0", STR_PAD_LEFT);

        $update_kgb->kgb_status = 1;
        $update_kgb->kgb_kerja_tahun = $req->kgb_kerja_tahun;
        $update_kgb->kgb_kerja_bulan = ($req->kgb_kerja_bulan=='')? 0 : $req->kgb_kerja_bulan;
        $update_kgb->kgb_gapok = isset($gaji->gaji_pokok) ? $gaji->gaji_pokok : 0;
        $update_kgb->kgb_tglsk = date("Y-m-d");
        $update_kgb->kgb_tglsurat = date("Y-m-d");
        $update_kgb->kgb_nosk = 'KP.06.02.'.$kdgol.'/'.$nourutsurat.'-BKPP/'.date('Y');
        $update_kgb->kgb_nosurat = 'KP.06.02.'.$kdgol.'/'.$nourutsurat.'-BKPP/'.date('Y');
        $update_kgb->user_id = Auth::user()->id;
        $update_kgb->ttd_pejabat = 'Walikota';
        $update_kgb->unit_kerja = $req->nm_unit_kerja;
        //$update_kgb->status_data = 'BY SISTEM';
        $update_kgb->save();
        
        
        //Update Status Usulan Detail    
        $update_usulan_detail = UsulanDetailKgb::where('usulan_id_detail', $update_kgb->usulan_id_detail)->first();
        $update_usulan_detail->status_usulan_pegawai = 50; // 50 => Done / Selesai
        $update_usulan_detail->save();
       
        //Cek
        $cek_usulan = UsulanDetailKgb::where('usulan_id',$update_usulan_detail->usulan_id)
                      ->where('status_usulan_pegawai',10) // Cek jumlah status yang masih proses
                      ->count();
        
        //Update Status Usulan
        if($cek_usulan < 1){
            $update_usulan = UsulanKgb::where('usulan_id',$update_usulan_detail->usulan_id)->first();
            $update_usulan->status = 50; // Jika semua sudah di proses update status done ke table usulan
            $update_usulan->save();
        }   
        
         //Update tgl kgb di table pegawai
         $update_tglkgb_peg = Pegawai::where('peg_id', $update_kgb->peg_id)->first();
         $update_tglkgb_peg->peg_tmt_kgb = $update_kgb->kgb_tmt;
         $update_tglkgb_peg->save();

        $this->generateBarcode($req->kgb_id);
        $this->generateWordSrtKgb($req->kgb_id);

        return response()->json([
        'message' => 'Verifikasi Berhasil!',
        'error' => $cek_usulan,
        'kgb_id' => $req->kgb_id])->setStatusCode(200);  
    }

    public function cancel(Request $req){
     
        $row_kgb = KGB::where('kgb_id', $req->kgb_id)->first();

      
        //Update Status Usulan Detail    
        $update_usulan_detail = UsulanDetailKgb::where('usulan_id_detail', $row_kgb->usulan_id_detail)->first();
        $update_usulan_detail->status_usulan_pegawai = 0; // 50 => Done / Selesai
        $update_usulan_detail->save();
       
        //Cek
        $cek_usulan = UsulanDetailKgb::where('usulan_id',$update_usulan_detail->usulan_id)
                      ->where('status_usulan_pegawai',50) // Cek jumlah status yang masih proses
                      ->count();
        
        //Update Status Usulan
       // if($cek_usulan < 1){
            $update_usulan = UsulanKgb::where('usulan_id',$update_usulan_detail->usulan_id)->first();
            $update_usulan->status = 20; // Jika semua sudah di proses update status kembali ke awal ke table usulan
            $update_usulan->save();
       // }              

        KGB::find($req->kgb_id)->delete();
			// UsulanDetailKgb::where('usulan_id', $id)->delete();
        logAction('Hapus Data KGB','',$req->kgb_id,Auth::user()->username);

        return response()->json([
        'message' => 'Pembatalan Berhasil!',
        'error' => $cek_usulan,
        'kgb_id' => $req->kgb_id])->setStatusCode(200);  
    }

    function TanggalIndo($date){
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $tahun = substr($date, 0, 4);
        $bulan = substr($date, 4, 2);
        $tgl   = substr($date, 6, 2);
         
        $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
        return($result);
    }

    function terbilang($x)
    {
        $x = $x?:0;
        $abil = array(
            "",
            " Satu",
            " Dua",
            " Tiga",
            " Empat",
            " Lima",
            " Enam",
            " Tujuh",
            " Delapan",
            " Sembilan",
            " Sepuluh",
            " Sebelas");
            
        if ($x < 12){
            return $abil[$x];}
        elseif ($x < 20){
            return $this->terbilang($x - 10) . " Belas";}
        elseif ($x < 100){
            return $this->terbilang($x / 10) . " Puluh" . $this->terbilang($x % 10);}
        elseif ($x < 200){
            return " Seratus" . $this->terbilang($x - 100);}
        elseif ($x < 1000){
            return $this->terbilang($x / 100) . " Ratus" . $this->terbilang($x % 100);}
        elseif ($x < 2000){
            return " Seribu" . $this->terbilang($x - 1000);}
        elseif ($x < 1000000){
            return $this->terbilang($x / 1000) . " Ribu" . $this->terbilang($x % 1000);}
        elseif ($x < 1000000000){
            return $this->terbilang($x / 1000000) . " Juta" . $this->terbilang($x % 1000000);}
    }

    public function generateBarcode($kgb_id)
    {
         QrCode::size(600)->format('png')->generate(url('uploads/surat_kgb/surat_KGB_'.$kgb_id.'.pdf'), public_path('images/barcode/'.$kgb_id.'.png'));
       // QrCode::format('png')->size(512)->generate(1);
            //QrCode::size(300)->generate('A basic example of QR code!');
    }
    public function generateWordSrtKgb($kgb_id)
    {
        $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
        $row_kgb = KGB::select('spg_pegawai_simpeg.*',
                                    DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),  
                                    DB::raw("(SELECT gaji_pokok FROM m_spg_gaji 
                                    where mgaji_id = $gaji_thn->mgaji_id AND gol_id = spg_kgb.gol_id AND gaji_masakerja = spg_pegawai_simpeg.peg_kerja_tahun) as gaji_pokok"),     
                                    'm_spg_satuan_kerja.satuan_kerja_nama','m_spg_unit_kerja.unit_kerja_nama',
                                    'gol.nm_gol', 'gol.nm_pkt', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama', 'spg_kgb.kgb_status',
                                    'spg_kgb.kgb_id','spg_kgb.gol_id','spg_kgb.gaji_id','spg_kgb.kgb_nosk','spg_kgb.kgb_tglsk',
                                    'spg_kgb.kgb_tmt','spg_kgb.kgb_status_peg','spg_kgb.kgb_thn','spg_kgb.kgb_bln','spg_kgb.kgb_gapok',
                                    'spg_kgb.kgb_tglsurat','spg_kgb.kgb_nosurat','spg_kgb.kgb_status','spg_kgb.user_id',
                                    'spg_kgb.kgb_kerja_tahun','spg_kgb.kgb_kerja_bulan',
                                    'spg_pegawai_simpeg.peg_kerja_tahun','spg_pegawai_simpeg.peg_kerja_bulan')->
                                    leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_kgb.peg_id')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_unit_kerja', 'm_spg_unit_kerja.unit_kerja_id', '=', 'spg_pegawai_simpeg.unit_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    whereRaw('spg_kgb.kgb_id = \''.$kgb_id.'\'')->first();
        //GET TTD
        $row_ttd = TTDKGB::select('pg.peg_nip','pg.peg_nama','pg.peg_gelar_depan','pg.peg_gelar_belakang','gol.nm_pkt','jab.jabatan_nama')
                    ->leftJoin('spg_pegawai_simpeg as pg','pg.peg_id','=','m_ttd_kgb.peg_id_ttd')
                    ->leftJoin('m_spg_golongan as gol','gol.gol_id','=','pg.gol_id_akhir')
                    ->leftJoin('m_spg_jabatan as jab','jab.jabatan_id','=','pg.jabatan_id')
                    ->whereRaw('\''.$row_kgb->gol_id.'\' BETWEEN m_ttd_kgb.gol_awal_id AND m_ttd_kgb.gol_akhir_id')->first(); 
        if($row_ttd->peg_id_ttd ==  137){ // jika sekda , kop nya jadi sekda
            $kop_srt = 'SEKRETARIAT DAERAH';
            $get_template = SettingTempSrt::where('id',2)->first(); // TEMPLATE SEKDA
        } else {
            $kop_srt = 'BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN';
          //  $kop_srt = 'SEKRETARIAT DAERAH';
             $get_template = SettingTempSrt::where('id',1)->first(); // TEMPLATE NORMAL
        }            

        //GET PANGKAT TERAKHIR
        $row_rwyt_pangkat = RiwayatPangkat::where('peg_id', $row_kgb->peg_id)
                            ->where('gol_id', $row_kgb->gol_id)
                            ->where('riw_pangkat_thn', $row_kgb->peg_kerja_tahun)
                            ->first(); 

        if(isset($row_kgb->jf_nama)){
            $nmjabatan = $row_kgb->jf_nama;
        } else if(isset($row_kgb->jfu_nama)){
            $nmjabatan = $row_kgb->jfu_nama;
        } else {
            $nmjabatan = $row_kgb->jabatan_nama;
        } 
        
      //  $kgb_tmt = ($row_kgb->kgb_tmt = '') ? $row_kgb->tgl_kenaikan : $row_kgb->kgb_tmt;
        $kgb_tmt = $row_kgb->tgl_kenaikan;
        
        $ttd_peg_gelar_depan = isset($row_ttd->peg_gelar_depan) ? $row_ttd->peg_gelar_depan : '';
        $ttd_peg_nama = isset($row_ttd->peg_nama) ? $row_ttd->peg_nama : 'MASTER TTD BELUM DI SETTING';
        $ttd_peg_gelar_belakang = isset($row_ttd->peg_gelar_belakang) ? $row_ttd->peg_gelar_belakang : '';
        $ttd_nm_pkt = isset($row_ttd->nm_pkt) ? $row_ttd->nm_pkt : '';
        $ttd_peg_nip = isset($row_ttd->peg_nip) ? $row_ttd->peg_nip : '';
        $ttd_peg_jabatan = isset($row_ttd->jabatan_nama) ? $row_ttd->jabatan_nama : 'KEPALA BADAN KEPEGAWAIAN PENDIDIKAN';
        $rwyt_pangkat_pejabat = isset($row_rwyt_pangkat->riw_pangkat_pejabat) ? $row_rwyt_pangkat->riw_pangkat_pejabat : '';
        $rwyt_pangkat_sktgl = isset($row_rwyt_pangkat->riw_pangkat_sktgl) ? $this->TanggalIndo(date("Ymd",strtotime($row_rwyt_pangkat->riw_pangkat_sktgl))) : '';
        $rwyt_pangkat_sk = isset($row_rwyt_pangkat->riw_pangkat_sk) ? $row_rwyt_pangkat->riw_pangkat_sk : '';
        $rwyt_pangkat_tmt = isset($row_rwyt_pangkat->riw_pangkat_tmt) ? $row_rwyt_pangkat->riw_pangkat_tmt : '';
       // $rwyt_pangkat_tmt = isset($row_rwyt_pangkat->riw_pangkat_tmt) ? $row_rwyt_pangkat->riw_pangkat_tmt : '';
         
       
    //    $title = new TextRun();
    //    $title->addText('This title has been set ', array('bold' => true, 'italic' => true, 'color' => 'blue'));
    //    $title->addText('dynamically', array('bold' => true, 'italic' => true, 'color' => 'red', 'underline' => 'single'));
    //    $templateProcessor->setComplexBlock('title', $title);
        //GET TEMPLATE SURAT KGB
       
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('file_doc/template_srt_kgb/'.$get_template->template));
        $kgb_selanjutnya = date('Y-m-d H:i:s', strtotime('+2 years', strtotime($kgb_tmt)));
        $templateProcessor->setValues(array(
                'nomor_surat_kgb' => $row_kgb->kgb_nosurat,
                'nama_pegawai' => $row_kgb->peg_gelar_depan.''.$row_kgb->peg_nama.', '.$row_kgb->peg_gelar_belakang, 
                'ttl' => $row_kgb->peg_lahir_tempat.', '.$this->TanggalIndo(date("Ymd",strtotime($row_kgb->peg_lahir_tanggal))), 
                'nip' => $row_kgb->peg_nip, 
                'pangkat_jabatan' => $row_kgb->nm_pkt.' / '.$nmjabatan, 
                'unit_kerja' => $row_kgb->unit_kerja_nama, 
                'gapok_lama' => 'Rp.'.number_format($row_kgb->gaji_pokok,0,',','.'), 
                'peg_kerja_tahun' => $row_kgb->peg_kerja_tahun, 
                'peg_kerja_bulan' => $row_kgb->peg_kerja_bulan, 
                'kgb_kerja_tahun' => $row_kgb->kgb_kerja_tahun, 
                'kgb_kerja_bulan' => $row_kgb->kgb_kerja_bulan, 
                'pejabat' => $rwyt_pangkat_pejabat,
                'tgl_surat' => $rwyt_pangkat_sktgl,
                'nomor_surat_lama' => $rwyt_pangkat_sk,
                'tmt_kgb_lama' => $this->TanggalIndo(date("Ymd",strtotime($rwyt_pangkat_tmt))), // SEMENTARA KE PANGKAT DULU KRN KGB TERAKHIR TIDAK ADA
                'gapok_baru' => 'Rp.'.number_format($row_kgb->kgb_gapok,0,',','.'), 
                'nmgol' => $row_kgb->nm_gol, 
                'kgb_tmt' => $this->TanggalIndo(date("Ymd",strtotime($kgb_tmt))), 
                'kgb_tmt_selanjutnya' => $this->TanggalIndo(date("Ymd",strtotime($kgb_selanjutnya))), 
                'nama_pejabat_penandatangan' => $ttd_peg_gelar_depan.''.$ttd_peg_nama.', '.$ttd_peg_gelar_belakang, 
                'nama_pangkat_pejabat_penandatangan' => $ttd_nm_pkt, 
                'nip_pejabat_penandatangan' => $ttd_peg_nip, 
                'terbilang_lama' => trim($this->terbilang($row_kgb->gaji_pokok).' rupiah'),
                'terbilang_baru' => trim($this->terbilang($row_kgb->kgb_gapok).' rupiah'),
                'tanggal_jam' => date("d-m-Y H:i:s"),
                'jabatan_pejabat_penandatangan' => strtoupper($ttd_peg_jabatan),
                'kop_srt' => strtoupper($kop_srt)
            ));
            // $inline = new TextRun();
            // $inline->addText('by a red italic text', array('size' => 9));
            // $templateProcessor->setComplexValue('kop_srt', $inline);
            
       // $templateProcessor->replaceImage(storage_path('file_doc/template_srt_kgb/qr-code.png'), $arrImagenes);   
       // $templateProcessor->setImg('barcode',array('src' => storage_path('file_doc/template_srt_kgb/qr-code.png'),'swh'=>'100'));
        $image_path = public_path('images/barcode/'.$kgb_id.'.png'); //storage_path('file_doc/template_srt_kgb/qr-code.png');
        $templateProcessor->setImg('barcode', array(
            'src'  => $image_path,
            'size' => array( 85, 85 ) //px
        ));

        $templateProcessor->setImg('barcode_footer', array(
            'src'  => $image_path,
            'size' => array( 55, 55 ) //px
        ));

        // $image_path_logo = public_path('images/logo_surat.png'); //storage_path('file_doc/template_srt_kgb/qr-code.png');
        // $templateProcessor->setImg('logo_surat', array(
        //     'src'  => $image_path_logo,
        //     'size' => array( 85, 85 ) //px
        // ));

        $templateProcessor->saveAs(storage_path('file_doc/surat_KGB_'.$kgb_id.'.docx'));


    }

    public function generateSrtKgb($kgb_id)
    {
        return response()->download(public_path('uploads/surat_kgb/surat_KGB_'.$kgb_id.'.pdf'));
    }

    public function convertPdf($kgb_id){
        //$rendererLibraryPath = realpath('/../vendor/dompdf/dompdf');
        Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
        // Any writable directory here. It will be ignored.
        Settings::setPdfRendererPath('.');

        $phpWord = IOFactory::load(storage_path('file_doc/surat_KGB_'.$kgb_id.'.docx'), 'Word2007');

        // $xmlWriter = IOFactory::createWriter($phpWord , 'PDF');
        // $xmlWriter->save(storage_path('file_pdf_surat_kgb/surat_KGB_'.$kgb_id.'.pdf'));

        $xmlWriter = IOFactory::createWriter($phpWord, 'ODText');
        $xmlWriter->save(storage_path('file_pdf_surat_kgb/surat_KGB_'.$kgb_id.'.odt'));
    }


    public function generateSrtKgbMultiple(Request $req)
    {
        $periode_awal=$req->periode_awal; 
        $periode_akhir=$req->periode_akhir;
        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        if(!$periode_awal) {
            $periode_awal = date('Y-m');
        } else {
            $periode_awal = date("Y-m", strtotime($periode_awal));
        }

        if(!$periode_akhir) {
            $periode_akhir = date('Y-m');
        } else{
            $periode_akhir = date("Y-m", strtotime($periode_akhir));
        }
//        $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
        $arr_kgb = KGB::select('spg_kgb.kgb_status',
                                    'spg_kgb.kgb_id','spg_kgb.gol_id','spg_kgb.gaji_id','spg_kgb.kgb_nosk','spg_kgb.kgb_tglsk',
                                    'spg_kgb.kgb_tmt','spg_kgb.kgb_status_peg','spg_kgb.kgb_thn','spg_kgb.kgb_bln','spg_kgb.kgb_gapok',
                                    'spg_kgb.kgb_tglsurat','spg_kgb.kgb_nosurat','spg_kgb.kgb_status','spg_kgb.user_id',
                                    'spg_kgb.kgb_kerja_tahun','spg_kgb.kgb_kerja_bulan')->
                                    leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_kgb.peg_id')->
                                    leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                    leftJoin('m_spg_unit_kerja', 'm_spg_unit_kerja.unit_kerja_id', '=', 'spg_pegawai_simpeg.unit_kerja_id')->
                                    leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                    leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                    leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                    leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                    whereRaw('concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) >= \''.$periode_awal.'\'
                                              AND concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) <= \''.$periode_akhir.'\'
                                              AND spg_kgb.kgb_status = 1 AND spg_kgb.kgb_status_pdf is null
                                             ')->get();
                                    
    foreach ($arr_kgb as $key => $value) {                                
    //     //GET TT
        $update_kgb = KGB::where('kgb_id', $arr_kgb[$key]['kgb_id'])->first();
        
        $update_kgb->kgb_status_pdf = 1; // TAMBAH FIELD BARU
        $update_kgb->save();

        ConvertApi::setApiSecret('kyQlfPn4BH0MnbjR');
        $result = ConvertApi::convert('pdf', [
                'File' => storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'),
            ], 'docx'
        );
        $result->saveFiles(public_path('uploads/surat_kgb'));

          //convert dengan libre office  
      //  shell_exec("soffice --headless --convert-to pdf '".storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx')."' --outdir '".public_path('uploads/surat_kgb')."'");





       // $converter = new OfficeConverter(storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'), public_path('uploads/surat_kgb'));
        //$converter->convertTo('surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.pdf'); //generates pdf file in same directory as test-file.docx
        // $outdir = public_path('uploads/surat_kgb');
       // $indir = storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx');
       // exec("C:\Program Files (x86)\LibreOffice 4.4\program\python.exe unoconv -f pdf --outdir '.$outdir.' '.$indir.'");
    //    \Gears\Pdf::convert(storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'), public_path('uploads/surat_kgb'));
        // $document = new \Gears\Pdf(storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'));
        // $document->converter = function()
        // {
        //     return new \Gears\Pdf\Docx\Converter\Unoconv();
        // };
        // $document->save(public_path('uploads/surat_kgb/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.pdf'));


        // $domPdfPath = realpath('/../vendor/dompdf/dompdf');
        // \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        // \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
            

        //  Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
        // // // Any writable directory here. It will be ignored.
        //  Settings::setPdfRendererPath('.');

        // $phpWord = IOFactory::load(storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'), 'Word2007');

        // $xmlWriter = IOFactory::createWriter($phpWord , 'PDF');
        // $xmlWriter->save(public_path('uploads/surat_kgb/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.pdf'));

        //return response()->download(storage_path('file_doc/surat_KGB_'.$arr_kgb[$key]['kgb_id'].'.docx'));
       
      }
     // $templateProcessor->saveAs(storage_path('file_doc/surat_KGB_999.docx'));
      return response()->json([
        'message' => 'Berhasil!',
        ])->setStatusCode(200);  
      //return response()->json(compact('arr_kgb', 'count'))->setStatusCode(200);     
    }



    public function testprint($fileName){
       DirectPrint::printFile(storage_path('file_doc/surat_KGB_220.docx')) ;
    }


    public function pdf_merger($periode_awal,$periode_akhir,$satker,$uker, $gol, $ttd)
    {
        $decrypt_per_awal = base64_decode($periode_awal);
        $decrypt_per_akhir = base64_decode($periode_akhir);
        $thnbln_awal = date("Y-m", strtotime($decrypt_per_awal));    
        $thnbln_akhir = date("Y-m", strtotime($decrypt_per_akhir));    
        $decrypt_gol = base64_decode($gol);
        $decrypt_satker = base64_decode($satker);
        $decrypt_uker = base64_decode($uker);
        $decrypt_ttd = base64_decode($ttd);
        $arr_gol = explode("," , $decrypt_gol);

        $pdf = new PDFMerger();
      //  $pdfFile1Path = public_path('uploads/surat_kgb') . '/surat_KGB_292.pdf';
       // $pdf->addPDF($pdfFile1Path, 'all');
       $gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first(); // CEK PP GAJI YG AKTIF
       $list_proses_kgb = KGB::select('spg_pegawai_simpeg.*', 
                                   DB::raw("EXTRACT(year FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as tahun_lama_kerja"),
                                   DB::raw("EXTRACT(month FROM age(to_char(now(), 'YYYY-MM-01')::date,spg_pegawai_simpeg.peg_cpns_tmt)) as bulan_lama_kerja"),
                                   DB::raw("TO_CHAR(spg_pegawai_simpeg.peg_tmt_kgb + INTERVAL '2 year', 'YYYY-MM-DD') AS tgl_kenaikan"),  
                                   DB::raw("(SELECT gaji_pokok FROM m_spg_gaji 
                                   where mgaji_id = $gaji_thn->mgaji_id AND gol_id = spg_kgb.gol_id AND gaji_masakerja = spg_pegawai_simpeg.peg_kerja_tahun) as gaji_pokok"),     
                                   'm_spg_satuan_kerja.satuan_kerja_nama','m_spg_unit_kerja.unit_kerja_nama',
                                   'gol.nm_gol', 'jab.jabatan_nama', 'jf.jf_nama', 'jfu.jfu_nama', 'spg_kgb.kgb_status',
                                   'spg_kgb.kgb_id','spg_kgb.gol_id','spg_kgb.gaji_id','spg_kgb.kgb_nosk','spg_kgb.kgb_tglsk',
                                   'spg_kgb.kgb_tmt','spg_kgb.kgb_status_peg','spg_kgb.kgb_thn','spg_kgb.kgb_bln','spg_kgb.kgb_gapok',
                                   'spg_kgb.kgb_tglsurat','spg_kgb.kgb_nosurat','spg_kgb.kgb_status','spg_kgb.user_id',
                                   'spg_kgb.kgb_kerja_tahun','spg_kgb.kgb_kerja_bulan', 'spg_kgb.kgb_status_pdf',
                                   'spg_pegawai_simpeg.peg_kerja_tahun','spg_pegawai_simpeg.peg_kerja_bulan', 'su.no_usulan')->
                                   leftJoin('spg_pegawai_simpeg', 'spg_pegawai_simpeg.peg_id', '=', 'spg_kgb.peg_id')->
                                 //  leftJoin('m_spg_gaji as gj', 'gj.gaji_masakerja', '=','spg_pegawai_simpeg.peg_kerja_tahun','and','gj.gol_id', '=','spg_kgb.gol_id', 'and', 'gj.mgaji_id = \''.$gaji_thn->mgaji_id.'\'')->
                                   leftJoin('m_spg_satuan_kerja', 'm_spg_satuan_kerja.satuan_kerja_id', '=', 'spg_pegawai_simpeg.satuan_kerja_id')->
                                   leftJoin('m_spg_unit_kerja', 'm_spg_unit_kerja.unit_kerja_id', '=', 'spg_pegawai_simpeg.unit_kerja_id')->
                                   leftJoin('m_spg_golongan as gol', 'gol.gol_id', '=', 'spg_pegawai_simpeg.gol_id_akhir')->
                                   leftJoin('m_spg_jabatan as jab', 'jab.jabatan_id', '=','spg_pegawai_simpeg.jabatan_id')->
                                   leftJoin('m_spg_referensi_jf as jf', 'jf.jf_id', '=','jab.jf_id', 'and', 'jab.jabatan_jenis = 3')->
                                   leftJoin('m_spg_referensi_jfu as jfu', 'jfu.jfu_id', '=','jab.jfu_id', 'and', 'jab.jabatan_jenis = 4')->
                                   leftJoin('spg_usulan_detail as sud', 'sud.peg_id', '=','spg_kgb.peg_id')->
                                   leftJoin('spg_usulan as su', 'su.usulan_id', '=','sud.usulan_id')->
                                   leftJoin('m_ttd_kgb', function($join){
                                        $join->on('spg_pegawai_simpeg.gol_id_akhir','>=','m_ttd_kgb.gol_awal_id');
                                        $join->on('spg_pegawai_simpeg.gol_id_akhir','<=','m_ttd_kgb.gol_akhir_id');
                                    })->
                                  // leftJoin('m_ttd_kgb as ttd',  "spg_pegawai_simpeg.gol_id_akhir", ">=", "ttd.gol_awal_id", "and" ,"spg_pegawai_simpeg.gol_id_akhir" ,"<=", "ttd.gol_akhir_id")->
                                   whereRaw('concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) >= \''.$thnbln_awal.'\'
                                   AND concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) <= \''.$thnbln_akhir.'\'')->
                                   where('spg_kgb.status_data', '<>', 'manual')->
                                   where('spg_kgb.kgb_status_pdf', '=', 1);
       if($decrypt_ttd != 'undefined'){
           $list_proses_kgb = $list_proses_kgb->where('m_ttd_kgb.peg_id_ttd',$decrypt_ttd);
       }

       if($decrypt_uker != 'undefined') {
           $list_proses_kgb = $list_proses_kgb->where('spg_pegawai_simpeg.unit_kerja_id',$uker);
       } 
       if($decrypt_gol != 'undefined') {
           $list_proses_kgb = $list_proses_kgb->whereIn('spg_pegawai_simpeg.gol_id_akhir',$arr_gol);
       } 

       $list_proses_kgb = $list_proses_kgb->orderBy('spg_kgb.kgb_id', 'asc')->get();

      // $arr_kgb = KGB::select('spg_kgb.kgb_id')->whereRaw('concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) >= \''.$periode_awal.'\'
     //  AND concat(spg_kgb.kgb_thn,\'-\',spg_kgb.kgb_bln) <= \''.$periode_akhir.'\' AND spg_kgb.kgb_status = 1 AND spg_kgb.kgb_status_pdf = 1')->get();
                
        foreach ($list_proses_kgb as $key => $value) 
        {
            $pdfFilePath = public_path('uploads/surat_kgb'). '/surat_KGB_'.$list_proses_kgb[$key]['kgb_id'].'.pdf';
            $pdf->addPDF($pdfFilePath, 'all');
        }             
         $pdf->merge('duplexMerge', "surat_kgb.pdf");
       // $binaryContent = $pdf->merge('string', "surat_kgb.pdf");

        // Return binary content as response
        // return response($binaryContent)
        //     ->header('Content-type' , 'application/pdf')
        // ;
    }
    
  
   
}
