<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\UnitKerja;
use App\Model\SatuanKerja;
use App\Model\Golongan;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use Validator;
use Input;
use Redirect;
use Auth;
use Excel;
use PHPExcel_Worksheet_PageSetup;

class RekapController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

    public function skpd(){
        return view('pages.rekap.skpd');
    }

	public function jabatan(){
		return view('pages.rekap.jabatan');
	}

    public function golongan(){
        return view('pages.rekap.golongan');
    }

    public function JenisKelamin(){
        return view('pages.rekap.jenis_kelamin');
    }

    public function pendidikan(){
        return view('pages.rekap.pendidikan');
    }

    public function usia(){
        return view('pages.rekap.kelompok_usia');
    }

    public function eselonJK(){
        return view('pages.rekap.eselon_jk');
    }

	public function getDataJabatan(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['a2'=>0,'b2'=>0,'a3'=>0,'b3'=>0,'a4'=>0,'b4'=>0,'a5'=>0,'s'=>0,'f'=>0,'fu'=>0,'total'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapJabatan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['eselon_2a'],
                $rekaps['eselon_2b'],
                $rekaps['eselon_3a'],
                $rekaps['eselon_3b'],
                $rekaps['eselon_4a'],
                $rekaps['eselon_4b'],
                $rekaps['eselon_5a'],
                $rekaps['total_s'],
                $rekaps['total_f'],
                $rekaps['total_fu'],
                $rekaps['total'],
            ];
            $jumlah['a2'] += $rekaps['eselon_2a'];
            $jumlah['b2'] += $rekaps['eselon_2b'];
            $jumlah['a3'] += $rekaps['eselon_3a'];
            $jumlah['b3'] += $rekaps['eselon_3b'];
            $jumlah['a4'] += $rekaps['eselon_4a'];
            $jumlah['b4'] += $rekaps['eselon_4b'];
            $jumlah['a5'] += $rekaps['eselon_5a'];
            $jumlah['s'] += $rekaps['total_s'];
            $jumlah['f'] += $rekaps['total_f'];
            $jumlah['fu'] += $rekaps['total_fu'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapJabatanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                        $rekaps['eselon_2a'],
                        $rekaps['eselon_2b'],
                        $rekaps['eselon_3a'],
                        $rekaps['eselon_3b'],
                        $rekaps['eselon_4a'],
                        $rekaps['eselon_4b'],
                        $rekaps['eselon_5a'],
                        $rekaps['total_s'],
                        $rekaps['total_f'],
                        $rekaps['total_fu'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapJabatanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['eselon_2a'],
                            $rekaps['eselon_2b'],
                            $rekaps['eselon_3a'],
                            $rekaps['eselon_3b'],
                            $rekaps['eselon_4a'],
                            $rekaps['eselon_4b'],
                            $rekaps['eselon_5a'],
                            $rekaps['total_s'],
                            $rekaps['total_f'],
                            $rekaps['total_fu'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }
        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jumlah
        ];

        return response()->json($result);
    }

    public function getDataGolongan(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['a1'=>0,'b1'=>0,'c1'=>0,'d1'=>0,'a2'=>0,'b2'=>0,'c2'=>0,'d2'=>0,'a3'=>0,'b3'=>0,'c3'=>0,'d3'=>0,'a4'=>0,'b4'=>0,'c4'=>0,'d4'=>0,'e4'=>0,'total'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapGolongan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['1a'],
                $rekaps['1b'],
                $rekaps['1c'],
                $rekaps['1d'],
                $rekaps['2a'],
                $rekaps['2b'],
                $rekaps['2c'],
                $rekaps['2d'],
                $rekaps['3a'],
                $rekaps['3b'],
                $rekaps['3c'],
                $rekaps['3d'],
                $rekaps['4a'],
                $rekaps['4b'],
                $rekaps['4c'],
                $rekaps['4d'],
                $rekaps['4e'],
                $rekaps['total'],
            ];
            $jumlah['a1'] += $rekaps['1a'];
            $jumlah['b1'] += $rekaps['1b'];
            $jumlah['c1'] += $rekaps['1c'];
            $jumlah['d1'] += $rekaps['1d'];
            $jumlah['a2'] += $rekaps['2a'];
            $jumlah['b2'] += $rekaps['2b'];
            $jumlah['c2'] += $rekaps['2c'];
            $jumlah['d2'] += $rekaps['2d'];
            $jumlah['a3'] += $rekaps['3a'];
            $jumlah['b3'] += $rekaps['3b'];
            $jumlah['c3'] += $rekaps['3c'];
            $jumlah['d3'] += $rekaps['3d'];
            $jumlah['a4'] += $rekaps['4a'];
            $jumlah['b4'] += $rekaps['4b'];
            $jumlah['c4'] += $rekaps['4c'];
            $jumlah['d4'] += $rekaps['4d'];
            $jumlah['e4'] += $rekaps['4e'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapGolonganSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp;'.$unit_kerja->unit_kerja_nama,
                        $rekaps['1a'],
                        $rekaps['1b'],
                        $rekaps['1c'],
                        $rekaps['1d'],
                        $rekaps['2a'],
                        $rekaps['2b'],
                        $rekaps['2c'],
                        $rekaps['2d'],
                        $rekaps['3a'],
                        $rekaps['3b'],
                        $rekaps['3c'],
                        $rekaps['3d'],
                        $rekaps['4a'],
                        $rekaps['4b'],
                        $rekaps['4c'],
                        $rekaps['4d'],
                        $rekaps['4e'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapGolonganSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['1a'],
                            $rekaps['1b'],
                            $rekaps['1c'],
                            $rekaps['1d'],
                            $rekaps['2a'],
                            $rekaps['2b'],
                            $rekaps['2c'],
                            $rekaps['2d'],
                            $rekaps['3a'],
                            $rekaps['3b'],
                            $rekaps['3c'],
                            $rekaps['3d'],
                            $rekaps['4a'],
                            $rekaps['4b'],
                            $rekaps['4c'],
                            $rekaps['4d'],
                            $rekaps['4e'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jumlah
        ];

        return response()->json($result);
    }

    public function getDataJenisKelamin(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);
        $jum = ['l'=>0,'p'=>0,'total'=>0,'b'=>0];

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapJenisKelamin($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['l'],
                $rekaps['p'],
                $rekaps['total'],
                $rekaps['b'],
            ];
            $jum['l'] += $rekaps['l'];
            $jum['p'] += $rekaps['p'];
            $jum['b'] += $rekaps['b'];
            $jum['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp;'.$unit_kerja->unit_kerja_nama,
                        $rekaps['l'],
                        $rekaps['p'],
                        $rekaps['total'],
                        $rekaps['b'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['l'],
                            $rekaps['p'],
                            $rekaps['total'],
                            $rekaps['b'],
                        ];

                    }
                }
            }
        }
        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jum
        ];

        return response()->json($result);
    }

    public function getDataPendidikan(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['sd'=>0,'smp'=>0,'sma'=>0,'d1'=>0,'d2'=>0,'d3'=>0,'d4'=>0,'s1'=>0,'s2'=>0,'s3'=>0,'total'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapPendidikan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['sd'],
                $rekaps['smp'],
                $rekaps['sma'],
                $rekaps['d1'],
                $rekaps['d2'],
                $rekaps['d3'],
                $rekaps['d4'],
                $rekaps['s1'],
                $rekaps['s2'],
                $rekaps['s3'],
                $rekaps['total'],
            ];
            $jumlah['sd'] += $rekaps['sd'];
            $jumlah['smp'] += $rekaps['smp'];
            $jumlah['sma'] += $rekaps['sma'];
            $jumlah['d1'] += $rekaps['d1'];
            $jumlah['d2'] += $rekaps['d2'];
            $jumlah['d3'] += $rekaps['d3'];
            $jumlah['d4'] += $rekaps['d4'];
            $jumlah['s1'] += $rekaps['s1'];
            $jumlah['s2'] += $rekaps['s2'];
            $jumlah['s3'] += $rekaps['s3'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapPendidikanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp;'.$unit_kerja->unit_kerja_nama,
                        $rekaps['sd'],
                        $rekaps['smp'],
                        $rekaps['sma'],
                        $rekaps['d1'],
                        $rekaps['d2'],
                        $rekaps['d3'],
                        $rekaps['d4'],
                        $rekaps['s1'],
                        $rekaps['s2'],
                        $rekaps['s3'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapPendidikanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['sd'],
                            $rekaps['smp'],
                            $rekaps['sma'],
                            $rekaps['d1'],
                            $rekaps['d2'],
                            $rekaps['d3'],
                            $rekaps['d4'],
                            $rekaps['s1'],
                            $rekaps['s2'],
                            $rekaps['s3'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jumlah,
        ];

        return response()->json($result);
    }

    public function getDataUsia(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['a'=>0,'b'=>0,'c'=>0,'d'=>0,'e'=>0,'f'=>0,'g'=>0,'total'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapUsia($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['1'],
                $rekaps['2'],
                $rekaps['3'],
                $rekaps['4'],
                $rekaps['5'],
                $rekaps['6'],
                $rekaps['7'],
                $rekaps['total'],
            ];
            $jumlah['a'] += $rekaps['1'];
            $jumlah['b'] += $rekaps['2'];
            $jumlah['c'] += $rekaps['3'];
            $jumlah['d'] += $rekaps['4'];
            $jumlah['e'] += $rekaps['5'];
            $jumlah['f'] += $rekaps['6'];
            $jumlah['g'] += $rekaps['7'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapUsiaSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp;'.$unit_kerja->unit_kerja_nama,
                        $rekaps['1'],
                        $rekaps['2'],
                        $rekaps['3'],
                        $rekaps['4'],
                        $rekaps['5'],
                        $rekaps['6'],
                        $rekaps['7'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapUsiaSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['1'],
                            $rekaps['2'],
                            $rekaps['3'],
                            $rekaps['4'],
                            $rekaps['5'],
                            $rekaps['6'],
                            $rekaps['7'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jumlah,
        ];

        return response()->json($result);
    }

    public function getDataEselonJK(Request $req) {
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['eselon_2a_l'=>0,'eselon_2a_p'=>0,'eselon_2b_l'=>0,'eselon_2b_p'=>0,'eselon_3a_l'=>0,'eselon_3a_p'=>0,'eselon_3b_l'=>0,'eselon_3b_p'=>0,'eselon_4a_l'=>0,'eselon_4a_p'=>0,'eselon_4b_l'=>0,'eselon_4b_p'=>0,'eselon_5a_l'=>0,'eselon_5a_p'=>0,'total_s'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapEselonJK($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['eselon_2a_l'],
                $rekaps['eselon_2a_p'],
                $rekaps['eselon_2b_l'],
                $rekaps['eselon_2b_p'],
                $rekaps['eselon_3a_l'],
                $rekaps['eselon_3a_p'],
                $rekaps['eselon_3b_l'],
                $rekaps['eselon_3b_p'],
                $rekaps['eselon_4a_l'],
                $rekaps['eselon_4a_p'],
                $rekaps['eselon_4b_l'],
                $rekaps['eselon_4b_p'],
                $rekaps['eselon_5a_l'],
                $rekaps['eselon_5a_p'],
                $rekaps['total_s'],
            ];
            $jumlah['eselon_2a_l'] += $rekaps['eselon_2a_l'];
            $jumlah['eselon_2a_p'] += $rekaps['eselon_2a_p'];
            $jumlah['eselon_2b_l'] += $rekaps['eselon_2b_l'];
            $jumlah['eselon_2b_p'] += $rekaps['eselon_2b_p'];
            $jumlah['eselon_3a_l'] += $rekaps['eselon_3a_l'];
            $jumlah['eselon_3a_p'] += $rekaps['eselon_3a_p'];
            $jumlah['eselon_3b_l'] += $rekaps['eselon_3b_l'];
            $jumlah['eselon_3b_p'] += $rekaps['eselon_3b_p'];
            $jumlah['eselon_4a_l'] += $rekaps['eselon_4a_l'];
            $jumlah['eselon_4a_p'] += $rekaps['eselon_4a_p'];
            $jumlah['eselon_4b_l'] += $rekaps['eselon_4b_l'];
            $jumlah['eselon_4b_p'] += $rekaps['eselon_4b_p'];
            $jumlah['eselon_5a_l'] += $rekaps['eselon_5a_l'];
            $jumlah['eselon_5a_p'] += $rekaps['eselon_5a_p'];
            $jumlah['total_s'] += $rekaps['total_s'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapEselonJKSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                        $rekaps['eselon_2a_l'],
                        $rekaps['eselon_2a_p'],
                        $rekaps['eselon_2b_l'],
                        $rekaps['eselon_2b_p'],
                        $rekaps['eselon_3a_l'],
                        $rekaps['eselon_3a_p'],
                        $rekaps['eselon_3b_l'],
                        $rekaps['eselon_3b_p'],
                        $rekaps['eselon_4a_l'],
                        $rekaps['eselon_4a_p'],
                        $rekaps['eselon_4b_l'],
                        $rekaps['eselon_4b_p'],
                        $rekaps['eselon_5a_l'],
                        $rekaps['eselon_5a_p'],
                        $rekaps['total_s'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapEselonJKSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$unit_kerja->unit_kerja_nama,
                            $rekaps['eselon_2a_l'],
                            $rekaps['eselon_2a_p'],
                            $rekaps['eselon_2b_l'],
                            $rekaps['eselon_2b_p'],
                            $rekaps['eselon_3a_l'],
                            $rekaps['eselon_3a_p'],
                            $rekaps['eselon_3b_l'],
                            $rekaps['eselon_3b_p'],
                            $rekaps['eselon_4a_l'],
                            $rekaps['eselon_4a_p'],
                            $rekaps['eselon_4b_l'],
                            $rekaps['eselon_4b_p'],
                            $rekaps['eselon_5a_l'],
                            $rekaps['eselon_5a_p'],
                            $rekaps['total_s'],
                        ];

                    }
                }
            }
        }

        $result = [
            'data' => $data,
            'count' => $count,
            'jumlah' => $jumlah,
        ];

        return response()->json($result);
    }

    public function exportRekapJabatan(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapJabatan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['eselon_2a'],
                $rekaps['eselon_2b'],
                $rekaps['eselon_3a'],
                $rekaps['eselon_3b'],
                $rekaps['eselon_4a'],
                $rekaps['eselon_4b'],
                $rekaps['eselon_5a'],
                $rekaps['total_s'],
                $rekaps['total_f'],
                $rekaps['total_fu'],
                $rekaps['total'],
            ];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapJabatanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['eselon_2a'],
                        $rekaps['eselon_2b'],
                        $rekaps['eselon_3a'],
                        $rekaps['eselon_3b'],
                        $rekaps['eselon_4a'],
                        $rekaps['eselon_4b'],
                        $rekaps['eselon_5a'],
                        $rekaps['total_s'],
                        $rekaps['total_f'],
                        $rekaps['total_fu'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapJabatanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '      '.$unit_kerja->unit_kerja_nama,
                            $rekaps['eselon_2a'],
                            $rekaps['eselon_2b'],
                            $rekaps['eselon_3a'],
                            $rekaps['eselon_3b'],
                            $rekaps['eselon_4a'],
                            $rekaps['eselon_4b'],
                            $rekaps['eselon_5a'],
                            $rekaps['total_s'],
                            $rekaps['total_f'],
                            $rekaps['total_fu'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Jabatan', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Jabatan', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:M2');
                $sheet->mergeCells('A3:M3');
                $sheet->mergeCells('A4:M4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:M7', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->mergeCells('A6:A7');
                $sheet->mergeCells('B6:B7');
                $sheet->mergeCells('C6:J6');
                $sheet->mergeCells('K6:K7');
                $sheet->mergeCells('L6:L7');
                $sheet->mergeCells('M6:M7');

                $sheet->row(6, ['No', 'SKPD']);
                $sheet->cell('C6', function($cell) {
                    $cell->setValue('Pejabat Struktural');
                });

                $sheet->cell('K6', function($cell) {
                    $cell->setValue('Fungsional Tertentu');
                });

                $sheet->cell('L6', function($cell) {
                    $cell->setValue('Fungsional Umum');
                });

                $sheet->cell('M6', function($cell) {
                    $cell->setValue('Jumlah Seluruh');
                });

                $sheet->cell('C7', function($cell) {
                    $cell->setValue('II.a');
                });

                $sheet->cell('D7', function($cell) {
                    $cell->setValue('II.b');
                });

                $sheet->cell('E7', function($cell) {
                    $cell->setValue('III.a');
                });

                $sheet->cell('F7', function($cell) {
                    $cell->setValue('III.b');
                });

                $sheet->cell('G7', function($cell) {
                    $cell->setValue('IV.a');
                });

                $sheet->cell('H7', function($cell) {
                    $cell->setValue('IV.b');
                });
                $sheet->cell('I7', function($cell) {
                    $cell->setValue('V.a');
                });
                $sheet->cell('J7', function($cell) {
                    $cell->setValue('Jumlah');
                });

                $sheet->setBorder('A6:M7', 'thin');
                $sheet->fromArray($data, "", "A8", true, false);

                for ($j = 8; $j < count($data) + 8; $j++) {
                    $sheet->setBorder('A'.$j.':M'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapGolongan(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapGolongan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['1a'],
                $rekaps['1b'],
                $rekaps['1c'],
                $rekaps['1d'],
                $rekaps['2a'],
                $rekaps['2b'],
                $rekaps['2c'],
                $rekaps['2d'],
                $rekaps['3a'],
                $rekaps['3b'],
                $rekaps['3c'],
                $rekaps['3d'],
                $rekaps['4a'],
                $rekaps['4b'],
                $rekaps['4c'],
                $rekaps['4d'],
                $rekaps['4e'],
                $rekaps['total'],
            ];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapGolonganSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['1a'],
                        $rekaps['1b'],
                        $rekaps['1c'],
                        $rekaps['1d'],
                        $rekaps['2a'],
                        $rekaps['2b'],
                        $rekaps['2c'],
                        $rekaps['2d'],
                        $rekaps['3a'],
                        $rekaps['3b'],
                        $rekaps['3c'],
                        $rekaps['3d'],
                        $rekaps['4a'],
                        $rekaps['4b'],
                        $rekaps['4c'],
                        $rekaps['4d'],
                        $rekaps['4e'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapGolonganSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '       '.$unit_kerja->unit_kerja_nama,
                            $rekaps['1a'],
                            $rekaps['1b'],
                            $rekaps['1c'],
                            $rekaps['1d'],
                            $rekaps['2a'],
                            $rekaps['2b'],
                            $rekaps['2c'],
                            $rekaps['2d'],
                            $rekaps['3a'],
                            $rekaps['3b'],
                            $rekaps['3c'],
                            $rekaps['3d'],
                            $rekaps['4a'],
                            $rekaps['4b'],
                            $rekaps['4c'],
                            $rekaps['4d'],
                            $rekaps['4e'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Golongan', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Golongan', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:T2');
                $sheet->mergeCells('A3:T3');
                $sheet->mergeCells('A4:T4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:T6', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', 'I/a','I/b', 'I/c', 'I/d', 'II/a', 'II/b', 'II/c','II/d','III/a','III/b','III/c','III/d','IV/a','IV/b','IV/c','IV/d','IV/e','Total']);
                $sheet->setBorder('A6:T6', 'thin');
                $sheet->fromArray($data, "", "A7", true, false);

                for ($j = 7; $j < count($data) + 7; $j++) {
                    $sheet->setBorder('A'.$j.':T'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapJenisKelamin(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapJenisKelamin($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['l'],
                $rekaps['p'],
                $rekaps['total'],
                $rekaps['b'] > 0 ? 'Belum terdata: '.$rekaps['b'] : '',
            ];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['l'],
                        $rekaps['p'],
                        $rekaps['total'],
                        $rekaps['b'] > 0 ? 'Belum terdata: '.$rekaps['b'] : '',
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '       '.$unit_kerja->unit_kerja_nama,
                            $rekaps['l'],
                            $rekaps['p'],
                            $rekaps['total'],
                            $rekaps['b'] > 0 ? 'Belum terdata: '.$rekaps['b'] : '',
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Jenis Kelamin', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Jenis Kelamin', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                $sheet->mergeCells('A4:E4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:E6', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', 'Laki-laki','Perempuan','Total']);
                $sheet->setBorder('A6:E6', 'thin');
                $sheet->fromArray($data, "", "A7", true, false);

                for ($j = 7; $j < count($data) + 7; $j++) {
                    $sheet->setBorder('A'.$j.':E'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapSKPD(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapJenisKelamin($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['total'],
            ];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapJenisKelaminSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '       '.$unit_kerja->unit_kerja_nama,
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap SKPD', function($excel) use ($data) {
            $excel->sheet('Rekap SKPD', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                $sheet->mergeCells('A4:E4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:C6', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', 'Total']);
                $sheet->setBorder('A6:C6', 'thin');
                $sheet->fromArray($data, "", "A7", true, false);

                for ($j = 7; $j < count($data) + 7; $j++) {
                    $sheet->setBorder('A'.$j.':C'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapPendidikan(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['sd'=>0,'smp'=>0,'sma'=>0,'d1'=>0,'d2'=>0,'d3'=>0,'d4'=>0,'s1'=>0,'s2'=>0,'s3'=>0,'total'=>0];

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapPendidikan($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['sd'],
                $rekaps['smp'],
                $rekaps['sma'],
                $rekaps['d1'],
                $rekaps['d2'],
                $rekaps['d3'],
                $rekaps['d4'],
                $rekaps['s1'],
                $rekaps['s2'],
                $rekaps['s3'],
                $rekaps['total'],
            ];
            $jumlah['sd'] += $rekaps['sd'];
            $jumlah['smp'] += $rekaps['smp'];
            $jumlah['sma'] += $rekaps['sma'];
            $jumlah['d1'] += $rekaps['d1'];
            $jumlah['d2'] += $rekaps['d2'];
            $jumlah['d3'] += $rekaps['d3'];
            $jumlah['d4'] += $rekaps['d4'];
            $jumlah['s1'] += $rekaps['s1'];
            $jumlah['s2'] += $rekaps['s2'];
            $jumlah['s3'] += $rekaps['s3'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapPendidikanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['sd'],
                        $rekaps['smp'],
                        $rekaps['sma'],
                        $rekaps['d1'],
                        $rekaps['d2'],
                        $rekaps['d3'],
                        $rekaps['d4'],
                        $rekaps['s1'],
                        $rekaps['s2'],
                        $rekaps['s3'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapPendidikanSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '       '.$unit_kerja->unit_kerja_nama,
                            $rekaps['sd'],
                            $rekaps['smp'],
                            $rekaps['sma'],
                            $rekaps['d1'],
                            $rekaps['d2'],
                            $rekaps['d3'],
                            $rekaps['d4'],
                            $rekaps['s1'],
                            $rekaps['s2'],
                            $rekaps['s3'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Tingkat Pendidikan', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Tingkat Pendidikan', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:M2');
                $sheet->mergeCells('A3:M3');
                $sheet->mergeCells('A4:M4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:M6', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', 'SD','SMP','SMA','D1','D2','D3','D4','S1','S2','S3','Total']);
                $sheet->setBorder('A6:M6', 'thin');
                $sheet->fromArray($data, "", "A7", true, false);

                for ($j = 7; $j < count($data) + 7; $j++) {
                    $sheet->setBorder('A'.$j.':M'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapUsia(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['a'=>0,'b'=>0,'c'=>0,'d'=>0,'e'=>0,'f'=>0,'g'=>0,'total'=>0];

        $count = $satkers->count();
        $satkers = $satkers->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapUsia($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['1'],
                $rekaps['2'],
                $rekaps['3'],
                $rekaps['4'],
                $rekaps['5'],
                $rekaps['6'],
                $rekaps['7'],
                $rekaps['total'],
            ];
            $jumlah['a'] += $rekaps['1'];
            $jumlah['b'] += $rekaps['2'];
            $jumlah['c'] += $rekaps['3'];
            $jumlah['d'] += $rekaps['4'];
            $jumlah['e'] += $rekaps['5'];
            $jumlah['f'] += $rekaps['6'];
            $jumlah['g'] += $rekaps['7'];
            $jumlah['total'] += $rekaps['total'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapUsiaSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['1'],
                        $rekaps['2'],
                        $rekaps['3'],
                        $rekaps['4'],
                        $rekaps['5'],
                        $rekaps['6'],
                        $rekaps['7'],
                        $rekaps['total'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapUsiaSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '       '.$unit_kerja->unit_kerja_nama,
                            $rekaps['1'],
                            $rekaps['2'],
                            $rekaps['3'],
                            $rekaps['4'],
                            $rekaps['5'],
                            $rekaps['6'],
                            $rekaps['7'],
                            $rekaps['total'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Kelompok Usia', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Kelompok Usia', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->mergeCells('A4:J4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:J6', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', '16-25','26-35','36-45','46-55','56-65','>65','Lain','Total']);
                $sheet->setBorder('A6:J6', 'thin');
                $sheet->fromArray($data, "", "A7", true, false);

                for ($j = 7; $j < count($data) + 7; $j++) {
                    $sheet->setBorder('A'.$j.':J'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

    public function exportRekapEselonJk(Request $req){
        $satkers = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1);
        if (Auth::user()->role_id != 1 && Auth::user()->role_id != 3 && Auth::user()->role_id != 6) {
            $satkers = $satkers->where('satuan_kerja_id',Auth::user()->satuan_kerja_id);
        }
        $data = [];
        $no = 0;
        $jumlah = ['eselon_2a_l'=>0,'eselon_2a_p'=>0,'eselon_2b_l'=>0,'eselon_2b_p'=>0,'eselon_3a_l'=>0,'eselon_3a_p'=>0,'eselon_3b_l'=>0,'eselon_3b_p'=>0,'eselon_4a_l'=>0,'eselon_4a_p'=>0,'eselon_4b_l'=>0,'eselon_4b_p'=>0,'eselon_5a_l'=>0,'eselon_5a_p'=>0,'total_s'=>0];
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $satkers->count();
        $satkers = $satkers->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_id')->get();

        foreach ($satkers as $key => $satker) {
            $rekaps = Pegawai::getRekapEselonJK($satker->satuan_kerja_id);
            
            $data[] = [
                ++$no, // NO
                $satker->satuan_kerja_nama,
                $rekaps['eselon_2a_l'],
                $rekaps['eselon_2a_p'],
                $rekaps['eselon_2b_l'],
                $rekaps['eselon_2b_p'],
                $rekaps['eselon_3a_l'],
                $rekaps['eselon_3a_p'],
                $rekaps['eselon_3b_l'],
                $rekaps['eselon_3b_p'],
                $rekaps['eselon_4a_l'],
                $rekaps['eselon_4a_p'],
                $rekaps['eselon_4b_l'],
                $rekaps['eselon_4b_p'],
                $rekaps['eselon_5a_l'],
                $rekaps['eselon_5a_p'],
                $rekaps['total_s'],
            ];
            $jumlah['eselon_2a_l'] += $rekaps['eselon_2a_l'];
            $jumlah['eselon_2a_p'] += $rekaps['eselon_2a_p'];
            $jumlah['eselon_2b_l'] += $rekaps['eselon_2b_l'];
            $jumlah['eselon_2b_p'] += $rekaps['eselon_2b_p'];
            $jumlah['eselon_3a_l'] += $rekaps['eselon_3a_l'];
            $jumlah['eselon_3a_p'] += $rekaps['eselon_3a_p'];
            $jumlah['eselon_3b_l'] += $rekaps['eselon_3b_l'];
            $jumlah['eselon_3b_p'] += $rekaps['eselon_3b_p'];
            $jumlah['eselon_4a_l'] += $rekaps['eselon_4a_l'];
            $jumlah['eselon_4a_p'] += $rekaps['eselon_4a_p'];
            $jumlah['eselon_4b_l'] += $rekaps['eselon_4b_l'];
            $jumlah['eselon_4b_p'] += $rekaps['eselon_4b_p'];
            $jumlah['eselon_5a_l'] += $rekaps['eselon_5a_l'];
            $jumlah['eselon_5a_p'] += $rekaps['eselon_5a_p'];
            $jumlah['total_s'] += $rekaps['total_s'];
            if ($satker->satuan_kerja_id == 3) {
                $unit_kerja_setda = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 1)->get();
                foreach ($unit_kerja_setda as $key => $unit_kerja) {
                    $rekaps = Pegawai::getRekapEselonJKSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                    $data[] = [
                        '', // NO
                        '   '.$unit_kerja->unit_kerja_nama,
                        $rekaps['eselon_2a_l'],
                        $rekaps['eselon_2a_p'],
                        $rekaps['eselon_2b_l'],
                        $rekaps['eselon_2b_p'],
                        $rekaps['eselon_3a_l'],
                        $rekaps['eselon_3a_p'],
                        $rekaps['eselon_3b_l'],
                        $rekaps['eselon_3b_p'],
                        $rekaps['eselon_4a_l'],
                        $rekaps['eselon_4a_p'],
                        $rekaps['eselon_4b_l'],
                        $rekaps['eselon_4b_p'],
                        $rekaps['eselon_5a_l'],
                        $rekaps['eselon_5a_p'],
                        $rekaps['total_s'],
                    ];

                    $unit_kerja_setda_child = UnitKerja::where('satuan_kerja_id',3)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $unit_kerja->unit_kerja_id)->get();
                    
                    foreach ($unit_kerja_setda_child as $key => $unit_kerja) {
                        $rekaps = Pegawai::getRekapEselonJKSetda($satker->satuan_kerja_id, $unit_kerja->unit_kerja_id);
                        $data[] = [
                            '', // NO
                            '      '.$unit_kerja->unit_kerja_nama,
                            $rekaps['eselon_2a_l'],
                            $rekaps['eselon_2a_p'],
                            $rekaps['eselon_2b_l'],
                            $rekaps['eselon_2b_p'],
                            $rekaps['eselon_3a_l'],
                            $rekaps['eselon_3a_p'],
                            $rekaps['eselon_3b_l'],
                            $rekaps['eselon_3b_p'],
                            $rekaps['eselon_4a_l'],
                            $rekaps['eselon_4a_p'],
                            $rekaps['eselon_4b_l'],
                            $rekaps['eselon_4b_p'],
                            $rekaps['eselon_5a_l'],
                            $rekaps['eselon_5a_p'],
                            $rekaps['total_s'],
                        ];

                    }
                }
            }
        }

        Excel::create('Rekap PNS Eselon Jenis Kelamin', function($excel) use ($data) {
            $excel->sheet('Rekap PNS Eselon Jenis Kelamin', function($sheet) use ($data) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->mergeCells('A4:J4');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A3', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('A4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $tahun = date('Y');
                $sheet->row(2, ['REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH']);
                $sheet->row(3, ['DI LINGKUNGAN PEMERINTAH KOTA BANDUNG']);
                $sheet->row(4, ['TAHUN'.$tahun]);

                $sheet->cells('A6:Q7', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });

                $sheet->row(6, ['No', 'SKPD', 'II.a','','II.b','','III.a','','III.b','','IV.a','','IV.b','','V.a','','Total']);
                $sheet->mergeCells('A6:A7');
                $sheet->mergeCells('B6:B7');
                $sheet->mergeCells('C6:D6');
                $sheet->mergeCells('E6:F6');
                $sheet->mergeCells('G6:H6');
                $sheet->mergeCells('I6:J6');
                $sheet->mergeCells('K6:L6');
                $sheet->mergeCells('M6:N6');
                $sheet->mergeCells('O6:P6');
                $sheet->mergeCells('Q6:Q7');

                $sheet->row(7, ['', '', 'L','P','L','P','L','P','L','P','L','P','L','P','L','P','']);
                $sheet->setBorder('A6:Q7', 'thin');
                $sheet->fromArray($data, "", "A8", true, false);

                for ($j = 8; $j < count($data) + 8; $j++) {
                    $sheet->setBorder('A'.$j.':Q'.$j, 'thin');
                }
                
                $sheet->setPageMargin(0.25);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->download('xlsx');

    }

}
