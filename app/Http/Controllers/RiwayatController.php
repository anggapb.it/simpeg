<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use Redirect;
use Validator;
use Input;
use DB;
use File;

use App\Model\Pegawai2;
use App\Model\StatusEditPegawai;
use App\Model\RiwayatPenghargaan2;
use App\Model\RiwayatCuti;
use App\Model\RiwayatKeluarga2;
use App\Model\RiwayatDiklat2;
use App\Model\DiklatStruktural;
use App\Model\DiklatFungsional;
use App\Model\DiklatTeknis;
use App\Model\RiwayatJabatan2;
use App\Model\RiwayatPangkat2;
use App\Model\Kgb_skpd;
use App\Model\KGB;
use App\Model\Pmk;
use App\Model\Pmk_skpd;
use App\Model\RiwayatNonFormal2;
use App\Model\RiwayatPendidikan2;
use App\Model\RiwayatKeahlian2;
use App\Model\RiwayatKeahlianRel2;
use App\Model\RiwayatHukuman2;
use App\Model\FilePegawai2;
use App\Model\PegawaiInaktif;
use App\Model\KepalaSekolah;
use App\Model\PelaksanaStruktural;
use App\Model\RiwayatKedHuk;
use App\Model\AngkaKredit;
use App\Model\SKP;
use App\Model\RiwayatAssessment;
use App\Model\Gaji;
use App\Model\GajiTahun;

class RiwayatController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function EditRiwayatPenghargaan($id, Request $req){
		$rp = RiwayatPenghargaan2::find($id);
		if($rp){
			$rp->penghargaan_id= $req->input('penghargaan_id');
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_penghargaan_instansi= $req->input('riw_penghargaan_instansi');
			$rp->riw_penghargaan_sk = $req->input('riw_penghargaan_sk');
			$rp->riw_penghargaan_tglsk = saveDate($req->input('riw_penghargaan_tglsk'));
			$rp->riw_penghargaan_jabatan = $req->input('riw_penghargaan_jabatan');
			$rp->riw_penghargaan_lokasi = $req->input('riw_penghargaan_lokasi');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Penghargaan',json_encode($rp),$rp->riw_penghargaan_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-penghargaan')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function Getjam(Request $req){
		$input_data = Input::except('_');
		$jenis = $input_data['jenis'] ? $input_data['jenis'] : null;
		$id = $input_data['id'] ? $input_data['id'] : null;
		if($jenis == 1){
			//struktural
			$model = DiklatStruktural::selectRaw('m_spg_diklat_struk_kategori.*,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_nasional as nasional,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_internasional as internasional')->leftjoin('m_spg_diklat_jenis','m_spg_diklat_struk_kategori.diklat_jenis_id','=','m_spg_diklat_jenis.diklat_jenis_id')->find($id);
		}elseif($jenis == 2){
			//teknis
			$model = DiklatTeknis::selectRaw('m_spg_diklat_teknis.*,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_nasional as nasional,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_internasional as internasional')->leftjoin('m_spg_diklat_jenis','m_spg_diklat_teknis.diklat_jenis_id','=','m_spg_diklat_jenis.diklat_jenis_id')->find($id);
		}elseif($jenis == 3){
			//fungsional
			$model = DiklatFungsional::selectRaw('m_spg_diklat_fungsional.*,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_nasional as nasional,m_spg_diklat_jenis.diklat_jenis_keterangan_konversi_jp_internasional as internasional')->leftjoin('m_spg_diklat_jenis','m_spg_diklat_fungsional.diklat_jenis_id','=','m_spg_diklat_jenis.diklat_jenis_id')->find($id);
		}

		echo json_encode($model);
	}

	public function AddRiwayatPenghargaan(Request $req){
		$rp=new RiwayatPenghargaan2;
		//$rp->riw_penghargaan_id = rand();
		$rp->penghargaan_id= $req->input('penghargaan_id');
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_penghargaan_instansi= $req->input('riw_penghargaan_instansi');
		$rp->riw_penghargaan_sk = $req->input('riw_penghargaan_sk');
		$rp->riw_penghargaan_tglsk = saveDate($req->input('riw_penghargaan_tglsk'));
		$rp->riw_penghargaan_jabatan = $req->input('riw_penghargaan_jabatan');
		$rp->riw_penghargaan_lokasi = $req->input('riw_penghargaan_lokasi');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Penghargaan',json_encode($rp),$rp->riw_penghargaan_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-penghargaan')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatSaudara($id, Request $req){
		$rp = RiwayatKeluarga2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_nama= $req->input('riw_nama');
			$rp->nik= $req->input('riw_nik');
			$rp->riw_kelamin = $req->input('riw_kelamin');
			$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
			$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
			$rp->riw_pendidikan = $req->input('riw_pendidikan');
			$rp->riw_pekerjaan =$req->input('riw_pekerjaan');
			$rp->riw_ket = $req->input('riw_ket');

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Saudara',json_encode($rp),$rp->riw_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-saudara')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatSaudara(Request $req){
		$rp=new RiwayatKeluarga2;
		//$rp->riw_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_nama= $req->input('riw_nama');
		$rp->nik= $req->input('riw_nik');
		$rp->riw_kelamin = $req->input('riw_kelamin');
		$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
		$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
		$rp->riw_pendidikan = $req->input('riw_pendidikan');
		$rp->riw_pekerjaan =$req->input('riw_pekerjaan');
		$rp->riw_ket = $req->input('riw_ket');
		$rp->riw_status = 2;
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_penghargaan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Saudara',json_encode($rp),$rp->riw_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-saudara')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatAnak($id, Request $req){
		$rp = RiwayatKeluarga2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_nama= $req->input('riw_nama');
			$rp->nik= $req->input('riw_nik');
			$rp->riw_kelamin = $req->input('riw_kelamin');
			$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
			$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
			$rp->riw_pendidikan = $req->input('riw_pendidikan');
			$rp->riw_pekerjaan = $req->input('riw_pekerjaan');
			$rp->riw_ket = $req->input('riw_ket');
			$rp->riw_status_perkawinan = $req->input('riw_status_perkawinan');
			$rp->riw_status_tunj = $req->input('riw_status_tunj');

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_anak";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_anak";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Anak',json_encode($rp),$rp->riw_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-anak')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatAnak(Request $req){
		$rp=new RiwayatKeluarga2;
		//$rp->riw_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_nama= $req->input('riw_nama');
		$rp->nik= $req->input('riw_nik');
		$rp->riw_kelamin = $req->input('riw_kelamin');
		$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
		$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
		$rp->riw_pendidikan = $req->input('riw_pendidikan');
		$rp->riw_pekerjaan = $req->input('riw_pekerjaan');
		$rp->riw_ket = $req->input('riw_ket');
		$rp->riw_status = 1;
		$rp->riw_status_perkawinan = $req->input('riw_status_perkawinan');
		$rp->riw_status_tunj = $req->input('riw_status_tunj');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_anak";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_anak";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Anak',json_encode($rp),$rp->riw_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-anak')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatPasutri($id, Request $req){
		$rp = RiwayatKeluarga2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_nama= $req->input('riw_nama');
			$rp->nik= $req->input('riw_nik');
			$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
			$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
			$rp->riw_pendidikan = $req->input('riw_pendidikan');
			$rp->riw_pekerjaan = $req->input('riw_pekerjaan');
			$rp->riw_ket = $req->input('riw_ket');
			$rp->riw_tgl_ket = saveDate($req->input('riw_tgl_ket'));
			$rp->riw_status_tunj = $req->input('riw_status_tunj');

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_pasutri";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_pasutri";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Pasutri',json_encode($rp),$rp->riw_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pasutri')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatPasutri(Request $req){
		$rp=new RiwayatKeluarga2;
		//$rp->riw_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_nama= $req->input('riw_nama');
		$rp->nik= $req->input('riw_nik');
		$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
		$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
		$rp->riw_pendidikan = $req->input('riw_pendidikan');
		$rp->riw_pekerjaan = $req->input('riw_pekerjaan');
		$rp->riw_ket = $req->input('riw_ket');
		$rp->riw_tgl_ket = saveDate($req->input('riw_tgl_ket'));
		$rp->riw_status = 4;
		$rp->riw_status_tunj = $req->input('riw_status_tunj');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_pasutri";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_pasutri";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Pasutri',json_encode($rp),$rp->riw_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pasutri')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatOrangTua($id, Request $req){
		$rp = RiwayatKeluarga2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_nama= $req->input('riw_nama');
			$rp->nik= $req->input('riw_nik');
			$rp->riw_kelamin = $req->input('riw_kelamin');
			$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
			$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
			$rp->riw_pendidikan = $req->input('riw_pendidikan');
			$rp->riw_pekerjaan =$req->input('riw_pekerjaan');
			$rp->riw_ket = $req->input('riw_ket');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Orang Tua',json_encode($rp),$rp->riw_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-orang-tua')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function EditRiwayatKepsek($id, Request $req){
		$rp = KepalaSekolah::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->tmt_mulai = $req->input('riw_tmt_mulai');
			$rp->tmt_selesai = $req->input('riw_tmt_selesai');
			$rp->nama_jabatan = $req->input('riw_nama_jabatan');
			$rp->unit_kerja_id = $req->input('riw_unit_kerja');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_kepala_sekolah";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_kepala_sekolah";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Kepala Sekolah',json_encode($rp),$rp->kepala_sekolah_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kepsek')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function EditRiwayatPlt($id, Request $req){
		$rp = PelaksanaStruktural::find($id);
		if($rp){
			$rp->jabatan_id = (!empty($req->jabatan_nama_select) ? $req->jabatan_nama_select : $rp->jabatan_id);
			$rp->tanggal_mulai = saveDate($req->tgl_mulai_plt);
			$rp->tanggal_selesai = saveDate($req->tgl_selesai_plt);
			$rp->jenis = $req->jenis;
			$rp->status = $req->status;
			$rp->jabatan_nama = $req->jabatan_nama;
			$rp->unit_kerja = $req->unit_kerja;
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_plt";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_plt";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat PLT/PLH',json_encode($rp),$rp->rpelaksana_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-plt')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatPlt(Request $req){
		$rp = new PelaksanaStruktural;
		if($rp){
			$rp->peg_id = $req->peg_id;
			$rp->jabatan_id = $req->jabatan_nama_select;
			$rp->tanggal_mulai = saveDate($req->tgl_mulai_plt);
			$rp->tanggal_selesai = saveDate($req->tgl_selesai_plt);
			$rp->jenis = $req->jenis;
			$rp->status = $req->status;
			$rp->jabatan_nama = $req->jabatan_nama;
			$rp->unit_kerja = $req->unit_kerja;
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_plt";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_plt";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat PLT/PLH',json_encode($rp),$rp->rpelaksana_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-plt')->with('message', 'Data Riwayat Telah Ditambahkan');
			}
		}
	}

	public function AddRiwayatKedHuk(Request $req) {
		$rkh = new RiwayatKedHuk;
		if($rkh) {
			$rkh->peg_id = $req->peg_id;
			$rkh->status = $req->status;
			$rkh->no_sk = $req->no_sk;
			$rkh->tgl_sk = saveDate($req->tgl_sk);
			$rkh->tmt = saveDate($req->tmt);
			$rkh->satuan_kerja_id = $req->satuan_kerja_id;
			if($rkh->save()) {
				$pegawai = Pegawai2::find($rkh->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rkh->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_kedhuk";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rkh->peg_id;
					$log->action ="add_riwayat_kedhuk";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Kedudukan Hukum',json_encode($rkh),$rkh->kedhuk_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rkh->peg_id.'#riwayat-kedhuk')->with('message', 'Data Riwayat Telah Ditambahkan');
			}
		}
	}

	public function EditRiwayatKedHuk($id,Request $req) {
		$rkh = RiwayatKedHuk::find($id);
		if($rkh){
			$rkh->status = $req->status;
			$rkh->no_sk = $req->no_sk;
			$rkh->tgl_sk = saveDate($req->tgl_sk);
			$rkh->tmt = saveDate($req->tmt);
			$rkh->satuan_kerja_id = $req->satuan_kerja_id;
			if($rkh->save()){
				$pegawai = Pegawai2::find($rkh->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rkh->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_kedhuk";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rkh->peg_id;
					$log->action ="edit_riwayat_kedhuk";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Kedudukan Hukum',json_encode($rkh),$rkh->kedhuk_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rkh->peg_id.'#riwayat-kedhuk')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatCuti(Request $req){
		$rp = new RiwayatCuti;
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->jeniscuti_id = $req->input('riw_jenis_cuti');
			$rp->cuti_no = $req->input('riw_cuti_surat');
			$rp->cuti_tgl = $req->input('riw_cuti_tanggal');
			$rp->cuti_mulai = $req->input('riw_cuti_mulai');
			$rp->cuti_selesai = $req->input('riw_cuti_selesai');
			$rp->cuti_ket = $req->input('riw_cuti_ket');
			if($rp->save()){
				$in = new PegawaiInaktif;
				$in->peg_id = $req->input('peg_id');
				$in->inaktif_mulai = $req->input('riw_cuti_mulai');
				$in->inaktif_selesai = $req->input('riw_cuti_selesai');
				$cari = DB::connection('pgsql2')->table('m_spg_jenis_cuti')->where('jeniscuti_id',$req->input('riw_jenis_cuti'))->first();
				$in->inaktif_keterangan = $cari ? $cari->jeniscuti_nm : '';
				$in->save();

				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_cuti";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_cuti";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Cuti',json_encode($rp),$rp->cuti_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-cuti')->with('message', 'Data Riwayat Telah Ditambahkan');
			}
		}
	}

	public function EditRiwayatCuti($id,Request $req){
		$rp = RiwayatCuti::find($id);
		if($rp){
			$caris = DB::connection('pgsql2')->table('m_spg_jenis_cuti')->where('jeniscuti_id',$rp->jeniscuti_id)->first();
			$cari = PegawaiInaktif::where('peg_id',$req->input('peg_id'))->where('inaktif_mulai',$rp->cuti_mulai)->where('inaktif_selesai',$rp->cuti_selesai)->where('inaktif_keterangan',$caris->jeniscuti_nm)->first();
			if($cari){
				$in = PegawaiInaktif::find($cari->id);
			}else{
				$in = new PegawaiInaktif;
			}
			$in->peg_id = $req->input('peg_id');
			$in->inaktif_mulai = $req->input('riw_cuti_mulai');
			$in->inaktif_selesai = $req->input('riw_cuti_selesai');
			$carix = DB::connection('pgsql2')->table('m_spg_jenis_cuti')->where('jeniscuti_id',$req->input('riw_jenis_cuti'))->first();
			$in->inaktif_keterangan = $carix ? $carix->jeniscuti_nm : '';
			$in->save();

			$rp->peg_id = $req->input('peg_id');
			$rp->jeniscuti_id = $req->input('riw_jenis_cuti');
			$rp->cuti_no = $req->input('riw_cuti_surat');
			$rp->cuti_tgl = $req->input('riw_cuti_tanggal');
			$rp->cuti_mulai = $req->input('riw_cuti_mulai');
			$rp->cuti_selesai = $req->input('riw_cuti_selesai');
			$rp->cuti_ket = $req->input('riw_cuti_ket');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_cuti";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_cuti";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Cuti',json_encode($rp),$id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-cuti')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatKepsek(Request $req){
		$rp=new KepalaSekolah;
		//$rp->riw_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->tmt_mulai = $req->input('riw_tmt_mulai');
		$rp->tmt_selesai = $req->input('riw_tmt_selesai');
		$rp->nama_jabatan = $req->input('riw_nama_jabatan');
		$rp->unit_kerja_id = $req->input('riw_unit_kerja');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_kepala_sekolah";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_kepala_sekolah";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Tambah Riwayat Kepala Sekolah',json_encode($rp),$rp->kepala_sekolah_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kepsek')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function AddRiwayatOrangTua(Request $req){
		$rp=new RiwayatKeluarga2;
		//$rp->riw_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_nama= $req->input('riw_nama');
		$rp->nik= $req->input('riw_nik');
		$rp->riw_kelamin = $req->input('riw_kelamin');
		$rp->riw_tgl_lahir =saveDate($req->input('riw_tgl_lahir'));
		$rp->riw_tempat_lahir = $req->input('riw_tempat_lahir');
		$rp->riw_pendidikan = $req->input('riw_pendidikan');
		$rp->riw_pekerjaan =$req->input('riw_pekerjaan');
		$rp->riw_ket = $req->input('riw_ket');
		$rp->riw_status = 3;
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_saudara";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Tambah Riwayat Keluarga',json_encode($rp),$rp->riw_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-orang-tua')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatDiklatTeknis($id, Request $req){
		$rp = RiwayatDiklat2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->diklat_jenis = $req->input('diklat_jenis');
			$rp->diklat_teknis_id= $req->input('diklat_teknis_id');
			$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
			$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
			$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
			$rp->diklat_sttp_no = trim($req->input('diklat_sttp_no'));
			$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
			$rp->diklat_sttp_pej = trim($req->input('diklat_sttp_pej'));
			$rp->diklat_penyelenggara = trim($req->input('diklat_penyelenggara'));
			$rp->diklat_tempat = $req->input('diklat_tempat');
			
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_diklat_teknis";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_diklat_teknis";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Diklat Teknis',json_encode($rp),$rp->diklat_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-teknis')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatDiklatTeknis(Request $req){
		$rp=new RiwayatDiklat2;
		//$rp->diklat_id =rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->diklat_jenis = $req->input('diklat_jenis');
		$rp->diklat_teknis_id= $req->input('diklat_teknis_id');
		$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
		$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
		$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
		$rp->diklat_sttp_no = trim($req->input('diklat_sttp_no'));
		$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
		$rp->diklat_sttp_pej = trim($req->input('diklat_sttp_pej'));
		$rp->diklat_penyelenggara = trim($req->input('diklat_penyelenggara'));
		$rp->diklat_tempat = $req->input('diklat_tempat');
			
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_diklat_teknis";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_diklat_teknis";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Tambah Riwayat Diklat Teknis',json_encode($rp),$rp->diklat_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-teknis')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}
	public function EditRiwayatDiklatFungsional($id, Request $req){
		$rp = RiwayatDiklat2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->diklat_jenis = $req->input('diklat_jenis');
			$rp->diklat_fungsional_id= $req->input('diklat_fungsional_id');
			$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
			$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
			$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
			$rp->diklat_sttp_no = $req->input('diklat_sttp_no');
			$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
			$rp->diklat_sttp_pej = $req->input('diklat_sttp_pej');
			$rp->diklat_penyelenggara = $req->input('diklat_penyelenggara');
			$rp->diklat_tempat = $req->input('diklat_tempat');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_diklat_fungsional";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_diklat_fungsional";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Diklat Fungsional',json_encode($rp),$rp->diklat_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-fungsional')->with('message', 'Data Riwayat Telah Diupdate');
			}		
		}
	}

	public function AddRiwayatDiklatFungsional(Request $req){
		$rp=new RiwayatDiklat2;
		//$rp->diklat_id =rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->diklat_jenis = $req->input('diklat_jenis');
		$rp->diklat_fungsional_id= $req->input('diklat_fungsional_id');
		$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
		$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
		$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
		$rp->diklat_sttp_no = $req->input('diklat_sttp_no');
		$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
		$rp->diklat_sttp_pej = $req->input('diklat_sttp_pej');
		$rp->diklat_penyelenggara = $req->input('diklat_penyelenggara');
		$rp->diklat_tempat = $req->input('diklat_tempat');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_diklat_fungsional";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_diklat_fungsional";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Diklat Fungsional',json_encode($rp),$rp->diklat_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-fungsional')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatDiklatStruktural($id, Request $req){
		$rp = RiwayatDiklat2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->diklat_jenis = $req->input('diklat_jenis');
			$rp->kategori_id= $req->input('kategori_id');
			$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
			$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
			$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
			$rp->diklat_sttp_no = $req->input('diklat_sttp_no');
			$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
			$rp->diklat_sttp_pej = $req->input('diklat_sttp_pej');
			$rp->diklat_penyelenggara = $req->input('diklat_penyelenggara');
			$rp->diklat_tempat = $req->input('diklat_tempat');

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_diklat_struktural";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_diklat_struktural";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Diklat Struktural',json_encode($rp),$rp->diklat_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-struktural')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatDiklatStruktural(Request $req){
		$rp=new RiwayatDiklat2;
		//$rp->diklat_id =rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->diklat_jenis = $req->input('diklat_jenis');
		$rp->kategori_id= $req->input('kategori_id');
		$rp->diklat_mulai = saveDate($req->input('diklat_mulai'));
		$rp->diklat_selesai = saveDate($req->input('diklat_selesai'));
		$rp->diklat_jumlah_jam = $req->input('diklat_jumlah_jam');
		$rp->diklat_sttp_no = $req->input('diklat_sttp_no');
		$rp->diklat_sttp_tgl =  saveDate($req->input('diklat_sttp_tgl'));
		$rp->diklat_sttp_pej = $req->input('diklat_sttp_pej');
		$rp->diklat_penyelenggara = $req->input('diklat_penyelenggara');
		$rp->diklat_tempat = $req->input('diklat_tempat');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_diklat_struktural";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_diklat_struktural";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Diklat Struktural',json_encode($rp),$rp->diklat_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-diklat-struktural')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatJabatan($id, Request $req){
		$rp = RiwayatJabatan2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_jabatan_nm= $req->input('riw_jabatan_nm');
			$rp->riw_jabatan_no = $req->input('riw_jabatan_no');
			$rp->riw_jabatan_pejabat = $req->input('riw_jabatan_pejabat');
			$rp->riw_jabatan_tgl = saveDate($req->input('riw_jabatan_tgl'));
			$rp->riw_jabatan_tmt =  saveDate($req->input('riw_jabatan_tmt'));
			$rp->riw_jabatan_unit = $req->input('riw_jabatan_unit');
			$rp->gol_id = $req->input('gol_id_ruang') ? $req->input('gol_id_ruang') : null ;
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_jabatan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_jabatan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Jabatan',json_encode($rp),$rp->riw_jabatan_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-jabatan')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatJabatan(Request $req){
		$rp=new RiwayatJabatan2;
		//$rp->riw_jabatan_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_jabatan_nm= $req->input('riw_jabatan_nm');
		$rp->riw_jabatan_no = $req->input('riw_jabatan_no');
		$rp->riw_jabatan_pejabat = $req->input('riw_jabatan_pejabat');
		$rp->riw_jabatan_tgl = saveDate($req->input('riw_jabatan_tgl'));
		$rp->riw_jabatan_tmt =  saveDate($req->input('riw_jabatan_tmt'));
		$rp->riw_jabatan_unit = $req->input('riw_jabatan_unit');
		$rp->gol_id = $req->input('gol_id_ruang') ? $req->input('gol_id_ruang') : null;
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_jabatan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_jabatan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Tambah Riwayat Jabatan',json_encode($rp),$rp->riw_jabatan_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-jabatan')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
		
	}

	public function EditRiwayatPangkat($id, Request $req){
		$rp = RiwayatPangkat2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_pangkat_thn= $req->input('riw_pangkat_thn') ? $req->input('riw_pangkat_thn') : 0;
			$rp->riw_pangkat_bln =$req->input('riw_pangkat_bln') ? $req->input('riw_pangkat_bln') : 0;
			$rp->riw_pangkat_gapok = $req->input('riw_pangkat_gapok')  ? $req->input('riw_pangkat_gapok') : 0;
			$rp->riw_pangkat_sk = $req->input('riw_pangkat_sk');
			$rp->riw_pangkat_sktgl =  saveDate($req->input('riw_pangkat_sktgl'));
			$rp->riw_pangkat_tmt =  saveDate($req->input('riw_pangkat_tmt'));
			$rp->riw_pangkat_pejabat = $req->input('riw_pangkat_pejabat');
			$rp->riw_pangkat_unit_kerja = $req->input('riw_pangkat_unit_kerja');
			$rp->gol_id = $req->input('gol_ruang_id');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_pangkat";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_pangkat";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Golongan',json_encode($rp),$rp->riw_pangkat_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kepangkatan')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatPangkat(Request $req){
		$rp=new RiwayatPangkat2;
		//$rp->riw_pangkat_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_pangkat_thn= $req->input('riw_pangkat_thn') ? $req->input('riw_pangkat_thn') : 0;
		$rp->riw_pangkat_bln =$req->input('riw_pangkat_bln') ? $req->input('riw_pangkat_bln') :0;
		$rp->riw_pangkat_gapok = $req->input('riw_pangkat_gapok')  ? $req->input('riw_pangkat_gapok') : 0;
		$rp->riw_pangkat_sk = $req->input('riw_pangkat_sk');
		$rp->riw_pangkat_sktgl =  saveDate($req->input('riw_pangkat_sktgl'));
		$rp->riw_pangkat_tmt =  saveDate($req->input('riw_pangkat_tmt'));
		$rp->riw_pangkat_pejabat = $req->input('riw_pangkat_pejabat');
		$rp->riw_pangkat_unit_kerja = $req->input('riw_pangkat_unit_kerja');
		$rp->gol_id = $req->input('gol_ruang_id');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_pangkat";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_pangkat";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Golongan',json_encode($rp),$rp->riw_pangkat_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kepangkatan')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatPendidikanNon($id, Request $req){
		$rp = RiwayatNonFormal2::find($id);
		if($rp){
			$rp->peg_id =  $req->input('peg_id');
			$rp->non_nama=  $req->input('non_nama');
			$rp->non_tgl_mulai = saveDate( $req->input('non_tgl_mulai'));
			$rp->non_tgl_selesai = saveDate( $req->input('non_tgl_selesai'));
			$rp->non_sttp =  $req->input('non_sttp');
			$rp->non_sttp_tanggal =  saveDate( $req->input('non_sttp_tanggal'));
			$rp->non_sttp_pejabat =  $req->input('non_sttp_pejabat');
			$rp->non_penyelenggara = $req->input('non_penyelenggara');
			$rp->non_tempat =  $req->input('non_tempat');			
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_pendidikan_nonformal";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_pendidikan_nonformal";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Pendidikan Non Formal',json_encode($rp),$rp->non_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pendidikan-nonformal')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function AddRiwayatPendidikanNon(Request $req){
		$rp=new  RiwayatNonFormal2;
		//$rp->non_id = rand();
		$rp->peg_id =  $req->input('peg_id');
		$rp->non_nama=  $req->input('non_nama');
		$rp->non_tgl_mulai = saveDate( $req->input('non_tgl_mulai'));
		$rp->non_tgl_selesai = saveDate( $req->input('non_tgl_selesai'));
		$rp->non_sttp =  $req->input('non_sttp');
		$rp->non_sttp_tanggal =  saveDate( $req->input('non_sttp_tanggal'));
		$rp->non_sttp_pejabat =  $req->input('non_sttp_pejabat');
		$rp->non_penyelenggara = $req->input('non_penyelenggara');
		$rp->non_tempat =  $req->input('non_tempat');				
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_pendidikan_nonformal";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_pendidikan_nonformal";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Pendidikan Non Formal',json_encode($rp),$rp->non_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pendidikan-nonformal')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatPendidikan($id, Request $req){
		$rp = RiwayatPendidikan2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->tingpend_id= $req->input('tingpend_id') ? $req->input('tingpend_id') : null;
			$rp->fakultas_id= $req->input('fakultas_id') ? $req->input('fakultas_id') : null;
			$rp->jurusan_id= $req->input('jurusan_id') ? $req->input('jurusan_id') : null;
			$rp->univ_id= $req->input('univ_id') ? $req->input('univ_id') : null;
			$rp->riw_pendidikan_sttb_ijazah= $req->input('riw_pendidikan_sttb_ijazah');
			$rp->riw_pendidikan_tgl = saveDate($req->input('riw_pendidikan_tgl'));
			$rp->riw_pendidikan_pejabat = $req->input('riw_pendidikan_pejabat');
			$rp->riw_pendidikan_nm = $req->input('riw_pendidikan_nm');
			$rp->riw_pendidikan_lokasi = $req->input('riw_pendidikan_lokasi');
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_pendidikan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_pendidikan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Pendidikan',json_encode($rp),$rp->riw_pendidikan_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pendidikan-formal')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function addRiwayatPendidikan(Request $req){
		$rp=new  RiwayatPendidikan2;
		//$rp->riw_pendidikan_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->tingpend_id= $req->input('tingpend_id') ? $req->input('tingpend_id') : null;
		$rp->fakultas_id= $req->input('fakultas_id') ? $req->input('fakultas_id') : null;
		$rp->jurusan_id= $req->input('jurusan_id') ? $req->input('jurusan_id') : null;
		$rp->univ_id= $req->input('univ_id') ? $req->input('univ_id') : null;
		$rp->riw_pendidikan_sttb_ijazah= $req->input('riw_pendidikan_sttb_ijazah');
		$rp->riw_pendidikan_tgl = saveDate($req->input('riw_pendidikan_tgl'));
		$rp->riw_pendidikan_pejabat = $req->input('riw_pendidikan_pejabat');
		$rp->riw_pendidikan_nm = $req->input('riw_pendidikan_nm');
		$rp->riw_pendidikan_lokasi = $req->input('riw_pendidikan_lokasi');
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_pendidikan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_pendidikan";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Pendidikan',json_encode($rp),$rp->riw_pendidikan_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pendidikan-formal')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function editRiwayatKeahlian($id, Request $req){
		$rp = RiwayatKeahlian2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->keahlian_id= $req->input('keahlian_id') ? $req->input('keahlian_id') : null;
			$rp->keahlian_level_id= $req->input('keahlian_level_id') ? $req->input('keahlian_level_id') : null;
			$rp->riw_keahlian_sejak = saveDate($req->input('riw_keahlian_sejak'));
			if($rp->save()){
				$relasi = RiwayatKeahlianRel2::where('riw_keahlian_id', $id)->first();
				if($relasi){
					$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id') ? $req->input('riw_pendidikan_id') : null;
					$relasi->non_id = $req->input('non_id') ? $req->input('non_id') : null;
					$relasi->diklat_id = $req->input('diklat_id') ? $req->input('diklat_id') : null;
					$relasi->predikat = $req->input('predikat');
				}else{
					$relasi = new RiwayatKeahlianRel2;
					$relasi->riw_keahlian_id = $rp->riw_keahlian_id;
					$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id') ? $req->input('riw_pendidikan_id') : null;
					$relasi->non_id = $req->input('non_id') ? $req->input('non_id') : null;
					$relasi->diklat_id = $req->input('diklat_id') ? $req->input('diklat_id') : null;
					$relasi->predikat = $req->input('predikat');	
				}
				$relasi->save();

				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_keahlian";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_keahlian";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Keahlian',json_encode($rp),$rp->riw_keahlian_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-tambahan')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function addRiwayatKeahlian(Request $req){
		$rp=new  RiwayatKeahlian2;
		//$rp->riw_keahlian_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->keahlian_id= $req->input('keahlian_id') ? $req->input('keahlian_id') : null;
		$rp->keahlian_level_id= $req->input('keahlian_level_id') ? $req->input('keahlian_level_id') : null;
		$rp->riw_keahlian_sejak = saveDate($req->input('riw_keahlian_sejak'));
		if($rp->save()){
			$relasi = new RiwayatKeahlianRel2;
			$relasi->riw_keahlian_id = $rp->riw_keahlian_id;
			$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id') ? $req->input('riw_pendidikan_id') : null;
			$relasi->non_id = $req->input('non_id') ? $req->input('non_id') : null;
			$relasi->diklat_id = $req->input('diklat_id') ? $req->input('diklat_id') : null;
			$relasi->predikat = $req->input('predikat');
			$relasi->save();

			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_keahlian";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_keahlian";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Keahlian',json_encode($rp),$rp->riw_keahlian_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-tambahan')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function editRiwayatBahasa($id, Request $req){
		$rp = RiwayatKeahlian2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->keahlian_id= $req->input('keahlian_id_bahasa') ? $req->input('keahlian_id_bahasa') : null;
			$lv1 = $req->input('keahlian_level_id_1') ? $req->input('keahlian_level_id_1') : '00';
			$lv2 = $req->input('keahlian_level_id_2') ? $req->input('keahlian_level_id_2') : '00';
			$lv3 = $req->input('keahlian_level_id_3') ? $req->input('keahlian_level_id_3') : '00';
			$lv4 = $req->input('keahlian_level_id_4') ? $req->input('keahlian_level_id_4') : '00';

			if($lv4!= '00'){
				$lv4 = '0'.$lv4;
			}

			if($lv3 != '00' && $lv3 == 9){
				$lv3 = '0'.$lv3;
			}

			$rp->keahlian_level_id= $lv1.$lv2.$lv3.$lv4;
			$rp->riw_keahlian_sejak = saveDate($req->input('riw_keahlian_sejak_bahasa'));
			if($rp->save()){
				$relasi = RiwayatKeahlianRel2::where('riw_keahlian_id', $id)->first();
				if($relasi){
					$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id_bahasa') ? $req->input('riw_pendidikan_id_bahasa') : null;
					$relasi->non_id = $req->input('non_id_bahasa') ? $req->input('non_id_bahasa') : null;
					$relasi->diklat_id = $req->input('diklat_id_bahasa') ? $req->input('diklat_id_bahasa') : null;
					$relasi->predikat = $req->input('predikat_bahasa');
				}else{
					$relasi = new RiwayatKeahlianRel2;
					$relasi->riw_keahlian_id = $rp->riw_keahlian_id;
					$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id_bahasa') ? $req->input('riw_pendidikan_id_bahasa') : null;
					$relasi->non_id = $req->input('non_id_bahasa') ? $req->input('non_id_bahasa') : null;
					$relasi->diklat_id = $req->input('diklat_id_bahasa') ? $req->input('diklat_id_bahasa') : null;
					$relasi->predikat = $req->input('predikat_bahasa');	
				}
				$relasi->save();

				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_bahasa";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_bahasa";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Keahlian Bahasa',json_encode($rp),$rp->riw_keahlian_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-tambahan')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function addRiwayatBahasa(Request $req){
		$rp=new  RiwayatKeahlian2;
		//$rp->riw_keahlian_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->keahlian_id= $req->input('keahlian_id_bahasa') ? $req->input('keahlian_id_bahasa') : null;

		$lv1 = $req->input('keahlian_level_id_1') ? $req->input('keahlian_level_id_1') : '00';
		$lv2 = $req->input('keahlian_level_id_2') ? $req->input('keahlian_level_id_2') : '00';
		$lv3 = $req->input('keahlian_level_id_3') ? $req->input('keahlian_level_id_3') : '00';
		$lv4 = $req->input('keahlian_level_id_4') ? $req->input('keahlian_level_id_4') : '00';

		if($lv4!= '00'){
			$lv4 = '0'.$lv4;
		}

		if($lv3 != '00' && $lv3 == 9){
			$lv3 = '0'.$lv3;
		}

		$rp->keahlian_level_id= $lv1.$lv2.$lv3.$lv4;
		$rp->riw_keahlian_sejak = saveDate($req->input('riw_keahlian_sejak_bahasa'));
		if($rp->save()){
			$relasi = new RiwayatKeahlianRel2;
			$relasi->riw_keahlian_id = $rp->riw_keahlian_id;
			$relasi->riw_pendidikan_id = $req->input('riw_pendidikan_id_bahasa') ? $req->input('riw_pendidikan_id_bahasa') : null;
			$relasi->non_id = $req->input('non_id_bahasa') ? $req->input('non_id_bahasa') : null;
			$relasi->diklat_id = $req->input('diklat_id_bahasa') ? $req->input('diklat_id_bahasa') : null;
			$relasi->predikat = $req->input('predikat_bahasa');
			$relasi->save();

			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_bahasa";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_bahasa";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Keahlian Bahasa',json_encode($rp),$rp->riw_keahlian_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-tambahan')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatHukuman($id, Request $req){
		$rp = RiwayatHukuman2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->peg_id = $req->input('peg_id');
			$rp->riw_hukum_sk= $req->input('riw_hukum_sk') ? $req->input('riw_hukum_sk') : null;
			$rp->riw_hukum_ket= $req->input('riw_hukum_ket') ? $req->input('riw_hukum_ket') : null;
			$rp->mhukum_id= $req->input('mhukum_id') ? $req->input('mhukum_id') : null;
			$rp->riw_hukum_tmt = saveDate($req->input('riw_hukum_tmt'));
			$rp->riw_hukum_sd = saveDate($req->input('riw_hukum_sd'));
			$rp->riw_hukum_tgl = saveDate($req->input('riw_hukum_tgl'));
			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_hukuman";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_hukuman";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Hukdis',json_encode($rp),$rp->riw_hukum_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-hukuman')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function addRiwayatHukuman(Request $req){
		$rp=new  RiwayatHukuman2;
		//$rp->riw_hukum_id = rand();
		$rp->peg_id = $req->input('peg_id');
		$rp->riw_hukum_sk= $req->input('riw_hukum_sk') ? $req->input('riw_hukum_sk') : null;
		$rp->riw_hukum_ket= $req->input('riw_hukum_ket') ? $req->input('riw_hukum_ket') : null;
		$rp->mhukum_id= $req->input('mhukum_id') ? $req->input('mhukum_id') : null;
		$rp->riw_hukum_tmt = saveDate($req->input('riw_hukum_tmt'));
		$rp->riw_hukum_sd = saveDate($req->input('riw_hukum_sd'));
		$rp->riw_hukum_tgl = saveDate($req->input('riw_hukum_tgl'));
		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_hukuman";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_hukuman";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Hukdis',json_encode($rp),$rp->riw_hukum_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-hukuman')->with('message', 'Data Riwayat Telah Diupdate');
		}
	}

	public function EditFilePegawai(Request $req, $id){
		$rp=FilePegawai2::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			$rp->file_nama= $req->input('file_nama');
			$rp->file_ket = $req->input('file_ket');
			$rp->file_tgl = date('Y-m-d H:i:s');
			$rp->file_lokasi = $req->input('filename');

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
					if($log){
						$log->action ="edit_file";
						$log->status_id = 1;
						$log->editor_id = Auth::user()->id;
						$log->save();
					}else{
						$log = new StatusEditPegawai;
						$log->peg_nip = $pegawai['peg_nip'];
						$log->peg_id = $rp->peg_id;
						$log->action ="edit_file";
						$log->status_id = 1;
						$log->editor_id = Auth::user()->id;
						$log->save();
					}
				logAction('Edit File Pegawai',json_encode($rp),$rp->file_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#file-pegawai')->with('message', 'File Telah Terupdate');
			}
		}	
	}

	public function AddFilePegawai(Request $req){
		$rp=new FilePegawai2;
		$rp->peg_id = $req->input('peg_id');
		$rp->file_nama= $req->input('file_nama');
		$rp->file_ket = $req->input('file_ket');
		$rp->file_tgl = date('Y-m-d H:i:s');
		$rp->file_lokasi = $req->input('filename');

		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_file";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_file";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah File Pegawai',json_encode($rp),$rp->file_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#file-pegawai')->with('message', 'File Telah Ditambahkan');
		}
	}

	public function uploadFile(Request $req){
		$file = Input::file('files');
		$pegawai = Pegawai2::find($req->peg_id);
		if($file){
			$data = [];
			$destinationPath = 'uploads/file'; // upload path
		    $extension = Input::file('files')->getClientOriginalExtension(); // getting file extension
	        $filename = $pegawai->peg_nip.'_'.$req->file_nama.'.' . $extension; // renameing image
		    $upload_success = $file->move($destinationPath, $filename); // uploading file to given path

		    $data = [
		    	'message' => 'success',
		    	'data' =>	$filename,
		    ];
		    if($upload_success){
		    	return response()->json($data);
			}
		}
	}

	public function deleteRiwayatPenghargaan($peg_id, $id){
		$del = RiwayatPenghargaan2::where('peg_id', $peg_id)->where('riw_penghargaan_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_penghargaan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_penghargaan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Penghargaan','',$id,Auth::user()->username);

			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-penghargaan')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatKeluarga($peg_id, $id){
		$del = RiwayatKeluarga2::where('peg_id', $peg_id)->where('riw_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_keluarga";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_keluarga";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Keluarga','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id)->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteKepsek($peg_id, $id){
		$del = KepalaSekolah::where('peg_id', $peg_id)->where('kepala_sekolah_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_kepala_sekolah";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_riwayat_kepala_sekolah";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Kepala Sekolah','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id)->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteKedHuk($peg_id, $id) {
		$del = RiwayatKedHuk::where('kedhuk_id',$id)->delete();

		if($del) {
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_kedhuk";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_riwayat_kedhuk";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Kedudukan Hukum','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-kedhuk')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deletePlt($peg_id, $id){
		$del = PelaksanaStruktural::where('peg_id', $peg_id)->where('rpelaksana_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_plt";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_riwayat_plt";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat PLT/PLH','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-plt')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteCuti($peg_id, $id){
		$rp = RiwayatCuti::find($id);
		$caris = DB::connection('pgsql2')->table('m_spg_jenis_cuti')->where('jeniscuti_id',$rp->jeniscuti_id)->first();
		$cari = PegawaiInaktif::where('peg_id',$peg_id)->where('inaktif_mulai',$rp->cuti_mulai)->where('inaktif_selesai',$rp->cuti_selesai)->where('inaktif_keterangan',$caris->jeniscuti_nm)->delete();

		if($rp->delete()){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_cuti";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_riwayat_cuti";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Cuti','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id)->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatDiklat($peg_id, $id){
		$del = RiwayatDiklat2::where('peg_id', $peg_id)->where('diklat_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_diklat";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_diklat";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Diklat','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id)->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatJabatan($peg_id, $id){
		$del = RiwayatJabatan2::where('peg_id', $peg_id)->where('riw_jabatan_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_jabatan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_jabatan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Jabatan','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-jabatan')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatPangkat($peg_id, $id){
		$del = RiwayatPangkat2::where('peg_id', $peg_id)->where('riw_pangkat_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_kepangkatan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_kepangkatan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Golongan','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-kepangkatan')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatPendidikan($peg_id, $id){
		$del = RiwayatPendidikan2::where('peg_id', $peg_id)->where('riw_pendidikan_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_pendidikan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_pendidikan";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Pendidikan','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-pendidikan')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatPendidikanNon($peg_id, $id){
		$del = RiwayatNonFormal2::where('peg_id', $peg_id)->where('non_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_pendidikan_nonformal";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_pendidikan_nonformal";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Pendidikan Non Formal','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-pendidikan-nonformal')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatKeahlian($peg_id, $id){
		$del = RiwayatKeahlian2::where('peg_id', $peg_id)->where('riw_keahlian_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_keahlian";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_keahlian";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			RiwayatKeahlianRel2::where('riw_keahlian_id', $id)->delete();
			logAction('Hapus Riwayat Keahlian','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-tambahan')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteRiwayatHukuman($peg_id, $id){
		$del = RiwayatHukuman2::where('peg_id', $peg_id)->where('riw_hukum_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_hukuman";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_hukuman";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Hukdis','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-hukuman')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function deleteFilePegawai($peg_id, $id){
		$del = FilePegawai2::where('peg_id', $peg_id)->where('file_id', $id)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_file";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_file";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus File Pegawai','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#file-pegawai')->with('message', 'File Telah di hapus');
		}
	}

	public function downloadFilePegawai($peg_id, $id){
		$model = FilePegawai2::where('peg_id', $peg_id)->where('file_id', $id)->first();

		if(!$model->file_lokasi){ // file does not exist
		    return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#file-pegawai')->with('message', 'File Tidak Ditemukan');
		} else {
	        if(File::exists('uploads/file/'.$model->file_lokasi)){
				$file = 'uploads/file/'.$model->file_lokasi;
				$data = File::get($file);
				
				header('Content-Description: File Transfer');
		        header('Content-Type: application/octet-stream');
		        header('Content-Disposition: attachment; filename='.basename($file));
		        header('Content-Transfer-Encoding: binary');
		        header('Expires: 0');
		        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		        header('Pragma: public');
		        header('Content-Length: ' . filesize($file));
		        ob_clean();
		        flush();
		        readfile($file);
		        exit;
			}
		}
	}

	public function AddAngkaKredit(Request $req) {
		$ak = new AngkaKredit;
		if($ak) {
			$ak->peg_id = $req->peg_id;
			$ak->no_sk_pak = $req->no_sk_pak;
			$ak->tgl_sk_pak = saveDate($req->tgl_sk_pak);
			$ak->tgl_mulai = saveDate($req->tgl_mulai);
			$ak->tgl_selesai = saveDate($req->tgl_selesai);
			$ak->kredit_utama = $req->kredit_utama;
			$ak->kredit_penunjang = $req->kredit_penunjang;
			$ak->kredit_total = $req->kredit_total;
			if($ak->save()) {
				$pegawai = Pegawai2::find($ak->peg_id);
				$log=StatusEditPegawai::where('peg_id', $ak->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_angka_kredit";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $ak->peg_id;
					$log->action ="add_angka_kredit";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Angka Kredit',json_encode($ak),$ak->angka_kredit_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$ak->peg_id.'#angka-kredit')->with('message', 'Data Angka Kredit Telah Ditambahkan');
			}
		}
	}

	public function EditAngkaKredit($id,Request $req) {
		$ak = AngkaKredit::find($id);
		if($ak){
			$ak->no_sk_pak = $req->no_sk_pak;
			$ak->tgl_sk_pak = saveDate($req->tgl_sk_pak);
			$ak->tgl_mulai = saveDate($req->tgl_mulai);
			$ak->tgl_selesai = saveDate($req->tgl_selesai);
			$ak->kredit_utama = $req->kredit_utama;
			$ak->kredit_penunjang = $req->kredit_penunjang;
			$ak->kredit_total = $req->kredit_total;
			if($ak->save()){
				$pegawai = Pegawai2::find($ak->peg_id);
				$log=StatusEditPegawai::where('peg_id', $ak->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_angka_kredit";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $ak->peg_id;
					$log->action ="edit_angka_kredit";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Angka Kredit',json_encode($ak),$ak->angka_kredit_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$ak->peg_id.'#angka-kredit')->with('message', 'Data Angka Kredit Telah Diupdate');
			}
		}
	}

	public function deleteAngkaKredit($peg_id, $id) {
		$del = AngkaKredit::where('angka_kredit_id',$id)->delete();

		if($del) {
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_angka_kredit";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_angka_kredit";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Angka Kredit','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#angka-kredit')->with('message', 'Angka Kredit Berhasil dihapus');
		}
	}

	public function AddSKP(Request $req) {
		$skp = new SKP;
		if($skp) {
			$skp->peg_id = $req->peg_id;
			$skp->tahun = $req->tahun;
			$skp->jenis_skp = $req->jenis_skp;
			$skp->nilai_capaian = $req->nilai_capaian;
			$skp->orientasi_pelayanan = $req->orientasi_pelayanan;
			$skp->integritas = $req->integritas;
			$skp->komitmen = $req->komitmen;
			$skp->disiplin = $req->disiplin;
			$skp->kerjasama = $req->kerjasama;
			$skp->kepemimpinan = $req->kepemimpinan ? $req->kepemimpinan : '0';
			if($skp->save()) {
				$pegawai = Pegawai2::find($skp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $skp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_skp";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $skp->peg_id;
					$log->action ="add_skp";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Sasaran Kerja Pegawai',json_encode($skp),$skp->skp_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$skp->peg_id.'#skp')->with('message', 'Data Sasaran Kerja Pegawai Telah Ditambahkan');
			}
		}
	}

	public function EditSKP($id,Request $req) {
		$skp = SKP::find($id);
		if($skp){
			$skp->tahun = $req->tahun;
			$skp->jenis_skp = $req->jenis_skp;
			$skp->nilai_capaian = $req->nilai_capaian;
			$skp->orientasi_pelayanan = $req->orientasi_pelayanan;
			$skp->integritas = $req->integritas;
			$skp->komitmen = $req->komitmen;
			$skp->disiplin = $req->disiplin;
			$skp->kerjasama = $req->kerjasama;
			$skp->kepemimpinan = $req->kepemimpinan ? $req->kepemimpinan : '0';
			if($skp->save()){
				$pegawai = Pegawai2::find($skp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $skp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_skp";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $skp->peg_id;
					$log->action ="edit_skp";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Sasaran Kerja Pegawai',json_encode($skp),$skp->skp_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$skp->peg_id.'#skp')->with('message', 'Data Sasaran Kerja Pegawai Telah Diupdate');
			}
		}
	}

	public function deleteSKP($peg_id, $id) {
		$del = SKP::where('skp_id',$id)->delete();

		if($del) {
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_skp";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_skp";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Sasaran Kerja Pegawai','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#skp')->with('message', 'Sasaran Kerja Pegawai Berhasil dihapus');
		}
	}

	public function AddAssessment(Request $req) {
		$riwayatAssessment = new RiwayatAssessment;
		if($riwayatAssessment) {
			$riwayatAssessment->peg_id = $req->peg_id;
			$riwayatAssessment->tanggal = saveDate($req->tanggal_ass);
			$riwayatAssessment->jabatan = $req->jabatan_ass;
			$riwayatAssessment->gol_id = $req->gol_ass;			
			$riwayatAssessment->kompetensi = $req->kompetensi_ass;			
			$riwayatAssessment->potensi = $req->potensi_ass;
			$riwayatAssessment->matriks_id = $req->matriks_ass;
			if($riwayatAssessment->save()) {
				$pegawai = Pegawai2::find($riwayatAssessment->peg_id);
				$log=StatusEditPegawai::where('peg_id', $riwayatAssessment->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_assessment";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $riwayatAssessment->peg_id;
					$log->action ="add_assessment";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Assessment Pegawai',json_encode($riwayatAssessment),$riwayatAssessment->riw_assessment_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$riwayatAssessment->peg_id.'#riwayat-assessment')->with('message', 'Riwayat Assessment Pegawai Telah Ditambahkan');
			}
		}
	}

	public function EditAssessment($id,Request $req) {
		$riwayatAssessment = RiwayatAssessment::find($id);
		if($riwayatAssessment){
			$riwayatAssessment->peg_id = $req->peg_id;
			$riwayatAssessment->tanggal = saveDate($req->tanggal_ass);
			$riwayatAssessment->jabatan = $req->jabatan_ass;
			$riwayatAssessment->gol_id = $req->gol_ass;			
			$riwayatAssessment->kompetensi = $req->kompetensi_ass;			
			$riwayatAssessment->potensi = $req->potensi_ass;
			$riwayatAssessment->matriks_id = $req->matriks_ass;
			if($riwayatAssessment->save()){
				$pegawai = Pegawai2::find($riwayatAssessment->peg_id);
				$log=StatusEditPegawai::where('peg_id', $riwayatAssessment->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_assessment";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $riwayatAssessment->peg_id;
					$log->action ="edit_assessment";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Sasaran Kerja Pegawai',json_encode($riwayatAssessment),$riwayatAssessment->riw_assessment_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$riwayatAssessment->peg_id.'#riwayat-assessment')->with('message', 'Data Riwayat Assessment Pegawai Telah Diupdate');
			}
		}
	}

	public function deleteAssessment($peg_id, $id) {
		$del = RiwayatAssessment::where('riw_assessment_id',$id)->delete();

		if($del) {
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_assessment";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $peg_id;
				$log->action ="delete_assessment";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Assessment Pegawai','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-assessment')->with('message', 'Riwayat Assessment Pegawai Berhasil dihapus');
		}
	}

	public function AddRiwayatKgb(Request $req){
		
		$rp=new KGB;
		//$rp->riw_pangkat_id = rand();
		$id_rp4=KGB::select('kgb_id')->max('kgb_id');
		$find_rpid4 = KGB::where('kgb_id', $id_rp4)->first();
		if($find_rpid4){
			$id_rp4 = $id_rp4+1;
			$rp->kgb_id = $id_rp4;
		}else{
			$rp->kgb_id = $id_rp4;
		}
		$rp->peg_id = $req->input('peg_id');
		
		$pmk = Pmk_skpd::where('peg_id', $rp->peg_id)->first();
		
		$kgb_kerja_tahun = $req->input('kgb_kerja_tahun') ? $req->input('kgb_kerja_tahun') : 0;
		$kgb_kerja_bulan = $req->input('kgb_kerja_bulan') ? $req->input('kgb_kerja_bulan') :0;
		$pmk_tahun = isset($pmk->pmk_tahun) ? $pmk->pmk_tahun:0;
		$pmk_bulan = isset($pmk->pmk_bulan) ? $pmk->pmk_bulan:0;
		
		if($pmk_tahun) {
			if($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) > 12) {
				$kgb_kerja_tahun = $kgb_kerja_tahun + $pmk_tahun + floor(($kgb_kerja_bulan+$pmk_bulan)/12);
				// $kgb_kerja_bulan = ($kgb_kerja_bulan+$pmk_bulan)%12);
			} else {
				$kgb_kerja_tahun = $kgb_kerja_tahun + $pmk_tahun;
			}
		}
		
		if($pmk_bulan) {
			if($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) > 12) {
				$kgb_kerja_bulan = (($kgb_kerja_bulan+$pmk_bulan)%12);
			}elseif($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) < 12) {
				$kgb_kerja_bulan = ($kgb_kerja_bulan+$pmk_bulan);
			}
		}

		$rp->kgb_kerja_tahun = $kgb_kerja_tahun;
		$rp->kgb_kerja_bulan = $kgb_kerja_bulan;

		$rp->kgb_nosk = $req->input('kgb_nosk');
		$rp->kgb_tglsk =  saveDate($req->input('kgb_tglsk'));
		$rp->kgb_tmt =  saveDate($req->input('kgb_tmt'));
		$rp->ttd_pejabat = $req->input('ttd_pejabat');
		$rp->unit_kerja = $req->input('unit_kerja');
		$rp->gol_id = $req->input('gol_id_kgb');
		$rp->created_at = date('Y-m-d H:i:s');
		
		$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
		$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $kgb_kerja_tahun)->first();
		$rp->kgb_gapok = $gaji['gaji_pokok']  ?: 0;
		
		$rp->kgb_thn = date('Y');
		$rp->kgb_bln = date('m');
		$rp->kgb_status = 50;
		$rp->user_id = Auth::user()->id;
		$rp->ttd_digital = 50;
		$rp->status_data = 'manual';
		// $rp->save();

		$rp_skpd=new Kgb_skpd;
		//$rp->riw_pangkat_id = rand();		
		$rp_skpd->kgb_id = $id_rp4;		
		$rp_skpd->peg_id = $req->input('peg_id');		
		$rp_skpd->kgb_kerja_tahun = $rp->kgb_kerja_tahun;
		$rp_skpd->kgb_kerja_bulan = $rp->kgb_kerja_bulan;
		$rp_skpd->kgb_nosk = $req->input('kgb_nosk');
		$rp_skpd->kgb_tglsk =  saveDate($req->input('kgb_tglsk'));
		$rp_skpd->kgb_tmt =  saveDate($req->input('kgb_tmt'));
		$rp_skpd->ttd_pejabat = $req->input('ttd_pejabat');
		$rp_skpd->unit_kerja = $req->input('unit_kerja');
		$rp_skpd->gol_id = $req->input('gol_id_kgb');	
		$rp_skpd->created_at = date('Y-m-d H:i:s');	
		$rp_skpd->kgb_gapok = $rp->kgb_gapok;		
		$rp_skpd->kgb_thn = date('Y');
		$rp_skpd->kgb_bln = date('m');
		$rp_skpd->kgb_status = 50;
		$rp_skpd->user_id = Auth::user()->id;
		$rp_skpd->ttd_digital = 50;
		$rp_skpd->status_data = 'manual';
		$rp_skpd->save();

		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="add_riwayat_kgb";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="add_riwayat_kgb";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Riwayat Kgb',json_encode($rp),$rp->kgb_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kgb')->with('message', 'Data Riwayat Telah Ditambahkan');
		}
	}

	public function EditRiwayatKgb($id, Request $req){
		$rp = KGB::find($id);
		if($rp){
			$rp->peg_id = $req->input('peg_id');
			
			$pmk = Pmk_skpd::where('peg_id', $rp->peg_id)->first();
			
			$kgb_kerja_tahun = $req->input('kgb_kerja_tahun') ? $req->input('kgb_kerja_tahun') : 0;
			$kgb_kerja_bulan = $req->input('kgb_kerja_bulan') ? $req->input('kgb_kerja_bulan') :0;
			$pmk_tahun = isset($pmk->pmk_tahun) ? $pmk->pmk_tahun:0;
			$pmk_bulan = isset($pmk->pmk_bulan) ? $pmk->pmk_bulan:0;
			
			if($pmk_tahun) {
				if($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) > 12) {
					$kgb_kerja_tahun = $kgb_kerja_tahun + $pmk_tahun + floor(($kgb_kerja_bulan+$pmk_bulan)/12);
					// $kgb_kerja_bulan = ($kgb_kerja_bulan+$pmk_bulan)%12);
				} else {
					$kgb_kerja_tahun = $kgb_kerja_tahun + $pmk_tahun;
				}
			}
			
			if($pmk_bulan) {
				if($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) > 12) {
					$kgb_kerja_bulan = (($kgb_kerja_bulan+$pmk_bulan)%12);
				}elseif($pmk_bulan && ($kgb_kerja_bulan + $pmk_bulan) < 12) {
					$kgb_kerja_bulan = ($kgb_kerja_bulan+$pmk_bulan);
				}
			}
			
			$rp->kgb_kerja_tahun = $kgb_kerja_tahun;
			$rp->kgb_kerja_bulan = $kgb_kerja_bulan;
			
			$rp->kgb_nosk = $req->input('kgb_nosk');
			$rp->kgb_tglsk =  saveDate($req->input('kgb_tglsk'));
			$rp->kgb_tmt =  saveDate($req->input('kgb_tmt'));
			$rp->ttd_pejabat = $req->input('ttd_pejabat');
			$rp->unit_kerja = $req->input('unit_kerja');
			$rp->gol_id = $req->input('gol_id_kgb');

			$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
			$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $kgb_kerja_tahun)->first();
			$rp->kgb_gapok = $gaji['gaji_pokok']  ?: 0;
			$rp->user_id = Auth::user()->id;
			$rp->updated_at = date('Y-m-d H:i:s');	
			
			$rp_skpd = Kgb_skpd::find($id);

			if($rp_skpd){
				$rp_skpd->peg_id = $req->input('peg_id');
				$rp_skpd->kgb_kerja_tahun = $kgb_kerja_tahun;
				$rp_skpd->kgb_kerja_bulan = $kgb_kerja_bulan;				
				$rp_skpd->kgb_nosk = $req->input('kgb_nosk');
				$rp_skpd->kgb_tglsk =  saveDate($req->input('kgb_tglsk'));
				$rp_skpd->kgb_tmt =  saveDate($req->input('kgb_tmt'));
				$rp_skpd->ttd_pejabat = $req->input('ttd_pejabat');
				$rp_skpd->unit_kerja = $req->input('unit_kerja');
				$rp_skpd->gol_id = $req->input('gol_id_kgb');
				$rp_skpd->kgb_gapok = $rp->kgb_gapok;
				$rp_skpd->user_id = Auth::user()->id;
				$rp_skpd->updated_at = date('Y-m-d H:i:s');	
				$rp_skpd->save();
			}

			if($rp->save()){
				$pegawai = Pegawai2::find($rp->peg_id);
				$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="edit_riwayat_kgb";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_nip = $pegawai['peg_nip'];
					$log->peg_id = $rp->peg_id;
					$log->action ="edit_riwayat_kgb";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Edit Riwayat Kgb',json_encode($rp),$rp->kgb_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-kgb')->with('message', 'Data Riwayat Telah Diupdate');
			}
		}
	}

	public function deleteRiwayatKgb($peg_id, $id, $kgb_id_bkd){
		$del = Kgb_skpd::where('peg_id', $peg_id)->where('kgb_id', $id)->delete();
		KGB::where('peg_id', $peg_id)->where('kgb_id', $kgb_id_bkd)->delete();

		if($del){
			$pegawai = Pegawai2::find($peg_id);
			$log=StatusEditPegawai::where('peg_id', $peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="delete_riwayat_kgb";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;
				$log->action ="delete_riwayat_kgb";
				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Hapus Riwayat Kgb','',$id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$peg_id.'#riwayat-kgb')->with('message', 'Riwayat Berhasil di hapus');
		}
	}

	public function EditRiwayatPmk($id, Request $req){
		if($id) {
			$rp = Pmk::find($id);
			$rp->peg_id = $id;
			$rp->updated_by = Auth::user()->id;
		} else {
			$rp = new Pmk;
			$rp->peg_id = $req->input('peg_id');
			$rp->created_by = Auth::user()->id;
		}
		
		$rp->pmk_tahun = $req->input('pmk_tahun')?:0;
		$rp->pmk_bulan = $req->input('pmk_bulan')?:0;
		$rp->masa_kerja_lama_thn = $req->input('masa_kerja_lama_thn');
		$rp->masa_kerja_lama_bln = $req->input('masa_kerja_lama_bln');
		$rp->pmk_gapok_lama = $req->input('pmk_gapok_lama')?:0;

		$rp->masa_kerja_baru_thn = $req->input('masa_kerja_baru_thn');
		$rp->masa_kerja_baru_bln = $req->input('masa_kerja_baru_bln');
		$rp->pmk_gapok_baru = $req->input('pmk_gapok_baru')?:0;

		$rp->pejabat_id = $req->input('pejabat_id');
		$rp->pmk_jabatan = $req->input('pmk_jabatan');
		
		$rp->pmk_nosk = $req->input('pmk_nosk');
		$rp->pmk_sktgl =  saveDate($req->input('pmk_sktgl'));
		$rp->pmk_tmt_lama =  saveDate($req->input('pmk_tmt_lama'));
		$rp->pmk_tmt_baru =  saveDate($req->input('pmk_tmt_baru'));
		$rp->pmk_ttd_pejabat = $req->input('pmk_ttd_pejabat');
		$rp->pmk_unit_kerja = $req->input('pmk_unit_kerja');
		$rp->gol_id = $req->input('gol_id_pmk');

		if($id) {
			$rp_skpd = Pmk_skpd::find($id);
			$rp_skpd->peg_id = $id;
			$rp_skpd->updated_by = Auth::user()->id;
		} else {
			$rp_skpd = new Pmk_skpd;
			$rp_skpd->peg_id = $req->input('peg_id');
			$rp_skpd->created_by = Auth::user()->id;
		}
		
		$rp_skpd->pmk_tahun = $req->input('pmk_tahun')?:0;
		$rp_skpd->pmk_bulan = $req->input('pmk_bulan')?:0;
		$rp_skpd->masa_kerja_lama_thn = $req->input('masa_kerja_lama_thn');
		$rp_skpd->masa_kerja_lama_bln = $req->input('masa_kerja_lama_bln');
		$rp_skpd->pmk_gapok_lama = $req->input('pmk_gapok_lama')?:0;
		$rp_skpd->masa_kerja_baru_thn = $req->input('masa_kerja_baru_thn');
		$rp_skpd->masa_kerja_baru_bln = $req->input('masa_kerja_baru_bln');
		$rp_skpd->pmk_gapok_baru = $req->input('pmk_gapok_baru')?:0;
		$rp_skpd->pejabat_id = $req->input('pejabat_id');
		$rp_skpd->pmk_jabatan = $req->input('pmk_jabatan');		
		$rp_skpd->pmk_nosk = $req->input('pmk_nosk');
		$rp_skpd->pmk_sktgl =  saveDate($req->input('pmk_sktgl'));
		$rp_skpd->pmk_tmt_lama =  saveDate($req->input('pmk_tmt_lama'));
		$rp_skpd->pmk_tmt_baru =  saveDate($req->input('pmk_tmt_baru'));
		$rp_skpd->pmk_ttd_pejabat = $req->input('pmk_ttd_pejabat');
		$rp_skpd->pmk_unit_kerja = $req->input('pmk_unit_kerja');
		$rp_skpd->gol_id = $req->input('gol_id_pmk');
		$rp_skpd->save();

		if($rp->save()){
			$pegawai = Pegawai2::find($rp->peg_id);
			$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				if($id)
					$log->action ="edit_riwayat_pmk";
				else
					$log->action ="add_riwayat_pmk";

				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_nip = $pegawai['peg_nip'];
				$log->peg_id = $rp->peg_id;

				if($id)
					$log->action ="edit_riwayat_pmk";
				else
					$log->action ="add_riwayat_pmk";

				$log->status_id = 1;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}

			if($id)
				logAction('Edit PMK',json_encode($rp),$rp->peg_id,Auth::user()->username);
			else
				logAction('Tambah PMK',json_encode($rp),$rp->peg_id,Auth::user()->username);

			return Redirect::to('/pegawai/profile/edit/'.$rp->peg_id.'#riwayat-pmk')->with('message', 'Data Riwayat Telah Diupdate');
		}
	}
}