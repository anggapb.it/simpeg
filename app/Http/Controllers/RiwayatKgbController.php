<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\StatusEditPegawai;
use App\Model\KGB;
use App\Model\UsulanDetailKGB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RiwayatKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id='')
    {
        if(!$id)
            $id = Auth::user()->satuan_kerja_id;

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		/* $uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get(); */
        $title = 'KGB';

        return view('pages.kgb.index', compact('satker', 'title'));
		/* if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $id){
			return view('pages.kgb.index', compact('id', 'satker','pegawai'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id == 6){
			return view('pages.kgb.index', compact('id', 'satker','pegawai'));
		}else if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.index', compact('id', 'satker','pegawai'));
			}else{
				return Redirect::back();
			}
		}else if(Auth::user()->role_id == 9){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai_induk = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE');
				$pegawai = Pegawai::whereRaw('unit_kerja_id in (select unit_kerja_id from m_spg_unit_kerja where unit_kerja_parent = '.Auth::user()->unit_kerja_id.')')->where('peg_status', 'TRUE')->union($pegawai_induk)->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.index', compact('id', 'satker','pegawai'));
			}else{
				return Redirect::back();
			}
		}else{
			return Redirect::back();
		} */
    }

    public function dms($id) {
        $pegawai = Pegawai::where('peg_id', $id)->first();

        return response()->json([
            'view_1' => view('includes.profile_baru', compact('pegawai'))->render(),
            'view_2' => view('includes.file_pegawai2', compact('pegawai'))->render()
        ]);
        // return view('includes.profile_baru', compact('pegawai'));
    }
    
    public function saveKgb(Request $req) {
        $usulan_detail_kgb = UsulanDetailKGB::where('usulan_id_detail', $req->usulan_id_detail)->first();
        
        $usulan_detail_kgb->status_usulan_pegawai = $req->status_usulan_pegawai;
        $usulan_detail_kgb->keterangan = $req->keterangan;
        $usulan_detail_kgb->save();
        
        logAction('Proses data KGB',json_encode($usulan_detail_kgb),$req->usulan_id_detail,Auth::user()->username);
        
        if($req->status_usulan_pegawai == 10) {
            $pegawai = Pegawai::where('peg_id', $req['peg_id'])->first();
            // logAction('Proses data KGB',json_encode($usulan_detail_kgb),$req['usulan_id_detail'],Auth::user()->username);

            $id_rp4=KGB::select('kgb_id')->max('kgb_id');
            $id_rp4 = $id_rp4?:1;
            $find_rpid4 = KGB::where('kgb_id', $id_rp4)->first();
            
            if($find_rpid4){
                $id_rp4 = $id_rp4+1;
                // $rp4->kgb_id = $id_rp4;
            }/* else{
                $rp4->kgb_id = $id_rp4;
            } */

            return KGB::create([
                'kgb_id' => $id_rp4,
                'peg_id' => $req['peg_id'],
                'gol_id' => $pegawai->gol_id_akhir,
                'kgb_thn' => date('Y'),
                'kgb_bln' => date('m'),
                'kgb_kerja_tahun' => $usulan_detail_kgb->kgb_kerja_tahun,
                'kgb_kerja_bulan' => $usulan_detail_kgb->kgb_kerja_bulan,
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'kgb_status' => 0,
                'kgb_tmt' => $usulan_detail_kgb->kgb_tmt,
                'status_data' => 'by system',
                'usulan_id_detail' => $req->usulan_id_detail
            ]);
        } else {
            return response()->json(['message' => 'berhasil di update'])->setStatusCode(200);   
        }
    }
    /* public function skp($id) {
        $pegawai = Pegawai::where('peg_id', $id)->first();

        $submit = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 2)->orderBy('id','desc')->first();
        return response()->json([
            'view_1' => view('includes.profile_baru', compact('pegawai'))->render(),
            'view_2' => view('includes.skp', compact('pegawai', 'submit'))->render()
        ]);
        // return view('includes.profile_baru', compact('pegawai'));
    } */
}
