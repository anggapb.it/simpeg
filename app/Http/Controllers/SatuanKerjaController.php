<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\SatuanKerja;
use Validator;
use Input;
use Redirect;
use Auth;

class SatuanKerjaController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $url = url('data/satker/get-data');
        $columns = [
            'satuan_kerja_nama' => ['Nama','satuan_kerja_nama'],
            'satuan_kerja_alamat' => ['Alamat','satuan_kerja_alamat'],
            'satuan_kerja_khusus' => ['Status','status | formatStatus'],
        ];
        return view('pages.satuan_kerja.index',compact('url','columns'));
	}

	public function getDataRender(Request $request) {
        $models = SatuanKerja::select('m_spg_satuan_kerja.*')->where('satuan_kerja_nama','not like', '%-%');
        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }
        
        if ($search != '') {
            $models = $models->whereRaw("lower(satuan_kerja_nama) like '%$search%'");
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = $models->count();
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->orderBy('satuan_kerja_nama')->get();

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create(){
        return view('pages.satuan_kerja.create');
    }

    public function store(Request $req){
        $max = SatuanKerja::max('satuan_kerja_id');
        $satker = new SatuanKerja;
        $satker->satuan_kerja_id = $max+1;
        $satker->satuan_kerja_nama = $req->satuan_kerja_nama;
        $satker->satuan_kerja_alamat = $req->satuan_kerja_alamat;
        $satker->status = $req->status;
        $satker->save();
        logAction('Tambah Satuan Kerja',json_encode($satker),$satker->satuan_kerja_id,Auth::user()->username);
        return Redirect::to('/data/satker')->with('message', 'Data Telah Ditambahkan');
    }

    public function edit($id){
        $model = SatuanKerja::find($id);
        return view('pages.satuan_kerja.edit', compact('model'));
    }

    public function update(Request $req, $id){
        $satker = SatuanKerja::find($id);
        $satker->satuan_kerja_nama = $req->satuan_kerja_nama;
        $satker->satuan_kerja_alamat = $req->satuan_kerja_alamat;
        $satker->status = $req->status;
        $satker->save();
        logAction('Edit Satuan Kerja',json_encode($satker),$satker->satuan_kerja_id,Auth::user()->username);
        return Redirect::to('/data/satker')->with('message', 'Data Telah Diupdate');
    }

}
