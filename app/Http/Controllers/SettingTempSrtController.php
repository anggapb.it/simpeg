<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\SettingTempSrt;
use Redirect;
use File;

 
class SettingTempSrtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index_old()
    // {
    //     $settemplate = SettingTempSrt::where('id',1)->first();
	// 	return view('pages.kgb.template-srt', compact('settemplate'));
    // }

    public function index(){
      //  $setting = Setting::get('lock.absen_manual.input');
        $title = 'KGB';
        return view('pages.kgb.format-srtkgb', compact('title'));
    }

    public function getDataTemplateSrtKgb()
    {
        $data = SettingTempSrt::where('id',1)->first();
        $result = [];

            $result[] = [
                $data->id,
                $data->template,
                $data->userid,
            ];

        return response()->json($result);
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_template_old(Request $request, $id=1)
    {       
        $template = SettingTempSrt::find($id);
        $template->template =  $request->input('template_srt'); 
        $template->save();         
  
     //   return redirect()->route('kgb.setting-template')
       //                 ->with('success','Updated successfully');
       // return Redirect::to('/kgb/setting-template-srt')->with('message', 'Data Telah Ditambahkan');                
       // return Redirect::to('/kgb/setting-template-srt')->with('message', 'Data Telah Ditambahkan');  
        return Redirect::back()->with('message','Data telah diupdate');              
    }

    public function download($file) {
  //      $filex = asset('uploads/file_laporan_kp/'.$file);
        $filex = storage_path('file_doc/template_srt_kgb/'.$file);
        $name = basename($filex);
        return response()->download($filex, $name);
    }

    public function update_template(Request $req){
        $id = $req->id;
        $setsrtkgb = SettingTempSrt::find($id);
        $files = 0;
      
            $destinationPath = 'file_doc/template_srt_kgb'; // upload path
            $extension = $req->template->getClientOriginalExtension(); // getting file extension
            if (!in_array($extension,['docx','doc'])) {
                return redirect()->back()->with('message','File surat harus dalam bentuk DOC atau DOCX');
            }
            $filename = $req->template->getClientOriginalName(); // getting file extension
            // while (File::exists(storage_path("$destinationPath/$filename"))) {
            //     $extension_pos = strrpos($filename, '.');
            //     $filename = substr($filename, 0, $extension_pos) .'_'.time(). substr($filename, $extension_pos);
            // }
            $req->template->move(storage_path("$destinationPath"), $filename);
        $setsrtkgb->template = $filename;
        $setsrtkgb->save();
        return redirect()->back()->with('message','Template surat berhasil diubah');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
