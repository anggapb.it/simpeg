<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use Redirect;
use Validator;
use Input;
use File;
use DB;
use Excel;

use App\Model\Pegawai;
use App\Model\Pegawai2;
use App\Model\Golongan;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Penghargaan;
use App\Model\RiwayatCuti;
use App\Model\RiwayatPenghargaan;
use App\Model\RiwayatPenghargaan2;
use App\Model\RiwayatPendidikan;
use App\Model\RiwayatPendidikan2;
use App\Model\RiwayatNonFormal;
use App\Model\RiwayatNonFormal2;
use App\Model\RiwayatPangkat;
use App\Model\RiwayatPangkat2;
use App\Model\KGB;
use App\Model\Kgb_skpd;
use App\Model\Pmk;
use App\Model\Pmk_skpd;
use App\Model\RiwayatJabatan;
use App\Model\RiwayatJabatan2;
use App\Model\RiwayatDiklat;
use App\Model\RiwayatDiklat2;
use App\Model\RiwayatKeluarga;
use App\Model\RiwayatKeluarga2;
use App\Model\RiwayatKeahlian;
use App\Model\RiwayatKeahlian2;
use App\Model\RiwayatKeahlianRel;
use App\Model\RiwayatKeahlianRel2;
use App\Model\RiwayatHukuman;
use App\Model\RiwayatHukuman2;
use App\Model\FilePegawai;
use App\Model\FilePegawai2;
use App\Model\StatusEditPegawai;
use App\Model\MStatus;
use App\Model\TingkatPendidikan;
use App\Model\Pendidikan;
use App\Model\KategoriPendidikan;
use App\Model\Kecamatan;
use App\Model\Kabupaten;
use App\Model\Jabatan;
use App\Model\JabatanFungsional;
use App\Model\JabatanFungsionalUmum;
use App\Model\StatusEditPegawaiLog;
use App\Model\LogEditPegawai;
use App\Model\User;
use App\Model\PegawaiPensiun;
use App\Model\PegawaiInaktif;
use App\Model\CalonJft;
use App\Model\LogKedudukan;
use App\Model\RiwayatKedHuk;

class SkpdController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	//list permohonan index
	public function indexPermohonan($id){
		$nama = MStatus::where('id', $id)->first();
		$url = url('permohonan/get-data/'.$id);
		$columns = [
            'updated_at' => ['Waktu','updated_at'],
            'peg_nama' => ['Nama Pegawai','pegawai.nama'],
            'nama' => ['Nama Editor','pegawai.nama'],
        ];
        return view('pages.list_permohonan2',compact('url','columns','nama'));
	}

	public function getDataRender($id,Request $request) {
        $models = StatusEditPegawai::select('status_edit_pegawai.*')->with(['pegawai','log']);
        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);

        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }

        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }

        /*if ($search != '') {
            $models = $models->where('pegawai.nip','like',"%$search%")
                    ->orWhere('pegawai.nama','like',"%$search%")
                    ->orWhere('pegawai.jabatan','like',"%$search%");
        }*/

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

        $count = $models->count();
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

	//List unit kerja
	public function index(){
		$id_satker = Auth::user()->satuan_kerja_id;
		$satker = SatuanKerja::where('satuan_kerja_id', $id_satker)->first();

		if(Auth::user()->role_id == 8 || Auth::user()->role_id == 9){
			return redirect('list-pegawai/all/'.Auth::user()->unit_kerja_id);
		}

		return view('pages.list_unit', compact('satker'));
	}

	//untuk user BKD bisa pilih unit
	public function listUnit($id){
		if(Auth::user()->role_id != 2){
			$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
			return view('pages.list_unit', compact('satker'));
		}else{
			return Redirect::to('list-unit');
		}
	}

	//list di dashboard
	public function listPermohonan($id){
		$user_id = Auth::user()->id;
		$nama = MStatus::where('id', $id)->first();

		$editor = '';
		if($nama->id==4){
			$editor = 'Nama Verivikator';
		}
		else{
			$editor = 'Nama Editor';
		}
		$columns = collect([
		    'updated_at' => ['Waktu','updated_at'],
		    'peg_nama' => ['Nama Pegawai','peg_nama'],
		    'nama_editor' => [$editor,'nama_editor'],

		]);

		$revisi = [];
		if($nama->id==3){
			$revisi = [
				'keterangan' => ['Keterangan Revisi','keterangan']
			];
		}

		$columns = $columns->merge($revisi);

		// if (Auth::user()->role_id == 2){
		// 	$status = DB::select("select distinct (peg_id),status_edit_pegawai.id,status_edit_pegawai.updated_at from status_edit_pegawai left join status_edit_pegawai_log
		// 		on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
		// 		where status_id = ".$id." and editor_id = ".$user_id."
		// 		group by peg_id, status_edit_pegawai.id, status_edit_pegawai_log.id
		// 		order by status_edit_pegawai.updated_at asc");
		// }else{
		// 	$status  = DB::select("select distinct (peg_id),status_edit_pegawai.id,status_edit_pegawai.updated_at from status_edit_pegawai left join status_edit_pegawai_log
		// 			on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
		// 			where status_id = ".$id."
		// 			group by peg_id, status_edit_pegawai.id, status_edit_pegawai_log.id
		// 			order by status_edit_pegawai.updated_at asc");
		// }
		//dd($status);
		return view('pages.list_permohonan', compact('nama','columns'));
	}

	public function listPermohonanData(Request $request)
	{
		$user_id = Auth::user()->id;
		$models = '';
		$tambah = '';
		$params = $request->get('params',false);
		if (Auth::user()->role_id == 2){
			if($params['id'] == 2){
				$tambah = " AND action = 'usul_perubahan'";
			}elseif($params['id'] == 3){
				$tambah = "";
			}elseif($params['id'] == 4){
				$tambah = "";
			}
			// $models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id), spg_pegawai.peg_nama, status_edit_pegawai. ID, status_edit_pegawai.updated_at, ( SELECT username FROM status_edit_pegawai_log LEFT JOIN users ON status_edit_pegawai_log.editor_id = users. ID WHERE status_edit_pegawai_log.sed_id = status_edit_pegawai. ID ORDER BY status_edit_pegawai_log.ID DESC LIMIT 1) AS nama_editor")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->where(DB::raw('( SELECT username FROM status_edit_pegawai_log LEFT JOIN users ON status_edit_pegawai_log.editor_id = users. ID WHERE status_edit_pegawai_log.sed_id = status_edit_pegawai. ID ORDER BY status_edit_pegawai_log.ID DESC LIMIT 1)'),$user_id)->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id');
			if($params['id'] == 3){
				$models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id),spg_pegawai.peg_nama,status_edit_pegawai. ID,status_edit_pegawai.updated_at,username as nama_editor,keterangan")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->leftJoin(DB::raw("( SELECT DISTINCT(peg_id), status_edit_pegawai.ID, editor_id, username, status_edit_pegawai_log.time FROM status_edit_pegawai LEFT JOIN status_edit_pegawai_log ON status_edit_pegawai. ID = status_edit_pegawai_log.sed_id LEFT JOIN users ON users.id = status_edit_pegawai_log.editor_id WHERE editor_id = ".Auth::user()->id." ORDER BY peg_id ASC,status_edit_pegawai_log.time DESC) AS ed"),'ed.id','=','status_edit_pegawai.id')->leftJoin(DB::raw("(SELECT DISTINCT ON (peg_id) keterangan,peg_id FROM log_edit_pegawai) as log"),'log.peg_id','=','status_edit_pegawai.peg_id')->where('editor_id',Auth::user()->id)->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id','username','keterangan');
			}
			if($params['id'] == 5){
				$models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id),spg_pegawai.peg_nama,status_edit_pegawai. ID,status_edit_pegawai.updated_at,username as nama_editor,keterangan")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->leftJoin(DB::raw("( SELECT DISTINCT(peg_id), status_edit_pegawai.ID, editor_id, username, status_edit_pegawai_log.time FROM status_edit_pegawai LEFT JOIN status_edit_pegawai_log ON status_edit_pegawai. ID = status_edit_pegawai_log.sed_id LEFT JOIN users ON users.id = status_edit_pegawai_log.editor_id ORDER BY peg_id ASC,status_edit_pegawai_log.time DESC) AS ed"),'ed.id','=','status_edit_pegawai.id')->leftJoin(DB::raw("(SELECT DISTINCT ON (peg_id) keterangan,peg_id FROM log_edit_pegawai) as log"),'log.peg_id','=','status_edit_pegawai.peg_id')->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id','username','keterangan');
			}

			else{
				$models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id),spg_pegawai.peg_nama,status_edit_pegawai. ID,status_edit_pegawai.updated_at,username as nama_editor")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->leftJoin(DB::raw("( SELECT DISTINCT(peg_id), status_edit_pegawai.ID, editor_id, username, status_edit_pegawai_log.time FROM status_edit_pegawai LEFT JOIN status_edit_pegawai_log ON status_edit_pegawai. ID = status_edit_pegawai_log.sed_id LEFT JOIN users ON users.id = status_edit_pegawai_log.editor_id WHERE editor_id = ".Auth::user()->id." ORDER BY peg_id ASC,status_edit_pegawai_log.time DESC) AS ed"),'ed.id','=','status_edit_pegawai.id')->where('editor_id',Auth::user()->id)->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id','username');
			}

			// $status = DB::select("select distinct (peg_id),status_edit_pegawai.id,status_edit_pegawai.updated_at from status_edit_pegawai left join status_edit_pegawai_log
			// 	on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
			// 	where status_id = ".$id." and editor_id = ".$user_id."
			// 	group by peg_id, status_edit_pegawai.id, status_edit_pegawai_log.id
			// 	order by status_edit_pegawai.updated_at asc");
		}else{
			if($params['id'] == 2){
				$tambah = " WHERE action = 'usul_perubahan'";
			}elseif($params['id'] == 3){
				$tambah = "";
			}elseif($params['id'] == 4){
				$tambah = "";
			}
			if($params['id'] == 3){
				$models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id),spg_pegawai.peg_nama,status_edit_pegawai. ID,status_edit_pegawai.updated_at,username as nama_editor")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->leftJoin(DB::raw("( SELECT DISTINCT ON (peg_id) peg_id, status_edit_pegawai.ID, editor_id, username, status_edit_pegawai_log.time FROM status_edit_pegawai LEFT JOIN status_edit_pegawai_log ON status_edit_pegawai. ID = status_edit_pegawai_log.sed_id LEFT JOIN users ON users.id = status_edit_pegawai_log.editor_id ORDER BY peg_id ASC,status_edit_pegawai_log.time DESC) AS ed"),'ed.id','=','status_edit_pegawai.id')->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id','username');
			}else{
				$models = DB::table('status_edit_pegawai')->selectRaw("DISTINCT(status_edit_pegawai.peg_id),spg_pegawai.peg_nama,status_edit_pegawai. ID,status_edit_pegawai.updated_at,username as nama_editor")->leftJoin('spg_pegawai','spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->leftJoin(DB::raw("( SELECT DISTINCT ON (peg_id) peg_id, status_edit_pegawai.ID, editor_id, username, status_edit_pegawai_log.time FROM status_edit_pegawai LEFT JOIN status_edit_pegawai_log ON status_edit_pegawai. ID = status_edit_pegawai_log.sed_id LEFT JOIN users ON users.id = status_edit_pegawai_log.editor_id ORDER BY peg_id ASC,status_edit_pegawai_log.time DESC) AS ed"),'ed.id','=','status_edit_pegawai.id')->groupBy('status_edit_pegawai.peg_id','spg_pegawai.peg_nama','status_edit_pegawai.id','username');
			}

			// $status  = DB::select("select distinct (peg_id),status_edit_pegawai.id,status_edit_pegawai.updated_at from status_edit_pegawai left join status_edit_pegawai_log
			// 		on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
			// 		where status_id = ".$id."
			// 		group by peg_id, status_edit_pegawai.id, status_edit_pegawai_log.id
			// 		order by status_edit_pegawai.updated_at asc");
		}
		$search_any = $request->get('search',false);
		$order  = $request->get('order' ,false);
		if ($params) {
		    foreach ($params as $key => $search) {
		        if ($search == '') continue;
		        switch($key) {
		        	case 'id':
		        		$models = $models->where('status_id',$search);
		        		break;
		            default:
		                break;
		        }
		    }
		}
		if (!empty($search_any)) {
		    $models = $models->where(function($query) use ($search_any){
		    	$query->where('peg_nama','ilike','%'.$search_any.'%')->orWhere('username','ilike','%'.$search_any.'%');
		    });
		}


		$page = $request->get('page',1);
		$perpage = $request->get('perpage',20);
		$count = count($models->get());
		if ($order) {
		    $order_direction = $request->get('order_direction','asc');
		    switch ($order) {
		    	case 'keterangan':
		    		break;
		        default:
		            $models = $models->orderBy($order,$order_direction);
		            break;
		    }
		}else{
			$models = $models->orderBy('peg_nama','asc');
		}

		$models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
		foreach ($models as &$model) {
			if($params['id'] == 4){
				$pegawai = Pegawai::where('peg_id', $model->peg_id)->where('peg_nip', '!=', '')->first();
			}else{
				$pegawai = Pegawai2::where('peg_id', $model->peg_id)->where('peg_nip', '!=', '')->first();
			}
			$model->peg_nama = $pegawai ? $pegawai->peg_gelar_depan." ".$pegawai->peg_nama.",".$pegawai->peg_gelar_belakang : 'Pegawai Tidak Ditemukan / Sudah Terhapus';
			$model->updated_at = transformDateTime($model->updated_at);
			$pegawai_bkd = Pegawai::where('peg_id', $model->peg_id)->where('peg_nip', '!=', '')->first();
			$model->pegawai_bkd = count($pegawai_bkd);

			$id_editor = StatusEditPegawaiLog::where('sed_id', $model->id)->orderBy('id', 'desc')->first();

			// $ket = LogEditPegawai::where('peg_id', $model->peg_id)->where('status_id', $params['id'])->orderBy('id', 'desc')->first();
			// if($params['id'] == 3){
			// 	$model->keterangan = $ket->keterangan;
			// }
			$a = StatusEditPegawai::where('peg_id', $model->peg_id)->where('status_id', $params['id'])->whereHas('log', function($query) use ($id_editor){
						$query->where('editor_id', $id_editor->editor_id);
					})->orderBy('id', 'desc')->first();
			$stat = StatusEditPegawaiLog::where('sed_id', $a->id)->where('action','!=','usul_perubahan')->where('action','!=','verifikasi_data')->where('action','!=','revisi_data')->where('action','!=','batal_usulan')->orderBy('id', 'desc')->first();
			$status_aktif = ["pensiun_pegawai","mutasi_keluar","mutasi_masuk_skpd","tambah_pegawai","update_pegawai",'pegawai_meninggal','pindah_pegawai'];
			$model->stat_cek = $stat ? in_array($stat->action, $status_aktif) : false;
			$model->stat = $stat ? $stat->action :'';
			$model->stat_length = count($stat);

			if($params['id'] == 3){
				$ket = LogEditPegawai::where('peg_id',$model->peg_id)->orderBy('id','desc')->first();
				$model->keterangan = $ket ? $ket->keterangan : '';
			}
			// $model->nama_editor = User::where('id', $id_editor->editor_id)->first();
		}
		$result = [
		    'data' => $models,
		    'count' => $count
		];

		return response()->json($result);
	}

	//list log edit
	public function listLogEdit($id){
		// $log =DB::select('select * from status_edit_pegawai right join status_edit_pegawai_log
		// 		on status_edit_pegawai.id = status_edit_pegawai_log.sed_id right join (select peg_id,peg_nama FROM spg_pegawai) as spg_pegawai ON spg_pegawai.peg_id = status_edit_pegawai.peg_id
		// 		where editor_id = '.$id.' group by editor_id, status_edit_pegawai.id, status_edit_pegawai_log.id, spg_pegawai.peg_id,spg_pegawai.peg_nama order by status_edit_pegawai.peg_id asc');
		// dd($log);
		$columns = collect([
		    'peg_nama' => ['Nama Pegawai','peg_nama'],
		    'action' => ['Keterangan','action'],
		    'time' => ['Waktu','time'],

		]);
		$nama = User::where('id', $id)->first();
		return view('pages.list_log_edit', compact('nama','columns'));
	}

	public function listLogEditData(Request $request)
	{
		$user_id = Auth::user()->id;
		// $models = DB::select('select * from status_edit_pegawai right join status_edit_pegawai_log on status_edit_pegawai.id = status_edit_pegawai_log.sed_id where editor_id = '.$id.' group by editor_id, status_edit_pegawai.id, status_edit_pegawai_log.id order by peg_id asc');
		$models = DB::table('status_edit_pegawai')->rightJoin('status_edit_pegawai_log','status_edit_pegawai.id','=','status_edit_pegawai_log.sed_id')->rightJoin(DB::raw('(SELECT peg_nama,peg_id FROM spg_pegawai) AS spg_pegawai'),'spg_pegawai.peg_id','=','status_edit_pegawai.peg_id')->groupBy('editor_id','status_edit_pegawai.id','status_edit_pegawai_log.id','spg_pegawai.peg_nama','spg_pegawai.peg_id');
		$params = $request->get('params',false);
		$search_any = $request->get('search',false);
		$order  = $request->get('order' ,false);

		if ($params) {
		    foreach ($params as $key => $search) {
		        if ($search == '') continue;
		        switch($key) {
		        	case 'id':
		        		$models = $models->where('editor_id',$search);
		        		break;
		            default:
		                break;
		        }
		    }
		}
		if (!empty($search_any)) {
		    $models = $models->where(function($query) use ($search_any){
		    	$query->where('peg_nama','ilike','%'.$search_any.'%');
		    });
		}


		$page = $request->get('page',1);
		$perpage = $request->get('perpage',20);
		$count = count($models->get());
		if ($order) {
		    $order_direction = $request->get('order_direction','asc');
		    switch ($order) {
		        default:
		            $models = $models->orderBy($order,$order_direction);
		            break;
		    }
		}

		$models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
		foreach ($models as &$model) {
			$pegawai = Pegawai::where('peg_id', $model->peg_id)->where('peg_nip', '!=', '')->first();
			$model->peg_nama = $pegawai->peg_gelar_depan." ".$pegawai->peg_nama.",".$pegawai->peg_gelar_belakang;
			$model->time = transformDateTime($model->time);
		}
		$result = [
		    'data' => $models,
		    'count' => $count
		];

		return response()->json($result);
	}

	//list pegawai per unit
	public function listPegawai($id,$ids=null){
		$user_satker_id = Auth::user()->satuan_kerja_id;
		if($id=='non-unit'){
			$uker='';
			$satker = SatuanKerja::where('satuan_kerja_id', $ids)->first();
			$pegawai = Pegawai::where('unit_kerja_id', null)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->orderBy('gol_id_akhir', 'desc')->orderBy('jabatan_id')->orderBy('peg_nama')->get();
		}elseif($id=='non-satuan-kerja'){
			$satker='';
			$uker='';
			$pegawai = Pegawai::where('satuan_kerja_id', null)->where('peg_status', 'TRUE')->orderBy('gol_id_akhir', 'desc')->orderBy('jabatan_id')->orderBy('peg_nama')->get();
		}else{
			$uker = UnitKerja::where('unit_kerja_id', $ids)->first();
			$satker = SatuanKerja::where('satuan_kerja_id', $uker->satuan_kerja_id)->first();
			$pegawai = Pegawai::where('unit_kerja_id', $ids)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->orderBy('gol_id_akhir', 'desc')->orderBy('jabatan_id')->orderBy('peg_nama')->get();
		}

		if(Auth::user() && Auth::user()->role_id==2 && !empty($satker) && $user_satker_id == $satker->satuan_kerja_id){
			return view('pages.list_pegawai', compact('id', 'satker','pegawai','uker','ids'));
		}else if(Auth::user() && (Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id == 6 || Auth::user()->role_id == 8)){
			return view('pages.list_pegawai', compact('id', 'satker','pegawai','uker','ids'));
		}else{
			return Redirect::back();
		}
	}

	//list pegawai di satuan kerja
	public function listAllPegawai($id){
		ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();


		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $id){
			return view('pages.list_all_pegawai', compact('id', 'satker','pegawai'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id == 6){
			return view('pages.list_all_pegawai', compact('id', 'satker','pegawai'));
		}else if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.list_all_pegawai', compact('id', 'satker','pegawai'));
			}else{
				return Redirect::back();
			}
		}else if(Auth::user()->role_id == 9){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai_induk = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE');
				$pegawai = Pegawai::whereRaw('unit_kerja_id in (select unit_kerja_id from m_spg_unit_kerja where unit_kerja_parent = '.Auth::user()->unit_kerja_id.')')->where('peg_status', 'TRUE')->union($pegawai_induk)->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.list_all_pegawai', compact('id', 'satker','pegawai'));
			}else{
				return Redirect::back();
			}
		}else{
			return Redirect::back();
		}
	}

	//get profile dari database bkd
	public function getProfilePegawai($id){
		$pegawai = Pegawai::where('peg_id', $id)->first();
		if (!$pegawai) {
			\Log::info('Error get pegawai peg_id'.$id);
			return redirect('/');
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai['satuan_kerja_id'])->first();

		/*if( && Auth::user()->satuan_kerja_id == $pegawai['satuan_kerja_id']){
			return view('pages.profile_pegawai', compact('pegawai', 'satker'));
		}else */if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id==2 || Auth::user()->role_id == 8){
			return view('pages.profile_pegawai', compact('pegawai', 'satker'));
		}else if(Auth::user()->role_id==4 && Auth::user()->username==$pegawai->peg_nip){
			return view('pages.profile_pegawai', compact('pegawai', 'satker'));
		}else{
			return Redirect::back();
		}
	}

	//get profile dari database skpd,  jika pegawai tidak ada di db skpd, maka di copy dulu dari db bkd
	public function getNewProfilePegawai($id){
		if(Auth::user()->role_id == 4) {
			return Redirect::back();
		}
		$pegawai = Pegawai2::where('peg_id', $id)->first();		
		if($pegawai == null){
			$p = Pegawai::where('peg_id', $id)->first();
			$pegawai = new Pegawai2;
			$pegawai->peg_id = $id;
			$pegawai->id_goldar=$p->id_goldar;
			$pegawai->gol_id_awal=$p->gol_id_awal;
			$pegawai->id_pend_awal=$p->id_pend_awal;
			$pegawai->unit_kerja_id=$p->unit_kerja_id;
			$pegawai->kecamatan_id=$p->kecamatan_id;
			$pegawai->gol_id_akhir=$p->gol_id_akhir;
			$pegawai->jabatan_id=$p->jabatan_id	;
			$pegawai->id_pend_akhir=$p->id_pend_akhir;
			$pegawai->id_agama=$p->id_agama;
			$pegawai->peg_nip=$p->peg_nip;
			$pegawai->peg_nama=$p->peg_nama;
			$pegawai->peg_gelar_depan=$p->peg_gelar_depan;
			$pegawai->peg_gelar_belakang=$p->peg_gelar_belakang;
			$pegawai->peg_lahir_tempat=$p->peg_lahir_tempat;
			$pegawai->peg_lahir_tanggal=$p->peg_lahir_tanggal;
			$pegawai->peg_jenis_kelamin=$p->peg_jenis_kelamin;
			$pegawai->peg_status_perkawinan=$p->peg_status_perkawinan;
			$pegawai->peg_karpeg=$p->peg_karpeg;
			$pegawai->peg_karsutri=$p->peg_karsutri;
			$pegawai->peg_status_kepegawaian=$p->peg_status_kepegawaian;
			$pegawai->peg_cpns_tmt=$p->peg_cpns_tmt;
			$pegawai->peg_pns_tmt=$p->peg_pns_tmt;
			$pegawai->peg_gol_awal_tmt=$p->peg_gol_awal_tmt;
			$pegawai->peg_kerja_tahun=$p->peg_kerja_tahun;
			$pegawai->peg_kerja_bulan=$p->peg_kerja_bulan;
			$pegawai->peg_jabatan_tmt=$p->peg_jabatan_tmt;
			$pegawai->peg_no_askes=$p->peg_no_askes;
			$pegawai->peg_npwp=$p->peg_npwp;
			$pegawai->peg_bapertarum=$p->peg_bapertarum;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_kel_desa=$p->peg_kel_desa;
			$pegawai->peg_kodepos=$p->peg_kodepos;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_telp=$p->peg_telp;
			$pegawai->peg_telp_hp=$p->peg_telp_hp;
			$pegawai->peg_tmt_kgb=$p->peg_tmt_kgb;
			$pegawai->peg_foto=$p->peg_foto;
			$pegawai->peg_pend_awal_th=$p->peg_pend_awal_th;
			$pegawai->peg_pend_akhir_th=$p->peg_pend_akhir_th;
			$pegawai->peg_gol_akhir_tmt=$p->peg_gol_akhir_tmt;
			$pegawai->tgl_entry=$p->tgl_entry;
			$pegawai->satuan_kerja_id=$p->satuan_kerja_id;
			$pegawai->peg_status=$p->peg_status;
			$pegawai->peg_nip_lama=$p->peg_nip_lama;
			$pegawai->peg_ktp=$p->peg_ktp;
			$pegawai->peg_instansi_dpk=$p->peg_instansi_dpk;
			$pegawai->peg_ak=$p->peg_ak;
			$pegawai->peg_email=$p->peg_email;
			$pegawai->peg_punya_kpe = $p->peg_punya_kpe;
			$pegawai->peg_punya_kpe_tgl = $p->peg_punya_kpe_tgl;
			$pegawai->peg_status_asn = $p->peg_status_asn;
			$pegawai->peg_status_gaji = $p->peg_status_gaji;
			$pegawai->id_status_kepegawaian = $p->id_status_kepegawaian;
			$pegawai->peg_jenis_asn = $p->peg_jenis_asn;
			$pegawai->peg_status_calon = $p->peg_status_calon;
			$pegawai->peg_jenis_pns = $p->peg_jenis_pns;
			$pegawai->jenis_guru = $p->jenis_guru;
			$pegawai->status_tkd = $p->status_tkd;
			$pegawai->kedudukan_pegawai = $p->kedudukan_pegawai;
			$pegawai->save();

			$pg = RiwayatPenghargaan::where('peg_id', $id)->get();
			foreach ($pg as $pg2) {
				$r = RiwayatPenghargaan2::find($pg2->riw_penghargaan_id);
				if(!$r){
					$rp = new RiwayatPenghargaan2;
					$rp->peg_id = $pg2->peg_id;
					$rp->riw_penghargaan_id = $pg2->riw_penghargaan_id;
					$rp->penghargaan_id = $pg2->penghargaan_id;
					$rp->riw_penghargaan_instansi= $pg2->riw_penghargaan_instansi;
					$rp->riw_penghargaan_sk = $pg2->riw_penghargaan_sk;
					$rp->riw_penghargaan_tglsk = saveDate($pg2->riw_penghargaan_tglsk);
					$rp->riw_penghargaan_jabatan = $pg2->riw_penghargaan_jabatan;
					$rp->riw_penghargaan_lokasi = $pg2->riw_penghargaan_lokasi;
					$rp->save();
				}
			}
			$pf = RiwayatPendidikan::where('peg_id', $id)->get();
			foreach ($pf as $pf2) {
				$r2 = RiwayatPendidikan2::find($pf2->riw_pendidikan_id);
				if(!$r2){
					$rp2=new RiwayatPendidikan2;
					$rp2->riw_pendidikan_id = $pf2->riw_pendidikan_id;
					$rp2->peg_id = $pf2->peg_id;
					$rp2->id_pend = $pf2->id_pend;
					$rp2->tingpend_id= $pf2->tingpend_id;
					$rp2->riw_pendidikan_fakultas= $pf2->riw_pendidikan_fakultas;
					$rp2->jurusan_id= $pf2->jurusan_id;
					$rp2->univ_id = $pf2->univ_id;
					$rp2->riw_pendidikan_sttb_ijazah= $pf2->riw_pendidikan_sttb_ijazah;
					$rp2->riw_pendidikan_tgl = saveDate($pf2->riw_pendidikan_tgl);
					$rp2->riw_pendidikan_pejabat = $pf2->riw_pendidikan_pejabat;
					$rp2->riw_pendidikan_nm = $pf2->riw_pendidikan_nm;
					$rp2->fakultas_id = $pf2->fakultas_id;
					$rp2->riw_pendidikan_lokasi = $pf2->riw_pendidikan_lokasi;
					$rp2->save();
				}
			}

			$pn = RiwayatNonFormal::where('peg_id', $id)->get();
			foreach ($pn as $pn2) {
				$r3 = RiwayatNonFormal2::find($pn2->non_id);
				if(!$r3){
					$rp3=new  RiwayatNonFormal2;
					$rp3->non_id = $pn2->non_id;
					$rp3->peg_id = $pn2->peg_id;
					$rp3->non_nama= $pn2->non_nama;
					$rp3->non_tgl_mulai = saveDate($pn2->non_tgl_mulai);
					$rp3->non_tgl_selesai = saveDate($pn2->non_tgl_selesai);
					$rp3->non_sttp = $pn2->non_sttp;
					$rp3->non_sttp_tanggal = saveDate($pn2->non_sttp_tanggal);
					$rp3->non_sttp_pejabat = $pn2->non_sttp_pejabat;
					$rp3->non_penyelenggara = $pn2->non_penyelenggara;
					$rp3->non_tempat = $pn2->non_tempat;
					$rp3->save();
				}
			}

			$pp = RiwayatPangkat::where('peg_id', $id)->get();
			foreach ($pp as $pp2) {
				$r4 = RiwayatPangkat2::find($pp2->riw_pangkat_id);
				if(!$r4){
					$rp4=new RiwayatPangkat2;
					$rp4->riw_pangkat_id = $pp2->riw_pangkat_id;
					$rp4->peg_id = $pp2->peg_id;
					$rp4->riw_pangkat_thn= $pp2->riw_pangkat_thn;
					$rp4->riw_pangkat_bln = $pp2->riw_pangkat_bln;
					$rp4->riw_pangkat_gapok = $pp2->riw_pangkat_gapok;
					$rp4->riw_pangkat_sk = $pp2->riw_pangkat_sk;
					$rp4->riw_pangkat_sktgl =  saveDate($pp2->riw_pangkat_sktgl);
					$rp4->riw_pangkat_tmt =  saveDate($pp2->riw_pangkat_tmt);
					$rp4->riw_pangkat_pejabat = $pp2->riw_pangkat_pejabat;
					$rp4->riw_pangkat_unit_kerja = $pp2->riw_pangkat_unit_kerja;
					$rp4->gol_id = $pp2->gol_id;
					$rp4->save();
				}
			}
/* 
			$kgb = KGB::where('peg_id', $id)->get();
			
			foreach ($kgb as $pp2) {
				$r4 = Kgb_skpd::find($pp2->kgb_id);
				if(!$r4){
					$r4=new Kgb_skpd;
					$r4->kgb_id = $pp2->kgb_id;
					$r4->peg_id =  $pp2->peg_id;
					$r4->kgb_kerja_tahun= $pp2->kgb_kerja_tahun;
					$r4->kgb_kerja_bulan = $pp2->kgb_kerja_bulan;
					$r4->kgb_gapok = $pp2->kgb_gapok;
					$r4->kgb_nosk = $pp2->kgb_nosk;
					$r4->kgb_tglsk =  saveDate($pp2->kgb_tglsk);
					$r4->kgb_tmt =  saveDate($pp2->kgb_tmt);
					$r4->ttd_pejabat = $pp2->ttd_pejabat;
					$r4->unit_kerja = $pp2->unit_kerja;
					$r4->gol_id = $pp2->gol_id;
					$r4->kgb_thn = date('Y');
					$r4->kgb_bln = date('m');
					$r4->kgb_status = 50;
					$r4->user_id = Auth::user()->id;
					$r4->ttd_digital = 50;
					$r4->status_data = $pp2->status_data;
					$r4->save();
				}
			}

			$pmk = Pmk::where('peg_id', $id)->get();
			
			foreach ($pmk as $pp2) {
				$r4 = Pmk_skpd::find($pp2->peg_id);
				if(!$r4){
					$r4=new Pmk_skpd;
					$r4->peg_id =  $pp2->peg_id;
					$r4->created_by = $pp2->created_by;
					$r4->updated_by = $pp2->updated_by;
				// }
				
					$r4->pmk_tahun = $pp2->pmk_tahun;
					$r4->pmk_bulan = $pp2->pmk_bulan;
					$r4->masa_kerja_lama_thn = $pp2->masa_kerja_lama_thn;
					$r4->masa_kerja_lama_bln = $pp2->masa_kerja_lama_bln;
					$r4->pmk_gapok_lama = $pp2->pmk_gapok_lama;
			
					$r4->masa_kerja_baru_thn = $pp2->masa_kerja_baru_thn;
					$r4->masa_kerja_baru_bln = $pp2->masa_kerja_baru_bln;
					$r4->pmk_gapok_baru = $pp2->pmk_gapok_baru;
			
					$r4->pejabat_id = $pp2->pejabat_id;
					$r4->pmk_jabatan = $pp2->pmk_jabatan;
					
					$r4->pmk_nosk = $pp2->pmk_nosk;
					$r4->pmk_sktgl =  $pp2->pmk_sktgl;
					$r4->pmk_tmt_lama =  $pp2->pmk_tmt_lama;
					$r4->pmk_tmt_baru =  $pp2->pmk_tmt_baru;
					$r4->pmk_ttd_pejabat = $pp2->pmk_ttd_pejabat;
					$r4->pmk_unit_kerja = $pp2->pmk_unit_kerja;
					$r4->gol_id = $pp2->gol_id;
					$r4->save();
				}
			} */

			$pj = RiwayatJabatan::where('peg_id', $id)->whereNull('jabatan_id')->get();
			foreach ($pj as $pj2) {
				$r5 =  RiwayatJabatan2::find($pj2->riw_jabatan_id);
				if(!$r5){
					$rp5=new RiwayatJabatan2;
					$rp5->riw_jabatan_id = $pj2->riw_jabatan_id;
					$rp5->peg_id = $pj2->peg_id;
					$rp5->riw_jabatan_nm= $pj2->riw_jabatan_nm;
					$rp5->riw_jabatan_no = $pj2->riw_jabatan_no;
					$rp5->riw_jabatan_pejabat = $pj2->riw_jabatan_pejabat;
					$rp5->riw_jabatan_tgl = saveDate($pj2->riw_jabatan_tgl);
					$rp5->riw_jabatan_tmt =  saveDate($pj2->riw_jabatan_tmt);
					$rp5->riw_jabatan_unit = $pj2->riw_jabatan_unit;
					$rp5->gol_id = $pj2->gol_id;
					$rp5->save();
				}
			}

			$pd = RiwayatDiklat::where('peg_id',$id)->get();
			foreach ($pd as $pd2) {
				$r6 = RiwayatDiklat2::find($pd2->diklat_id);
				if(!$r6){
					$rp6=new RiwayatDiklat2;
					$rp6->diklat_id = $pd2->diklat_id;
					$rp6->peg_id = $pd2->peg_id;
					$rp6->kategori_id= $pd2->kategori_id;
					$rp6->diklat_fungsional_id= $pd2->diklat_fungsional_id;
					$rp6->diklat_teknis_id= $pd2->diklat_teknis_id;
					$rp6->diklat_jenis = $pd2->diklat_jenis;
					$rp6->diklat_mulai = saveDate($pd2->diklat_mulai);
					$rp6->diklat_selesai = saveDate($pd2->diklat_selesai);
					$rp6->diklat_penyelenggara = $pd2->diklat_penyelenggara;
					$rp6->diklat_tempat = $pd2->diklat_tempat;
					$rp6->diklat_jumlah_jam = $pd2->diklat_jumlah_jam;
					$rp6->diklat_sttp_no = $pd2->diklat_sttp_no;
					$rp6->diklat_sttp_tgl =  saveDate($pd2->diklat_sttp_tgl);
					$rp6->diklat_sttp_pej = $pd2->diklat_sttp_pej;
					$rp6->diklat_usul_no = $pd2->diklat_usul_no;
					$rp6->diklat_usul_tgl = $pd2->diklat_usul_tgl;
					$rp6->pejabat_id = $pd2->pejabat_id;
					$rp6->diklat_nama = $pd2->diklat_nama;
					$rp6->diklat_tipe = $pd2->diklat_tipe;
					$rp6->save();
				}
			}

			$pr = RiwayatKeluarga::where('peg_id', $id)->get();
			foreach ($pr as $pr2) {
				$r7 = RiwayatKeluarga2::find($pr2->riw_id);
				if(!$r7){
					$rp7=new RiwayatKeluarga2;
					$rp7->riw_id = $pr2->riw_id;
					$rp7->peg_id = $id;
					$rp7->riw_status = $pr2->riw_status;
					$rp7->riw_nama= $pr2->riw_nama;
					$rp7->nik= $pr2->nik;
					$rp7->riw_tgl_lahir =saveDate($pr2->riw_tgl_lahir);
					$rp7->riw_tempat_lahir = $pr2->riw_tempat_lahir;
					$rp7->riw_kelamin = $pr2->riw_kelamin;
					$rp7->riw_pendidikan = $pr2->riw_pendidikan;
					$rp7->riw_pekerjaan = $pr2->riw_pekerjaan;
					$rp7->riw_ket = $pr2->riw_ket;
					$rp7->riw_tgl_ket = $pr2->riw_tgl_ket;
					$rp7->riw_status_tunj = $pr2->riw_status_tunj;
					$rp7->riw_status_sutri = $pr2->riw_status_sutri;
					$rp7->riw_status_perkawinan = $pr2->riw_status_perkawinan;
					$rp7->save();
				}
			}

			$pk = RiwayatKeahlian::where('peg_id', $id)->get();
			foreach ($pk as $pk2) {
				$r8 = RiwayatKeahlian2::find($pk2->riw_keahlian_id);
				if(!$r8){
					$r8 = new RiwayatKeahlian2;
					$r8->riw_keahlian_id = $pk2->riw_keahlian_id;
					$r8->peg_id = $id;
					$r8->keahlian_id = $pk2->keahlian_id;
					$r8->keahlian_level_id = $pk2->keahlian_level_id;
					$r8->riw_keahlian_sejak = $pk2->riw_keahlian_sejak;
					if($r8->save()){
						$pkr = RiwayatKeahlianRel::where('riw_keahlian_id', $pk2->riw_keahlian_id)->first();
						$r9 = new RiwayatKeahlianRel2;
						//$r9->riw_keahlian_rel_id = $pkr->riw_keahlian_rel_id;
						$r9->riw_keahlian_id = $pkr->riw_keahlian_id;
						$r9->non_id = $pkr->non_id;
						$r9->riw_pendidikan_id = $pkr->riw_pendidikan_id;
						$r9->diklat_id = $pkr->diklat_id;
						$r9->predikat = $pkr->predikat;
						$r9->save();
 					}
				}
			}

			$ph = RiwayatHukuman::where('peg_id', $id)->get();
			foreach ($ph as $ph2) {
				$r10 = RiwayatHukuman2::find($ph2->riw_hukum_id);
				if(!$r10){
					$rp10=new RiwayatHukuman2;
					$rp10->riw_hukum_id = $ph2->riw_hukum_id;
					$rp10->peg_id = $id;
					$rp10->mhukum_id = $ph2->mhukum_id;
					$rp10->riw_hukum_tmt= saveDate($ph2->riw_hukum_tmt);
					$rp10->riw_hukum_sd =saveDate($ph2->riw_hukum_sd);
					$rp10->riw_hukum_sk = $ph2->riw_hukum_sk;
					$rp10->riw_hukum_tgl = saveDate($ph2->riw_hukum_tgl);
					$rp10->riw_hukum_ket = $ph2->riw_hukum_ket;
					$rp10->save();
				}
			}

			$pfile = FilePegawai::where('peg_id', $id)->get();
			foreach ($pfile as $pfile2) {
				$r11 = FilePegawai2::find($pfile2->file_id);
				if(!$r11){
					$rp11=new FilePegawai2;
					$rp11->file_id = $pfile2->file_id;
					$rp11->peg_id = $id;
					$rp11->file_nama = $pfile2->file_nama;
					$rp11->file_ket = $pfile2->file_ket;
					$rp11->file_tgl= $pfile2->file_tgl;
					$rp11->file_lokasi =$pfile2->file_lokasi;
					$rp11->save();
				}
			}
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
		//dd($pegawai->satuan_kerja_id);
		/*if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $pegawai->satuan_kerja_id){
			return view('pages.new_profile_pegawai', compact('pegawai','satker'));
		}else*/ if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id==2 || Auth::user()->role_id==6 || Auth::user()->role_id == 8 || Auth::user()->role_id == 5 || Auth::user()->role_id == 9){
			return view('pages.new_profile_pegawai', compact('pegawai','satker'));
		}else if(Auth::user()->role_id==4 && Auth::user()->username==$pegawai->peg_nip){
			return view('pages.new_profile_pegawai', compact('pegawai','satker'));
		}else{
			return Redirect::back();
		}

	}

	//tambah pegawai di dalam unit
	public function addPegawaiUnit($id){
		$uker = UnitKerja::where('unit_kerja_id', $id)->first();
		$satker = SatuanKerja::where('satuan_kerja_id', $uker->satuan_kerja_id)->first();
		$message =null;
		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $uker->satuan_kerja_id){
			return view('pages.add_pegawai_unit', compact('uker', 'satker','message'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3){
			return view('pages.add_pegawai_unit', compact('uker', 'satker','message'));
		}else{
			return Redirect::back();
		}

	}

	//tambah pegawai di dalam satuan kerja (ada memilih unit kerja)
	public function addPegawaiSkpd($id){
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
		$message =null;
		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $id){
			return view('pages.add_pegawai_skpd', compact('satker','message'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3){
			return view('pages.add_pegawai_skpd', compact('satker','message'));
		}else{
			return Redirect::back();
		}

	}

	//edit pegawai, jika pegawai tidak ada di db skpd, maka di copy dulu dari db bkd
	public function editPegawai($id){
		if(!in_array(Auth::user()->role_id, canSkpdEdit() ? [1,3,2,5,8,9] : [1,3])) {
			return Redirect::to('/home');
		}
		$pegawai = Pegawai2::where('peg_id', $id)->first();
		if($pegawai == null){
			$p = Pegawai::where('peg_id', $id)->first();
			$pegawai = new Pegawai2;
			$pegawai->peg_id = $id;
			$pegawai->id_goldar=$p->id_goldar;
			$pegawai->gol_id_awal=$p->gol_id_awal;
			$pegawai->id_pend_awal=$p->id_pend_awal;
			$pegawai->unit_kerja_id=$p->unit_kerja_id;
			$pegawai->kecamatan_id=$p->kecamatan_id;
			$pegawai->gol_id_akhir=$p->gol_id_akhir;
			$pegawai->jabatan_id=$p->jabatan_id	;
			$pegawai->id_pend_akhir=$p->id_pend_akhir;
			$pegawai->id_agama=$p->id_agama;
			$pegawai->peg_nip=$p->peg_nip;
			$pegawai->peg_nama=$p->peg_nama;
			$pegawai->peg_gelar_depan=$p->peg_gelar_depan;
			$pegawai->peg_gelar_belakang=$p->peg_gelar_belakang;
			$pegawai->peg_lahir_tempat=$p->peg_lahir_tempat;
			$pegawai->peg_lahir_tanggal=$p->peg_lahir_tanggal;
			$pegawai->peg_jenis_kelamin=$p->peg_jenis_kelamin;
			$pegawai->peg_status_perkawinan=$p->peg_status_perkawinan;
			$pegawai->peg_karpeg=$p->peg_karpeg;
			$pegawai->peg_karsutri=$p->peg_karsutri;
			$pegawai->peg_status_kepegawaian=$p->peg_status_kepegawaian;
			$pegawai->peg_cpns_tmt=$p->peg_cpns_tmt;
			$pegawai->peg_pns_tmt=$p->peg_pns_tmt;
			$pegawai->peg_gol_awal_tmt=$p->peg_gol_awal_tmt;
			$pegawai->peg_kerja_tahun=$p->peg_kerja_tahun;
			$pegawai->peg_kerja_bulan=$p->peg_kerja_bulan;
			$pegawai->peg_jabatan_tmt=$p->peg_jabatan_tmt;
			$pegawai->peg_no_askes=$p->peg_no_askes;
			$pegawai->peg_npwp=$p->peg_npwp;
			$pegawai->peg_bapertarum=$p->peg_bapertarum;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_kel_desa=$p->peg_kel_desa;
			$pegawai->peg_kodepos=$p->peg_kodepos;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_telp=$p->peg_telp;
			$pegawai->peg_telp_hp=$p->peg_telp_hp;
			$pegawai->peg_tmt_kgb=$p->peg_tmt_kgb;
			$pegawai->peg_foto=$p->peg_foto;
			$pegawai->peg_pend_awal_th=$p->peg_pend_awal_th;
			$pegawai->peg_pend_akhir_th=$p->peg_pend_akhir_th;
			$pegawai->peg_gol_akhir_tmt=$p->peg_gol_akhir_tmt;
			$pegawai->tgl_entry=$p->tgl_entry;
			$pegawai->satuan_kerja_id=$p->satuan_kerja_id;
			$pegawai->peg_status=$p->peg_status;
			$pegawai->peg_nip_lama=$p->peg_nip_lama;
			$pegawai->peg_ktp=$p->peg_ktp;
			$pegawai->peg_instansi_dpk=$p->peg_instansi_dpk;
			$pegawai->peg_ak=$p->peg_ak;
			$pegawai->peg_punya_kpe = $p->peg_punya_kpe;
			$pegawai->peg_punya_kpe_tgl = $p->peg_punya_kpe_tgl;
			$pegawai->peg_status_asn = $p->peg_status_asn;
			$pegawai->peg_status_gaji = $p->peg_status_gaji;
			$pegawai->id_status_kepegawaian = $p->id_status_kepegawaian;
			$pegawai->peg_jenis_asn = $p->peg_jenis_asn;
			$pegawai->peg_status_calon = $p->peg_status_calon;
			$pegawai->peg_jenis_pns = $p->peg_jenis_pns;
			$pegawai->save();

			$pg = RiwayatPenghargaan::where('peg_id', $id)->get();
			foreach ($pg as $pg2) {
				$r = RiwayatPenghargaan2::find($pg2->riw_penghargaan_id);
				if(!$r){
					$rp = new RiwayatPenghargaan2;
					$rp->peg_id = $pg2->peg_id;
					$rp->riw_penghargaan_id = $pg2->riw_penghargaan_id;
					$rp->penghargaan_id = $pg2->penghargaan_id;
					$rp->riw_penghargaan_instansi= $pg2->riw_penghargaan_instansi;
					$rp->riw_penghargaan_sk = $pg2->riw_penghargaan_sk;
					$rp->riw_penghargaan_tglsk = saveDate($pg2->riw_penghargaan_tglsk);
					$rp->riw_penghargaan_jabatan = $pg2->riw_penghargaan_jabatan;
					$rp->riw_penghargaan_lokasi = $pg2->riw_penghargaan_lokasi;
					$rp->save();
				}
			}
			$pf = RiwayatPendidikan::where('peg_id', $id)->get();
			foreach ($pf as $pf2) {
				$r2 = RiwayatPendidikan2::find($pf2->riw_pendidikan_id);
				if(!$r2){
					$rp2=new RiwayatPendidikan2;
					$rp2->riw_pendidikan_id = $pf2->riw_pendidikan_id;
					$rp2->peg_id = $pf2->peg_id;
					$rp2->id_pend = $pf2->id_pend;
					$rp2->tingpend_id= $pf2->tingpend_id;
					$rp2->riw_pendidikan_fakultas= $pf2->riw_pendidikan_fakultas;
					$rp2->jurusan_id= $pf2->jurusan_id;
					$rp2->univ_id = $pf2->univ_id;
					$rp2->riw_pendidikan_sttb_ijazah= $pf2->riw_pendidikan_sttb_ijazah;
					$rp2->riw_pendidikan_tgl = saveDate($pf2->riw_pendidikan_tgl);
					$rp2->riw_pendidikan_pejabat = $pf2->riw_pendidikan_pejabat;
					$rp2->riw_pendidikan_nm = $pf2->riw_pendidikan_nm;
					$rp2->riw_pendidikan_lokasi = $pf2->riw_pendidikan_lokasi;
					$rp2->fakultas_id = $pf2->fakultas_id;
					$rp2->save();
				}
			}

			$pn = RiwayatNonFormal::where('peg_id', $id)->get();
			foreach ($pn as $pn2) {
				$r3 = RiwayatNonFormal2::find($pn2->non_id);
				if(!$r3){
					$rp3=new  RiwayatNonFormal2;
					$rp3->non_id = $pn2->non_id;
					$rp3->peg_id = $pn2->peg_id;
					$rp3->non_nama= $pn2->non_nama;
					$rp3->non_tgl_mulai = saveDate($pn2->non_tgl_mulai);
					$rp3->non_tgl_selesai = saveDate($pn2->non_tgl_selesai);
					$rp3->non_sttp = $pn2->non_sttp;
					$rp3->non_sttp_tanggal = saveDate($pn2->non_sttp_tanggal);
					$rp3->non_sttp_pejabat = $pn2->non_sttp_pejabat;
					$rp3->non_penyelenggara = $pn2->non_penyelenggara;
					$rp3->non_tempat = $pn2->non_tempat;
					$rp3->save();
				}
			}

			$pp = RiwayatPangkat::where('peg_id', $id)->get();
			foreach ($pp as $pp2) {
				$r4 = RiwayatPangkat2::find($pp2->riw_pangkat_id);
				if(!$r4){
					$rp4=new RiwayatPangkat2;
					$rp4->riw_pangkat_id = $pp2->riw_pangkat_id;
					$rp4->peg_id = $pp2->peg_id;
					$rp4->riw_pangkat_thn= $pp2->riw_pangkat_thn;
					$rp4->riw_pangkat_bln = $pp2->riw_pangkat_bln;
					$rp4->riw_pangkat_gapok = $pp2->riw_pangkat_gapok;
					$rp4->riw_pangkat_sk = $pp2->riw_pangkat_sk;
					$rp4->riw_pangkat_sktgl =  saveDate($pp2->riw_pangkat_sktgl);
					$rp4->riw_pangkat_tmt =  saveDate($pp2->riw_pangkat_tmt);
					$rp4->riw_pangkat_pejabat = $pp2->riw_pangkat_pejabat;
					$rp4->riw_pangkat_unit_kerja = $pp2->riw_pangkat_unit_kerja;
					$rp4->gol_id = $pp2->gol_id;
					$rp4->save();
				}
			}

			$pj = RiwayatJabatan::where('peg_id', $id)->get();
			foreach ($pj as $pj2) {
				$r5 =  RiwayatJabatan2::find($pj2->riw_jabatan_id);
				if(!$r5){
					$rp5=new RiwayatJabatan2;
					$rp5->riw_jabatan_id = $pj2->riw_jabatan_id;
					$rp5->peg_id = $pj2->peg_id;
					$rp5->riw_jabatan_nm= $pj2->riw_jabatan_nm;
					$rp5->riw_jabatan_no = $pj2->riw_jabatan_no;
					$rp5->riw_jabatan_pejabat = $pj2->riw_jabatan_pejabat;
					$rp5->riw_jabatan_tgl = saveDate($pj2->riw_jabatan_tgl);
					$rp5->riw_jabatan_tmt =  saveDate($pj2->riw_jabatan_tmt);
					$rp5->riw_jabatan_unit = $pj2->riw_jabatan_unit;
					$rp5->gol_id = $pj2->gol_id;
					$rp5->save();
				}
			}

			$pd = RiwayatDiklat::where('peg_id',$id)->get();
			foreach ($pd as $pd2) {
				$r6 = RiwayatDiklat2::find($pd2->diklat_id);
				if(!$r6){
					$rp6=new RiwayatDiklat2;
					$rp6->diklat_id = $pd2->diklat_id;
					$rp6->peg_id = $pd2->peg_id;
					$rp6->kategori_id= $pd2->kategori_id;
					$rp6->diklat_fungsional_id= $pd2->diklat_fungsional_id;
					$rp6->diklat_teknis_id= $pd2->diklat_teknis_id;
					$rp6->diklat_jenis = $pd2->diklat_jenis;
					$rp6->diklat_mulai = saveDate($pd2->diklat_mulai);
					$rp6->diklat_selesai = saveDate($pd2->diklat_selesai);
					$rp6->diklat_penyelenggara = $pd2->diklat_penyelenggara;
					$rp6->diklat_tempat = $pd2->diklat_tempat;
					$rp6->diklat_jumlah_jam = $pd2->diklat_jumlah_jam;
					$rp6->diklat_sttp_no = $pd2->diklat_sttp_no;
					$rp6->diklat_sttp_tgl =  saveDate($pd2->diklat_sttp_tgl);
					$rp6->diklat_sttp_pej = $pd2->diklat_sttp_pej;
					$rp6->diklat_usul_no = $pd2->diklat_usul_no;
					$rp6->diklat_usul_tgl = $pd2->diklat_usul_tgl;
					$rp6->pejabat_id = $pd2->pejabat_id;
					$rp6->diklat_nama = $pd2->diklat_nama;
					$rp6->diklat_tipe = $pd2->diklat_tipe;
					$rp6->save();
				}
			}

			$pr = RiwayatKeluarga::where('peg_id', $id)->get();
			foreach ($pr as $pr2) {
				$r7 = RiwayatKeluarga2::find($pr2->riw_id);
				if(!$r7){
					$rp7=new RiwayatKeluarga2;
					$rp7->riw_id = $pr2->riw_id;
					$rp7->peg_id = $id;
					$rp7->riw_status = $pr2->riw_status;
					$rp7->riw_nama= $pr2->riw_nama;
					$rp7->riw_tgl_lahir =saveDate($pr2->riw_tgl_lahir);
					$rp7->riw_tempat_lahir = $pr2->riw_tempat_lahir;
					$rp7->riw_kelamin = $pr2->riw_kelamin;
					$rp7->riw_pendidikan = $pr2->riw_pendidikan;
					$rp7->riw_pekerjaan = $pr2->riw_pekerjaan;
					$rp7->riw_ket = $pr2->riw_ket;
					$rp7->riw_tgl_ket = $pr2->riw_tgl_ket;
					$rp7->riw_status_tunj = $pr2->riw_status_tunj;
					$rp7->riw_status_sutri = $pr2->riw_status_sutri;
					$rp7->riw_status_perkawinan = $pr2->riw_status_perkawinan;
					$rp7->save();
				}
			}

			$pk = RiwayatKeahlian::where('peg_id', $id)->get();
			foreach ($pk as $pk2) {
				$r8 = RiwayatKeahlian2::find($pk2->riw_keahlian_id);
				if(!$r8){
					$r8 = new RiwayatKeahlian2;
					$r8->riw_keahlian_id = $pk2->riw_keahlian_id;
					$r8->peg_id = $id;
					$r8->keahlian_id = $pk2->keahlian_id;
					$r8->keahlian_level_id = $pk2->keahlian_level_id;
					$r8->riw_keahlian_sejak = $pk2->riw_keahlian_sejak;
					if($r8->save()){
						$pkr = RiwayatKeahlianRel::where('riw_keahlian_id', $pk2->riw_keahlian_id)->first();
						$r9 = new RiwayatKeahlianRel2;
						$r9->riw_keahlian_rel_id = $pkr->riw_keahlian_rel_id;
						$r9->riw_keahlian_id = $pkr->riw_keahlian_id;
						$r9->non_id = $pkr->non_id;
						$r9->riw_pendidikan_id = $pkr->riw_pendidikan_id;
						$r9->diklat_id = $pkr->diklat_id;
						$r9->predikat = $pkr->predikat;
						$r9->save();
 					}
				}
			}

			$ph = RiwayatHukuman::where('peg_id', $id)->get();
			foreach ($ph as $ph2) {
				$r10 = RiwayatHukuman2::find($ph2->riw_hukum_id);
				if(!$r10){
					$rp10=new RiwayatHukuman2;
					$rp10->riw_hukum_id = $ph2->riw_hukum_id;
					$rp10->peg_id = $id;
					$rp10->mhukum_id = $ph2->mhukum_id;
					$rp10->riw_hukum_tmt= saveDate($ph2->riw_hukum_tmt);
					$rp10->riw_hukum_sd =saveDate($ph2->riw_hukum_sd);
					$rp10->riw_hukum_sk = $ph2->riw_hukum_sk;
					$rp10->riw_hukum_tgl = saveDate($ph2->riw_hukum_tgl);
					$rp10->riw_hukum_ket = $ph2->riw_hukum_ket;
					$rp10->save();
				}
			}

			$pfile = FilePegawai::where('peg_id', $id)->get();
			foreach ($pfile as $pfile2) {
				$r11 = FilePegawai2::find($pfile2->file_id);
				if(!$r11){
					$rp11=new FilePegawai2;
					$rp11->file_id = $pfile2->file_id;
					$rp11->peg_id = $id;
					$rp11->file_nama = $pfile2->file_nama;
					$rp11->file_ket = $pfile2->file_ket;
					$rp11->file_tgl= $pfile2->file_tgl;
					$rp11->file_lokasi =$pfile2->file_lokasi;
					$rp11->save();
				}
			}
		}
		$satker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();
		//dd(Auth::user()->satuan_kerja_id);
		/*if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $pegawai->satuan_kerja_id){
			return view('pages.edit_pegawai', compact('pegawai', 'satker'));
		}else */
		if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id==2 || Auth::user()->role_id == 8 || Auth::user()->role_id == 9){
			return view('pages.edit_pegawai', compact('pegawai', 'satker'));
		}else if(Auth::user()->role_id==4 && Auth::user()->username==$pegawai->peg_nip){
			return view('pages.edit_pegawai', compact('pegawai', 'satker'));
		}else{
			return Redirect::back();
		}

	}

	public function submitPegawai($id, Request $req){
		$data = $req->except('_token');

		$pegawai = Pegawai2::where('peg_id', $id)->first();

		$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
		if($log){
			$log->action ="usul_perubahan";
			if(Auth::user()->role_id==8){
				$log->status_id = 5;
			}
			else{
				$log->status_id = 2;
			}

			$log->editor_id = Auth::user()->id;
			$log->save();

		}else{
			$log = new StatusEditPegawai;
			$log->peg_id = $id;
			$log->peg_nip = $pegawai->peg_nip;
			$log->action ="usul_perubahan";
			if(Auth::user()->role_id==8){
				$log->status_id = 5;
			}
			else{
				$log->status_id = 2;
			}
			$log->editor_id = Auth::user()->id;
			$log->save();
		}
		logAction('Submit Pegawai','NIP : '.$pegawai->peg_nip,$pegawai->peg_id,Auth::user()->username);
		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $pegawai->satuan_kerja_id){
			return Redirect::to('/pegawai/profile/edit/'.$id)->with('message', 'Data telah di submit.');
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3){
			return Redirect::to('/pegawai/profile/edit/'.$id)->with('message', 'Data telah di submit.');
		}else{
			return Redirect::back();
		}


	}

	public function storePegawaiUnit($id, Request $req){
		$data = $req->except('_token');
		$cek = Pegawai::where('peg_nip', $req->input('peg_nip'))->first();
		if(!$cek){
			$uker = UnitKerja::where('unit_kerja_id', $id)->first();
			$id_satker = Auth::user()->satuan_kerja_id;
			date_default_timezone_set("Asia/Bangkok");
			$time = date("Y-m-d");
			$pg=Pegawai::select('peg_id')->where('peg_id','<',2000000000)->max('peg_id');
			$i=mt_rand();

			$pegawai = new Pegawai2;
			$pegawai->peg_id = $pg+$i;
			$pegawai->id_goldar = $req->input('id_goldar') ? $req->input('id_goldar') : null;
			$pegawai->gol_id_awal = $req->input('gol_id_awal') ? $req->input('gol_id_awal') : null;
			$pegawai->id_pend_awal = $req->input('id_pend_awal') ? $req->input('id_pend_awal') : null;
			$pegawai->unit_kerja_id = $id;
			$pegawai->kecamatan_id = $req->input('kecamatan_id') ? $req->input('kecamatan_id') : null;
			$pegawai->gol_id_akhir = $req->input('gol_id_akhir') ? $req->input('gol_id_akhir') : null;
			if($req->input('jenis_jabatan')==2){
				$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
			}else if ($req->input('jenis_jabatan')==4){
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 4;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jfu_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}elseif ($req->input('jenis_jabatan')==3) {
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 3;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jf_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}
			$pegawai->id_pend_akhir = $req->input('id_pend_akhir') ? $req->input('id_pend_akhir') : null;
			$pegawai->id_agama = $req->input('id_agama') ? $req->input('id_agama') : null;
			$pegawai->peg_nip = $req->input('peg_nip');
			$pegawai->peg_nama = $req->input('peg_nama');
			$pegawai->peg_gelar_depan = $req->input('peg_gelar_depan');
			$pegawai->peg_gelar_belakang = $req->input('peg_gelar_belakang');
			$pegawai->peg_lahir_tempat = $req->input('peg_lahir_tempat');
			$pegawai->peg_lahir_tanggal = saveDate($req->input('peg_lahir_tanggal'));
			$pegawai->peg_jenis_kelamin = $req->input('peg_jenis_kelamin');
			$pegawai->peg_status_perkawinan = $req->input('peg_status_perkawinan');
			$pegawai->peg_karpeg = $req->input('peg_karpeg');
			$pegawai->peg_karsutri = $req->input('peg_karsutri');
			$pegawai->peg_status_kepegawaian = $req->input('peg_status_kepegawaian') ? $req->input('peg_status_kepegawaian') : null;
			$pegawai->peg_cpns_tmt =saveDate($req->input('peg_cpns_tmt')) ;
			$pegawai->peg_pns_tmt = saveDate($req->input('peg_pns_tmt'));
			$pegawai->peg_gol_awal_tmt = saveDate($req->input('peg_gol_awal_tmt'));
			$pegawai->peg_kerja_tahun = $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : null;
			$pegawai->peg_kerja_bulan = $req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') : null;
			$pegawai->peg_jabatan_tmt = saveDate($req->input('peg_jabatan_tmt'));
			$pegawai->peg_no_askes = $req->input('peg_no_askes') ?  $req->input('peg_no_askes') : null;
			$pegawai->peg_npwp = $req->input('peg_npwp') ? $req->input('peg_npwp') : null;
			$pegawai->peg_bapertarum = $req->input('peg_bapertarum') ? $req->input('peg_bapertarum') : null;
			$pegawai->peg_rumah_alamat = $req->input('peg_rumah_alamat') ? $req->input('peg_rumah_alamat') : null;
			$pegawai->peg_kel_desa = $req->input('peg_kel_desa');
			$pegawai->peg_kodepos = $req->input('peg_kodepos') ? $req->input('peg_kodepos') : null;
			$pegawai->peg_telp = $req->input('peg_telp') ? $req->input('peg_telp') : null;
			$pegawai->peg_telp_hp = $req->input('peg_telp_hp') ? $req->input('peg_telp_hp') :null;
			$pegawai->peg_tmt_kgb = saveDate($req->input('peg_tmt_kgb'));
			$pegawai->peg_pend_awal_th = $req->input('peg_pend_awal_th') ? $req->input('peg_pend_awal_th') : null;
			$pegawai->peg_pend_akhir_th = $req->input('peg_pend_akhir_th') ?  $req->input('peg_pend_akhir_th') : null;
			$pegawai->peg_gol_akhir_tmt = saveDate($req->input('peg_gol_akhir_tmt'));
			$pegawai->tgl_entry = $time;
			$pegawai->satuan_kerja_id = Auth::user()->satuan_kerja_id;
			$pegawai->peg_status = 'TRUE';
			$pegawai->peg_nip_lama = $req->input('peg_nip_lama') ? $req->input('peg_nip_lama') : null;
			$pegawai->peg_ktp = $req->input('peg_ktp') ? $req->input('peg_ktp') : null;
			$pegawai->peg_instansi_dpk = $req->input('peg_instansi_dpk') ?  $req->input('peg_instansi_dpk') : null;
			$pegawai->peg_ak = $req->input('peg_ak') ? $req->input('peg_ak') : null;
			$pegawai->peg_email = $req->input('peg_email');
			$pegawai->peg_status_asn =  $req->input('peg_status_asn') ? $req->input('peg_status_asn') : null;
			$pegawai->peg_status_gaji = $req->input('peg_status_gaji') ?  $req->input('peg_status_gaji') : null;
			$pegawai->id_status_kepegawaian = $req->input('id_status_kepegawaian') ?  $req->input('id_status_kepegawaian') : null;
			$pegawai->peg_jenis_asn = $req->input('peg_jenis_asn') ?  $req->input('peg_jenis_asn') : null;
			$pegawai->peg_status_calon = $req->input('peg_status_calon') ?  $req->input('peg_status_calon') : null;
			$pegawai->peg_jenis_pns = $req->input('peg_jenis_pns') ?  $req->input('peg_jenis_pns') : null;

			if($req->file('peg_foto')){
				$destinationPath = 'uploads'; // upload path
			    $extension = Input::file('peg_foto')->getClientOriginalExtension(); // getting file extension
		        $filename = $req->input('peg_nip') . '.' . $extension; // renameing image
			    $upload_success = Input::file('peg_foto')->move($destinationPath, $filename); // uploading file to given path

			    if($upload_success){
			    	$pegawai->peg_foto = $filename;
				}
			}

			if($pegawai->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="tambah_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $req->input('peg_nip');
					$log->action ="tambah_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Tambah Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id);
			}
		}else{
			$uker = UnitKerja::where('unit_kerja_id', $id)->first();
			$satker = SatuanKerja::where('satuan_kerja_id', $uker->satuan_kerja_id)->first();
			$message = "NIP telah terdaftar";
			return view('pages.add_pegawai_unit', compact('cek','uker','satker', 'message'));
		}
	}

	public function storePegawaiSkpd($id, Request $req){
		$data = $req->except('_token');
		$cek = Pegawai::where('peg_nip', $req->input('peg_nip'))->first();
		if(!$cek){
			date_default_timezone_set("Asia/Bangkok");
			$time = date("Y-m-d");
			$pg=Pegawai::select('peg_id')->where('peg_id','<',2000000000)->max('peg_id');


		    $i=mt_rand();

			$pegawai = new Pegawai2;
			$pegawai->peg_id = $pg+$i;
			$pegawai->id_goldar = $req->input('id_goldar') ? $req->input('id_goldar') : null;
			$pegawai->gol_id_awal = $req->input('gol_id_awal') ? $req->input('gol_id_awal') : null;
			$pegawai->id_pend_awal = $req->input('id_pend_awal') ? $req->input('id_pend_awal') : null;
			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
			$pegawai->kecamatan_id = $req->input('kecamatan_id') ? $req->input('kecamatan_id') : null;
			$pegawai->gol_id_akhir = $req->input('gol_id_akhir') ? $req->input('gol_id_akhir') : null;
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;
			if($req->input('jenis_jabatan')==2){
				$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
			}else if ($req->input('jenis_jabatan')==4){
				if($unit_kerja == null){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 4;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
					$jab->jfu_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}elseif ($req->input('jenis_jabatan')==3) {
				if($unit_kerja == null){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 3;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
					$jab->jf_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}

			$pegawai->id_pend_akhir = $req->input('id_pend_akhir') ? $req->input('id_pend_akhir') : null;
			$pegawai->id_agama = $req->input('id_agama') ? $req->input('id_agama') : null;
			$pegawai->peg_nip = $req->input('peg_nip') ? $req->input('peg_nip') : null;
			$pegawai->peg_nama = $req->input('peg_nama') ? $req->input('peg_nama') : null;
			$pegawai->peg_gelar_depan = $req->input('peg_gelar_depan');
			$pegawai->peg_gelar_belakang = $req->input('peg_gelar_belakang');
			$pegawai->peg_lahir_tempat = $req->input('peg_lahir_tempat');
			$pegawai->peg_lahir_tanggal = saveDate($req->input('peg_lahir_tanggal'));
			$pegawai->peg_jenis_kelamin = $req->input('peg_jenis_kelamin');
			$pegawai->peg_status_perkawinan = $req->input('peg_status_perkawinan') ? $req->input('peg_status_perkawinan') : null;
			$pegawai->peg_karpeg = $req->input('peg_karpeg') ? $req->input('peg_karpeg') : '';
			$pegawai->peg_karsutri = $req->input('peg_karsutri') ? $req->input('peg_karsutri') : '';
			$pegawai->peg_jenis_asn = $req->peg_jenis_asn ? $req->peg_jenis_asn : null;
			$pegawai->peg_status_calon = $req->peg_status_calon ? $req->peg_status_calon : null;
			$pegawai->peg_jenis_pns = $req->peg_jenis_pns ? $req->peg_jenis_pns : null;
			$pegawai->status_tkd = $req->peg_status_tkd ? $req->peg_status_tkd : null;
			$pegawai->peg_status_kepegawaian = $req->input('peg_status_kepegawaian') ? $req->input('peg_status_kepegawaian') : null;
			$pegawai->peg_cpns_tmt =saveDate($req->input('peg_cpns_tmt')) ;
			$pegawai->peg_pns_tmt = saveDate($req->input('peg_pns_tmt'));
			$pegawai->peg_gol_awal_tmt = saveDate($req->input('peg_gol_awal_tmt'));
			$pegawai->peg_kerja_tahun = $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : "0";
			$pegawai->peg_kerja_bulan = $req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') : "0";
			$pegawai->peg_jabatan_tmt = saveDate($req->input('peg_jabatan_tmt'));
			$pegawai->peg_no_askes = $req->input('peg_no_askes') ? $req->input('peg_no_askes') : null;
			$pegawai->peg_npwp = $req->input('peg_npwp') ? $req->input('peg_npwp') : null;
			$pegawai->peg_bapertarum = $req->input('peg_bapertarum') ? $req->input('peg_bapertarum') : null;
			$pegawai->peg_rumah_alamat = $req->input('peg_rumah_alamat') ? $req->input('peg_rumah_alamat') : null;
			$pegawai->peg_kel_desa = $req->input('peg_kel_desa') ?  $req->input('peg_kel_desa') : '';
			$pegawai->peg_kodepos = $req->input('peg_kodepos') ? $req->input('peg_kodepos') : '';
			$pegawai->peg_telp = $req->input('peg_telp') ? $req->input('peg_telp') : '';
			$pegawai->peg_telp_hp = $req->input('peg_telp_hp') ? $req->input('peg_telp_hp') : '';
			$pegawai->peg_tmt_kgb = saveDate($req->input('peg_tmt_kgb'));
			$pegawai->peg_pend_awal_th = $req->input('peg_pend_awal_th') ? $req->input('peg_pend_awal_th') : null;
			$pegawai->peg_pend_akhir_th = $req->input('peg_pend_akhir_th') ?  $req->input('peg_pend_akhir_th') : null;
			$pegawai->peg_gol_akhir_tmt = saveDate($req->input('peg_gol_akhir_tmt'));
			$pegawai->tgl_entry = $time;
			$pegawai->satuan_kerja_id = $id;
			$pegawai->peg_status = 'TRUE';
			$pegawai->peg_nip_lama = $req->input('peg_nip_lama');
			$pegawai->peg_ktp = $req->input('peg_ktp');
			$pegawai->peg_instansi_dpk = $req->input('peg_instansi_dpk');
			$pegawai->peg_ak = $req->input('peg_ak');
			$pegawai->peg_email = $req->input('peg_email');
			$pegawai->peg_status_asn =  $req->input('peg_status_asn') ? $req->input('peg_status_asn') : null;
			$pegawai->peg_status_gaji = $req->input('peg_status_gaji') ? $req->input('peg_status_gaji')  : null;
			$pegawai->id_status_kepegawaian = $req->input('id_status_kepegawaian') ? $req->input('id_status_kepegawaian') : null;
			$pegawai->peg_jenis_asn = $req->input('peg_jenis_asn') ? $req->input('peg_jenis_asn') : null;
			$pegawai->peg_status_calon = $req->input('peg_status_calon') ? $req->input('peg_status_calon') : null;
			$pegawai->peg_jenis_pns = $req->input('peg_jenis_pns') ? $req->input('peg_jenis_pns') : null;

			if($req->file('peg_foto')){
				$destinationPath = 'uploads'; // upload path
			    $extension = Input::file('peg_foto')->getClientOriginalExtension(); // getting file extension
		        $filename = $req->input('peg_nip') . '.' . $extension; // renameing image
			    $upload_success = Input::file('peg_foto')->move($destinationPath, $filename); // uploading file to given path

			    if($upload_success){
			    	$pegawai->peg_foto = $filename;
				}
			}

			if($pegawai->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="tambah_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $req->input('peg_nip');
					$log->action ="tambah_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}

				logAction('Tambah Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id);
			}
		}else{
			$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
			$message = "NIP telah terdaftar";
			return view('pages.add_pegawai_skpd', compact('cek','satker', 'message'));
		}
	}

	//update data pegawai
	public function updatePegawai($id, Request $req){
		$data = $req->except('_token');
		date_default_timezone_set("Asia/Bangkok");
		$time = date("Y-m-d");

	   	$pegawai = Pegawai2::find($id);
		if($pegawai){
			if(empty($req->input('satuan_kerja_id')) || $req->input('satuan_kerja_id') == null){
				return Redirect::to('/pegawai/edit-pegawai/'.$pegawai->peg_id)->with('message', 'Gagal! Silahkan Pilih Satuan Kerja Terlebih Dahulu.');
			}


			$jabs = Jabatan::where('jabatan_id',$req->input('jabatan_id'))->first();
			$cekriwjab = RiwayatJabatan2::where('peg_id',$pegawai->peg_id)->orderBy('riw_jabatan_tmt','desc')->first();
			if($pegawai->jabatan_id != (int) $req->input('jabatan_id')){
				if($cekriwjab && $cekriwjab->riw_jabatan_tmt == saveDate($req->input('peg_jabatan_tmt'))){
					$rw_jab = RiwayatJabatan2::find($cekriwjab->riw_jabatan_id);
				}else{
					$rw_jab = new RiwayatJabatan2;
					$rw_jab->peg_id = $pegawai->peg_id;
					$rw_jab->riw_jabatan_tmt = saveDate($req->input('peg_jabatan_tmt'));
				}
				if($req->input('satuan_kerja_id') != 3){
					$idkec = SatuanKerja::where('satuan_kerja_nama','ilike','kecamatan%')->lists('satuan_kerja_id')->toArray();
					$allkec = UnitKerja::whereIn('satuan_kerja_id',$idkec)->lists('unit_kerja_id')->toArray();
					$kel = UnitKerja::where('unit_kerja_nama','ilike','kelurahan%')->lists('unit_kerja_id')->toArray();
					$allkel = UnitKerja::whereIn('unit_kerja_parent',$kel)->lists('unit_kerja_id')->toArray();
					$kelid = array_merge($kel,$allkel);

					if(in_array($req->input('unit_kerja_id'), $idkec)){
						$searchunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
						$nmkec = SatuanKerja::where('satuan_kerja_id',$searchunit->satuan_kerja_id)->first();
						$rw_jab->riw_jabatan_unit = $nmkec->satuan_kerja_nama;
					}elseif (in_array($req->input('unit_kerja_id'), $kelid)){
						$searchunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
						$nmkel = $searchunit->unit_kerja_parent ? UnitKerja::where('unit_kerja_id',$searchunit->unit_kerja_parent)->first() : $searchunit;
						$rw_jab->riw_jabatan_unit = $nmkel->unit_kerja_nama;
					}else{
						$nmskpd = SatuanKerja::where('satuan_kerja_id',$req->input('satuan_kerja_id'))->first();
						$rw_jab->riw_jabatan_unit = $nmskpd->satuan_kerja_nama;
					}
				}else{
					$searchunit = UnitKerja::where('unit_kerja_nama','ilike','bagian%')->lists('unit_kerja_id')->toArray();
					$bagian = UnitKerja::whereIn('unit_kerja_parent',$searchunit)->lists('unit_kerja_id')->toArray();
					if(in_array($req->input('unit_kerja_id'), $bagian)){
						$nmunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
						$namaunit = $nmunit->unit_kerja_parent ? unitKerja::where('unit_kerja_id',$nmunit->unit_kerja_parent)->first() : $nmunit;
						$rw_jab->riw_jabatan_unit = $nmunit->unit_kerja_nama;
					}else{
						$nmunit = UnitKerja::where('unit_kerja_id',$req->input('unit_kerja_id'))->first();
						$rw_jab->riw_jabatan_unit = $nmunit->unit_kerja_nama;
					}
				}
				$rw_jab->riw_jabatan_nm = $req->input('jabatan_nama');
				$rw_jab->riw_jabatan_no = $req->input('no_sk_jabatan');
				$rw_jab->riw_jabatan_tgl = saveDate($req->input('tanggal_sk_jabatan'));
				$rw_jab->riw_jabatan_pejabat = $req->input('jabatan_penandatangan_jab');
				$rw_jab->gol_id = $req->input('gol_id_akhir');
				$rw_jab->save();
			}
			if($pegawai->gol_id_akhir != $req->input('gol_id_akhir')){
				$rp=new RiwayatPangkat2;
				//$rp->riw_pangkat_id = rand();
				$rp->peg_id = $pegawai->peg_id;
				$rp->riw_pangkat_thn= $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : 0;
				$rp->riw_pangkat_bln =$req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') : 0;
				$rp->riw_pangkat_sk = $req->input('no_sk');
				$rp->riw_pangkat_sktgl =  saveDate($req->input('tanggal_sk'));
				$rp->riw_pangkat_tmt =  saveDate($req->input('peg_gol_akhir_tmt'));
				$rp->riw_pangkat_pejabat = $req->input('jabatan_penandatangan');
				$rp->riw_pangkat_unit_kerja = $req->input('unit_kerja_gol');
				$rp->gol_id = $req->input('gol_id_akhir') ? : null;
				if($rp->save()){
					$pegawai = Pegawai2::find($rp->peg_id);
					$log=StatusEditPegawai::where('peg_id', $rp->peg_id)->orderBy('id', 'desc')->first();
						if($log){
							$log->action ="add_riwayat_pangkat";
							$log->status_id = 1;
							$log->editor_id = Auth::user()->id;
							$log->save();
						}else{
							$log = new StatusEditPegawai;
							$log->peg_nip = $pegawai['peg_nip'];
							$log->peg_id = $rp->peg_id;
							$log->action ="add_riwayat_pangkat";
							$log->status_id = 1;
							$log->editor_id = Auth::user()->id;
							$log->save();
						}
				}
			}

			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : ($jabs ? $jabs->unit_kerja_id : null);
			$pegawai->id_goldar = $req->input('id_goldar') ? $req->input('id_goldar') : null;
			$pegawai->gol_id_awal = $req->input('gol_id_awal') ? $req->input('gol_id_awal') : null;
			$pegawai->id_pend_awal = $req->input('id_pend_awal') ? $req->input('id_pend_awal') : null;
			$pegawai->kecamatan_id = $req->input('kecamatan_id') ?  $req->input('kecamatan_id') : null;
			$pegawai->gol_id_akhir = $req->input('gol_id_akhir') ?  $req->input('gol_id_akhir') : null;
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

			if($pegawai->jabatan_id != $req->input('jabatan_id')){
				if($req->input('jenis_jabatan')==2){
					$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
				}else if ($req->input('jenis_jabatan')==4){
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jfu_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 4;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
						$jab->jfu_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}elseif ($req->input('jenis_jabatan')==3) {
					if($unit_kerja == null){
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
					}else{
						$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $unit_kerja)->where('jf_id', $req->input('jabatan_id'))->first();
					}
					if(!$jab){
						$jab = new Jabatan();
						$jab->jabatan_jenis = 3;
						$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
						$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
						$jab->jf_id = $req->input('jabatan_id');
						if($jab->save()){
							$pegawai->jabatan_id = $jab->jabatan_id;
						}
					}else{
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}
			}

			if($req->input('jabatan_is_calon_jft') == 'true' && !empty($req->input('jabatan_calon_jft_id')) && $req->input('jabatan_calon_jft_id') != null){
				$calon = CalonJft::find($pegawai->peg_id);
				if(!$calon){
					$calon = new CalonJft;
					$calon->peg_id = $pegawai->peg_id;
				}
				$calon->jf_id = $req->input('jabatan_calon_jft_id');
				$calon->is_calon = $req->input('jabatan_is_calon_jft') == 'true' ? true : false;
				$calon->save();
			}
			$pegawai->id_pend_akhir = $req->input('id_pend_akhir') ? $req->input('id_pend_akhir') : null;
			$pegawai->id_agama = $req->input('id_agama') ? $req->input('id_agama') : null;
			$pegawai->peg_nip = $req->input('peg_nip') ? $req->input('peg_nip') : null;
			$pegawai->peg_nama = $req->input('peg_nama') ? $req->input('peg_nama') : null;
			$pegawai->peg_gelar_depan = $req->input('peg_gelar_depan') ? $req->input('peg_gelar_depan') : null;
			$pegawai->peg_gelar_belakang = $req->input('peg_gelar_belakang') ? $req->input('peg_gelar_belakang') : null;
			$pegawai->peg_lahir_tempat = $req->input('peg_lahir_tempat') ?  $req->input('peg_lahir_tempat') : null;
			$pegawai->peg_lahir_tanggal = saveDate($req->input('peg_lahir_tanggal'));
			$pegawai->peg_jenis_kelamin = $req->input('peg_jenis_kelamin') ? $req->input('peg_jenis_kelamin') : null;
			$pegawai->peg_status_perkawinan = $req->input('peg_status_perkawinan') ? $req->input('peg_status_perkawinan') : null;
			$pegawai->peg_karpeg = $req->input('peg_karpeg') ? $req->input('peg_karpeg') : null;
			$pegawai->peg_karsutri = $req->input('peg_karsutri');
			$pegawai->peg_status_kepegawaian = $req->input('peg_status_kepegawaian');
			$pegawai->peg_cpns_tmt =saveDate($req->input('peg_cpns_tmt')) ;
			$pegawai->peg_pns_tmt = saveDate($req->input('peg_pns_tmt'));
			$pegawai->peg_gol_awal_tmt = saveDate($req->input('peg_gol_awal_tmt'));
			$pegawai->peg_kerja_tahun = $req->input('peg_kerja_tahun') ? $req->input('peg_kerja_tahun') : 0;
			$pegawai->peg_kerja_bulan = $req->input('peg_kerja_bulan') ? $req->input('peg_kerja_bulan') :0;
			$pegawai->peg_jabatan_tmt = saveDate($req->input('peg_jabatan_tmt'));
			$pegawai->peg_no_askes = $req->input('peg_no_askes');
			$pegawai->peg_npwp = $req->input('peg_npwp');
			$pegawai->peg_bapertarum = $req->input('peg_bapertarum') ? $req->input('peg_bapertarum') : null;
			$pegawai->peg_rumah_alamat = $req->input('peg_rumah_alamat');
			$pegawai->peg_kel_desa = $req->input('peg_kel_desa');
			$pegawai->peg_kodepos = $req->input('peg_kodepos') ? $req->input('peg_kodepos') : null;
			$pegawai->peg_telp = $req->input('peg_telp') ? $req->input('peg_telp') : null;
			$pegawai->peg_telp_hp = $req->input('peg_telp_hp') ? $req->input('peg_telp_hp') : null;
			$pegawai->peg_tmt_kgb = saveDate($req->input('peg_tmt_kgb'));
			$pegawai->peg_pend_awal_th = $req->input('peg_pend_awal_th');
			$pegawai->peg_pend_akhir_th = $req->input('peg_pend_akhir_th');
			$pegawai->peg_gol_akhir_tmt = saveDate($req->input('peg_gol_akhir_tmt'));
			// $pegawai->tgl_entry = $time;
			// $pegawai->satuan_kerja_id = $req->input('satuan_kerja_id');
			$pegawai->satuan_kerja_id = $req->input('satuan_kerja_id') ? $req->input('satuan_kerja_id') : ($jabs ? $jabs->satuan_kerja_id : null);
			$pegawai->peg_status = 'TRUE';
			$pegawai->peg_nip_lama = $req->input('peg_nip_lama');
			$pegawai->peg_ktp = $req->input('peg_ktp');
			$pegawai->peg_instansi_dpk = $req->input('peg_instansi_dpk');
			$pegawai->peg_ak = $req->input('peg_ak');
			$pegawai->peg_email = $req->input('peg_email');
			$pegawai->peg_status_asn =  $req->input('peg_status_asn') ? $req->input('peg_status_asn') : null;
			$pegawai->peg_status_gaji = $req->input('peg_status_gaji') ? $req->input('peg_status_gaji') : null;
			$pegawai->id_status_kepegawaian = $req->input('id_status_kepegawaian') ? $req->input('id_status_kepegawaian') : null;
			$pegawai->jenis_guru = $req->input('jenis_guru') ? $req->input('jenis_guru') : null;
			$pegawai->peg_jenis_asn = $req->input('peg_jenis_asn') ? $req->input('peg_jenis_asn') : null;
			$pegawai->peg_status_calon = $req->input('peg_status_calon') ? $req->input('peg_status_calon') : null;
			$pegawai->peg_jenis_pns = $req->input('peg_jenis_pns') ? $req->input('peg_jenis_pns') : null;
			$pegawai->status_tkd = $req->input('peg_status_tkd') ? $req->input('peg_status_tkd') : null;


			if($req->file('peg_foto')){
				$destinationPath = 'uploads'; // upload path
			    $extension = Input::file('peg_foto')->getClientOriginalExtension(); // getting file extension
		        $filename = $req->input('peg_nip') . '.' . $extension; // renameing image
			    $upload_success = Input::file('peg_foto')->move($destinationPath, $filename); // uploading file to given path

			    if($upload_success){
			    	$pegawai->peg_foto = $filename;
				}
			}

			if($pegawai->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="update_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $req->input('peg_nip');
					$log->action ="update_pegawai";
					$log->status_id = 1;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				logAction('Update Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
				return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id)->with('message', 'Data Berhasil Diupdate!');
			}
		}
	}

	public function pensiunPegawai($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai2::find($id);
		$pegawai->peg_status = 'FALSE';
		if($pegawai->save()){
			$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="pensiun_pegawai";
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action ="pensiun_pegawai";
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Set Pensiun Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
			return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id)->with('message', 'Pegawai Telah Dipensiunkan');
		}
	}

	public function updateStatusPegawai($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai2::find($id);
		$status = '';
		$cek = PegawaiPensiun::where('peg_id',$id)->first();

		if($cek){
			$pensiun = PegawaiPensiun::find($cek->rpensiun_id);
		}else{
			$pensiun = new PegawaiPensiun;
		}
		if($data['status'] == 'meninggal'){
			$pensiun->peg_id = $id;
			$pensiun->pensiun_id = 2;
			$pensiun->rpensiun_usia = hitungUmur($pegawai->peg_lahir_tanggal);
			$pensiun->rpensiun_nosk = $data['noskmeninggal'];
			$pensiun->rpensiun_tglsk = $data['tanggal_meninggal'] != '' ? $data['tanggal_meninggal'] : null;
			$pensiun->save();
			logAction('Set Meninggal Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama.', Tanggal Meninggal : '.$data['tanggal_meninggal'],$pegawai->peg_id,Auth::user()->username);

			$pegawai->peg_status = 'FALSE';
			$pegawai->peg_ketstatus = 'Meninggal ('.$data['tanggal_meninggal'].')';
			$status = 'pegawai_meninggal';
		}elseif($data['status'] == 'pensiun'){
			$pensiun->peg_id = $id;
			$pensiun->pensiun_id = 1;
			$pensiun->rpensiun_usia = $pegawai->peg_lahir_tanggal != null ? hitungUmur($pegawai->peg_lahir_tanggal) : 58;
			$pensiun->rpensiun_nosk = $data['noskpensiun'];
			$pensiun->rpensiun_tglsk = $data['tmt_pensiun'] != '' ? $data['tmt_pensiun'] : null;
			$pensiun->save();
			logAction('Set Pensiun Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama.', Tanggal Pensiun : '.$data['tmt_pensiun'],$pegawai->peg_id,Auth::user()->username);

			$pegawai->peg_status = 'FALSE';
			$pegawai->peg_ketstatus = 'Pensiun';
			$status = 'pensiun_pegawai';
		}elseif ($data['status'] == 'cuti') {
			$rcuti = new RiwayatCuti;
			$rcuti->peg_id = $id;
			$rcuti->jeniscuti_id = $data['jeniscuti_id'];
			$rcuti->cuti_no = $data['cuti_no'];
			$rcuti->cuti_tgl = date('Y-m-d');
			$rcuti->cuti_mulai = $data['cuti_mulai'];
			$rcuti->cuti_selesai = $data['cuti_selesai'];

			if($rcuti->save()){
				$cuti = new pegawaiInaktif;
				$cuti->peg_id = $id;
				$cuti->inaktif_mulai = $data['cuti_mulai'];
				$cuti->inaktif_selesai = $data['cuti_selesai'];
				switch($data['jeniscuti_id']){					
					case 6:	$cuti->inaktif_keterangan = 'Tugas belajar';
							break;
					case 8:	$cuti->inaktif_keterangan = 'Tugas belajar APBD';
							break;
					default:$cuti->inaktif_keterangan = 'Cuti diluar tanggungan negara';
				}				
				$cuti->save();

				logAction('Set Cuti Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama.'Jenis Cuti : '.$cuti->inaktif_keterangan,$pegawai->peg_id,Auth::user()->username);
			}
			$status = 'pegawai_cuti';
		}
		else{
			$pensiun->peg_id = $id;
			$pensiun->pensiun_id = 6;
			$pensiun->rpensiun_usia = hitungUmur($pegawai->peg_lahir_tanggal);
			$pensiun->rpensiun_nosk = $data['noskpindah'];
			$pensiun->rpensiun_tglsk = $data['tmt_pindah'];
			$pensiun->save();
			logAction('Set Pindah Pegawai','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama.', Pindah Ke : '.$data['ket_pindah'],$pegawai->peg_id,Auth::user()->username);

			$pegawai->peg_status = 'FALSE';
			$pegawai->peg_ketstatus = 'Pindah Ke Luar '.$data['ket_pindah'];
			$status = 'pindah_pegawai';
		}
		if($pegawai->save()){
			$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action =$status;
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action =$status;
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			return Redirect::to('/pegawai/profile/edit/'.$pegawai->peg_id)->with('message', 'Pegawai Telah Dipensiunkan');
		}
	}

	public function updateKedudukanPegawai($id, Request $req){
		$data = $req->except('_token');
		$status = '';
		$kedhuk= LogKedudukan::where('peg_id',$id)->exists();
		if($kedhuk){
			$temp = LogKedudukan::where('peg_id',$id)->orderBy('updated_at', 'desc')->first();
			$status = $temp->kedudukan_baru;
		}
		else
		{
			$status=0;
		}
		$satuan_kerja= Pegawai::where('peg_id',$id)->first();
		$logkedhuk = new LogKedudukan;
		$logkedhuk->peg_id = $id;
		$logkedhuk->kedudukan_lama = $status;
		$logkedhuk->kedudukan_baru = $data['kedudukan_id'];
		$logkedhuk->nomor_sk = $data['nosk'];
		$logkedhuk->tanggal_sk = $data['tanggal_sk'];
		$logkedhuk->tmt_kedudukan = $data['tmt_kedudukan'];
		$logkedhuk->updated_at = date('Y-m-d H:i:s');
		$logkedhuk->satuan_kerja_id = $satuan_kerja->satuan_kerja_id;

		if($logkedhuk->save()){

			$pegawai = Pegawai2::find($id);
			$pegawai->kedudukan_pegawai = $data['kedudukan_id'];

			if($pegawai->save()){


				$riwayat = new RiwayatKedHuk;
				$riwayat->peg_id = $id;
				$riwayat->status = $data['kedudukan_id'];
				$riwayat->no_sk = $data['nosk'];
				$riwayat->tgl_sk = $data['tanggal_sk'];
				$riwayat->tmt = $data['tmt_kedudukan'];
				$riwayat->satuan_kerja_id = $satuan_kerja->satuan_kerja_id;
				$riwayat->save();
			}
		}


		return Redirect::to('/pegawai/profile/edit/'.$id)->with('message', 'Kedudukan Pegawai Telah Ditambahkan');
	}

	public function pegawaiInaktif($id)
	{
		if($id=='pensiun'){
			$pegawai = Pegawai::where(function($query){
				$query->whereNull('peg_ketstatus')->orWhere('peg_ketstatus','like','Pensiun%');
			})->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
		}elseif($id=='meninggal'){
			$pegawai = Pegawai::where('peg_ketstatus','like','Meninggal%')->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
		}else{
			$pegawai = Pegawai::where('peg_ketstatus','like','Pindah%')->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
		}
		return view('pages.inaktif.index', compact('id','pegawai'));
	}

	public function getTingkatPendidikan(){
		$input_data = Input::except('_');
		$pend_id=$input_data['kat_pend_id'];

		$pend = Pendidikan::where('id_pend', $pend_id)->first();
		$kp = KategoriPendidikan::where('kat_pend_id', $pend->kat_pend_id)->first();
		$tp = TingkatPendidikan::where('tingpend_id', $kp['tingpend_id'])->first();

		$nama = $tp->nm_tingpend;
		echo json_encode($nama);
	}

	public function getJabatan(){
		$input_data = Input::except('_');
		$id = $input_data['jabatan_id'];

		$jabatan = Jabatan::where('jabatan_id', $id)->first();
		if($jabatan->jabatan_jenis == 2){
			$jenis = "Struktural";
		}else if($jabatan->jabatan_jenis == 3){
			$jenis = "Fungsional Tertentu";
		}else if($jabatan->jabatan_jenis == 4){
			$jenis = "Fungsional Umum";
		}

		$satker = SatuanKerja::where('satuan_kerja_id', $jabatan->satuan_kerja_id)->first();

		$arr = array(
		  'jenis'=>$jenis,
		  'satker'=>$satker->satuan_kerja_nama,
		  'id' =>$satker->satuan_kerja_id
		);
		echo json_encode($arr);
	}

	public function getJabatanSkpd(){
		$input_data = Input::except('_');
		$unit_kerja_id = $input_data['unit_kerja_id'] ? $input_data['unit_kerja_id'] : null;
		$satuan_kerja_id = isset($input_data['satuan_kerja_id']) && $input_data['satuan_kerja_id'] ? $input_data['satuan_kerja_id'] : Auth::user()->satuan_kerja_id;
		$jenis_jabatan = $input_data['jenis_jabatan'] ? $input_data['jenis_jabatan'] : null;

		if ($unit_kerja_id == 'all') {
			$jabatan = Jabatan::leftJoin('m_spg_unit_kerja','m_spg_unit_kerja.unit_kerja_id','=','m_spg_jabatan.unit_kerja_id')->where('m_spg_jabatan.satuan_kerja_id',$satuan_kerja_id)->where('jabatan_jenis',$jenis_jabatan)->where('jabatan_nama','not like','%-%')->where('jabatan_nama','!=',' ')->get();
		}
		else {
			$jabatan = Jabatan::where('satuan_kerja_id', $satuan_kerja_id)->where('unit_kerja_id', $unit_kerja_id)->where('jabatan_jenis', $jenis_jabatan)->get();
		}

		echo json_encode($jabatan);
	}

	public function getJabatanSkpdNew(){
		$input_data = Input::except('_');
		$unit_kerja_id = $input_data['unit_kerja_id'] ? $input_data['unit_kerja_id'] : null;
		$satuan_kerja_id = isset($input_data['satuan_kerja_id']) && $input_data['satuan_kerja_id'] ? $input_data['satuan_kerja_id'] : Auth::user()->satuan_kerja_id;
		$jenis_jabatan = $input_data['jenis_jabatan'] ? $input_data['jenis_jabatan'] : null;

		//dd($jenis_jabatan);
		$jabatan = Jabatan::where('satuan_kerja_id', $satuan_kerja_id)->where('unit_kerja_id', $unit_kerja_id)->where('jabatan_jenis', $jenis_jabatan)->get();
		$arr = array(
		  'jabatan'=>$jabatan,
		  'jenis'=>$jenis_jabatan,
		);
		echo json_encode($arr);
	}

	public function getJabatanFungsional(Request $request){
		$uker = $request->unit_kerja_id;
		$satker = $request->satuan_kerja_id;
		if (empty($uker)) {
			$jab = Jabatan::whereNull('unit_kerja_id')->where('satuan_kerja_id',$satker)->whereNotNull('jf_id')->get(['jf_id']);
		} else {
			$jab = Jabatan::where('unit_kerja_id',$uker)->where('satuan_kerja_id',$satker)->whereNotNull('jf_id')->get(['jf_id']);
		}
		$id = [];
		foreach ($jab as $j) {
			$id[] = $j->jf_id;
		}

		$input_data = Input::except('_');

		if(!empty($jab)){
			$jabatan = JabatanFungsional::get();
		}else{
			$jabatan = [];
		}

		echo json_encode($jabatan);
	}

	public function getJabatanFungsionalUmum(Request $request){
		$uker = $request->unit_kerja_id;
		$satker = $request->satuan_kerja_id;
		if (empty($uker)) {
			$jab = Jabatan::whereNull('unit_kerja_id')->where('satuan_kerja_id',$satker)->whereNotNull('jfu_id')->get(['jfu_id']);
		} else {
			$jab = Jabatan::where('unit_kerja_id',$uker)->where('satuan_kerja_id',$satker)->whereNotNull('jfu_id')->get(['jfu_id']);
		}
		$id = [];
		foreach ($jab as $j) {
			$id[] = $j->jfu_id;
		}
		$input_data = Input::except('_');
		if(!empty($jab)){
			$jabatan = JabatanFungsionalUmum::get();
		}else{
			$jabatan = [];
		}
		echo json_encode($jabatan);
	}


	public function getKabupaten(){
		$input_data = Input::except('_');
		$id = $input_data['kecamatan_id'];

		$kecamatan = Kecamatan::where("kecamatan_id", $id)->first();
    	$kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();

    	$arr = array(
		  'id'=>$kab->kabupaten_id,
		  'nama'=>$kab->kabupaten_nm
		);
		echo json_encode($arr);

	}

	public function getUnitKerja(){
		$input_data = Input::except('_');
		$id = $input_data['satuan_kerja_id'];
		$unit_kerja = collect();

		$unit_kerjas = UnitKerja::where('satuan_kerja_id', $id)->whereNull('unit_kerja_parent')->orderBy('unit_kerja_left')->get();
		foreach ($unit_kerjas as $uker) {
			$unit_kerja->push($uker);
			$unit_kerja = $unit_kerja->merge(getAllUnitKerjaChildrenObj($uker->unit_kerja_id));
		}
		echo json_encode($unit_kerja->transform(function($item,$key) {
			if ($item->unit_kerja_parent) {
				$item->unit_kerja_nama = str_repeat("-",($item->unit_kerja_level-1) * 2)." ".$item->unit_kerja_nama;
			}
			return $item;
		}));
	}

	public function cekNip(){
		$input_data = Input::except('_');
		$id = $input_data['peg_nip'];
		$nip = Pegawai::where('peg_nip', $id)->first();

		if($nip){
			$data = "false";
		}else{
			$data = "true";
		}
		echo json_encode($data);
	}

	public function addCek(){
		$input_data = Input::except('_');
		$id = $input_data['peg_nip'];
		$nip = Pegawai::where('peg_nip', $id)->first();
		$cek = $this->validateNip($id);
		if($nip){
			echo json_encode(['valid' => $cek,'pegawai' => $nip]);
		}else{
			echo json_encode(['valid' => $cek,'pegawai' => 0]);
		}

	}

	public function fileCek(){
		$input_data = Input::except('_');
		$id = $input_data['peg_id'];
		$nama = $input_data['file_nama'];

		$cek = FilePegawai2::where('peg_id',$id)->where('file_nama',$nama)->count();

		echo json_encode($cek);
	}

	public function editCek(){
		$input_data = Input::except('_');
		$id = $input_data['peg_nip'];
		$nip = Pegawai::where('peg_nip', $id)->first();
		$cek = $this->validateNip($id);

		if($nip){
			if($nip->peg_id == $input_data['peg_id']){
				echo json_encode(['valid' => $cek,'pegawai' => 0]);
			}else{
				echo json_encode(['valid' => $cek,'pegawai' => $nip]);
			}
		}else{
			echo json_encode(['valid' => $cek,'pegawai' => 0]);
		}

	}

	public function validateNip($nip) {
	        if (strlen($nip) != 18) {
	            return false;
	        }
	        if (strtotime(substr($nip, 0,8)) === false) {
	            return false;
	        }
	        if (strtotime(substr($nip, 8,6).'01') === false) {
	            return false;
	        }
	        if (!in_array(substr($nip, 14,1),['1','2'])) {
	            return false;
	        }
	        if (intval(substr($nip,-3)) == 0) {
	            return false;
	        }
	        return true;
	    }

	public function updateKPE($id){
		date_default_timezone_set("Asia/Bangkok");
        $date = date("Y-m-d");
		$input_data = Input::except('_');
		$pegawai = Pegawai2::find($id);
		$pegawai->peg_punya_kpe = $input_data['peg_punya_kpe'];
		$pegawai->peg_punya_kpe_tgl = $date;
		$pegawai->save();
	}

	public function addNewJFU(Request $req){
		$data = $req->except('_token');
		$jabatan = new Jabatan();
		$jabatan->jfu_id = $req->input('jfu_id');
		$jabatan->satuan_kerja_id = $req->input('satuan_kerja_id');
		$jabatan->unit_kerja_id = $req->input('unit_kerja_id');
		$jabatan->jabatan_jenis = 4;
		$jabatan->save();
	}

	//List Mutasi
	public function listMutasi(){
		if(Auth::user()->role_id != '2' && Auth::user()->role_id != '3'){
			return redirect('/');
		}
		$pegawai = Pegawai::where('satuan_kerja_id', 999999)->where('unit_kerja_id', 999999)->get();
		return view('pages.list_mutasi', compact('pegawai'));
	}

	public function MutasiPegawaiKeluar($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai2::find($id);
		$pegawai->satuan_kerja_id = 999999;
		$pegawai->unit_kerja_id = 999999;
		if($pegawai->save()){
			$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
			if($log){
				$log->action ="mutasi_keluar";
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}else{
				$log = new StatusEditPegawai;
				$log->peg_id = $pegawai->peg_id;
				$log->peg_nip = $pegawai->peg_nip;
				$log->action ="mutasi_keluar";
				$log->status_id =2;
				$log->editor_id = Auth::user()->id;
				$log->save();
			}
			logAction('Mutasi Pegawai Keluar','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
			return Redirect::to('/home')->with('message', 'Pegawai Telah Dimutasi Keluar, Tunggu Verifikasi BKD');
		}
	}

	public function MutasiPegawaiMasuk($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai::find($id);
		$id_satker = Auth::user()->satuan_kerja_id;
		$satker = SatuanKerja::where('satuan_kerja_id', $id_satker)->first();
		$asal = DB::select("select editor_id from status_edit_pegawai left join status_edit_pegawai_log
				on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
				where action = 'mutasi_keluar' and status_id = 4 and peg_id=".$pegawai->peg_id."
				order by updated_at desc limit 1");

		if($asal){
			foreach($asal as $a){
			$usr = User::where('id', $a->editor_id)->select('satuan_kerja_id')->first();
		}
			$satker_asal = SatuanKerja::where('satuan_kerja_id', $usr['satuan_kerja_id'])->first();
		}else{
			$satker_asal = SatuanKerja::where('satuan_kerja_id', $pegawai['satuan_kerja_id'])->first();
		}

		return view('pages.mutasi_masuk', compact('pegawai','satker', 'satker_asal'));
	}

	public function UpdateMutasiPegawaiMasuk($id, Request $req){
		$data = $req->except('_token');
		$pegawai = Pegawai::find($id);
		$pegawai->satuan_kerja_id = $req->input('satuan_kerja_id');
		$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;
		$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

		if($req->input('jenis_jabatan')==2){
			$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
		}else if ($req->input('jenis_jabatan')==4){
			if($unit_kerja){
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
			}else{
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
			}
			if(!$jab){
				$jab = new Jabatan();
				$jab->jabatan_jenis = 4;
				$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
				$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id')  : null;
				$jab->jfu_id = $req->input('jabatan_id');
				if($jab->save()){
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}else{
				$pegawai->jabatan_id = $jab->jabatan_id;
			}
		}elseif ($req->input('jenis_jabatan')==3) {
			if($unit_kerja){
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
			}else{
				$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
			}
			if(!$jab){
				$jab = new Jabatan();
				$jab->jabatan_jenis = 3;
				$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
				$jab->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id')  : null;
				$jab->jf_id = $req->input('jabatan_id');
				if($jab->save()){
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}else{
				$pegawai->jabatan_id = $jab->jabatan_id;
			}
		}

		if($pegawai->save()){
			$pegawai2 = Pegawai2::find($id);
			if($pegawai2){
				$pegawai2->satuan_kerja_id = $req->input('satuan_kerja_id');
				$pegawai2->unit_kerja_id = $req->input('unit_kerja_id');
				$pegawai2->jabatan_id = $pegawai->jabatan_id;
				$pegawai2->save();
			}


			$jabatan = Jabatan::where('jabatan_id', $req->input('jabatan_id'))->first();
			$rp=new RiwayatJabatan;
			$rp->peg_id = $id;
			if($jabatan['jabatan_jenis']==2){
				$rp->riw_jabatan_nm= $jabatan['jabatan_nama'];
			}else if($jabatan['jabatan_jenis']==3){
				$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
				$rp->riw_jabatan_nm= $jf['jf_nama'];
			}else if($jabatan['jabatan_jenis']==4){
				$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
				$rp->riw_jabatan_nm= $jfu['jfu_nama'];
			}
			$unit = UnitKerja::where('unit_kerja_id', $req->input('unit_kerja_id'))->first();
			$rp->riw_jabatan_unit = $unit['unit_kerja_nama'];
			if($rp->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="mutasi_masuk";
					$log->status_id =4;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $pegawai->peg_nip;
					$log->action ="mutasi_masuk";
					$log->status_id =4;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Mutasi Pegawai Masuk','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
				return Redirect::to('/home')->with('message', 'Pegawai Telah Dimutasi Masuk');
			}
		}
	}

	public function batalUsulan($id){
		$log = StatusEditPegawai::where('peg_id', $id)->orderBy('id', 'desc')->first();
		$pegawai = Pegawai::where('peg_id', $id)->first();
		if($log){
			$log->action ="batal_usulan";
			$log->status_id =1;
			$log->editor_id = Auth::user()->id;
			$log->save();
		}else{
			$log = new StatusEditPegawai;
			$log->peg_id = $id;
			$log->peg_nip = $pegawai->peg_nip;
			$log->action ="batal_usulan";
			$log->status_id =1;
			$log->editor_id = Auth::user()->id;
			$log->save();
		}
		return Redirect::to('/home')->with('message', 'Usulan Pegawai Dibatalkan');
	}

	public function Mutasi(){
		return view('pages.mutasi_search');
	}

	public function searchPegawai(Request $req){
		$models = Pegawai::select("*");
        $search = $req->get('search',false);
        $search = addslashes(strtolower($search));
		if ($search != '') {
            $models = $models->where('peg_nip','like',"%$search%")
                    ->orWhereRaw("lower(peg_nama) like '%$search%'");
        }
        $pegawai = $models->get();
        $count = count($pegawai);
        return view('pages.mutasi', compact('pegawai', 'count'));
	}

	public function mutasiMasukSKPD($id, Request $req){
		$pegawai = Pegawai2::find($id);
		if($pegawai){
			$pegawai->satuan_kerja_id = Auth::user()->satuan_kerja_id;
			$pegawai->peg_status = true;
			$pegawai->peg_ketstatus = '';
			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

			if($req->input('jenis_jabatan')==2){
				$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
			}else if ($req->input('jenis_jabatan')==4){
				if($unit_kerja){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 4;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jfu_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}elseif ($req->input('jenis_jabatan')==3) {
				if($unit_kerja){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 3;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jf_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}

			if($pegawai->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="mutasi_masuk_skpd";
					$log->status_id =2;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $pegawai->peg_nip;
					$log->action ="mutasi_masuk_skpd";
					$log->status_id =2;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
			logAction('Mutasi Pegawai Masuk','NIP : '.$pegawai->peg_nip.',Nama : '.$pegawai->peg_nama,$pegawai->peg_id,Auth::user()->username);
				return Redirect::to('/home')->with('message', 'Pegawai Telah Pindah Masuk, Tunggu Konfirmasi BKD');
			}
		}else{
			$p = Pegawai::where('peg_id', $id)->first();
			$pegawai = new Pegawai2;
			$pegawai->satuan_kerja_id = Auth::user()->satuan_kerja_id;
			$pegawai->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;
			$unit_kerja =  $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : NULL;

			if($req->input('jenis_jabatan')==2){
				$pegawai->jabatan_id = $req->input('jabatan_id') ? $req->input('jabatan_id') : null;
			}else if ($req->input('jenis_jabatan')==4){
				if($unit_kerja){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jfu_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 4;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jfu_id = $req->input('jabatan_id');

					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}elseif ($req->input('jenis_jabatan')==3) {
				if($unit_kerja){
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('unit_kerja_id', $req->input('unit_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}else{
					$jab = Jabatan::where('satuan_kerja_id', $req->input('satuan_kerja_id'))->where('jf_id', $req->input('jabatan_id'))->first();
				}
				if(!$jab){
					$jab = new Jabatan();
					$jab->jabatan_jenis = 3;
					$jab->satuan_kerja_id = $req->input('satuan_kerja_id');
					$jab->unit_kerja_id = $req->input('unit_kerja_id');
					$jab->jf_id = $req->input('jabatan_id');
					if($jab->save()){
						$pegawai->jabatan_id = $jab->jabatan_id;
					}
				}else{
					$pegawai->jabatan_id = $jab->jabatan_id;
				}
			}

			$pegawai->peg_id = $id;
			$pegawai->id_goldar=$p->id_goldar;
			$pegawai->gol_id_awal=$p->gol_id_awal;
			$pegawai->id_pend_awal=$p->id_pend_awal;
			$pegawai->kecamatan_id=$p->kecamatan_id;
			$pegawai->gol_id_akhir=$p->gol_id_akhir;
			$pegawai->id_pend_akhir=$p->id_pend_akhir;
			$pegawai->id_agama=$p->id_agama;
			$pegawai->peg_nip=$p->peg_nip;
			$pegawai->peg_nama=$p->peg_nama;
			$pegawai->peg_gelar_depan=$p->peg_gelar_depan;
			$pegawai->peg_gelar_belakang=$p->peg_gelar_belakang;
			$pegawai->peg_lahir_tempat=$p->peg_lahir_tempat;
			$pegawai->peg_lahir_tanggal=$p->peg_lahir_tanggal;
			$pegawai->peg_jenis_kelamin=$p->peg_jenis_kelamin;
			$pegawai->peg_status_perkawinan=$p->peg_status_perkawinan;
			$pegawai->peg_karpeg=$p->peg_karpeg;
			$pegawai->peg_karsutri=$p->peg_karsutri;
			$pegawai->peg_status_kepegawaian=$p->peg_status_kepegawaian;
			$pegawai->peg_cpns_tmt=$p->peg_cpns_tmt;
			$pegawai->peg_pns_tmt=$p->peg_pns_tmt;
			$pegawai->peg_gol_awal_tmt=$p->peg_gol_awal_tmt;
			$pegawai->peg_kerja_tahun=$p->peg_kerja_tahun;
			$pegawai->peg_kerja_bulan=$p->peg_kerja_bulan;
			$pegawai->peg_jabatan_tmt=$p->peg_jabatan_tmt;
			$pegawai->peg_no_askes=$p->peg_no_askes;
			$pegawai->peg_npwp=$p->peg_npwp;
			$pegawai->peg_bapertarum=$p->peg_bapertarum;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_kel_desa=$p->peg_kel_desa;
			$pegawai->peg_kodepos=$p->peg_kodepos;
			$pegawai->peg_rumah_alamat=$p->peg_rumah_alamat;
			$pegawai->peg_telp=$p->peg_telp;
			$pegawai->peg_telp_hp=$p->peg_telp_hp;
			$pegawai->peg_tmt_kgb=$p->peg_tmt_kgb;
			$pegawai->peg_foto=$p->peg_foto;
			$pegawai->peg_pend_awal_th=$p->peg_pend_awal_th;
			$pegawai->peg_pend_akhir_th=$p->peg_pend_akhir_th;
			$pegawai->peg_gol_akhir_tmt=$p->peg_gol_akhir_tmt;
			$pegawai->tgl_entry=$p->tgl_entry;
			$pegawai->peg_status=$p->peg_status;
			$pegawai->peg_nip_lama=$p->peg_nip_lama;
			$pegawai->peg_ktp=$p->peg_ktp;
			$pegawai->peg_instansi_dpk=$p->peg_instansi_dpk;
			$pegawai->peg_ak=$p->peg_ak;
			$pegawai->peg_punya_kpe = $p->peg_punya_kpe;
			$pegawai->peg_punya_kpe_tgl = $p->peg_punya_kpe_tgl;
			$pegawai->peg_status_asn =  $p->peg_status_asn;
			$pegawai->peg_status_gaji = $p->peg_status_gaji;
			$pegawai->id_status_kepegawaian = $p->id_status_kepegawaian;
			$pegawai->peg_jenis_asn = $p->peg_jenis_asn;
			$pegawai->peg_status_calon = $p->peg_status_calon;
			$pegawai->peg_jenis_pns = $p->peg_jenis_pns;
			//dd($pegawai->jabatan_id);

			$pg = RiwayatPenghargaan::where('peg_id', $id)->get();
			foreach ($pg as $pg2) {
				$r = RiwayatPenghargaan2::find($pg2->riw_penghargaan_id);
				if(!$r){
					$rp = new RiwayatPenghargaan2;
					$rp->peg_id = $pg2->peg_id;
					$rp->riw_penghargaan_id = $pg2->riw_penghargaan_id;
					$rp->penghargaan_id = $pg2->penghargaan_id;
					$rp->riw_penghargaan_instansi= $pg2->riw_penghargaan_instansi;
					$rp->riw_penghargaan_sk = $pg2->riw_penghargaan_sk;
					$rp->riw_penghargaan_tglsk = saveDate($pg2->riw_penghargaan_tglsk);
					$rp->riw_penghargaan_jabatan = $pg2->riw_penghargaan_jabatan;
					$rp->riw_penghargaan_lokasi = $pg2->riw_penghargaan_lokasi;
					$rp->save();
				}
			}
			$pf = RiwayatPendidikan::where('peg_id', $id)->get();
			foreach ($pf as $pf2) {
				$r2 = RiwayatPendidikan2::find($pf2->riw_pendidikan_id);
				if(!$r2){
					$rp2=new RiwayatPendidikan2;
					$rp2->riw_pendidikan_id = $pf2->riw_pendidikan_id;
					$rp2->peg_id = $pf2->peg_id;
					$rp2->id_pend = $pf2->id_pend;
					$rp2->tingpend_id= $pf2->tingpend_id;
					$rp2->riw_pendidikan_fakultas= $pf2->riw_pendidikan_fakultas;
					$rp2->jurusan_id= $pf2->jurusan_id;
					$rp2->univ_id = $pf2->univ_id;
					$rp2->riw_pendidikan_sttb_ijazah= $pf2->riw_pendidikan_sttb_ijazah;
					$rp2->riw_pendidikan_tgl = saveDate($pf2->riw_pendidikan_tgl);
					$rp2->riw_pendidikan_pejabat = $pf2->riw_pendidikan_pejabat;
					$rp2->riw_pendidikan_nm = $pf2->riw_pendidikan_nm;
					$rp2->riw_pendidikan_lokasi = $pf2->riw_pendidikan_lokasi;
					$rp2->save();
				}
			}

			$pn = RiwayatNonFormal::where('peg_id', $id)->get();
			foreach ($pn as $pn2) {
				$r3 = RiwayatNonFormal2::find($pn2->non_id);
				if(!$r3){
					$rp3=new  RiwayatNonFormal2;
					$rp3->non_id = $pn2->non_id;
					$rp3->peg_id = $pn2->peg_id;
					$rp3->non_nama= $pn2->non_nama;
					$rp3->non_tgl_mulai = saveDate($pn2->non_tgl_mulai);
					$rp3->non_tgl_selesai = saveDate($pn2->non_tgl_selesai);
					$rp3->non_sttp = $pn2->non_sttp;
					$rp3->non_sttp_tanggal = saveDate($pn2->non_sttp_tanggal);
					$rp3->non_sttp_pejabat = $pn2->non_sttp_pejabat;
					$rp3->non_penyelenggara = $pn2->non_penyelenggara;
					$rp3->non_tempat = $pn2->non_tempat;
					$rp3->save();
				}
			}

			$pp = RiwayatPangkat::where('peg_id', $id)->get();
			foreach ($pp as $pp2) {
				$r4 = RiwayatPangkat2::find($pp2->riw_pangkat_id);
				if(!$r4){
					$rp4=new RiwayatPangkat2;
					$rp4->riw_pangkat_id = $pp2->riw_pangkat_id;
					$rp4->peg_id = $pp2->peg_id;
					$rp4->riw_pangkat_thn= $pp2->riw_pangkat_thn;
					$rp4->riw_pangkat_bln = $pp2->riw_pangkat_bln;
					$rp4->riw_pangkat_gapok = $pp2->riw_pangkat_gapok;
					$rp4->riw_pangkat_sk = $pp2->riw_pangkat_sk;
					$rp4->riw_pangkat_sktgl =  saveDate($pp2->riw_pangkat_sktgl);
					$rp4->riw_pangkat_tmt =  saveDate($pp2->riw_pangkat_tmt);
					$rp4->riw_pangkat_pejabat = $pp2->riw_pangkat_pejabat;
					$rp4->riw_pangkat_unit_kerja = $pp2->riw_pangkat_unit_kerja;
					$rp4->gol_id = $pp2->gol_id;
					$rp4->save();
				}
			}

			$pj = RiwayatJabatan::where('peg_id', $id)->get();
			foreach ($pj as $pj2) {
				$r5 =  RiwayatJabatan2::find($pj2->riw_jabatan_id);
				if(!$r5){
					$rp5=new RiwayatJabatan2;
					$rp5->riw_jabatan_id = $pj2->riw_jabatan_id;
					$rp5->peg_id = $pj2->peg_id;
					$rp5->riw_jabatan_nm= $pj2->riw_jabatan_nm;
					$rp5->riw_jabatan_no = $pj2->riw_jabatan_no;
					$rp5->riw_jabatan_pejabat = $pj2->riw_jabatan_pejabat;
					$rp5->riw_jabatan_tgl = saveDate($pj2->riw_jabatan_tgl);
					$rp5->riw_jabatan_tmt =  saveDate($pj2->riw_jabatan_tmt);
					$rp5->riw_jabatan_unit = $pj2->riw_jabatan_unit;
					$rp5->gol_id = $pj2->gol_id;
					$rp5->save();
				}
			}

			$pd = RiwayatDiklat::where('peg_id',$id)->get();
			foreach ($pd as $pd2) {
				$r6 = RiwayatDiklat2::find($pd2->diklat_id);
				if(!$r6){
					$rp6=new RiwayatDiklat2;
					$rp6->diklat_id = $pd2->diklat_id;
					$rp6->peg_id = $pd2->peg_id;
					$rp6->kategori_id= $pd2->kategori_id;
					$rp6->diklat_fungsional_id= $pd2->diklat_fungsional_id;
					$rp6->diklat_teknis_id= $pd2->diklat_teknis_id;
					$rp6->diklat_jenis = $pd2->diklat_jenis;
					$rp6->diklat_mulai = saveDate($pd2->diklat_mulai);
					$rp6->diklat_selesai = saveDate($pd2->diklat_selesai);
					$rp6->diklat_penyelenggara = $pd2->diklat_penyelenggara;
					$rp6->diklat_tempat = $pd2->diklat_tempat;
					$rp6->diklat_jumlah_jam = $pd2->diklat_jumlah_jam;
					$rp6->diklat_sttp_no = $pd2->diklat_sttp_no;
					$rp6->diklat_sttp_tgl =  saveDate($pd2->diklat_sttp_tgl);
					$rp6->diklat_sttp_pej = $pd2->diklat_sttp_pej;
					$rp6->diklat_usul_no = $pd2->diklat_usul_no;
					$rp6->diklat_usul_tgl = $pd2->diklat_usul_tgl;
					$rp6->pejabat_id = $pd2->pejabat_id;
					$rp6->diklat_nama = $pd2->diklat_nama;
					$rp6->diklat_tipe = $pd2->diklat_tipe;
					$rp6->save();
				}
			}

			$pr = RiwayatKeluarga::where('peg_id', $id)->get();
			foreach ($pr as $pr2) {
				$r7 = RiwayatKeluarga2::find($pr2->riw_id);
				if(!$r7){
					$rp7=new RiwayatKeluarga2;
					$rp7->riw_id = $pr2->riw_id;
					$rp7->peg_id = $id;
					$rp7->riw_status = $pr2->riw_status;
					$rp7->riw_nama= $pr2->riw_nama;
					$rp7->riw_tgl_lahir =saveDate($pr2->riw_tgl_lahir);
					$rp7->riw_tempat_lahir = $pr2->riw_tempat_lahir;
					$rp7->riw_kelamin = $pr2->riw_kelamin;
					$rp7->riw_pendidikan = $pr2->riw_pendidikan;
					$rp7->riw_pekerjaan = $pr2->riw_pekerjaan;
					$rp7->riw_ket = $pr2->riw_ket;
					$rp7->riw_tgl_ket = $pr2->riw_tgl_ket;
					$rp7->riw_status_tunj = $pr2->riw_status_tunj;
					$rp7->riw_status_sutri = $pr2->riw_status_sutri;
					$rp7->riw_status_perkawinan = $pr2->riw_status_perkawinan;
					$rp7->save();
				}
			}

			$pk = RiwayatKeahlian::where('peg_id', $id)->get();
			foreach ($pk as $pk2) {
				$r8 = RiwayatKeahlian2::find($pk2->riw_keahlian_id);
				if(!$r8){
					$r8 = new RiwayatKeahlian2;
					$r8->riw_keahlian_id = $pk2->riw_keahlian_id;
					$r8->peg_id = $id;
					$r8->keahlian_id = $pk2->keahlian_id;
					$r8->keahlian_level_id = $pk2->keahlian_level_id;
					$r8->riw_keahlian_sejak = $pk2->riw_keahlian_sejak;
					if($r8->save()){
						$pkr = RiwayatKeahlianRel::where('riw_keahlian_id', $pk2->riw_keahlian_id)->first();
						$r9 = new RiwayatKeahlianRel2;
						$r9->riw_keahlian_rel_id = $pkr->riw_keahlian_rel_id;
						$r9->riw_keahlian_id = $pkr->riw_keahlian_id;
						$r9->non_id = $pkr->non_id;
						$r9->riw_pendidikan_id = $pkr->riw_pendidikan_id;
						$r9->diklat_id = $pkr->diklat_id;
						$r9->predikat = $pkr->predikat;
						$r9->save();
 					}
				}
			}

			if($pegawai->save()){
				$log = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id', 'desc')->first();
				if($log){
					$log->action ="mutasi_masuk_skpd";
					$log->status_id =2;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}else{
					$log = new StatusEditPegawai;
					$log->peg_id = $pegawai->peg_id;
					$log->peg_nip = $pegawai->peg_nip;
					$log->action ="mutasi_masuk_skpd";
					$log->status_id =2;
					$log->editor_id = Auth::user()->id;
					$log->save();
				}
				return Redirect::to('/home')->with('message', 'Pegawai Telah Pindah Masuk, Tunggu Konfirmasi BKD');
			}

		}
	}

	public function pegawaiStruktural(Request $request)
	{
		$pegawai = Pegawai::selectRaw('extract(year from age(peg_jabatan_tmt)) as usia_jabatan,*')->leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_pegawai_simpeg.jabatan_id')->where(DB::raw("extract(year from age(peg_jabatan_tmt))"),'>',5)->where('peg_status','TRUE')->where('m_spg_jabatan.jabatan_jenis',2)->whereNotNull('m_spg_jabatan.eselon_id')->get();

		return view('pages.list_pegawai_struktural',compact('pegawai'));
	}

	public function exportDataPensiun($id){
	    set_time_limit (5000);
	    ini_set('memory_limit','2048M');
	    date_default_timezone_set('Asia/Jakarta');
	    $data = [];
	    $header = [];
	    if($id=='pensiun'){
	    	$models = Pegawai::where(function($query){
	    		$query->whereNull('peg_ketstatus')->orWhere('peg_ketstatus','like','Pensiun%');
	    	})->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
	    }elseif($id=='meninggal'){
	    	$models = Pegawai::where('peg_ketstatus','like','Meninggal%')->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
	    }else{
	    	$models = Pegawai::where('peg_ketstatus','like','Pindah%')->where('peg_status', 'FALSE')->orderBy('peg_nama')->get();
	    }

	    foreach ($models as $i => $a) {
            $golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
            $jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
            if($a->peg_status_kepegawaian == '1'){
                $status = 'PNS';
                $tmt =  transformDate($a->peg_pns_tmt);
            }else if($a->peg_status_kepegawaian == '2'){
                $status = 'CPNS';
                $tmt = transformDate($a->peg_cpns_tmt);
            }else{
                $status = '-';
            }

            $pensiun = PegawaiPensiun::where('peg_id',$a->peg_id)->first();

            if($a->peg_gelar_belakang != null){
                $nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
            }else{
                $nama= $a->peg_gelar_depan.$a->peg_nama;
            }

            $tempat = str_replace(' ', '', $a->peg_lahir_tempat);
            if($jabatan['jabatan_jenis'] == 2){
                $jab_jenis = $a['jabatan']['eselon']['eselon_nm'];
                $nama_jabatan = $jabatan->jabatan_nama;
                $jabatan_kelas = $jabatan->jabatan_kelas;
            }elseif($jabatan['jabatan_jenis'] == 3){
                $jab_jenis = 'Fungsional Tertentu';
                $jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
                $nama_jabatan = $jf->jf_nama;
                $jabatan_kelas = $jf->jf_kelas;
            }elseif($jabatan['jabatan_jenis'] == 4){
                $jab_jenis ='Fungsional Umum';
                $jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
                if($jfu){
                    $nama_jabatan = $jfu['jfu_nama'];
                }else{
                    $nama_jabatan = 'Pelaksana';
                }
                $jabatan_kelas = $jfu->jfu_kelas;
            }else{
                $jab_jenis = '';
                $nama_jabatan = '';
                $jabatan_kelas = '';
            }

            if($a->peg_jenis_kelamin == 'L'){
                $jenis_kelamin = 'Laki-laki';
            }else if($a->peg_jenis_kelamin == 'P'){
                $jenis_kelamin = 'Perempuan';
            }else{
                $jenis_kelamin='-';
            }

            if($a->peg_status_perkawinan == '1'){
                $peg_status_perkawinan = 'Kawin';
            }elseif($a->peg_status_perkawinan == '2'){
                $peg_status_perkawinan = 'Belum Kawin';
            }elseif($a->peg_status_perkawinan == '3'){
                $peg_status_perkawinan = 'Janda';
            }elseif($a->peg_status_perkawinan == '4'){
                $peg_status_perkawinan = 'Duda';
            }else{
                $peg_status_perkawinan = '-';
            }

            if($a['unit_kerja']['unit_kerja_level'] == 1){
                $uker = $a->unit_kerja->unit_kerja_nama;
            }else if($a['unit_kerja']['unit_kerja_level'] == 2){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                if (strpos(strtolower($a['satuan_kerja']['satuan_kerja_nama']),'kecamatan') !== false) {
                    $uker = $parent['unit_kerja_nama'].', '.$a->unit_kerja->unit_kerja_nama;
                } else {
                    $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'];
                }
            }else if($a['unit_kerja']['unit_kerja_level'] == 3){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                $grandparent = UnitKerja::where('unit_kerja_id', $parent['unit_kerja_parent'])->first();
                $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'].', '.$grandparent['unit_kerja_nama'];
            }else{
                $uker = '';
            }

            $d = [
                $i+1,
                $nama,
                $tempat.','.transformDate($a->peg_lahir_tanggal),
                $a->peg_nip,
                $a['satuan_kerja']['satuan_kerja_nama'],
                $uker,
                $golongan['nm_gol'],
                transformDate($a->peg_gol_akhir_tmt),
                $jab_jenis,
                $nama_jabatan,
                $a->peg_ketstatus,
                $pensiun ? transformDate($pensiun->rpensiun_tglsk) : '',
                $jabatan_kelas,
                transformDate($a->peg_jabatan_tmt),
                $status,
                $tmt,
                $a->peg_kerja_tahun,
                $a->peg_kerja_bulan,
                $jenis_kelamin,
                $a['agama']['nm_agama'],
                $peg_status_perkawinan,
                $a['pendidikan_awal']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_awal']['nm_pend'],
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],
                $a->peg_no_askes,
                $a->peg_npwp,
                $a->peg_ktp,
                $a->peg_rumah_alamat.' '. $a->peg_kel_desa.' '. $a['kecamatan']['kecamatan_nm'],
                $a->peg_telp,
                $a->peg_telp_hp,
                $a->peg_email,
            ];

            $data[] = $d;

        }
        $h= [
                'No',
                'Nama',
                'Tempat Tanggal Lahir',
                'NIP',
                'Satuan Kerja',
                'Unit Kerja',
                'Golongan Pangkat',
                'TMT Golongan',
                'Eselon',
                'Nama Jabatan',
                'Jenis Inaktif',
                'Tanggal Inaktif',
                'Jabatan Kelas',
                'TMT Jabatan',
                'Status Pegawai',
                'TMT Pegawai',
                'Masa Kerja Tahun',
                'Masa Kerja Bulan',
                'Jenis Kelamin',
                'Agama',
                'Status Perkawinan',
                'Pendidikan Awal',
                'Pendidikan Akhir',
                'No Askes',
                'No NPWP',
                'No KTP',
                'Alamat Rumah',
                'Telp',
                'HP',
                'E-mail',
            ];
            $header = $h;

	    Excel::create('Data Pegawai '.ucwords($id), function($excel) use ($data, $header) {
	        $excel->sheet('Data Pegawai', function($sheet) use ($data, $header) {
	            $sheet->setFontFamily('Calibri');
	            $sheet->setFontSize(12);
	            $sheet->setAutoSize(true);
	            $sheet->mergeCells('A1:H1');

	            $sheet->cell('A1', function($cell) {
	                $cell->setAlignment('center');
	                $cell->setFontWeight('bold');
	                $cell->setFontSize(14);
	            });
	            $sheet->row(1, ['DATA PEGAWAI']);

	            $sheet->cells('A3:AD3', function($cells) {
	                $cells->setAlignment('center');
	                $cells->setFontWeight('bold');
	                $cells->setBackground('#b7dee8');
	            });
	            $sheet->row(3, $header);
	            $sheet->setAutoFilter('A3:AD3');

	            $sheet->fromArray($data, "", "A4", true, false);

	            for ($j = 3; $j < count($data) + 4; $j++) {
	                $sheet->setBorder('A'.$j.':AD'.$j, 'thin');
	            }

	            $sheet->setPageMargin(0.25);
	            $sheet->getPageSetup()->setFitToWidth(1);
	            $sheet->getPageSetup()->setFitToHeight(0);
	        });
	    })->download('xlsx');

	}

}
