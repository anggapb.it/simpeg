<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;

use Validator;
use Auth;
use Redirect;

use App\Model\TTDKGB;
use App\Model\Pegawai;

class TTDKGBController extends Controller
{    
    public function index(){
		// if(Auth::user()->role_id == 1){
			$list = TTDKGB::select('m_ttd_kgb.*',
							'gol1.nm_gol as nm_gol_awal',
							'gol2.nm_gol as nm_gol_akhir',
							'peg.peg_gelar_depan',
							'peg.peg_nama',
							'peg.peg_gelar_belakang',
							'peg.satuan_kerja_id',
							'uker.unit_kerja_id',
							'uker.unit_kerja_parent')->
							leftJoin('spg_pegawai_simpeg as peg', 'peg.peg_id', '=', 'm_ttd_kgb.peg_id_ttd')->
							leftJoin('m_spg_golongan as gol1', 'gol1.gol_id', '=', 'm_ttd_kgb.gol_awal_id')->
							leftJoin('m_spg_golongan as gol2', 'gol2.gol_id', '=', 'm_ttd_kgb.gol_akhir_id')->
							leftJoin('m_spg_unit_kerja as uker', 'uker.unit_kerja_id', '=', 'peg.unit_kerja_id')->
							// leftJoin('m_spg_unit_kerja as uker_parent', 'uker_parent.unit_kerja_id', '=', 'uker.unit_kerja_parent')->
							orderBy('m_ttd_kgb.id', 'asc')->
							get();
							// ->where('status', 1)->get();
			
			$list_golongan = \App\Model\Golongan::orderBy('gol_id')->lists('nm_gol','gol_id');
			$title = 'KGB';

			return view('pages.kgb.list_ttd_kgb', compact('list', 'list_golongan', 'title'));
		// }else{
		// 	return Redirect::back();
		// }
    }
    
    public function addDataTTDKGB(Request $req){
		// $data = $req->except('_token');
		$cek = TTDKGB::where('peg_id_ttd', $req->input('peg_id_ttd'))->count();
		if($req->input('peg_id_ttd') && $req->input('gol_awal_id') && $req->input('gol_akhir_id') && $req->input('pin') && $cek==0){
			$ttd = new TTDKGB();
			$ttd->gol_awal_id = $req->input('gol_awal_id')?:0; 
			$ttd->gol_akhir_id = $req->input('gol_akhir_id')?:0; 
			$ttd->peg_id_ttd = $req->input('peg_id_ttd');
			$ttd->jenis_jabatan = $req->input('jenis_jabatan');
			$ttd->pin 		 = Hash::make($req->pin);
			$ttd->status = 1;
			
			if($req->hasFile('photo_ttd')) {

				$filename = $req->photo_ttd->getClientOriginalName(); // getting file extension

				$validator = Validator::make($req->all(), [
						'photo_ttd' => 'max:2048',
					]);

				if ($validator->fails()) {
					return Redirect::back()
						->withInput($req->all())
						->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
						->withErrors('Max file upload size 2 MB');
				}

				$destinationPath = 'ttd_digital'; // upload path
				$extension = $req->photo_ttd->getClientOriginalExtension(); // getting file extension
				if (!in_array($extension,['jpg','jpeg','png'])) {
					return redirect()->back()->with('message','File ttd digital harus dalam bentuk JPG, JPEG atau PNG');
				}
				// while (File::exists(storage_path("$destinationPath/$filename"))) {
				//     $extension_pos = strrpos($filename, '.');
				//     $filename = substr($filename, 0, $extension_pos) .'_'.time(). substr($filename, $extension_pos);
				// }
				$nama_baru = time()."_".$filename;
				$req->photo_ttd->move(storage_path("$destinationPath"), $nama_baru);
				$ttd->photo_ttd = $nama_baru;
			}

			$ttd->save();
			logAction('Tambah Data TTD KGB',json_encode($ttd),$ttd->id,Auth::user()->username);
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('data pegawai, golongan dan pin Tidak boleh kosong dan Nama tidak boleh duplicate');
		}

		return Redirect::back()->with('message','Data berhasil ditambahkan');
	}

	public function updateTTDKGB($id, Request $req){
		// $data = $req->except('_token');
		$ttd = TTDKGB::find($id);
		$ttd->gol_awal_id  = $req->input('gol_awal_id'); 
        $ttd->gol_akhir_id = $req->input('gol_akhir_id'); 
        $ttd->peg_id_ttd   = $req->input('peg_id_ttd');
		$ttd->jenis_jabatan = $req->input('jenis_jabatan');
		$ttd->status 	   = $req->input('status');

		if($req->pin)
		$ttd->pin 		 = Hash::make($req->pin);
		
		if($req->hasFile('photo_ttd')) {

			$filename = $req->photo_ttd->getClientOriginalName(); // getting file extension
			$validator = Validator::make($req->all(), [
				'photo_ttd' => 'max:2048',
			]);

			if ($validator->fails()) {
				return Redirect::back()
					->withInput($req->all())
					->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
					->withErrors('Max file upload size 2 MB');
			}

			$destinationPath = 'ttd_digital'; // upload path
			$extension = $req->photo_ttd->getClientOriginalExtension(); // getting file extension
			if (!in_array($extension,['jpg','jpeg','png'])) {
				return redirect()->back()->with('message','File ttd digital harus dalam bentuk JPG, JPEG atau PNG');
			}
			// while (File::exists(storage_path("$destinationPath/$filename"))) {
			//     $extension_pos = strrpos($filename, '.');
			//     $filename = substr($filename, 0, $extension_pos) .'_'.time(). substr($filename, $extension_pos);
			// }
			$nama_baru = time()."_".$filename;
			$req->photo_ttd->move(storage_path("$destinationPath"), $nama_baru);

			$foto_lama = $ttd->photo_ttd;
			$ttd->photo_ttd = $nama_baru;
			
			if($foto_lama && $nama_baru != $foto_lama)
				unlink(storage_path("$destinationPath").'/'.$foto_lama);
		}

		$ttd->save();
		logAction('Edit Data TTD KGB',json_encode($ttd),$ttd->id,Auth::user()->username);
		return Redirect::back()->with('message','Data telah diupdate');
	}

	public function deleteTTDKGB($id){
		// if (Auth::user()->role_id == 1) {
			$ttd = TTDKGB::find($id)->first();

			$destinationPath = 'ttd_digital'; // upload path

			if($ttd->photo_ttd)
				unlink(storage_path("$destinationPath").'/'.$ttd->photo_ttd);

			$ttd->delete();
			logAction('Hapus Data TTD KGB','',$id,Auth::user()->username);
			return Redirect::back()->with('message','Delete Sukses');
		// }else{
		// 	return Redirect::back();
		// }
	}
}
