<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Auth;
use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\StatusEditPegawai;
use App\Model\KGB;        
use App\Model\TTDKGB;        
use App\Model\FilePegawai;        
use App\Model\FilePegawai2;        

class TtdDigitalKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id='')
    {
        if(!$id)
            $id = Auth::user()->satuan_kerja_id;

        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $title = 'KGB';

        return view('pages.kgb.ttd_digital', compact('satker', 'title'));
    }
    
    public function updateTtdDigital(Request $req) {
        $kgb = KGB::where('kgb_id', $req['kgb_id'])->first();
        $pegawai = Pegawai::where('peg_nip', Auth::user()->username)->first();
        $ttd_kgb = TTDKGB::where('peg_id_ttd', $pegawai->peg_id)->first();
        
        if($req->pin) {
        // if(Hash::check($req->pin, $ttd_kgb->pin)) {
            
            $esign = new \App\Esign\src\BSrE_Esign_Cli();

            // $status = $esign->checkStatus('30042019'); //nik

            // if(!$status) echo $esign->getError();
            // else echo $status;
            // die();
            $esign->setDocument(public_path('uploads\surat_kgb\\surat_KGB_'.$kgb->kgb_id.'.pdf'));

            //$esign->setDirOutput('/output', false);

            //$esign->setSuffixFileName('');
            
            $esign->setAppearance(
                $x = 406,
                $y = 188,
                $width = 474,
                $height = 390,
                $page = 1,
                $spesimen = public_path('images/barcode/'.$kgb->kgb_id.'.png'),
                //$spesimen = '',
                $qr = false
            );

            $hasil = $esign->sign(
                '30042019',   //nik
                '1234qwer'    //passphrase
            );

            if(!$hasil) {                
                return response()->json(['message' => '',
                                            'error' => $esign->getError()])->setStatusCode(400);   
            } else {
               // die;
                $kgb->ttd_digital = 1;
                $kgb->save();

                $rp=new FilePegawai2;
                $rp->peg_id = $kgb->peg_id;
                $rp->file_nama= 'SK KGB'.$kgb->kgb_thn;
                $rp->file_ket = 'SK KGB'.$kgb->kgb_thn;
                $rp->file_tgl = date('Y-m-d H:i:s');
                $rp->file_lokasi = 'surat_KGB_'.$kgb->kgb_id.'_signed.pdf';

                $rp->save();

                $id_pfile2 = $rp->file_id;
                $find_id11 = FilePegawai::where('file_id', $id_pfile2)->first();
                $rp11=new FilePegawai;
                if(!$find_id11){
                    $rp11->file_id = $id_pfile2;
                }else{
                    $id_rp11=FilePegawai::select('file_id')->max('file_id');
                    $find_rpid11 = FilePegawai::where('file_id', $id_rp11)->first();
                    if($find_rpid11){
                        $id_rp11 = $id_rp11+1;
                        $rp11->file_id = $id_rp11;
                    }else{
                        $rp11->file_id = $id_rp11;
                    }
                }
                        
                // $file=new FilePegawai;
                $rp11->peg_id = $kgb->peg_id;
                $rp11->file_nama= 'SK KGB'.$kgb->kgb_thn;
                $rp11->file_ket = 'SK KGB'.$kgb->kgb_thn;
                $rp11->file_tgl = date('Y-m-d H:i:s');
                $rp11->file_lokasi = 'surat_KGB_'.$kgb->kgb_id.'_signed.pdf';

                $rp11->save();
                copy(public_path('uploads\surat_kgb\\surat_KGB_'.$kgb->kgb_id.'_signed.pdf'), 'uploads/file/surat_KGB_'.$kgb->kgb_id.'_signed.pdf');

                logAction('Approve ttd digital KGB',json_encode($kgb),$req['kgb_id'],Auth::user()->username);
            //return response()->json(['message' => 'berhasil di update', 'error' => ''])->setStatusCode(200);           
                return response()->json(['message' => 'Proses tanda tangan digital berhasil', 'error' => ''])->setStatusCode(200);           
            }  
            
        } else {
            return response()->json(['message' => '',
                                        'error' => 'Pin tidak boleh kosong'])->setStatusCode(400);   
        }
    }
}
