<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Pegawai;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use Validator;
use Input;
use Redirect;
use Auth;

class UnitKerjaController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $satuan_kerja_id = $request->satuan_kerja_id;
        $satker = SatuanKerja::find($satuan_kerja_id);
        return view('pages.unit_kerja.list',compact('satker'));
        $url = url('data/uker/get-data');
        $columns = [
            'unit_kerja_nama' => ['Nama','unit_kerja_nama'],
            'satuan_kerja_id' => ['Satuan Kerja','satuan_kerja.satuan_kerja_nama'],
            'unit_kerja_level' => ['Unit Kerja Level','unit_kerja_level'],
            'unit_kerja_parent' => ['Unit Kerja Parent','unit_kerja_parent'],
            'status' => ['Status','status | formatStatus'],
        ];
        return view('pages.unit_kerja.index',compact('url','columns'));
	}

	public function getDataRender(Request $request) {
        $models = UnitKerja::select('m_spg_unit_kerja.*')->leftJoin('m_spg_satuan_kerja','m_spg_satuan_kerja.satuan_kerja_id','=','m_spg_unit_kerja.satuan_kerja_id')->where('unit_kerja_nama','not like', '%-%')->with('satuan_kerja');
        $params = $request->get('params',false);
        $search = $request->get('search',false);
        $search = addslashes(strtolower($search));
        $order  = $request->get('order' ,false);
        
        if ($order) {
            $order_direction = $request->get('order_direction','asc');
            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    default:
                        break;
                }
            }
        }
        
        if ($search != '') {
            $models = $models->whereRaw("lower(unit_kerja_nama) like '%$search%'")->orWhereRaw("lower(satuan_kerja_nama) like '%$search%'");
        }

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);
        
        $count = $models->count();
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as $key => $value) {
            $uker = UnitKerja::where('unit_kerja_id', $value->unit_kerja_parent)->first();
            $models[$key]['unit_kerja_parent'] = $uker['unit_kerja_nama'];

        }
        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create(Request $request){
        $satuan_kerja_id = $request->satuan_kerja_id;
        $satker = SatuanKerja::find($satuan_kerja_id);
        return view('pages.unit_kerja.create',compact('satker'));
    }

    public function store(Request $req){
        $max = UnitKerja::max('unit_kerja_id');
        $uker = new UnitKerja;
        $uker->unit_kerja_id = $max+1;
        $uker->unit_kerja_nama = $req->unit_kerja_nama;
        $uker->unit_kerja_level = $req->unit_kerja_level;
        $uker->satuan_kerja_id = $req->satuan_kerja_id;
        $uker->unit_kerja_parent = $req->get('unit_kerja_parent',null);
        if (empty($uker->unit_kerja_parent)) {
            $uker->unit_kerja_parent = null;
        }
        if (!$req->unit_kerja_after) $req->unit_kerja_after = 0;
        $uker_after = UnitKerja::find($req->unit_kerja_after);
        if ($uker_after) {
            $uker->unit_kerja_left = $uker_after->unit_kerja_left + 1;
            $uker->unit_kerja_right = $uker_after->unit_kerja_right + 1;
        } else {
            $ukers = UnitKerja::where('satuan_kerja_id',$uker->satuan_kerja_id)->where('unit_kerja_parent',$uker->unit_kerja_parent)->get();
            foreach ($ukers as $sibling) {
                $sibling->unit_kerja_left++;
                $sibling->unit_kerja_right++;
                $sibling->save();
            }
            $uker->unit_kerja_left = 2;
            $uker->unit_kerja_right = 60;
        }
        $uker->status = $req->status;
        $uker->save();
            logAction('Tambah Unit Kerja',json_encode($uker),$uker->unit_kerja_id,Auth::user()->username);

        return Redirect::to('/data/uker?satuan_kerja_id='.$req->satuan_kerja_id)->with('message', 'Data Telah Ditambahkan');
    }

    public function edit($id){
        $model = UnitKerja::find($id);
        $satker = SatuanKerja::find($model->satuan_kerja_id);
        return view('pages.unit_kerja.edit', compact('model','satker'));
    }

    public function update(Request $req, $id){
        $uker = UnitKerja::find($id);
        $uker->unit_kerja_nama = $req->unit_kerja_nama;
        $uker->unit_kerja_parent = $req->get('unit_kerja_parent',null);
        if (empty($uker->unit_kerja_parent)) {
            $uker->unit_kerja_parent = null;
        } else {
            $uker_parent = UnitKerja::find($req->unit_kerja_parent);
            $uker->unit_kerja_level = $uker_parent->unit_kerja_level + 1;
        }
        if ($id != $req->unit_kerja_after) {
            if ($req->unit_kerja_after == '0'){
                $req->unit_kerja_after = 0;
            }elseif (!$req->unit_kerja_after){
                $req->unit_kerja_after = $id;
            }
            $uker_after = UnitKerja::find($req->unit_kerja_after);
            if ($uker_after && $uker_after->unit_kerja_id != $id) {
                $uker->unit_kerja_left = $uker_after->unit_kerja_left + 1;
                $uker->unit_kerja_right = $uker_after->unit_kerja_right + 1;
            } elseif(!$uker_after) {
                $ukers = UnitKerja::where('satuan_kerja_id',$uker->satuan_kerja_id)->where('unit_kerja_parent',$uker->unit_kerja_parent)->get();
                foreach ($ukers as $sibling) {
                    $sibling->unit_kerja_left++;
                    $sibling->unit_kerja_right++;
                    $sibling->save();
                }
                $uker->unit_kerja_left = 2;
                $uker->unit_kerja_right = 60;
            }
        }
        $uker->status = $req->status;
        $uker->save();
            logAction('Edit Unit Kerja',json_encode($uker),$uker->unit_kerja_id,Auth::user()->username);
        return Redirect::to('/data/uker?satuan_kerja_id='.$uker->satuan_kerja_id)->with('message', 'Data Telah Ditambahkan');
    }

    public function destroy(Request $req, $id){
        $uker = UnitKerja::find($id);
        $satuan_kerja_id = $uker->satuan_kerja_id;
        $uker->is_deleted = true;
        $uker->save();
        logAction('Hapus Unit Kerja','',$id,Auth::user()->username);
        return Redirect::to('/data/uker?satuan_kerja_id='.$satuan_kerja_id)->with('message', 'Data Telah Dihapus');
    }

    public function getUnitKerjaParent(Request $req){
        $uker = UnitKerja::where('satuan_kerja_id', $req->satuan_kerja_id)->orderBy('unit_kerja_parent');

        if($req->unit_kerja_level == '2'){
            return $uker->where('unit_kerja_level', 1)->get();
        }elseif ($req->unit_kerja_level == '3') {
            $ukers = $uker->where('unit_kerja_level', 2)->get();
            foreach ($ukers as &$uker) {
                $uker->unit_kerja_nama = UnitKerja::where('unit_kerja_id',$uker->unit_kerja_parent)->first()->unit_kerja_nama.' >> '.$uker->unit_kerja_nama;
            }
            return $ukers;
        }elseif ($req->unit_kerja_level == '1') {
            return [['unit_kerja_id'=>null,'unit_kerja_nama'=>'Dibawah Kepala / Ketua']];
        }else{
            // return $uker->whereIn('unit_kerja_level', [1,2,3])->get();
            return [['unit_kerja_id'=>null,'unit_kerja_nama'=>'Pilih Organisasi dan Unit Kerja Level terlebih dahulu']];
        }
    }

}
