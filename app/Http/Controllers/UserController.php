<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use Redirect;
use Validator;
use Input;
use Hash;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index(){
		if(Auth::user()->role_id == 1){
			$user = User::all();
			return view('pages.list_user', compact('user'));
		}else{
			return Redirect::back();
		}
	}

	public function edit()
	{
		$id = Auth::user()->id;
		$user = User::find($id);
		return view('pages.edit_profile', compact('user'));
	}

	public function change($id)
	{
		$user = User::find($id);
		return view('pages.change_password', compact('user'));
	}

	public function update(Request $req, $id = 0)
	{
		$data = $req->except('_token');
		if ($id == 0 || Auth::user()->role_id != 1) {
			$id = Auth::user()->id;
			$user = User::find($id);
		} else {
			$user = User::find($id);
			$user->role_id = $req->input('role_id');
			$user->satuan_kerja_id = $req->input('satuan_kerja_id') ? $req->input('satuan_kerja_id') : null;
			$user->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
		}
		//$user->nama =  $req->input('nama');
		//$user->username = $req->input('username');
		if (User::where('username',$user->username)->where('id','<>',$id)->count()) {
			return Redirect::back()->with(array('error'=>'Terjadi kesalahan input data. Username sudah ada yang memiliki',
			'info'=>'danger','message'=>'Username sudah ada yang memiliki'));
		}
		
		if (!empty(Input::get('password_baru'))){
			$old_password = Input::get('password_lama');
			if (Hash::check($old_password, $user->password) || Auth::user()->role_id == 1){
				$user->password = Hash::make(Input::get('password_baru'));
			} else {
				return Redirect::back()->with(array('message'=>'Password lama tidak sesuai','info'=>'warning'));
			}	
		}
		$user->status_aktif = $req->input('status_aktif',$user->status_aktif);
		$user->save();
            logAction('Edit User','Username : '.$user->username,$id,Auth::user()->username);
		return Redirect::back()->with('message','Data berhasil di update');
		
	}

	public function changePassword($id, Request $req){
		$data = $req->except('_token');
		$validation = Validator::make(Input::all(), User::$change_password, User::$error_messages);
		if($validation->passes())
		{
			$user = User::find($id);
			$user->password =  $req->input('password');
			$user->save();
            logAction('Ganti Password','Username : '.$user->username,$id,Auth::user()->username);
			return Redirect::back()->with('Update Sukses');
		}else{
			return Redirect::back()
			->withInput()
			->with(array('error'=>'Terjadi kesalahan input data','info'=>'danger'))
			->withErrors($validation);
		}
	}

	public function delete($id){
		if (Auth::user()->role_id == 1) {
			User::find($id)->delete();
            logAction('Hapus User','',Auth::user()->username);
			return Redirect::back()->with('message','Delete Sukses');
		}else{
			return Redirect::back();
		}
	}

	public function addUser(Request $req){
		$data = $req->except('_token');
		if (Auth::user()->role_id == 1) {
			$validation = Validator::make(Input::all(), User::$rules, User::$error_messages);
			if($validation->passes()){
				$user = new User();
				$user->nama = $req->input('nama');
				$user->username = $req->input('username');
				$user->password = Hash::make($req->input('password'));
				$user->role_id = $req->input('role_id');
				$user->satuan_kerja_id = $req->input('satuan_kerja_id') ? $req->input('satuan_kerja_id') : null;
				$user->unit_kerja_id = $req->input('unit_kerja_id') ? $req->input('unit_kerja_id') : null;
				$user->status_aktif = $req->input('status_aktif');
				$user->save();
            	logAction('Tambah User','Username : '.$user->username,$user->id,Auth::user()->username);
				return Redirect::back()->with('message','User berhasil ditambahkan');
			}else{
				return Redirect::back()
				->withInput()
				->with(array('error'=>'Terjadi kesalahan input data','info'=>'danger'))
				->withErrors($validation);
			}
		}else{
			return Redirect::back();
		}
	}



}
