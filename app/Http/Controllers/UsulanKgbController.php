<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Redirect;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Pegawai;
use App\Model\UsulanKgb;
use App\Model\UsulanDetailKgb;

use Storage;
use Response;

class UsulanKgbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(!$id)
        $id = Auth::user()->satuan_kerja_id;
        
        // ini_set('max_execution_time', 10000);
        // ini_set('memory_limit','2048M');
		$satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        $title = 'KGB';

        return view('pages.kgb.usulan_kgb.index', compact('satker', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function input($usulan_id='')
    {
        // if(!$id)
        $id = Auth::user()->satuan_kerja_id;
        
        ini_set('max_execution_time', 10000);
        ini_set('memory_limit','2048M');
        $usulan = UsulanKgb::where('usulan_id', $usulan_id?:0)->with('statusUsulan')->first();

        $list_satker = SatuanKerja::where('satuan_kerja_id', $id)->first();
        
		$uker=UnitKerja::where('satuan_kerja_id', $id)->select('unit_kerja_id')->get();
		$unit = array();
		foreach ($uker as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$pegawai = Pegawai::where('satuan_kerja_id', $id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();


		if(Auth::user()->role_id==2 && Auth::user()->satuan_kerja_id == $id){
			return view('pages.kgb.usulan_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
		}else if(Auth::user()->role_id==1 || Auth::user()->role_id==3 || Auth::user()->role_id == 6){
			return view('pages.kgb.usulan_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
		}else if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE')->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.usulan_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
			}else{
				return Redirect::back();
			}
		}else if(Auth::user()->role_id == 9){
			if(Auth::user()->unit_kerja_id != null){
				$pegawai_induk = Pegawai::where('unit_kerja_id', Auth::user()->unit_kerja_id)->where('peg_status', 'TRUE');
				$pegawai = Pegawai::whereRaw('unit_kerja_id in (select unit_kerja_id from m_spg_unit_kerja where unit_kerja_parent = '.Auth::user()->unit_kerja_id.')')->where('peg_status', 'TRUE')->union($pegawai_induk)->orderBy('jabatan_id')->orderBy('gol_id_akhir', 'desc')->get();
				return view('pages.kgb.usulan_kgb.input', compact('id', 'list_satker','pegawai','usulan'));
			}else{
				return Redirect::back();
			}
		}else{
			return Redirect::back();
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req, $status=0)
    {
        // $data = $req->except('_token');
        $usulan_id = $req->usulan_id?:null;
        $cek = 0;

        if(!$usulan_id)
        $cek = UsulanKgb::where('no_usulan', $req->input('no_usulan'))->count();
        
		if($req->input('no_usulan') != null && $req->input('tgl_usulan') != null && $cek==0){
            if($usulan_id) {
                $usulan = UsulanKgb::find($usulan_id);
            } else {
                $usulan = new UsulanKgb();
            }

            $usulan_det = UsulanDetailKgb::where('usulan_id', $usulan_id)->count();
            
            if($usulan_det || $status == 0) {
                $usulan->no_usulan = $req->input('no_usulan'); 
                $usulan->tgl_usulan = saveDate($req->input('tgl_usulan')); 
                $usulan->satuan_kerja_id = Auth::user()->satuan_kerja_id;
                $usulan->user_id = Auth::user()->id;
                $usulan->status = $status;

                if($usulan->surat_pengantar || $req->hasFile('surat_pengantar')) {
                    if($req->hasFile('surat_pengantar')) {
                        $filename = $req->surat_pengantar->getClientOriginalName(); // getting file extension
        
                        $validator = Validator::make($req->all(), [
                                'surat_pengantar' => 'max:2048',
                            ]);
        
                        if ($validator->fails()) {
                            return Redirect::back()
                                ->withInput($req->all())
                                ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
                                ->withErrors('Max file upload size 2 MB');
                        }
        
                        $destinationPath = 'surat_pengantar_kgb'; // upload path
                        $extension = $req->surat_pengantar->getClientOriginalExtension(); // getting file extension
                        if (!in_array($extension,['pdf'])) {
                            return redirect()->back()->withInput($req->all())->with('message','File surat pengantar harus dalam bentuk PDF');
                        }
                        // while (File::exists(storage_path("$destinationPath/$filename"))) {
                        //     $extension_pos = strrpos($filename, '.');
                        //     $filename = substr($filename, 0, $extension_pos) .'_'.time(). substr($filename, $extension_pos);
                        // }
                        $nama_baru = time()."_".$filename;
                        $foto_lama = $usulan->surat_pengantar;

                        $req->surat_pengantar->move(storage_path("$destinationPath"), $nama_baru);
                        $usulan->surat_pengantar = $nama_baru;
                        
                        // $ttd->surat_pengantar = $nama_baru;
                        
                        if($foto_lama && $nama_baru != $foto_lama)
                            unlink(storage_path("$destinationPath").'/'.$foto_lama);
                    }
                } else {
                    return Redirect::back()->withInput($req->all())->with('message','Surat Pengantar harus diinput terlebih dahulu');    
                }

                $usulan->save();
                
                if($usulan_id){
                    logAction('Edit Data Usulan',json_encode($usulan),$usulan_id,Auth::user()->username);
                    return Redirect::to('kgb/usulan-kgb/input/'.$usulan_id)->with('message','Data berhasil diupdate');
                } else {
                    logAction('Tambah Data Usulan',json_encode($usulan),$usulan_id,Auth::user()->username);
                    return Redirect::back()->with('message','Data berhasil ditambahkan');
                }    
            } else {
                return Redirect::back()->withInput($req->all())->with('message','Detail usulan harus diinput terlebih dahulu');
            }   
		}else{
			return Redirect::back()
			->withInput($req->all())
            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
            ->withErrors('Isian Tidak boleh kosong dan No Usulan '.$req->input('no_usulan').' duplicate');
		}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (Auth::user()->role_id == 1) {
        // DB::transaction(function () {
            $usulan = UsulanKgb::find($id);
            
            $destinationPath = 'surat_pengantar_kgb'; // upload path

			if($usulan->surat_pengantar)
                unlink(storage_path("$destinationPath").'/'.$usulan->surat_pengantar);

            $usulan->delete();    

			UsulanDetailKgb::where('usulan_id', $id)->delete();
            logAction('Hapus Data Usulan','',$id,Auth::user()->username);

        // });
        
        return ['message' => 'Data Usulan telah dihapus'];
        // return Redirect::back()->with('message','Delete Sukses');
		// }else{
		// 	return Redirect::back();
		// }
    }

    public function storeDetail(Request $req, $usulan_id=null, $status=0)
    {
        // $data = $req->except('_token');
		// if($req->input('keahlian_nama') != null && $cek==0){
            // $usulan_id?:$req->usulan_id;
            
            if(!$usulan_id) {
                $cek = UsulanKgb::where('no_usulan', $req->input('no_usulan'))->count();
                if((!$req->input('no_usulan') && !$req->input('tgl_usulan') && !$req->hasFile('surat_pengantar')) || $cek) {
                    return response()->json(['message' => '',
                                        'error' => 'Data Usulan Tidak boleh kosong dan No Usulan '.$req->input('no_usulan').' duplicate',
                                        'usulan_id' => 0])->setStatusCode(400);                            
                }

                $usulan = new UsulanKgb();
                
                $usulan->no_usulan = $req->input('no_usulan'); 
                $usulan->tgl_usulan = saveDate($req->input('tgl_usulan')); 
                $usulan->satuan_kerja_id = Auth::user()->satuan_kerja_id;
                $usulan->user_id = Auth::user()->id;
                $usulan->status = $status;

                if($req->hasFile('surat_pengantar')) {

                    $filename = $req->surat_pengantar->getClientOriginalName(); // getting file extension
    
                    $validator = Validator::make($req->all(), [
                            'surat_pengantar' => 'max:2048',
                        ]);
    
                    if ($validator->fails()) {
                        return Redirect::back()
                            ->withInput($req->all())
                            ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
                            ->withErrors('Max file upload size 2 MB');
                    }
    
                    $destinationPath = 'surat_pengantar_kgb'; // upload path
                    $extension = $req->surat_pengantar->getClientOriginalExtension(); // getting file extension
                    if (!in_array($extension,['pdf'])) {
                        return redirect()->back()->withInput($req->all())->with('message','File surat pengantar harus dalam bentuk PDF');
                    }
                    // while (File::exists(storage_path("$destinationPath/$filename"))) {
                    //     $extension_pos = strrpos($filename, '.');
                    //     $filename = substr($filename, 0, $extension_pos) .'_'.time(). substr($filename, $extension_pos);
                    // }
                    $nama_baru = time()."_".$filename;
                    $foto_lama = $usulan->surat_pengantar;

                    $req->surat_pengantar->move(storage_path("$destinationPath"), $nama_baru);
                    $usulan->surat_pengantar = $nama_baru;
                    
                    // $ttd->surat_pengantar = $nama_baru;
                    
                    if($foto_lama && $nama_baru != $foto_lama)
                        unlink(storage_path("$destinationPath").'/'.$foto_lama);
                }
                
                $usulan->save();
                $usulan_id = $usulan->usulan_id;
            }

            $id = $req->usulan_id_detail?:0;

            if($req->usulan_id_detail) {
                $usulan_det = UsulanDetailKgb::find($req->usulan_id_detail);
            } else {
                $usulan_det = new UsulanDetailKgb();
            }

            $cek_peg_id = UsulanDetailKgb::where('usulan_id', $usulan_id)->
                            where('peg_id', $req->peg_id)->count();

            if($cek_peg_id) {
                $pegawai = Pegawai::find($req->peg_id);
                return response()->json(['message' => '',
                                    'error' => 'Data Usulan dengan nama '.$pegawai->peg_nama.' sudah ada !',
                                    'usulan_id' => $usulan_id])->setStatusCode(400);                            
            }

			$usulan_det->usulan_id = $usulan_id; 
			$usulan_det->peg_id = $req->input('peg_id'); 
            $usulan_det->user_id = Auth::user()->id;
			$usulan_det->status_usulan_pegawai = 0;
			$usulan_det->kgb_kerja_tahun = $req->input('kgb_kerja_tahun');
			$usulan_det->kgb_kerja_bulan = $req->input('kgb_kerja_bulan');
			$usulan_det->kgb_tmt = saveDate($req->input('kgb_tmt'));
            $usulan_det->save();
            
            if($id){
                logAction('Edit Data Usulan Detail',json_encode($usulan_det),$usulan_det->usulan_id_detail,Auth::user()->username);
                // return Redirect::back()->with('message','Data berhasil diupdate');
                return response()->json(['message' => 'Data berhasil diupdate',
                                        'error' => '',
                                        'usulan_id' => $usulan_id])->setStatusCode(200);                            
                
            } else {
                logAction('Tambah Data Usulan Detail',json_encode($usulan_det),$usulan_det->usulan_id_detail,Auth::user()->username);
                // return Redirect::back()->with('message','Data berhasil ditambahkan');
                return response()->json(['message' => 'Data berhasil ditambahkan',
                                        'error' => '',
                                        'usulan_id' => $usulan_id])->setStatusCode(200);                            
            }    
		// }else{
		// 	return Redirect::back()
		// 	->withInput($req->all())
        //     ->with(array('error'=>trans('Data gagal ditambahkan!'),'info'=>'warning'))
        //     ->withErrors('Isian Tidak boleh kosong dan Nama tidak boleh duplicate');
		// }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDetail($id)
    {
        // dd($id);
        // if (Auth::user()->role_id == 1) {
        // DB::transaction(function ($id) {
			UsulanDetailKgb::find($id)->delete();
			// UsulanDetailKgb::where('usulan_id', $id)->delete();
            logAction('Hapus Data Usulan Detail','',$id,Auth::user()->username);
        // });
    
        return ['message' => 'Data Usulan pegawai telah dihapus'];
        // return Redirect::back()->with('message','Delete Sukses');
		// }else{
		// 	return Redirect::back();
		// }
    }
}
