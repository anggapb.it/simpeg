<?php namespace App\Http\Controllers;

use Navigator;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		Navigator::setTemplate('vendor.navigator.template.arjuna');
		Navigator::set('Dashboard', url('home'), 'home');
		Navigator::set('User')->child(function($nav) {
			$nav->set('Semua', url('user'), 'users');
			$nav->set('Baru', url('user/create'), 'plus');
			return $nav;
		});
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('auth.login');
	}

}
