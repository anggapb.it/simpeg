<?php

namespace App\Http\Middleware;

use Auth;
use DB;
use Closure;
use Symfony\Component\HttpFoundation\Response;
use App\Model\UserDevice;
use App\Model\User;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $device = UserDevice::find($request->get('device_id'));
        $time   = $request->get('time',0);
        $key    = $request->get('key');
        if ($time < time() - 6000)
        {
            $response = [
                'response' => 'FAILED',
                'statusCode' => 403,
                'message' => 'Request token expired',
                'time' => time()
            ];
            return response()->json($response);
        }
        if (!$device)
        {
            $response = [
                'response' => 'FAILED',
                'statusCode' => 403,
                'message' => 'Unauthorized access: Unknown device_id'
            ];
            return response()->json($response);
        }
        if (md5(config('app.key') . $device->api_key . $time) != $key)
        {
            $response = [
                'response' => 'FAILED',
                'statusCode' => 403,
                'message' => 'Unauthorized access'. md5(config('app.key') . $device->api_key . $time) //debug key
                //'message' => 'Unauthorized access'
            ];
            return response()->json($response);
        }
        $user = User::find($device->user_id);
        Auth::login($user);

        return $next($request);
    }

}
