<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class BkdMiddleware
{
     protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->auth->guest()){
            if ($this->auth->user()->role_id == 3 || $this->auth->user()->role_id ==1 || $this->auth->user()->role_id ==2) {
                return $next($request);
            }else{
                return redirect('auth/login');
            }
        }
        if ($request->ajax())
        {
            return response('Unauthorized.', 401);
        }
        return redirect()->guest('auth/login');
    }
}
