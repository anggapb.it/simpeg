<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	private $openRoutes = ['api/*'];

	public function handle($request, Closure $next) {
		
		//CSRF Exclude
		foreach($this->openRoutes as $route) {
			if ($request->is($route)) {
		        return $next($request);
		      }
		}
		try {
			return parent::handle($request, $next);
		} catch (TokenMismatchException $e) {
			return redirect()->back()->withInput()
					->withErrors([
						'username' => "Mohon ulangi login",
					]);
		}
	}

}
