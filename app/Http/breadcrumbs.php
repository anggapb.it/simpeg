<?php

Breadcrumbs::register('welcome', function($breadcrumbs)
{
    $breadcrumbs->push('Welcome', route('welcome'));
});

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('semua', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Data Aset', route('semua'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Page]
Breadcrumbs::register('page', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('category', $page->category);
    $breadcrumbs->push($page->title, route('page', $page->id));
});
