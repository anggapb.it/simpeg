<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('as'=>'welcome','uses'=>'WelcomeController@index'));

Route::get('search-nip', 'GrafikNoAuthController@searchNip');
Route::post('search-nip/search', 'GrafikNoAuthController@prosesSearch');
Route::get('home', 'HomeController@index');
Route::get('home/read-notif', 'HomeController@readNotif');
Route::get('home/delete-notif', 'HomeController@deleteNotif');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['prefix' => 'user'], function() {
Route::get('all', 'UserController@index');
Route::get('data', 'UserController@getData');
Route::post('add','UserController@addUser');
Route::get('edit/', ['as' => 'edit','uses'=>'UserController@edit']);
Route::get('change/{id}', ['as' => 'change','uses'=>'UserController@change']);
Route::post('update/{id?}', 'UserController@update');
Route::post('change-password/{id}', 'UserController@changePassword');
Route::get('delete/{id}', 'UserController@delete');
});
//Route::get('list-permohonan2/{id}', 'SkpdController@indexPermohonan');
//Route::get('permohonan/get-data/{id}', 'SkpdController@getDataRender');
Route::get('list-unit/', 'SkpdController@index');
Route::get('list-unit/{id}', 'SkpdController@listUnit');
Route::get('list-pegawai/{id}/{ids}', 'SkpdController@listPegawai');
Route::get('list-all-pegawai/{id}', 'SkpdController@listAllPegawai');
Route::get('list-permohonan/get-data', 'SkpdController@listPermohonanData');
Route::get('list-permohonan/{id}', 'SkpdController@listPermohonan');
Route::get('list-log-edit/get-data', 'SkpdController@listLogEditData');
Route::get('list-log-edit/{id}', 'SkpdController@listLogEdit');
Route::get('pegawai/list/', 'SkpdController@getListPegawai');
Route::get('pegawai/profile/{id}', 'SkpdController@getProfilePegawai');
Route::get('pegawai-inaktif/{id}', 'SkpdController@pegawaiInaktif');
Route::get('pegawai-inaktif/export/{id}', 'SkpdController@exportDataPensiun');
Route::get('pegawai/profile/edit/{id}', 'SkpdController@getNewProfilePegawai');
Route::get('add-pegawai-unit/{id}', 'SkpdController@addPegawaiUnit');
Route::get('add-pegawai-skpd/{id}', 'SkpdController@addPegawaiSkpd');
Route::post('store-pegawai-unit/{id}', 'SkpdController@storePegawaiUnit');
Route::post('store-pegawai-skpd/{id}', 'SkpdController@storePegawaiSkpd');
Route::get('edit-pegawai/{id}', 'SkpdController@editPegawai');
Route::get('submit-pegawai/{id}', 'SkpdController@submitPegawai');
Route::post('update-pegawai/{id}', 'SkpdController@updatePegawai');
Route::post('pegawai/update-status/{id}', 'SkpdController@updateStatusPegawai');
Route::post('pegawai/update-kedudukan/{id}', 'SkpdController@updateKedudukanPegawai');
Route::get('pegawai/pensiun/{id}', 'SkpdController@pensiunPegawai');
Route::get('pegawai/mutasi-keluar/{id}', 'SkpdController@MutasiPegawaiKeluar');
Route::get('mutasi-masuk/{id}', 'SkpdController@MutasiPegawaiMasuk');
Route::post('mutasi-masuk/update/{id}', 'SkpdController@UpdateMutasiPegawaiMasuk');
Route::get('list-mutasi/', 'SkpdController@listMutasi');
Route::get('mutasi', 'SkpdController@Mutasi');
Route::post('search-pegawai', 'SkpdController@searchPegawai');
Route::post('mutasi-masuk-skpd/{id}', 'SkpdController@mutasiMasukSKPD');
Route::get('batal-usulan/{id}', 'SkpdController@batalUsulan');

Route::post('riwayat/add/penghargaan/', 'RiwayatController@AddRiwayatPenghargaan');
Route::post('riwayat/add/saudara/', 'RiwayatController@AddRiwayatSaudara');
Route::post('riwayat/add/anak/', 'RiwayatController@AddRiwayatAnak');
Route::post('riwayat/add/pasutri/', 'RiwayatController@AddRiwayatPasutri');
Route::post('riwayat/add/ortu/', 'RiwayatController@AddRiwayatOrangTua');
Route::post('riwayat/add/diklat-teknis/', 'RiwayatController@AddRiwayatDiklatTeknis');
Route::post('riwayat/add/diklat-fungsional/', 'RiwayatController@AddRiwayatDiklatFungsional');
Route::post('riwayat/add/diklat-struktural/', 'RiwayatController@AddRiwayatDiklatStruktural');
Route::post('riwayat/add/jabatan/', 'RiwayatController@AddRiwayatJabatan');
Route::post('riwayat/add/pangkat/', 'RiwayatController@AddRiwayatPangkat');
Route::post('riwayat/add/kgb/', 'RiwayatController@AddRiwayatKgb');
Route::post('riwayat/add/pendidikan-non/', 'RiwayatController@AddRiwayatPendidikanNon');
Route::post('riwayat/add/pendidikan/', 'RiwayatController@AddRiwayatPendidikan');
Route::post('riwayat/add/keahlian/', 'RiwayatController@AddRiwayatKeahlian');
Route::post('riwayat/add/bahasa/', 'RiwayatController@AddRiwayatBahasa');
Route::post('riwayat/add/hukuman/', 'RiwayatController@AddRiwayatHukuman');
Route::post('riwayat/add/file', 'RiwayatController@AddFilePegawai');
Route::post('riwayat/add/kepsek/', 'RiwayatController@AddRiwayatKepsek');
Route::post('riwayat/add/plt/', 'RiwayatController@AddRiwayatPlt');
Route::post('riwayat/add/cuti/', 'RiwayatController@AddRiwayatCuti');
Route::post('riwayat/add/kedhuk/', 'RiwayatController@AddRiwayatKedHuk');
Route::post('riwayat/add/kredit/', 'RiwayatController@AddAngkaKredit');
Route::post('riwayat/add/skp/', 'RiwayatController@AddSKP');
Route::post('riwayat/add/assessment/', 'RiwayatController@AddAssessment');
Route::post('upload-file/', 'RiwayatController@UploadFile');
Route::get('upload-assessment/', 'AssessmentController@UploadAssessment');
Route::post('upload-assessment/', 'AssessmentController@UploadAssessmentPost');

Route::post('riwayat/edit/penghargaan/{id}', 'RiwayatController@EditRiwayatPenghargaan');
Route::post('riwayat/edit/saudara/{id}', 'RiwayatController@EditRiwayatSaudara');
Route::post('riwayat/edit/anak/{id}', 'RiwayatController@EditRiwayatAnak');
Route::post('riwayat/edit/pasutri/{id}', 'RiwayatController@EditRiwayatPasutri');
Route::post('riwayat/edit/ortu/{id}', 'RiwayatController@EditRiwayatOrangTua');
Route::post('riwayat/edit/diklat-teknis/{id}', 'RiwayatController@EditRiwayatDiklatTeknis');
Route::post('riwayat/edit/diklat-fungsional/{id}', 'RiwayatController@EditRiwayatDiklatFungsional');
Route::post('riwayat/edit/diklat-struktural/{id}', 'RiwayatController@EditRiwayatDiklatStruktural');
Route::post('riwayat/edit/jabatan/{id}', 'RiwayatController@EditRiwayatJabatan');
Route::post('riwayat/edit/pangkat/{id}', 'RiwayatController@EditRiwayatPangkat');
Route::post('riwayat/edit/kgb/{id}', 'RiwayatController@EditRiwayatKgb');
Route::post('riwayat/edit/pmk/{id}', 'RiwayatController@EditRiwayatPmk');
Route::post('riwayat/edit/pendidikan-non/{id}', 'RiwayatController@EditRiwayatPendidikanNon');
Route::post('riwayat/edit/pendidikan/{id}', 'RiwayatController@EditRiwayatPendidikan');
Route::post('riwayat/edit/keahlian/{id}', 'RiwayatController@EditRiwayatKeahlian');
Route::post('riwayat/edit/bahasa/{id}', 'RiwayatController@EditRiwayatBahasa');
Route::post('riwayat/edit/hukuman/{id}', 'RiwayatController@EditRiwayatHukuman');
Route::post('riwayat/edit/kepsek/{id}', 'RiwayatController@EditRiwayatKepsek');
Route::post('riwayat/edit/plt/{id}', 'RiwayatController@EditRiwayatPlt');
Route::post('riwayat/edit/cuti/{id}', 'RiwayatController@EditRiwayatCuti');
Route::post('riwayat/edit/kedhuk/{id}', 'RiwayatController@EditRiwayatKedHuk');
Route::post('riwayat/edit/kredit/{id}', 'RiwayatController@EditAngkaKredit');
Route::post('riwayat/edit/skp/{id}', 'RiwayatController@EditSKP');
Route::post('riwayat/edit/assessment/{id}', 'RiwayatController@EditAssessment');
Route::post('riwayat/edit/file/{id}', 'RiwayatController@EditFilePegawai');

Route::get('riwayat/delete-penghargaan/{idp}/{id}', 'RiwayatController@deleteRiwayatPenghargaan');
Route::get('riwayat/delete-keluarga/{idp}/{id}', 'RiwayatController@deleteRiwayatKeluarga');
Route::get('riwayat/delete-diklat/{idp}/{id}', 'RiwayatController@deleteRiwayatDiklat');
Route::get('riwayat/delete-jabatan/{idp}/{id}', 'RiwayatController@deleteRiwayatJabatan');
Route::get('riwayat/delete-pangkat/{idp}/{id}', 'RiwayatController@deleteRiwayatPangkat');
Route::get('riwayat/delete-kgb/{idp}/{id}/{idBKD}', 'RiwayatController@deleteRiwayatKgb');
Route::get('riwayat/delete-pendidikan/{idp}/{id}', 'RiwayatController@deleteRiwayatPendidikan');
Route::get('riwayat/delete-pendidikan-non/{idp}/{id}', 'RiwayatController@deleteRiwayatPendidikanNon');
Route::get('riwayat/delete-keahlian/{idp}/{id}', 'RiwayatController@deleteRiwayatKeahlian');
Route::get('riwayat/delete-hukuman/{idp}/{id}', 'RiwayatController@deleteRiwayatHukuman');
Route::get('riwayat/delete-file/{idp}/{id}', 'RiwayatController@deleteFilePegawai');
Route::get('riwayat/delete-kepsek/{idp}/{id}', 'RiwayatController@deleteKepsek');
Route::get('riwayat/delete-plt/{idp}/{id}', 'RiwayatController@deletePlt');
Route::get('riwayat/delete-cuti/{idp}/{id}', 'RiwayatController@deleteCuti');
Route::get('riwayat/delete-kedhuk/{idp}/{id}', 'RiwayatController@deleteKedHuk');
Route::get('riwayat/delete-kredit/{idp}/{id}', 'RiwayatController@deleteAngkaKredit');
Route::get('riwayat/delete-skp/{idp}/{id}', 'RiwayatController@deleteSKP');
Route::get('riwayat/delete-assessment/{idp}/{id}', 'RiwayatController@deleteAssessment');
Route::get('riwayat/download-file/{idp}/{id}', 'RiwayatController@downloadFilePegawai');
Route::get('riwayat/getjam/', 'RiwayatController@Getjam');

Route::post('pegawai/revisi/{id}', 'BkdController@revisiPegawai');
Route::get('pegawai/approve/{id}', 'BkdController@approvePegawai');
Route::get('pegawai/approve/all/{id}', 'BkdController@approveAllPegawai');
Route::get('pegawai/pensiun/batal/{id}', 'BkdController@pensiunPegawai');
Route::get('pegawai/update/{stat}/batal/{id}', 'BkdController@updateStatusPegawai');
Route::get('pegawai/update/{stat}/batal/{id}', 'BkdController@updateStatusPegawai');
Route::get('pegawai/updateKPE/{id}', 'SkpdController@updateKPE');
Route::get('delete-pegawai/{id}', 'BkdController@deletePegawai');

Route::get('pegawai/tingkat-pendidikan/', 'SkpdController@getTingkatPendidikan');
Route::get('pegawai/jabatan/', 'SkpdController@getJabatan');
Route::get('pegawai/getJabatan/', 'SkpdController@getJabatanSkpd');
Route::get('pegawai/getJabatanNew/', 'SkpdController@getJabatanSkpdNew');
Route::get('pegawai/jabatanFungsional/', 'SkpdController@getJabatanFungsional');
Route::get('pegawai/jabatanFungsionalUmum/', 'SkpdController@getJabatanFungsionalUmum');
Route::get('pegawai/getUnitKerja/', 'SkpdController@getUnitKerja');
Route::get('pegawai/cek-nip/', 'SkpdController@cekNip');
Route::get('pegawai/add-cek/', 'SkpdController@addCek');
Route::get('pegawai/edit-cek/', 'SkpdController@editCek');
Route::get('pegawai/file-cek/', 'SkpdController@fileCek');
Route::get('pegawai/kabupaten/', 'SkpdController@getKabupaten');
Route::post('pegawai/new_jfu/', 'SkpdController@addNewJFU');


Route::get('list-all-pegawai/download/{id}/{uker}', 'ExportController@downloadPegawaiSkpd');
Route::get('list-all-pegawai/downloadduk/{id}', 'ExportController@downloadDukSkpd');
Route::get('download-list-pegawai/{id}', 'ExportController@downloadPegawaiUnit');
Route::get('pegawai/download/{id}', 'ExportController@downloadProfilePegawai');
Route::get('pegawai-bkd/download/{id}', 'ExportController@downloadProfilePegawaiBkd');

Route::get('data/keahlian','AdminController@dataKeahlian');
Route::post('data/keahlian/add','AdminController@AddDataKeahlian');
Route::post('data/keahlian/addchild/{id}','AdminController@AddDataKeahlianChild');
Route::post('data/keahlian/update/{id}','AdminController@UpdateDataKeahlian');
Route::get('data/keahlian/delete/{id}','AdminController@DeleteDataKeahlian');

Route::get('data/bahasa','AdminController@dataBahasa');
Route::post('data/bahasa/add','AdminController@AddDataBahasa');
Route::post('data/bahasa/update/{id}','AdminController@UpdateDataBahasa');
Route::get('data/bahasa/delete/{id}','AdminController@DeleteDataBahasa');

Route::post('data/keahlian/new','AdminController@dataKeahlianNew');
Route::post('data/bahasa/new','AdminController@dataBahasaNew');

Route::get('data/pengajuan','AdminController@dataPengajuan');
Route::post('data/pengajuan/add','AdminController@addDataPengajuan');
Route::post('data/pengajuan/update/{id}','AdminController@updateDataPengajuan');
Route::get('data/pengajuan/delete/{id}','AdminController@deleteDataPengajuan');

Route::group(['prefix' => 'data/satker'], function() {
	Route::get('/', 'SatuanKerjaController@index');
	Route::get('get-data', 'SatuanKerjaController@getDataRender');
	Route::get('create', 'SatuanKerjaController@create');
	Route::post('store', 'SatuanKerjaController@store');
	Route::get('edit/{id}', 'SatuanKerjaController@edit');
	Route::post('update/{id}', 'SatuanKerjaController@update');
	// Route::any('destroy/{id}', 'SatuanKerjaController@destroy');
});

Route::group(['prefix' => 'data/uker'], function() {
	Route::get('/', 'UnitKerjaController@index');
	Route::get('get-data', 'UnitKerjaController@getDataRender');
	Route::get('create', 'UnitKerjaController@create');
	Route::post('store', 'UnitKerjaController@store');
	Route::get('edit/{id}', 'UnitKerjaController@edit');
	Route::post('update/{id}', 'UnitKerjaController@update');
	Route::any('destroy/{id}', 'UnitKerjaController@destroy');
	Route::get('getUnitKerjaParent', 'UnitKerjaController@getUnitKerjaParent');
});

Route::group(['prefix' => 'data/jabatan'], function() {
	Route::get('/', 'JabatanController@index');
	Route::get('tambah', 'JabatanController@create');
	Route::post('store', 'JabatanController@store');
	Route::get('delete/{id}', 'JabatanController@delete');
	Route::get('edit/{id}', 'JabatanController@edit');
	Route::post('update/{id}', 'JabatanController@update');
	Route::get('get-data', 'JabatanController@getPegawai');
});

Route::group(['prefix' => 'data/jft'], function() {
	Route::get('/', 'JabatanController@indexJft');
	Route::get('tambah', 'JabatanController@createJft');
	Route::get('searchkojab', 'JabatanController@searchKojab');
	Route::post('store', 'JabatanController@storeJft');
	Route::get('delete/{id}', 'JabatanController@deleteJft');
	Route::get('edit/{id}', 'JabatanController@editJft');
	Route::post('update/{id}', 'JabatanController@updateJft');
	Route::get('get-pegawai', 'JabatanController@getPegawaiJf');
});

Route::group(['prefix' => 'data/jfu'], function() {
	Route::get('/', 'JabatanController@indexJfu');
	Route::get('tambah', 'JabatanController@createJfu');
	Route::get('searchkojab', 'JabatanController@searchKojabJfu');
	Route::post('store', 'JabatanController@storeJfu');
	Route::get('delete/{id}', 'JabatanController@deleteJfu');
	Route::get('edit/{id}', 'JabatanController@editJfu');
	Route::post('update/{id}', 'JabatanController@updateJfu');
	Route::get('get-pegawai', 'JabatanController@getPegawaiJfu');
});


Route::post('import/pegawai/jfu', ['uses'=>'ImportController@importJfuPegawai', 'as'=>'import[pegawai_jfu]']);
Route::get('import/pegawai/jfu', 'ImportController@importJfuPegawaiView');

Route::group(['prefix' => 'pushdata'], function(){
	Route::get('jabatan', 'BkdController@viewStaging');
	Route::get('jabatan/{id}', 'BkdController@pushData');
	Route::get('jabatan-all', 'BkdController@pushAllData');
	Route::get('pensiun', 'BkdController@viewStagingPensiun');
	Route::get('pensiun/{id}', 'BkdController@pushDataPensiun');
	Route::get('pensiun-all', 'BkdController@pushAllDataPensiun');
});

Route::group(['prefix' => 'evjab/jfu'], function(){
	Route::get('/', 'BkdController@evjabIndexJfu');
	Route::get('terisi', 'BkdController@evjabJfuTerisi');
	Route::get('tambah', 'BkdController@evjabCreateJfu');
	Route::get('searchkojab', 'BkdController@evjabSearchKojabJfu');
	Route::post('store', 'BkdController@evjabStoreJfu');
	Route::get('delete/{id}', 'BkdController@evjabDeleteJfu');
	Route::get('edit/{id}', 'BkdController@evjabEditJfu');
	Route::post('update/{id}', 'BkdController@evjabUpdateJfu');
	Route::get('get-pegawai', 'BkdController@evjabGetPegawaiJfu');
});

Route::group(['prefix' => 'evjab'], function(){
	Route::get('search', 'BkdController@evjabSearch');
	Route::get('get-data', 'BkdController@evjabGetDataRender');
	Route::get('search/profile/edit/{id}', 'BkdController@evjabGetProfilePegawai');
	Route::get('search/profile-ujikom/edit/{id}', 'BkdController@evjabGetProfileUjikomPegawai');
	Route::get('search/edit-pegawai/{id}', 'BkdController@evjabEditPegawai');
	Route::get('search/edit-pegawai-ujikom/{id}', 'BkdController@evjabEditUjikomPegawai');
	Route::get('search/delete-pegawai/{id}', 'BkdController@evjabDeletePegawai');
	Route::post('update-pegawai/{id}', 'BkdController@evjabUpdatePegawai');
	Route::post('update-pegawai-ujikom/{id}', 'BkdController@evjabUpdateUjikomPegawai');
	Route::get('list-unit', 'BkdController@evjabIndex');
	Route::get('list-unit/{id}', 'BkdController@evjabListUnit');
	Route::get('list-clear-unit', 'BkdController@evjabIndexClear');
	Route::get('list-clear-unit/{id}', 'BkdController@evjabListUnitClear');
	Route::get('list-pegawai-naik', 'BkdController@evjabListPegawaiNaik');
	Route::get('list-pegawai-naik/download', 'BkdController@evjabDownloadPegawaiNaik');
	Route::get('list-pegawai-turun', 'BkdController@evjabListPegawaiTurun');
	Route::get('list-pegawai-belum', 'BkdController@evjabListPegawaiBelum');
	Route::get('list-pegawai-ujikom', 'BkdController@evjabListPegawaiUjikom');
	Route::get('list-pegawai-ujikom/download', 'BkdController@evjabDownloadPegawaiUjikom');
	Route::get('list-unit/download/{id}/{status}', 'BkdController@evjabDownloadPegawaiSkpd');
});

Route::group(['prefix' => 'rekon-sapk'], function(){
	Route::get('upload', 'PegawaiSapkController@rekonUpload');
	Route::post('upload', 'PegawaiSapkController@rekonUploadPost');
	Route::get('sapk', 'PegawaiSapkController@rekonTidakAdaDiSapk');
	Route::get('simpeg', 'PegawaiSapkController@rekonTidakAdaDiSimpeg');
	Route::get('beda-gol', 'PegawaiSapkController@rekonBedaGolongan');
	Route::get('update-kp', 'PegawaiSapkController@updateKpKolektif');
	Route::post('update-kp', 'PegawaiSapkController@updateKpKolektifPost');
});

Route::get('pns/search', 'HomeController@search');
Route::get('pns/get-data', 'HomeController@getDataRender');
Route::get('pns/export', 'HomeController@exportData');
Route::get('pns-evjab/export', 'HomeController@exportDataEvjab');
Route::get('search', 'SearchController@index');

Route::group(['prefix' => 'search'], function() {
	Route::get('/', 'SearchController@index');
	Route::get('get-data', 'SearchController@getDataRender');
	Route::get('export', 'SearchController@exportData');
	Route::get('jabatan-kosong', 'SearchController@jabatanKosong');
	Route::get('get-data-jabatan-kosong', 'SearchController@getDataJabatanKosong');
	Route::get('export-jabatan-kosong', 'SearchController@exportDataJabatanKosong');
	Route::get('jabatan-kosong/non-eselon', 'SearchController@jabatanKosongNonEselon');
	Route::get('get-data-jabatan-kosong/non-eselon', 'SearchController@getDataJabatanKosongNonEselon');
	Route::get('export-jabatan-kosong/non-eselon', 'SearchController@exportDataJabatanKosongNonEselon');
});

Route::group(['prefix' => 'rekap'], function() {
	Route::get('/skpd', 'RekapController@skpd');
	Route::get('/jabatan', 'RekapController@jabatan');
	Route::get('/golongan', 'RekapController@golongan');
	Route::get('/jenis-kelamin', 'RekapController@JenisKelamin');
	Route::get('/pendidikan', 'RekapController@pendidikan');
	Route::get('/usia', 'RekapController@usia');
	Route::get('/eselon-jk', 'RekapController@eselonJK');
	Route::get('get-data-jabatan', 'RekapController@getDataJabatan');
	Route::get('get-data-golongan', 'RekapController@getDataGolongan');
	Route::get('get-data-jenis-kelamin', 'RekapController@getDataJenisKelamin');
	Route::get('get-data-pendidikan', 'RekapController@getDataPendidikan');
	Route::get('get-data-usia', 'RekapController@getDataUsia');
	Route::get('get-data-eselon-jk', 'RekapController@getDataEselonJK');
	Route::get('/jabatan/export', 'RekapController@exportRekapJabatan');
	Route::get('/golongan/export', 'RekapController@exportRekapGolongan');
	Route::get('/pendidikan/export', 'RekapController@exportRekapPendidikan');
	Route::get('/jenis-kelamin/export', 'RekapController@exportRekapJenisKelamin');
	Route::get('/skpd/export', 'RekapController@exportRekapSKPD');
	Route::get('/usia/export', 'RekapController@exportRekapUsia');
	Route::get('/eselon-jk/export', 'RekapController@exportRekapEselonJk');
});

Route::group(['prefix' => 'grafik'], function() {
	Route::get('/skpd', 'GrafikController@skpd');
	Route::get('/jabatan', 'GrafikController@jabatan');
	Route::get('/golongan', 'GrafikController@golongan');
	Route::get('/jenis-kelamin', 'GrafikController@JenisKelamin');
	Route::get('/pendidikan', 'GrafikController@pendidikan');
	Route::get('/usia', 'GrafikController@usia');
	Route::get('/eselon-jk', 'GrafikController@eselonJK');
	Route::get('get-data-jabatan/{id?}', 'GrafikController@getDataJabatan');
	Route::get('get-data-golongan/{id?}', 'GrafikController@getDataGolongan');
	Route::get('get-data-jenis-kelamin/{id?}', 'GrafikController@getDataJenisKelamin');
	Route::get('get-data-pendidikan/{id?}', 'GrafikController@getDataPendidikan');
	Route::get('get-data-usia/{id?}', 'GrafikController@getDataUsia');
	Route::get('get-data-eselon-jk/{id?}', 'GrafikController@getDataEselonJK');
});

Route::group(['prefix' => 'grafik-no-auth'], function() {
	Route::get('/skpd', 'GrafikNoAuthController@skpd');
	Route::get('/jabatan', 'GrafikNoAuthController@jabatan');
	Route::get('/golongan', 'GrafikNoAuthController@golongan');
	Route::get('/jenis-kelamin', 'GrafikNoAuthController@JenisKelamin');
	Route::get('/pendidikan', 'GrafikNoAuthController@pendidikan');
	Route::get('/usia', 'GrafikNoAuthController@usia');
	Route::get('/eselon-jk', 'GrafikNoAuthController@eselonJK');
	Route::get('get-data-jabatan/{id?}', 'GrafikNoAuthController@getDataJabatan');
	Route::get('get-data-golongan/{id?}', 'GrafikNoAuthController@getDataGolongan');
	Route::get('get-data-jenis-kelamin/{id?}', 'GrafikNoAuthController@getDataJenisKelamin');
	Route::get('get-data-pendidikan/{id?}', 'GrafikNoAuthController@getDataPendidikan');
	Route::get('get-data-usia/{id?}', 'GrafikNoAuthController@getDataUsia');
	Route::get('get-data-eselon-jk/{id?}', 'GrafikNoAuthController@getDataEselonJK');
});

Route::group(['prefix' => 'walikota'], function() {
	Route::get('/', 'PimpinanController@index');
	Route::get('/get-data', 'PimpinanController@getData');
	Route::get('/add', 'PimpinanController@add');
	Route::get('/edit/{peg_id}/{id}', 'PimpinanController@edit');
	Route::post('/store', 'PimpinanController@store');
	Route::post('/update/{peg_id}/{id}', 'PimpinanController@updates');
});

Route::group(['prefix' => 'pengajuan'], function() {
	Route::get('/create', 'PengajuanController@create');
	Route::post('/store', 'PengajuanController@store');

	Route::get('/pengajuan-saya', 'PengajuanController@pengajuanSaya');
	Route::get('pengajuan-saya/get-data', 'PengajuanController@getPengajuanSaya');

	Route::get('/edit/{id}', 'PengajuanController@edit');
	Route::patch('/update', 'PengajuanController@update');

	Route::get('batal/{id}', 'PengajuanController@batal');
	Route::post('ubah-status/{id}', 'PengajuanController@ubahStatus');

	Route::get('/daftar-pengajuan', 'PengajuanController@daftarPengajuan');
	Route::get('get-data', 'PengajuanController@getDataRender');
	Route::get('/detail/{id}', 'PengajuanController@detailPengajuan');
});

Route::get('daftar-hadir', 'HomeController@daftarHadir');

Route::group(['prefix' => 'api'], function()
{
	Route::any('auth', 'ApiController@authenticateDevice');
	
	Route::post('authenticate', 'ApiController@authenticate');
	Route::post('token', 'ApiController@getToken');
	Route::resource('kgb', 'Api\KGBController');
	Route::resource('usulan-kgb', 'Api\UsulanKGBController@usulan');
	Route::get('usulan-kgb/get_pdf/{usulan_id?}', 'Api\UsulanKgbController@getDocument');
	Route::resource('usulan-detail-kgb', 'Api\UsulanKGBController@usulanDetail');
	Route::get('usulan-detail-kgb/{usulan_id?}', 'Api\UsulanKGBController@usulanDetail');
	Route::resource('proses-kgb', 'Api\ProsesKgbController');
	Route::resource('dashboard-kgb', 'Api\DashboardKgbController');
	// Route::get('kgb/{year}/{month}', 'Api\KGBController@index');	

	Route::group(['prefix' => 'user'], function()
	{
		Route::any('test-gcm', 'ApiController@testGcm');
		Route::any('change-gcm', 'ApiController@changeGcm');
		Route::any('change-pin', 'ApiController@changePin');
		Route::any('change-password', 'ApiController@changePassword');
		Route::any('change-profile', 'ApiController@changeProfile');
		Route::any('deauthenticate', 'ApiController@deauthenticate');
		Route::any('profile', 'ApiController@profile');
	});
	Route::group(['prefix' => 'pegawai'], function()
	{
		Route::any('getData', 'ApiController@getData');
		Route::any('getBawahan', 'ApiController@getBawahan');
		Route::any('list/{page?}/{limit?}', 'ApiController@getPegawais');
		Route::any('detail/{peg_id}', 'ApiController@getPegawai');
	});
	Route::group(['prefix' => 'master'], function()
	{
		Route::any('skpd', 'ApiController@masterSkpd');
		Route::any('golongan', 'ApiController@masterGolongan');
		Route::any('eselon', 'ApiController@masterEselon');
		Route::any('pendidikan', 'ApiController@masterPendidikan');
	});
	Route::group(['prefix' => 'simpeg-adm/L439vMRX'], function()
	{
		Route::any('send-notification', 'ApiController@sendNotificationAdminSimpegAdm');
	});
});

Route::group(['prefix' => 'iframe'], function()
{
	Route::group(['prefix' => 'search'], function() {
		Route::get('/', 'Iframe\DaftarPegawaiController@index');
		Route::get('get-data', 'Iframe\DaftarPegawaiController@getDataRender');
		Route::get('export', 'Iframe\DaftarPegawaiController@exportData');
	});
});

Route::group(['prefix' => 'notifikasi'], function() {
	Route::get('prediksi', 'NotifController@prediksi');
	Route::get('get-prediksi', 'NotifController@getPrediksi');
	Route::get('lebih', 'NotifController@lebih');
	Route::get('get-lebih', 'NotifController@getLebih');
	Route::get('prediksipangkat', 'NotifController@prediksiPangkat');
	Route::get('get-prediksipangkat', 'NotifController@getPrediksiPangkat');
	Route::get('pegawai-struktural', 'SkpdController@pegawaiStruktural');
});

Route::get('ulang-tahun/harian', 'ExecutiveController@ulangTahunLocal');
Route::get('ulang-tahun/harian/{id}', 'ExecutiveController@ulangTahunUnit');
Route::get('ulang-tahun/bulanan', 'ExecutiveController@ulangTahunBulananLocal');
Route::get('ulang-tahun/bulanan/{id}', 'ExecutiveController@ulangTahunBulananUnit');
Route::get('penjagaan/kpo', 'KpoController@indexKpoPelaksana');
Route::get('penjagaan/kpo/{id}/{id2}', 'KpoController@indexKpoPelaksana');
Route::get('penjagaan/kpo-skpd', 'KpoController@indexKpoSkpd');

Route::get('kgb/rekomendasi-kgb/{id?}', 'RiwayatKgbController@index');
// Route::get('skp-kgb/{id}', 'RiwayatKgbController@skp');
Route::get('dms-kgb/{id}', 'RiwayatKgbController@dms');

Route::get('kgb/usulan-kgb/', 'UsulanKgbController@index');
Route::get('kgb/usulan-kgb/input/{usulan_id?}', 'UsulanKgbController@input');
Route::post('kgb/usulan-kgb/{status?}', 'UsulanKgbController@store');
Route::get('kgb/usulan-kgb-delete/{id?}', 'UsulanKgbController@destroy');
Route::post('kgb/usulan-kgb-detail/{usulan_id?}', 'UsulanKgbController@storeDetail');
Route::get('kgb/usulan-kgb-delete-detail/{id?}', 'UsulanKgbController@destroyDetail');

Route::get('kgb/inbox-kgb/', 'InboxKgbController@index');
Route::get('kgb/inbox-kgb-ubah-status/', 'InboxKgbController@changeStatusUsulan');
Route::get('kgb/inbox-kgb/detail/{usulan_id?}', 'InboxKgbController@detail');
Route::post('kgb/inbox-kgb', 'InboxKgbController@store');
Route::post('kgb/proses-kgb/', 'RiwayatKgbController@saveKgb');

Route::get('kgb/dashboard', 'Api\DashboardKgbController@dashboard');


Route::get('kgb/proses-kgb/get-data', 'ProsesKgbController@dataKgb');
Route::post('kgb/proses-kgb/verifikasi', 'ProsesKgbController@verifikasi');
Route::post('kgb/proses-kgb/cancel', 'ProsesKgbController@cancel');
Route::get('kgb/srt-kgb/{id}', ['as'=>'SuratKgb','uses'=>'ProsesKgbController@generateSrtKgb']);
Route::get('kgb/srt-kgb-multiple', ['as'=>'SuratKgbMultiple','uses'=>'ProsesKgbController@generateSrtKgbMultiple']);
Route::get('kgb/pdf-merger/{periode_awal?}/{periode_akhir?}/{satker?}/{uker?}/{gol?}/{ttd?}', ['as'=>'PdfMerger','uses'=>'ProsesKgbController@pdf_merger']);



Route::get('kgb/ttd-kgb', 'TTDKGBController@index');
Route::post('kgb/ttd-kgb', 'TTDKGBController@addDataTTDKGB');
Route::post('kgb/ttd-kgb/{id}', 'TTDKGBController@updateTTDKGB');
Route::get('kgb/delete-ttd-kgb/{id}', 'TTDKGBController@deleteTTDKGB');

Route::get('kgb/ttd-digital/', 'TtdDigitalKgbController@index');
Route::post('kgb/ttd-digital/', 'TtdDigitalKgbController@updateTtdDigital'); 

// laporan KGB
Route::get('kgb/laporan-kgb/{id?}', 'LaporanKgbController@index');
//Route::get('kgb/laporan-kgb-pdf/{id?}', 'LaporanKgbController@downloadPdf');
Route::get('kgb/laporan-kgb-excel/{type?}', 'LaporanKgbController@downloadExcel');
//Route::get('kgb/listing-kgb-pdf/{periode_awal?}/{periode_akhir?}', 'LaporanKgbController@getPdf');
Route::get('kgb/listing-kgb-pdf/{periode_awal?}/{periode_akhir?}/{satker?}/{uker?}/{gol?}/{ttd?}', 'LaporanKgbController@getPdf');

//Setting Template
Route::get('kgb/setting-template-srt', 'SettingTempSrtController@index');
Route::post('kgb/update-template-srt', 'SettingTempSrtController@update_template');
Route::get('kgb/get-data', 'SettingTempSrtController@getDataTemplateSrtKgb');
Route::get('kgb/download-srt-kgb/{id}', 'SettingTempSrtController@download');



Route::get('getSatuanOrganisasi', 'homeController@getSatuanOrganisasi');
Route::get('getUnitKerja', 'homeController@getUnitKerja');
Route::get('getPegawai', 'homeController@getPegawai');