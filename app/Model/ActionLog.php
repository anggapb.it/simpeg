<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class ActionLog extends Model
{
    protected $table = 'action_log';
    protected $primaryKey   = 'id';
    protected $fillable = [
        'username', 'keterangan', 'waktu','aksi','entity_id'
    ];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        
        static::creating(function($model) {
            $model->waktu = time();
        });
    }
}
