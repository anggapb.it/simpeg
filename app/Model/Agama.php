<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Agama extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_agama";
	protected $primaryKey="id_agama";
	public $timestamps = false;
}

?>