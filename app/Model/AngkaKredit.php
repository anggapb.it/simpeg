<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AngkaKredit extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_angka_kredit";
    protected $primaryKey="angka_kredit_id";
    public $timestamps = false;
}
