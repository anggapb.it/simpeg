<?php namespace App\Model;
use Auth;
use Request;
use DB;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
    use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $softDelete = true;

	public static function boot()
	{
		parent::boot();
		
		static::creating(function($model) {
			//$model->create_user = Auth::user()?Auth::user()->id:Request::ip();
			return true;
		});
		static::updating(function($model) {
			//$model->update_user = Auth::user()?Auth::user()->id:Request::ip();
			$tablename = static::getTableName();
			$pk = strtolower(static::getPrimaryKeyName());
			$original = [];
			$dirty = $model->getDirty();
			foreach($dirty as $attribute => $value){
				switch ($attribute) {
					default:
						$original[$attribute] = $model->getOriginal($attribute);
						break;
				}
			}
			DB::table('user_action_log')->insert([
				'user_id' => Auth::user()?Auth::user()->id:0,
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => $tablename,
				'model_id' => $model->$pk,
				'action' => 'update',
				'before' => json_encode($original),
				'after' => json_encode($dirty),
			]);
			return true;
		});
		
		static::created(function($model) {
			$pk = static::getPrimaryKeyName();
			DB::table('user_action_log')->insert([
				'user_id' => Auth::user()?Auth::user()->id:0,
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => static::getTableName(),
				'model_id' => $model->$pk,
				'action' => 'create',
			]);
		});
		
		static::deleting(function($model) {
			$pk = static::getPrimaryKeyName();
			DB::table('user_action_log')->insert([
				'user_id' => Auth::user()?Auth::user()->id:0,
				'ip_user' => Request::ip(),
				'time' => date("Y-m-d H:i:s"),
				'model' => static::getTableName(),
				'model_id' => $model->$pk,
				'action' => 'delete',
			]);
		});
	}
	
	public static function getTableName()
	{
		return with(new static)->getTable();
	}
	
	public static function getPrimaryKeyName()
	{
		return with(new static)->getKeyName();
	}
}