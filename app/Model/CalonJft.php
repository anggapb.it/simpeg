<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CalonJft extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_calon_jft";
    protected $primaryKey="peg_id";
    public $timestamps = false;
}
