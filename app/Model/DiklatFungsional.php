<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class DiklatFungsional extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_diklat_fungsional";
	protected $primaryKey="diklat_fungsional_id";
	public $timestamps = false;
}

?>