<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class DiklatStruktural extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_diklat_struk_kategori";
	protected $primaryKey="kategori_id";
	public $timestamps = false;
}



?>