<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class DiklatTeknis extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_diklat_teknis";
	protected $primaryKey="diklat_teknis_id";
	public $timestamps = false;
}

?>