<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Eselon extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_eselon";
	protected $primaryKey="eselon_id";
	public $timestamps = false;
}


?>