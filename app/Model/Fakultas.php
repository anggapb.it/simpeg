<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_fakultas";
	protected $primaryKey="fakultas_id";
	public $timestamps = false;
}

?>