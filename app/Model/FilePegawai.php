<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class FilePegawai extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_file_pegawai";
	protected $primaryKey="file_id";
	public $timestamps = false;
}


?>