<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class FilePegawai2 extends Model {
	protected $table="spg_file_pegawai";
	protected $primaryKey="file_id";
	public $timestamps = false;
}

?>