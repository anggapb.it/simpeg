<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Gaji extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_gaji";
	protected $primaryKey="gaji_id";
	public $timestamps = false;
}


?>