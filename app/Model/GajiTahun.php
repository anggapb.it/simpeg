<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class GajiTahun extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_gaji_tahun";
	protected $primaryKey="mgaji_id";
	public $timestamps = false;
}


?>