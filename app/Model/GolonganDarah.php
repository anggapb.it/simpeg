<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class GolonganDarah extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_golongan_darah";
	protected $primaryKey="id_goldar";
	public $timestamps = false;
}

?>