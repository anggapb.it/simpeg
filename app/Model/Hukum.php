<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Hukum extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_hukum";
	protected $primaryKey="mhukum_id";
	public $timestamps = false;
}

?>