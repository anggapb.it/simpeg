<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class HukumKategori extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_hukum_kategori";
	protected $primaryKey="mhukum_cat_id";
	public $timestamps = false;
}

?>