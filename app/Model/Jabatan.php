<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_jabatan";
	protected $primaryKey="jabatan_id";
	public $timestamps = false;
	
	public function jabatan_fungsional() {
		return $this->belongsTo('App\Model\JabatanFungsional','jf_id','jf_id');
	}
	
	public function jabatan_fungsional_umum() {
		return $this->belongsTo('App\Model\JabatanFungsionalUmum','jfu_id','jfu_id');
	}

	public function eselon(){
		return $this->belongsTo('App\Model\Eselon', 'eselon_id', 'eselon_id');
	}
	
	public function unit_kerja() {
		return $this->belongsTo('App\Model\UnitKerja','unit_kerja_id','unit_kerja_id');
	}

	public function satuan_kerja() {
		return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}
	
	public static function getOrCreateJabatan($satuan_kerja_id,$unit_kerja_id,$jfu_id,$jf_id) {
		$jab = Jabatan::where('satuan_kerja_id', $satuan_kerja_id)->where('unit_kerja_id', $unit_kerja_id)->where('jfu_id', $jfu_id)->where('jf_id', $jf_id)->first();
		if (!$jab) {
			$jab = new Jabatan();
			$jab->jabatan_jenis = $jfu_id?4:($jf_id?3:0);
			$jab->satuan_kerja_id = $satuan_kerja_id;
			$jab->unit_kerja_id = $unit_kerja_id;
			$jab->jfu_id = $jfu_id;
			$jab->jf_id = $jf_id;
			if($jab->save()){
				return $jab;
			}
			
		} else {
			return $jab;
		}
	}
}


?>