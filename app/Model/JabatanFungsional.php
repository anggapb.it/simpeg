<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JabatanFungsional extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_referensi_jf";
	protected $primaryKey="jf_id";
	public $timestamps = false;
}


?>