<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JabatanFungsionalUmum extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_referensi_jfu";
	protected $primaryKey="jfu_id";
	public $timestamps = false;
}


?>