<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JenisAsn extends Model {
	protected $connection = 'pgsql2';
	protected $table="jenis_asn";
	protected $primaryKey="id_jen_asn";
	public $timestamps = false;
}

?>