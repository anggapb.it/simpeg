<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JenisPns extends Model {
	protected $connection = 'pgsql2';
	protected $table="jenis_pns";
	protected $primaryKey="id_jen_pns";
	public $timestamps = false;
}

?>