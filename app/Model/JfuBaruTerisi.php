<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JfuBaruTerisi extends Model {
	protected $connection = 'pgsql2';
	protected $table="jabatan_baru_terisi";
	protected $primaryKey="jfu_nama";
	public $timestamps = false;
}


?>
