<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_jurusan";
	protected $primaryKey="jurusan_id";
	public $timestamps = false;
}

?>