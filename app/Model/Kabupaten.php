<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_kabupaten";
	protected $primaryKey="kabupaten_id";
	public $timestamps = false;

	public function kecamatan(){
        return $this->hasMany('Kecamatan');
	}

	public function propinsi(){
	    return $this->belongsTo('App\Model\Propinsi','propinsi_id','propinsi_id');
	}


}


?>