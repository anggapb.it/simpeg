<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class KategoriPendidikan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_kategori_pendidikan";
	protected $primaryKey="katpend_id";
	public $timestamps = false;


	public function tingkat_pendidikan() {
		return $this->belongsTo('App\Model\TingkatPendidikan','tingpend_id','tingpend_id');
	}

}



?>