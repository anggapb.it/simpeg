<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Keahlian extends Model {
	use SoftDeletes;
	protected $connection = 'pgsql2';
	protected $table="m_spg_keahlian";
	protected $primaryKey="keahlian_id";


	public function childKeahlian()
	{
	    return $this->hasMany('App\Model\Keahlian', 'spesifik_dari_keahlian_id', 'keahlian_id');
	}

	public function allChildKeahlian()
	{
	    return $this->childKeahlian()->with('allChildKeahlian');
	}

	public function getRules(){
        return array(
            'keahlian_nama'=>'unique:hari_libur,tgl,'.$this->id.',id|required',
        );
    }

    public static $rules=array(
        'keahlian_nama'=>'unique:keahlian_nama|required',
    );

    public static $message = array(
        'unique' => 'Nama ini sudah terdaftar sebelumnya',
        'required' => ':attribute wajib untuk diisi',
        'mimes'=>':attribute harus dalam format png,pdf,jpeg,jpg,doc,docx,jpeg-large,jpg-large',
        'size'=>':attribute maksimal 3 MB.',
        'max'=>':attribute melibihi panjang maksimum karakter.',
    );

}


?>