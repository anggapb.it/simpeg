<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KeahlianLevel extends Model {
	use SoftDeletes;

	protected $connection = 'pgsql2';
	protected $table="m_spg_keahlian_level";
	protected $primaryKey="keahlian_level_id";
}

?>