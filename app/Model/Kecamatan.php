<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_kecamatan";
	protected $primaryKey="kecamatan_id";
	public $timestamps = false;
	
	public function kabupaten(){
    	return $this->belongsTo('App\Model\Kabupaten','kabupaten_id','kabupaten_id');
	}
}



?>