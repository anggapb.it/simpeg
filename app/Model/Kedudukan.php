<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kedudukan extends Model
{
    protected $connection = 'pgsql2';
    protected $table="m_spg_kedudukan_pns";
    protected $primaryKey="id";
    public $timestamps = false;
}
