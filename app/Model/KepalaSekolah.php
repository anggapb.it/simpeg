<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KepalaSekolah extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_kepala_sekolah";
    protected $primaryKey="kepala_sekolah_id";
    public $timestamps = false;
}
