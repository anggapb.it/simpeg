<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kgb_skpd extends Model
{
    // protected $connection = 'pgsql2';
    protected $table = "spg_kgb";
    protected $primaryKey = 'kgb_id';
    // public $timestamps = false;
    protected $fillable = [
        'kgb_id','peg_id', 'gol_id', 'kgb_status', 'kgb_thn','kgb_nosk', 'kgb_bln', 'user_id', 'gaji_id', 'kgb_tmt', 'kgb_yad', 'kgb_tglsurat', 'kgb_nosurat',
        'kgb_kerja_tahun','kgb_kerja_bulan','kgb_status_peg','created_at', 'updated_at', 'usulan_id_detail', 'status_data', 'kgb_tmt'
    ];
}
