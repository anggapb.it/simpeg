<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ListSpgPegawai extends Model {
	protected $connection = 'pgsql2';
	protected $table="list_spg_pegawai";
	protected $primaryKey="added_at";
	public $timestamps = false;

	public function pegawaiSimpeg(){
		return $this->belongsTo('App\Model\Pegawai','peg_id','peg_id');
	}

	public function pegawaiErk(){
		return $this->belongsTo('App\Model\PegawaiErk','peg_id','peg_id');
	}
}

?>