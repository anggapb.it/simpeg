<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ListSpgPensiun extends Model {
	protected $connection = 'pgsql2';
	protected $table="list_spg_pensiun";
	protected $primaryKey="added_at";
	public $timestamps = false;

	public function pegawaiSimpeg(){
		return $this->belongsTo('App\Model\Pegawai','peg_id','peg_id');
	}

	public function pegawaiPensiun(){
		return $this->belongsTo('App\Model\PegawaiPensiun','peg_id','peg_id');
	}
}

?>
