<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class LogEditPegawai extends Model {
	protected $table="log_edit_pegawai";
	protected $primaryKey="id";
	public $timestamps = false;
}

?>