<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogKedudukan extends Model
{
    protected $connection = 'pgsql2';
    protected $table="log_kedhuk";
    protected $primaryKey="id";
    public $timestamps = false;
}
