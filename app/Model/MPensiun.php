<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MPensiun extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_pensiun";
	protected $primaryKey="pensiun_id";
	public $timestamps = false;
}

?>
