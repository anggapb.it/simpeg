<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MStatus extends Model {
	protected $table="m_status";
	protected $primaryKey="id";
	public $timestamps = false;
}

?>