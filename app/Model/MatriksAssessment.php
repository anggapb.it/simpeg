<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MatriksAssessment extends Model
{
    protected $connection = 'pgsql2';
    protected $table="m_spg_matriks_assessment";
    protected $primaryKey="matriks_id";
    public $timestamps = false;
}
