<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {
	protected $table="notifications";
	protected $primaryKey="id";
	public $timestamps = false;
	protected $casts = [
		'role_ids' => 'array'
	];
}

?>