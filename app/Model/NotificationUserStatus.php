<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationUserStatus extends Model
{
    protected $table="notifications_user_status";
    protected $primaryKey="id";
    public $timestamps = false;
}
