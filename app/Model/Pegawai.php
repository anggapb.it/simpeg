<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use DB;
use Excel;
use PHPExcel_Worksheet_PageSetup;

class Pegawai extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_pegawai_simpeg";
	protected $primaryKey="peg_id";
	public $timestamps = false;

	public function golongan_darah() {
		return $this->belongsTo('App\Model\GolonganDarah','id_goldar','id_goldar');
	}

	public function golongan_awal() {
		return $this->belongsTo('App\Model\Golongan','gol_id_awal','gol_id');
	}

	public function golongan_akhir() {
		return $this->belongsTo('App\Model\Golongan','gol_id_akhir','gol_id');
	}

	public function pendidikan_awal() {
		return $this->belongsTo('App\Model\Pendidikan','id_pend_awal','id_pend');
	}

	public function pendidikan_akhir() {
		return $this->belongsTo('App\Model\Pendidikan','id_pend_akhir','id_pend');
	}

	public function unit_kerja() {
		return $this->belongsTo('App\Model\UnitKerja','unit_kerja_id','unit_kerja_id');
	}

	public function kecamatan() {
		return $this->belongsTo('App\Model\Kecamatan','kecamatan_id','kecamatan_id');
	}

	public function jabatan() {
		return $this->belongsTo('App\Model\Jabatan','jabatan_id');
	}

	public function agama() {
		return $this->belongsTo('App\Model\Agama','id_agama','id_agama');
	}

	public function satuan_kerja() {
		return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}

	public function status_edit() {
		return $this->belongsTo('App\Model\StatusEditPegawai','peg_id','peg_id');
	}

	public function riwayat_diklat() {
		return $this->hasMany('App\Model\RiwayatDiklat','peg_id','peg_id');
	}

	public function riwayat_jabatan() {
		return $this->hasMany('App\Model\RiwayatJabatan','peg_id','peg_id');
	}

	public function riwayat_keluarga() {
		return $this->hasMany('App\Model\RiwayatKeluarga','peg_id','peg_id');
	}

	public function riwayat_non_formal() {
		return $this->hasMany('App\Model\RiwayatNonFormal','peg_id','peg_id');
	}

	public function riwayat_pangkat() {
		return $this->hasMany('App\Model\RiwayatPangkat','peg_id','peg_id');
	}

	public function riwayat_pendidikan() {
		return $this->hasMany('App\Model\RiwayatPendidikan','peg_id','peg_id');
	}

	public function riwayat_penghargaan() {
		return $this->hasMany('App\Model\RiwayatPenghargaan','peg_id','peg_id');
	}

	public function status_kepegawaian() {
		return $this->belongsTo('App\Model\StatusKepegawaian','id_status_kepegawaian','id_status_kepegawaian');
	}

	public function jenis_asn() {
		return $this->belongsTo('App\Model\JenisAsn','peg_jenis_asn','id_jen_asn');
	}

	public function status_calon() {
		return $this->belongsTo('App\Model\StatusCalon','peg_status_calon','id_stat_calon');
	}

	public function jenis_pns() {
		return $this->belongsTo('App\Model\JenisPns','peg_jenis_pns','id_jen_pns');
	}

	public function kedudukan() {
		return $this->belongsTo('App\Model\Kedudukan','kedudukan_pegawai','id');
	}

	public function statustkd() {
		return $this->belongsTo('App\Model\StatusTKD','status_tkd','id');
	}

    public function getPhotoUrl()
    {
        return "https://simpeg.bandung.go.id/uploads/".$this->peg_foto;
    }

    public function getPhotoUrlResize()
    {
        return "https://simpeg.bandung.go.id/uploads-resize/".$this->peg_foto;
    }

	public static function getRekapJabatan($satker){
		if (empty($satker)) {
			$unit = UnitKerja::lists('unit_kerja_id');
			$q_satker = function($q) {

			};
		} else {
			$unit = UnitKerja::where('satuan_kerja_id', $satker)->lists('unit_kerja_id');
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}

		$js = Jabatan::where('jabatan_jenis',2)->where($q_satker)->lists('jabatan_id');
		$jf = Jabatan::where('jabatan_jenis', 3)->where($q_satker)->lists('jabatan_id');
		$jfu = Jabatan::where('jabatan_jenis', 4)->where($q_satker)->lists('jabatan_id');

		$s_2a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',4)->lists('jabatan_id');
		$s_2b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',5)->lists('jabatan_id');
		$s_3a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',6)->lists('jabatan_id');
		$s_3b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',7)->lists('jabatan_id');
		$s_4a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',8)->lists('jabatan_id');
		$s_4b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',9)->lists('jabatan_id');
		$s_5a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',27)->lists('jabatan_id');

		$s = $js;
		$f = $jf;
		$fu = $jfu;
		$s2a = $s_2a;
		$s2b = $s_2b;
		$s3a = $s_3a;
		$s3b = $s_3b;
		$s4a = $s_4a;
		$s4b = $s_4b;
		$s5a = $s_5a;

		$js_2a = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->count();
		$js_2b = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->count();
		$js_3a = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->count();
		$js_3b = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->count();
		$js_4a = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->count();
		$js_4b = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->count();
		$js_5a = Pegawai::where($q_satker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->count();
		$ps = Pegawai::where($q_satker)->whereIn('jabatan_id', $s)->where('peg_status', true)->count();
		$pf = Pegawai::where($q_satker)->whereIn('jabatan_id', $f)->where('peg_status', true)->count();
		$pfu = Pegawai::where($q_satker)->whereIn('jabatan_id', $fu)->where('peg_status', true)->count();
		$total = Pegawai::where($q_satker)->where('peg_status', true)->count();

		$data= [
			'eselon_2a' => $js_2a,
			'eselon_2b' => $js_2b,
			'eselon_3a' => $js_3a,
			'eselon_3b' => $js_3b,
			'eselon_4a' => $js_4a,
			'eselon_4b' => $js_4b,
			'eselon_5a' => $js_5a,
			'total_s' => $ps,
			'total_f' => $pf,
			'total_fu' => $pfu,
			'total_uk' => $total - $pfu - $pf - $ps,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapJabatanSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$js = Jabatan::where('jabatan_jenis',2)->where($q_satker)->lists('jabatan_id');
		$jf = Jabatan::where('jabatan_jenis', 3)->where($q_satker)->lists('jabatan_id');
		$jfu = Jabatan::where('jabatan_jenis', 4)->where($q_satker)->lists('jabatan_id');

		$s_2a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',4)->lists('jabatan_id');
		$s_2b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',5)->lists('jabatan_id');
		$s_3a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',6)->lists('jabatan_id');
		$s_3b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',7)->lists('jabatan_id');
		$s_4a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',8)->lists('jabatan_id');
		$s_4b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',9)->lists('jabatan_id');
		$s_5a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',27)->lists('jabatan_id');

		$s = $js;
		$f = $jf;
		$fu = $jfu;
		$s2a = $s_2a;
		$s2b = $s_2b;
		$s3a = $s_3a;
		$s3b = $s_3b;
		$s4a = $s_4a;
		$s4b = $s_4b;
		$s5a = $s_5a;

		$js_2a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->count();
		$js_2b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->count();
		$js_3a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->count();
		$js_3b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->count();
		$js_4a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->count();
		$js_4b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->count();
		$js_5a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->count();
		$ps = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s)->where('peg_status', true)->count();
		$pf = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $f)->where('peg_status', true)->count();
		$pfu = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $fu)->where('peg_status', true)->count();
		$total = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_status', true)->count();

		$data= [
			'eselon_2a' => $js_2a,
			'eselon_2b' => $js_2b,
			'eselon_3a' => $js_3a,
			'eselon_3b' => $js_3b,
			'eselon_4a' => $js_4a,
			'eselon_4b' => $js_4b,
			'eselon_5a' => $js_5a,
			'total_s' => $ps,
			'total_f' => $pf,
			'total_fu' => $pfu,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapGolongan($satker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		$gol_1a = Pegawai::where($q_satker)->where('gol_id_akhir', 1)->where('peg_status', true)->count();
		$gol_1b = Pegawai::where($q_satker)->where('gol_id_akhir', 2)->where('peg_status', true)->count();
		$gol_1c = Pegawai::where($q_satker)->where('gol_id_akhir', 3)->where('peg_status', true)->count();
		$gol_1d = Pegawai::where($q_satker)->where('gol_id_akhir', 4)->where('peg_status', true)->count();
		$gol_2a = Pegawai::where($q_satker)->where('gol_id_akhir', 5)->where('peg_status', true)->count();
		$gol_2b = Pegawai::where($q_satker)->where('gol_id_akhir', 6)->where('peg_status', true)->count();
		$gol_2c = Pegawai::where($q_satker)->where('gol_id_akhir', 7)->where('peg_status', true)->count();
		$gol_2d = Pegawai::where($q_satker)->where('gol_id_akhir', 8)->where('peg_status', true)->count();
		$gol_3a = Pegawai::where($q_satker)->where('gol_id_akhir', 9)->where('peg_status', true)->count();
		$gol_3b = Pegawai::where($q_satker)->where('gol_id_akhir', 10)->where('peg_status', true)->count();
		$gol_3c = Pegawai::where($q_satker)->where('gol_id_akhir', 11)->where('peg_status', true)->count();
		$gol_3d = Pegawai::where($q_satker)->where('gol_id_akhir', 12)->where('peg_status', true)->count();
		$gol_4a = Pegawai::where($q_satker)->where('gol_id_akhir', 13)->where('peg_status', true)->count();
		$gol_4b = Pegawai::where($q_satker)->where('gol_id_akhir', 14)->where('peg_status', true)->count();
		$gol_4c = Pegawai::where($q_satker)->where('gol_id_akhir', 15)->where('peg_status', true)->count();
		$gol_4d = Pegawai::where($q_satker)->where('gol_id_akhir', 16)->where('peg_status', true)->count();
		$gol_4e = Pegawai::where($q_satker)->where('gol_id_akhir', 17)->where('peg_status', true)->count();

		$total = Pegawai::where($q_satker)->where('peg_status', true)->count();

		$data= [
			'1a' => $gol_1a,
			'1b' => $gol_1b,
			'1c' => $gol_1c,
			'1d' => $gol_1d,
			'2a' => $gol_2a,
			'2b' => $gol_2b,
			'2c' => $gol_2c,
			'2d' => $gol_2d,
			'3a' => $gol_3a,
			'3b' => $gol_3b,
			'3c' => $gol_3c,
			'3d' => $gol_3d,
			'4a' => $gol_4a,
			'4b' => $gol_4b,
			'4c' => $gol_4c,
			'4d' => $gol_4d,
			'4e' => $gol_4d,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapGolonganSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$gol_1a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 1)->where('peg_status', true)->count();
		$gol_1b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 2)->where('peg_status', true)->count();
		$gol_1c = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 3)->where('peg_status', true)->count();
		$gol_1d = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 4)->where('peg_status', true)->count();
		$gol_2a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 5)->where('peg_status', true)->count();
		$gol_2b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 6)->where('peg_status', true)->count();
		$gol_2c = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 7)->where('peg_status', true)->count();
		$gol_2d = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 8)->where('peg_status', true)->count();
		$gol_3a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 9)->where('peg_status', true)->count();
		$gol_3b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 10)->where('peg_status', true)->count();
		$gol_3c = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 11)->where('peg_status', true)->count();
		$gol_3d = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 12)->where('peg_status', true)->count();
		$gol_4a = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 13)->where('peg_status', true)->count();
		$gol_4b = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 14)->where('peg_status', true)->count();
		$gol_4c = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 15)->where('peg_status', true)->count();
		$gol_4d = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 16)->where('peg_status', true)->count();
		$gol_4e = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('gol_id_akhir', 17)->where('peg_status', true)->count();

		$total = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_status', true)->count();

		$data= [
			'1a' => $gol_1a,
			'1b' => $gol_1b,
			'1c' => $gol_1c,
			'1d' => $gol_1d,
			'2a' => $gol_2a,
			'2b' => $gol_2b,
			'2c' => $gol_2c,
			'2d' => $gol_2d,
			'3a' => $gol_3a,
			'3b' => $gol_3b,
			'3c' => $gol_3c,
			'3d' => $gol_3d,
			'4a' => $gol_4a,
			'4b' => $gol_4b,
			'4c' => $gol_4c,
			'4d' => $gol_4d,
			'4e' => $gol_4d,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapJenisKelamin($satker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		$jenis_l = Pegawai::where($q_satker)->where('peg_jenis_kelamin', 'L')->where('peg_status', true)->count();
		$jenis_p = Pegawai::where($q_satker)->where('peg_jenis_kelamin', 'P')->where('peg_status', true)->count();

		$total = Pegawai::where($q_satker)->where('peg_status', true)->count();

		$data= [
			'l' => $jenis_l,
			'p' => $jenis_p,
			'b' => $total - ($jenis_l+$jenis_p),
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapJenisKelaminSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$jenis_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_jenis_kelamin', 'L')->where('peg_status', true)->count();
		$jenis_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_jenis_kelamin', 'P')->where('peg_status', true)->count();

		$total = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_status', true)->count();
		$data= [
			'l' => $jenis_l,
			'p' => $jenis_p,
			'b' => $total - ($jenis_l+$jenis_p),
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapPendidikan($satker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		$pend_sd = Pendidikan::where('kat_pend_id', 1)->get();
		$pend_smp = Pendidikan::whereIn('kat_pend_id', [5,6])->get();
		$pend_sma = Pendidikan::whereIn('kat_pend_id', [3,4])->get();
		$pend_d1 = Pendidikan::where('kat_pend_id', 14)->get();
		$pend_d2 = Pendidikan::where('kat_pend_id', 7)->get();
		$pend_d3 = Pendidikan::where('kat_pend_id', 8)->get();
		$pend_d4 = Pendidikan::where('kat_pend_id', 9)->get();
		$pend_s1 = Pendidikan::whereIn('kat_pend_id', [10,11,15])->get();
		$pend_s2 = Pendidikan::where('kat_pend_id', 12)->get();
		$pend_s3 = Pendidikan::where('kat_pend_id', 13)->get();

		$sd = array();
		$smp = array();
		$sma = array();
		$d1 = array();
		$d2 = array();
		$d3 = array();
		$d4 = array();
		$s1 = array();
		$s2 = array();
		$s3 = array();

		foreach ($pend_sd as $key => $value) {
			$sd[] = $value->id_pend;
		}

		foreach ($pend_smp as $key => $value) {
			$smp[] = $value->id_pend;
		}

		foreach ($pend_sma as $key => $value) {
			$sma[] = $value->id_pend;
		}

		foreach ($pend_d1 as $key => $value) {
			$d1[] = $value->id_pend;
		}

		foreach ($pend_d2 as $key => $value) {
			$d2[] = $value->id_pend;
		}

		foreach ($pend_d3 as $key => $value) {
			$d3[] = $value->id_pend;
		}

		foreach ($pend_d4 as $key => $value) {
			$d4[] = $value->id_pend;
		}

		foreach ($pend_s1 as $key => $value) {
			$s1[] = $value->id_pend;
		}

		foreach ($pend_s2 as $key => $value) {
			$s2[] = $value->id_pend;
		}

		foreach ($pend_s3 as $key => $value) {
			$s3[] = $value->id_pend;
		}


		$jenis_sd = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $sd)->where('peg_status', true)->count();
		$jenis_smp = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $smp)->where('peg_status', true)->count();
		$jenis_sma = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $sma)->where('peg_status', true)->count();
		$jenis_d1 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $d1)->where('peg_status', true)->count();
		$jenis_d2 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $d2)->where('peg_status', true)->count();
		$jenis_d3 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $d3)->where('peg_status', true)->count();
		$jenis_d4 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $d4)->where('peg_status', true)->count();
		$jenis_s1 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $s1)->where('peg_status', true)->count();
		$jenis_s2 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $s2)->where('peg_status', true)->count();
		$jenis_s3 = Pegawai::where($q_satker)->whereIn('id_pend_akhir', $s3)->where('peg_status', true)->count();

		$total = $jenis_sd+$jenis_smp+$jenis_sma+$jenis_d1+$jenis_d2+$jenis_d3+$jenis_d4+$jenis_s1+$jenis_s2+$jenis_s3;

		$data= [
			'sd' => $jenis_sd,
			'smp' => $jenis_smp,
			'sma' => $jenis_sma,
			'd1' => $jenis_d1,
			'd2' => $jenis_d2,
			'd3' => $jenis_d3,
			'd4' => $jenis_d4,
			's1' => $jenis_s1,
			's2' => $jenis_s2,
			's3' => $jenis_s3,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapPendidikanSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$pend_sd = Pendidikan::where('kat_pend_id', 1)->get();
		$pend_smp = Pendidikan::whereIn('kat_pend_id', [5,6])->get();
		$pend_sma = Pendidikan::whereIn('kat_pend_id', [3,4])->get();
		$pend_d1 = Pendidikan::where('kat_pend_id', 14)->get();
		$pend_d2 = Pendidikan::where('kat_pend_id', 7)->get();
		$pend_d3 = Pendidikan::where('kat_pend_id', 8)->get();
		$pend_d4 = Pendidikan::where('kat_pend_id', 9)->get();
		$pend_s1 = Pendidikan::whereIn('kat_pend_id', [10,11,15])->get();
		$pend_s2 = Pendidikan::where('kat_pend_id', 12)->get();
		$pend_s3 = Pendidikan::where('kat_pend_id', 13)->get();

		$sd = array();
		$smp = array();
		$sma = array();
		$d1 = array();
		$d2 = array();
		$d3 = array();
		$d4 = array();
		$s1 = array();
		$s2 = array();
		$s3 = array();

		foreach ($pend_sd as $key => $value) {
			$sd[] = $value->id_pend;
		}

		foreach ($pend_smp as $key => $value) {
			$smp[] = $value->id_pend;
		}

		foreach ($pend_sma as $key => $value) {
			$sma[] = $value->id_pend;
		}

		foreach ($pend_d1 as $key => $value) {
			$d1[] = $value->id_pend;
		}

		foreach ($pend_d2 as $key => $value) {
			$d2[] = $value->id_pend;
		}

		foreach ($pend_d3 as $key => $value) {
			$d3[] = $value->id_pend;
		}

		foreach ($pend_d4 as $key => $value) {
			$d4[] = $value->id_pend;
		}

		foreach ($pend_s1 as $key => $value) {
			$s1[] = $value->id_pend;
		}

		foreach ($pend_s2 as $key => $value) {
			$s2[] = $value->id_pend;
		}

		foreach ($pend_s3 as $key => $value) {
			$s3[] = $value->id_pend;
		}


		$jenis_sd = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $sd)->where('peg_status', true)->count();
		$jenis_smp = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $smp)->where('peg_status', true)->count();
		$jenis_sma = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $sma)->where('peg_status', true)->count();
		$jenis_d1 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $d1)->where('peg_status', true)->count();
		$jenis_d2 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $d2)->where('peg_status', true)->count();
		$jenis_d3 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $d3)->where('peg_status', true)->count();
		$jenis_d4 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $d4)->where('peg_status', true)->count();
		$jenis_s1 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $s1)->where('peg_status', true)->count();
		$jenis_s2 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $s2)->where('peg_status', true)->count();
		$jenis_s3 = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('id_pend_akhir', $s3)->where('peg_status', true)->count();

		$total = $jenis_sd+$jenis_smp+$jenis_sma+$jenis_d1+$jenis_d2+$jenis_d3+$jenis_d4+$jenis_s1+$jenis_s2+$jenis_s3;

		$data= [
			'sd' => $jenis_sd,
			'smp' => $jenis_smp,
			'sma' => $jenis_sma,
			'd1' => $jenis_d1,
			'd2' => $jenis_d2,
			'd3' => $jenis_d3,
			'd4' => $jenis_d4,
			's1' => $jenis_s1,
			's2' => $jenis_s2,
			's3' => $jenis_s3,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapUsia($satker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		$usia = Pegawai::select(DB::raw("date_part('year',age(peg_lahir_tanggal)) as usia, count(*) as total"))
                     ->where('peg_status', true)
                     ->where($q_satker)
                     ->groupBy('usia')
                     ->get();
        $kel_1=0;
        $kel_2=0;
        $kel_3=0;
        $kel_4=0;
        $kel_5=0;
        $kel_6=0;
        $kel_7=0;

		foreach ($usia as $key => $value) {
			if($value->usia >= 16 && $value->usia <= 25){
				$kel_1 += $value->total;
			}elseif($value->usia >= 26 && $value->usia <= 35){
				$kel_2 += $value->total;
			}elseif($value->usia >= 36 && $value->usia <= 45){
				$kel_3 += $value->total;
			}elseif($value->usia >= 46 && $value->usia <= 55){
				$kel_4 += $value->total;
			}elseif($value->usia >= 56 && $value->usia <= 65){
				$kel_5 += $value->total;
			}elseif($value->usia > 65){
				$kel_6 += $value->total;
			}else{
				$kel_7 += $value->total;
			}
		}
		$total = Pegawai::where($q_satker)->where('peg_status', true)->count();

		$data= [
			'1' => $kel_1,
			'2' => $kel_2,
			'3' => $kel_3,
			'4' => $kel_4,
			'5' => $kel_5,
			'6' => $kel_6,
			'7' => $kel_7,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapUsiaSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$usia = DB::table('spg_pegawai')
                     ->select(DB::raw("date_part('year',age(peg_lahir_tanggal)) as usia"))
                     ->where('peg_status', true)
                     ->where($q_satker)
                     ->whereIn('unit_kerja_id', $list_uker)
                     ->get();
        $kel_1=0;
        $kel_2=0;
        $kel_3=0;
        $kel_4=0;
        $kel_5=0;
        $kel_6=0;
        $kel_7=0;

		foreach ($usia as $key => $value) {
			if($value->usia >= 16 && $value->usia <= 25){
				$kel_1++;
			}elseif($value->usia >= 26 && $value->usia <= 35){
				$kel_2++;
			}elseif($value->usia >= 36 && $value->usia <= 45){
				$kel_3++;
			}elseif($value->usia >= 46 && $value->usia <= 55){
				$kel_4++;
			}elseif($value->usia >= 56 && $value->usia <= 65){
				$kel_5++;
			}elseif($value->usia > 65){
				$kel_6++;
			}else{
				$kel_7++;
			}
		}
		$total = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_status', true)->count();

		$data= [
			'1' => $kel_1,
			'2' => $kel_2,
			'3' => $kel_3,
			'4' => $kel_4,
			'5' => $kel_5,
			'6' => $kel_6,
			'7' => $kel_7,
			'total' => $total,
		];

		return $data;
	}

	public static function exportAllPegawai(){
		date_default_timezone_set('Asia/Jakarta');
        $all = false;
        $data = array();
        $header = array();
		$models = Pegawai::with(['satuan_kerja','status_edit'])->where('peg_status', 'true')->where('satuan_kerja_id','<>',999999)->get();

		foreach ($models as $i=>$a) {
            $golongan = Golongan::where('gol_id', $a->gol_id_akhir)->first();
            $jabatan = Jabatan::where('jabatan_id', $a->jabatan_id)->first();
            if($a->peg_status_kepegawaian == '1'){
                $status = 'PNS';
                $tmt =  transformDate($a->peg_pns_tmt);
            }else if($a->peg_status_kepegawaian == '2'){
                $status = 'CPNS';
                $tmt = transformDate($a->peg_cpns_tmt);
            }else{
                $status = '-';
            }

            if($a->peg_gelar_belakang != null){
                $nama = $a->peg_gelar_depan.$a->peg_nama.','.$a->peg_gelar_belakang;
            }else{
                $nama= $a->peg_gelar_depan.$a->peg_nama;
            }

            $tempat = str_replace(' ', '', $a->peg_lahir_tempat);
            if($jabatan['jabatan_jenis'] == 2){
                $jab_jenis = $a['jabatan']['eselon']['eselon_nm'];
                $nama_jabatan = $jabatan->jabatan_nama;
            }elseif($jabatan['jabatan_jenis'] == 3){
                $jab_jenis = 'Fungsional Tertentu';
                $jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
                $nama_jabatan = $jf->jf_nama;
            }elseif($jabatan['jabatan_jenis'] == 4){
                $jab_jenis ='Fungsional Umum';
                $jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
                if($jfu){
                    $nama_jabatan = $jfu['jfu_nama'];
                }else{
                    $nama_jabatan = 'Pelaksana';
                }
            }else{
                $jab_jenis = '';
                $nama_jabatan = '';
            }

            if($a->peg_jenis_kelamin == 'L'){
                $jenis_kelamin = 'Laki-laki';
            }else if($a->peg_jenis_kelamin == 'P'){
                $jenis_kelamin = 'Perempuan';
            }else{
                $jenis_kelamin='-';
            }

            if($a->peg_status_perkawinan == '1'){
                $peg_status_perkawinan = 'Kawin';
            }elseif($a->peg_status_perkawinan == '2'){
                $peg_status_perkawinan = 'Belum Kawin';
            }elseif($a->peg_status_perkawinan == '3'){
                $peg_status_perkawinan = 'Janda';
            }elseif($a->peg_status_perkawinan == '4'){
                $peg_status_perkawinan = 'Duda';
            }else{
                $peg_status_perkawinan = '-';
            }

            if($a['unit_kerja']['unit_kerja_level'] == 1){
                $uker = $a->unit_kerja->unit_kerja_nama;
            }else if($a['unit_kerja']['unit_kerja_level'] == 2){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                // $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'];
                $uker = $parent['unit_kerja_nama'].' - '.$a->unit_kerja->unit_kerja_nama;
            }else if($a['unit_kerja']['unit_kerja_level'] == 3){
                $parent = UnitKerja::where('unit_kerja_id', $a->unit_kerja->unit_kerja_parent)->first();
                $grandparent = UnitKerja::where('unit_kerja_id', $parent['unit_kerja_parent'])->first();
                // $uker = $a->unit_kerja->unit_kerja_nama.', '.$parent['unit_kerja_nama'].', '.$grandparent['unit_kerja_nama'];
                $uker = $grandparent['unit_kerja_nama'].' - '.$parent['unit_kerja_nama'].' - '.$a->unit_kerja->unit_kerja_nama;
            }else{
                $uker = '';
            }
            $kedudukan_pegawai=StatusKepegawaian::where('id_status_kepegawaian',$a->id_status_kepegawaian)->pluck('status');
            $d = [
                $i+1,
                $nama,
                $tempat.','.transformDate($a->peg_lahir_tanggal),
                $a->peg_nip,
                $a['satuan_kerja']['satuan_kerja_nama'],
                $uker,
                $golongan['nm_gol'],
                transformDate($a->peg_gol_akhir_tmt),
                $jab_jenis,
                $nama_jabatan,
                transformDate($a->peg_jabatan_tmt),
                $status,
                $tmt,
                $a->peg_kerja_tahun,
                $a->peg_kerja_bulan,
                $jenis_kelamin,
                $a['agama']['nm_agama'],
                $peg_status_perkawinan,
                $a['pendidikan_awal']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_awal']['nm_pend'],
                $a['pendidikan_akhir']['kategori_pendidikan']['tingkat_pendidikan']['nm_tingpend'].' - '.$a['pendidikan_akhir']['nm_pend'],
                $a->peg_no_askes,
                $a->peg_npwp,
                $a->peg_ktp,
                $a->peg_rumah_alamat.' '. $a->peg_kel_desa.' '. $a['kecamatan']['kecamatan_nm'],
                $a->peg_telp,
                $a->peg_telp_hp,
                $a->peg_email,
                $kedudukan_pegawai,
            ];

            $h= [
                    'No',
                    'Nama',
                    'Tempat Tanggal Lahir',
                    'NIP',
                    'Satuan Kerja',
                    'Unit Kerja',
                    'Golongan Pangkat',
                    'TMT Golongan',
                    'Eselon',
                    'Nama Jabatan',
                    'TMT Jabatan',
                    'Status Pegawai',
                    'TMT Pegawai',
                    'Masa Kerja Tahun',
                    'Masa Kerja Bulan',
                    'Jenis Kelamin',
                    'Agama',
                    'Status Perkawinan',
                    'Pendidikan Awal',
                    'Pendidikan Akhir',
                    'No Askes',
                    'No NPWP',
                    'No KTP',
                    'Alamat Rumah',
                    'Telp',
                    'HP',
                    'E-mail',
                    'Kedudukan Pegawai',
                ];

            $data[] = $d;
            $header = $h;
        }

        Excel::create('DATA SELURUH PEGAWAI KOTA BANDUNG', function($excel) use ($data, $header) {
            $excel->sheet('DAFTAR PEGAWAI', function($sheet) use ($data, $header) {
                $sheet->setFontFamily('Calibri');
                $sheet->setFontSize(12);
                $sheet->setAutoSize(true);

                $sheet->mergeCells('A2:AB2');
                $sheet->cell('A2', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->row(2, ['DAFTAR SELURUH PEGAWAI KOTA BANDUNG']);

                $sheet->cells('A4:AB4', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#b7dee8');
                });
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                    'G' => '@',
                    'H' => '@',
                    'I' => '@',
                    'J' => '@',
                    'K' => '@',
                    'L' => '@',
                    'M' => '@',
                    'N' => '@',
                    'O' => '@',
                    'P' => '@',
                    'Q' => '@',
                    'R' => '@',
                    'S' => '@',
                    'T' => '@',
                    'U' => '@',
                    'V' => '@',
                    'W' => '@',
                    'X' => '@',
                    'Y' => '@',
                    'Z' => '@',
                    'AA' => '@',
                ));
                // $sheet->setAutoFilter('A4:AB4');
                $sheet->row(4, $header);
                $sheet->setBorder('A4:AB4', 'thin');
                $sheet->fromArray($data, "", "A5", true, false);
                for ($j = 5; $j < count($data) + 5; $j++) {
                    $sheet->setBorder('A'.$j.':AB'.$j, 'thin');
                }

                $sheet->setPageMargin(array(0.25, 0.25, 0.25, 0.25));
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        })->store('xlsx');

	}

	public static function getRekapEselonJK($satker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		$uker = UnitKerja::where($q_satker)->get();
		$unit = array();

		foreach ($unit as $key => $value) {
			$unit[] = $value->unit_kerja_id;
		}

		$js = Jabatan::where('jabatan_jenis',2)->where($q_satker)->lists('jabatan_id');
		$jf = Jabatan::where('jabatan_jenis', 3)->where($q_satker)->lists('jabatan_id');
		$jfu = Jabatan::where('jabatan_jenis', 4)->where($q_satker)->lists('jabatan_id');

		$s_2a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',4)->lists('jabatan_id');
		$s_2b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',5)->lists('jabatan_id');
		$s_3a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',6)->lists('jabatan_id');
		$s_3b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',7)->lists('jabatan_id');
		$s_4a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',8)->lists('jabatan_id');
		$s_4b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',9)->lists('jabatan_id');
		$s_5a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',27)->lists('jabatan_id');

		$s = $js;
		$f = $jf;
		$fu = $jfu;
		$s2a = $s_2a;
		$s2b = $s_2b;
		$s3a = $s_3a;
		$s3b = $s_3b;
		$s4a = $s_4a;
		$s4b = $s_4b;
		$s5a = $s_5a;

		$js_2a_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_2a_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_2b_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_2b_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_3a_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_3a_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_3b_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_3b_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_4a_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_4a_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_4b_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_4b_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_5a_l = Pegawai::where($q_satker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_5a_p = Pegawai::where($q_satker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$ps = Pegawai::where($q_satker)->whereIn('jabatan_id', $s)->where('peg_status', true)->count();
		$pf = Pegawai::where($q_satker)->whereIn('jabatan_id', $f)->where('peg_status', true)->count();
		$pfu = Pegawai::where($q_satker)->whereIn('jabatan_id', $fu)->where('peg_status', true)->count();
		$total = Pegawai::where($q_satker)->where('peg_status', true)->count();

		$data= [
			'eselon_2a_l' => $js_2a_l,
			'eselon_2a_p' => $js_2a_p,
			'eselon_2b_l' => $js_2b_l,
			'eselon_2b_p' => $js_2b_p,
			'eselon_3a_l' => $js_3a_l,
			'eselon_3a_p' => $js_3a_p,
			'eselon_3b_l' => $js_3b_l,
			'eselon_3b_p' => $js_3b_p,
			'eselon_4a_l' => $js_4a_l,
			'eselon_4a_p' => $js_4a_p,
			'eselon_4b_l' => $js_4b_l,
			'eselon_4b_p' => $js_4b_p,
			'eselon_5a_l' => $js_5a_l,
			'eselon_5a_p' => $js_5a_p,
			'total_s' => $ps,
			'total' => $total,
		];

		return $data;
	}

	public static function getRekapEselonJKSetda($satker, $uker){
		if (empty($satker)) {
			$q_satker = function($q) {

			};
		} else {
			$q_satker = function($q) use ($satker) {
				$q->where('satuan_kerja_id', $satker);
			};
		}
		if (in_array($uker, [2,3,4])) {
			$list_uker = [$uker];
		} else {
			$list_uker = getAllUnitKerjaChildren($uker);
		}
		$js = Jabatan::where('jabatan_jenis',2)->where($q_satker)->lists('jabatan_id');
		$jf = Jabatan::where('jabatan_jenis', 3)->where($q_satker)->lists('jabatan_id');
		$jfu = Jabatan::where('jabatan_jenis', 4)->where($q_satker)->lists('jabatan_id');

		$s_2a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',4)->lists('jabatan_id');
		$s_2b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',5)->lists('jabatan_id');
		$s_3a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',6)->lists('jabatan_id');
		$s_3b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',7)->lists('jabatan_id');
		$s_4a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',8)->lists('jabatan_id');
		$s_4b = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',9)->lists('jabatan_id');
		$s_5a = Jabatan::where('jabatan_jenis',2)->where($q_satker)->where('eselon_id',27)->lists('jabatan_id');

		$s = $js;
		$f = $jf;
		$fu = $jfu;
		$s2a = $s_2a;
		$s2b = $s_2b;
		$s3a = $s_3a;
		$s3b = $s_3b;
		$s4a = $s_4a;
		$s4b = $s_4b;
		$s5a = $s_5a;

		$js_2a_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_2a_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_2b_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_2b_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s2b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_3a_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_3a_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_3b_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_3b_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s3b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_4a_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_4a_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_4b_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_4b_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s4b)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$js_5a_l = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->where('peg_jenis_kelamin','L')->count();
		$js_5a_p = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s5a)->where('peg_status', true)->where('peg_jenis_kelamin','P')->count();
		$ps = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $s)->where('peg_status', true)->count();
		$pf = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $f)->where('peg_status', true)->count();
		$pfu = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->whereIn('jabatan_id', $fu)->where('peg_status', true)->count();
		$total = Pegawai::where($q_satker)->whereIn('unit_kerja_id', $list_uker)->where('peg_status', true)->count();

		$data= [
			'eselon_2a_l' => $js_2a_l,
			'eselon_2a_p' => $js_2a_p,
			'eselon_2b_l' => $js_2b_l,
			'eselon_2b_p' => $js_2b_p,
			'eselon_3a_l' => $js_3a_l,
			'eselon_3a_p' => $js_3a_p,
			'eselon_3b_l' => $js_3b_l,
			'eselon_3b_p' => $js_3b_p,
			'eselon_4a_l' => $js_4a_l,
			'eselon_4a_p' => $js_4a_p,
			'eselon_4b_l' => $js_4b_l,
			'eselon_4b_p' => $js_4b_p,
			'eselon_5a_l' => $js_5a_l,
			'eselon_5a_p' => $js_5a_p,
			'total_s' => $ps,
			'total' => $total,
		];

		return $data;
	}

    public function getJabatan() {
        $jabatan = $this->jabatan;
        $eselon = '';
        $eselon_kd = '';
        if (!$jabatan) {
            return [
                'jabatan' => '',
                'eselon' => '',
                'eselon_kd' => '',
                'jenis_jabatan' => '',
                'kode_jabatan' => '',
                'kode_lokasi' => '',
            ];
        }
        if ($jabatan->jabatan_jenis == 2) {
            $eselon = $jabatan->eselon ? $jabatan->eselon->eselon_nm : '-';
            $eselon_kd = $jabatan->eselon ? $jabatan->eselon->eselon_kd : '-';
            $jab_jenis = 'S';
            $nama_jabatan = $jabatan->jabatan_nama;
            $kode_jabatan = $jabatan->kode_jabatan;
        } elseif ($jabatan->jabatan_jenis == 3) {
            $jab_jenis = 'T';
            $jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
            $nama_jabatan = $jf ? $jf->jf_nama : '-';
            $kode_jabatan = $jf ? $jf->kode_jabatan : '';
        } elseif ($jabatan->jabatan_jenis == 4) {
            $jab_jenis ='F';
            $jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
            if ($jfu) {
                $nama_jabatan = $jfu->jfu_nama;
                $kode_jabatan = $jfu->kode_jabatan;
            } else {
                $nama_jabatan = 'Pelaksana';
                $kode_jabatan = '';
            }
        }
        $satker = SatuanKerja::find($this->satuan_kerja_id);
        if ($satker) {
            $kode_lokasi = $satker->kode_skpd;
        } else {
            $kode_lokasi = '';
        }
        return [
            'jabatan' => $nama_jabatan,
            'eselon' => $eselon,
            'eselon_kd' => $eselon_kd,
            'jenis_jabatan' => $jab_jenis,
            'kode_jabatan' => $kode_jabatan,
            'kode_lokasi' => $kode_lokasi,
        ];
    }

    public function cekSama(Pegawai $peg)
    {
    	return $this->getKey() == $user->getKey();
    }
}

?>