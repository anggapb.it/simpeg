<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Pegawai2 extends Model {
	protected $table="spg_pegawai";
	protected $primaryKey="peg_id";
	public $timestamps = false;

	public function status_kepegawaian() {
		return $this->belongsTo('App\Model\StatusKepegawaian','id_status_kepegawaian','id_status_kepegawaian');
	}

	public function jenis_asn() {
		return $this->belongsTo('App\Model\JenisAsn','peg_jenis_asn','id_jen_asn');
	}

	public function status_calon() {
		return $this->belongsTo('App\Model\StatusCalon','peg_status_calon','id_stat_calon');
	}

	public function jenis_pns() {
		return $this->belongsTo('App\Model\JenisPns','peg_jenis_pns','id_jen_pns');
	}

	public function kedudukan() {
		return $this->belongsTo('App\Model\Kedudukan','kedudukan_pegawai','id');
	}

	public function statustkd() {
		return $this->belongsTo('App\Model\StatusTKD','status_tkd','id');
	}

	public function jabatanid() {
		return $this->belongsTo('App\Model\Jabatan','jabatan_id');
	}
}

?>