<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use DB;

class PegawaiBelum extends Model {
	protected $connection = 'pgsql2';
	protected $table="jfu_belum_dipilih";
	protected $primaryKey="peg_id";
	public $timestamps = false;

	public function jabatan() {
		return $this->belongsTo('App\Model\Jabatan','jabatan_id');
	}

	public function satuan_kerja() {
		return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}

}

?>
