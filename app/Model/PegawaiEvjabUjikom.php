<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class PegawaiEvjabUjikom extends Model {
	protected $connection = 'pgsql2';
	protected $table="jfuevjabujikom";
	protected $primaryKey="peg_id";
	public $timestamps = false;
}


?>
