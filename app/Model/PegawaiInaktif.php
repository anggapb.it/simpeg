<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PegawaiInaktif extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_pegawai_inaktif";
    protected $primaryKey="id";
    public $timestamps = false;
}
