<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use DB;

class PegawaiNaikTurun extends Model {
	protected $connection = 'pgsql2';
	protected $table="jfu_naik_turun_kelas";
	protected $primaryKey="peg_id";
	public $timestamps = false;

	public function jabatan() {
		return $this->belongsTo('App\Model\Jabatan','jabatan_id');
	}

	public function satuan_kerja() {
		return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}

}

?>
