<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PegawaiPensiun extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_pensiun";
    protected $primaryKey="rpensiun_id";
    public $timestamps = false;
}
