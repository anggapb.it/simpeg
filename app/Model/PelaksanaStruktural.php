<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PelaksanaStruktural extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_riwayat_pelaksana_struktural";
    protected $primaryKey="rpelaksana_id";
    public $timestamps = false;

    public function jabatan() {
    	return $this->belongsTo('App\Model\Jabatan','jabatan_id');
    }
}
