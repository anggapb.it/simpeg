<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_pendidikan";
	protected $primaryKey="id_pend";
	public $timestamps = false;

	public function kategori_pendidikan() {
		return $this->belongsTo('App\Model\KategoriPendidikan','kat_pend_id','kat_pend_id');
	}
	

}



?>