<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $table = 'spg_pengajuan';
	protected $primaryKey = 'pengajuan_id';

	protected $connection = 'pgsql2';

	protected $casts = [
		'lampiran' => 'array'
	];

	protected $hidden = ['created_at', 'updated_at'];

	public function pegawai()
	{
		return $this->belongsTo('App\Model\Pegawai', 'peg_id', 'peg_id');
	}
}
