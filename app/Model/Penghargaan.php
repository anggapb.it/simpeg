<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Penghargaan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_penghargaan";
	protected $primaryKey="penghargaan_id";
	public $timestamps = false;

	public function riwayat_penghargaan(){
   		return $this->hasMany('App\Model\RiwayatPenghargaan');
	}
}



?>