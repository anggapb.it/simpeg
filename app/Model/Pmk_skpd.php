<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pmk_skpd extends Model
{
    // protected $connection = 'pgsql2';
    protected $table = "spg_pegawai_pmk";
    protected $primaryKey = 'peg_id';
     public $timestamps = true;
}
