<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_propinsi";
	protected $primaryKey="propinsi_id";
	public $timestamps = false;
	
	public function kabupaten(){
   		return $this->hasMany('Kabupaten');
	}
}



?>