<?php namespace App\Model\Relation;

trait HasManySamples {

    public function samples()
    {
        return $this->hasMany('App\Model\Sample');
    }
}

?>