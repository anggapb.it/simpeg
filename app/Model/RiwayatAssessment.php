<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiwayatAssessment extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_riwayat_assessment";
    protected $primaryKey="riw_assessment_id";
    public $timestamps = false;
}
