<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiwayatCuti extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_riwayat_cuti";
    protected $primaryKey="cuti_id";
    public $timestamps = false;
}
