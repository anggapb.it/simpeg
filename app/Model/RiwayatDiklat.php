<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatDiklat extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat_diklat";
	protected $primaryKey="diklat_id";
	public $timestamps = false;
	
	public function diklat_fungsional() {
		return $this->belongsTo('App\Model\DiklatFungsional','diklat_fungsional_id','diklat_fungsional_id');
	}
	
	public function diklat_struktural() {
		return $this->belongsTo('App\Model\DiklatStruktural','kategori_id','kategori_id');
	}
	
	public function diklat_teknis() {
		return $this->belongsTo('App\Model\DiklatTeknis','diklat_teknis_id','diklat_teknis_id');
	}

}



?>