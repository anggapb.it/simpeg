<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatDiklat2 extends Model {
	protected $table="spg_riwayat_diklat";
	protected $primaryKey="diklat_id";
	public $timestamps = false;
}

?>