<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatHukuman2 extends Model {
	protected $table="spg_riwayat_hukuman";
	protected $primaryKey="riw_hukum_id";
	public $timestamps = false;
}

?>