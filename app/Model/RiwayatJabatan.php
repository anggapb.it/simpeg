<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatJabatan extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat_jabatan";
	protected $primaryKey="riw_jabatan_id";
	public $timestamps = false;
	
	public function golongan() {
		return $this->belongsTo('App\Model\Golongan','gol_id','gol_id');
	}
}


?>