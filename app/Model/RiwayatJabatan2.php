<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatJabatan2 extends Model {
	protected $table="spg_riwayat_jabatan";
	protected $primaryKey="riw_jabatan_id";
	public $timestamps = false;
}

?>