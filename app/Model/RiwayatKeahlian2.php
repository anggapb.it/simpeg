<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatKeahlian2 extends Model {
	protected $table="spg_riwayat_keahlian";
	protected $primaryKey="riw_keahlian_id";
	public $timestamps = false;
}

?>