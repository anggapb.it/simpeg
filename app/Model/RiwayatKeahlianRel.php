<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatKeahlianRel extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat_keahlian_rel";
	protected $primaryKey="riw_keahlian_rel_id";
	public $timestamps = false;
}

?>