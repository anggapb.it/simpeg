<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatKeahlianRel2 extends Model {
	protected $table="spg_riwayat_keahlian_rel";
	protected $primaryKey="riw_keahlian_rel_id";
	public $timestamps = false;
}

?>