<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RiwayatKedHuk extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_riwayat_kedhuk";
    protected $primaryKey="kedhuk_id";
    public $timestamps = false;

    public function skpd()
    {
    	return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id');
    }

    public function kedudukan()
    {
    	return $this->belongsTo('App\Model\Kedudukan','status','id');
    }


}
