<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatKeluarga extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat";
	protected $primaryKey="riw_id";
	public $timestamps = false;
}

?>