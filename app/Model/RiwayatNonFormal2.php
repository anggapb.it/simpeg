<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatNonFormal2 extends Model {
	protected $table="spg_riwayat_nonformal";
	protected $primaryKey="non_id";
	public $timestamps = false;
}

?>