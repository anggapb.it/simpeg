<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatPangkat2 extends Model {
	protected $table="spg_riwayat_pangkat";
	protected $primaryKey="riw_pangkat_id";
	public $timestamps = false;
}

?>