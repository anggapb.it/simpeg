<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat_pendidikan";
	protected $primaryKey="riw_pendidikan_id";
	public $timestamps = false;

	public function tingkat_pendidikan(){
	    return $this->belongsTo('App\Model\TingkatPendidikan','tingpend_id');
	}

	public function kategori_pendidikan(){
	    return $this->belongsTo('App\Model\KategoriPendidikan');
	}

	public function jurusan(){
	    return $this->belongsTo('App\Model\Jurusan','jurusan_id');
	}

	public function universitas(){
	    return $this->belongsTo('App\Model\Universitas','univ_id');
	}

}



?>