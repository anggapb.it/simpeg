<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan2 extends Model {
	protected $table="spg_riwayat_pendidikan";
	protected $primaryKey="riw_pendidikan_id";
	public $timestamps = false;
}

?>