<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatPenghargaan extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_riwayat_penghargaan";
	protected $primaryKey="riw_penghargaan_id";
	public $timestamps = false;

	public function penghargaan(){
	    return $this->belongsTo('App\Model\Penghargaan');
	}
}



?>