<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RiwayatPenghargaan2 extends Model {
	protected $table="spg_riwayat_penghargaan";
	protected $primaryKey="riw_penghargaan_id";
	public $timestamps = false;
}

?>