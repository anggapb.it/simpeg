<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SKP extends Model
{
    protected $connection = 'pgsql2';
    protected $table="spg_skp";
    protected $primaryKey="skp_id";
    public $timestamps = false;
}
