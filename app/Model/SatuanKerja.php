<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class SatuanKerja extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_satuan_kerja";
	protected $primaryKey="satuan_kerja_id";
	public $timestamps = false;
}

?>