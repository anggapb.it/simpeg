<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SettingTempSrt extends Model
{
    protected $table = 'm_kgb_setting_template_srt';
    protected $connection = 'pgsql2';
    protected $fillable = ['template','id','userid'];
}
