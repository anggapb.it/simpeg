<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class StatusCalon extends Model {
	protected $connection = 'pgsql2';
	protected $table="status_calon";
	protected $primaryKey="id_stat_calon";
	public $timestamps = false;
}

?>