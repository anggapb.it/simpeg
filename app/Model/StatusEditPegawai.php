<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use DB;
class StatusEditPegawai extends Model {
	protected $table="status_edit_pegawai";
	protected $primaryKey="id";
	public $editor_id;
	public $action;

	public static function boot(){
		parent::boot();
		static::updating(function($model) {
			$dirty = $model->getDirty();
			DB::table('status_edit_pegawai_log')->insert([
				'sed_id' => $model->id,
				'status_id_before' => $dirty['status_id'],
				'status_id_after' => $model->status_id,
				'editor_id' => $model->editor_id,
				'action' => $model->action,
			]);
			return true;
		});
		static::saving(function($model) {
			$dirty = $model->getDirty();
			DB::table('status_edit_pegawai_log')->insert([
				'sed_id' => $model->id,
				'status_id_before' => 0,
				'status_id_after' => $model->status_id,
				'editor_id' => $model->editor_id,
				'action' => $model->action,
			]);
			return true;
		});
		static::created(function($model) {
			DB::table('status_edit_pegawai_log')->insert([
				'sed_id' => $model->id,
				'status_id_before' => 0,
				'status_id_after' => $model->status_id,
				'editor_id' => $model->editor_id,
				'action' => $model->action,
			]);
		});
	}

	public function pegawai(){
        return $this->belongsTo('App\Model\Pegawai2','peg_id','peg_id');
    }

	public function log(){
        return $this->hasMany('App\Model\StatusEditPegawaiLog','sed_id','id');
    }

}

?>