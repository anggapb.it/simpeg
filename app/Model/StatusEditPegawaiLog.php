<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class StatusEditPegawaiLog extends Model {
	protected $table="status_edit_pegawai_log";
	protected $primaryKey="id";
	public $timestamps = false;

	public function status(){
        return $this->belongsTo('App\Model\StatusEditPegawai','id','sed_id');
    }

    public function editor(){
        return $this->belongsTo('App\Model\User','id','editor_id');
    }
}

?>