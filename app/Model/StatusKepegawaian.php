<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class StatusKepegawaian extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_status_kepegawaian";
	protected $primaryKey="id_status_kepegawaian";
	public $timestamps = false;
}

?>