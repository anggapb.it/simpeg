<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusTKD extends Model
{
    protected $connection = 'pgsql2';
    protected $table="m_spg_status_tkd";
    protected $primaryKey="id";
    public $timestamps = false;
}
