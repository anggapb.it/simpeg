<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusUsulan extends Model
{
    protected $connection = 'pgsql2';
    protected $table = "m_status_usulan";
}
