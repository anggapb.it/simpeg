<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TTDKGB extends Model
{
	protected $connection = 'pgsql2';
    protected $table = "m_ttd_kgb"; 
    protected $fillable = [
        'peg_id_ttd', 'peg_nama'
    ];
}
