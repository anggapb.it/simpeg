<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class TingkatPendidikan extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_tingkat_pendidikan";
	protected $primaryKey="tingpend_id";
	public $timestamps = false;

	public function kategori_pendidikan(){
	    return $this->belongsTo('App\Model\KategoriPendidikan');
	}

	public function riwayat_pendidikan(){
   		return $this->hasMany('App\Model\RiwayatPendidikan');
	}

}



?>