<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_unit_kerja";
	protected $primaryKey="unit_kerja_id";
	public $timestamps = false;

	 public function satuan_kerja()
    {
        return $this->belongsTo('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
    }
}



?>