<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Universitas extends Model {
	protected $connection = 'pgsql2';
	protected $table="m_spg_universitas";
	protected $primaryKey="univ_id";
	public $timestamps = false;
}

?>