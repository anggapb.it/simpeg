<?php namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static $registration_rules=array(
		'username'=>'required|min:5|unique:users,username',
		'email'=>'required|email|unique:users,email',
		'password'=>'required|min:6|same:password_confirmation',
	);

	public static $change_password=array(
		'password'=>'required|min:6|same:password_confirmation',
	);

	public static $rules=array(
		'username'=>'required|unique:users,username',
		'password'=>'required|min:6',
		'nama'=>'required',
	);

	public static $error_messages=array(
		'required'=>':attribute harus diisi',
		'min'=>':attribute harus diatas :min karakter',
		'max'=>':attribute harus dibawah :max karakter',
		'same'=>':attribute dan :other harus sama.',
		'email'=>':attribute harus dalam format email.',
		'unique'=>':attribute sudah digunakan.',
	);

	public function role(){
	    return $this->belongsTo('App\Model\Role','role_id');
	}


}
