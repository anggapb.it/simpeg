<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model {
    protected $table        = 'users_device';
    protected $primaryKey   = 'id';
    protected $fillable     = ['id','user_id','api_key','device_type','device_description','cloud_key','gcm_key','apns_key'];
    
    public $timestamps      = false;
    
    public static function getUsersToken($users) {
        $gcm = [];
        $apns = [];
        foreach ($users as $user) {
            $devices = UserDevice::where('user_id',$user)->get();
            foreach ($devices as $device) {
	            if ($device->gcm_key != null && $device->gcm_key != "")
	                $gcm[] = $device->gcm_key;
	            if ($device->apns_key != null && $device->apns_key != "")
	                $apns[] = $device->apns_key;
	        }
        }
        return ['gcm'=>$gcm,'apns'=>$apns];
    }
}

?>
