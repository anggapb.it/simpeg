<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsulanDetailKgb extends Model
{
    protected $connection = 'pgsql2';
    protected $table = "spg_usulan_detail";
    protected $primaryKey = 'usulan_id_detail';

    /* protected $fillable = [
        'peg_id', 'user_id', 'status_kgb'
    ]; */
}
