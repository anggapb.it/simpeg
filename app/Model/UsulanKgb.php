<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsulanKgb extends Model
{
    protected $connection = 'pgsql2';
    protected $table = "spg_usulan";
    protected $primaryKey = 'usulan_id';

    /* protected $fillable = [
        'no_surat', 'tgl_surat', 'satker_id', 'user_id', 'status'
    ]; */

    public function statusUsulan()
    {
        return $this->hasMany('App\Model\StatusUsulan', 'status', 'status');
    }

    public function countUsulanDetail()
    {
        return $this->hasMany('App\Model\UsulanDetailKgb', 'usulan_id', 'usulan_id');
    }
}
