<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Walikota extends Model {
	protected $connection = 'pgsql2';
	protected $table="spg_walikota";
	protected $primaryKey="walikota_id";
	public $timestamps = false;
}

?>