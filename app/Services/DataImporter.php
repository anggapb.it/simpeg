<?php
namespace App\Services;
use File;
use Input;

class DataImporter extends \Maatwebsite\Excel\Files\ExcelFile {
	
	public function getFile(){
	   // Import a user provided file
		$file = Input::file('excelfile');
		$destinationPath = 'uploads'; // upload path
		if (!$file) {
			return false;
		}
		$extension = $file->getClientOriginalExtension(); // getting file extension
		$filename = $file->getClientOriginalName(); // getting file extension
		
		while (File::exists("$destinationPath/$filename")) {
			$extension_pos = strrpos($filename, '.');
			$filename = substr($filename, 0, $extension_pos) . str_random(5) . substr($filename, $extension_pos);
		}

		$upload_success = $file->move($destinationPath, $filename); // uploading file to given path

		if($upload_success){
			return "uploads/".$filename;
		}
	}

	/*public function getFilters()
	{
		return [
			'chunk'
		];
	}*/

}

?>