<?php
	function canSkpdEdit() {
		return true;
	}
	function canSkpdEditJabatan() {
		return false;
	}
	function isDisdik() {
		return Auth::user()->satuan_kerja_id == 21 || Auth::user()->satuan_kerja_id == 29;
	}

	function transformDate($date){
		setlocale(LC_ALL, 'IND');
		$newDate = strftime("%d %B %Y", strtotime($date));

		return $newDate;
	}

	function transformDateTime($date){
		setlocale(LC_ALL, 'IND');
		$newDate = strftime("%d %B %Y %H:%M", strtotime($date));

		return $newDate;
	}

	function editDate($date){
		$newDate = $date ? date('d-m-Y',strtotime($date)) : NULL;

		return $newDate;
	}

	function saveDate($date){
		$newDate = $date ? date("Y-m-d", strtotime($date)) : NULL;

		return $newDate;
	}

	function listBulan() {
   		return [""=>"-",1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
	}

	function getFullDate($time = 0) {
		if ($time == "") return $time;
		if ($time) {
	        $time = strtotime($time);
	    } else {
	        return "-";
	    }
	    return date("j",$time)." ".listBulan()[date("n",$time)]." ".date("Y",$time);
	}

	function getFullDateTime($time = 0) {
	    if ($time == "") return $time;
	    if ($time && $time != "0000-00-00 00:00:00") {
	        $time = strtotime($time);
	    } else {
	        return "-";
	    }
	    return date("j",$time)." ".listBulan()[date("n",$time)]." ".date("Y H:i:s",$time);
	}

	function getNamaBulan($tanggal){
	    return listBulan()[intval(date("n", strtotime($tanggal)))];
	}

	function getDateFromMySql($date, &$year, &$month, &$day) {
	  $year = intval(substr($date,0,4));
	  $month = intval(substr($date,5,2));
	  $day = intval(substr($date,8,2));
	}

	function getIndonesianMonth($month) {
	  $indonesianMonths = array("","Januari","Februari","Maret","April","Mei","Juni",
	    "Juli","Agustus","September","Oktober","November","Desember");
	  return $indonesianMonths[$month];
	}

	function getIndonesianDate($date) {
	  if ($date == 0) return("");
	  getDateFromMySql($date, $year, $month, $day);
	  return($day." ".getIndonesianMonth($month)." ".$year);
	}

	function getSatuanKerjaNama($id){
	    $satker = App\Model\SatuanKerja::find($id);

	    return $satker->satuan_kerja_nama;
	}
	function getAllUnitKerjaChildren($unit_kerja_id) {
		$lists = [$unit_kerja_id];
		$uks = [$unit_kerja_id];
		do {
			$uks = App\Model\UnitKerja::whereIn('unit_kerja_parent',$uks)->lists('unit_kerja_id');
			$lists = array_merge($lists,$uks->toArray());
		} while (count($uks) > 0);
		return $lists;
	}
	function getAllUnitKerjaChildrenObj($unit_kerja_id) {
		$lists = collect();
		$uks = [$unit_kerja_id];
		do {
			$ukobj = App\Model\UnitKerja::whereIn('unit_kerja_parent',$uks)->orderBy('unit_kerja_left')->get();
			$uks = App\Model\UnitKerja::whereIn('unit_kerja_parent',$uks)->lists('unit_kerja_id');
			$lists = $lists->merge($ukobj);
		} while (count($uks) > 0);
		return $lists;
	}

	function hitungUmur($tanggal_lahir) {
	    list($year,$month,$day) = explode("-",$tanggal_lahir);
	    $year_diff  = date("Y") - $year;
	    $month_diff = date("m") - $month;
	    $day_diff   = date("d") - $day;
	    if ($month_diff < 0) $year_diff--;
	        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
	    return $year_diff;
	}
	function logAction($aksi,$keterangan='',$entity_id=null,$username=null) {
	    if (!$username) {
	        $username = Auth::user() ? Auth::user()->username : 'Guest';
	    }
	    App\Model\ActionLog::create([
	        'username' => $username,
	        'aksi' => $aksi,
	        'keterangan' => $keterangan,
	        'entity_id' => $entity_id
	    ]);
	}

?>