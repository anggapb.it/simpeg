/*
 Navicat Premium Data Transfer

 Source Server         : Postgres 9.6
 Source Server Type    : PostgreSQL
 Source Server Version : 90612
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90612
 File Encoding         : 65001

 Date: 29/10/2019 15:51:24
*/


-- ----------------------------
-- Table structure for m_kgb_setting_template_srt
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_kgb_setting_template_srt";
CREATE TABLE "public"."m_kgb_setting_template_srt" (
  "template" text COLLATE "pg_catalog"."default",
  "userid" int2,
  "id" int2
)
;

-- ----------------------------
-- Records of m_kgb_setting_template_srt
-- ----------------------------
INSERT INTO "public"."m_kgb_setting_template_srt" VALUES ('<html>
<body>
<table>
    <tr>
        <td width="70%"></td>
        <td><p>Bandung,</p></td>
    </tr>    
    <tr>
        <td></td>
        <td><p>Kepada</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>Yth. Kepala Badan Pengelolaan Keuangan</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>dan Aset Kota Bandung</p></td>
    </tr>    
    <tr>
        <td></td>
        <td><p>(up. Kepala Bidang Perbendaharaan)</p></td>
    </tr>    
    <tr>
        <td></td>
        <td><p>di</p></td>
    </tr>   
     <tr>
        <td></td>
        <td><center><p><u>B A N D U N G</u></p></center></td>
    </tr>   
</table>   
<br \>
<center><b><u>SURAT PEMBERITAHUAN KENAIKAN GAJI BERKALA</u></b></center>
<center><p>NOMOR : ............................................</p></center>
<p>Dengan ini diberitahukan bahwa berhubung dengan telah dipenuhinya masa kerja syarat Administratif lainnya kepada :</p>
<table>
    <tr>
        <td>1.</td>    
        <td>Nama</td>  
        <td>:</td>   
        <td>{{$nama_pegawai}}</td>   
    </tr>    
     <tr>
        <td>2.</td>    
        <td>Tempat, Tanggal Lahir</td>  
        <td>:</td>   
        <td>{{$ttl}}</td>   
    </tr>    
     <tr>
        <td>3.</td>    
        <td>N I P</td>  
        <td>:</td>   
        <td>{{$nip}}</td>   
    </tr>    
    <tr>
        <td>4.</td>    
        <td>Pangkat / Jabatan</td>  
        <td>:</td>   
        <td>{{$pangkat_jabatan}}</td>   
    </tr>    
     <tr>
        <td>5.</td>    
        <td>Unit Kerja</td>  
        <td>:</td>   
        <td>{{$unit_kerja}}</td>   
    </tr>    
     <tr>
        <td>6.</td>    
        <td>Gaji Pokok Lama</td>  
        <td>:</td>   
        <td><b>Rp. {{$gapok_lama}} ({{$terbilang}})</b></td>   
    </tr>    
     <tr>
        <td></td>    
        <td><p>(Berdasarkan SKP terakhir Wali Kota Bandung tentang Gaji dan Pangkat yang ditetapkan)</p></td>  
    </tr>   
     <tr>
        <td></td>    
        <td>a. Oleh pejabat</td>  
        <td>:</td>   
        <td>Kepala Dinas Pendidikan dan Kebudayaan</td>   
    </tr>    
    <tr>
        <td></td>    
        <td>b. Tanggal</td>  
        <td>:</td>   
        <td>05 Januari 2015</td>   
    </tr>   
     <tr>
        <td></td>    
        <td>c. Nomor</td>  
        <td>:</td>   
        <td>822.4/01665/DISDIKBUD/2015</td>   
    </tr>    
     <tr>
        <td></td>    
        <td>d. Tanggal mulai berlaku gaji tersebut</td>  
        <td>:</td>   
        <td>01 Maret 2015</td>   
    </tr>    
     <tr>
        <td></td>    
        <td>e. Masa Kerja Gol pada tanggal tersebut</td>  
        <td>:</td>   
        <td>24 tahun 00 bulan</td>   
    </tr>    
     <tr>
        <td></td>    
        <td><p>Diberikan Kenaikan Gaji Berkala, hingga memperoleh :</p></td>  
    </tr>   
     <tr>
        <td>7.</td>    
        <td>Gaji Pokok Baru</td>  
        <td>:</td>   
        <td><b>Rp. {{$gapok_baru}} ({{$terbilang}})</b></td>   
    </tr>  
     <tr>
        <td>8.</td>    
        <td>Berdasarkan masa kerja</td>  
        <td>:</td>   
        <td><b>{{$masa_kerja}}</b></td>   
    </tr>  
     <tr>
        <td>9.</td>    
        <td>Dalam Golongan</td>  
        <td>:</td>   
        <td><b>{{$golongan}}</b></td>   
    </tr>   
     <tr>
        <td>10.</td>    
        <td>Mulai Tanggal</td>  
        <td>:</td>   
        <td><b>{{$mulai_tanggal}}</b></td>   
    </tr>   
</table>    
<br \>
<p>Keterangan</p>
<p>a. Yang bersangkutan adalah Pegawai Negeri Sipil Daerah</p>
<p>b. Kenaikan Gaji Berkala y.a.d pada tanggal : <b>01 Maret 2019</b></p>
<p>c. Jika memenuhi syarat - syarat yang diperlukan</p>
<br \>
<p>Diharapkan agar kepada Pegawai tersebut dibayarkan penghasilan gaji pokok yang baru Berdasarkan Peraturan Pemerintah Nomor 30 Tahun 2015</p>
<br \>

<table>
    <tr>
        <td width="70%"></td>
        <td><center><p>a.n WALIKOTA BANDUNG </p></center></td>
    </tr>    
   
     <tr>
        <td></td>
        <td><center><p>KEPALA BADAN KEPEGAWAIAN</p></center></td>
    </tr>    
     <tr>
        <td></td>
        <td><center><p>DAN PELATIHAN</p></center></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>    
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>   
     <tr>
        <td></td>
        <td><p>&nbsp;</p></td>
    </tr>   
   
    <tr>
        <td></td>
        <td><center><p>{{$nama_pejabat_penandatangan}}</p></center></td>
    </tr>    
    <tr>
        <td></td>
        <td><center><p>{{$nama_pangkat_pejabat_penandatangan}}</p></center></td>
    </tr>   
     <tr>
        <td></td>
        <td><center><p>NIP : {{$nip_pejabat_penandatangan}}</p></center></td>
    </tr>   
</table>  
<br \>
<p><b>Tembusan :</b></p>
<p>1. Yth. Bapak Sekretaris Daerah Kota Bandung (sebagai laporan);</p>
<p>2. Yth. Inspektur Kota Bandung</p>
<p>3. Yth. Kepala Dinas Pendidikan Kota Bandung</p>
</body>
</html>
', NULL, 1);
