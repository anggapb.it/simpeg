/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 05/12/2019 14:22:28
*/


-- ----------------------------
-- Table structure for m_status_usulan
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_status_usulan";
CREATE TABLE "public"."m_status_usulan" (
  "status" int2 NOT NULL,
  "nama_status" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of m_status_usulan
-- ----------------------------
INSERT INTO "public"."m_status_usulan" VALUES (0, 'Konsep');
INSERT INTO "public"."m_status_usulan" VALUES (10, 'Menunggu Verifikasi');
INSERT INTO "public"."m_status_usulan" VALUES (20, 'Sedang Verifikasi');
INSERT INTO "public"."m_status_usulan" VALUES (30, 'Proses');
INSERT INTO "public"."m_status_usulan" VALUES (50, 'Selesai');
INSERT INTO "public"."m_status_usulan" VALUES (-10, 'Ditolak');

-- ----------------------------
-- Primary Key structure for table m_status_usulan
-- ----------------------------
ALTER TABLE "public"."m_status_usulan" ADD CONSTRAINT "m_status_usulan_pkey" PRIMARY KEY ("status");
