/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 19/12/2019 10:28:20
*/


-- ----------------------------
-- Table structure for m_ttd_kgb
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_ttd_kgb";
CREATE TABLE "public"."m_ttd_kgb" (
  "id" int2 NOT NULL DEFAULT nextval('m_ttd_kgb_id_seq'::regclass),
  "gol_awal_id" int2,
  "peg_id_ttd" int4,
  "status" int2,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "gol_akhir_id" int2,
  "pin" varchar(255) COLLATE "pg_catalog"."default",
  "photo_ttd" varchar(255) COLLATE "pg_catalog"."default",
  "jenis_jabatan" int2
)
;
COMMENT ON COLUMN "public"."m_ttd_kgb"."peg_id_ttd" IS 'pegawai yang menandatangani';
COMMENT ON COLUMN "public"."m_ttd_kgb"."status" IS '1 = aktif, 2 = non aktif';
COMMENT ON COLUMN "public"."m_ttd_kgb"."jenis_jabatan" IS '1 = fungsional, 2 = pelaksana';

-- ----------------------------
-- Records of m_ttd_kgb
-- ----------------------------
INSERT INTO "public"."m_ttd_kgb" VALUES (10, 13, 16317, 1, '2019-12-09 14:27:35', '2019-12-19 10:27:10', 17, '$2y$10$sd/2J6XzKvf.MmQ/C1OHDe4tIZIJJLWygby9h7degF1K0bjMYcqrm', '1575876522_kenaikan-pangkat-pns-11-638.jpg', 1);
