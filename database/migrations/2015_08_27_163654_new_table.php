<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_status', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('status');
            $table->timestamps();
        });
		
		 Schema::create('status_edit_pegawai', function(Blueprint $table)
        {
            $table->increments('id');
			$table->integer('peg_id');
            $table->integer('peg_nip');
			$table->integer('status_id');
			$table->integer('editor_id');
            $table->timestamps();
        });
		
		 Schema::create('log_edit_pegawai', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('peg_id');
            $table->string('peg_nip');
			$table->integer('status_id');
			$table->string('action');
			$table->integer('user_id');
			$table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_status');
        Schema::drop('status_edit_pegawai');
        Schema::drop('log_edit_pegawai');
    }
}
