<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKeahlian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spg_pegawai', function ($table) {
            $table->string('peg_email', 100)->nullable();
            $table->integer('peg_punya_kpe')->nullable();
            $table->date('peg_punya_kpe_tgl')->nullable();

        });
      
        
        Schema::create('m_spg_keahlian', function(Blueprint $table)
        {
            $table->increments('keahlian_id');
            $table->string('keahlian_nama', 100)->nullable();
            $table->string('keahlian_deskripsi', 1000)->nullable();
            $table->integer('spesifik_dari_keahlian_id')->default(0);
            $table->string('keahlian_tipe')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at');
        });

        Schema::create('m_spg_keahlian_level', function(Blueprint $table)
        {
            $table->increments('keahlian_level_id');
            $table->integer('keahlian_id');
            $table->string('keahlian_level_nama')->nullable();
            $table->string('keahlian_level_deskripsi', 1000)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at');
        });

        Schema::create('spg_riwayat_keahlian', function(Blueprint $table)
        {
            $table->increments('riw_keahlian_id');
            $table->integer('peg_id');
            $table->integer('keahlian_id');
            $table->integer('keahlian_level_id')->nullable();
            $table->date('riw_keahlian_sejak')->nullable();
            $table->timestamps();
        });

        Schema::create('spg_riwayat_keahlian_rel', function(Blueprint $table)
        {
            $table->increments('riw_keahlian_rel_id');
            $table->integer('riw_keahlian_id');
            $table->integer('non_id')->nullable();
            $table->integer('riw_pendidikan_id')->nullable();
            $table->integer('diklat_id')->nullable();
            $table->string('predikat')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('m_spg_keahlian');
          Schema::drop('m_spg_keahlian_level');
          Schema::drop('spg_riwayat_keahlian');
          Schema::drop('spg_riwayat_keahlian_rel');
    }
}
