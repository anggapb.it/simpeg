<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatHukuman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spg_riwayat_hukuman', function ($table) {
            $table->increments('riw_hukum_id');
            $table->integer('peg_id');
            $table->integer('mhukum_id')->nullable();
            $table->date('riw_hukum_tmt')->nullable();
            $table->date('riw_hukum_sd')->nullable();
            $table->string('riw_hukum_sk')->nullable();
            $table->date('riw_hukum_tgl')->nullable();
            $table->text('riw_hukum_ket')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('spg_riwayat_hukuman');
    }
}
