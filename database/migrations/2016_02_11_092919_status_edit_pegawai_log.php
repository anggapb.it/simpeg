<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatusEditPegawaiLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('status_edit_pegawai_log', function ($table) {
            $table->increments('id');
            $table->integer('sed_id');
            $table->integer('status_id_before')->nullable();
            $table->integer('status_id_after')->nullable();
            $table->integer('editor_id')->nullable();
            $table->string('action')->nullable();
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
