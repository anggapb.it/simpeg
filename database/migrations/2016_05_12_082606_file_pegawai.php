<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilePegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spg_file_pegawai', function(Blueprint $table)
        {
            $table->increments('file_id');
            $table->integer('peg_id');
            $table->string('file_nama')->nullable();
            $table->string('file_lokasi')->nullable();
            $table->string('file_ket')->nullable();
            $table->timestamp('file_tgl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('spg_file_pegawai');
    }
}
