<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('spg_pengajuan', function(Blueprint $table)
        {
            $table->increments('pengajuan_id');
            $table->string('nama_pengajuan');
            $table->string('status');
            $table->bigInteger('peg_id');
            $table->text('lampiran');
            $table->date('tanggal_pengajuan');
            $table->string('keterangan');
            $table->integer('is_batal')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('spg_pengajuan');
    }
}
