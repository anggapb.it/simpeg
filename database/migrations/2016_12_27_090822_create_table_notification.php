<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('sender');
            $table->text('title');
            $table->text('content')->nullable();
            $table->jsonb('role_ids');
            $table->bigInteger('time');
            $table->index(['time']);
        });
        Schema::create('notifications_user_status', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('notification_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('is_readed')->unsigned()->default(0);
            $table->integer('is_deleted')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications_user_status');
        Schema::drop('notifications');
    }
}
