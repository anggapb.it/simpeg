<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//$this->call('UserTableSeeder');
		//$this->call('RoleTableSeeder');
		//$this->call('StatusTableSeeder');
		$this->call('KeahlianSeeder');
	}

}


class UserTableSeeder extends Seeder{
	public function run(){
    DB::table('users')->insert(array(
    		'username' => 'indana',
            'email' => 'izulfa28@gmail.com',
            'password' => Hash::make('123456'),
            'role_id' => '1',
            'satuan_kerja_id'=> '1',
        )
    );
    DB::table('users')->insert(array(
    		'username' => 'bkd',
            'email' => 'bkd@gmail.com',
            'password' => Hash::make('123456'),
            'role_id' => '3',
            'satuan_kerja_id'=> '6',
        )
    );
    
    DB::table('users')->insert(array(
    		'username' => 'skpd',
            'email' => 'bkd@gmail.com',
            'password' => Hash::make('123456'),
            'role_id' => '2',
            'satuan_kerja_id'=> '5',
        )
    );

    DB::table('users')->insert(array(
    		'username' => '196807021988031003',
            'email' => 'bkd@gmail.com',
            'password' => Hash::make('123456'),
            'role_id' => '4',
            'satuan_kerja_id'=> '3',
        )
    );
        
	}
}

class RoleTableSeeder extends Seeder{
	public function run(){
	    DB::table('role')->insert(array(
	    		'id' => '1',
	            'role_name' => 'Admin',
	        )
	    );
	    DB::table('role')->insert(array(
	    		'id' => '2',
	            'role_name' => 'SKPD',
	        )
	    );
	   	DB::table('role')->insert(array(
	    		'id' => '3',
	            'role_name' => 'BKD',
	        )
	    );
	    DB::table('role')->insert(array(
	    		'id' => '4',
	            'role_name' => 'Pegawai',
	        )
	    );
	}
}

class StatusTableSeeder extends Seeder{
	public function run(){
		DB::table('m_status')->insert(array(
	    		'id' => '1',
	            'status' => 'Draft',
	        )
	    );
	    DB::table('m_status')->insert(array(
	    		'id' => '2',
	            'status' => 'Submit',
	        )
	    );
	   	DB::table('m_status')->insert(array(
	    		'id' => '3',
	            'status' => 'Revisi',
	        )
	    );
	    DB::table('m_status')->insert(array(
	    		'id' => '4',
	            'status' => 'Verifikasi',
	        )
	    );
	}
}

class KeahlianSeeder extends Seeder{
	public function run(){
		DB::table('m_spg_keahlian')->insert(array(
	    		'keahlian_id' => 0,
	            'keahlian_nama' => 'Keahlian General',
	            'keahlian_deskripsi' => 'Keahlian General',
	            'spesifik_dari_keahlian_id' => null,
	            'keahlian_tipe'=>'default',
	        )
	    );
	    DB::table('m_spg_keahlian')->insert(array(
	    		'keahlian_id' => 1,
	            'keahlian_nama' => 'Bahasa',
	            'keahlian_deskripsi' => 'Bahasa General',
	            'spesifik_dari_keahlian_id' => 0,
	            'keahlian_tipe'=>'bahasa',
	        )
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>1,	
	    		'keahlian_id'=>0,
	    		'keahlian_level_nama'=>'Sangat Mahir',
	    		'keahlian_level_deskripsi'=>'Sangat Mahir',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>2,	
	    		'keahlian_id'=>0,
	    		'keahlian_level_nama'=>'Mahir',
	    		'keahlian_level_deskripsi'=>'Mahir',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>3,	
	    		'keahlian_id'=>0,
	    		'keahlian_level_nama'=>'Cukup Mahir',
	    		'keahlian_level_deskripsi'=>'Cukup Mahir',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>4,	
	    		'keahlian_id'=>0,
	    		'keahlian_level_nama'=>'Sedikit Mahir',
	    		'keahlian_level_deskripsi'=>'Sedikit Mahir',
	    	)
	    );

	   	 DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>5,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sangat Mahir Berbicara',
	    		'keahlian_level_deskripsi'=>'Sangat Mahir Berbicara',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>6,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Mahir Berbicara',
	    		'keahlian_level_deskripsi'=>'Mahir Berbicara',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>7,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Cukup Mahir Berbicara',
	    		'keahlian_level_deskripsi'=>'Cukup Mahir Berbicara',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>8,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sedikit Mahir Berbicara',
	    		'keahlian_level_deskripsi'=>'Sedikit Mahir Berbicara',
	    	)
	    );


	   	 DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>9,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sangat Mahir Menulis',
	    		'keahlian_level_deskripsi'=>'Sangat Mahir Menulis',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>10,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Mahir Menulis',
	    		'keahlian_level_deskripsi'=>'Mahir Menulis',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>11,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Cukup Mahir Menulis',
	    		'keahlian_level_deskripsi'=>'Cukup Mahir Menulis',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>12,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sedikit Mahir Menulis',
	    		'keahlian_level_deskripsi'=>'Sedikit Mahir Menulis',
	    	)
	    );

	     DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>13,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sangat Mahir Membaca',
	    		'keahlian_level_deskripsi'=>'Sangat Mahir Membaca',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>14,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Mahir Membaca',
	    		'keahlian_level_deskripsi'=>'Mahir Membaca',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>15,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Cukup Mahir Membaca',
	    		'keahlian_level_deskripsi'=>'Cukup Mahir Membaca',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>16,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sedikit Mahir Membaca',
	    		'keahlian_level_deskripsi'=>'Sedikit Mahir Membaca',
	    	)
	    );


	   	 DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>17,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sangat Mahir Mendengarkan',
	    		'keahlian_level_deskripsi'=>'Sangat Mahir Mendengarkan',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>18,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Mahir Mendengarkan',
	    		'keahlian_level_deskripsi'=>'Mahir Mendengarkan',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>19,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Cukup Mahir Mendengarkan',
	    		'keahlian_level_deskripsi'=>'Cukup Mahir Mendengarkan',
	    	)
	    );

	    DB::table('m_spg_keahlian_level')->insert(array(
	    		'keahlian_level_id'=>20,	
	    		'keahlian_id'=>1,
	    		'keahlian_level_nama'=>'Sedikit Mahir Mendengarkan',
	    		'keahlian_level_deskripsi'=>'Sedikit Mahir Mendengarkan',
	    	)
	    );
	}
}
