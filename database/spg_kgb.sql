/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 08/01/2020 16:12:58
*/


-- ----------------------------
-- Table structure for spg_kgb
-- ----------------------------
DROP TABLE IF EXISTS "public"."spg_kgb";
CREATE TABLE "public"."spg_kgb" (
  "kgb_id" int4 NOT NULL DEFAULT nextval('spg_kgb_kgb_id_seq'::regclass),
  "otorisasi_id" int4,
  "peg_id" int4,
  "gol_id" int4,
  "gaji_id" int4,
  "kgb_nosk" varchar(50) COLLATE "pg_catalog"."default",
  "kgb_tglsk" date,
  "kgb_tmt" date,
  "kgb_yad" date,
  "kgb_status_peg" bool,
  "kgb_thn" varchar(4) COLLATE "pg_catalog"."default",
  "kgb_bln" varchar(2) COLLATE "pg_catalog"."default",
  "kgb_otorisasi" varchar(150) COLLATE "pg_catalog"."default",
  "kgb_gapok" float8,
  "kgb_tglsurat" date,
  "kgb_nosurat" varchar(100) COLLATE "pg_catalog"."default",
  "kgb_status" int2,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "user_id" int4,
  "kgb_kerja_tahun" int2,
  "kgb_kerja_bulan" int2,
  "usulan_id_detail" int4,
  "ttd_digital" int2,
  "kgb_status_pdf" int2,
  "status_data" varchar(50) COLLATE "pg_catalog"."default",
  "ttd_pejabat" varchar(50) COLLATE "pg_catalog"."default",
  "unit_kerja" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table spg_kgb
-- ----------------------------
ALTER TABLE "public"."spg_kgb" ADD CONSTRAINT "spg_kgb_pkey1" PRIMARY KEY ("kgb_id");
