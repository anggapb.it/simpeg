/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 08/01/2020 16:13:44
*/


-- ----------------------------
-- Table structure for spg_pegawai_pmk
-- ----------------------------
DROP TABLE IF EXISTS "public"."spg_pegawai_pmk";
CREATE TABLE "public"."spg_pegawai_pmk" (
  "peg_id" int4 NOT NULL,
  "pmk_tahun" int2,
  "pmk_bulan" int2,
  "pmk_tmt_baru" date,
  "pmk_nosk" varchar(50) COLLATE "pg_catalog"."default",
  "pmk_sktgl" date,
  "masa_kerja_lama_thn" char(2) COLLATE "pg_catalog"."default",
  "masa_kerja_lama_bln" char(2) COLLATE "pg_catalog"."default",
  "pejabat_id" int4,
  "gol_id" int4,
  "pmk_ttd_pejabat" varchar(200) COLLATE "pg_catalog"."default",
  "pmk_jabatan" varchar(255) COLLATE "pg_catalog"."default",
  "pmk_unit_kerja" varchar(255) COLLATE "pg_catalog"."default",
  "pmk_gapok_lama" numeric(12) DEFAULT 0,
  "pmk_gapok_baru" numeric(12),
  "masa_kerja_baru_thn" char(2) COLLATE "pg_catalog"."default",
  "masa_kerja_baru_bln" char(2) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "created_by" int4,
  "updated_by" int4,
  "pmk_tmt_lama" date
)
;
