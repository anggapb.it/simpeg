/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 09/01/2020 11:42:08
*/


-- ----------------------------
-- Table structure for spg_usulan
-- ----------------------------
DROP TABLE IF EXISTS "public"."spg_usulan";
CREATE TABLE "public"."spg_usulan" (
  "usulan_id" int4 NOT NULL DEFAULT nextval('spg_usulan_usulan_id_seq'::regclass),
  "no_usulan" varchar(255) COLLATE "pg_catalog"."default",
  "tgl_usulan" date,
  "satuan_kerja_id" int2,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "user_id" int2,
  "status" int2,
  "surat_pengantar" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."spg_usulan"."status" IS '-10 = reject, 0 = draft, 10 = dikirim, 20 = checking, 30 = menunggu verifikasi, 50 = selesai';

-- ----------------------------
-- Primary Key structure for table spg_usulan
-- ----------------------------
ALTER TABLE "public"."spg_usulan" ADD CONSTRAINT "spg_usulan_pkey" PRIMARY KEY ("usulan_id");
