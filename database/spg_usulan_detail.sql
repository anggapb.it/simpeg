/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : simpegbaru
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 08/01/2020 16:13:14
*/


-- ----------------------------
-- Table structure for spg_usulan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."spg_usulan_detail";
CREATE TABLE "public"."spg_usulan_detail" (
  "usulan_id_detail" int4 NOT NULL DEFAULT nextval('spg_usulan_detail_usulan_id_detail_seq'::regclass),
  "peg_id" int4,
  "status_usulan_pegawai" int2,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "user_id" int4,
  "usulan_id" int4,
  "keterangan" text COLLATE "pg_catalog"."default",
  "kgb_kerja_tahun" int2,
  "kgb_kerja_bulan" int2,
  "kgb_tmt" date
)
;
COMMENT ON COLUMN "public"."spg_usulan_detail"."status_usulan_pegawai" IS '0 = draft, 10 = proses, -10 = ditolak, 50 = selesai';

-- ----------------------------
-- Primary Key structure for table spg_usulan_detail
-- ----------------------------
ALTER TABLE "public"."spg_usulan_detail" ADD CONSTRAINT "spg_usulan_detail_pkey" PRIMARY KEY ("usulan_id_detail");
