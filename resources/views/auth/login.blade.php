@extends('layouts.frontpage')

@section('content')
<header>
	  <div class="head-block">
		<div class="container">
		  <div class="line1"></div>
		  <div class="middle-block">
			<div class="row-eq-height">
			  <!-- <div class="col-md-2"> -->
				<div style="width: 100px;height: 90px;padding: 5px;">
				  <img src="{{ asset('images/logo.png') }}" class="logo"/>
				</div>
			  <!-- </div> -->
			  <div class="col-md-12 bg">
				<div class="row">
				  <div class="col-md-10 head-left">
					<h3><span class="hidden-xs">Sistem Informasi Manajemen Kepegawaian (SIMPEG)</span><span class="visible-xs">SIMPEG</span></h3>
					<h4><span class="hidden-xs">Badan Kepegawaian, Pendidikan dan Pelatihan Kota Bandung</span><span class="visible-xs">BKPP</span></h4>
				  </div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	</header>
<!-- <div class="row">
	<div class="header-image">
		<img src="{{ asset('/images/header.png')}}">
		</img>
	</div>
	<h4 class="blue" id="id-company-text"></h4>
</div> -->
<div class="col-sm-10 col-sm-offset-1">
	<div class="login-container">

		<div class="space-6"></div>
		<div class="position-relative">
			<div id="login-box" class="login-box visible widget-box no-border">
				<div class="widget-body">
					<div class="widget-main">
						<div class="center">
							<h1>
								<i class="ace-icon fa fa-user red"></i>
								<span class="blue"></span>
							</h1>
						</div>
						<div class="space-6"></div>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							Ada kesalahan<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						<form method="POST" action="{{ url('/auth/login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<fieldset>
								<label class="block clearfix">
									<span class="block input-icon input-icon-right">
										<input type="username" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}"/>
										<i class="ace-icon fa fa-user"></i>
									</span>
								</label>

								<label class="block clearfix">
									<span class="block input-icon input-icon-right">
										<input type="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') }}"/>
										<i class="ace-icon fa fa-lock"></i>
									</span>
								</label>

								<div class="space"></div>

								<div class="clearfix">
									<label class="inline">
										<input type="checkbox" class="ace" />
										<span class="lbl"> Remember Me</span>
									</label>

									<button type="button submit" class="width-35 pull-right btn btn-sm btn-primary">
										<i class="ace-icon fa fa-key"></i>
										<span class="bigger-110">Login</span>
									</button>
								</div>
								<div class="clearfix" style="padding-top:10px">
									<a href="#" class="btn btn-sso" style="display: block">
										<img src="{{ URL::asset('sso/sso-icon.png') }}" class="sso-icon">
										Login with <b>SSO</b>
									</a>
								</div>
							</fieldset>
						</form>
					</div><!-- /.widget-main -->
				</div><!-- /.widget-body -->
			</div><!-- /.login-box -->
		</div><!-- /.position-relative -->

	</div>
</div><!-- /.col -->
</div><!-- /.row -->
@endsection
@section('scripts')
<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
	$(document).on('click', '.toolbar a[data-target]', function(e) {
		e.preventDefault();
		var target = $(this).data('target');
		$('.widget-box.visible').removeClass('visible');//hide others
		$(target).addClass('visible');//show target
	});
});
</script>

@endsection