@extends('layouts.frontpage')

@section('content')
<div class="row">
	<div class="header-image">
		<img src="{{ asset('/images/header.png')}}">
		</img>
	</div>
	<h4 class="blue" id="id-company-text"></h4>
</div>
<div class="container">
  	<center>
	    <i class="fa fa-frown-o fa-4x"></i>
	    <h1>404</h1>
	    <p>Halaman Tidak Ditemukan</p>
	    <p><a href="{{ url('/') }}" class="btn btn-primary">Kembali</a></p>
	</center>
</div>
@endsection