@extends('layouts.frontpage')

@section('content')
<div class="row">
	<div class="header-image">
		<img src="{{ asset('/images/header.png')}}">
		</img>
	</div>
	<h4 class="blue" id="id-company-text"></h4>
</div>
<div class="container">
  	<center>
	    <i class="fa fa-frown-o fa-4x"></i>
	    <p>Mohon bersabar. Hubungi administrator untuk bantuan teknis. Terimakasih</p>
	</center>
</div>
@endsection