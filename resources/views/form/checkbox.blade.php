<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:'[]')));
	  $valj= json_decode($val);
	  if ($valj == null) { $valj = []; $val = '[]'; } ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<input id="{{$name}}" name="{{$name}}" multiple="" type="hidden" value="{{$val}}"/>
				<div class="checkbox-list">
					@foreach ($data as $key => $value)
					<label>
					<input class="checkbox-checked" type="checkbox" data-value="{{$key}}" data-target="{{$name}}"{{in_array($key,$valj)?' checked':''}}>{{$value}}</label>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
