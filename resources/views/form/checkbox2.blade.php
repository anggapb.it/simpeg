<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:'[]')));
	  $valj= json_decode($val);
	  if ($valj == null) { $valj = []; $val = '[]'; } ?>

<td>{{$label}} {!! isset($required) && $required?'<span class="required" aria-required="true">*</span>':'' !!}</td>
<td>:</td>
<td>
	{{-- <input id="{{$name}}" name="{{$name}}" multiple="" type="hidden" value="{{$val}}"/> --}}
	<div class="checkbox-list">
		@foreach ($data as $key => $value)
		<label>
		<input name="{{$name}}[]" value="{{ $key }}" class="checkbox-checked" type="checkbox" data-value="{{$key}}" data-target="{{$name}}"{{in_array($key,$valj)?' checked':''}}> {{$value}}</label>
		<br>
		@endforeach
	</div>
</td>
