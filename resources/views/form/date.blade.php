<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4 label-form">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-6">
				<input name="{{$name}}" type="text" id="{{$name}}" value="{{ $val }}" data-date-format="yyyy-mm-dd" class="form-control date-picker label-form-4" aria-required="{{$required?'true':'false'}}" aria-describedby="{{$name}}-datepicker-error">
				<span id="{{$name}}-datepicker-error" class="help-block help-block-error"></span>
			</div>
		</div>
	</div>
</div>
