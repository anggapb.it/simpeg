<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<div class="row">
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
		<div class="col-md-8">
			<input name="{{$name}}" type="text" id="{{$name}}" value="{{ $val }}" data-date-format="yyyy-mm-dd" class="form-control date-picker" {{$required?'required':''}}>
		</div>
	</div>
</div>
</div>