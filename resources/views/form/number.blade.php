<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<td>{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</td>
<td>:</td>
<td><input type="text" class="quantity" name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> value="{{$val}}" maxlength="{{$maxlength or '200'}}" placeholder="{{$placeholder or ''}}" style="width:250px;" {{$required?'required':''}}/></td>
