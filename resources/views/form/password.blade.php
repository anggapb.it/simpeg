<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<input type="password" name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> <?php if (isset($width)) echo 'style="max-width:150px;"' ?> class="form-control" placeholder="{{$placeholder or ''}}">
			</div>
		</div>
	</div>
</div>
