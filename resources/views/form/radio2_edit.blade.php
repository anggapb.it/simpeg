<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
@foreach ($data as $key => $value)
	<label>
		<input type="radio" class="{{$class}}" name="{{$name}}" id="{{$name}}" value="{{$key}}" {{$val==$key?' checked':''}}> {{$value}}
	</label>
@endforeach
