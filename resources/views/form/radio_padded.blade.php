<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-6">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-6">
				@foreach ($data as $key => $value)
				<div class="radio">
					<label>
						<input id="{{$name}}_{{$key}}" name="{{$name}}" type="radio" class="ace" value="{{$key}}" {{$val==$key?' checked':''}}>
						<span class="lbl"> {{$value}}</span>
					</label>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
