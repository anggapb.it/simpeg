<td>{{$label}}</td>
<td>:</td>
<td colspan="7">
    <select name="{{$name}}">
        @if (isset($empty))
		<option value="">{{$empty}}</option>
		@endif
		@foreach ($data as $key => $value)
		<option value="{{$key}}"{{old($name)==$key?' selected':''}}>{{$value}}</option>
		@endforeach
    </select>
</td>