<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<td>{{$label}} {!! isset($required) && $required?'<span class="required" aria-required="true">*</span>':'' !!}</td>
<td>:</td>
<td>
    <select id="{{$name}}" name="{{$name}}" class="responsive"> style="max-width:500px;" {{isset($required) && $required?'required':''}} <?php if (isset($disabled)) echo 'disabled' ?>>
        @if (isset($empty))
		<option value="">{{$empty}}</option>
		@endif
		@foreach ($data as $key => $value)
		<option value="{{$key}}"{{$val===$key?' selected':''}}>{{$value}}</option>
		@endforeach
    </select>

</td>
