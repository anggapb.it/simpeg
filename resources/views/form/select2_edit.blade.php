<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<select id="{{$name}}" name="{{$name}}" class="{{$class}}"  style="max-width:150px;">
    @if (isset($empty))
	<option value="">{{$empty}}</option>
	@endif
	@foreach ($data as $key => $value)
	<option value="{{$key}}"{{$val===$key?' selected':''}}>{{$value}}</option>
	@endforeach
</select>

