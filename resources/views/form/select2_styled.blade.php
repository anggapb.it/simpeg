<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="profile-info-row" id="{{$name}}_container">
	<div class="profile-info-name"> {{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</div>
	<div class="profile-info-value">
		<select class="form-control select2me" id="{{$name}}" name="{{$name}}">
			@if (isset($empty))
			<option value="">{{$empty}}</option>
			@endif
			@foreach ($data as $key => $value)
			<option value="{{$key}}"{{$val==$key?' selected':''}}>{{$value}}</option>
			@endforeach
		</select>
	</div>
</div>
