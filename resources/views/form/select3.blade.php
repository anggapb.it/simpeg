<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row" id="{{$name}}_container">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
            <div class="col-md-8">
                <div class="form-group col-md-7">
                <select class="form-control select2me" id="{{$name}}" name="{{$name}}">
                    @if (isset($empty))
                    <option value="">{{$empty}}</option>
                    @endif
                    @foreach ($data as $key => $value)
                    <option value="{{$key}}"{{$val==$key?' selected':''}}>{{$value}}</option>
                    @endforeach
                </select>
                </div>
                <input   type="button"  class="btn btn-minus" value=" - " style="margin-left: 10px;"/>
            </div>
        </div>
    </div>
</div>
