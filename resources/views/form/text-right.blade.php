<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<td align="right">{{$label}}</td>
<td>:</td>
<td><input type="text" name="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> value="{{$val}}" placeholder="{{$placeholder or ''}}"/></td>
