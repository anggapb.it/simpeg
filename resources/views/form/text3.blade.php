<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<div class="row" >
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
		<div class="col-md-8">
			<input type="text" name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> class="form-control {{$name}}" value="{{$val}}" placeholder="{{$placeholder or ''}}" style="font-size:14px;" {{$required?'required':''}} v-model="file_nama" v-on="keyup:doSearch | debounce 500">
		</div>
	</div>
</div>
</div>