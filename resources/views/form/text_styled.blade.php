<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="profile-info-row">
	<div class="profile-info-name"> {{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</div>
	<div class="profile-info-value">
		<input type="text" name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> class="form-control" value="{{$val}}" placeholder="{{$placeholder or ''}}">
	</div>
</div>
