<?php $val = (old($name)?old($name):(isset($model)?$model->$name:(isset($value)?$value:''))) ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label col-md-4">{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</label>
			<div class="col-md-8">
				<div id="{{$name}}">
					<input type="text" name="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> class="form-control typeahead" value="{{$val}}" placeholder="{{$placeholder or ''}}">
				</div>
			</div>
		</div>
	</div>
</div>
