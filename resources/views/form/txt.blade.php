<?php $val = (old($name)?old($name):(isset($value)?$value:(isset($model)?$model->$name:''))) ?>
<td>{{$label}} {!! $required?'<span class="required" aria-required="true">*</span>':'' !!}</td>
<td>:</td>
<td><input type="text" name="{{$name}}" id="{{$name}}" <?php if (isset($readonly)) echo 'readonly' ?> value="{{$val}}" placeholder="{{$placeholder or ''}}" style="width:250px;" maxlength="{{$maxlength or ''}}" {{$required?'required':''}}/></td>
