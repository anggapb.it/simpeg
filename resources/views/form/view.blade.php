<?php $val = (isset($value)?$value:(isset($model->$name)?$model->$name:'')) ?>
<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			<label class="control-label col-md-4 label-form-2">{{$label}}</label>
			<div class="col-md-6">
				<p class="form-control-static label-form-4" id="{{$name}}">
					{{ $val }}
				</p>
			</div>
		</div>
	</div>
</div>
