<?php $val = (isset($value)?$value:(isset($model->$name)?$model->$name:'')) ?>
<div class="profile-info-row">
	<div class="profile-info-name"> {{$label}} </div>
	<div class="profile-info-value">
	@if (isset($editable))
		<input class="profile-input-value" style="display: none" name="{{$name}}">
		<span id="{{$name}}" class="profile-view-value">{{ $val }}</span>&nbsp;
	@else
		<span id="{{$name}}">{{ $val }}</span>&nbsp;
	@endif
	</div>
</div>
