@extends('layouts.app')

<?php 
	use App\Model\StatusEditPegawai;
	use App\Model\MStatus;
	use App\Model\Pegawai2;
	use App\Model\LogEditPegawai;
	use App\Model\User;
	if(isDisdik()){
		$status = MStatus::orderBy('id','asc')->get();
	}
	elseif(Auth::user()->role_id == 1){
		
		$status = MStatus::orderBy('id','asc')->get();
	}
	else{
		$status = MStatus::orderBy('id','asc')->wherein('id',[1,2,3,4])->get();
	}

	$s_edit = DB::select('select editor_id, count(distinct status_edit_pegawai_log.sed_id) from status_edit_pegawai right join status_edit_pegawai_log 
		on status_edit_pegawai.id = status_edit_pegawai_log.sed_id group by editor_id;');
?>
@section('content')
<div class="row">
	<div class="col-xs-12">
		@if(Auth::user()->role_id != 4 && Auth::user()->role_id != 6 && Auth::user()->role_id != 5)
		<center><h3 class="title">DAFTAR STATUS</h3></center>
	 	<hr>
	 	@if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
	    	<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
	        {{ session('message') }}
	    </div>
		@endif
	  	<table class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     	<tr>
	     		<th>Status</th>
	     		<th>Jumlah</th>
	     		<th>Aksi</th>
	     	</tr>
	     </thead>
	     <tbody>
	     	@foreach($status as $s)
	     		<?php 

	     			$user_id = Auth::user()->id;
	     			if(Auth::user()->role_id == 8){

	     			if(Auth::user()->role_id == 8 && in_array($s->id,[5,6])){

	     					$c = DB::select('select count(DISTINCT(peg_id)) from status_edit_pegawai left join status_edit_pegawai_log 
							on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
							where status_id = '.$s->id.' and editor_id = '.Auth::user()->id);
						$count = $c[0]->count;
	     			}
	     			}
	     			else{

	     			if(Auth::user()->role_id == 2){
	     				/*$count = StatusEditPegawai::where('status_id', $s->id)->whereHas('log', function($query) use($user_id){
	     					$query->where('editor_id', $user_id);
	     				})->count(DB::raw('DISTINCT peg_id'));*/
	     				//$count = 1;
	     				if(in_array($s->id,[1,2,3,4])){	
							$c = DB::select('select count(DISTINCT(peg_id)) from status_edit_pegawai left join status_edit_pegawai_log on status_edit_pegawai.id = status_edit_pegawai_log.sed_id where status_id = '.$s->id.' and editor_id = '.$user_id);
	     				}
	     				else{
	     					$c = DB::select('select count(DISTINCT(peg_id)) from status_edit_pegawai left join status_edit_pegawai_log 
							on status_edit_pegawai.id = status_edit_pegawai_log.sed_id
							where status_id = '.$s->id);
	     				}
						$count = $c[0]->count;
	     			}
	     			else{
	     				$count = DB::table('status_edit_pegawai')->where('status_id', $s->id)->count(DB::raw('DISTINCT peg_id'));
	     			}
	     			}
	     		?>
	     	<tr>
	     		@if(Auth::user()->role_id == 8)
	     		@if(Auth::user()->role_id == 8 && in_array($s->id,[5,6]))
	     		<td>{{$s->status}}</td>
	     		<td>{{$count}}</td>
	     		<td><a href="{{url('/list-permohonan',$s->id)}}">Lihat Detail</a></td>
	     		@endif
	     		@else
	     		<td>{{$s->status}}</td>
	     		<td>{{$count}}</td>
	     		<td><a href="{{url('/list-permohonan',$s->id)}}">Lihat Detail</a></td>
	     		@endif

	     	</tr>
	     	@endforeach
	     </tbody>
	   	</table>
	   	@else
	   		@if(Auth::user()->role_id != 6 && Auth::user()->role_id != 5 && Auth::user()->role_id != 8)
	   		<?php
	   			$nip = Auth::user()->username;
	   			$peg = Pegawai2::where('peg_nip', $nip)->first();
	   			$st = StatusEditPegawai::where('peg_id', $peg->peg_id)->orderBy('id', 'desc')->first();
	   			$nama_status = MStatus::where('id', $st->status_id)->first();
	   			$ket = LogEditPegawai::where('status_id',$nama_status->id)->where('peg_id', $peg->peg_id)->orderBy('id','desc')->first();
	   		?>
	   		<center><h3 class="title">STATUS EDIT DATA PEGAWAI</h3></center>
	 		<hr>
	 		<p>Status Edit Data Anda saat ini yaitu <b>{{$nama_status->status}}</b></p>
	 		@if($nama_status->id == 3)
	 		<p>Detail Revisi :</p>
	 			<b>{{$ket->keterangan}}</b>
	 		@endif
	 		@else
	 		<center><h3 class="title">Dashboard</h3></center>
	 		<hr>
	 		<p>Selamat Datang</p>
	 		@endif
	   	@endif
	</div>
</div>
@if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
<div class="row">
	<div class="col-xs-12">
		<center><h3 class="title">STATISTIK OPERATOR</h3></center>
	 	<hr>
	  	<table class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0" id="list-operator">
	     <thead>
	     	<tr>
	     		<th>Nama Operator</th>
	     		<th>Jumlah</th>
	     		<th>Aksi</th>
	     	</tr>
	     </thead>
	     <tbody>
	     	@foreach ($s_edit as $key => $value)
	     		<?php 
	     			$operator = User::withTrashed()->where('id', $value->editor_id)->first();
	     		?>
	     	<tr>
	     		<td>{{$operator->nama or ''}}</td>
	     		<td>{{$value->count}}</td>
	     		<td><a href="{{url('/list-log-edit',$value->editor_id)}}">Lihat Detail</a></td>
	     	</tr>
	     	@endforeach
	     </tbody>
	   	</table>
	</div>
</div>
@endif
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#list-operator').DataTable({
			"bSort" : false,
			"iDisplayLength": 10,
    	});
	});
</script>
@endsection
