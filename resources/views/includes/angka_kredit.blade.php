<?php

	use App\Model\AngkaKredit;

	$struk = AngkaKredit::where('peg_id',$pegawai->peg_id)->get();
	$k_utama = AngkaKredit::where('peg_id',$pegawai->peg_id)->sum('kredit_utama');
	$k_penunjang = AngkaKredit::where('peg_id',$pegawai->peg_id)->sum('kredit_penunjang');
?>

<div class="col-xs-12">
	<h3 align="middle">Angka Kredit</h3><br>
	<div class="pull-left">Total Kredit Utama: <b>{{$k_utama}}</b><br>
	Total Kredit Penunjang: <b>{{$k_penunjang}}</b></div><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-kredit" onclick="addKredit()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>No. SK PAK</th>
		    <th>Tanggal SK PAK</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
			<th>Kredit Utama</th>
			<th>Kredit Penunjang</th>
			<th>Kredit Total</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Aksi</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($struk as $s)
	      	<tr>
	      		<td>{{$no++}}</td>
	      		<td>{{$s->no_sk_pak}}</td>
	      		<td>{{getFullDate($s->tgl_sk_pak)}}</td>
	      		<td>{{getFullDate($s->tgl_mulai)}}</td>
	      		<td>{{getFullDate($s->tgl_selesai)}}</td>
	      		<td>{{$s->kredit_utama}}</td>
	      		<td>{{$s->kredit_penunjang}}</td>
	      		<td>{{$s->kredit_total}}</td>
	      		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-kredit" 
					data-id="{{$s->angka_kredit_id}}" 
					data-no_sk_pak="{{$s->no_sk_pak}}" 
					data-tgl_sk_pak="{{$s->tgl_sk_pak}}" 
					data-tgl_mulai="{{$s->tgl_mulai}}" 
					data-tgl_selesai="{{$s->tgl_selesai}}" 
					data-kredit_utama="{{$s->kredit_utama}}" 
					data-kredit_penunjang="{{$s->kredit_penunjang}}" 
					data-kredit_total="{{$s->kredit_total}}"
					onclick="editKredit(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-kredit',$s->peg_id).'/'.$s->angka_kredit_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Angka Kredit Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
			@endif
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-kredit" tabindex="-1" role="dialog" aria-labelledby="modal-kredit" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelKredit"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/add/kredit')}}" id="formKredit">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'No. SK PAK','required'=>true,'name'=>'no_sk_pak'])
			<br>
			@include('form.date2',['label'=>'Tanggal SK PAK','required'=>true,'name'=>'tgl_sk_pak'])
			<br>
			@include('form.date2',['label'=>'Tanggal Mulai','required'=>true,'name'=>'tgl_mulai'])
			<br>
			@include('form.date2',['label'=>'Tanggal Selesai','required'=>true,'name'=>'tgl_selesai'])
			<br>
			@include('form.text2',['label'=>'Kredit Utama','required'=>true,'name'=>'kredit_utama'])
			<br>
			@include('form.text2',['label'=>'Kredit Penunjang','required'=>true,'name'=>'kredit_penunjang'])
			<br>
			@include('form.text2',['label'=>'Total Kredit','required'=>true,'name'=>'kredit_total','readonly'=>'readonly'])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

function hitungTotal() {
	var total,utama,penunjang;
	utama = $("#kredit_utama").val();
	penunjang = $("#kredit_penunjang").val();
	total = parseFloat(utama) + parseFloat(penunjang);
	if(!isNaN(total)) {
		$("#kredit_total").val(total.toFixed(3));
	}
}

var addKredit = function(){
	$("#formKredit").attr("action","{{ URL::to('riwayat/add/kredit/') }}");
	$("#labelKredit").html("Tambah Angka Kredit");
	$("#no_sk_pak").val("");
	$("#tgl_sk_pak,#tgl_mulai,#tgl_selesai").val("");
	$("#tgl_sk_pak,#tgl_mulai,#tgl_selesai").datepicker({autoclose: true});
	$("#kredit_utama,#kredit_penunjang").val("").keyup(function() {
		hitungTotal();
	});
	$("#kredit_total").val("");
	$("#kredit_utama,#kredit_penunjang").keydown(function (e) {
		var key   = e.keyCode ? e.keyCode : e.which;
		if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 || 
			(key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
			(key >= 35 && key <= 40) || 
			(key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) || 
			(key >= 96 && key <= 105))) 
			e.preventDefault();
	});
}
var editKredit = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formKredit").attr("action","{{ URL::to('riwayat/add/kredit/') }}");
	}else{
		$("#formKredit").attr("action","{{ URL::to('riwayat/edit/kredit/') }}/"+id);
	}

	$("#labelKredit").html("Edit Angka Kredit");
	$("#no_sk_pak").val($(e).data('no_sk_pak'));
	$("#tgl_sk_pak,#tgl_mulai,#tgl_selesai").datepicker({autoclose: true});
	$("#tgl_sk_pak").val($(e).data('tgl_sk_pak'));
	$("#tgl_mulai").val($(e).data('tgl_mulai'));
	$("#tgl_selesai").val($(e).data('tgl_selesai'));
	$("#kredit_utama").val($(e).data('kredit_utama')).keyup(function() {
		hitungTotal();
	});
	$("#kredit_penunjang").val($(e).data('kredit_penunjang')).keyup(function() {
		hitungTotal();
	});
	$("#kredit_total").val($(e).data('kredit_total'));
	$("#kredit_utama,#kredit_penunjang").keydown(function (e) {
		var key   = e.keyCode ? e.keyCode : e.which;
		if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 || 
			(key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
			(key >= 35 && key <= 40) || 
			(key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) || 
			(key >= 96 && key <= 105))) 
			e.preventDefault();
	});
}
</script>
