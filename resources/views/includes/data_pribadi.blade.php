<div class="col-xs-12">
	@if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
	    	<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
	        {{ session('message') }}
	    </div>
	@endif
	<div class="profile-user-info profile-user-info-striped">
		<div class="profile-info-row">
			<div class="profile-info-name"> Tempat, Tanggal Lahir </div>
			@if($pegawai_bkd->peg_lahir_tempat != $pegawai->peg_lahir_tempat || $pegawai_bkd->peg_lahir_tanggal != $pegawai->peg_lahir_tanggal)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_lahir_tempat}}, {{transformDate($pegawai_bkd->peg_lahir_tanggal)}}">
				<span class="editable" id="ttl">{{$pegawai->peg_lahir_tempat}}, {{transformDate($pegawai->peg_lahir_tanggal)}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Jenis Kelamin </div>
			@if($pegawai_bkd->peg_jenis_kelamin != $pegawai->peg_jenis_kelamin)
			<div class="profile-info-value alert-success">
			@else 
			<div class="profile-info-value">
			@endif
				@if($pegawai->peg_jenis_kelamin == 'L')
					@if($pegawai_bkd->peg_jenis_kelamin == 'L')
					<a class="grey show-option" href="#" title="Laki-laki">
					@elseif($pegawai_bkd->peg_jenis_kelamin == 'P')
					<a class="grey show-option" href="#" title="Perempuan">
					@endif
					<span class="editable" id="jk">Laki-laki</span>
				</a>
				@elseif($pegawai->peg_jenis_kelamin == 'P')
					@if($pegawai_bkd->peg_jenis_kelamin == 'L')
					<a class="grey show-option" href="#" title="Laki-laki">
					@elseif($pegawai_bkd->peg_jenis_kelamin == 'P')
					<a class="grey show-option" href="#" title="Perempuan">
					@endif
					<span class="editable" id="jk">Perempuan</span>
				</a>
				@else
					@if($pegawai_bkd->peg_jenis_kelamin == 'L')
					<a class="grey show-option" href="#" title="Laki-laki">
					@elseif($pegawai_bkd->peg_jenis_kelamin == 'P')
					<a class="grey show-option" href="#" title="Perempuan">
					@endif
					<span class="editable" id="jk">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Status Perkawinan </div>
			@if($pegawai_bkd->peg_status_perkawinan != $pegawai->peg_status_perkawinan)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pegawai->peg_status_perkawinan == '1')
					@if($pegawai_bkd->peg_status_perkawinan == '1')
						<a class="grey show-option" href="#" title="Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '2')
						<a class="grey show-option" href="#" title="Belum Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '3')
						<a class="grey show-option" href="#" title="Janda">
					@elseif($pegawai_bkd->peg_status_perkawinan == '4')
						<a class="grey show-option" href="#" title="Duda">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="status_perkawinan">Kawin</span>
					</a>
				@elseif($pegawai->peg_status_perkawinan == '2')
					@if($pegawai_bkd->peg_status_perkawinan == '1')
						<a class="grey show-option" href="#" title="Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '2')
						<a class="grey show-option" href="#" title="Belum Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '3')
						<a class="grey show-option" href="#" title="Janda">
					@elseif($pegawai_bkd->peg_status_perkawinan == '4')
						<a class="grey show-option" href="#" title="Duda">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="status_perkawinan">Belum Kawin</span>
					</a>
				@elseif($pegawai->peg_status_perkawinan == '3')
					@if($pegawai_bkd->peg_status_perkawinan == '1')
						<a class="grey show-option" href="#" title="Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '2')
						<a class="grey show-option" href="#" title="Belum Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '3')
						<a class="grey show-option" href="#" title="Janda">
					@elseif($pegawai_bkd->peg_status_perkawinan == '4')
						<a class="grey show-option" href="#" title="Duda">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="status_perkawinan">Janda</span>
					</a>
				@elseif($pegawai->peg_status_perkawinan == '4')
					@if($pegawai_bkd->peg_status_perkawinan == '1')
						<a class="grey show-option" href="#" title="Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '2')
						<a class="grey show-option" href="#" title="Belum Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '3')
						<a class="grey show-option" href="#" title="Janda">
					@elseif($pegawai_bkd->peg_status_perkawinan == '4')
						<a class="grey show-option" href="#" title="Duda">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="status_perkawinan">Duda</span>
					</a>
				@else
					@if($pegawai_bkd->peg_status_perkawinan == '1')
						<a class="grey show-option" href="#" title="Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '2')
						<a class="grey show-option" href="#" title="Belum Kawin">
					@elseif($pegawai_bkd->peg_status_perkawinan == '3')
						<a class="grey show-option" href="#" title="Janda">
					@elseif($pegawai_bkd->peg_status_perkawinan == '4')
						<a class="grey show-option" href="#" title="Duda">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="status_perkawinan">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Agama</div>
			@if($pegawai_bkd->id_agama != $pegawai->id_agama)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($agama)
					@if($agama_bkd)
					<a class="grey show-option" href="#" title="{{$agama_bkd->nm_agama ? $agama_bkd->nm_agama : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="agama">{{ $agama->nm_agama ? $agama->nm_agama : '-'}}</span>
					</a>
				@else
					@if($agama_bkd)
					<a class="grey show-option" href="#" title="{{$agama_bkd->nm_agama ? $agama_bkd->nm_agama : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="agama">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Status Pegawai </div>
			@if($pegawai_bkd->peg_status_kepegawaian != $pegawai->peg_status_kepegawaian )
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pegawai_bkd->peg_status_kepegawaian == '1')
					<a class="grey show-option" href="#" title="PNS">
				@elseif($pegawai_bkd->peg_status_kepegawaian == '2')
					<a class="grey show-option" href="#" title="CPNS">
				@elseif($pegawai_bkd->peg_status_kepegawaian == '3')
					<a class="grey show-option" href="#" title="PPPK">
				@else
					<a class="grey show-option" href="#" title="-">
				@endif
				@if($pegawai->peg_status_kepegawaian == '1')
					<span class="editable" id="status_pegawai">PNS</span>
				@elseif($pegawai->peg_status_kepegawaian == '2')
					<span class="editable" id="status_pegawai">CPNS</span>
				@elseif($pegawai->peg_status_kepegawaian == '3')
					<span class="editable" id="status_pegawai">PPPK</span>
				@else
					<span class="editable" id="status_pegawai">-</span>
				@endif
				</a>
			</div>
			<div class="profile-info-name"> TMT CPNS</div>
				@if($pegawai_bkd->peg_cpns_tmt != $pegawai->peg_cpns_tmt)
				<div class="profile-info-value alert-success">
				@else
				<div class="profile-info-value">
				@endif
				<a class="grey show-option" href="#" title="{{ $pegawai_bkd->peg_cpns_tmt ? transformDate($pegawai_bkd->peg_cpns_tmt) : '-'}}">
					<span class="editable" id="tmt_cpns">{{ $pegawai->peg_cpns_tmt ? transformDate($pegawai->peg_cpns_tmt) : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name"> TMT PNS </div>
				@if($pegawai_bkd->peg_pns_tmt != $pegawai->peg_pns_tmt)
				<div class="profile-info-value alert-success">
				@else
				<div class="profile-info-value">
				@endif
				<a class="grey show-option" href="#" title="{{ $pegawai_bkd->peg_pns_tmt ? transformDate($pegawai_bkd->peg_pns_tmt) : '-'}}">
					<span class="editable" id="tmt_pns">{{ $pegawai->peg_pns_tmt ? transformDate($pegawai->peg_pns_tmt) : '-'}}</span>
				</a>
				</div>
			</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis ASN</div>
			@if($pegawai_bkd->peg_jenis_asn != $pegawai->peg_jenis_asn)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{isset($pegawai->jenis_asn->nama_jenis_asn)?$pegawai->jenis_asn->nama_jenis_asn:''}}">
				<span class="editable" id="jenis_asn">{{isset($pegawai->jenis_asn->nama_jenis_asn)?$pegawai->jenis_asn->nama_jenis_asn:''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status Calon</div>
			@if($pegawai_bkd->peg_status_calon != $pegawai->peg_status_calon)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{isset($pegawai->status_calon->status)?$pegawai->status_calon->status:''}}">
				<span class="editable" id="status_calon">{{isset($pegawai->status_calon->status)?$pegawai->status_calon->status:''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis PNS</div>
			@if($pegawai_bkd->peg_jenis_pns != $pegawai->peg_jenis_pns)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{isset($pegawai->jenis_pns->nama_jenis)?$pegawai->jenis_pns->nama_jenis:''}}">
				<span class="editable" id="jenis_pns">{{isset($pegawai->jenis_pns->nama_jenis)?$pegawai->jenis_pns->nama_jenis:''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Kedudukan PNS</div>
			@if($pegawai_bkd->kedudukan_pegawai != $pegawai->kedudukan_pegawai)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{ isset($pegawai->kedudukan->kedudukan_pns) ? $pegawai->kedudukan->kedudukan_pns : '' }}">
				<span class="editable" id="kedudukan_pegawai">{{ isset($pegawai->kedudukan->kedudukan_pns) ? $pegawai->kedudukan->kedudukan_pns : '' }}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status TKD</div>
			@if($pegawai_bkd->status_tkd != $pegawai->status_tkd)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{isset($pegawai->statustkd->status_tkd)?$pegawai->statustkd->status_tkd:''}}">
				<span class="editable" id="status_tkd">{{isset($pegawai->statustkd->status_tkd)?$pegawai->statustkd->status_tkd:''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Pendidikan Awal</div>
			@if($pegawai_bkd->id_pend_awal != $pegawai->id_pend_awal)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pend_awal)
					@if (intval($kat_pend_awal->kat_pend_id) <= 6)
						@if($pend_awal_bkd)
							@if (intval($kat_pend_awal_bkd->kat_pend_id) <= 6)
								<a class="grey show-option" href="#" title="{{ $pend_awal_bkd->nm_pend }}">
							@else
								<a class="grey show-option" href="#" title="{{$kat_pend_awal_bkd->kat_nama}}-{{ $pend_awal_bkd->nm_pend }}">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="pendidikan_awal">{{ $pend_awal->nm_pend }}</span>
						</a>
					@else
						@if($pend_awal_bkd)
							@if (intval($kat_pend_awal_bkd->kat_pend_id) <= 6)
								<a class="grey show-option" href="#" title="{{ $pend_awal_bkd->nm_pend }}">
							@else
								<a class="grey show-option" href="#" title="{{$kat_pend_awal_bkd->kat_nama}}-{{ $pend_awal_bkd->nm_pend }}">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="pendidikan_awal">{{$kat_pend_awal->kat_nama}}-{{ $pend_awal->nm_pend }}</span>
						</a>
					@endif
				@else
					@if($pend_awal_bkd)
						@if (intval($kat_pend_awal_bkd->kat_pend_id) <= 6)
							<a class="grey show-option" href="#" title="{{ $pend_awal_bkd->nm_pend }}">
						@else
							<a class="grey show-option" href="#" title="{{$kat_pend_awal_bkd->kat_nama}}-{{ $pend_awal_bkd->nm_pend }}">
						@endif
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="pendidikan_awal">-</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">Tahun Pendidikan Awal</div>
			@if($pegawai_bkd->peg_pend_awal_th != $pegawai->peg_pend_awal_th)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{ $pegawai_bkd->peg_pend_awal_th ? $pegawai_bkd->peg_pend_awal_th : '-'}}">
					<span class="editable" id="pendidikan_awal_th">{{ $pegawai->peg_pend_awal_th ? $pegawai->peg_pend_awal_th : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Pendidikan Akhir</div>
			@if($pegawai_bkd->id_pend_akhir != $pegawai->id_pend_akhir)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pend_akhir)
					@if (intval($kat_pend_akhir->kat_pend_id) <= 6)
						@if($pend_akhir_bkd)
							@if (intval($kat_pend_akhir_bkd->kat_pend_id) <= 6)
								<a class="grey show-option" href="#" title="{{ $pend_akhir_bkd->nm_pend }}">
							@else
								<a class="grey show-option" href="#" title="{{$kat_pend_akhir_bkd->kat_nama}}-{{ $pend_akhir_bkd->nm_pend }}">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="pendidikan_akhir">{{ $pend_akhir->nm_pend }}</span>
						</a>
					@else
						@if($pend_akhir_bkd)
							@if (intval($kat_pend_akhir_bkd->kat_pend_id) <= 6)
								<a class="grey show-option" href="#" title="{{ $pend_akhir_bkd->nm_pend }}">
							@else
								<a class="grey show-option" href="#" title="{{$kat_pend_akhir_bkd->kat_nama}}-{{ $pend_akhir_bkd->nm_pend }}">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="pendidikan_akhir">{{$kat_pend_akhir->kat_nama}}-{{ $pend_akhir->nm_pend }}</span>
						</a>
					@endif
				@else
					@if($pend_akhir_bkd)
						@if (intval($kat_pend_akhir_bkd->kat_pend_id) <= 6)
							<a class="grey show-option" href="#" title="{{ $pend_akhir_bkd->nm_pend }}">
						@else
							<a class="grey show-option" href="#" title="{{$kat_pend_akhir_bkd->kat_nama}}-{{ $pend_akhir_bkd->nm_pend }}">
						@endif
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
					<span class="editable" id="pendidikan_akhir"></span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">Tahun Pendidikan Akhir</div>
			@if($pegawai_bkd->peg_pend_akhir_th != $pegawai->peg_pend_akhir_th)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{ $pegawai_bkd->peg_pend_akhir_th ? $pegawai_bkd->peg_pend_akhir_th : '-'}}">
					<span class="editable" id="pendidikan_akhir_th">{{ $pegawai->peg_pend_akhir_th ? $pegawai->peg_pend_akhir_th : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis Jabatan</div>
				@if($pegawai_bkd->jabatan_id != $pegawai->jabatan_id)
				<div class="profile-info-value alert-success">
				@else
				<div class="profile-info-value">
				@endif

				@if($jabatan)
					@if($jabatan->jabatan_jenis == 2)
						@if($jabatan_bkd)
							@if($jabatan_bkd->jabatan_jenis == 2)
								<a class="grey show-option" href="#" title="Struktural">
							@elseif($jabatan_bkd->jabatan_jenis == 3)
								<a class="grey show-option" href="#" title="Fungsional Tertentu">
							@elseif($jabatan_bkd->jabatan_jenis == 4)
								<a class="grey show-option" href="#" title="Fungsional Umum">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
					<span class="editable" id="jenis_jabatan">Struktural</span>
					</a>
					@elseif($jabatan->jabatan_jenis == 3)
						@if($jabatan_bkd)
							@if($jabatan_bkd->jabatan_jenis == 2)
								<a class="grey show-option" href="#" title="Struktural">
							@elseif($jabatan_bkd->jabatan_jenis == 3)
								<a class="grey show-option" href="#" title="Fungsional Tertentu">
							@elseif($jabatan_bkd->jabatan_jenis == 4)
								<a class="grey show-option" href="#" title="Fungsional Umum">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="jenis_jabatan">Fungsional Tertentu</span>
						</a>
					@elseif($jabatan->jabatan_jenis == 4)
						@if($jabatan_bkd)
							@if($jabatan_bkd->jabatan_jenis == 2)
								<a class="grey show-option" href="#" title="Struktural">
							@elseif($jabatan_bkd->jabatan_jenis == 3)
								<a class="grey show-option" href="#" title="Fungsional Tertentu">
							@elseif($jabatan_bkd->jabatan_jenis == 4)
								<a class="grey show-option" href="#" title="Fungsional Umum">
							@endif
						@else
							<a class="grey show-option" href="#" title="-">
						@endif
						<span class="editable" id="jenis_jabatan">Fungsional Umum</span>
						</a>
					@endif
				@else
					@if($jabatan_bkd)
						@if($jabatan_bkd->jabatan_jenis == 2)
							<a class="grey show-option" href="#" title="Struktural">
						@elseif($jabatan_bkd->jabatan_jenis == 3)
							<a class="grey show-option" href="#" title="Fungsional Tertentu">
						@elseif($jabatan_bkd->jabatan_jenis == 4)
							<a class="grey show-option" href="#" title="Fungsional Umum">
						@endif
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="jenis_jabatan">-</span>
				</a>
				@endif

			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Eselon</div>
			@if($pegawai_bkd->jabatan_id != $pegawai->jabatan_id)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$eselon_bkd['eselon_nm'] ? $eselon_bkd['eselon_nm'] : '-'}}">
					<span class="editable" id="eselon">{{$eselon['eselon_nm'] ? $eselon['eselon_nm'] : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status Gaji</div>
			@if($pegawai_bkd->peg_status_gaji != $pegawai->peg_status_gaji)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pegawai->peg_status_gaji == 1)
				<a class="grey show-option" href="#" title="Pemkot">
					<span class="editable" id="status_gaji">Pemkot</span>
				</a>
				@elseif($pegawai->peg_status_gaji == 2)
				<a class="grey show-option" href="#" title="Luar Pemkot">
					<span class="editable" id="status_gaji">Luar Pemkot</span>
				</a>
				@else
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_gaji">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Kedudukan Pegawai</div>
			@if($pegawai_bkd->id_status_kepegawaian != $pegawai->id_status_kepegawaian)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pegawai->id_status_kepegawaian != null)
				<a class="grey show-option" href="#" title="">
					<span class="editable" id="status_kepegawaian">{{$pegawai['status_kepegawaian']['status'] ? $pegawai['status_kepegawaian']['status'] : '-'}}</span>
				</a>
				@else
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_kepegawaian">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Nama Jabatan</div>
			@if($pegawai_bkd->jabatan_id != $pegawai->jabatan_id)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($jabatan)
					@if($jabatan_bkd)
						<a class="grey show-option" href="#" title="{{$jabatan_bkd->jabatan_nama ? $jabatan_bkd->jabatan_nama : 'Pelaksana'}}">
					@else
						<a class="grey show-option" href="#" title="Pelaksana">
					@endif
				<span class="editable" id="nama_jabatan">{{$jabatan->jabatan_nama ? $jabatan->jabatan_nama : 'Pelaksana'}}</span>
				</a>
				@else
					@if($jabatan_bkd)
						<a class="grey show-option" href="#" title="{{$jabatan_bkd->jabatan_nama ? $jabatan_bkd->jabatan_nama : 'Pelaksana'}}">
					@else
						<a class="grey show-option" href="#" title="Pelaksana">
					@endif
				<span class="editable" id="nama_jabatan">Pelaksana</span>
				</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Jabatan</div>
			@if($pegawai_bkd->peg_jabatan_tmt != $pegawai->peg_jabatan_tmt)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_jabatan_tmt ? transformDate($pegawai_bkd->peg_jabatan_tmt) : ''}}">
				<span class="editable" id="tmt_jabatan">{{$pegawai->peg_jabatan_tmt ? transformDate($pegawai->peg_jabatan_tmt) : ''}}</span>
				</a>
			</div>
		</div>

@if(($jabatan_bkd->jabatan_jenis == 3 && (isset($jf->rumpun_id) && $jf->rumpun_id == 17)) || ($jabatan_bkd->jabatan_jenis == 4 && $jabatan_bkd->jfu_id == 580))
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis Guru</div>
			@if($pegawai_bkd->jenis_guru != $pegawai->jenis_guru)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{($pegawai->jenis_guru)?$pegawai->jenis_guru:''}}">
				<span class="editable" id="jenis_guru">{{($pegawai->jenis_guru)?$pegawai->jenis_guru:''}}</span>
				</a>
			</div>
		</div>
@endif

		<div class="profile-info-row">
			<div class="profile-info-name">SOPD</div>
			@if($pegawai_bkd->satuan_kerja_id != $pegawai->satuan_kerja_id)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($uker)
					@if($uker['unit_kerja_level']==1)
					<a class="grey show-option" href="#" rel="tooltip" data-html="true" title="{{$uker_bkd['unit_kerja_nama']}} <br>&nbsp;&nbsp;&nbsp;&nbsp;{{$satker_bkd['satuan_kerja_nama']}}">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@elseif($uker['unit_kerja_level']==2)
						<?php
							$uker2 = App\Model\UnitKerja::where('unit_kerja_id', $uker['unit_kerja_parent'])->first();
							$uker2_bkd = App\Model\UnitKerja::where('unit_kerja_id', $uker_bkd['unit_kerja_parent'])->first();
						?>
					<a class="grey show-option" href="#" data-html="true" title="{{$uker_bkd['unit_kerja_nama']}} <br>&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2_bkd['unit_kerja_nama']}} <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker_bkd['satuan_kerja_nama']}}">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@elseif($uker['unit_kerja_level']==3)
						<?php
							$uker3 = App\Model\UnitKerja::where('unit_kerja_id', $uker['unit_kerja_parent'])->first();
							$uker3_bkd = App\Model\UnitKerja::where('unit_kerja_id', $uker_bkd['unit_kerja_parent'])->first();
							$uker2 = App\Model\UnitKerja::where('unit_kerja_id', $uker3['unit_kerja_parent'])->first();
							$uker2_bkd = App\Model\UnitKerja::where('unit_kerja_id', $uker3_bkd['unit_kerja_parent'])->first();
						?>
					<a class="grey show-option" href="#" data-html="true" title="{{$uker_bkd['unit_kerja_nama']}} <br>&nbsp;&nbsp;&nbsp;&nbsp;{{$uker3_bkd['unit_kerja_nama']}} <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2['unit_kerja_nama']}}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker_bkd['satuan_kerja_nama']}}">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;{{$uker3['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@endif							
						
				@else
				<a class="grey show-option" href="#" title="{{$satker_bkd['satuan_kerja_nama']}}">
					<span class="editable" id="sopd">
								<b>{{$satker['satuan_kerja_nama']}}</b></span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Instansi (jika dipekerjakan)</div>
			@if($pegawai_bkd->peg_instansi_dpk != $pegawai->peg_instansi_dpk)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_instansi_dpk ? $pegawai_bkd->peg_instansi_dpk : '-'}}">
					<span class="editable" id="instansi">{{$pegawai->peg_instansi_dpk ? $pegawai->peg_instansi_dpk : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Awal</div>
			@if($pegawai_bkd->gol_id_awal != $pegawai->gol_id_awal)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($golongan_awal)
					@if($golongan_awal_bkd)
						<a class="grey show-option" href="#" title="{{$golongan_awal_bkd->nm_gol}}, {{$golongan_awal_bkd->nm_pkt}}">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_awal">{{$golongan_awal->nm_gol}}, {{$golongan_awal->nm_pkt}}</span>
				</a>
				@else
					@if($golongan_awal_bkd)
						<a class="grey show-option" href="#" title="{{$golongan_awal_bkd->nm_gol}}, {{$golongan_awal_bkd->nm_pkt}}">
					@else
						<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_awal">-</span>
				</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Golongan Awal</div>
			@if($pegawai_bkd->peg_gol_awal_tmt != $pegawai->peg_gol_awal_tmt)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{transformDate($pegawai_bkd->peg_gol_awal_tmt) ? transformDate($pegawai_bkd->peg_gol_awal_tmt) : ''}}">
					<span class="editable" id="tmt_golongan_awal">{{transformDate($pegawai->peg_gol_awal_tmt) ? transformDate($pegawai->peg_gol_awal_tmt) : ''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Akhir</div>
			@if($pegawai_bkd->gol_id_akhir != $pegawai->gol_id_akhir)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($golongan_akhir)
					@if($golongan_akhir_bkd)
					<a class="grey show-option" href="#" title="{{$golongan_akhir_bkd->nm_gol}}, {{$golongan_akhir_bkd->nm_pkt}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_akhir">{{$golongan_akhir->nm_gol}}, {{$golongan_akhir->nm_pkt}}</span>
				</a>
				@else
					@if($golongan_akhir_bkd)
					<a class="grey show-option" href="#" title="{{$golongan_akhir_bkd->nm_gol}}, {{$golongan_akhir_bkd->nm_pkt}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_akhir">-</span>
				</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Golongan Akhir</div>
			@if($pegawai_bkd->peg_gol_akhir_tmt != $pegawai->peg_gol_akhir_tmt)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{transformDate($pegawai_bkd->peg_gol_akhir_tmt) ? transformDate($pegawai_bkd->peg_gol_akhir_tmt) : '-'}}">
					<span class="editable" id="tmt_golongan_akhir">{{transformDate($pegawai->peg_gol_akhir_tmt) ? transformDate($pegawai->peg_gol_akhir_tmt) : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Masa Kerja Gol. pada TMT Gol. Akhir</div>
			@if($pegawai_bkd->peg_kerja_tahun != $pegawai->peg_kerja_tahun || $pegawai_bkd->peg_kerja_bulan != $pegawai->peg_kerja_bulan)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_kerja_tahun}} Tahun {{$pegawai_bkd->peg_kerja_bulan}} Bulan">
				<span class="editable" id="masa_kerja_golongan">{{$pegawai->peg_kerja_tahun}} Tahun {{$pegawai->peg_kerja_bulan}} Bulan</span>
				</a>
			</div>

			<div class="profile-info-name">Gaji Pokok</div>
			@if($pegawai_bkd->peg_kerja_tahun != $pegawai->peg_kerja_tahun || $pegawai_bkd->peg_kerja_bulan != $pegawai->peg_kerja_bulan)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="Rp {{number_format($gaji_bkd['gaji_pokok'], 2)}}">
					<span class="editable" id="gaji_pokok">Rp {{number_format($gaji['gaji_pokok'], 2)}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. KARPEG</div>
			@if($pegawai_bkd->peg_karpeg != $pegawai->peg_karpeg)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_karpeg ? $pegawai_bkd->peg_karpeg : '-'}}">
					<span class="editable" id="karpeg">{{$pegawai->peg_karpeg ? $pegawai->peg_karpeg : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name">No. Karis/Karsu</div>
			@if($pegawai_bkd->peg_karsutri != $pegawai->peg_karsutri)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_karsutri ? $pegawai_bkd->peg_karsutri : '-'}}">
					<span class="editable" id="karsutri">{{$pegawai->peg_karsutri ? $pegawai->peg_karsutri : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. Askes</div>
			@if($pegawai_bkd->peg_no_askes != $pegawai->peg_no_askes)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_no_askes ? $pegawai_bkd->peg_no_askes : '-'}}">
					<span class="editable" id="askes">{{$pegawai->peg_no_askes ? $pegawai->peg_no_askes : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. KTP</div>
			@if($pegawai_bkd->peg_ktp != $pegawai->peg_ktp)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_ktp ? $pegawai_bkd->peg_ktp : '-'}}">
					<span class="editable" id="ktp">{{$pegawai->peg_ktp ? $pegawai->peg_ktp : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name">No. NPWP</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_npwp ? $pegawai_bkd->peg_npwp : '-'}}">
					<span class="editable" id="npwp">{{$pegawai->peg_npwp ? $pegawai->peg_npwp : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Darah</div>
			@if($pegawai_bkd->id_goldar != $pegawai->id_goldar)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($goldar)
					@if($goldar_bkd)
					<a class="grey show-option" href="#" title="{{$goldar_bkd['nm_goldar'] ? $goldar_bkd['nm_goldar'] : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_darah">{{$goldar['nm_goldar'] ? $goldar['nm_goldar'] : '-'}}</span>
				</a>
				@else
					@if($goldar_bkd)
					<a class="grey show-option" href="#" title="{{$goldar_bkd['nm_goldar'] ? $goldar_bkd['nm_goldar'] : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="golongan_darah">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">BAPETARUM</div>
			@if($pegawai_bkd->peg_bapertarum != $pegawai->peg_bapertarum)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($pegawai->peg_bapertarum == 1)
					@if($pegawai_bkd->peg_bapertarum == 1)
					<a class="grey show-option" href="#" title="Sudah Diambil">
					@elseif($pegawai_bkd->peg_bapertarum == 2)
					<a class="grey show-option" href="#" title="Belum Diambil">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="bapetarum">Sudah Diambil</span>
				</a>
				@elseif($pegawai->peg_bapertarum == 2)
					@if($pegawai_bkd->peg_bapertarum == 1)
					<a class="grey show-option" href="#" title="Sudah Diambil">
					@elseif($pegawai_bkd->peg_bapertarum == 2)
					<a class="grey show-option" href="#" title="Belum Diambil">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="bapetarum">Belum Diambil</span>
				</a>
				@else
					@if($pegawai_bkd->peg_bapertarum == 1)
					<a class="grey show-option" href="#" title="Sudah Diambil">
					@elseif($pegawai_bkd->peg_bapertarum == 2)
					<a class="grey show-option" href="#" title="Belum Diambil">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="bapetarum">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">TMT Gaji Berkala Terbaru</div>
			@if($pegawai_bkd->peg_tmt_kgb != $pegawai->peg_tmt_kgb)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_tmt_kgb ? transformDate($pegawai_bkd->peg_tmt_kgb) : '-'}}">
					<span class="editable" id="tmt_gaji">{{$pegawai->peg_tmt_kgb ? transformDate($pegawai->peg_tmt_kgb) : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Alamat Rumah</div>
			@if($pegawai_bkd->peg_rumah_alamat != $pegawai->peg_rumah_alamat)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_rumah_alamat ? $pegawai_bkd->peg_rumah_alamat : '-'}}">
					<span class="editable" id="alamat_rumah">{{$pegawai->peg_rumah_alamat ? $pegawai->peg_rumah_alamat : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Kel./Desa</div>
			@if($pegawai_bkd->peg_kel_desa != $pegawai->peg_kel_desa)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_kel_desa ? $pegawai_bkd->peg_kel_desa : '-'}}">
				<span class="editable" id="kelurahan">{{$pegawai->peg_kel_desa ? $pegawai->peg_kel_desa : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name info-alamat">Kec.</div>
			@if($pegawai_bkd->kecamatan_id != $pegawai->kecamatan_id)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($kecamatan != null)
					@if($kecamatan_bkd != null)
					<a class="grey show-option" href="#" title="{{$kecamatan_bkd->kecamatan_nm ? $kecamatan_bkd->kecamatan_nm : '-' }}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="kecamatan">{{$kecamatan->kecamatan_nm ? $kecamatan->kecamatan_nm : '-' }}</span>
				</a>
				@else
					@if($kecamatan_bkd != null)
					<a class="grey show-option" href="#" title="{{$kecamatan_bkd->kecamatan_nm ? $kecamatan_bkd->kecamatan_nm : '-' }}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="kecamatan">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Kab./Kota</div>
			@if($kab_bkd != $kab)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				@if($kab)
					@if($kab_bkd)
					<a class="grey show-option" href="#" title="{{$kab_bkd->kabupaten_nm ? $kab_bkd->kabupaten_nm : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="kota">{{$kab->kabupaten_nm ? $kab->kabupaten_nm : '-'}}</span>
				</a>
				@else
					@if($kab_bkd)
					<a class="grey show-option" href="#" title="{{$kab_bkd->kabupaten_nm ? $kab_bkd->kabupaten_nm : '-'}}">
					@else
					<a class="grey show-option" href="#" title="-">
					@endif
				<span class="editable" id="kota">-</span>
				</a>
				@endif
			</div>
			<div class="profile-info-name info-alamat">Kode Pos</div>
			@if($pegawai_bkd->peg_kodepos != $pegawai->peg_kodepos)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_kodepos ? $pegawai_bkd->peg_kodepos : '-'}}">
					<span class="editable" id="kode_pos">{{$pegawai->peg_kodepos ? $pegawai->peg_kodepos : '-' }}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Telp</div>
			@if($pegawai_bkd->peg_telp != $pegawai->peg_telp)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_telp ? $pegawai_bkd->peg_telp : '-'}}">
					<span class="editable" id="telp">{{$pegawai->peg_telp ? $pegawai->peg_telp : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name info-alamat">HP</div>
			@if($pegawai_bkd->peg_telp_hp != $pegawai->peg_telp_hp)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<span class="editable" id="hp">
					<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_telp_hp ? $pegawai_bkd->peg_telp_hp : '-'}}">
						{{$pegawai->peg_telp_hp or '-'}}
					</a>
				</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Email</div>
			@if($pegawai_bkd->peg_email != $pegawai->peg_email)
			<div class="profile-info-value alert-success">
			@else
			<div class="profile-info-value">
			@endif
				<a class="grey show-option" href="#" title="{{$pegawai_bkd->peg_email ? $pegawai_bkd->peg_email : '-'}}">
					<span class="editable" id="telp">{{$pegawai->peg_email ? $pegawai->peg_email : '-'}}</span>
				</a>
			</div>
		</div>
	</div>
	

	<div class="modal fade" id="modal-revisi" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
	 	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="{{url('/pegawai/revisi',$pegawai->peg_id)}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Revisi<div id="modal-button-edit"></div></h4>
			</div>
			<div class="modal-body" id="modal-detail-content">
				@include('form.text',['label'=>'Nama Pegawai','required'=>false,'name'=>'peg_nama', 'id'=>'txt_peg_nama','placeholder'=>'', 'value'=>$pegawai->peg_nama, 'readonly'=>true])
				<br>@include('form.textarea',['label'=>'Keterangan Revisi', 'required'=>false,'name'=>'keterangan'])
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary btn-xs" value="Submit">
			</div>
			</form>
		</div>
		</div>
	</div>
</div>