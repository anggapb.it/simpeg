<div class="col-xs-12">
	<script type="text/javascript">
	 setTimeout(function(){
        $("#messageFlash").fadeOut();
    },2000);
</script>
	@if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
	    	<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
	        {{ session('message') }}
	    </div>
	@endif
	<div class="profile-user-info profile-user-info-striped">
		<div class="profile-info-row">
			<div class="profile-info-name"> Tempat, Tanggal Lahir </div>
			<div class="profile-info-value">
				<span class="editable" id="ttl">{{$pegawai->peg_lahir_tempat}}, {{transformDate($pegawai->peg_lahir_tanggal)}}</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Jenis Kelamin </div>
			<div class="profile-info-value">
				@if($pegawai->peg_jenis_kelamin == 'L')
					<span class="editable" id="jk">Laki-laki</span>
				@elseif($pegawai->peg_jenis_kelamin == 'P')
					<span class="editable" id="jk">Perempuan</span>
				@else
					<span class="editable" id="jk">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Status Perkawinan </div>
			<div class="profile-info-value">
				@if($pegawai->peg_status_perkawinan == '1')
					<span class="editable" id="status_perkawinan">Kawin</span>
				@elseif($pegawai->peg_status_perkawinan == '2')
					<span class="editable" id="status_perkawinan">Belum Kawin</span>
				@elseif($pegawai->peg_status_perkawinan == '3')
					<span class="editable" id="status_perkawinan">Janda</span>
				@elseif($pegawai->peg_status_perkawinan == '4')
					<span class="editable" id="status_perkawinan">Duda</span>
				@else
					<span class="editable" id="status_perkawinan">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Agama</div>
			<div class="profile-info-value">
				@if($agama)
					<span class="editable" id="agama">{{ $agama->nm_agama ? $agama->nm_agama : '-'}}</span>
				@else
					<span class="editable" id="agama">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Status Pegawai </div>
			<div class="profile-info-value">
				@if($pegawai->peg_status_kepegawaian == '1')
					<span class="editable" id="status_pegawai">PNS</span>
				@elseif($pegawai->peg_status_kepegawaian == '2')
					<span class="editable" id="status_pegawai">CPNS</span>
				@elseif($pegawai->peg_status_kepegawaian == '3')
					<span class="editable" id="status_pegawai">PPPK</span>
				@else
					<span class="editable" id="status_pegawai">-</span>
				@endif
			</div>
			<div class="profile-info-name"> TMT CPNS</div>
			<div class="profile-info-value">
				<span class="editable" id="tmt_cpns">{{ $pegawai->peg_cpns_tmt ? transformDate($pegawai->peg_cpns_tmt) : '-'}}</span>
			</div>
			<div class="profile-info-name"> TMT PNS </div>
			<div class="profile-info-value">
				<span class="editable" id="tmt_pns">{{ $pegawai->peg_pns_tmt ? transformDate($pegawai->peg_pns_tmt) : '-'}}</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis ASN</div>
			<div class="profile-info-value">
				@if($pegawai->jenis_asn)
					<span class="editable" id="jenis_asn">{{ $pegawai->jenis_asn->nama_jenis_asn ? $pegawai->jenis_asn->nama_jenis_asn : '-'}}</span>
				@else
					<span class="editable" id="jenis_asn">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status Calon</div>
			<div class="profile-info-value">
				@if($pegawai->status_calon)
					<span class="editable" id="status_calon">{{ $pegawai->status_calon->status ? $pegawai->status_calon->status : '-'}}</span>
				@else
					<span class="editable" id="status_calon">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis PNS</div>
			<div class="profile-info-value">
				@if($pegawai->jenis_pns)
					<span class="editable" id="jenis_pns">{{ $pegawai->jenis_pns->nama_jenis ? $pegawai->jenis_pns->nama_jenis : '-'}}</span>
				@else
					<span class="editable" id="jenis_pns">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Kedudukan PNS</div>
			<div class="profile-info-value">
				@if($pegawai->kedudukan)
					<span class="editable" id="kedudukan_pegawai">{{ $pegawai->kedudukan->kedudukan_pns ? $pegawai->kedudukan->kedudukan_pns : '-'}}</span>
				@else
					<span class="editable" id="kedudukan_pegawai">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status TKD</div>
			<div class="profile-info-value">
				@if($pegawai->statustkd)
					<span class="editable" id="status_tkd">{{ $pegawai->statustkd->status_tkd ? $pegawai->statustkd->status_tkd : '-'}}</span>
				@else
					<span class="editable" id="status_tkd">-</span>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Pendidikan Awal</div>
			<div class="profile-info-value">
				@if($pend_awal)
					@if (intval($kat_pend_awal->kat_pend_id) <= 6)
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="pendidikan_awal">{{ $pend_awal->nm_pend }}</span>
						</a>
					@else
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="pendidikan_awal">{{$kat_pend_awal->kat_nama}}-{{ $pend_awal->nm_pend }}</span>
						</a>
					@endif
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="pendidikan_awal">-</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">Tahun Pendidikan Awal</div>
			<div class="profile-info-value">
				<span class="editable" id="pendidikan_awal_th">{{ $pegawai->peg_pend_awal_th ? $pegawai->peg_pend_awal_th : '-'}}</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Pendidikan Akhir</div>
			<div class="profile-info-value">
				@if($pend_akhir)
					@if (intval($kat_pend_akhir->kat_pend_id) <= 6)
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="pendidikan_akhir">{{ $pend_akhir->nm_pend }}</span>
						</a>
					@else
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="pendidikan_akhir">{{$kat_pend_akhir->kat_nama}}-{{ $pend_akhir->nm_pend }}</span>
						</a>
					@endif
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="pendidikan_akhir"></span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">Tahun Pendidikan Akhir</div>
			<div class="profile-info-value">
				<span class="editable" id="pendidikan_akhir_th">{{ $pegawai->peg_pend_akhir_th ? $pegawai->peg_pend_akhir_th : '-'}}</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Jenis Jabatan</div>
				<div class="profile-info-value">
				@if($jabatan)
					@if($jabatan->jabatan_jenis == 2)
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="jenis_jabatan">Struktural</span>
						</a>
					@elseif($jabatan->jabatan_jenis == 3)
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="jenis_jabatan">Fungsional Tertentu</span>
						</a>
					@elseif($jabatan->jabatan_jenis == 4)
						<a class="grey show-option" href="#" title="-">
							<span class="editable" id="jenis_jabatan">Fungsional Umum</span>
						</a>
					@endif
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="jenis_jabatan">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Eselon</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="eselon">{{$eselon['eselon_nm'] ? $eselon['eselon_nm'] : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Status Gaji</div>
			<div class="profile-info-value">
				@if($pegawai->peg_status_gaji == 1)
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_gaji">Pemkot</span>
				</a>
				@elseif($pegawai->peg_status_gaji == 2)
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_gaji">Luar Pemkot</span>
				</a>
				@else
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_gaji">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Kedudukan Pegawai</div>
			<div class="profile-info-value">
				@if($pegawai->id_status_kepegawaian != null)
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_kepegawaian">{{$pegawai['status_kepegawaian']['status'] ? $pegawai['status_kepegawaian']['status'] : '-'}}</span>
				</a>
				@else
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="status_kepegawaian">-</span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Nama Jabatan</div>
			<div class="profile-info-value">
				@if($jabatan)
					<a class="grey show-option" href="#" title="Pelaksana">
						<span class="editable" id="nama_jabatan">{{$jabatan->jabatan_nama ? $jabatan->jabatan_nama : 'Pelaksana'}}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="Pelaksana">
						<span class="editable" id="nama_jabatan">Pelaksana</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Jabatan</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
				<span class="editable" id="tmt_jabatan">{{$pegawai->peg_jabatan_tmt ? transformDate($pegawai->peg_jabatan_tmt) : ''}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">SOPD</div>
			<div class="profile-info-value">
				@if($uker)
					@if($uker['unit_kerja_level']==1)
					<a class="grey show-option" href="#" rel="tooltip" data-html="true" title="-">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@elseif($uker['unit_kerja_level']==2)
						<?php
							$uker2 = App\Model\UnitKerja::where('unit_kerja_id', $uker['unit_kerja_parent'])->first();
						?>
					<a class="grey show-option" href="#" data-html="true" title="-">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@elseif($uker['unit_kerja_level']==3)
						<?php
							$uker3 = App\Model\UnitKerja::where('unit_kerja_id', $uker['unit_kerja_parent'])->first();
							$uker2 = App\Model\UnitKerja::where('unit_kerja_id', $uker3['unit_kerja_parent'])->first();
						?>
					<a class="grey show-option" href="#" data-html="true" title="-">
						<span class="editable" id="sopd">
						{{$uker['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;{{$uker3['unit_kerja_nama']}}</br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$uker2['unit_kerja_nama']}}</br>
						<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$satker['satuan_kerja_nama']}}</b></span>
					</a>
					@endif							
						
				@else
				<a class="grey show-option" href="#" title="">
					<span class="editable" id="sopd">
								<b>{{$satker['satuan_kerja_nama']}}</b></span>
				</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Instansi (jika dipekerjakan)</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="instansi">{{$pegawai->peg_instansi_dpk ? $pegawai->peg_instansi_dpk : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Awal</div>
			<div class="profile-info-value">
				@if($golongan_awal)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_awal">{{$golongan_awal->nm_gol}}, {{$golongan_awal->nm_pkt}}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_awal">-</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Golongan Awal</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="tmt_golongan_awal">{{transformDate($pegawai->peg_gol_awal_tmt) ? transformDate($pegawai->peg_gol_awal_tmt) : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Akhir</div>
			<div class="profile-info-value">
				@if($golongan_akhir)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_akhir">{{$golongan_akhir->nm_gol}}, {{$golongan_akhir->nm_pkt}}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_akhir">-</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name">TMT Golongan Akhir</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="tmt_golongan_akhir">{{transformDate($pegawai->peg_gol_akhir_tmt) ? transformDate($pegawai->peg_gol_akhir_tmt) : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Masa Kerja Gol. pada TMT Gol. Akhir</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
				<span class="editable" id="masa_kerja_golongan">{{$pegawai->peg_kerja_tahun}} Tahun {{$pegawai->peg_kerja_bulan}} Bulan</span>
				</a>
			</div>

			<div class="profile-info-name">Gaji Pokok</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="gaji_pokok">Rp {{number_format($gaji['gaji_pokok'], 2)}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. KARPEG</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="karpeg">{{$pegawai->peg_karpeg ? $pegawai->peg_karpeg : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name">No. Karis/Karsu</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="karsutri">{{$pegawai->peg_karsutri ? $pegawai->peg_karsutri : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. Askes</div>
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="askes">{{$pegawai->peg_no_askes ? $pegawai->peg_no_askes : '-'}}</span>
				</a>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">No. KTP</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="ktp">{{$pegawai->peg_ktp ? $pegawai->peg_ktp : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name">No. NPWP</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="npwp">{{$pegawai->peg_npwp ? $pegawai->peg_npwp : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Golongan Darah</div>
			<div class="profile-info-value">
				@if($goldar)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_darah">{{$goldar['nm_goldar'] ? $goldar['nm_goldar'] : '-'}}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="golongan_darah">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">BAPETARUM</div>
			<div class="profile-info-value">
				@if($pegawai->peg_bapertarum == 1)
					<a class="grey show-option" href="#" title="-">
				<span class="editable" id="bapetarum">Sudah Diambil</span>
				</a>
				@elseif($pegawai->peg_bapertarum == 2)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="bapetarum">Belum Diambil</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="bapetarum">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">TMT Gaji Berkala Terbaru</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="tmt_gaji">{{$pegawai->peg_tmt_kgb ? transformDate($pegawai->peg_tmt_kgb) : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name">Alamat Rumah</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="alamat_rumah">{{$pegawai->peg_rumah_alamat ? $pegawai->peg_rumah_alamat : '-'}}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Kel./Desa</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
				<span class="editable" id="kelurahan">{{$pegawai->peg_kel_desa ? $pegawai->peg_kel_desa : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name info-alamat">Kec.</div>
			<div class="profile-info-value">
				@if($kecamatan != null)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="kecamatan">{{$kecamatan->kecamatan_nm ? $kecamatan->kecamatan_nm : '-' }}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="kecamatan">-</span>
					</a>
				@endif
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Kab./Kota</div>
			<div class="profile-info-value">
				@if($kab)
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="kota">{{$kab->kabupaten_nm ? $kab->kabupaten_nm : '-'}}</span>
					</a>
				@else
					<a class="grey show-option" href="#" title="-">
						<span class="editable" id="kota">-</span>
					</a>
				@endif
			</div>
			<div class="profile-info-name info-alamat">Kode Pos</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="kode_pos">{{$pegawai->peg_kodepos ? $pegawai->peg_kodepos : '-' }}</span>
				</a>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Telp</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="telp">{{$pegawai->peg_telp ? $pegawai->peg_telp : '-'}}</span>
				</a>
			</div>
			<div class="profile-info-name info-alamat">HP</div>
			<div class="profile-info-value">
				<span class="editable" id="hp">
					<a class="grey show-option" href="#" title="-">
						{{$pegawai->peg_telp_hp ? $pegawai->peg_telp_hp : '-'}}
					</a>
				</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name info-alamat">Email</div>
			<div class="profile-info-value">
				<a class="grey show-option" href="#" title="-">
					<span class="editable" id="telp">{{$pegawai->peg_email ? $pegawai->peg_email : '-'}}</span>
				</a>
			</div>
		</div>
	</div>
	

	<div class="modal fade" id="modal-revisi" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
	 	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="{{url('/pegawai/revisi',$pegawai->peg_id)}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Revisi<div id="modal-button-edit"></div></h4>
			</div>
			<div class="modal-body" id="modal-detail-content">
				@include('form.text',['label'=>'Nama Pegawai','required'=>false,'name'=>'peg_nama', 'id'=>'txt_peg_nama','placeholder'=>'', 'value'=>$pegawai->peg_nama, 'readonly'=>true])
				<br>@include('form.textarea',['label'=>'Keterangan Revisi', 'required'=>false,'name'=>'keterangan'])
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary btn-xs" value="Submit">
			</div>
			</form>
		</div>
		</div>
	</div>
</div>