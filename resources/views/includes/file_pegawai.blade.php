<?php

	use App\Model\FilePegawai2;
	use App\Model\FilePegawai;
	use App\Model\StatusEditPegawai;

	$file = FilePegawai::where('peg_id', $pegawai->peg_id)->orderBy('file_nama')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Document Management System</h3><br>
	<div class="pull-right tableTools-container">
	</div>
	<table id="tabel-riwayat-anak" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>Nama File</th>
			<th>File</th>
			<th>Keterangan</th>
		    <th>Tanggal Upload</th>
		</tr>
	     </thead>
	      <tbody>
	    	@foreach ($file as $f)
	    	<?php
	    		$f2 = FilePegawai2::where('peg_id', $pegawai->peg_id)->orderBy('file_nama')->first();
	    	?>
	    	<tr class="aktifFile{{$f->file_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$f2)
		    		<td class="alert-danger">{{$f->file_nama}}</td>
		    		<td class="alert-danger">{{$f->file_lokasi}}</a></td>
		    		<td class="alert-danger">{{$f->file_ket}}</td>
		    		<td class="alert-danger">{{$f->file_tgl}}</td>
	    		@else
	    			@if($f->file_nama != $f2->file_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_nama ? $f2->file_nama : '-'}}">{{ $f->file_nama ? $f->file_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$f->file_nama}}</td>
	    			@endif

	    			@if($f->file_lokasi != $f2->file_lokasi)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_lokasi ? $f2->file_lokasi : ''}}">{{ $f->file_lokasi }}</a></td>
	    			@else
	    				<td>{{$f->file_lokasi}}</td>
	    			@endif
	    			
	    			@if($f->file_ket != $f2->file_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_ket ? $f2->file_ket : ''}}">{{ $f->file_ket }}</a></td>
	    			@else
	    				<td>{{$f->file_ket}}</td>
	    			@endif

	    			@if($f->file_tgl != $f2->file_tgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_tgl ? $f2->file_tgl : ''}}">{{ $f->file_tgl }}</a></td>
	    			@else
	    				<td>{{$f->file_tgl}}</td>
	    			@endif
	    		@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
</div>
