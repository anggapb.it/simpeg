<?php

	use App\Model\FilePegawai2;
	use App\Model\FilePegawai;
	use App\Model\StatusEditPegawai;

	// $dokumen_penting = ['SK Pengangkatan PNS','Akte Kelahiran'];
	$dokumen_penting = [];
	$submit = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 2)->first();
	$file = FilePegawai2::whereNotIn('file_nama',$dokumen_penting)->where('peg_id', $pegawai->peg_id)->orderBy('file_nama')->get();
	$i=count($dokumen_penting) + 1;
	$n=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Document Management System</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-file" onclick="addFilePegawai()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>Nama File</th>
			<th>File</th>
			<th>Keterangan</th>
		    <th>Tanggal Upload</th>
		    @if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Pilihan</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<tr>
	      	@foreach($dokumen_penting as $d)
	      		<?php 
	      			$cek_penting = FilePegawai2::where('peg_id',$pegawai->peg_id)->where('file_nama',$d)->first();
	      			$cek_penting2 = null;
	      			if($cek_penting){
		      			$cek_penting2 = FilePegawai::where('peg_id', $pegawai->peg_id)->where('file_id', $cek_penting->file_id)->orderBy('file_nama')->first();
	      			}
	      		?>
	      		<td>{{$n++}}</td>
	      		@if(!$cek_penting2)
	      		<td class="alert-danger">{{$d}}</td>
	      		<td>
	      			@if($cek_penting)
	      				<a href="{{url('uploads/file',$cek_penting->file_lokasi)}}" target="_blank"><img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" />{{$cek_penting->file_lokasi}}</img></a>
	      			@else
	      				<img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" /></img>
	      			@endif
	      		</td>
	      		<td class="alert-danger">{{$cek_penting ? $cek_penting->file_ket : $d}}</td>
	      		<td class="alert-danger">{{$cek_penting ? $cek_penting->file_tgl : "Belum Upload"}}</td>
	      		@else
	      		<td>{{$d}}</td>
	      		<td>
	      			@if($cek_penting)
	      				<a href="{{url('uploads/file',$cek_penting->file_lokasi)}}" target="_blank"><img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" />{{$cek_penting->file_lokasi}}</img></a>
	      			@else
	      				<img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" /></img>
	      			@endif
	      		</td>
	      		@if($cek_penting->file_ket != $cek_penting2->file_ket)
	      			<td class="alert-success"><a class="grey show-option" href="#" title="{{$cek_penting2->file_ket ? $cek_penting2->file_ket : ''}}">{{ $cek_penting->file_ket }}</a></td>
	      		@else
	      			<td>{{$cek_penting->file_ket}}</td>
	      		@endif

	      		@if($cek_penting->file_tgl != $cek_penting2->file_tgl)
	      			<td class="alert-success"><a class="grey show-option" href="#" title="{{$cek_penting2->file_tgl ? $cek_penting2->file_tgl : ''}}">{{ $cek_penting->file_tgl }}</a></td>
	      		@else
	      			<td>{{$cek_penting->file_tgl}}</td>
	      		@endif
	      		@endif
	      		@if(!$submit)
	      		<td>
	      			@if($cek_penting)
	    			<a href="{{url('uploads/file',$cek_penting->file_lokasi)}}" target="_blank" class="btn btn-info btn-xs">
	    				<i class="fa fa-search"></i> Lihat File
	    			</a>
					<a href="{{url('/riwayat/download-file',array($pegawai->peg_id,$cek_penting->file_id),false)}}" 
		    			class="btn btn-success btn-xs">
						<i class="ace-icon fa fa-download bigger-120"></i>
							Download
					</a>
	    			@endif

	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-file" 
					data-nama="{{$d}}" 
					data-id="{{$cek_penting ? $cek_penting->file_nama : ''}}" 
					data-lokasi="{{$cek_penting ? $cek_penting->file_lokasi : ''}}" 
					data-ket="{{$cek_penting ? $cek_penting->file_ket : $d}}"
					data-penting="true"
					onclick="editFilePegawai(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
	      		</td>
	      		@endif
	      	</tr>
	      	@endforeach
	    	@foreach ($file as $f)
	    	<?php
	    		$f2 = FilePegawai::where('peg_id', $pegawai->peg_id)->where('file_id', $f->file_id)->orderBy('file_nama')->first();
	    		$tipe = substr($f['file_lokasi'], strpos($f['file_lokasi'], ".") + 1);
	    	?>
	    	<tr class="aktifFile{{$f->file_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$f2)
		    		<td class="alert-danger">{{$f->file_nama}}</td>
		    		@if($tipe== 'png' || $tipe == 'gif' || $tipe== 'jpeg' || $tipe=='jpg')
		    			<td class="alert-danger"><a href="{{url('uploads/file',$f->file_lokasi)}}" target="_blank"><img id="avatar" class="editable img-responsive" src="{{asset('/uploads/file/'.$f->file_lokasi) }}" />{{$f->file_lokasi}}</img></a></td>
		    		@else
    					<td><a href="{{url('uploads/file',$f->file_lokasi)}}" target="_blank"><img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" />{{$f->file_lokasi}}</img></a></td>
    				@endif
		    		<td class="alert-danger">{{$f->file_ket}}</td>
		    		<td class="alert-danger">{{$f->file_tgl}}</td>
	    		@else
	    			@if($f->file_nama != $f2->file_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_nama ? $f2->file_nama : '-'}}">{{ $f->file_nama ? $f->file_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$f->file_nama}}</td>
	    			@endif

	    			@if($f->file_lokasi != $f2->file_lokasi)
	    				@if($tipe== 'png' || $tipe == 'gif' || $tipe== 'jpeg' || $tipe=='jpg')
	    					<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_lokasi ? $f2->file_lokasi : ''}}"><img id="avatar" class="editable img-responsive" src="{{asset('/uploads/file/'.$f->file_lokasi) }}" />{{$f->file_lokasi}}</img></a></td>
	    				@else
	    					<td><a href="{{url('uploads/file',$f->file_lokasi)}}" target="_blank"><img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" />{{$f->file_lokasi}}</img></a></td>
	    				@endif
	    			@else
	    				@if($tipe== 'png' || $tipe == 'gif' || $tipe== 'jpeg' || $tipe=='jpg')
	    					<td><img id="avatar" class="editable img-responsive" src="{{asset('/uploads/file/'.$f->file_lokasi) }}" />{{$f->file_lokasi}}</img></td>
	    				@else
	    					<td><img id="avatar" class="editable img-responsive" src="{{asset('/img/doc.png') }}" />{{$f->file_lokasi}}</img></td>
	    				@endif
	    			@endif
	    			
	    			@if($f->file_ket != $f2->file_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_ket ? $f2->file_ket : ''}}">{{ $f->file_ket }}</a></td>
	    			@else
	    				<td>{{$f->file_ket}}</td>
	    			@endif

	    			@if($f->file_tgl != $f2->file_tgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$f2->file_tgl ? $f2->file_tgl : ''}}">{{ $f->file_tgl }}</a></td>
	    			@else
	    				<td>{{$f->file_tgl}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			@if($f->file_lokasi != null)
	    			<a href="{{url('uploads/file',$f->file_lokasi)}}" target="_blank" class="btn btn-info btn-xs">
	    				<i class="fa fa-search"></i> Lihat File
	    			</a>
	    			@endif
					<a href="{{url('/riwayat/download-file',array($pegawai->peg_id,$f->file_id),false)}}" 
		    			class="btn btn-success btn-xs">
						<i class="ace-icon fa fa-download bigger-120"></i>
							Download
					</a>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-file" 
					data-nama="{{$f->file_nama}}" 
					data-id="{{$f->file_id}}" 
					data-lokasi="{{$f->file_lokasi}}" 
					data-ket="{{$f->file_ket}}"
					data-penting="false"
					onclick="editFilePegawai(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-file',array($pegawai->peg_id,$f->file_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-file" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelFile"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/file')}}" id="fileForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			<p style="color: red">	*Isi Nama File dan Keterangan Terlebih dahulu sebelum memilih File </p>
			@include('form.text3',['label'=>'Nama File','required'=>false,'name'=>'file_nama','placeholder'=>''])
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<div class="error-file"></div>
					</div>
				</div>
			</div>
			<br>
			@include('form.text2',['label'=>'Keterangan','required'=>false,'name'=>'file_ket','placeholder'=>''])
			<br>
			<div class="row" >
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">File</label>
						<div class="col-md-8" id="buatupload" >
						   <span class="btn btn-success fileinput-button" id="upload1">
						        <i class="glyphicon glyphicon-plus"></i>
						        <span>Select files...</span>
						        <!-- The file input field used as target for the file upload widget -->
						        <input id="fileupload" type="file" name="files" onchange="validate_fileupload(this.value);" accept="application/pdf">
						    </span>
							 <!-- The global progress bar -->
						    <div id="progress" class="progress">
						        <div class="progress-bar progress-bar-success"></div>
						    </div>
						    <!-- The container for the uploaded files -->
						    <div id="files" class="files"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="filename" class="filename" value="" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">

var addFilePegawai = function(){
	$("#fileForm").attr("action","{{ URL::to('riwayat/add/file/') }}");
	$("#labelFile").text("Tambah File");
	$(".file_nama").val("");
	$(".file_ket").val("");
}
var editFilePegawai = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == '' && disable){
		$("#fileForm").attr("action","{{ URL::to('riwayat/add/file/') }}");
	}else{
		$("#fileForm").attr("action","{{ URL::to('riwayat/edit/file/') }}/"+$(e).data('id'));
	}
	$("#labelFile").text("Edit File");
	$(".file_nama").val($(e).data('nama'));
	$(".file_ket").val($(e).data('ket'));
	$('#files').append('<p/>').text($(e).data('lokasi'));
	$(".filename").val($(e).data('lokasi'));
	$('.file_nama').prop('readonly', disable);
}

function validate_fileupload(fileName){
      var allowed_extensions = new Array("xlsx","xls","csv","pdf","doc","docx","jpg","png","jpeg","gif");
      var file_extension = fileName.split('.').pop(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.
      var size = document.getElementById('fileupload').files[0].size;
      
      
      for(var i = 0; i < allowed_extensions.length; i++)
      {
          if(allowed_extensions[i]==file_extension && (size/1024/1024)<5)
          {
              return true; // valid file extension
          }
      }

      

      alert('input file tidak sesuai');
      $("#fileupload").val("");
      return false;
    }

    
</script>
