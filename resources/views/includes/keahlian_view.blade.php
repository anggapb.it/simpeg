<tr>
    <td style="padding-left:{{$i*15+4}}px">{{$k->keahlian_nama}}</td>
    <td>{{$k->keahlian_deskripsi}}</td>
    <td>{{$k->keahlian_tipe}}</td>
    <td>
        <button class="btn btn-primary btn-xs"
            data-toggle="modal" data-target="#modal-keahlian"
            data-parent="{{$k->keahlian_id}}"
            onclick="addKeahlianChild(this)">
            <i class="ace-icon glyphicon glyphicon-plus"></i>
            Sub Keahlian</button>
        <button class="btn btn-warning btn-xs"
            data-toggle="modal" data-target="#modal-keahlian" 
            data-nama="{{$k->keahlian_nama}}" 
            data-id="{{$k->keahlian_id}}" 
            data-deskripsi="{{$k->keahlian_deskripsi}}" 
            data-tipe="{{$k->keahlian_tipe}}"
            onclick="editKeahlian(this)">
            <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
            Edit</button>
        <a href="{{url('/data/keahlian/delete',$k->keahlian_id)}}" 
            class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                Delete
        </a>
    </td>
</tr>
 @foreach($k->allChildKeahlian as $k2)
        @include('includes.keahlian_view',['k'=>$k2,'i'=>$i+1])
    @endforeach