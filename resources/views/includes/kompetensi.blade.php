<?php

	use App\Model\PelaksanaStruktural;

	$struk = PelaksanaStruktural::leftJoin('m_spg_jabatan','m_spg_jabatan.jabatan_id','=','spg_riwayat_pelaksana_struktural.jabatan_id')->where('peg_id',$pegawai->peg_id)->get();
?>

<div class="col-xs-12">
	<h3 align="middle">Kompetensi</h3><br>
	<div class="pull-right tableTools-container">
<!-- 		@if(!$submit && Auth::user()->role_id != 5)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-kompetensi" onclick="addKompetensi()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif -->
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
		    <th>Status</th>
			<th>No. SK</th>
			<th>Tanggal SK</th>
			<th>TMT</th>
			<th>SKPD</th>
<!-- 			@if(!$submit && Auth::user()->role_id != 5 )
			<th>Aksi</th>
			@endif -->
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($struk as $s)
	      	<tr>
	      		<td>{{$no++}}</td>
	      		<td>{{$s->jabatan_nama}}</td>
	      		<td>{{getFullDate($s->tanggal_mulai)}}</td>
	      		<td>{{getFullDate($s->tanggal_selesai)}}</td>
	      		<td>{{strtoupper($s->jenis)}}</td>
<!-- 			@if(!$submit && Auth::user()->role_id != 5 )
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-kompetensi" 
					data-id="{{$s->rpelaksana_id}}" 
					data-mulai="{{$s->tanggal_mulai}}" 
					data-selesai="{{$s->tanggal_selesai}}" 
					data-unit="{{$s->jenis}}"
					onclick="editKompetensi(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-kompetensi',$s->peg_id).'/'.$s->rpelaksana_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Riwayat Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
			@endif -->
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-kompetensi" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelKompetensi"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/kompetensi')}}" id="formKompetensi">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'Tanggal Mulai','required'=>false,'name'=>'riw_tanggal_mulai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Selesai','required'=>false,'name'=>'riw_tanggal_selesai','placeholder'=>''])
			<br>
			<div class="row" id="jenis_container">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">Jenis</label>
						<div class="col-md-8">
							<select class="form-control select2me responsive" id="riw_jenis" name="riw_jenis">
								<option value="plt">PLT</option>
								<option value="plh">PLH</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

var addKompetensi= function(){
	$("#formKompetensi").attr("action","{{ URL::to('riwayat/add/kompetensi/') }}");
	$("#labelKompetensi").html("Tambah Kompetensi");
	$("#riw_nama_jabatan").val("");
	$("#riw_unit_kerja").val("");
	$("#riw_tanggal_mulai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_tanggal_selesai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_unit_kerja").select2();
}
var editKompetensi = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formKompetensi").attr("action","{{ URL::to('riwayat/add/kompetensi/') }}");
	}else{
		$("#formKompetensi").attr("action","{{ URL::to('riwayat/edit/kompetensi/') }}/"+id);
	}

	$("#labelKompetensi").html("Edit Kompetensi");
	$("#riw_nama_jabatan").val($(e).data('nama'));
	$("#riw_unit_kerja").val($(e).data('unit'));
	$("#riw_tanggal_mulai").val($(e).data('mulai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_tanggal_selesai").val($(e).data('selesai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_unit_kerja").select2();

}
</script>
