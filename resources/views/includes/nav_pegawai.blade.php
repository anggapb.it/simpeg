<li class="active">
	<a data-toggle="tab" href="#data-pribadi">Data Pribadi</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-pendidikan-formal">Riwayat Pendidikan Formal</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-pendidikan-nonformal">Riwayat Pendidikan Non Formal</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-kepangkatan">Riwayat Kepangkatan</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-jabatan">Riwayat Jabatan</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-kgb">Riwayat KGB</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-pmk">PMK</a>
</li>
@if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,3,2,5] : [1,3]))
<li>
	<a data-toggle="tab" href="#riwayat-hukuman">Catatan Hukdis</a>
</li>
@endif
@if(Auth::user()->id == 298)
<li>
	<a data-toggle="tab" href="#riwayat-assessment">Riwayat Assessment</a>
</li>
@endif
<li>
	<a data-toggle="tab" href="#riwayat-diklat-struktural">Riwayat Diklat Struktural</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-diklat-fungsional">Riwayat Diklat Fungsional</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-diklat-teknis">Riwayat Diklat Teknis</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-penghargaan">Riwayat Penghargaan</a>
</li>
<li>
	@if($pegawai->peg_jenis_kelamin == 'L')
	<a data-toggle="tab" href="#riwayat-pasutri">Riwayat Istri</a>
	@else
	<a data-toggle="tab" href="#riwayat-pasutri">Riwayat Suami</a>
	@endif
</li>
<li>
	<a data-toggle="tab" href="#riwayat-anak">Riwayat Anak</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-orang-tua">Riwayat Orang Tua</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-saudara">Riwayat Saudara</a>
</li>

<li>
	<a data-toggle="tab" href="#riwayat-tambahan">Riwayat Tambahan</a>
</li>

<li>
	<a data-toggle="tab" href="#riwayat-plt">Riwayat PLT/PLH</a>
</li>

@if($kepsek)
<li>
	<a data-toggle="tab" href="#riwayat-kepsek">Riwayat Kepala Sekolah</a>
</li>
@endif

<li>
	<a data-toggle="tab" href="#riwayat-cuti">Riwayat Cuti</a>
</li>

<li>
	<a data-toggle="tab" href="#file-pegawai">Document Management System</a>
</li>
<li>
	<a data-toggle="tab" href="#riwayat-kedhuk">Riwayat Kedudukan Hukum</a>
</li>
<!-- <li>
	<a data-toggle="tab" href="#kompetensi">Kompetensi</a>
</li> -->
<li>
	<a data-toggle="tab" href="#angka-kredit">Angka Kredit</a>
</li>
<li>
	<a data-toggle="tab" href="#skp">Sasaran Kerja Pegawai</a>
</li>
