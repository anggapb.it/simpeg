<div class="col-xs-12 col-sm-3 center">
	<div>
		<!-- #section:pages/profile.picture -->
		<span class="profile-picture">
			@if(file_exists('uploads/'.$pegawai->peg_foto) && $pegawai->peg_foto != null)
				<a href="{{asset('/uploads/'.$pegawai->peg_foto) }}" target="_blank">
					<img id="avatar" class="editable img-responsive" src="{{asset('/uploads/'.$pegawai->peg_foto) }}" />
				</a>
			@else
				<img id="avatar" class="editable img-responsive" src="{{asset('/uploads/none.png') }}" />
			@endif
		</span>

		<!-- /section:pages/profile.picture -->
		<div class="space-4"></div>
	</div>
</div>
<div class="col-xs-12 col-sm-5">
	<div class="profile-user-info profile-user-info-striped">
		<div class="profile-info-row">
			<div class="profile-info-name"> NIP </div>
			<div class="profile-info-value alert-warning">
				<span class="editable" id="nip">{{$pegawai->peg_nip}}</span>
			</div>
		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> NIP Lama </div>
			<div class="profile-info-value alert-warning">
				<span class="editable" id="nip_lama">{{$pegawai->peg_nip_lama}}</span>
			</div>

		</div>
		<div class="profile-info-row">
			<div class="profile-info-name"> Nama Lengkap </div>
			<div class="profile-info-value">
				@if($pegawai->peg_gelar_belakang == '')
				<span class="editable" id="nama_lengkap">{{ $pegawai->peg_gelar_depan}}{{$pegawai->peg_nama}} {{$pegawai->peg_gelar_belakang}}</span>
				@else
				<span class="editable" id="nama_lengkap">{{ $pegawai->peg_gelar_depan}}{{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</span>
			
				@endif
			</div>
		</div>
	</div>
	<div class="space-12"></div>
</div>


