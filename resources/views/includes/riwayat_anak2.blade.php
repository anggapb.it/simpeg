<?php

	use App\Model\RiwayatKeluarga2;
	use App\Model\RiwayatKeluarga;
	use App\Model\StatusEditPegawai;

	$submit = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 2)->first();
	$kel = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',1)->orderBy('riw_tgl_lahir')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Riwayat Anak</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-anak" onclick="addAnak()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-anak" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>NIK</th>
			<th>Nama Anak</th>
			<th>Jenis<br/>Kelamin</th>
			<th>Tempat dan Tanggal Lahir</th>
		    <th>Status <br/>Perkawinan</th>
		    <th>Memperoleh<br/>Tunjangan</th>
		    <th>Pendidikan</th>
		    <th>Pekerjaan</th>
		    <th>Keterangan</th>
		    @if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Pilihan</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	    	@foreach ($kel as $k)
	    	<?php
	    		$k2 = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_id', $k->riw_id)->where('riw_status',1)->orderBy('riw_tgl_lahir')->first();
	    		if($k->riw_status_tunj == 'TRUE'){
	    			$stat ='YA';	
	    		}elseif($k->riw_status_tunj == 'FALSE'){
	    			$stat ='TIDAK';
	    		}else{
	    			$stat = '-';
	    		}

	    		if($k2['riw_status_tunj'] == 'TRUE'){
	    			$stat2 ='YA';	
	    		}elseif($k2['riw_status_tunj'] == 'FALSE'){
	    			$stat2 ='TIDAK';
	    		}else{
	    			$stat2 = '-';
	    		}

	    		if($k->riw_kelamin == 'L'){
	    			$jk = 'Laki-laki';
	    		}elseif($k->riw_kelamin == 'P'){
	    			$jk = 'Perempuan';
	    		}else{
	    			$jk = '-';
	    		}

	    		if($k2['riw_kelamin'] == 'L'){
	    			$jk2 = 'Laki-laki';
	    		}elseif($k2['riw_kelamin'] == 'P'){
	    			$jk2 = 'Perempuan';
	    		}else{
	    			$jk2 = '-';
	    		}

	    		if($k->riw_status_perkawinan==1){
	    			$kaw = 'Kawin';
	    		}elseif($k->riw_status_perkawinan==2){
	    			$kaw = 'Belum Kawin';
	    		}elseif($k->riw_status_perkawinan==3){
	    			$kaw = 'Janda';
	    		}elseif($k->riw_status_perkawinan==4){
	    			$kaw = 'Duda';
	    		}else{
	    			$kaw = '-';
	    		}

	    		if($k2['riw_status_perkawinan']==1){
	    			$kaw2 = 'Kawin';
	    		}elseif($k2['riw_status_perkawinan']==2){
	    			$kaw2 = 'Belum Kawin';
	    		}elseif($k2['riw_status_perkawinan']==3){
	    			$kaw2 = 'Janda';
	    		}elseif($k2['riw_status_perkawinan']==4){
	    			$kaw2 = 'Duda';
	    		}else{
	    			$kaw2 = '-';
	    		}
	    	?>
	    	<tr class="aktifKeluarga{{$k->riw_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$k2)
		    		<td class="alert-danger">{{$k->nik}}</td>
		    		<td class="alert-danger">{{$k->riw_nama}}</td>
		    		<td class="alert-danger">{{$jk}}</td>
		    		<td class="alert-danger">{{$k->riw_tempat_lahir}}, {{($k->riw_tgl_lahir) ? transformDate($k->riw_tgl_lahir) : ''}}</td>
		    		<td class="alert-danger">{{$kaw}}</td>
		    		<td class="alert-danger">{{$stat}}</td>
		    		<td class="alert-danger">{{$k->riw_pendidikan}}</td>
		    		<td class="alert-danger">{{$k->riw_pekerjaan}}</td>
		    		<td class="alert-danger">{{$k->riw_ket}}</td>
	    		@else
	    			@if($k->nik != $k2->nik)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->nik ? $k2->nik : '-'}}">{{ $k->nik ? $k->nik : '-' }}</a></td>
	    			@else
	    				<td>{{$k->nik}}</td>
	    			@endif

	    			@if($k->riw_nama != $k2->riw_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_nama ? $k2->riw_nama : '-'}}">{{ $k->riw_nama ? $k->riw_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_nama}}</td>
	    			@endif

	    			@if($k->riw_kelamin != $k2->riw_kelamin)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$jk2}}">{{ $jk }}</a></td>
	    			@else
	    				<td>{{$jk}}</td>
	    			@endif
	    			
	    			@if($k->riw_tempat_lahir != $k2->riw_tempat_lahir || $k->riw_tgl_lahir != $k2->riw_tgl_lahir)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_tempat_lahir}}, {{$k2->riw_tgl_lahir ? transformDate($k2->riw_tgl_lahir) : ''}}">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</a></td>
	    			@else
	    				<td>{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
	    			@endif
		    		@if($k->riw_status_perkawinan != $k2->riw_status_perkawinan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$kaw2}}">{{ $kaw }}</a></td>
	    			@else
	    				<td>{{$kaw}}</td>
	    			@endif
		    		@if($k->riw_status_tunj != $k2->riw_status_tunj)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$stat2}}">{{ $stat }}</a></td>
	    			@else
	    				<td>{{$stat}}</td>
	    			@endif
		    		@if($k->riw_pendidikan != $k2->riw_pendidikan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pendidikan ? $k2->riw_pendidikan : '-'}}">{{ $k->riw_pendidikan ? $k->riw_pendidikan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pendidikan}}</td>
	    			@endif
		    		@if($k->riw_pekerjaan != $k2->riw_pekerjaan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pekerjaan ? $k2->riw_pekerjaan : '-'}}">{{ $k->riw_pekerjaan ? $k->riw_pekerjaan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pekerjaan}}</td>
	    			@endif
	    			@if($k->riw_ket != $k2->riw_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_ket ? $k2->riw_ket : '-'}}">{{ $k->riw_ket ? $k->riw_ket : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_ket}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-anak" 
					data-nama="{{$k->riw_nama}}" 
					data-nik="{{$k->nik}}" 
					data-id="{{$k->riw_id}}" 
					data-jk="{{$k->riw_kelamin}}" 
					data-tempat-lahir="{{$k->riw_tempat_lahir}}" 
					data-tanggal-lahir="{{$k->riw_tgl_lahir}}"
					data-status-perkawinan="{{$k->riw_status_perkawinan}}"
					data-status-tunjangan="{{$k->riw_status_tunj}}"
					data-pendidikan="{{$k->riw_pendidikan}}"
					data-pekerjaan="{{$k->riw_pekerjaan}}"
					data-keterangan="{{$k->riw_ket}}"
					onclick="editAnak(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-keluarga',array($pegawai->peg_id,$k->riw_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-anak" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelAnak"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/anak')}}" id="anakForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'NIK','required'=>false,'name'=>'riw_nik','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nama','required'=>false,'name'=>'riw_nama','placeholder'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Jenis Kelamin','required'=>false,'name'=>'riw_kelamin','data'=>[
            'L' => 'Laki Laki','P' => 'Perempuan' ]])
			<br>
			@include('form.text2',['label'=>'Tempat Lahir','required'=>false,'name'=>'riw_tempat_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Lahir','required'=>false,'name'=>'riw_tgl_lahir','placeholder'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Status Perkawinan','required'=>false,'name'=>'riw_status_perkawinan','data'=>[
            '1' => 'Kawin','2' => 'Belum Kawin','3'=>'Janda','4'=>'Duda' ]])
			<br>
			@include('form.radio_modal',['label'=>'Memperoleh Tunjangan','required'=>false,'name'=>'riw_status_tunj','data'=>[
            'TRUE' => 'Ya','FALSE' => 'Tidak' ]])
			<br>
			@include('form.text2',['label'=>'Pendidikan','required'=>false,'name'=>'riw_pendidikan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pekerjaan','required'=>false,'name'=>'riw_pekerjaan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Keterangan','required'=>false,'name'=>'riw_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addAnak = function(){
	$("#anakForm").attr("action","{{ URL::to('riwayat/add/anak/') }}");
	$("#labelAnak").text("Tambah Data Anak");
	$(".riw_nama").val("");
	$(".riw_nik").val("");
	$(".riw_kelamin1").prop('checked', false);
	$(".riw_kelamin2").prop('checked', false);
	$(".riw_tgl_lahir").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val("");
	$(".riw_pendidikan").val("");
	$(".riw_ket").val("");
	$(".riw_status_perkawinan1").prop('checked', false);
	$(".riw_status_perkawinan3").prop('checked', false);
	$(".riw_status_perkawinan4").prop('checked', false);
	$(".riw_status_perkawinan5").prop('checked', false);
	$(".riw_status_tunj1").prop('checked', false);
	$(".riw_status_tunj2").prop('checked', false);
	$(".riw_pekerjaan").val("");
}
var editAnak = function(e){
	var dateAr = $(e).data('tanggal-lahir').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var jk = ($(e).data('jk'));
	var sp = ($(e).data('status-perkawinan'));
	var st = ($(e).data('status-tunjangan'));

	$("#anakForm").attr("action","{{ URL::to('riwayat/edit/anak/') }}/"+$(e).data('id'));
	$("#labelAnak").text("Edit Data Anak");
	$(".riw_nama").val($(e).data('nama'));
	$(".riw_nik").val($(e).data('nik'));
	console.log(jk);
	if(jk == 'L'){
		$(".riw_kelamin1").val(jk).prop( "checked", true );
	}else if(jk == 'P'){
		$(".riw_kelamin2").val(jk).prop( "checked", true );
	}
	
	if(sp=='1'){
		$(".riw_status_perkawinan1").val(sp).prop( "checked", true );
	}else if(sp=='2'){
		$(".riw_status_perkawinan2").val(sp).prop( "checked", true );
	}else if(sp=='3'){
		$(".riw_status_perkawinan3").val(sp).prop( "checked", true );
	}else if(sp=='4'){
		$(".riw_status_perkawinan4").val(sp).prop( "checked", true );
	}
	console.log(st);
	if(st=='1'){
		$(".riw_status_tunj1").val(st).prop( "checked", true );
	}else if(st=='2'){
		$(".riw_status_tunj2").val(st).prop( "checked", true );
	}

	$(".riw_tgl_lahir").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val($(e).data('tempat-lahir'));
	$(".riw_pendidikan").val($(e).data('pendidikan'));
	$(".riw_pekerjaan").val($(e).data('pekerjaan'));
	$(".riw_ket").val($(e).data('keterangan'));
}

</script>