<?php

	use App\Model\MatriksAssessment;
	use App\Model\RiwayatAssessment;
	use App\Model\Golongan;
	use App\Model\Pegawai;
	$riw_assessment = RiwayatAssessment::where('peg_id', $pegawai->peg_id)->orderBy('tanggal')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Riwayat Assessment</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 )
		<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-assessment" onclick="addAssessment()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Data</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-assessment" class="table table-striped table-bordered table-hover">
		<input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
			<tr align="center">
				<th>No.</th>
				<th>Tanggal</th>
				<th>Jabatan</th>
				<th>Pangkat/Gol.</th>
			    <th>Kompetensi</th>
			    <th>Potensi</th>
			    <th>Matriks</th>
				<th>Kesimpulan</th>
				<th>Pilihan</th>				
			</tr>			
	     </thead>
	     <tbody>
	    	@foreach ($riw_assessment as $ra)
			<?php
				$gol = Golongan::where('gol_id', $ra->gol_id)->first(); 
				$mat = MatriksAssessment::where('matriks_id', $ra->matriks_id)->first();
			?>	    	
	    	<tr>
	    		<td>{{$i}}</td>
		    		<td>{{$ra->tanggal ? transformDate($ra->tanggal) : ''}}</td>
		    		<td>{{$ra->jabatan}}</td>
		    		<td>{{$gol->nm_pkt}}, {{$gol->nm_gol}}</td>
		    		<td>{{$ra->kompetensi}}</td>
		    		<td>{{$ra->potensi}}</td>
		    		<td>{{$mat->matriks_nm}}</td>
		    		<td>{{$mat->matriks_def}}</td>
	    			<td>
						<button class="btn btn-warning btn-xs"
						data-toggle="modal" data-target="#modal-assessment"
						data-tanggal="{{$ra->tanggal}}"
						data-id="{{$ra->riw_assessment_id}}"
						data-jabatan="{{$ra->jabatan}}"
						data-gol-ruang="{{$ra->gol_id}}"
						data-kompetensi="{{$ra->kompetensi}}"
						data-potensi="{{$ra->potensi}}"
						data-matriks="{{$ra->matriks_id}}"
						onclick="editAssessment(this)">
						<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
						Edit</button>
						<a href="{{url('/riwayat/delete-assessment',array($pegawai->peg_id,$ra->riw_assessment_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
					</td>				
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-assessment" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelAssessment"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/assessment')}}" id="assessmentForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'Tanggal','required'=>true,'name'=>'tanggal_ass','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan','required'=>true,'name'=>'jabatan_ass','placeholder'=>''])
			<br>
			@include('form.select2_modal',['label'=>'Golongan','required'=>true,'name'=>'gol_ass','data'=>
			Golongan::orderBy('gol_id')->lists('nm_gol','gol_id'),'empty'=>'-Pilih-'])	
			<br>
			@include('form.text2',['label'=>'Kompetensi','required'=>true,'name'=>'kompetensi_ass','placeholder'=>''])			
			<br>
			@include('form.text2',['label'=>'Potensi','required'=>true,'name'=>'potensi_ass','placeholder'=>''])
			<br>
			@include('form.select2_modal',['label'=>'Matriks','required'=>true,'name'=>'matriks_ass','data'=>
			MatriksAssessment::orderBy('matriks_id')->lists('matriks_nm','matriks_id'),'empty'=>'-Pilih-'])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitAssessment" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addAssessment = function(){
	$("#assessmentForm").attr("action","{{ URL::to('riwayat/add/assessment/') }}");
	$("#labelAssessment").text("Tambah Data Assessment");
	$(".tanggal_ass").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".jabatan_ass").val("");
	$("#gol_ass").val("").select2();
	$(".kompetensi_ass").val("");
	$(".potensi_ass").val("");
	$("#matriks_ass").val("").select2();	
	$('#assessmentForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

var editAssessment = function(e){
	var dateAr = $(e).data('tanggal').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];

	$("#assessmentForm").attr("action","{{ URL::to('riwayat/edit/assessment/') }}/"+$(e).data('id'));
	$("#labelAssessment").text("Edit Data Assessment");
	$(".tanggal_ass").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".jabatan_ass").val($(e).data('jabatan'));
	$("#gol_ass").val($(e).data('gol-ruang')).select2();
	$(".kompetensi_ass").val($(e).data('kompetensi'));
	$(".potensi_ass").val($(e).data('potensi'));
	$("#matriks_ass").val($(e).data('matriks')).select2();
}

</script>