<?php
	
	use App\Model\RiwayatKeahlian2;
	use App\Model\RiwayatKeahlian;
	use App\Model\RiwayatKeahlianRel2;
	use App\Model\RiwayatKeahlianRel;
	use App\Model\Keahlian;
	use App\Model\KeahlianLevel;
	use App\Model\RiwayatPendidikan2;
	use App\Model\RiwayatPendidikan;
	use App\Model\Universitas;
	use App\Model\TingkatPendidikan;
	use App\Model\RiwayatNonFormal2;
	use App\Model\RiwayatNonFormal;
	use App\Model\RiwayatDiklat2;
	use App\Model\RiwayatDiklat;
	use App\Model\DiklatStruktural;
	use App\Model\DiklatFungsional;
	use App\Model\DiklatTeknis;

	$keahlian = Keahlian::where('spesifik_dari_keahlian_id', 1)->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Kemampuan Bahasa</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit)
		<!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-bahasa" onclick="addBahasa()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		 -->@endif
	</div>
	<table id="tabel-riwayat-bahasa" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Bahasa</th>
			<th colspan="4">Level Keahlian</th>
		    <th rowspan="2">Tanggal Mulai Keahlian</th>
		    <th colspan="3">Pendidikan Penunjang</th>
		    <th rowspan="2">Predikat</th>
			@if(!$submit)
			<!-- <th rowspan="2">Pilihan</th> -->
			@endif
		</tr>
		<tr align="center">
			<th>Membaca</th>
			<th>Mendengarkan</th>
			<th>Menulis</th>
			<th>Berbicara</th>
			<th>Formal</th>
			<th>Non Formal</th>
			<th>Diklat</th>
		</tr>
	     </thead>
	      <tbody>
	      	@foreach($keahlian as $k)
	      	<?php
				$riw_keahlian = RiwayatKeahlian::where('peg_id', $pegawai->peg_id)->where('keahlian_id',$k->keahlian_id)->get();
	      	?>
	      		@foreach($riw_keahlian as $rk)
	      		<?php 
	      			$array = str_split($rk->keahlian_level_id);
	      			if($array[0]!=0){
	      				$lv1 = $array[0].$array[1];
	      			}elseif($array[0] ==0 && $array[1] == 0){
	      				$lv1 = null;
	      			}else{
	      				$lv1 = $array[1];
	      			}

	      			if($array[2]!=0){
	      				$lv2 = $array[2].$array[3];
	      			}elseif($array[2] ==0 && $array[3] == 0){
	      				$lv2 = null;
	      			}else{
	      				$lv2 = $array[3];
	      			}

	      			if($array[4]!=0){
	      				$lv3 = $array[4].$array[5];
	      			}elseif($array[4] ==0 && $array[5] == 0){
	      				$lv3 = null;
	      			}else{
	      				$lv3 = $array[5];
	      			}

	      			if($array[6]!=0){
	      				$lv4 = $array[6].$array[7];
	      			}elseif($array[6] ==0 && $array[7] == 0){
	      				$lv4 = null;
	      			}else{
	      				$lv4 = $array[7];
	      			}

	      			$level1 = KeahlianLevel::where('keahlian_level_id', $lv1)->first();
	      			$level2 = KeahlianLevel::where('keahlian_level_id', $lv2)->first();
	      			$level3 = KeahlianLevel::where('keahlian_level_id', $lv3)->first();
	      			$level4 = KeahlianLevel::where('keahlian_level_id', $lv4)->first();
	      			$rel = RiwayatKeahlianRel::where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
	      			$pend = RiwayatPendidikan::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel['riw_pendidikan_id'])->first();
	      			$univ = Universitas::where('univ_id', $pend['univ_id'])->first();
	      			$pend_non = RiwayatNonFormal::where('peg_id', $pegawai->peg_id)->where('non_id', $rel['non_id'])->first();
	      			$diklat = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel['diklat_id'])->first();
	      			if($diklat){
		      			if($diklat['diklat_jenis']==1){
							$jns = DiklatStruktural::where('kategori_id', $diklat->kategori_id)->first();
							$nm = $jns['kategori_nama'];
						}elseif($diklat['diklat_jenis']==2){
							$jns = DiklatFungsional::where('diklat_fungsional_id', $diklat->diklat_fungsional_id)->first();
							$nm = $jns['diklat_fungsional_nm'];
						}elseif($diklat['diklat_jenis']==3){
							$jns = DiklatTeknis::where('diklat_teknis_id', $diklat->diklat_teknis_id)->first();
							$nm = $jns['diklat_teknis_nm'];
						}
					}else{
						$nm = "";
					}

					$rk2=RiwayatKeahlian2::where('peg_id', $pegawai->peg_id)->where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
					if($rk2){
						$array2 = str_split($rk2->keahlian_level_id);
		      			if($array2[0]!=0){
		      				$lv1b = $array2[0].$array2[1];
		      			}elseif($array2[0] ==0 && $array2[1] == 0){
		      				$lv1b = null;
		      			}else{
		      				$lv1b = $array2[1];
		      			}

		      			if($array2[2]!=0){
		      				$lv2b = $array2[2].$array2[3];
		      			}elseif($array2[2] ==0 && $array2[3] == 0){
		      				$lv2b = null;
		      			}else{
		      				$lv2b = $array2[3];
		      			}

		      			if($array2[4]!=0){
		      				$lv3b = $array2[4].$array2[5];
		      			}elseif($array2[4] ==0 && $array2[5] == 0){
		      				$lv3b = null;
		      			}else{
		      				$lv3b = $array2[5];
		      			}

		      			if($array2[6]!=0){
		      				$lv4b = $array2[6].$array2[7];
		      			}elseif($array2[6] ==0 && $array2[7] == 0){
		      				$lv4b = null;
		      			}else{
		      				$lv4b = $array2[7];
		      			}

		      			$k2 = Keahlian::where('keahlian_id', $rk2->keahlian_id)->first();
		      			$level1b = KeahlianLevel::where('keahlian_level_id', $lv1b)->first();
		      			$level2b = KeahlianLevel::where('keahlian_level_id', $lv2b)->first();
		      			$level3b = KeahlianLevel::where('keahlian_level_id', $lv3b)->first();
		      			$level4b = KeahlianLevel::where('keahlian_level_id', $lv4b)->first();
		      			$rel2 = RiwayatKeahlianRel2::where('riw_keahlian_id', $rk2->riw_keahlian_id)->first();
		      			$pend2 = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel2['riw_pendidikan_id'])->first();
		      			$univ2 = Universitas::where('univ_id', $pend2['univ_id'])->first();
		      			$pend_non2 = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->where('non_id', $rel2['non_id'])->first();
		      			$diklat2 = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel2['diklat_id'])->first();
		      			if($diklat2){
			      			if($diklat2['diklat_jenis']==1){
								$jns2 = DiklatStruktural::where('kategori_id', $diklat2->kategori_id)->first();
								$nm2 = $jns2['kategori_nama'];
							}elseif($diklat2['diklat_jenis']==2){
								$jns2 = DiklatFungsional::where('diklat_fungsional_id', $diklat2->diklat_fungsional_id)->first();
								$nm2 = $jns2['diklat_fungsional_nm'];
							}elseif($diklat2['diklat_jenis']==3){
								$jns2 = DiklatTeknis::where('diklat_teknis_id', $diklat2->diklat_teknis_id)->first();
								$nm2 = $jns2['diklat_teknis_nm'];
							}
						}else{
							$nm2 = "";
						}
					}
	      		?>
	      		<tr>
	      			<td>{{$i}}</td>
	      			@if(!$rk2)
		      			<td class="alert-danger">{{$k->keahlian_nama}}</td>
		      			<td class="alert-danger">{{$level1['keahlian_level_nama']}}</td>
		      			<td class="alert-danger">{{$level2['keahlian_level_nama']}}</td>
		      			<td class="alert-danger">{{$level3['keahlian_level_nama']}}</td>
		      			<td class="alert-danger">{{$level4['keahlian_level_nama']}}</td>
		      			<td class="alert-danger">{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</td>
		      			<td class="alert-danger">{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</td>
		      			<td class="alert-danger">{{$pend_non['non_nama']}}</td>
		      			<td class="alert-danger">{{$nm}}</td>
		      			<td class="alert-danger">{{$rel['predikat']}}</td>
	      			@else
	      				@if($rk->keahlian_id != $rk2->keahlian_id)
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->keahlian_nama ? $k2->keahlian_nama : '-'}}">{{$k->keahlian_nama ? $k->keahlian_nama : '-'}}</a></td>
		      			@else
		      				<td>{{$k->keahlian_nama}}</td>
		      			@endif
		      			@if($level1['keahlian_level_id'] != $level1b['keahlian_level_id'] )
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$level1b['keahlian_level_nama'] ? $level1b['keahlian_level_nama'] : '-'}}">{{$level1['keahlian_level_nama'] ? $level1['keahlian_level_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$level1['keahlian_level_nama']}}</td>
		      			@endif
		      			
		      			@if($level2['keahlian_level_id'] != $level2b['keahlian_level_id'] )
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$level2b['keahlian_level_nama'] ? $level2b['keahlian_level_nama'] : '-'}}">{{$level2['keahlian_level_nama'] ? $level2['keahlian_level_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$level2['keahlian_level_nama']}}</td>
		      			@endif

		      			@if($level3['keahlian_level_id'] != $level3b['keahlian_level_id'] )
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$level3b['keahlian_level_nama'] ? $level3b['keahlian_level_nama'] : '-'}}">{{$level3['keahlian_level_nama'] ? $level3['keahlian_level_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$level3['keahlian_level_nama']}}</td>
		      			@endif
		      			
		      			@if($level4['keahlian_level_id'] != $level4b['keahlian_level_id'] )
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$level4b['keahlian_level_nama'] ? $level4b['keahlian_level_nama'] : '-'}}">{{$level4['keahlian_level_nama'] ? $level4['keahlian_level_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$level4['keahlian_level_nama']}}</td>
		      			@endif

		      			@if($rk->riw_keahlian_sejak != $rk2->riw_keahlian_sejak)
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rk2->riw_keahlian_sejak ? transformDate($rk2->riw_keahlian_sejak) : '-'}}">{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</a></td>
		      			@else
		      				<td>{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</td>
		      			@endif
		      			
		      			@if($rel['riw_pendidikan_id'] != $rel2['riw_pendidikan_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pend2['riw_pendidikan_nm']}}{{$univ2['univ_nmpti'] ? $univ2['univ_nmpti'] : '-' }}">{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</a></td>
		      			@else
		      				<td>{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</td>
		      			@endif

		      			@if($rel['non_id'] != $rel2['non_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pend_non2['non_nama'] ? $pend_non2['non_nama'] : '-'}}">{{$pend_non['non_nama'] ? $pend_non['non_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$pend_non['non_nama']}}</td>
		      			@endif

		      			@if($rel['diklat_id'] != $rel2['diklat_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$nm2}}">{{$nm}}</a></td>
		      			@else
		      				<td>{{$nm}}</td>
		      			@endif
		      			
		      			@if($rel['predikat'] != $rel2['predikat'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rel2['predikat'] ? $rel2['predikat'] : '-'}}">{{$rel['predikat'] ? $rel['predikat'] : '-'}}</a></td>
		      			@else
		      				<td>{{$rel['predikat']}}</td>
		      			@endif
	      			@endif
	      			@if(!$submit)
	      			<!-- <td>
		    			<button class="btn btn-warning btn-xs"
						data-toggle="modal" data-target="#modal-bahasa" 
						data-id-keahlian="{{$rk->keahlian_id}}" 
						data-id="{{$rk->riw_keahlian_id}}"
						data-id-level1="{{$level1['keahlian_level_id']}}"
						data-id-level2="{{$level2['keahlian_level_id']}}"
						data-id-level3="{{$level3['keahlian_level_id']}}"
						data-id-level4="{{$level4['keahlian_level_id']}}"
						data-tanggal="{{$rk->riw_keahlian_sejak}}"
						data-id-pendidikan="{{$rel['riw_pendidikan_id']}}"
						data-id-non="{{$rel['non_id']}}"
						data-id-diklat="{{$rel['diklat_id']}}"
						data-predikat="{{$rel['predikat']}}"
						onclick="editBahasa(this)">
						<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
						Edit</button>
			    		<a href="{{url('/riwayat/delete-keahlian',array($pegawai->peg_id,$rk->riw_keahlian_id),false)}}" 
			    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
								Delete
						</a>
					</td> -->
					@endif
				</tr>
				<?php $i++;?>
	      		@endforeach
	      	@endforeach
	      </tbody>
	</table>
</div>
<div class="modal fade" id="modal-bahasa" tabindex="-1" role="dialog" aria-labelledby="modal-bahasa" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/keahlian')}}" id="bahasaForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Bahasa','required'=>false,'name'=>'keahlian_id_bahasa','data'=>
          	Keahlian::with('allChildKeahlian')->where('spesifik_dari_keahlian_id',1)->lists('keahlian_nama','keahlian_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Level Keahlian Membaca','required'=>false,'name'=>'keahlian_level_id_1','data'=>
          	KeahlianLevel::where('keahlian_id', 1)->where('keahlian_level_nama', 'like', '%Membaca%')->lists('keahlian_level_nama','keahlian_level_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Level Keahlian Mendengarkan','required'=>false,'name'=>'keahlian_level_id_2','data'=>
          	KeahlianLevel::where('keahlian_id', 1)->where('keahlian_level_nama', 'like', '%Mendengarkan%')->lists('keahlian_level_nama','keahlian_level_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Level Keahlian Menulis','required'=>false,'name'=>'keahlian_level_id_3','data'=>
          	KeahlianLevel::where('keahlian_id', 1)->where('keahlian_level_nama', 'like', '%Menulis%')->lists('keahlian_level_nama','keahlian_level_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Level Keahlian Berbicara','required'=>false,'name'=>'keahlian_level_id_4','data'=>
          	KeahlianLevel::where('keahlian_id', 1)->where('keahlian_level_nama', 'like', '%Berbicara%')->lists('keahlian_level_nama','keahlian_level_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.text2',['label'=>'Tanggal Mulai Keahlian','required'=>false,'name'=>'riw_keahlian_sejak_bahasa','placeholder'=>''])
			<br>
			<div class="row" id="riw_pendidikan_id_container">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">Pendidikan Penunjang (Formal)</label>
						<div class="col-md-8">
							<select class="form-control select2me" id="riw_pendidikan_id_bahasa" name="riw_pendidikan_id_bahasa" style="border:none; margin-left:-13px; width:578px">
								<?php
									$data = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->orderBy('tingpend_id')->get(); 
								?>
								<option value="">-Pilih-</option>
								@foreach ($data as $key)
									<?php
										$tp= TingkatPendidikan::where('tingpend_id', $key['tingpend_id'])->first(); 
										$universitas = Universitas::where('univ_id', $key['univ_id'])->first();
									?>
								<option value="{{$key['riw_pendidikan_id']}}">{{$tp->nm_tingpend}}-{{$key['riw_pendidikan_nm']}}{{$universitas['univ_nmpti']}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
          	<br>
          	@include('form.select2_modal',['label'=>'Pendidikan Penunjang (Non Formal)','required'=>false,'name'=>'non_id_bahasa','data'=>
          	RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->lists('non_nama','non_id') ,'empty'=>'-Pilih-'])
          	<br>
          	<div class="row" id="diklat_id_container">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">Pendidikan Penunjang (Diklat)</label>
						<div class="col-md-8">
							<select class="form-control select2me" id="diklat_id_bahasa" name="diklat_id_bahasa" style="border:none; margin-left:-13px; width:578px">
								<?php
									$riw_diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->get(); 
								?>
								<option value="">-Pilih-</option>
								@foreach ($riw_diklat as $rd)
									<?php
										if($rd->diklat_jenis==1){
											$jenis = DiklatStruktural::where('kategori_id', $rd->kategori_id)->first();
											$nama = $jenis['kategori_nama'];
										}elseif($rd->diklat_jenis==2){
											$jenis = DiklatFungsional::where('diklat_fungsional_id', $rd->diklat_fungsional_id)->first();
											$nama = $jenis['diklat_fungsional_nm'];
										}elseif($rd->diklat_jenis==3){
											$jenis = DiklatTeknis::where('diklat_teknis_id', $rd->diklat_teknis_id)->first();
											$nama = $jenis['diklat_teknis_nm'];
										}
									?>
								<option value="{{$rd['diklat_id']}}">{{$nama}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
          	<br>
          	@include('form.text2',['label'=>'Predikat','required'=>false,'name'=>'predikat_bahasa','placeholder'=>''])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitKeahlian" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addBahasa = function(){
	$("#bahasaForm").attr("action","{{ URL::to('riwayat/add/bahasa/') }}");
	$("#labelBahasa").text("Tambah Riwayat Bahasa");

	$("#keahlian_id_bahasa").val("").select2();
	$("#keahlian_level_id_1").val("").select2();
	$("#keahlian_level_id_2").val("").select2();
	$("#keahlian_level_id_3").val("").select2();
	$("#keahlian_level_id_4").val("").select2();
	$(".riw_keahlian_sejak_bahasa").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_pendidikan_id_bahasa").val("").select2();
	$("#non_id_bahasa").val("").select2();
	$("#diklat_id_bahasa").val("").select2();
	$(".predikat_bahasa").val("");
}

var editBahasa= function(e){
	var dateAr = $(e).data('tanggal').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	$("#bahasaForm").attr("action","{{ URL::to('riwayat/edit/bahasa/') }}/"+$(e).data('id'));
	$("#labelBahasa").text("Edit Riwayat Bahasa");

	$("#keahlian_id_bahasa").val($(e).data('id-keahlian')).select2();
	$("#keahlian_level_id_1").val($(e).data('id-level1')).select2();
	$("#keahlian_level_id_2").val($(e).data('id-level2')).select2();
	$("#keahlian_level_id_3").val($(e).data('id-level3')).select2();
	$("#keahlian_level_id_4").val($(e).data('id-level4')).select2();
	$(".riw_keahlian_sejak_bahasa").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_pendidikan_id_bahasa").val($(e).data('id-pendidikan')).select2();
	$("#non_id_bahasa").val($(e).data('id-non')).select2();
	$("#diklat_id_bahasa").val($(e).data('id-diklat')).select2();
	$(".predikat_bahasa").val($(e).data('predikat'));
}

</script>