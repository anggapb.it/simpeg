<?php

	use App\Model\RiwayatCuti;

	$struk = RiwayatCuti::leftJoin('m_spg_jenis_cuti','m_spg_jenis_cuti.jeniscuti_id','=','spg_riwayat_cuti.jeniscuti_id')->where('peg_id',$pegawai->peg_id)->get();
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Cuti Pegawai</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-cuti" onclick="addCuti()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
		    <th>Jenis Cuti</th>
		    <th>No. Surat</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
			<th>Keterangan</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Aksi</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($struk as $s)
	      	<tr>
	      		<td>{{$no++}}</td>
	      		<td>{{$s->jeniscuti_nm}}</td>
	      		<td>{{$s->cuti_no}}</td>
	      		<td>{{getFullDate($s->cuti_mulai)}}</td>
	      		<td>{{getFullDate($s->cuti_selesai)}}</td>
	      		<td>{{$s->cuti_ket}}</td>
				@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-cuti" 
					data-id="{{$s->cuti_id}}" 
					data-mulai="{{$s->cuti_mulai}}" 
					data-selesai="{{$s->cuti_selesai}}" 
					data-jenis="{{$s->jeniscuti_id}}"
					data-nosurat="{{$s->cuti_no}}"
					data-tanggal="{{$s->cuti_tgl}}"
					data-ket="{{$s->cuti_ket}}"
					onclick="editCuti(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-cuti',$s->peg_id).'/'.$s->cuti_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Riwayat Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
				@endif
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-cuti" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelCuti"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/add/cuti')}}" id="formCuti">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Jenis Cuti','required'=>false,'name'=>'riw_jenis_cuti','data'=>\DB::connection('pgsql2')->table('m_spg_jenis_cuti')->lists('jeniscuti_nm','jeniscuti_id'),'empty'=>''])
			<br>
			@include('form.text2',['label'=>'No. Surat','required'=>false,'name'=>'riw_cuti_surat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Surat','required'=>false,'name'=>'riw_cuti_tanggal','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Mulai Cuti','required'=>false,'name'=>'riw_cuti_mulai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Selesai Cuti','required'=>false,'name'=>'riw_cuti_selesai','placeholder'=>''])
			<br>
			@include('form.textarea',['label'=>'Keterangan Cuti','required'=>false,'name'=>'riw_cuti_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

var addCuti = function(){
	$("#formCuti").attr("action","{{ URL::to('riwayat/add/cuti/') }}");
	$("#labelCuti").html("Tambah Riwayat Cuti");
	$("#riw_cuti_ket").val("");
	$("#riw_cuti_surat").val("");
	$("#riw_jenis_cuti").val("");
	$("#riw_cuti_mulai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_cuti_selesai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_cuti_tanggal").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_jenis_cuti").select2({
		placeholder:'Pilih Jenis Cuti'
	});
}
var editCuti = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formCuti").attr("action","{{ URL::to('riwayat/add/cuti/') }}");
	}else{
		$("#formCuti").attr("action","{{ URL::to('riwayat/edit/cuti/') }}/"+id);
	}

	$("#labelCuti").html("Edit Riwayat Cuti");
	$("#riw_cuti_ket").val($(e).data('ket'));
	$("#riw_cuti_surat").val($(e).data('nosurat'));
	$("#riw_nama_jabatan").val($(e).data('nama'));
	$("#riw_jenis_cuti").val($(e).data('jenis'));
	$("#riw_cuti_mulai").val($(e).data('mulai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_cuti_selesai").val($(e).data('selesai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_cuti_tanggal").val($(e).data('tanggal')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_jenis_cuti").select2({
		placeholder:'Pilih Jenis Cuti'
	});

}
</script>
