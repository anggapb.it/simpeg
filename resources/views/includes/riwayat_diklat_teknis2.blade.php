<?php
	use App\Model\RiwayatDiklat2;
	use App\Model\RiwayatDiklat;
	use App\Model\DiklatTeknis;

	$riwayat_diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_jenis', 3)->orderBy('diklat_selesai')->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Diklat Teknis</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-diklat-teknis" onclick="addDiklatTeknis()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-diklat-teknis" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Diklat Teknis</th>
			<th colspan="2">Tanggal</th>
		    <th rowspan="2">Jmlh Jam</th>
		    <th colspan="3">STTP</th>
		    <th colspan="2">Instansi Penyelengggara</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
			<th>Mulai</th>
			<th>Selesai</th>
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Jabatan<br/>Penandatangan</th>
		    <th>Instansi</th>
		    <th>Lokasi</th>
		</tr>
	     </thead>
	      <tbody>
	    	@foreach ($riwayat_diklat as $rd)
	    	<?php $nama_diklat = DiklatTeknis::where('diklat_teknis_id', $rd->diklat_teknis_id)->first(); 
	    		$rd2 = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rd->diklat_id)->where('diklat_jenis', 3)->orderBy('diklat_selesai')->first();
	    		if (!$nama_diklat) $nama_diklat = new DiklatTeknis;
				
				if($rd2){
	    			$nama_diklat2 = DiklatTeknis::where('diklat_teknis_id', $rd2->diklat_teknis_id)->first(); 
	    			if (!$nama_diklat2) $nama_diklat2 = new DiklatTeknis;
	    		}
	    	?>
	    	<tr class="aktifDiklat{{$rd->diklat_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$rd2)
	    		<td class="alert-danger">{{$nama_diklat->diklat_teknis_nm}}</td>
	    		<td class="alert-danger">{{$rd->diklat_mulai ? transformDate($rd->diklat_mulai) : ''}}</td>
	    		<td class="alert-danger">{{$rd->diklat_selesai ? transformDate($rd->diklat_selesai) : ''}}</td>
	    		<td class="alert-danger">{{$rd->diklat_jumlah_jam}}</td>
	    		<td class="alert-danger">{{$rd->diklat_sttp_no}}</td>
	    		<td class="alert-danger">{{$rd->diklat_sttp_tgl ? transformDate($rd->diklat_sttp_tgl) : ''}}</td>
	    		<td class="alert-danger">{{$rd->diklat_sttp_pej}}</td>
	    		<td class="alert-danger">{{$rd->diklat_penyelenggara}}</td>
	    		<td class="alert-danger">{{$rd->diklat_tempat}}</td>
	    		@else
	    			@if($rd->diklat_teknis_id != $rd2->diklat_teknis_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$nama_diklat2['diklat_fungsional_nm'] ? $nama_diklat2['diklat_fungsional_nm'] : '-'}}">{{ $nama_diklat->diklat_fungsional_nm ? $nama_diklat->diklat_fungsional_nm : '-' }}</a></td>
	    			@else
	    				<td>{{$nama_diklat->diklat_teknis_nm}}</td>
	    			@endif
	    			@if($rd->diklat_mulai != $rd2->diklat_mulai)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_mulai ? transformDate($rd2->diklat_mulai) : '-'}}">{{ $rd->diklat_mulai ? transformDate($rd->diklat_mulai) : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_mulai ? transformDate($rd->diklat_mulai) : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_selesai != $rd2->diklat_selesai)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_selesai ? transformDate($rd2->diklat_selesai) : '-'}}">{{ $rd->diklat_selesai ? transformDate($rd->diklat_selesai) : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_selesai ? transformDate($rd->diklat_selesai) : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_jumlah_jam != $rd2->diklat_jumlah_jam)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_jumlah_jam ? $rd2->diklat_jumlah_jam : '-'}}">{{ $rd->diklat_jumlah_jam ? $rd->diklat_jumlah_jam : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_jumlah_jam ? $rd->diklat_jumlah_jam : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_sttp_no != $rd2->diklat_sttp_no)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_sttp_no ? $rd2->diklat_sttp_no : '-'}}">{{ $rd->diklat_sttp_no ? $rd->diklat_sttp_no : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_sttp_no ? $rd->diklat_sttp_no : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_sttp_tgl != $rd2->diklat_sttp_tgl)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_sttp_tgl ? transformDate($rd2->diklat_sttp_tgl) : '-'}}">{{ $rd->diklat_sttp_tgl ? transformDate($rd->diklat_sttp_tgl) : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_sttp_tgl ? transformDate($rd->diklat_sttp_tgl) : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_sttp_pej != $rd2->diklat_sttp_pej)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_sttp_pej ? $rd2->diklat_sttp_pej : '-'}}">{{ $rd->diklat_sttp_pej ? $rd->diklat_sttp_pej : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_sttp_pej ? $rd->diklat_sttp_pej : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_penyelenggara != $rd2->diklat_penyelenggara)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_penyelenggara ? $rd2->diklat_penyelenggara : '-'}}">{{ $rd->diklat_penyelenggara ? $rd->diklat_penyelenggara : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_penyelenggara ? $rd->diklat_penyelenggara : '-'}}</td>
		    		@endif

		    		@if($rd->diklat_tempat != $rd2->diklat_tempat)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rd2->diklat_tempat ? $rd2->diklat_tempat : '-'}}">{{ $rd->diklat_tempat ? $rd->diklat_tempat : '-'}}</a></td>
		    		@else
		    			<td>{{$rd->diklat_tempat ? $rd->diklat_tempat : '-'}}</td>
		    		@endif
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-diklat-teknis" 
					data-nama="{{$rd->diklat_teknis_id}}" 
					data-id="{{$rd->diklat_id}}" 
					data-diklat-mulai="{{$rd->diklat_mulai}}" 
					data-diklat-selesai="{{$rd->diklat_selesai}}" 
					data-diklat-jam="{{$rd->diklat_jumlah_jam}}"
					data-diklat-no="{{$rd->diklat_sttp_no}}"
					data-diklat-tgl="{{$rd->diklat_sttp_tgl}}"
					data-diklat-pejabat="{{$rd->diklat_sttp_pej}}"
					data-diklat-penyelenggara="{{$rd->diklat_penyelenggara}}"
					data-diklat-tempat="{{$rd->diklat_tempat}}"
					onclick="editDiklatTeknis(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-diklat',array($pegawai->peg_id,$rd->diklat_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>	
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-diklat-teknis" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelDiklatT"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/diklat-teknis')}}" id="diklatTeknisForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Nama Diklat','required'=>true,'name'=>'diklat_teknis_id','data'=>
          	DiklatTeknis::orderBy('diklat_teknis_nm')->lists('diklat_teknis_nm','diklat_teknis_id'),'empty'=>'-Pilih-'])
          	<br><br>
			@include('form.text2',['label'=>'Tanggal Mulai','required'=>false,'name'=>'diklat_mulai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Selesai','required'=>false,'name'=>'diklat_selesai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jumlah Jam','required'=>true,'name'=>'diklat_jumlah_jam','placeholder'=>''])
			<div class="row" >
				<div class="col-md-12">
					<label class="control-label col-md-4"></label>
					<div class="col-md-8">
						Nasional : <label id="dikteknasional" class="dikteknasional"></label> Internasional : <label id="diktekinternasional" class="diktekinternasional"></label>
					</div>
				</div>
			</div>
			<br>
			@include('form.text2',['label'=>'No. STTP','required'=>false,'name'=>'diklat_sttp_no','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal STTP','required'=>false,'name'=>'diklat_sttp_tgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan STTP','required'=>false,'name'=>'diklat_sttp_pej','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Penyelenggara Diklat','required'=>false,'name'=>'diklat_penyelenggara','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tempat Diklat','required'=>false,'name'=>'diklat_tempat','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<input type="hidden" name="diklat_jenis" value="3" class="diklat_jenis">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addDiklatTeknis = function(){
	$("#diklatTeknisForm").attr("action","{{ URL::to('riwayat/add/diklat-teknis/') }}");
	$("#labelDiklatT").text("Tambah Data Diklat Teknis");
	$("#diklat_teknis_id").val("").select2();
	$(".diklat_mulai").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_selesai").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_jumlah_jam").val("");
	$(".diklat_sttp_no").val("");
	$(".diklat_sttp_tgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_sttp_pej").val("");
	$(".diklat_penyelenggara").val("");
	$(".diklat_tempat").val("");
	$('#dikteknasional').text('');
	$('#diktekinternasional').text('');
	$(".diklat_jumlah_jam").on("keydown", function (e) {
		numberOnly(e);
	}); 
	$('#diklatTeknisForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
	$("#diklat_teknis_id").change(function() {
	var id = $("#diklat_teknis_id").val();
	var jam= 0;
	  $.ajax({
			type: "GET",
			cache: false,
			url: "{{ URL::to('riwayat/getjam') }}",
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');
				if(token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			data: {"jenis":2, "id":id},
			dataType: 'json',
			success: function(data) {
				jam = data.diklat_teknis_standar_jp;
				$(".diklat_jumlah_jam").val(jam);
				$('#dikteknasional').text(data.nasional);
				$('#diktekinternasional').text(data.internasional);
				$(".diklat_jumlah_jam").on("keydown", function (e) {
					numberOnly(e);
				}); 
				console.log(data.nasional);
			},
			error: function (e){
				console.log(e);
				alert("ERROR!");
			},
		});
	});
}
	


var editDiklatTeknis = function(e){
	var dateAr = $(e).data('diklat-mulai').split('-');
	var dateAr2 = $(e).data('diklat-selesai').split('-');
	var dateAr3 = $(e).data('diklat-tgl').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];
	var newDate3 = dateAr3[2] + '-' + dateAr3[1] + '-' + dateAr3[0];
	var id = $("#diklat_teknis_id").val();
	  $.ajax({
			type: "GET",
			cache: false,
			url: "{{ URL::to('riwayat/getjam') }}",
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');
				if(token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			data: {"jenis":2, "id":id},
			dataType: 'json',
			success: function(data) {
				$('#dikteknasional').text(data.nasional);
				$('#diktekinternasional').text(data.internasional);
				
				console.log(data.nasional);
			},
			error: function (e){
				console.log(e);
				alert("ERROR!");
			},
		});

	console.log($(e).data('nama'));

	$("#diklatTeknisForm").attr("action","{{ URL::to('riwayat/edit/diklat-teknis/') }}/"+$(e).data('id'));
	$("#labelDiklatT").text("Edit Data Diklat Teknis");
	$("#diklat_teknis_id").val($(e).data('nama')).select2();
	$(".diklat_mulai").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_selesai").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_jumlah_jam").val($(e).data('diklat-jam'));
	$(".diklat_sttp_no").val($(e).data('diklat-no'));
	$(".diklat_sttp_tgl").val(newDate3).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".diklat_sttp_pej").val($(e).data('diklat-pejabat'));
	$(".diklat_penyelenggara").val($(e).data('diklat-penyelenggara'));
	$(".diklat_tempat").val($(e).data('diklat-tempat'));
	
	$(".diklat_jumlah_jam").on("keydown", function (e) {
		numberOnly(e);
	}); 
	$('#diklatTeknisForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
	$("#diklat_teknis_id").change(function() {
	var id = $("#diklat_teknis_id").val();
	var jam= 0;
	  $.ajax({
			type: "GET",
			cache: false,
			url: "{{ URL::to('riwayat/getjam') }}",
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');
				if(token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			data: {"jenis":2, "id":id},
			dataType: 'json',
			success: function(data) {
				jam = data.diklat_teknis_standar_jp;
				$(".diklat_jumlah_jam").val(jam);
				$(".diklat_jumlah_jam").on("keydown", function (e) {
					numberOnly(e);
				}); 
				$('#dikteknasional').text(data.nasional);
				$('#diktekinternasional').text(data.internasional);
				console.log(jam);
			},
			error: function (e){
				console.log(e);
				alert("ERROR!");
			},
		});
	});
}

</script>