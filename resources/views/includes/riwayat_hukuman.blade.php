<?php

	use App\Model\RiwayatHukuman2;
	use App\Model\RiwayatHukuman;
	use App\Model\Hukum;
	use App\Model\HukumKategori;
	$hukuman = RiwayatHukuman::where('peg_id', $pegawai->peg_id)->orderBy('riw_hukum_tmt')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Riwayat Hukuman</h3><br>
	<div class="pull-right tableTools-container">
	</div>
	<table id="tabel-riwayat-hukuman" class="table table-striped table-bordered table-hover">
		<input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
			<tr align="center">
				<th rowspan="2">No.</th>
				<th rowspan="2">Kategori Hukuman</th>
				<th rowspan="2">Nama Hukuman</th>
			    <th colspan="2">SK</th>
			    <th colspan="2">Lama</th>
			    <th rowspan="2">Keterangan Pelanggaran</th>
			</tr>
			<tr align="center">
				<th>No SK</th>
				<th>Tanggal SK</th>
				<th>Tanggal Mulai</th>
				<th>Tanggal Selesai</th>
			</tr>
	     </thead>
	     <tbody>
	    	@foreach ($hukuman as $h)
	    	<?php
	    		$h2 = RiwayatHukuman2::where('peg_id', $pegawai->peg_id)->where('riw_hukum_id', $h->riw_hukum_id)->orderBy('riw_hukum_tmt')->first();
	    		$nm_hukuman = Hukum::where('mhukum_id', $h->mhukum_id)->first();
	    		$nm_hukuman2 = Hukum::where('mhukum_id', $h2['mhukum_id'])->first();
	    		$kat_hukuman = HukumKategori::where('mhukum_cat_id', $nm_hukuman->mhukum_cat_id)->first();
	    		$kat_hukuman2 = HukumKategori::where('mhukum_cat_id', $nm_hukuman2['mhukum_cat_id'])->first();
	    		if (!$nm_hukuman) $nm_hukuman = (object) [];
	    		if (!$nm_hukuman2) $nm_hukuman2 = (object) [];
	    		if (!$kat_hukuman) $kat_hukuman = (object) [];
	    		if (!$kat_hukuman2) $kat_hukuman2 = (object) [];
	    	?>
	    	<tr class="aktifHukuman{{$h->riw_hukum_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$h2)
		    		<td class="alert-danger">{{$kat_hukuman ? $kat_hukuman->mhukum_cat_nm : ''}}</td>
		    		<td class="alert-danger">{{$nm_hukuman ? $nm_hukuman->mhukum_hukuman : ''}}</td>
		    		<td class="alert-danger">{{$h->riw_hukum_sk}}</td>
		    		<td class="alert-danger">{{$h->riw_hukum_tgl ? transformDate($h->riw_hukum_tgl) : ''}}</td>
		    		<td class="alert-danger">{{$h->riw_hukum_tmt ? transformDate($h->riw_hukum_tmt) : ''}}</td>
		    		<td class="alert-danger">{{$h->riw_hukum_sd ? transformDate($h->riw_hukum_sd) : ''}}</td>
		    		<td class="alert-danger">{{$h->riw_hukum_ket}}</td>
	    		@else
		    		@if($kat_hukuman->mhukum_cat_nm != $kat_hukuman2->mhukum_cat_nm)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$kat_hukuman2->mhukum_cat_nm ? $kat_hukuman2->mhukum_cat_nm : '-'}}">{{ $kat_hukuman->mhukum_cat_nm ? $kat_hukuman->mhukum_cat_nm : '-' }}</a></td>
	    			@else
	    				<td>{{$kat_hukuman->mhukum_cat_nm}}</td>
	    			@endif

	    			@if($nm_hukuman->mhukum_hukuman != $nm_hukuman2->mhukum_hukuman)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$nm_hukuman2->mhukum_hukuman ? $nm_hukuman2->mhukum_hukuman : '-'}}">{{ $nm_hukuman->mhukum_hukuman }}</a></td>
	    			@else
	    				<td>{{$nm_hukuman->mhukum_hukuman}}</td>
	    			@endif
	    			
	    			@if($h->riw_hukum_sk != $h2->riw_hukum_sk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$h2->riw_hukum_sk ? $h2->riw_hukum_sk : '-'}}">{{$h->riw_hukum_sk}}</a></td>
	    			@else
	    				<td>{{$h->riw_hukum_sk}}</td>
	    			@endif
		    		@if($h->riw_hukum_tgl != $h2->riw_hukum_tgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$h2->riw_hukum_tgl ? $h2->riw_hukum_tgl : '-'}}">{{ $h->riw_hukum_tgl ? $h->riw_hukum_tgl : '-' }}</a></td>
	    			@else
	    				<td>{{$h->riw_hukum_tgl}}</td>
	    			@endif
		    		@if($h->riw_hukum_tmt != $h2->riw_hukum_tmt)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$h2->riw_hukum_tmt ? $h2->riw_hukum_tmt : '-'}}">{{ $h->riw_hukum_tmt ? $h->riw_hukum_tmt : '-' }}</a></td>
	    			@else
	    				<td>{{$h->riw_hukum_tmt}}</td>
	    			@endif
	    			@if($h->riw_hukum_sd != $h2->riw_hukum_sd)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$h2->riw_hukum_sd ? $h2->riw_hukum_sd : '-'}}">{{ $h->riw_hukum_sd ? $h->riw_hukum_sd : '-' }}</a></td>
	    			@else
	    				<td>{{$h->riw_hukum_sd}}</td>
	    			@endif
	    			@if($h->riw_hukum_ket != $h2->riw_hukum_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$h2->riw_hukum_ket ? $h2->riw_hukum_ket : '-'}}">{{ $h->riw_hukum_ket ? $h->riw_hukum_ket : '-' }}</a></td>
	    			@else
	    				<td>{{$h->riw_hukum_ket}}</td>
	    			@endif
	    		@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
</div>

<div class="modal fade" id="modal-hukuman" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelHukuman"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/hukuman')}}" id="hukumanForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Nama Hukuman','required'=>false,'name'=>'mhukum_id','data'=>
          	Hukum::orderBy('mhukum_cat_id')->orderBy('mhukum_hukuman')->lists('mhukum_hukuman','mhukum_id') ,'empty'=>'-Pilih-'])
          	<br>
			@include('form.text2',['label'=>'No SK','required'=>false,'name'=>'riw_hukum_sk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_hukum_tgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Mulai (TMT)','required'=>false,'name'=>'riw_hukum_tmt','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Selesai','required'=>false,'name'=>'riw_hukum_sd','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Keterangan Pelanggaran','required'=>false,'name'=>'riw_hukum_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitHukuman" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addHukuman = function(){
	$("#hukumanForm").attr("action","{{ URL::to('riwayat/add/hukuman/') }}");
	$("#labelHukuman").text("Tambah Data Hukuman");
	$("#mhukum_id").val("");
	$(".riw_hukum_sk").val("");
	$(".riw_hukum_tgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_hukum_tmt").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_hukum_sd").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_hukum_ket").val("");
}
var editHukuman = function(e){
	var dateAr = $(e).data('tgl-sk').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	
	var dateAr2 = $(e).data('tmt').split('-');
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];

	var dateAr3 = $(e).data('sd').split('-');
	var newDate3 = dateAr3[2] + '-' + dateAr3[1] + '-' + dateAr3[0];

	$("#hukumanForm").attr("action","{{ URL::to('riwayat/edit/hukuman/') }}/"+$(e).data('id'));
	$("#labelHukuman").text("Edit Data Hukuman");

	$("#mhukum_id").val($(e).data('nama')).select2();

	$(".riw_hukum_tgl").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_hukum_tmt").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_hukum_sd").val(newDate3).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});

	$(".riw_hukum_sk").val($(e).data('sk'));
	$(".riw_hukum_ket").val($(e).data('keterangan'));
}

</script>