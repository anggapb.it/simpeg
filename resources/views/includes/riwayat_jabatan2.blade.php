<?php
	use App\Model\RiwayatJabatan2;
	use App\Model\RiwayatJabatan;
	use App\Model\Golongan;

	$riwayat_jabatan = RiwayatJabatan2::where('peg_id', $pegawai->peg_id)->orderBy('riw_jabatan_tmt')->get();
	$i=1;
?>
<div class="col-xs-12">
	<h3 align="middle">Riwayat Jabatan</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-jabatan" onclick="addJabatan()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-jabatan" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	    <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Jabatan</th>
			<th rowspan="2">Gol.<br/>Ruang</th>
			<th colspan="3">Surat Keputusan</th>
			<th rowspan="2">TMT</th>
			<th rowspan="2">Unit Kerja</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Jabatan Penandatangan</th>
		</tr>
	     </thead>
	    <tbody>
	    	@foreach ($riwayat_jabatan as $rj)
	    	<?php 
	    		$rj2= RiwayatJabatan::where('peg_id', $pegawai->peg_id)->where('riw_jabatan_id', $rj->riw_jabatan_id)->first();
	    		$gol = Golongan::where('gol_id', $rj->gol_id)->first(); 

	    		if($rj2){
	    			$gol2 = Golongan::where('gol_id', $rj2->gol_id)->first(); 
	    		}	
	    	?>
	    	<tr class="aktifJabatan{{$rj->riw_jabatan_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$rj2)
	    		<td class="alert-danger">{{$rj->riw_jabatan_nm}}</td>
	    		<td class="alert-danger">{{$gol['nm_gol']}}</td>
	    		<td class="alert-danger">{{$rj->riw_jabatan_no}}</td>
	    		<td class="alert-danger">{{$rj->riw_jabatan_tgl ? transformDate($rj->riw_jabatan_tgl) : ''}}</td>
	    		<td class="alert-danger">{{$rj->riw_jabatan_pejabat}}</td>
	    		<td class="alert-danger">{{$rj->riw_jabatan_tmt ? transformDate($rj->riw_jabatan_tmt) : ''}}</td>
	    		<td class="alert-danger">{{$rj->riw_jabatan_unit}}</td>
	    		@else
	    			@if($rj->riw_jabatan_nm != $rj2->riw_jabatan_nm)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_nm ? $rj2->riw_jabatan_nm : '-'}}">{{ $rj->riw_jabatan_nm ? $rj->riw_jabatan_nm : '-' }}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_nm}}</td>
	    			@endif
	    			@if($rj->gol_id != $rj2->gol_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$gol2['nm_gol'] ? $gol2['nm_gol'] : '-'}}">{{ $gol['nm_gol'] ? $gol['nm_gol'] : '-' }}</a></td>
	    			@else
	    				<td>{{$gol['nm_gol']}}</td>
	    			@endif
	    			@if($rj->riw_jabatan_no != $rj2->riw_jabatan_no)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_no ? $rj2->riw_jabatan_no : '-'}}">{{ $rj->riw_jabatan_no ? $rj->riw_jabatan_no : '-' }}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_no}}</td>
	    			@endif

	    			@if($rj->riw_jabatan_tgl != $rj2->riw_jabatan_tgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_tgl ? transformDate($rj2->riw_jabatan_tgl) : ''}}">{{$rj->riw_jabatan_tgl ? transformDate($rj->riw_jabatan_tgl) : ''}}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_tgl ? transformDate($rj->riw_jabatan_tgl) : ''}}</td>
	    			@endif

	    			@if($rj->riw_jabatan_pejabat != $rj2->riw_jabatan_pejabat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_pejabat ? $rj2->riw_jabatan_pejabat : '-'}}">{{ $rj->riw_jabatan_pejabat ? $rj->riw_jabatan_pejabat : '-' }}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_pejabat}}</td>
	    			@endif

	    			@if($rj->riw_jabatan_tmt != $rj2->riw_jabatan_tmt)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_tmt ? transformDate($rj2->riw_jabatan_tmt) : ''}}">{{$rj->riw_jabatan_tmt ? transformDate($rj->riw_jabatan_tmt) : ''}}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_tmt ? transformDate($rj->riw_jabatan_tmt) : ''}}</td>
	    			@endif

	    			@if($rj->riw_jabatan_unit != $rj2->riw_jabatan_unit)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rj2->riw_jabatan_unit ? $rj2->riw_jabatan_unit : '-'}}">{{ $rj->riw_jabatan_unit ? $rj->riw_jabatan_unit : '-' }}</a></td>
	    			@else
	    				<td>{{$rj->riw_jabatan_unit}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-jabatan" 
					data-nama="{{$rj->riw_jabatan_nm}}" 
					data-id="{{$rj->riw_jabatan_id}}" 
					data-gol-ruang="{{$rj->gol_id}}" 
					data-no="{{$rj->riw_jabatan_no}}" 
					data-tgl="{{$rj->riw_jabatan_tgl}}"
					data-jabatan-ttd="{{$rj->riw_jabatan_pejabat}}"
					data-tmt="{{$rj->riw_jabatan_tmt}}"
					data-unit="{{$rj->riw_jabatan_unit}}"
					onclick="editJabatan(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-jabatan',array($pegawai->peg_id,$rj->riw_jabatan_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>
<div class="modal fade" id="modal-jabatan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelJabatan"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/jabatan')}}" id="jabatanForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'Nama Jabatan','required'=>false,'name'=>'riw_jabatan_nm','placeholder'=>''])
			<br>
			@include('form.select2_modal',['label'=>'Gol. Ruang','required'=>false,'name'=>'gol_id_ruang','data'=>
          	Golongan::orderBy('nm_gol')->lists('nm_gol','gol_id'),'empty'=>'-Pilih-'])
          	<br>
			@include('form.text2',['label'=>'No. SK','required'=>false,'name'=>'riw_jabatan_no','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_jabatan_tgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'riw_jabatan_pejabat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'TMT','required'=>false,'name'=>'riw_jabatan_tmt','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Unit Kerja','required'=>false,'name'=>'riw_jabatan_unit','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addJabatan = function(){
	$("#jabatanForm").attr("action","{{ URL::to('riwayat/add/jabatan/') }}");
	$("#labelJabatan").text("Tambah Data Jabatan");
	$("#gol_id_ruang").val("").select2();
	$(".riw_jabatan_nm").val("");
	$(".riw_jabatan_tgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_jabatan_tmt").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_jabatan_no").val("");
	$(".riw_jabatan_pejabat").val("");
	$(".riw_jabatan_unit").val("");
}
var editJabatan = function(e){
	var dateAr = $(e).data('tgl').split('-');
	var dateAr2 = $(e).data('tmt').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];

	$("#jabatanForm").attr("action","{{ URL::to('riwayat/edit/jabatan/') }}/"+$(e).data('id'));
	$("#labelJabatan").text("Edit Data Jabatan");
	$(".riw_jabatan_nm").val($(e).data('nama'));
	$("#gol_id_ruang").val($(e).data('gol-ruang')).select2();
	$(".riw_jabatan_tgl").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_jabatan_tmt").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_jabatan_no").val($(e).data('no'));
	$(".riw_jabatan_pejabat").val($(e).data('jabatan-ttd'));
	$(".riw_jabatan_unit").val($(e).data('unit'));
}

</script>