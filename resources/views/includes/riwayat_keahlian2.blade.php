<?php
	use App\Model\RiwayatKeahlian2;
	use App\Model\RiwayatKeahlian;
	use App\Model\RiwayatKeahlianRel2;
	use App\Model\RiwayatKeahlianRel;
	use App\Model\Keahlian;
	use App\Model\KeahlianLevel;
	use App\Model\RiwayatPendidikan2;
	use App\Model\RiwayatPendidikan;
	use App\Model\Universitas;
	use App\Model\TingkatPendidikan;
	use App\Model\RiwayatNonFormal2;
	use App\Model\RiwayatNonFormal;
	use App\Model\RiwayatDiklat2;
	use App\Model\RiwayatDiklat;
	use App\Model\DiklatStruktural;
	use App\Model\DiklatFungsional;
	use App\Model\DiklatTeknis;

	$keahlian = Keahlian::where('spesifik_dari_keahlian_id', '<>', 1)->where('keahlian_id','<>',1)->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Keahlian</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-keahlian" onclick="addKeahlian()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-keahlian" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Keahlian</th>
			<th rowspan="2">Level Keahlian</th>
		    <th rowspan="2">Tanggal Mulai Keahlian</th>
		    <th colspan="3">Pendidikan Penunjang</th>
		    <th rowspan="2">Predikat</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
			<th>Formal</th>
			<th>Non Formal</th>
			<th>Diklat</th>
		</tr>
	     </thead>
	      <tbody>
	      	@foreach($keahlian as $k)
	      	<?php
				$riw_keahlian = RiwayatKeahlian2::where('peg_id', $pegawai->peg_id)->where('keahlian_id',$k->keahlian_id)->get();
	      	?>
	      		@foreach($riw_keahlian as $rk)
	      		<?php 
	      			$level = KeahlianLevel::where('keahlian_level_id', $rk->keahlian_level_id)->first();
	      			$rel = RiwayatKeahlianRel2::where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
	      			$pend = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel['riw_pendidikan_id'])->first();
	      			$univ = Universitas::where('univ_id', $pend['univ_id'])->first();
	      			$pend_non = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->where('non_id', $rel['non_id'])->first();
	      			$diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel['diklat_id'])->first();
	      			if($diklat){
		      			if($diklat['diklat_jenis']==1){
							$jns = DiklatStruktural::where('kategori_id', $diklat->kategori_id)->first();
							$nm = $jns['kategori_nama'];
						}elseif($diklat['diklat_jenis']==2){
							$jns = DiklatFungsional::where('diklat_fungsional_id', $diklat->diklat_fungsional_id)->first();
							$nm = $jns['diklat_fungsional_nm'];
						}elseif($diklat['diklat_jenis']==3){
							$jns = DiklatTeknis::where('diklat_teknis_id', $diklat->diklat_teknis_id)->first();
							$nm = $jns['diklat_teknis_nm'];
						}else{
							$nm = '-';
						}
					}else{
						$nm = "-";
					}

					$rk2 = RiwayatKeahlian::where('peg_id', $pegawai->peg_id)->where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
	      			if($rk2){
	      				$k2 = Keahlian::where('keahlian_id', $rk2->keahlian_id)->first();
	      				$level2 = KeahlianLevel::where('keahlian_level_id', $rk2->keahlian_level_id)->first();
		      			$rel2 = RiwayatKeahlianRel::where('riw_keahlian_id', $rk2->riw_keahlian_id)->first();
		      			$pend2 = RiwayatPendidikan::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel2['riw_pendidikan_id'])->first();
		      			$univ2 = Universitas::where('univ_id', $pend2['univ_id'])->first();
		      			$pend_non2 = RiwayatNonFormal::where('peg_id', $pegawai->peg_id)->where('non_id', $rel2['non_id'])->first();
		      			$diklat2 = RiwayatDiklat::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel2['diklat_id'])->first();
		      			if($diklat2){
			      			if($diklat2['diklat_jenis']==1){
								$jns2 = DiklatStruktural::where('kategori_id', $diklat2->kategori_id)->first();
								$nm2 = $jns2['kategori_nama'];
							}elseif($diklat2['diklat_jenis']==2){
								$jns2 = DiklatFungsional::where('diklat_fungsional_id', $diklat2->diklat_fungsional_id)->first();
								$nm2 = $jns2['diklat_fungsional_nm'];
							}elseif($diklat2['diklat_jenis']==3){
								$jns2 = DiklatTeknis::where('diklat_teknis_id', $diklat2->diklat_teknis_id)->first();
								$nm2 = $jns2['diklat_teknis_nm'];
							}else{
								$nm2 = '-';
							}
						}else{
							$nm2 = "-";
						}
	      			}

	      		?>
	      		<tr>
	      			<td>{{$i}}</td>
	      			@if(!$rk2)
	      			<td class="alert-danger">{{$k->keahlian_nama}}</td>
	      			<td class="alert-danger">{{$level->keahlian_level_nama}}</td>
	      			<td class="alert-danger">{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</td>
	      			<td class="alert-danger">{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</td>
	      			<td class="alert-danger">{{$pend_non['non_nama']}}</td>
	      			<td class="alert-danger">{{$nm}}</td>
	      			<td class="alert-danger">{{$rel['predikat']}}</td>
	      			@else
		      			@if($rk->keahlian_id != $rk2->keahlian_id)
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->keahlian_nama ? $k2->keahlian_nama : '-'}}">{{$k->keahlian_nama ? $k->keahlian_nama : '-'}}</a></td>
		      			@else
		      				<td>{{$k->keahlian_nama}}</td>
		      			@endif
		      			@if($rk->keahlian_level_id != $rk2->keahlian_level_id)
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$level2->keahlian_level_nama ? $level2->keahlian_level_nama : '-'}}">{{$level->keahlian_level_nama ? $level->keahlian_level_nama : '-'}}</a></td>
		      			@else
		      				<td>{{$level->keahlian_level_nama}}</td>
		      			@endif
		      			
		      			@if($rk->riw_keahlian_sejak != $rk2->riw_keahlian_sejak)
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rk2->riw_keahlian_sejak ? transformDate($rk2->riw_keahlian_sejak) : '-'}}">{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</a></td>
		      			@else
		      				<td>{{$rk->riw_keahlian_sejak ? transformDate($rk->riw_keahlian_sejak) : '-'}}</td>
		      			@endif
		      			
		      			@if($rel['riw_pendidikan_id'] != $rel2['riw_pendidikan_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pend2['riw_pendidikan_nm']}}{{$univ2['univ_nmpti'] ? $univ2['univ_nmpti'] : '-' }}">{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</a></td>
		      			@else
		      				<td>{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</td>
		      			@endif

		      			@if($rel['non_id'] != $rel2['non_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pend_non2['non_nama'] ? $pend_non2['non_nama'] : '-'}}">{{$pend_non['non_nama'] ? $pend_non['non_nama'] : '-'}}</a></td>
		      			@else
		      				<td>{{$pend_non['non_nama']}}</td>
		      			@endif

		      			@if($rel['diklat_id'] != $rel2['diklat_id'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$nm2}}">{{$nm}}</a></td>
		      			@else
		      				<td>{{$nm}}</td>
		      			@endif
		      			
		      			@if($rel['predikat'] != $rel2['predikat'])
		      				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rel2['predikat'] ? $rel2['predikat'] : '-'}}">{{$rel['predikat'] ? $rel['predikat'] : '-'}}</a></td>
		      			@else
		      				<td>{{$rel['predikat']}}</td>
		      			@endif
	      			@endif
	      			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    			<td>
		    			<button class="btn btn-warning btn-xs"
						data-toggle="modal" data-target="#modal-keahlian" 
						data-id-keahlian="{{$rk->keahlian_id}}" 
						data-id="{{$rk->riw_keahlian_id}}"
						data-id-level="{{$rk->keahlian_level_id}}"
						data-tanggal="{{$rk->riw_keahlian_sejak}}"
						data-id-pendidikan="{{$rel['riw_pendidikan_id']}}"
						data-id-non="{{$rel['non_id']}}"
						data-id-diklat="{{$rel['diklat_id']}}"
						data-predikat="{{$rel['predikat']}}"
						onclick="editKeahlian(this)">
						<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
						Edit</button>
			    		<a href="{{url('/riwayat/delete-keahlian',array($pegawai->peg_id,$rk->riw_keahlian_id),false)}}" 
			    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
								Delete
						</a>
					</td>
				@endif
				</tr>
				<?php $i++;?>
	      		@endforeach
	      	@endforeach
	      </tbody>
	</table>
	</div>
</div>

<div class="modal fade" id="modal-keahlian" tabindex="-1" role="dialog" aria-labelledby="modal-keahlian" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelKeahlian"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="" id="keahlianForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal_btn',['label'=>'Nama Keahlian','required'=>true,'name'=>'keahlian_id','data'=>
          	Keahlian::with('allChildKeahlian')->where('spesifik_dari_keahlian_id', '<>',1)->where('keahlian_id','<>',1)->lists('keahlian_nama','keahlian_id') ,'empty'=>'-Pilih-',
          	'target'=>'#modal-new-keahlian','click'=>'addNewKeahlian()','judul'=>'Tambah Keahlian Baru'])
          	<br>
          	@include('form.select2_modal',['label'=>'Level Keahlian','required'=>true,'name'=>'keahlian_level_id','data'=>
          	KeahlianLevel::where('keahlian_id', 0)->lists('keahlian_level_nama','keahlian_level_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.text2',['label'=>'Tanggal Mulai Keahlian','required'=>false,'name'=>'riw_keahlian_sejak','placeholder'=>''])
			<br>
			<div class="row" id="riw_pendidikan_id_container">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">Pendidikan Penunjang (Formal)</label>
						<div class="col-md-8">
							<select class="form-control select2me" id="riw_pendidikan_id" name="riw_pendidikan_id" style="border:none; margin-left:-13px; width:578px">
								<?php
									$data = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->orderBy('tingpend_id')->get(); 
								?>
								<option value="">-Pilih-</option>
								@foreach ($data as $key)
									<?php
										$tp= TingkatPendidikan::where('tingpend_id', $key['tingpend_id'])->first(); 
										$universitas = Universitas::where('univ_id', $key['univ_id'])->first();
									?>
								<option value="{{$key['riw_pendidikan_id']}}">{{$tp->nm_tingpend}}-{{$key['riw_pendidikan_nm']}}{{$universitas['univ_nmpti']}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
          	<br>
          	@include('form.select2_modal',['label'=>'Pendidikan Penunjang (Non Formal)','required'=>false,'name'=>'non_id','data'=>
          	RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->lists('non_nama','non_id') ,'empty'=>'-Pilih-'])
          	<br>
          	<div class="row" id="diklat_id_container">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label col-md-4">Pendidikan Penunjang (Diklat)</label>
						<div class="col-md-8">
							<select class="form-control select2me" id="diklat_id" name="diklat_id" style="border:none; margin-left:-13px; width:578px">
								<?php
									$riw_diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->get(); 
								?>
								<option value="">-Pilih-</option>
								@foreach ($riw_diklat as $rd)
									<?php
										if($rd->diklat_jenis==1){
											$jenis = DiklatStruktural::where('kategori_id', $rd->kategori_id)->first();
											$nama = $jenis['kategori_nama'];
										}elseif($rd->diklat_jenis==2){
											$jenis = DiklatFungsional::where('diklat_fungsional_id', $rd->diklat_fungsional_id)->first();
											$nama = $jenis['diklat_fungsional_nm'];
										}elseif($rd->diklat_jenis==3){
											$jenis = DiklatTeknis::where('diklat_teknis_id', $rd->diklat_teknis_id)->first();
											$nama = $jenis['diklat_teknis_nm'];
										}
									?>
								<option value="{{$rd['diklat_id']}}">{{$nama}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
          	<br>
          	@include('form.text2',['label'=>'Predikat','required'=>false,'name'=>'predikat','placeholder'=>''])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitKeahlian" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<div class="modal fade" id="modal-new-keahlian" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelKeahlianNew"><div id="modal-button-edit"></div></h4>
        </div>
       
        <div class="modal-body" id="modal-detail-content">
            @include('form.text2',['label'=>'Nama Keahlian','required'=>false,'name'=>'keahlian_nama','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Deskripsi Keahlian','required'=>false,'name'=>'keahlian_deskripsi','placeholder'=>''])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitNewKeahlian" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </div>
    </div>
</div>

<script type="text/javascript">
var addKeahlian = function(){
	$("#keahlianForm").attr("action","{{ URL::to('riwayat/add/keahlian/') }}");
	$("#labelKeahlian").text("Tambah Riwayat Keahlian");

	$("#keahlian_id").val("").select2({ width: '450px' });
	$("#keahlian_level_id").val("").select2();
	$(".riw_keahlian_sejak").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_pendidikan_id").val("").select2();
	$("#non_id").val("").select2();
	$("#diklat_id").val("").select2();
	$(".predikat").val("");
	$('#keahlianForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

var editKeahlian= function(e){
	var dateAr = $(e).data('tanggal').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	$("#keahlianForm").attr("action","{{ URL::to('riwayat/edit/keahlian/') }}/"+$(e).data('id'));
	$("#labelKeahlian").text("Edit Riwayat Keahlian");

	$("#keahlian_id").val($(e).data('id-keahlian')).select2({ width: '450px' });
	$("#keahlian_level_id").val($(e).data('id-level')).select2();
	$(".riw_keahlian_sejak").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_pendidikan_id").val($(e).data('id-pendidikan')).select2();
	$("#non_id").val($(e).data('id-non')).select2();
	$("#diklat_id").val($(e).data('id-diklat')).select2();
	$(".predikat").val($(e).data('predikat'));
	$('#keahlianForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

var addNewKeahlian = function(){
	$("#labelKeahlianNew").text("Tambah Keahlian");
	$(".keahlian_nama").val("");
	$(".keahlian_deskripsi").val("");
	$("#submitNewKeahlian").click(function() {
		var nama = $("#keahlian_nama").val();
		var deskripsi = $("#keahlian_deskripsi").val();
		$.ajax({
	        type: "POST",
	        cache: false,
	        url: "{{ URL::to('data/keahlian/new') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
	        },
	        data: {"keahlian_nama":nama, "keahlian_deskripsi":deskripsi},
	        dataType: 'json',
	        success: function(response) {
	            console.log(response);
	            $('#modal-new-keahlian').modal('toggle');
	            var op, index, select, option;

			    // Get the raw DOM object for the select box
			    select = document.getElementById('keahlian_id');

			    // Clear the old options
			    select.length = 0;

			    // Load the new options
			    op = response; // Or whatever source information you're working with
			    for (index = 0; index < op.length; ++index) {
			      option = op[index];
			      select.add(new Option(option.keahlian_nama, option.keahlian_id));
			    }
			}
	    }).error(function (e){
	        console.log(e);
	        alert("ERROR!");
	    });
	});
}

</script>