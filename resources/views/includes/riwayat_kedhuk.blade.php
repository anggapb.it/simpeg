<?php

	use App\Model\RiwayatKedHuk;

	$struk = RiwayatKedHuk::where('peg_id',$pegawai->peg_id)->get();
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Kedudukan Hukum</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-kedhuk" onclick="addKedhuk()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
		    <th>Status</th>
			<th>No. SK</th>
			<th>Tanggal SK</th>
			<th>TMT</th>
			<th>SKPD</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Aksi</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($struk as $s)
	      	<tr>
	      		<td>{{$no++}}</td>
	      		<td>{{$s->kedudukan->kedudukan_pns}}</td>
	      		<td>{{$s->no_sk}}</td>
	      		<td>{{getFullDate($s->tgl_sk)}}</td>
	      		<td>{{getFullDate($s->tmt)}}</td>
	      		<td>{{$s->skpd->satuan_kerja_nama}}</td>
	      		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-kedhuk" 
					data-id="{{$s->kedhuk_id}}" 
					data-status="{{$s->status}}" 
					data-no_sk="{{$s->no_sk}}" 
					data-tgl_sk="{{$s->tgl_sk}}" 
					data-tmt="{{$s->tmt}}" 
					data-skpd="{{$s->satuan_kerja_id}}"
					onclick="editKedHuk(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-kedhuk',$s->peg_id).'/'.$s->kedhuk_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Riwayat Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
			@endif
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-kedhuk" tabindex="-1" role="dialog" aria-labelledby="modal-kedhuk" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelKedhuk"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/add/kedhuk')}}" id="formKedhuk">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Status','required'=>true,'name'=>'status','data'=>\DB::connection('pgsql2')->table('m_spg_kedudukan_pns')->lists('kedudukan_pns','id'),'empty'=>''])
			<br>
			@include('form.text2',['label'=>'No. SK','required'=>true,'name'=>'no_sk'])
			<br>
			@include('form.date2',['label'=>'Tanggal SK','required'=>true,'name'=>'tgl_sk'])
			<br>
			@include('form.date2',['label'=>'TMT','required'=>true,'name'=>'tmt'])
			<br>
			@include('form.select2_modal',['label'=>'SKPD','required'=>true,'name'=>'satuan_kerja_id','data'=>App\Model\SatuanKerja::where('satuan_kerja_nama','not like','%-%')->where('status',1)->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

var addKedhuk = function(){
	$("#formKedhuk").attr("action","{{ URL::to('riwayat/add/kedhuk/') }}");
	$("#labelKedhuk").html("Tambah Riwayat Kedudukan Hukum");
	$("#status").val("");
	$("#no_sk").val("");
	$("#tgl_sk,#tmt").val("");
	$("#tgl_sk,#tmt").datepicker({autoclose: true});
	$("#satuan_kerja_id").val("");
	$("#satuan_kerja_id").select2({
		placeholder: 'Pilih SKPD'
	});
	$("#status").select2({
		placeholder: 'Pilih Status'
	});
}
var editKedHuk = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formKedhuk").attr("action","{{ URL::to('riwayat/add/kedhuk/') }}");
	}else{
		$("#formKedhuk").attr("action","{{ URL::to('riwayat/edit/kedhuk/') }}/"+id);
	}

	$("#labelKedhuk").html("Edit Riwayat Kedudukan Hukum");
	$("#status").val($(e).data('status'));
	$("#no_sk").val($(e).data('no_sk'));
	$("#tgl_sk").val($(e).data('tgl_sk'));
	$("#tmt").val($(e).data('tmt'));
	$("#tgl_sk,#tmt").datepicker({autoclose: true});
	$("#status").val($(e).data('status'));
	$("#satuan_kerja_id").val($(e).data('skpd'));
	$("#satuan_kerja_id").select2({
		placeholder: 'Pilih SKPD'
	});
	$("#status").select2({
		placeholder: 'Pilih Status'
	});
}
</script>
