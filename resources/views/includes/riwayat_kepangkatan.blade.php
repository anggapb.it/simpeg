<?php
	use App\Model\RiwayatPangkat2;
	use App\Model\RiwayatPangkat;
	use App\Model\Golongan;
	use App\Model\Gaji;
	use App\Model\GajiTahun;

	$riwayat_pangkat = RiwayatPangkat::where('peg_id', $pegawai->peg_id)->orderBy('gol_id')->get();
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$i=1;
?>
<div class="col-xs-12">
	<h3 align="middle">Riwayat Kepangkatan</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit)
		<!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pangkat" onclick="addPangkat()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button> -->
		@endif
	</div>
	<table id="tabel-riwayat-kepangkatan" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	    <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Gol.<br/>Ruang</th>
			<th colspan="2">Masa Kerja</th>
		  	<th rowspan="2">Gaji Pokok</th>
		  	<th colspan="3">Surat Keputusan</th>
			<th rowspan="2">TMT</th>
			<th rowspan="2">Unit Kerja</th>
			@if(!$submit)
			<!-- <th rowspan="2">Pilihan</th> -->
			@endif
		</tr>
		<tr align="center">
		    <th>Thn</th>
		    <th>Bln</th>
		    <th>Nomor</th>
		    <th>Tanggal</th>
			<th>Jabatan<br/>Penandatangan</th>
		</tr>
	     </thead>
	    <tbody>
	    	@foreach($riwayat_pangkat as $rp)
	    		<?php 
	    			$rp2 = RiwayatPangkat2::where('peg_id', $pegawai->peg_id)->where('riw_pangkat_id', $rp->riw_pangkat_id)->first();
	    			$gol = Golongan::where('gol_id', $rp->gol_id)->first(); 
	    			if (!$gol) $gol = new Golongan;
					$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $rp->riw_pangkat_thn)->first();

					if($rp2){
						$gol2 = Golongan::where('gol_id', $rp2->gol_id)->first(); 
	    				if (!$gol2) $gol2 = new Golongan;
						$gaji2 = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp2->gol_id)->where('gaji_masakerja', $rp2->riw_pangkat_thn)->first();
					}
	    		?>
	    	<tr class="aktifPangkat{{$rp->riw_pangkat_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$rp2)
		    		<td class="alert-danger">{{$gol->nm_gol}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_thn}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_bln}}</td>
		    		<td class="alert-danger">Rp {{number_format($gaji['gaji_pokok'],2)}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_sk}}</td>
		    		<td class="alert-danger">{{ $rp->riw_pangkat_sktgl ? transformDate($rp->riw_pangkat_sktgl) : ''}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_pejabat}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_tmt ? transformDate($rp->riw_pangkat_tmt) : ''}}</td>
		    		<td class="alert-danger">{{$rp->riw_pangkat_unit_kerja}}</td>
	    		@else
	    			@if($rp->gol_id != $rp2->gol_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$gol2->nm_gol ? $gol2->nm_gol : '-'}}">{{ $gol->nm_gol ? $gol->nm_gol : '-' }}</a></td>
	    			@else
	    				<td>{{$gol->nm_gol}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_thn != $rp2->riw_pangkat_thn)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_thn ? $rp2->riw_pangkat_thn : '-'}}">{{ $rp->riw_pangkat_thn ? $rp->riw_pangkat_thn : '-' }}</a></td>
	    			@else
	    				<td>{{$rp->riw_pangkat_thn}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_bln != $rp2->riw_pangkat_bln)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_bln ? $rp2->riw_pangkat_bln : '-'}}">{{ $rp->riw_pangkat_bln ? $rp->riw_pangkat_bln : '-' }}</a></td>
	    			@else
	    				<td>{{$rp->riw_pangkat_bln}}</td>
	    			@endif
	    			@if($rp->gol_id != $rp2->gol_id || $rp->riw_pangkat_thn != $rp2->riw_pangkat_thn)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="Rp {{number_format($gaji2['gaji_pokok'],2)}}">Rp {{number_format($gaji['gaji_pokok'],2)}}</a></td>
	    			@else
	    				<td>Rp {{number_format($gaji['gaji_pokok'],2)}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_sk != $rp2->riw_pangkat_sk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_sk ? $rp2->riw_pangkat_sk : '-'}}">{{ $rp->riw_pangkat_sk ? $rp->riw_pangkat_sk : '-' }}</a></td>
	    			@else
	    				<td>{{$rp->riw_pangkat_sk}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_sktgl != $rp2->riw_pangkat_sktgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_sktgl ? transformDate($rp2->riw_pangkat_sktgl) : '-'}}">{{ $rp->riw_pangkat_sktgl ? transformDate($rp->riw_pangkat_sktgl) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->riw_pangkat_sktgl ? transformDate($rp->riw_pangkat_sktgl) : ''}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_pejabat != $rp2->riw_pangkat_pejabat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_pejabat ? $rp2->riw_pangkat_pejabat : '-'}}">{{$rp->riw_pangkat_pejabat ? $rp->riw_pangkat_pejabat : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->riw_pangkat_pejabat}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_tmt != $rp2->riw_pangkat_tmt)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_tmt ? transformDate($rp2->riw_pangkat_tmt) : '-'}}">{{ $rp->riw_pangkat_tmt ? transformDate($rp->riw_pangkat_tmt) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->riw_pangkat_tmt ? transformDate($rp->riw_pangkat_tmt) : ''}}</td>
	    			@endif
	    			@if($rp->riw_pangkat_unit_kerja != $rp2->riw_pangkat_unit_kerja)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pangkat_unit_kerja ? $rp2->riw_pangkat_unit_kerja : '-'}}">{{$rp->riw_pangkat_unit_kerja ? $rp->riw_pangkat_unit_kerja : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->riw_pangkat_unit_kerja}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit)
	    		<!-- <td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-pangkat" 
					data-nama="{{$rp->gol_id}}" 
					data-id="{{$rp->riw_pangkat_id}}" 
					data-pangkat-thn="{{$rp->riw_pangkat_thn}}" 
					data-pangkat-bln="{{$rp->riw_pangkat_bln}}" 
					data-gapok="{{$rp->riw_pangkat_gapok}}"
					data-sk-nomor="{{$rp->riw_pangkat_sk}}"
					data-sk-tgl="{{$rp->riw_pangkat_sktgl }}"
					data-sk-pejabat="{{$rp->riw_pangkat_pejabat}}"
					data-tmt-pangkat="{{$rp->riw_pangkat_tmt}}"
					data-unit-kerja="{{$rp->riw_pangkat_unit_kerja}}"
					onclick="editPangkat(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-pangkat',array($pegawai->peg_id,$rp->riw_pangkat_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td> -->
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
</div>
<div class="modal fade" id="modal-pangkat" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelPangkat"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/pangkat')}}" id="pangkatForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Gol. Ruang','required'=>false,'name'=>'gol_ruang_id','data'=>
          	Golongan::orderBy('nm_gol')->lists('nm_gol','gol_id'),'empty'=>'-Pilih-'])
          	<br>
          	@include('form.text2',['label'=>'Masa Kerja Tahun','required'=>false,'name'=>'riw_pangkat_thn','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Masa Kerja Bulan','required'=>false,'name'=>'riw_pangkat_bln','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nomor SK','required'=>false,'name'=>'riw_pangkat_sk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_pangkat_sktgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'riw_pangkat_pejabat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'TMT','required'=>false,'name'=>'riw_pangkat_tmt','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Unit Kerja','required'=>false,'name'=>'riw_pangkat_unit_kerja','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addPangkat = function(){
	$("#pangkatForm").attr("action","{{ URL::to('riwayat/add/pangkat/') }}");
	$("#labelPangkat").text("Tambah Riwayat Pangkat");
	$("#gol_ruang_id").val("").select2();
	$(".riw_pangkat_thn").val("");
	$(".riw_pangkat_bln").val("");
	$(".riw_pangkat_gapok").val("");
	$(".riw_pangkat_sktgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_pangkat_tmt").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_pangkat_sk").val("");
	$(".riw_pangkat_pejabat").val("");
	$(".riw_pangkat_unit_kerja").val("");
}
var editPangkat = function(e){
	var dateAr = $(e).data('sk-tgl').split('-');
	var dateAr2 = $(e).data('tmt-pangkat').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];

	$("#pangkatForm").attr("action","{{ URL::to('riwayat/edit/pangkat/') }}/"+$(e).data('id'));
	$("#labelPangkat").text("Edit Riwayat Pangkat");
	$("#gol_ruang_id").val($(e).data('nama')).select2();
	$(".riw_pangkat_thn").val($(e).data('pangkat-thn'));
	$(".riw_pangkat_bln").val($(e).data('pangkat-bln'));
	$(".riw_pangkat_gapok").val($(e).data('gapok'));
	$(".riw_pangkat_sk").val($(e).data('sk-nomor'));
	$(".riw_pangkat_sktgl").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_pangkat_tmt").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_pangkat_pejabat").val($(e).data('sk-pejabat'));
	$(".riw_pangkat_unit_kerja").val($(e).data('unit-kerja'));
}

function formatNumber(number)
{
    var number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
</script>