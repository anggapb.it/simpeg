<?php

	use App\Model\KepalaSekolah;
	use App\Model\Jabatan;
	use App\Model\UnitKerja;

	$iduker = Jabatan::where('jabatan_nama','like','Kepala Sekolah%')->lists('unit_kerja_id');
	$iduker = $iduker->unique()->toArray();
	$uker = UnitKerja::whereIn('unit_kerja_id',$iduker)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id');

	$kepalasek = KepalaSekolah::leftJoin('m_spg_unit_kerja','m_spg_unit_kerja.unit_kerja_id','=','spg_kepala_sekolah.unit_kerja_id')->where('peg_id',$pegawai->peg_id)->get();
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Kepala Sekolah</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-kepsek" onclick="addFile()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
		    <th>Nama Jabatan</th>
			<th>Nama Sekolah</th>
			<th>TMT Mulai</th>
			<th>TMT Selesai</th>
			@if(!$submit && Auth::user()->role_id != 5)
			<th>Aksi</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($kepalasek as $ks)
	      	<tr {{$ks->unit_kerja_id == $pegawai->unit_kerja_id ? 'style=background-color:#42d238 !important;' : ''}}>
	      		<td>{{$no++}}</td>
	      		<td>{{$ks->nama_jabatan}}</td>
	      		<td>{{$ks->unit_kerja_nama}}</td>
	      		<td>{{getFullDate($ks->tmt_mulai)}}</td>
	      		<td>{{getFullDate($ks->tmt_selesai)}}</td>
			@if(!$submit && Auth::user()->role_id != 5)
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-kepsek" 
					data-nama="{{$ks->nama_jabatan}}" 
					data-id="{{$ks->kepala_sekolah_id}}" 
					data-mulai="{{$ks->tmt_mulai}}" 
					data-selesai="{{$ks->tmt_selesai}}" 
					data-unit="{{$ks->unit_kerja_id}}"
					onclick="editFile(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-kepsek',$ks->peg_id).'/'.$ks->kepala_sekolah_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Riwayat Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
			@endif
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-kepsek" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelKepsek"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/kepsek')}}" id="formKepsek">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Nama Sekolah','data' => $uker,'required'=>false,'name'=>'riw_unit_kerja','value' => '','placeholder'=>'','empty' => ''])
			<br>
			@include('form.text3',['label'=>'Nama Jabatan','required'=>false,'name'=>'riw_nama_jabatan','value' => 'Kepala Sekolah','placeholder'=>''])
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<div class="error-file"></div>
					</div>
				</div>
			</div>
			<br>
			@include('form.text2',['label'=>'TMT Mulai','required'=>false,'name'=>'riw_tmt_mulai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'TMT Selesai','required'=>false,'name'=>'riw_tmt_selesai','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">
	window.jQuery || document.write("<script src='{{ asset('/js/jquery.min.js')}}'>"+"<"+"/script>");
</script>
<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
</script>
<script type="text/javascript">

var addFile = function(){
	$("#formKepsek").attr("action","{{ URL::to('riwayat/add/kepsek/') }}");
	$("#labelKepsek").html("Tambah Riwayat Kepala Sekolah");
	$("#riw_nama_jabatan").val("Kepala Sekolah");
	$("#riw_unit_kerja").val("");
	$("#riw_tmt_mulai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_tmt_selesai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_unit_kerja").select2();
}
var editFile = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formKepsek").attr("action","{{ URL::to('riwayat/add/kepsek/') }}");
	}else{
		$("#formKepsek").attr("action","{{ URL::to('riwayat/edit/kepsek/') }}/"+id);
	}

	$("#labelKepsek").html("Edit Riwayat Kepala Sekolah");
	$("#riw_nama_jabatan").val($(e).data('nama'));
	$("#riw_unit_kerja").val($(e).data('unit'));
	$("#riw_tmt_mulai").val($(e).data('mulai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_tmt_selesai").val($(e).data('selesai')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	$("#riw_unit_kerja").select2();

}

$(document).on('change','#riw_unit_kerja',function(){
	var that = $(this);
	var val = $(this).val();
	var text = this.options[this.selectedIndex].text;
	$("#riw_nama_jabatan").val("Kepala Sekolah " + text);
});

    
</script>
