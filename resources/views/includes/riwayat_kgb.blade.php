<?php
	use App\Model\KGB;
	use App\Model\Kgb_skpd;
	use App\Model\Golongan;
	use App\Model\Gaji;
	use App\Model\GajiTahun;

	$riwayat_kgb = KGB::where('peg_id', $pegawai->peg_id)->orderBy('gol_id')->get();
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$i=1;
?>
<div class="col-xs-12">
	<h3 align="middle">Riwayat KGB</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-kgb" onclick="addKgb()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-kgb" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	    <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Gol.<br/>Ruang</th>
			<th colspan="2">Masa Kerja (+ pmk jika ada)</th>
		  	<th rowspan="2">Gaji Pokok</th>
		  	<th colspan="3">Surat Keputusan</th>
			<th rowspan="2">TMT</th>
			<th rowspan="2">Unit Kerja</th>
			<th rowspan="2">Status data</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
		    <th>Thn</th>
		    <th>Bln</th>
		    <th>Nomor</th>
		    <th>Tanggal</th>
			<th>Jabatan<br/>Penandatangan</th>
		</tr>
	     </thead>
	    <tbody>
	    	@foreach($riwayat_kgb as $rp)
	    		<?php 
	    			$rp2 = Kgb_skpd::where('peg_id', $pegawai->peg_id)->where('kgb_gapok', $rp->kgb_gapok)->where('gol_id', $rp->gol_id)->first();
	    			$gol = Golongan::where('gol_id', $rp->gol_id)->first(); 
	    			if (!$gol) $gol = new Golongan;
	    			// dd($rp->kgb_kerja_tahun);
	    			if(!$rp->kgb_thn || $rp->kgb_thn =='-'){
	    				$rp->kgb_thn = 0;
	    			}
					$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $rp->kgb_kerja_tahun)->first();

					if($rp2){
						if(!$rp2->kgb_thn || $rp2->kgb_thn =='-'){
                            $rp2->kgb_thn = 0;
                        }

						$gol2 = Golongan::where('gol_id', $rp2->gol_id)->first(); 
	    				if (!$gol2) $gol2 = new Golongan;
						$gaji2 = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp2->gol_id)->where('gaji_masakerja', $rp2->kgb_kerja_tahun)->first();
					}
	    		?>
	    	<tr class="aktifKgb{{$rp->kgb_id}}">
	    		<td>{{$i}}</td>
		    		<td>{{$gol->nm_gol}}</td>
		    		<td>{{$rp->kgb_kerja_tahun}}</td>
		    		<td>{{$rp->kgb_kerja_bulan}}</td>
		    		<td>Rp {{number_format($gaji['gaji_pokok'],2)}}</td>
		    		<td>{{$rp->kgb_nosk}}</td>
		    		<td>{{ $rp->kgb_tglsk ? transformDate($rp->kgb_tglsk) : ''}}</td>
		    		<td>{{$rp->ttd_pejabat}}</td>
		    		<td>{{$rp->kgb_tmt ? transformDate($rp->kgb_tmt) : ''}}</td>
		    		<td>{{$rp->unit_kerja}}</td>
		    		<td>{{$rp->status_data}}</td>
	    		<!-- @if(!$rp2) -->
	    		<!-- @else
	    			@if($rp->gol_id != $rp2->gol_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$gol2->nm_gol ? $gol2->nm_gol : '-'}}">{{ $gol->nm_gol ? $gol->nm_gol : '-' }}</a></td>
	    			@else
	    				<td>{{$gol['nm_gol'] ? $gol['nm_gol'] : ''}}</td>
	    			@endif
	    			@if($rp->kgb_kerja_tahun != $rp2->kgb_kerja_tahun)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->kgb_kerja_tahun ? $rp2->kgb_kerja_tahun : ''}}">{{ $rp->kgb_kerja_tahun ? $rp->kgb_kerja_tahun : '' }}</a></td>
	    			@else
	    				<td>{{$rp->kgb_kerja_tahun}}</td>
	    			@endif
	    			@if($rp->kgb_kerja_bulan != $rp2->kgb_kerja_bulan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->kgb_kerja_bulan ? $rp2->kgb_kerja_bulan : ''}}">{{ $rp->kgb_kerja_bulan ? $rp->kgb_kerja_bulan : '' }}</a></td>
	    			@else
	    				<td>{{$rp->kgb_kerja_bulan}}</td>
	    			@endif
	    			@if($rp->gol_id != $rp2->gol_id || $rp->kgb_kerja_tahun != $rp2->kgb_kerja_tahun)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="Rp {{number_format($gaji2['gaji_pokok'],2)}}">Rp {{number_format($gaji['gaji_pokok'],2)}}</a></td>
	    			@else
	    				<td>Rp {{number_format($gaji['gaji_pokok'],2)}}</td>
	    			@endif
	    			@if($rp->kgb_nosk != $rp2->kgb_nosk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->kgb_nosk ? $rp2->kgb_nosk : '-'}}">{{ $rp->kgb_nosk ? $rp->kgb_nosk : '-' }}</a></td>
	    			@else
	    				<td>{{$rp->kgb_nosk}}</td>
	    			@endif
	    			@if($rp->kgb_tglsk != $rp2->kgb_tglsk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->kgb_tglsk ? transformDate($rp2->kgb_tglsk) : '-'}}">{{ $rp->kgb_tglsk ? transformDate($rp->kgb_tglsk) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->kgb_tglsk ? transformDate($rp->kgb_tglsk) : ''}}</td>
	    			@endif
	    			@if($rp->ttd_pejabat != $rp2->ttd_pejabat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->ttd_pejabat ? $rp2->ttd_pejabat : '-'}}">{{$rp->ttd_pejabat ? $rp->ttd_pejabat : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->ttd_pejabat}}</td>
	    			@endif
	    			@if($rp->kgb_tmt != $rp2->kgb_tmt)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->kgb_tmt ? transformDate($rp2->kgb_tmt) : '-'}}">{{ $rp->kgb_tmt ? transformDate($rp->kgb_tmt) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->kgb_tmt ? transformDate($rp->kgb_tmt) : ''}}</td>
	    			@endif
	    			@if($rp->unit_kerja != $rp2->unit_kerja)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->unit_kerja ? $rp2->unit_kerja : '-'}}">{{$rp->unit_kerja ? $rp->unit_kerja : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->unit_kerja}}</td>
	    			@endif
	    			@if($rp->status_data != $rp2->status_data)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->status_data ? $rp2->status_data : '-'}}">{{$rp->status_data ? $rp->status_data : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->status_data}}</td>
	    			@endif
	    		@endif -->
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
					@if($rp->status_data == 'manual')
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-kgb" 
					data-gol_id="{{$rp->gol_id}}" 
					data-id="{{$rp->kgb_id}}" 
					data-kgb-thn="{{$rp->kgb_kerja_tahun}}" 
					data-kgb-bln="{{$rp->kgb_kerja_bulan}}" 
					data-gapok="{{$rp->kgb_gapok}}"
					data-sk-nomor="{{$rp->kgb_nosk}}"
					data-sk-tgl="{{$rp->kgb_tglsk }}"
					data-sk-pejabat="{{$rp->ttd_pejabat}}"
					data-tmt-kgb="{{$rp->kgb_tmt}}"
					data-unit-kerja="{{$rp->unit_kerja}}"
					onclick="editKgb(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-kgb',array($pegawai->peg_id,$rp->kgb_id,(isset($rp2->kgb_id) ? $rp2->kgb_id : 0)),false)}}" 
						class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
					@endif
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>
<div class="modal fade" id="modal-kgb" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelKgb"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/kgb')}}" id="kgbForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			<div class="bs-callout bs-callout-warning hidden">
			  <h4>Edit Data Gagal!</h4>
			  <p>Data Harus Lengkap!</p>
			</div>
			@include('form.select2_modal',['label'=>'Gol. Ruang','required'=>true,'name'=>'gol_id_kgb','data'=>
          	Golongan::orderBy('nm_gol')->lists('nm_gol','gol_id'),'empty'=>'-Pilih-'])
          	<br>
          	<!-- @include('form.text2',['label'=>'PMK Tahun','required'=>false,'name'=>'pmk_tahun','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'PMK Bulan','required'=>false,'name'=>'pmk_bulan','placeholder'=>''])
            <br> -->
          	@include('form.text2',['label'=>'Masa Kerja Tahun','required'=>false,'name'=>'kgb_kerja_tahun','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Masa Kerja Bulan','required'=>false,'name'=>'kgb_kerja_bulan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nomor SK','required'=>false,'name'=>'kgb_nosk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'kgb_tglsk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'ttd_pejabat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'TMT','required'=>false,'name'=>'kgb_tmt','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Unit Kerja','required'=>false,'name'=>'unit_kerja','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
@section('scripts')
<script type="text/javascript">
$.fn.ready(function() {
    $('#gol_id_kgb').select2({ width: '100%',allowClear: true });
});

var addKgb = function(){
	$("#kgbForm").attr("action","{{ URL::to('riwayat/add/kgb/') }}");
	$("#labelKgb").text("Tambah Riwayat Kgb");
    $("#gol_id_kgb").val("").select2();
    
	$(".kgb_kerja_tahun").val("");
	$(".kgb_kerja_bulan").val("");
	$(".kgb_gapok").val("");
	$(".kgb_tglsk").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".kgb_tmt").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".kgb_nosk").val("");
	$(".ttd_pejabat").val("");
	$(".unit_kerja").val("");

	$(".kgb_kerja_tahun").on("keydown", function (e) {
		numberOnly(e);
	}); 
	$(".kgb_kerja_bulan").on("keydown", function (e) {
		numberOnly(e);
	});

	$('#kgbForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}
var editKgb = function(e){
	var dateAr = $(e).data('sk-tgl').split('-');
	var dateAr2 = $(e).data('tmt-kgb').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];

	$("#kgbForm").attr("action","{{ URL::to('riwayat/edit/kgb/') }}/"+$(e).data('id'));
	$("#labelKgb").text("Edit Riwayat Kgb");
	$("#gol_id_kgb").val($(e).data('gol_id')).select2();
	$(".kgb_kerja_tahun").val($(e).data('kgb-thn'));
	$(".kgb_kerja_bulan").val($(e).data('kgb-bln'));
	$(".kgb_gapok").val($(e).data('gapok'));
	$(".kgb_nosk").val($(e).data('sk-nomor'));
	$(".kgb_tglsk").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".kgb_tmt").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".ttd_pejabat").val($(e).data('sk-pejabat'));
	$(".unit_kerja").val($(e).data('unit-kerja'));

	$(".kgb_kerja_tahun").on("keydown", function (e) {
		numberOnly(e);
	}); 
	$(".kgb_kerja_bulan").on("keydown", function (e) {
		numberOnly(e);
	});

	$('#kgbForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

function formatNumber(number)
{
    var number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function numberOnly(e){
	var key   = e.keyCode ? e.keyCode : e.which;

	if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
	     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
	     (key >= 35 && key <= 40) ||
	     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
	     (key >= 96 && key <= 105)
	   )) e.preventDefault();
}

</script>
@endsection