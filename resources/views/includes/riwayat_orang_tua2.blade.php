<?php

	use App\Model\RiwayatKeluarga2;
	use App\Model\RiwayatKeluarga;

	$kel = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_status',3)->orderBy('riw_tgl_lahir')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Riwayat Orang Tua</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-ortu" onclick="addOrtu()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-orang-tua" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>NIK</th>
			<th>Nama Orang Tua</th>
			<th>Jenis Kelamin</th>
			<th>Tempat dan Tanggal Lahir</th>
			<th>Pendidikan</th>
			<th>Pekerjaan</th>
			<th>Keterangan</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Pilihan</th>
			@endif
		</tr>
	     </thead>
	       <tbody>
	    	@foreach ($kel as $k)
	    	<?php
	    		$k2 = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_id', $k->riw_id)->where('riw_status',3)->orderBy('riw_tgl_lahir')->first();
	    	
	    		if($k->riw_kelamin == 'L'){
	    			$jk = 'Laki-laki';
	    		}elseif($k->riw_kelamin == 'P'){
	    			$jk = 'Perempuan';
	    		}else{
	    			$jk = '-';
	    		}

	    		if($k2['riw_kelamin'] == 'L'){
	    			$jk2 = 'Laki-laki';
	    		}elseif($k2['riw_kelamin'] == 'P'){
	    			$jk2 = 'Perempuan';
	    		}else{
	    			$jk2 = '-';
	    		}
	    	?>
	    	<tr class="aktifKeluarga{{$k->riw_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$k2)
		    		<td class="alert-danger">{{$k->nik}}</td>
		    		<td class="alert-danger">{{$k->riw_nama}}</td>
		    		<td class="alert-danger">{{$jk}}</td>
		    		<td class="alert-danger">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
		    		<td class="alert-danger">{{$k->riw_pendidikan}}</td>
		    		<td class="alert-danger">{{$k->riw_pekerjaan}}</td>
		    		<td class="alert-danger">{{$k->riw_ket}}</td>
	    		@else
	    			@if($k->nik != $k2->nik)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->nik ? $k2->nik : '-'}}">{{ $k->nik ? $k->nik : '-' }}</a></td>
	    			@else
	    				<td>{{$k->nik}}</td>
	    			@endif

		    		@if($k->riw_nama != $k2->riw_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_nama ? $k2->riw_nama : '-'}}">{{ $k->riw_nama ? $k->riw_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_nama}}</td>
	    			@endif

	    			@if($k->riw_kelamin != $k2->riw_kelamin)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$jk2}}">{{ $jk }}</a></td>
	    			@else
	    				<td>{{$jk}}</td>
	    			@endif
	    			
	    			@if($k->riw_tempat_lahir != $k2->riw_tempat_lahir || $k->riw_tgl_lahir != $k2->riw_tgl_lahir)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_tempat_lahir}}, {{$k2->riw_tgl_lahir ? transformDate($k2->riw_tgl_lahir) : ''}}">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</a></td>
	    			@else
	    				<td>{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
	    			@endif
		    		@if($k->riw_pendidikan != $k2->riw_pendidikan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pendidikan ? $k2->riw_pendidikan : '-'}}">{{ $k->riw_pendidikan ? $k->riw_pendidikan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pendidikan}}</td>
	    			@endif
		    		@if($k->riw_pekerjaan != $k2->riw_pekerjaan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pekerjaan ? $k2->riw_pekerjaan : '-'}}">{{ $k->riw_pekerjaan ? $k->riw_pekerjaan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pekerjaan}}</td>
	    			@endif
	    			@if($k->riw_ket != $k2->riw_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_ket ? $k2->riw_ket : '-'}}">{{ $k->riw_ket ? $k->riw_ket : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_ket}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-ortu" 
					data-nama="{{$k->riw_nama}}" 
					data-nik="{{$k->nik}}" 
					data-id="{{$k->riw_id}}" 
					data-jenis-kelamin="{{$k->riw_kelamin}}" 
					data-tempat-lahir="{{$k->riw_tempat_lahir}}" 
					data-tanggal-lahir="{{$k->riw_tgl_lahir}}"
					data-pendidikan="{{$k->riw_pendidikan}}"
					data-pekerjaan="{{$k->riw_pekerjaan}}"
					data-keterangan="{{$k->riw_ket}}"
					onclick="editOrtu(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-keluarga',array($pegawai->peg_id,$k->riw_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	 @endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-ortu" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelOrtu"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/ortu')}}" id="ortuForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'NIK','required'=>false,'name'=>'riw_nik','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nama','required'=>false,'name'=>'riw_nama','placeholder'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Jenis Kelamin','required'=>false,'name'=>'riw_kelamin','data'=>[
            'L' => 'Laki Laki','P' => 'Perempuan' ]])
			<br>
			@include('form.text2',['label'=>'Tempat Lahir','required'=>false,'name'=>'riw_tempat_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Lahir','required'=>false,'name'=>'riw_tgl_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pendidikan','required'=>false,'name'=>'riw_pendidikan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pekerjaan','required'=>false,'name'=>'riw_pekerjaan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Keterangan','required'=>false,'name'=>'riw_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addOrtu = function(){
	$("#ortuForm").attr("action","{{ URL::to('riwayat/add/ortu/') }}");
	$("#labelOrtu").text("Tambah Data Orang Tua");
	$(".riw_nama").val("");
	$(".riw_nik").val("");
	$(".riw_kelamin1").val("").prop('checked', false);
	$(".riw_kelamin2").val("").prop('checked', false);
	$(".riw_tgl_lahir").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val("");
	$(".riw_pendidikan").val("");
	$(".riw_ket").val("");
	$(".riw_pekerjaan").val("");
}
var editOrtu = function(e){
	var dateAr = $(e).data('tanggal-lahir').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var jk = ($(e).data('jenis-kelamin'));

	$("#ortuForm").attr("action","{{ URL::to('riwayat/edit/ortu/') }}/"+$(e).data('id'));
	$("#labelOrtu").text("Edit Data Orang Tua");
	$(".riw_nama").val($(e).data('nama'));
	$(".riw_nik").val($(e).data('nik'));
	if(jk == 'L'){
		$(".riw_kelamin1").val($(e).data('jenis-kelamin')).prop( "checked", true );
	}else if(jk == 'P'){
		$(".riw_kelamin2").val($(e).data('jenis-kelamin')).prop( "checked", true );
	}
	
	$(".riw_tgl_lahir").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val($(e).data('tempat-lahir'));
	$(".riw_pendidikan").val($(e).data('pendidikan'));
	$(".riw_pekerjaan").val($(e).data('pekerjaan'));
	$(".riw_ket").val($(e).data('keterangan'));
}

</script>