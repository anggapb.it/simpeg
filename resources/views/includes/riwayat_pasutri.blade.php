<?php

	use App\Model\RiwayatKeluarga;
	use App\Model\RiwayatKeluarga2;

	$kel = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',4)->get();
	$i=1;
?>

<div class="col-xs-12">
	@if($pegawai->peg_jenis_kelamin == 'L')
	<h3 align="middle">Riwayat Istri</h3><br>
	@else
	<h3 align="middle">Riwayat Suami</h3><br>
	@endif

	<div class="pull-right tableTools-container">
		@if(!$submit)
		<!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pasutri" onclick="addPasutri()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button> -->
		@endif
	</div>
	<table id="tabel-riwayat-pasutri" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			@if($pegawai->peg_jenis_kelamin == 'L')
			<th>Nama Istri</th>
			@else
			<th>Nama Suami</th>
			@endif
			<th>Tempat dan Tanggal Lahir</th>
			<th>Tanggal Nikah</th>
			<th>Memperoleh<br/>Tunjangan</th>
			<th>Pendidikan</th>
		    <th>Pekerjaan</th>
		    <th>Keterangan</th>
		    @if(!$submit)
			<!-- <th>Pilihan</th> -->
			@endif
		</tr>
	    </thead>
	     <tbody>
	    	@foreach ($kel as $k)
	    	<?php
	    		$k2 =  RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_id', $k->riw_id)->where('riw_status',4)->first(); 
	    		if($k->riw_status_tunj == 'TRUE'){
	    			$stat ='YA';	
	    		}elseif($k->riw_status_tunj == 'FALSE'){
	    			$stat ='TIDAK';
	    		}else{
	    			$stat = '-';
	    		}

	    		if($k2->riw_status_tunj == 'TRUE'){
	    			$stat2 ='YA';	
	    		}elseif($k2->riw_status_tunj == 'FALSE'){
	    			$stat2 ='TIDAK';
	    		}else{
	    			$stat2 = '-';
	    		}
	    	?>
	    	<tr class="aktifKeluarga{{$k->riw_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$k2)
		    		<td class="alert-danger">{{$k->riw_nama}}</td>
		    		<td class="alert-danger">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
		    		<td class="alert-danger">{{$k->riw_tgl_ket ? transformDate($k->riw_tgl_ket) : ''}}</td>
		    		<td class="alert-danger">{{$stat}}</td>
		    		<td class="alert-danger">{{$k->riw_pendidikan}}</td>
		    		<td class="alert-danger">{{$k->riw_pekerjaan}}</td>
		    		<td class="alert-danger">{{$k->riw_ket}}</td>
	    		@else
	    			@if($k->riw_nama != $k2->riw_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_nama ? $k2->riw_nama : '-'}}">{{ $k->riw_nama ? $k->riw_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_nama}}</td>
	    			@endif

	    			@if($k->riw_tempat_lahir != $k2->riw_tempat_lahir || $k->riw_tgl_lahir != $k2->riw_tgl_lahir)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_tempat_lahir}}, {{$k2->riw_tgl_lahir ? transformDate($k2->riw_tgl_lahir) : ''}}">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</a></td>
	    			@else
	    				<td>{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
	    			@endif
	    			
		    		@if($k->riw_tgl_ket != $k2->riw_tgl_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_tgl_ket ? transformDate($k2->riw_tgl_ket) : ''}}">{{$k->riw_tgl_ket ? transformDate($k->riw_tgl_ket) : ''}}</a></td>
	    			@else
	    				<td>{{$k->riw_tgl_ket ? transformDate($k->riw_tgl_ket) : ''}}</td>
	    			@endif
		    		@if($k->riw_status_tunj != $k2->riw_status_tunj)
			    		<td class="alert-success"><a class="grey show-option" href="#" title="{{$stat2}}">{{$stat}}</a></td>
		    		@else
			    		<td>{{$stat}}</td>
			    	@endif
			    	@if($k->riw_pendidikan != $k2->riw_pendidikan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pendidikan ? $k2->riw_pendidikan : '-'}}">{{ $k->riw_pendidikan ? $k->riw_pendidikan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pendidikan}}</td>
	    			@endif
		    		@if($k->riw_pekerjaan != $k2->riw_pekerjaan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pekerjaan ? $k2->riw_pekerjaan : '-'}}">{{ $k->riw_pekerjaan ? $k->riw_pekerjaan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pekerjaan}}</td>
	    			@endif
	    			@if($k->riw_ket != $k2->riw_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_ket ? $k2->riw_ket : '-'}}">{{ $k->riw_ket ? $k->riw_ket : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_ket}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit)
	    		<!-- <td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-pasutri" 
					data-nama="{{$k->riw_nama}}" 
					data-id="{{$k->riw_id}}" 
					data-tempat-lahir="{{$k->riw_tempat_lahir}}" 
					data-tanggal-lahir="{{$k->riw_tgl_lahir}}"
					data-tanggal-nikah="{{$k->riw_tgl_ket}}"
					data-status-tunjangan="{{$k->riw_status_tunj}}"
					data-pendidikan="{{$k->riw_pendidikan}}"
					data-pekerjaan="{{$k->riw_pekerjaan}}"
					data-keterangan="{{$k->riw_ket}}"
					onclick="editPasutri(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-keluarga',array($pegawai->peg_id,$k->riw_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td> -->
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	  @endforeach
	    </tbody>
 	</table>
</div>

<div class="modal fade" id="modal-pasutri" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelPasutri"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/pasutri')}}" id="pasutriForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'Nama','required'=>false,'name'=>'riw_nama','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tempat Lahir','required'=>false,'name'=>'riw_tempat_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Lahir','required'=>false,'name'=>'riw_tgl_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Nikah','required'=>false,'name'=>'riw_tgl_ket','placeholder'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Memperoleh Tunjangan','required'=>false,'name'=>'riw_status_tunj','data'=>[
            'TRUE' => 'Ya','FALSE' => 'Tidak' ]])
			<br>
			@include('form.text2',['label'=>'Pendidikan','required'=>false,'name'=>'riw_pendidikan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pekerjaan','required'=>false,'name'=>'riw_pekerjaan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Keterangan','required'=>false,'name'=>'riw_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addPasutri = function(){
	$("#pasutriForm").attr("action","{{ URL::to('riwayat/add/pasutri/') }}");
		@if($pegawai->peg_jenis_kelamin == 'L')
			$("#labelPasutri").text("Tambah Data Istri");
		@else
			$("#labelPasutri").text("Tambah Data Suami");
		@endif
	$(".riw_nama").val("");
	$(".riw_tgl_lahir").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tgl_ket").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val("");
	$(".riw_pendidikan").val("");
	$(".riw_ket").val("");
	$(".riw_status_tunj1").prop('checked', false);
	$(".riw_status_tunj2").prop('checked', false);
}
var editPasutri= function(e){
	var dateAr = $(e).data('tanggal-lahir').split('-');
	var dateAr2 = $(e).data('tanggal-nikah').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];
	var jk = ($(e).data('jenis-kelamin'));
	var sp = ($(e).data('status-perkawinan'));
	var st = ($(e).data('status-tunjangan'));

	$("#pasutriForm").attr("action","{{ URL::to('riwayat/edit/pasutri/') }}/"+$(e).data('id'));
	@if($pegawai->peg_jenis_kelamin == 'L')
		$("#labelPasutri").text("Edit Data Istri");
	@else
		$("#labelPasutri").text("Edit Data Suami");
	@endif
	$(".riw_nama").val($(e).data('nama'));
	
	if(st=='1'){
		$(".riw_status_tunj1").val(st).prop( "checked", true );
	}else if(st=='2'){
		$(".riw_status_tunj2").val(st).prop( "checked", true );
	}

	$(".riw_tgl_lahir").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tgl_ket").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val($(e).data('tempat-lahir'));
	$(".riw_pendidikan").val($(e).data('pendidikan'));
	$(".riw_pekerjaan").val($(e).data('pekerjaan'));
	$(".riw_ket").val($(e).data('keterangan'));
}

</script>