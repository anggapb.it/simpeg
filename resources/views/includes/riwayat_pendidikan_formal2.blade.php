<?php
	use App\Model\RiwayatPendidikan2;
	use App\Model\RiwayatPendidikan;
	use App\Model\TingkatPendidikan;
	use App\Model\Universitas;
	use App\Model\Jurusan;
	use App\Model\Fakultas;

	$riwayat_pend = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->orderBy('tingpend_id')->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Pendidikan Formal</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pendidikan" onclick="addPendidikan()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-pendidikan-formal" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Tingkat Pendidikan</th>
		    <th rowspan="2">Fakultas</th>
		    <th rowspan="2">Jurusan</th>
			<th colspan="3">STTB/Ijazah</th>
			<th colspan="2">Sekolah/Perguruan Tinggi</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr>
		    <th>Nomor</th>
		    <th>Tanggal</th>
		    <th>Nama Kepala Sekolah/Rektor</th>
		    <th>Nama</th>
		    <th>Lokasi (Kab./Kota)</th>
		</tr>
	     </thead>
	     <tbody>
	     	@foreach($riwayat_pend as $rp)
	     	<?php 
	     		$rp2 = RiwayatPendidikan::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rp->riw_pendidikan_id)->first();
	     		$tp= TingkatPendidikan::where('tingpend_id', $rp->tingpend_id)->first(); 
	     		$univ = Universitas::where('univ_id', $rp->univ_id)->first();
	     		$jurusan = Jurusan::where('jurusan_id', $rp->jurusan_id)->first();
	     		$fakultas = Fakultas::where('fakultas_id', $rp->fakultas_id)->first();
	     		if($rp2){
	     			$tp2= TingkatPendidikan::where('tingpend_id', $rp2->tingpend_id)->first();
	     			$univ2 = Universitas::where('univ_id', $rp2->univ_id)->first();
		     		$jurusan2 = Jurusan::where('jurusan_id', $rp2->jurusan_id)->first();
		     		$fakultas2 = Fakultas::where('fakultas_id', $rp2->fakultas_id)->first(); 
	     		}
	     	?>
	     	<tr  class="aktifPendidikan{{$rp->riw_pendidikan_id}}">
	     		<td>{{ $i }}</td>
	     		@if($rp2)
		     		@if($rp->tingpend_id != $rp2->tingpend_id)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$tp2->nm_tingpend ? $tp2->nm_tingpend : '-'}}">{{ $tp->nm_tingpend ? $tp->nm_tingpend : '-' }}</a></td>
		     		@else
		     			<td>{{ $tp->nm_tingpend }}</td>
		     		@endif

		     		@if($rp->fakultas_id != $rp2->fakultas_id)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$fakultas2['fakultas_nm'] ? $fakultas2['fakultas_nm'] : '-'}}">{{$fakultas['fakultas_nm'] ? $fakultas['fakultas_nm'] : '-'}}</a></td>
		     		@else
		     			<td>{{$fakultas['fakultas_nm']}}</td>
		     		@endif

		     		@if($rp->jurusan_id != $rp2->jurusan_id)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$jurusan2['jurusan_nm'] ? $jurusan2['jurusan_nm'] : '-'}}">{{$jurusan['jurusan_nm'] ? $jurusan['jurusan_nm'] : '-'}}</a></td>
		     		@else
		     			<td>{{$jurusan['jurusan_nm']}}</td>
		     		@endif

		     		@if($rp->riw_pendidikan_sttb_ijazah != $rp2->riw_pendidikan_sttb_ijazah)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{ $rp2->riw_pendidikan_sttb_ijazah ? $rp2->riw_pendidikan_sttb_ijazah : '-' }}">{{ $rp->riw_pendidikan_sttb_ijazah ? $rp->riw_pendidikan_sttb_ijazah : '-' }}</a></td>
		     		@else
		     			<td>{{ $rp->riw_pendidikan_sttb_ijazah }}</td>
		     		@endif

		     		@if($rp->riw_pendidikan_tgl != $rp2->riw_pendidikan_tgl)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pendidikan_tgl ? transformDate($rp2->riw_pendidikan_tgl) : '-'}}">{{$rp->riw_pendidikan_tgl ? transformDate($rp->riw_pendidikan_tgl) : '-'}}</a></td>
		     		@else
		     			<td>{{$rp->riw_pendidikan_tgl ? transformDate($rp->riw_pendidikan_tgl) : ''}}</td>
		     		@endif

		     		@if($rp->riw_pendidikan_pejabat != $rp2->riw_pendidikan_pejabat)
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pendidikan_pejabat ? $rp2->riw_pendidikan_pejabat : '-'}}">{{$rp->riw_pendidikan_pejabat ? $rp->riw_pendidikan_pejabat : '-'}}</a></td>
		     		@else
		     			<td>{{$rp->riw_pendidikan_pejabat}}</td>
		     		@endif

		     		@if($rp->riw_pendidikan_nm != $rp2->riw_pendidikan_nm || $univ['univ_nmpti'] != $univ2['univ_nmpti'])
		     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pendidikan_nm ? $rp2->riw_pendidikan_nm : '-'}}{{$univ2['univ_nmpti'] ? $univ2['univ_nmpti'] : '-'}}">{{$rp->riw_pendidikan_nm ? $rp->riw_pendidikan_nm : '-'}}{{$univ['univ_nmpti'] ? $univ['univ_nmpti'] : '-'}}</a></td>
		     		@else
		     			<td>{{$rp->riw_pendidikan_nm}}{{$univ['univ_nmpti']}}</td>
		     		@endif

		     		@if($rp->riw_pendidikan_lokasi != $rp2->riw_pendidikan_lokasi ||  $univ['univ_kota'] != $univ2['univ_kota'])
		     			@if($tp->kd_tingpend == 2)
		     				<td class="alert-success"><a class="grey show-option" href="#" title="{{$univ2['univ_kota'] ? $univ2['univ_kota'] : '-'}}">{{$univ['univ_kota'] ? $univ['univ_kota'] : '-'}}</a></td>
			     		@elseif($tp->kd_tingpend ==1)
			     			<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->riw_pendidikan_lokasi ? $rp2->riw_pendidikan_lokasi : '-'}}">{{$rp->riw_pendidikan_lokasi ? $rp->riw_pendidikan_lokasi : '-'}}</a></td>
			     		@endif
		     		@else
		     			@if($tp->kd_tingpend == 2)
			     			<td>{{$univ['univ_kota']}}</td>
			     		@elseif($tp->kd_tingpend ==1)
			     			<td>{{$rp->riw_pendidikan_lokasi}}</td>
			     		@endif
		     		@endif
	     		@else
		     		<td class="alert-danger">{{ $tp->nm_tingpend }}</td>
		     		<td class="alert-danger">{{$fakultas['fakultas_nm']}}</td>
		     		<td class="alert-danger">{{$jurusan['jurusan_nm']}}</td>
		     		<td class="alert-danger">{{ $rp->riw_pendidikan_sttb_ijazah }}</td>
		     		<td class="alert-danger">{{$rp->riw_pendidikan_tgl ? transformDate($rp->riw_pendidikan_tgl) : ''}}</td>
		     		<td class="alert-danger">{{$rp->riw_pendidikan_pejabat}}</td>
		     		<td class="alert-danger">{{$rp->riw_pendidikan_nm}}{{$univ['univ_nmpti']}}</td>
		     		@if($tp->kd_tingpend == 2)
		     		<td class="alert-danger">{{$univ['univ_kota']}}</td>
		     		@elseif($tp->kd_tingpend ==1)
		     		<td class="alert-danger">{{$rp->riw_pendidikan_lokasi}}</td>
		     		@endif
	     		@endif	     		
	     		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	     		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-pendidikan" 
					data-id="{{$rp->riw_pendidikan_id}}" 
					data-tingpend="{{$rp->tingpend_id}}" 
					data-fakultas="{{$rp->fakultas_id}}" 
					data-jurusan="{{$rp->jurusan_id}}" 
					data-ijazah-no="{{$rp->riw_pendidikan_sttb_ijazah}}"
					data-ijazah-tgl="{{$rp->riw_pendidikan_tgl}}"
					data-ijazah-pejabat="{{$rp->riw_pendidikan_pejabat}}"
					data-nama-sekolah="{{$rp->riw_pendidikan_nm}}"
					data-nama-universitas="{{$rp->univ_id}}"
					data-lokasi-sekolah="{{$rp->riw_pendidikan_lokasi}}"
					data-lokasi-universitas="{{$univ['univ_kota']}}"
					data-kode="{{$tp->kd_tingpend}}"
					onclick="editPendidikan(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-pendidikan',array($pegawai->peg_id,$rp->riw_pendidikan_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	     	</tr>
	     	<?php $i++;?>
	     	@endforeach
	     </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-pendidikan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelPendidikan"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/pendidikan')}}" id="pendidikanForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			<div class="bs-callout bs-callout-warning hidden">
			  <h4>Edit Data Gagal!</h4>
			  <p>Data Harus Lengkap!</p>
			</div>
			@include('form.select2_modal',['label'=>'Tingkat Pendidikan','required'=>true,'name'=>'tingpend_id','data'=>
          	TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Fakultas','required'=>false,'name'=>'fakultas_id','data'=>
          	Fakultas::orderBy('fakultas_nm')->lists('fakultas_nm','fakultas_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.select2_modal',['label'=>'Jurusan','required'=>false,'name'=>'jurusan_id','data'=>
          	Jurusan::orderBy('jurusan_nm')->lists('jurusan_nm','jurusan_id') ,'empty'=>'-Pilih-'])
          	<br>
          	@include('form.text2',['label'=>'Nomor Ijazah/STTB','required'=>false,'name'=>'riw_pendidikan_sttb_ijazah','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Ijazah/STTB','required'=>false,'name'=>'riw_pendidikan_tgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nama Kepala Sekolah/Rektor','required'=>false,'name'=>'riw_pendidikan_pejabat','placeholder'=>''])
			<br>
			<div id="nama_sekolah" style="visible:none">
			@include('form.text2',['label'=>'Nama Sekolah','required'=>false,'name'=>'riw_pendidikan_nm','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Lokasi Sekolah','required'=>false,'name'=>'riw_pendidikan_lokasi','placeholder'=>''])
			<br>
			</div>
			<div id="nama_pt" style="visible:none">
			@include('form.select2_modal',['label'=>'Nama Perguruan Tinggi','required'=>false,'name'=>'univ_id','data'=>
          	Universitas::orderBy('univ_nmpti')->lists('univ_nmpti','univ_id') ,'empty'=>'-Pilih-'])
          	<br>
          	</div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addPendidikan = function(){
	$("#pendidikanForm").attr("action","{{ URL::to('riwayat/add/pendidikan/') }}");
	$("#labelPendidikan").text("Tambah Riwayat Pendidikan Formal");
	$("#tingpend_id").val("").select2().on("change",function() {
		var ting = $("#tingpend_id").val();
		console.log(ting);
		if(ting == 21 || ting == 22 || ting == 23){
			$("#fakultas_id").val("").select2().prop('disabled', true);
			$("#nama_pt").hide();
			$("#nama_sekolah").show();
		}else{
			$("#nama_sekolah").hide();
			$("#nama_pt").show();
			$("#fakultas_id").val("").select2().prop('disabled', false);
		}
	});

	$("#jurusan_id").val("").select2();
	$(".riw_pendidikan_sttb_ijazah").val("");
	$(".riw_pendidikan_pejabat").val("");
	$(".riw_pendidikan_lokasi").val("");
	$(".riw_pendidikan_tgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#univ_id").val("").select2();
	$(".riw_pendidikan_nm").val("");

	$('#pendidikanForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}
var editPendidikan = function(e){
	var dateAr = $(e).data('ijazah-tgl').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var kode = $(e).data('kode');
	$("#pendidikanForm").attr("action","{{ URL::to('riwayat/edit/pendidikan/') }}/"+$(e).data('id'));
	$("#labelPendidikan").text("Edit Riwayat Pendidikan Formal");

	$("#tingpend_id").val($(e).data('tingpend')).select2().on("change",function() {
		var ting = $("#tingpend_id").val();
		if(ting == 21 || ting == 22 || ting == 23){
			$("#fakultas_id").val($(e).data('fakultas')).select2().prop('disabled', true);
			$("#nama_pt").hide();
			$("#nama_sekolah").show();
		}else{
			$("#nama_sekolah").hide();
			$("#nama_pt").show();
			$("#fakultas_id").val($(e).data('fakultas')).select2().prop('disabled', false);
		}
	});

	$("#jurusan_id").val($(e).data('jurusan')).select2();
	$(".riw_pendidikan_sttb_ijazah").val($(e).data('ijazah-no'));
	$(".riw_pendidikan_pejabat").val($(e).data('ijazah-pejabat'));
	$(".riw_pendidikan_lokasi").val($(e).data('lokasi'));
	$(".riw_pendidikan_tgl").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#univ_id").val($(e).data('nama-universitas')).select2();
	$(".riw_pendidikan_nm").val($(e).data('nama-sekolah'));

	if(kode == 1){
		$("#fakultas_id").val($(e).data('fakultas')).select2().prop('disabled', true);
		$("#nama_pt").hide();
		$("#nama_sekolah").show();
	}else if(kode == 2){
		$("#nama_sekolah").hide();
		$("#nama_pt").show();
		$("#fakultas_id").val($(e).data('fakultas')).select2().prop('disabled', false);
	}

	$('#pendidikanForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}


</script>