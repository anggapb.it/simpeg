<?php

	use App\Model\RiwayatNonFormal2;
	use App\Model\RiwayatNonFormal;

	$pend_non = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->orderBy('non_tgl_mulai')->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Pendidikan Non Formal</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pendidikan-non" onclick="addPendidikanNon()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<table id="tabel-riwayat-pendidikan-nonformal" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Kursus/Seminar/LokaKarya</th>
		  	<th colspan="2">Tanggal</th>
			<th colspan="3">Ijazah/Tanda Lulus/Surat Keterangan</th>
			<th rowspan="2">Instansi<br/>Penyelenggara</th>
			<th rowspan="2">Tempat</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="2">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
			<th>Mulai</th>
			<th>selesai</th>
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Nama Pejabat</th>
		</tr>
	     </thead>
	    <tbody>
	    	@foreach($pend_non as $pn)
	    	<?php
	    		$pn2 = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->where('non_id', $pn->non_id)->first();
	    	?>
	    	<tr class="aktifPendidikan{{$pn->non_id}}">	
	    		<td>{{$i}}</td>
	    		@if(!$pn2)
	    		<td class="alert-danger">{{$pn->non_nama}}</td>
	    		<td class="alert-danger">{{$pn->non_tgl_mulai ? transformDate($pn->non_tgl_mulai) : ''}}</td>
	    		<td class="alert-danger">{{$pn->non_tgl_selesai ? transformDate($pn->non_tgl_selesai) : ''}}</td>
	    		<td class="alert-danger">{{$pn->non_sttp}}</td>
	    		<td class="alert-danger">{{$pn->non_sttp_tanggal ? transformDate($pn->non_sttp_tanggal) : ''}}</td>
	    		<td class="alert-danger">{{$pn->non_sttp_pejabat}}</td>
	    		<td class="alert-danger">{{$pn->non_penyelenggara}}</td>
	    		<td class="alert-danger">{{$pn->non_tempat}}</td>
	    		@else
	    			@if($pn->non_nama != $pn2->non_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_nama ? $pn2->non_nama : '-'}}">{{$pn->non_nama ? $pn->non_nama : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_nama}}</td>
	    			@endif

	    			@if($pn->non_tgl_mulai != $pn2->non_tgl_mulai)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_tgl_mulai ? transformDate($pn2->non_tgl_mulai) : '-'}}">{{$pn->non_tgl_mulai ? transformDate($pn->non_tgl_mulai) : ''}}</a></td>
	    			@else
	    				<td>{{$pn->non_tgl_mulai ? transformDate($pn->non_tgl_mulai) : ''}}</td>
	    			@endif

	    			@if($pn->non_tgl_selesai != $pn2->non_tgl_selesai)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_tgl_selesai ? transformDate($pn2->non_tgl_selesai) : '-'}}">{{$pn->non_tgl_selesai ? transformDate($pn->non_tgl_selesai) : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_tgl_selesai ? transformDate($pn->non_tgl_selesai) : ''}}</td>
	    			@endif

	    			@if($pn->non_sttp != $pn2->non_sttp)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_sttp ? $pn2->non_sttp: '-'}}">{{$pn->non_sttp ? $pn->non_sttp : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_sttp}}</td>
	    			@endif

	    			@if($pn->non_sttp_tanggal != $pn2->non_sttp_tanggal)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_sttp_tanggal ? transformDate($pn2->non_sttp_tanggal) : '-'}}">{{$pn->non_sttp_tanggal ? transformDate($pn->non_sttp_tanggal) : ''}}</a></td>
	    			@else
	    				<td>{{$pn->non_sttp_tanggal ? transformDate($pn->non_sttp_tanggal) : ''}}</td>
	    			@endif
	    			@if($pn->non_sttp_pejabat != $pn2->non_sttp_pejabat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_sttp_pejabat ? $pn2->non_sttp_pejabat : '-'}}">{{$pn->non_sttp_pejabat ? $pn->non_sttp_pejabat : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_sttp_pejabat}}</td>
	    			@endif
	    			@if($pn->non_penyelenggara != $pn2->non_penyelenggara)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_penyelenggara ? $pn2->non_penyelenggara : '-'}}">{{$pn->non_penyelenggara ? $pn->non_penyelenggara : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_penyelenggara}}</td>
	    			@endif
	    			@if($pn->non_tempat != $pn2->non_tempat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$pn2->non_tempat ? $pn2->non_tempat : '-'}}">{{$pn->non_tempat ? $pn->non_tempat : '-'}}</a></td>
	    			@else
	    				<td>{{$pn->non_tempat}}</td>
	    			@endif
	    		@endif

	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-pendidikan-non" 
					data-nama="{{$pn->non_nama}}" 
					data-id="{{$pn->non_id}}" 
					data-tgl-mulai="{{$pn->non_tgl_mulai}}" 
					data-tgl-selesai="{{$pn->non_tgl_selesai}}" 
					data-sttp-no="{{$pn->non_sttp}}"
					data-sttp-tgl="{{$pn->non_sttp_tanggal}}"
					data-sttp-pejabat="{{$pn->non_sttp_pejabat }}"
					data-penyelenggara="{{$pn->non_penyelenggara}}"
					data-tempat="{{$pn->non_tempat}}"
					data-jenis="{{$pn->diklat_jenis_id}}"
					data-jam="{{$pn->non_jumlah_jam}}"
					onclick="editPendidikanNon(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<a href="{{url('/riwayat/delete-pendidikan-non',array($pegawai->peg_id,$pn->non_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td>
				@endif
	    	</tr>	
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>	
 	</table>
</div>
<div class="modal fade" id="modal-pendidikan-non" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelPendidikanNon"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/pendidikan-non')}}" id="pendidikanNonformalForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
          	@include('form.text2',['label'=>'Nama Kursus/Seminar/LokaKarya','required'=>false,'name'=>'non_nama','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Mulai','required'=>false,'name'=>'non_tgl_mulai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Selesai','required'=>false,'name'=>'non_tgl_selesai','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Nomor Ijazah/Tanda Lulus/Surat Keterangan','required'=>false,'name'=>'non_sttp','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Ijazah/Tanda Lulus/Surat Keterangan','required'=>false,'name'=>'non_sttp_tanggal','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan Ijazah/Tanda Lulus/Surat Keterangan','required'=>false,'name'=>'non_sttp_pejabat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Instansi Penyelenggara','required'=>false,'name'=>'non_penyelenggara','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tempat','required'=>false,'name'=>'non_tempat','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addPendidikanNon = function(){
	$("#pendidikanNonformalForm").attr("action","{{ URL::to('riwayat/add/pendidikan-non/') }}");
	$("#labelPendidikanNon").text("Tambah Data Pendidikan Non Formal");
	$(".non_nama").val("");
	$(".non_sttp").val("");
	$(".non_penyelenggara").val("");
	$(".non_tgl_mulai").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_tgl_selesai").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_sttp_tanggal").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_tempat").val("");
}
var editPendidikanNon = function(e){
	var dateAr = $(e).data('tgl-mulai').split('-');
	var dateAr2 = $(e).data('tgl-selesai').split('-');
	var dateAr3 = $(e).data('sttp-tgl').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];
	var newDate3 = dateAr3[2] + '-' + dateAr3[1] + '-' + dateAr3[0];

	$("#pendidikanNonformalForm").attr("action","{{ URL::to('riwayat/edit/pendidikan-non/') }}/"+$(e).data('id'));
	$("#labelPendidikanNon").text("Edit Data Pendidikan Non Formal");
	$(".non_nama").val($(e).data('nama'));
	$(".non_tgl_mulai").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_tgl_selesai").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_sttp").val($(e).data('sttp-no'));
	$(".non_sttp_tanggal").val(newDate3).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".non_sttp_pejabat").val($(e).data('sttp-pejabat'));
	$(".non_penyelenggara").val($(e).data('penyelenggara'));
	$(".non_tempat").val($(e).data('tempat'));
}
</script>