<?php

	use App\Model\Penghargaan;
	use App\Model\RiwayatPenghargaan;
	use App\Model\RiwayatPenghargaan2;

	$penghargaan = RiwayatPenghargaan::where('peg_id', $pegawai->peg_id)->get();
	$i=1;
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat Penghargaan</h3><br>

	<div class="pull-right tableTools-container">
		@if(!$submit)
		<!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-penghargaan" onclick="addPenghargaan()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button> -->
		@endif
	</div>
	<table id="tabel-riwayat-penghargaan"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
		<tr align="center">
			<th rowspan="2">No.</th>
			<th rowspan="2">Nama Penghargaan</th>
			<th colspan="3">Surat Keputusan</th>
			<th colspan="2">Instansi Penyelenggara</th>
			@if(!$submit)
			<!-- <th rowspan="2">Pilihan</th> -->
			@endif
		</tr>
		<tr align="center">
			<th>Nomor</th>
			<th>Tanggal</th>
			<th>Jabatan Penandatangan</th>
			<th>Instansi</th>
			<th>Lokasi</th>
		</tr>
	    </thead>
	    <tbody>
	    	@foreach ($penghargaan as $p)
	    	<?php $np= Penghargaan::where('penghargaan_id', $p->penghargaan_id)->first(); 
	    		$p2 = RiwayatPenghargaan2::where('peg_id', $pegawai->peg_id)->where('riw_penghargaan_id', $p->riw_penghargaan_id)->first();

	    		if($p2){
	    			$np2= Penghargaan::where('penghargaan_id', $p2->penghargaan_id)->first(); 
	    		}
	    	?>
	    	<tr class="aktif{{$p->riw_penghargaan_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$p2)
		    		<td class="alert-danger">{{$np['penghargaan_nm']}}</td>
		    		<td class="alert-danger">{{$p->riw_penghargaan_sk}}</td>
		    		<td class="alert-danger">{{$p->riw_penghargaan_tglsk ? transformDate($p->riw_penghargaan_tglsk) : ''}}</td>
		    		<td class="alert-danger">{{$p->riw_penghargaan_jabatan}}</td>
		    		<td class="alert-danger">{{$p->riw_penghargaan_instansi}}</td>
		    		<td class="alert-danger">{{$p->riw_penghargaan_lokasi}}</td>
	    		@else
	    			@if($p->penghargaan_id != $p2->penghargaan_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$np2['penghargaan_nm'] ? $np2['penghargaan_nm'] : '-'}}">{{ $np['penghargaan_nm'] ? $np['penghargaan_nm'] : '-'}}</a></td>
	    			@else
			    		<td>{{$np['penghargaan_nm']}}</td>
	    			@endif
	    			@if($p->riw_penghargaan_sk != $p2->riw_penghargaan_sk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$p2->riw_penghargaan_sk ? $p2->riw_penghargaan_sk: '-'}}">{{ $p->riw_penghargaan_sk ? $p->riw_penghargaan_sk : '-'}}</a></td>
	    			@else
		    			<td>{{$p->riw_penghargaan_sk}}</td>
		    		@endif
		    		@if($p->riw_penghargaan_tglsk != $p2->riw_penghargaan_tglsk)
		    			<td class="alert-success"><a class="grey show-option" href="#" title="{{$p2->riw_penghargaan_tglsk ? transformDate($p2->riw_penghargaan_tglsk) : ''}}">{{$p->riw_penghargaan_tglsk ? transformDate($p->riw_penghargaan_tglsk) : ''}}</a></td>
		    		@else
		    			<td>{{$p->riw_penghargaan_tglsk ? transformDate($p->riw_penghargaan_tglsk) : ''}}</td>
		    		@endif
		    		@if($p->riw_penghargaan_jabatan != $p2->riw_penghargaan_jabatan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$p2->riw_penghargaan_jabatan ? $p2->riw_penghargaan_jabatan: '-'}}">{{ $p->riw_penghargaan_jabatan ? $p->riw_penghargaan_jabatan : '-'}}</a></td>
	    			@else
		    			<td>{{$p->riw_penghargaan_jabatan}}</td>
		    		@endif
		    		@if($p->riw_penghargaan_instansi != $p2->riw_penghargaan_instansi)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$p2->riw_penghargaan_instansi ? $p2->riw_penghargaan_instansi: '-'}}">{{ $p->riw_penghargaan_instansi ? $p->riw_penghargaan_instansi : '-'}}</a></td>
	    			@else
		    			<td>{{$p->riw_penghargaan_instansi}}</td>
		    		@endif
		    		@if($p->riw_penghargaan_lokasi != $p2->riw_penghargaan_lokasi)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$p2->riw_penghargaan_lokasi ? $p2->riw_penghargaan_lokasi: '-'}}">{{ $p->riw_penghargaan_lokasi ? $p->riw_penghargaan_lokasi : '-'}}</a></td>
	    			@else
		    			<td>{{$p->riw_penghargaan_lokasi}}</td>
		    		@endif
	    		@endif
	    		@if(!$submit)
	    		<!-- <td>
					<button class="btn btn-warning btn-xs"
							data-toggle="modal" data-target="#modal-penghargaan" 
							data-nama="{{$p->penghargaan_id}}" 
							data-id="{{$p->riw_penghargaan_id}}" 
							data-sk="{{$p->riw_penghargaan_sk}}" 
							data-tgl-sk="{{$p->riw_penghargaan_tglsk ? transformDate($p->riw_penghargaan_tglsk) : ''}}"
							data-jabatan="{{$p->riw_penghargaan_jabatan}}"
							data-instansi="{{$p->riw_penghargaan_instansi}}"
							data-lokasi="{{$p->riw_penghargaan_lokasi}}"
							onclick="editPenghargaan(this)">
							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
							Edit</button>

		    		<a href="{{url('/riwayat/delete-penghargaan',array($pegawai->peg_id,$p->riw_penghargaan_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td> -->
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	 @endforeach
	    </tbody>
 	</table>
</div>
<div class="modal fade" id="modal-penghargaan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelPenghargaan"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/penghargaan')}}" id="userForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'Nama Penghargaan','required'=>false,'name'=>'penghargaan_id','data'=>
          	Penghargaan::orderBy('penghargaan_nm')->lists('penghargaan_nm','penghargaan_id') ,'empty'=>'-Pilih-'])
          	<br>
			@include('form.text2',['label'=>'Nomor SK','required'=>false,'name'=>'riw_penghargaan_sk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_penghargaan_tglsk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'riw_penghargaan_jabatan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Instansi','required'=>false,'name'=>'riw_penghargaan_instansi','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Lokasi','required'=>false,'name'=>'riw_penghargaan_lokasi','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addPenghargaan = function(){
	$("#userForm").attr("action","{{ URL::to('riwayat/add/penghargaan/') }}");
	$("#labelPenghargaan").text("Tambah Data Penghargaan");
	$("#penghargaan_id").select2().val("");
	$("#riw_penghargaan_sk").val("");
	$("#riw_penghargaan_tglsk").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_penghargaan_jabatan").val("");
	$("#riw_penghargaan_instansi").val("");
	$("#riw_penghargaan_lokasi").val("");
}
var editPenghargaan = function(e){
	$("#userForm").attr("action","{{ URL::to('riwayat/edit/penghargaan/') }}/"+$(e).data('id'));
	$("#labelPenghargaan").text("Edit Data Penghargaan");
	$("#penghargaan_id").val($(e).data('nama')).select2();
	$("#riw_penghargaan_sk").val($(e).data('sk'));
	$("#riw_penghargaan_tglsk").val($(e).data('tgl-sk')).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$("#riw_penghargaan_jabatan").val($(e).data('jabatan'));
	$("#riw_penghargaan_instansi").val($(e).data('instansi'));
	$("#riw_penghargaan_lokasi").val($(e).data('lokasi'));
}

</script>