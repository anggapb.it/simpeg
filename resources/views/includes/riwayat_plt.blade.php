<?php

	use App\Model\PelaksanaStruktural;

	$struk = PelaksanaStruktural::where('peg_id',$pegawai->peg_id)->get();
?>

<div class="col-xs-12">
	<h3 align="middle">Riwayat PLT/PLH</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-plt" onclick="addPLT()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
		<input type="hidden" id="token" value="{{ csrf_token() }}">
		<thead>
		<tr align="center">
			<th>No.</th>
			<th>Nama Jabatan</th>
			<th>Unit Kerja</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
			<th>Jenis</th>
			<th>Status</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Aksi</th>
			@endif
		</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			?>
			@foreach($struk as $s)
			<tr>
				<td>{{$no++}}</td>
				<td>{{empty($s->jabatan->jabatan_nama) ? $s->jabatan_nama : $s->jabatan->jabatan_nama}}</td>
				<td>{{$s->unit_kerja}}</td>
				<td>{{getFullDate($s->tanggal_mulai)}}</td>
				<td>{{getFullDate($s->tanggal_selesai)}}</td>
				<td>{{strtoupper($s->jenis)}}</td>
				<td>{{($s->status == 't') ? 'Aktif' : 'Inaktif'}}</td>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
				<td>
					<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-plt" 
					data-id="{{$s->rpelaksana_id}}" 
					data-jabatan_nama="{{$s->jabatan->jabatan_nama}}" 
					data-unit_kerja="{{$s->unit_kerja}}" 
					data-mulai="{{$s->tanggal_mulai}}" 
					data-selesai="{{$s->tanggal_selesai}}" 
					data-jenis="{{$s->jenis}}" 
					data-status="{{$s->status}}" 
					data-jabatan_id="{{$s->jabatan_id}}"
					onclick="editPLT(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-plt',$s->peg_id).'/'.$s->rpelaksana_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Riwayat Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
				</td>
			@endif
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>

<div class="modal fade" id="modal-plt" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelPlt"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/plt')}}" id="formPlt">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.select2_modal',['label'=>'SKPD','required'=>false,'name'=>'skpd','data'=>
				App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
			<br>
			@include('form.select2_modal',['label'=>'Nama Jabatan','required'=>false,'name'=>'jabatan_nama_select','data'=>[''=>''],'empty'=>''])
			<br>
			@include('form.text2',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_nama'])
			<br>
			@include('form.text2',['label'=>'Unit Kerja','required'=>true,'name'=>'unit_kerja'])
			<br>
			@include('form.date2',['label'=>'Tanggal Mulai','required'=>true,'name'=>'tgl_mulai_plt'])
			<br>
			@include('form.date2',['label'=>'Tanggal Selesai','required'=>true,'name'=>'tgl_selesai_plt'])
			<br>
			@include('form.radio_modal',['label'=>'Jenis','required'=>true,'name'=>'jenis','data'=>['plt'=>'PLT','plh'=>'PLH'],'empty'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Status','required'=>true,'name'=>'status','data'=>['1'=>'Aktif','0'=>'Inaktif'],'empty'=>''])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

var addPLT = function(){
var data_jabatan = '';
	$("#formPlt").attr("action","{{ URL::to('riwayat/add/plt/') }}");
	$("#labelPlt").html("Tambah Riwayat PLT/PLH");
	$("#jabatan_nama,#unit_kerja,#tgl_mulai_plt,#tgl_selesai_plt,#skpd,#jabatan_nama_select").val("");
	$("#tgl_mulai_plt,#tgl_selesai_plt").datepicker({autoclose: true});
	$(".jenis1,.jenis2").prop('checked', false);
	$(".status1,.status2").prop('checked', false);

	$('#skpd').select2({placeholder: 'Pilih SKPD'}).on('change', function (e){
		var skpd = $('#skpd').val();
		$("#jabatan_nama,#unit_kerja").val("");
		$("#jabatan_nama_select").empty();
		$("#jabatan_nama_select").select2({
			placeholder: 'Pilih Jabatan'
		});
		$.ajax({
			type: "GET",
			cache: false,
			url: "{{ URL::to('pegawai/getJabatan') }}",
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');
				if(token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			data: {"jenis_jabatan":2, "unit_kerja_id":'all', "satuan_kerja_id":skpd},
			dataType: 'json',
			success: function(data) {
				data_jabatan = data;
				$('#jabatan_nama_select').append($('<option></option>').attr('value', '').text(''));
				for(row in data) {
					$('#jabatan_nama_select').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
				}
			},
			error: function (e){
				console.log(e);
				alert("ERROR!");
			},
		});
	});

	$('#jabatan_nama_select').select2({placeholder: 'Pilih Jabatan'}).on('change', function (e){
		changeUnitKerja(data_jabatan);
	});
}

var editPLT = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	var status = $(e).data('status');
	var jenis = $(e).data('jenis');
	if(id == ''){
		$("#formPlt").attr("action","{{ URL::to('riwayat/add/plt/') }}");
	}else{
		$("#formPlt").attr("action","{{ URL::to('riwayat/edit/plt/') }}/"+id);
	}

	$("#labelPlt").html("Edit Riwayat PLT/PLH");
	$("#jabatan_nama").val($(e).data('jabatan_nama'));
	$("#unit_kerja").val($(e).data('unit_kerja'));
	$("#tgl_mulai_plt").val($(e).data('mulai'));
	$("#tgl_selesai_plt").val($(e).data('mulai'));
	$("#tgl_mulai_plt,#tgl_selesai_plt").datepicker({autoclose: true});
	if(jenis == 'plt') {
		$(".jenis1").prop('checked', true);
	}
	else if(jenis == 'plh') {
		$(".jenis2").prop('checked', true);
	}
	if(status == 1) {
		$(".status1").prop('checked', true);
	}
	else if(status == 0) {
		$(".status2").prop('checked', true);
	}

	$('#skpd,#jabatan_nama_select').val("");
	$('#skpd').select2({placeholder: 'Pilih SKPD'}).on('change', function (e){
		var skpd = $('#skpd').val();
		$("#jabatan_nama,#unit_kerja").val("");
		$("#jabatan_nama_select").empty();
		$("#jabatan_nama_select").select2({
			placeholder: 'Pilih Jabatan'
		});
		$.ajax({
			type: "GET",
			cache: false,
			url: "{{ URL::to('pegawai/getJabatan') }}",
			beforeSend: function (xhr) {
				var token = $('meta[name="csrf_token"]').attr('content');
				if(token) {
					return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				}
			},
			data: {"jenis_jabatan":2, "unit_kerja_id":'all', "satuan_kerja_id":skpd},
			dataType: 'json',
			success: function(data) {
				data_jabatan = data;
				for(row in data) {
					$('#jabatan_nama_select').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
				}
			},
			error: function (e){
				console.log(e);
				alert("ERROR!");
			},
		});
	});

	$('#jabatan_nama_select').select2({placeholder: 'Pilih Jabatan'}).on('change', function (e){
		changeUnitKerja(data_jabatan);
	});
}

function changeUnitKerja(data_jabatan) {
	var jabatan_id = $('#jabatan_nama_select').val()
	for(row in data_jabatan) {
		if(data_jabatan[row].jabatan_id == jabatan_id) {
			$('#jabatan_nama').val(data_jabatan[row].jabatan_nama);
			$('#unit_kerja').val(data_jabatan[row].unit_kerja_nama);
		}
	}
}
</script>
