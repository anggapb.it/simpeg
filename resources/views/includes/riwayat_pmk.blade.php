<?php
	use App\Model\Pmk;
	use App\Model\Pmk_skpd;
	use App\Model\Golongan;
	use App\Model\Gaji;
	use App\Model\GajiTahun;

	$riwayat_pmk = Pmk_skpd::where('peg_id', $pegawai->peg_id)->orderBy('gol_id')->get();
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$i=1;
?>
<div class="col-xs-12">
	<h3 align="middle">Penambahan Masa Kerja</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
            @if(!$riwayat_pmk->count())
            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pmk" onclick="addPmk()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
            @endif
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-riwayat-pmk" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	    <thead>
		<tr align="center">
			<th rowspan="3">No.</th>
			<th rowspan="3">Gol. Ruang<br/>Pangkat</th>
			<th colspan="4">Masa Kerja Gol.</th>
			<th colspan="2" rowspan="2">PMK<br>(MK baru - MK lama)</th>
		  	<th colspan="2" rowspan="2">Gaji Pokok</th>
		  	<th colspan="3" rowspan="2">Surat Keputusan</th>
			<th colspan="2" rowspan="2">TMT</th>
			<th rowspan="3">Unit Kerja</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th rowspan="3">Pilihan</th>
			@endif
		</tr>
		<tr align="center">
		    <th colspan="2">Lama</th>
		    <th colspan="2">Baru</th>
		</tr>
		<tr align="center">
			<th>Thn</th>
			<th>Bln</th>
			<th>Thn</th>
			<th>Bln</th>
			<th>Thn</th>
			<th>Bln</th>
			<th>Lama</th>
			<th>Baru</th>
		    <th>Nomor</th>
		    <th>Tanggal</th>
			<th>Jabatan<br/>Penandatangan</th>
			<th>Lama</th>
			<th>Baru</th>
		</tr>
	     </thead>
	    <tbody>
	    	@foreach($riwayat_pmk as $rp)
	    		<?php 
	    			$rp2 = Pmk::where('peg_id', $pegawai->peg_id)->where('pmk_gapok_lama', $rp->pmk_gapok_lama)->where('gol_id', $rp->gol_id)->first();
	    			$gol = Golongan::where('gol_id', $rp->gol_id)->first(); 
	    			if (!$gol) $gol = new Golongan;
	    			// dd($rp->pmk_tahun);
	    			if(!$rp->pmk_tahun || $rp->pmk_tahun =='-'){
	    				$rp->pmk_tahun = 0;
	    			}
					$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $rp->pmk_tahun)->first();

					if($rp2){
						if(!$rp2->pmk_tahun || $rp2->pmk_tahun =='-'){
                            $rp2->pmk_tahun = 0;
                        }

						$gol2 = Golongan::where('gol_id', $rp2->gol_id)->first(); 
	    				if (!$gol2) $gol2 = new Golongan;
						$gaji2 = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp2->gol_id)->where('gaji_masakerja', $rp2->pmk_tahun)->first();
					}
	    		?>
	    	<tr class="aktifKgb{{$rp->peg_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$rp2)
					<td class="alert-danger">{{$gol->nm_gol.' - '.$gol->nm_pkt}}</td>
		    		<td class="text-center alert-danger">{{$rp->masa_kerja_lama_thn}}</td>
		    		<td class="text-center alert-danger">{{$rp->masa_kerja_lama_bln}}</td>
		    		<td class="text-center alert-danger">{{$rp->masa_kerja_baru_thn}}</td>
		    		<td class="text-center alert-danger">{{$rp->masa_kerja_baru_bln}}</td>
		    		<td class="text-center alert-danger">{{strlen($rp->pmk_tahun)==1 ? '0'.$rp->pmk_tahun : $rp->pmk_tahun}}</td>
		    		<td class="text-center alert-danger">{{strlen($rp->pmk_bulan)==1 ? '0'.$rp->pmk_bulan : $rp->pmk_bulan}}</td>
		    		<td class="alert-danger">Rp {{number_format($rp->pmk_gapok_lama,2)}}</td>
		    		<td class="alert-danger">Rp {{number_format($rp->pmk_gapok_baru,2)}}</td>
		    		<td class="alert-danger">{{$rp->pmk_nosk}}</td>
		    		<td class="alert-danger">{{ $rp->pmk_sktgl ? date('d-m-Y', strtotime($rp->pmk_sktgl)) : ''}}</td>
		    		<td class="alert-danger">{{$rp->pmk_ttd_pejabat}}</td>
		    		<td class="alert-danger">{{$rp->pmk_tmt_lama ? date('d-m-Y', strtotime($rp->pmk_tmt_lama)) : ''}}</td>
		    		<td class="alert-danger">{{$rp->pmk_tmt_baru ? date('d-m-Y', strtotime($rp->pmk_tmt_baru)) : ''}}</td>
		    		<td class="alert-danger">{{$rp->pmk_unit_kerja}}</td>
		    		<!-- <td class="alert-danger">{{$rp->status_data}}</td> -->
	    		@else
	    			@if($rp->gol_id != $rp2->gol_id)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$gol2->nm_gol ? $gol2->nm_gol : '-'}}">{{ $gol->nm_gol ? $gol->nm_gol.' - '.$gol->nm_pkt : '-' }}</a></td>
	    			@else
	    				<td>{{$gol['nm_gol'] ? $gol->nm_gol.' - '.$gol->nm_pkt : ''}}</td>
	    			@endif
	    			@if($rp->masa_kerja_lama_thn != $rp2->masa_kerja_lama_thn)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->masa_kerja_lama_thn ? $rp2->masa_kerja_lama_thn : ''}}">{{ $rp->masa_kerja_lama_thn ? $rp->masa_kerja_lama_thn : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{$rp->masa_kerja_lama_thn}}</td>
	    			@endif
	    			@if($rp->masa_kerja_lama_bln != $rp2->masa_kerja_lama_bln)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->masa_kerja_lama_bln ? $rp2->masa_kerja_lama_bln : ''}}">{{ $rp->masa_kerja_lama_bln ? $rp->masa_kerja_lama_bln : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{$rp->masa_kerja_lama_bln}}</td>
	    			@endif
					@if($rp->masa_kerja_baru_thn != $rp2->masa_kerja_baru_thn)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->masa_kerja_baru_thn ? $rp2->masa_kerja_baru_thn : ''}}">{{ $rp->masa_kerja_baru_thn ? $rp->masa_kerja_baru_thn : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{$rp->masa_kerja_baru_thn}}</td>
	    			@endif
	    			@if($rp->masa_kerja_baru_bln != $rp2->masa_kerja_baru_bln)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->masa_kerja_baru_bln ? $rp2->masa_kerja_baru_bln : ''}}">{{ $rp->masa_kerja_baru_bln ? $rp->masa_kerja_baru_bln : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{$rp->masa_kerja_baru_bln}}</td>
	    			@endif
	    			@if($rp->pmk_tahun != $rp2->pmk_tahun)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_tahun ? $rp2->pmk_tahun : ''}}">{{ $rp->pmk_tahun ? $rp->pmk_tahun : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{strlen($rp->pmk_tahun)==1 ? '0'.$rp->pmk_tahun : $rp->pmk_tahun}}</td>
	    			@endif
	    			@if($rp->pmk_bulan != $rp2->pmk_bulan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_bulan ? $rp2->pmk_bulan : ''}}">{{ $rp->pmk_bulan ? $rp->pmk_bulan : '' }}</a></td>
	    			@else
	    				<td class="text-center">{{strlen($rp->pmk_bulan)==1 ? '0'.$rp->pmk_bulan : $rp->pmk_bulan}}</td>
	    			@endif
	    			@if($rp->pmk_gapok_lama != $rp2->pmk_gapok_lama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_gapok_lama ? $rp2->pmk_gapok_lama : ''}}">{{ $rp->pmk_gapok_lama ? $rp->pmk_gapok_lama : '' }}</a></td>
	    			@else
						<td>Rp {{number_format($rp->pmk_gapok_lama,2)}}</td>
	    			@endif
	    			@if($rp->pmk_gapok_baru != $rp2->pmk_gapok_baru)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_gapok_baru ? $rp2->pmk_gapok_baru : ''}}">{{ $rp->pmk_gapok_baru ? $rp->pmk_gapok_baru : '' }}</a></td>
	    			@else
						<td>Rp {{number_format($rp->pmk_gapok_baru,2)}}</td>
	    			@endif
	    			<!-- @if($rp->gol_id != $rp2->gol_id || $rp->pmk_tahun != $rp2->pmk_tahun)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="Rp {{number_format($gaji2['gaji_pokok'],2)}}">Rp {{number_format($gaji['gaji_pokok'],2)}}</a></td>
	    			@else
	    				<td>Rp {{number_format($gaji['gaji_pokok'],2)}}</td>
	    			@endif -->
	    			@if($rp->pmk_nosk != $rp2->pmk_nosk)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_nosk ? $rp2->pmk_nosk : '-'}}">{{ $rp->pmk_nosk ? $rp->pmk_nosk : '-' }}</a></td>
	    			@else
	    				<td>{{$rp->pmk_nosk}}</td>
	    			@endif
	    			@if($rp->pmk_sktgl != $rp2->pmk_sktgl)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_sktgl ? transformDate($rp2->pmk_sktgl) : '-'}}">{{ $rp->pmk_sktgl ? transformDate($rp->pmk_sktgl) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->pmk_sktgl ? transformDate($rp->pmk_sktgl) : ''}}</td>
	    			@endif
	    			@if($rp->pmk_ttd_pejabat != $rp2->pmk_ttd_pejabat)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_ttd_pejabat ? $rp2->pmk_ttd_pejabat : '-'}}">{{$rp->pmk_ttd_pejabat ? $rp->pmk_ttd_pejabat : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->pmk_ttd_pejabat}}</td>
	    			@endif
	    			@if($rp->pmk_tmt_lama != $rp2->pmk_tmt_lama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_tmt_lama ? transformDate($rp2->pmk_tmt_lama) : '-'}}">{{ $rp->pmk_tmt_lama ? transformDate($rp->pmk_tmt_lama) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->pmk_tmt_lama ? transformDate($rp->pmk_tmt_lama) : ''}}</td>
	    			@endif
	    			@if($rp->pmk_tmt_baru != $rp2->pmk_tmt_baru)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_tmt_baru ? transformDate($rp2->pmk_tmt_baru) : '-'}}">{{ $rp->pmk_tmt_baru ? transformDate($rp->pmk_tmt_baru) : '-' }}</a></td>
	    			@else
	    				<td>{{ $rp->pmk_tmt_baru ? transformDate($rp->pmk_tmt_baru) : ''}}</td>
	    			@endif
	    			@if($rp->pmk_unit_kerja != $rp2->pmk_unit_kerja)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->pmk_unit_kerja ? $rp2->pmk_unit_kerja : '-'}}">{{$rp->pmk_unit_kerja ? $rp->pmk_unit_kerja : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->pmk_unit_kerja}}</td>
	    			@endif
	    			<!-- @if($rp->status_data != $rp2->status_data)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$rp2->status_data ? $rp2->status_data : '-'}}">{{$rp->status_data ? $rp->status_data : '-'}}</a></td>
	    			@else
	    				<td>{{$rp->status_data}}</td>
	    			@endif -->
	    		@endif
	    		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	    		<td>
                    @if($riwayat_pmk->count())
	    			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-pmk" 
					data-gol_id="{{$rp->gol_id}}" 
					data-nm_pkt="{{$gol->nm_pkt}}" 
					data-id="{{$rp->peg_id}}" 
					data-pmk-thn="{{$rp->pmk_tahun}}" 
					data-pmk-bln="{{$rp->pmk_bulan}}" 
					data-gapok-lama="{{$rp->pmk_gapok_lama}}"
					data-gapok-baru="{{$rp->pmk_gapok_baru}}"
					data-sk-nomor="{{$rp->pmk_nosk}}"
					data-sk-tgl="{{$rp->pmk_sktgl }}"
					data-sk-pejabat="{{$rp->pmk_ttd_pejabat}}"
					data-tmt-pmk-lama="{{$rp->pmk_tmt_lama}}"
					data-tmt-pmk-baru="{{$rp->pmk_tmt_baru}}"
					data-unit-kerja="{{$rp->pmk_unit_kerja}}"
					data-mk-kerja-lama-thn="{{$rp->masa_kerja_lama_thn}}"
					data-mk-kerja-lama-bln="{{$rp->masa_kerja_lama_bln}}"
					data-mk-kerja-baru-thn="{{$rp->masa_kerja_baru_thn}}"
					data-mk-kerja-baru-bln="{{$rp->masa_kerja_baru_bln}}"
					data-pmk-tahun="{{$rp->pmk_tahun}}"
					data-pmk-bulan="{{$rp->pmk_bulan}}"
					onclick="editPmk(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
		    		<!-- <a href="{{url('/riwayat/delete-pmk',array($pegawai->peg_id,$rp->peg_id,(isset($rp2->peg_id) ? $rp2->peg_id : 0)),false)}}" 
						class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a> -->
                    @endif
				</td>
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>
<div class="modal fade" id="modal-pmk" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelKgb"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/pmk')}}" id="pmkForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			<div class="bs-callout bs-callout-warning hidden">
			  <h4>Edit Data Gagal!</h4>
			  <p>Data Harus Lengkap!</p>
			</div>
			<?php $gol = Golongan::orderBy('nm_gol')->select('nm_gol','gol_id','nm_pkt')->get() ?>
            <div class="row" id="gol_id_pmk_container">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Gol. Ruang - pangkat <span class="required" aria-required="true">*</span></label>
                        <div class="col-md-8">
                            <select class="form-control select2" id="gol_id_pmk" name="gol_id_pmk" required style="border:none; margin-left:-13px; width:578px">                                
                                <option value="">-Pilih-</option>
                                @foreach ($gol as $key)
                                <option value="{{$key->gol_id}}">{{$key->nm_gol.' - '.$key->nm_pkt}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!-- @include('form.text2',['label'=>'Pangkat','required'=>false,'name'=>'nm_pkt','placeholder'=>''])
          	<br> -->
          	<!-- @include('form.text2',['label'=>'PMK Tahun','required'=>false,'name'=>'pmk_tahun','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'PMK Bulan','required'=>false,'name'=>'pmk_bulan','placeholder'=>''])
            <br> -->
          <!-- 	@include('form.text2',['label'=>'Masa Kerja Tahun','required'=>false,'name'=>'pmk_tahun','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Masa Kerja Bulan','required'=>false,'name'=>'pmk_bulan','placeholder'=>''])
			<br> -->
			@include('form.text2',['label'=>'Nomor SK','required'=>false,'name'=>'pmk_nosk','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'pmk_sktgl','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'pmk_ttd_pejabat','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Unit Kerja','required'=>false,'name'=>'pmk_unit_kerja','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'TMT Lama','required'=>false,'name'=>'pmk_tmt_lama','placeholder'=>''])
            <br>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Masa Kerja Gol. Lama</label>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" onblur="calculatePmk()" name="masa_kerja_lama_thn" id="masa_kerja_lama_thn" class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Tahun</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" onblur="calculatePmk()" name="masa_kerja_lama_bln" id="masa_kerja_lama_bln" class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Bulan</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            @include('form.text2',['label'=>'Gapok Lama','required'=>false,'name'=>'pmk_gapok_lama','placeholder'=>''])
            <br>
			@include('form.text2',['label'=>'TMT Baru','required'=>false,'name'=>'pmk_tmt_baru','placeholder'=>''])
            <br>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Masa Kerja Gol. Baru</label>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" onblur="calculatePmk()" name="masa_kerja_baru_thn" id="masa_kerja_baru_thn" class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Tahun</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" onblur="calculatePmk()" name="masa_kerja_baru_bln" id="masa_kerja_baru_bln" class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Bulan</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            @include('form.text2',['label'=>'Gapok Baru','required'=>false,'name'=>'pmk_gapok_baru','placeholder'=>''])
            <br>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Masa Kerja PMK</label>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" name="pmk_tahun" id="pmk_tahun" <?php echo 'readonly' ?> class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Tahun</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <input type="text" name="pmk_bulan" id="pmk_bulan" <?php echo 'readonly' ?> class="form-control" style="font-size:14px;">
                                </div>
                                <label class="control-label col-md-4">Bulan</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">
/* $.fn.ready(function() {
    $('#gol_id_pmk').select2({ width: '100%',allowClear: true });
}); */

function addPmk() {
	$("#pmkForm").attr("action","{{ URL::to('riwayat/edit/pmk/0') }}");
	$("#labelKgb").text("Tambah Riwayat Kgb");
    $("#gol_id_pmk").val("").select2();
    
	$(".pmk_tahun").val("");
	$(".pmk_bulan").val("");
	$(".pmk_gapok_lama").val("");
	$(".pmk_sktgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_tmt_lama").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_tmt_baru").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_nosk").val("");
	$(".pmk_ttd_pejabat").val("");
	$(".pmk_unit_kerja").val("");

	$(".pmk_gapok_lama").on("keydown", function (e) {
		numberOnly(e);
	}); 
	$(".pmk_gapok_baru").on("keydown", function (e) {
		numberOnly(e);
	});

	$('#pmkForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

function editPmk(e){
	var dateAr = $(e).data('sk-tgl').split('-');
	var dateAr2 = $(e).data('tmt-pmk-lama').split('-');
	var dateAr3 = $(e).data('tmt-pmk-baru').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var newDate2 = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];
	var newDate3 = dateAr3[2] + '-' + dateAr3[1] + '-' + dateAr3[0];

	$("#pmkForm").attr("action","{{ URL::to('riwayat/edit/pmk/') }}/"+$(e).data('id'));
	$("#labelKgb").text("Edit Riwayat Kgb");
	$("#gol_id_pmk").val($(e).data('gol_id')).select2();
	$(".pmk_tahun").val($(e).data('pmk-thn'));
	$(".pmk_bulan").val($(e).data('pmk-bln'));
	$(".pmk_gapok_lama").val($(e).data('gapok-lama'));
	$(".pmk_gapok_baru").val($(e).data('gapok-baru'));
	$(".pmk_nosk").val($(e).data('sk-nomor'));
	$(".pmk_sktgl").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_tmt_lama").val(newDate2).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_tmt_baru").val(newDate3).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".pmk_ttd_pejabat").val($(e).data('sk-pejabat'));
	$(".pmk_unit_kerja").val($(e).data('unit-kerja'));
	$("#masa_kerja_lama_thn").val($(e).data('mk-kerja-lama-thn'));
	$("#masa_kerja_lama_bln").val($(e).data('mk-kerja-lama-bln'));
	$("#masa_kerja_baru_thn").val($(e).data('mk-kerja-baru-thn'));
	$("#masa_kerja_baru_bln").val($(e).data('mk-kerja-baru-bln'));
	$("#pmk_tahun").val($(e).data('pmk-tahun'));
	$("#pmk_bulan").val($(e).data('pmk-bulan'));

	$(".pmk_gapok_lama").on("keydown", function (e) {
		numberOnly(e);

	}); 
	$(".pmk_gapok_baru").on("keydown", function (e) {
		numberOnly(e);
	});

	$('#pmkForm').parsley().on('field:validated', function() {
	    var ok = $('.parsley-error').length === 0;
	    $('.bs-callout-info').toggleClass('hidden', !ok);
	    $('.bs-callout-warning').toggleClass('hidden', ok);
	});
}

function calculatePmk() {
	var masa_kerja_lama_thn = parseInt($('#masa_kerja_lama_thn').val())*12;
	var masa_kerja_lama_bln = parseInt($('#masa_kerja_lama_bln').val());
	var masa_kerja_baru_thn = parseInt($('#masa_kerja_baru_thn').val())*12;
	var masa_kerja_baru_bln = parseInt($('#masa_kerja_baru_bln').val());

	var mk_lama = 0;
	var mk_baru = 0;
	mk_lama = masa_kerja_lama_thn + masa_kerja_lama_bln;
	mk_baru = masa_kerja_baru_thn + masa_kerja_baru_bln;
		// console.log(mk_lama+'-'+mk_baru);
	
	var pmk_tahun = 0;
	var pmk_bulan = 0;

	/* if(masa_kerja_lama_bln+masa_kerja_baru_bln > 12) {
		pmk_tahun = masa_kerja_lama_thn + masa_kerja_baru_thn + (Math.Floor((masa_kerja_lama_bln+masa_kerja_baru_bln)/12));
		pmk_bulan = (masa_kerja_lama_bln+masa_kerja_baru_bln)%12;
	} else { */
	
		pmk_tahun = Math.floor((mk_baru - mk_lama)/12);
		pmk_bulan = (mk_baru - mk_lama)%12;
	// }
	$('#pmk_tahun').val(pmk_tahun);
	$('#pmk_bulan').val(pmk_bulan);
}

function formatNumber(number)
{
    var number = number.toFixed(2) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function numberOnly(e){
	var key   = e.keyCode ? e.keyCode : e.which;

	if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
	     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
	     (key >= 35 && key <= 40) ||
	     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
	     (key >= 96 && key <= 105)
	   )) e.preventDefault();
}

</script>