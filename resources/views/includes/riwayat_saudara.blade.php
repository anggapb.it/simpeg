<?php

	use App\Model\RiwayatKeluarga;
	use App\Model\RiwayatKeluarga2;

	$kel = RiwayatKeluarga::where('peg_id', $pegawai->peg_id)->where('riw_status',2)->orderBy('riw_tgl_lahir')->get();
	$i=1;
?>


<div class="col-xs-12">
	<h3 align="middle">Riwayat Saudara</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit)
		<!-- <button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-saudara" onclick="addSaudara()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button> -->
		@endif
	</div>
	<table id="tabel-riwayat-saudara" class="table table-striped table-bordered table-hover">
		<input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>Nama Saudara</th>
			<th>Jenis Kelamin</th>
			<th>Tempat dan Tanggal Lahir</th>
			<th>Pendidikan</th>
			<th>Pekerjaan</th>
			<th>Keterangan</th>
			@if(!$submit)
			<!-- <th>Pilihan</th> -->
			@endif
		</tr>
	     </thead>
	     <tbody>
	    	@foreach ($kel as $k)
	    	<?php
	    		$k2 = RiwayatKeluarga2::where('peg_id', $pegawai->peg_id)->where('riw_id', $k->riw_id)->where('riw_status',2)->orderBy('riw_tgl_lahir')->first();
	    	
	    		if($k->riw_kelamin == 'L'){
	    			$jk = 'Laki-laki';
	    		}elseif($k->riw_kelamin == 'P'){
	    			$jk = 'Perempuan';
	    		}else{
	    			$jk = '-';
	    		}

	    		if($k2->riw_kelamin == 'L'){
	    			$jk2 = 'Laki-laki';
	    		}elseif($k2->riw_kelamin == 'P'){
	    			$jk2 = 'Perempuan';
	    		}else{
	    			$jk2 = '-';
	    		}
	    	?>
	    	<tr class="aktifKeluarga{{$k->riw_id}}">
	    		<td>{{$i}}</td>
	    		@if(!$k2)
		    		<td class="alert-danger">{{$k->riw_nama}}</td>
		    		<td class="alert-danger">{{$jk}}</td>
		    		<td class="alert-danger">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
		    		<td class="alert-danger">{{$k->riw_pendidikan}}</td>
		    		<td class="alert-danger">{{$k->riw_pekerjaan}}</td>
		    		<td class="alert-danger">{{$k->riw_ket}}</td>
	    		@else
		    		@if($k->riw_nama != $k2->riw_nama)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_nama ? $k2->riw_nama : '-'}}">{{ $k->riw_nama ? $k->riw_nama : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_nama}}</td>
	    			@endif

	    			@if($k->riw_kelamin != $k2->riw_kelamin)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$jk2}}">{{ $jk }}</a></td>
	    			@else
	    				<td>{{$jk}}</td>
	    			@endif
	    			
	    			@if($k->riw_tempat_lahir != $k2->riw_tempat_lahir || $k->riw_tgl_lahir != $k2->riw_tgl_lahir)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_tempat_lahir}}, {{$k2->riw_tgl_lahir ? transformDate($k2->riw_tgl_lahir) : ''}}">{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</a></td>
	    			@else
	    				<td>{{$k->riw_tempat_lahir}}, {{$k->riw_tgl_lahir ? transformDate($k->riw_tgl_lahir) : ''}}</td>
	    			@endif
		    		@if($k->riw_pendidikan != $k2->riw_pendidikan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pendidikan ? $k2->riw_pendidikan : '-'}}">{{ $k->riw_pendidikan ? $k->riw_pendidikan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pendidikan}}</td>
	    			@endif
		    		@if($k->riw_pekerjaan != $k2->riw_pekerjaan)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_pekerjaan ? $k2->riw_pekerjaan : '-'}}">{{ $k->riw_pekerjaan ? $k->riw_pekerjaan : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_pekerjaan}}</td>
	    			@endif
	    			@if($k->riw_ket != $k2->riw_ket)
	    				<td class="alert-success"><a class="grey show-option" href="#" title="{{$k2->riw_ket ? $k2->riw_ket : '-'}}">{{ $k->riw_ket ? $k->riw_ket : '-' }}</a></td>
	    			@else
	    				<td>{{$k->riw_ket}}</td>
	    			@endif
	    		@endif
	    		@if(!$submit)
	    		<!-- <td>
					<button class="btn btn-warning btn-xs"
							data-toggle="modal" data-target="#modal-saudara" 
							data-nama="{{$k->riw_nama}}" 
							data-id="{{$k->riw_id}}" 
							data-jeniskel="{{$k->riw_kelamin}}" 
							data-tempat-lahir="{{$k->riw_tempat_lahir}}" 
							data-tanggal-lahir="{{$k->riw_tgl_lahir}}"
							data-pendidikan="{{$k->riw_pendidikan}}"
							data-pekerjaan="{{$k->riw_pekerjaan}}"
							data-keterangan="{{$k->riw_ket}}"
							onclick="editSaudara(this)">
							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
							Edit</button>
		    		<a href="{{url('/riwayat/delete-keluarga',array($pegawai->peg_id,$k->riw_id),false)}}" 
		    			class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
							Delete
					</a>
				</td> -->
				@endif
	    	</tr>
	    	<?php $i++;?>
	    	@endforeach
	    </tbody>
 	</table>
</div>

<div class="modal fade" id="modal-saudara" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelSaudara"><div id="modal-button-edit"></div></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/edit/saudara')}}" id="saudaraForm">
			{!! csrf_field() !!}
		<div class="modal-body" id="modal-detail-content">
			@include('form.text2',['label'=>'Nama','required'=>false,'name'=>'riw_nama','placeholder'=>''])
			<br>
			@include('form.radio_modal',['label'=>'Jenis Kelamin','required'=>false,'name'=>'riw_kelamin','data'=>[
            'L' => 'Laki Laki','P' => 'Perempuan' ]])
			<br>
			@include('form.text2',['label'=>'Tempat Lahir','required'=>false,'name'=>'riw_tempat_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Tanggal Lahir','required'=>false,'name'=>'riw_tgl_lahir','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pendidikan','required'=>false,'name'=>'riw_pendidikan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Pekerjaan','required'=>false,'name'=>'riw_pekerjaan','placeholder'=>''])
			<br>
			@include('form.text2',['label'=>'Keterangan','required'=>false,'name'=>'riw_ket','placeholder'=>''])
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script type="text/javascript">
var addSaudara = function(){
	$("#saudaraForm").attr("action","{{ URL::to('riwayat/add/saudara/') }}");
	$("#labelSaudara").text("Tambah Data Saudara");
	$(".riw_nama").val("");
	$(".riw_kelamin1").prop('checked', false);
	$(".riw_kelamin2").prop('checked', false);
	$(".riw_tgl_lahir").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val("");
	$(".riw_pendidikan").val("");
	$(".riw_ket").val("");
	$(".riw_pekerjaan").val("");
}
var editSaudara = function(e){
	var dateAr = $(e).data('tanggal-lahir').split('-');
	var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
	var jk = ($(e).data('jeniskel'));

	$("#saudaraForm").attr("action","{{ URL::to('riwayat/edit/saudara/') }}/"+$(e).data('id'));
	$("#labelSaudara").text("Edit Data Saudara");
	$(".riw_nama").val($(e).data('nama'));
	if(jk == 'L'){
		$(".riw_kelamin1").val(jk).prop( "checked", true );
	}else if(jk == 'P'){
		$(".riw_kelamin2").val(jk).prop( "checked", true );
	}
	
	console.log($(e).data('jeniskel'));
	$(".riw_tgl_lahir").val(newDate).mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
	$(".riw_tempat_lahir").val($(e).data('tempat-lahir'));
	$(".riw_pendidikan").val($(e).data('pendidikan'));
	$(".riw_pekerjaan").val($(e).data('pekerjaan'));
	$(".riw_ket").val($(e).data('keterangan'));
}

</script>