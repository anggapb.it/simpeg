<?php

	use App\Model\SKP;

	$struk = SKP::where('peg_id',$pegawai->peg_id)->get();

	function nilai_akhir($jenis_skp,$nilai_capaian,$orientasi_pelayanan,$integritas,$komitmen,$disiplin,$kerjasama,$kepemimpinan) {
		$capaian_kerja = ($nilai_capaian * 60)/100;
		if($jenis_skp == 'Struktural') {
			$perilaku = ((($orientasi_pelayanan + $integritas + $komitmen + $disiplin + $kerjasama + $kepemimpinan)/6) * 40)/100;
		}
		else {
			$perilaku = ((($orientasi_pelayanan + $integritas + $komitmen + $disiplin + $kerjasama)/5) * 40)/100;
		}
		$nilai_akhir = $capaian_kerja + $perilaku;
		return number_format($nilai_akhir, 2, '.', '');
	}
?>

<div class="col-xs-12">
	<h3 align="middle">Sasaran Kerja Pegawai</h3><br>
	<div class="pull-right tableTools-container">
		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
		<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-skp" onclick="addSKP()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
		@endif
	</div>
	<div class="table-responsive">
	<table id="tabel-file" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	    <input type="hidden" id="token" value="{{ csrf_token() }}">
	     <thead>
		<tr align="center">
			<th>No.</th>
			<th>Tahun</th>
			<th>Jenis SKP</th>
		    <th>Nilai Rata-Rata Capaian Kerja</th>
			<th>Orientasi Pelayanan</th>
			<th>Integritas</th>
			<th>Komitmen</th>
			<th>Disiplin</th>
			<th>Kerjasama</th>
			<th>Kepemimpinan</th>
			<th>Nilai Akhir</th>
			@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
			<th>Aksi</th>
			@endif
		</tr>
	     </thead>
	      <tbody>
	      	<?php
	      	$no = 1;
	      	 ?>
	      	@foreach($struk as $s)
	      	<tr>
	      		<td>{{$no++}}</td>
	      		<td>{{$s->tahun}}</td>
	      		<td>{{$s->jenis_skp}}</td>
	      		<td>{{$s->nilai_capaian}}</td>
	      		<td>{{$s->orientasi_pelayanan}}</td>
	      		<td>{{$s->integritas}}</td>
	      		<td>{{$s->komitmen}}</td>
	      		<td>{{$s->disiplin}}</td>
	      		<td>{{$s->kerjasama}}</td>
	      		<td>{{($s->kepemimpinan != 0) ? $s->kepemimpinan : '-'}}</td>
	      		<td>{{nilai_akhir($s->jenis_skp,$s->nilai_capaian,$s->orientasi_pelayanan,$s->integritas,$s->komitmen,$s->disiplin,$s->kerjasama,$s->kepemimpinan)}}</td>
	      		@if(!$submit && Auth::user()->role_id != 5 && Auth::user()->role_id != 6)
	      		<td>
	      			<button class="btn btn-warning btn-xs"
					data-toggle="modal" data-target="#modal-skp" 
					data-id="{{$s->skp_id}}" 
					data-tahun="{{$s->tahun}}" 
					data-jenis_skp="{{$s->jenis_skp}}" 
					data-nilai_capaian="{{$s->nilai_capaian}}" 
					data-orientasi_pelayanan="{{$s->orientasi_pelayanan}}" 
					data-integritas="{{$s->integritas}}" 
					data-komitmen="{{$s->komitmen}}" 
					data-disiplin="{{$s->disiplin}}" 
					data-kerjasama="{{$s->kerjasama}}" 
					data-kepemimpinan="{{$s->kepemimpinan}}"
					onclick="editSKP(this)">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
					Edit</button>
					<a href="{{url('riwayat/delete-skp',$s->peg_id).'/'.$s->skp_id}}" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda Yakin Akan Menghapus SKP Ini?')"><span class="fa fa-trash-o"></span> Hapus</a>
	      		</td>
			@endif
	      	</tr>
	      	@endforeach
	    </tbody>
 	</table>
 	</div>
</div>

<div class="modal fade" id="modal-skp" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 id="labelSKP"></h4>
		</div>
		<form method="POST" action="{{url('/riwayat/add/skp')}}" id="formSKP">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
			@include('form.date2',['label'=>'Tahun','required'=>true,'name'=>'tahun'])
			<br>
			@include('form.select2_modal',['label'=>'Jenis SKP','required'=>true,'name'=>'jenis_skp','data'=>['Struktural'=>'Struktural','Fungsional Tertentu'=>'Fungsional Tertentu','Fungsional Umum'=>'Fungsional Umum'],'empty'=>''])
			<br>
			@include('form.text2',['label'=>'Nilai Rata-Rata Capaian Kerja','required'=>true,'name'=>'nilai_capaian'])
			<br>
			@include('form.text2',['label'=>'Orientasi Pelayanan','required'=>true,'name'=>'orientasi_pelayanan'])
			<br>
			@include('form.text2',['label'=>'Integritas','required'=>true,'name'=>'integritas'])
			<br>
			@include('form.text2',['label'=>'Komitmen','required'=>true,'name'=>'komitmen'])
			<br>
			@include('form.text2',['label'=>'Disiplin','required'=>true,'name'=>'disiplin'])
			<br>
			@include('form.text2',['label'=>'Kerjasama','required'=>true,'name'=>'kerjasama'])
			<br>
			@include('form.text2',['label'=>'Kepemimpinan','required'=>true,'name'=>'kepemimpinan'])
			<br>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<input type="hidden" name="jabatan_id" class="jabatan_id" value="{{$pegawai->jabatan_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">

function hideKepemimpinan() {
	if($("#jenis_skp").val() !== 'Struktural') {
		$("#kepemimpinan").prop('disabled', true);
		$("#kepemimpinan").val("");
	}
	else {
		$("#kepemimpinan").prop('disabled', false);
	}
}

var addSKP = function(){
	$("#formSKP").attr("action","{{ URL::to('riwayat/add/skp/') }}");
	$("#labelSKP").html("Tambah SKP");
	$("#tahun,#nilai_capaian,#orientasi_pelayanan,#integritas,#komitmen,#disiplin,#kerjasama,#kepemimpinan").val("");
	$("#tahun").datepicker({
		autoclose: true,
		format: "yyyy",
		viewMode: "years", 
		minViewMode: "years"
	});
	hideKepemimpinan();
	$("#jenis_skp").select2({
		placeholder: 'Pilih SKP'
	}).change(function() {
		hideKepemimpinan();
	});
	$("#nilai_capaian,#orientasi_pelayanan,#integritas,#komitmen,#disiplin,#kerjasama,#kepemimpinan").keydown(function (e) {
		var key   = e.keyCode ? e.keyCode : e.which;
		if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 || 
			(key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
			(key >= 35 && key <= 40) || 
			(key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) || 
			(key >= 96 && key <= 105))) 
			e.preventDefault();
	});
}
var editSKP = function(e){
	var id = $(e).data('id');
	var disable = $(e).data('penting');
	if(id == ''){
		$("#formSKP").attr("action","{{ URL::to('riwayat/add/skp/') }}");
	}else{
		$("#formSKP").attr("action","{{ URL::to('riwayat/edit/skp/') }}/"+id);
	}

	$("#labelSKP").html("Edit SKP");
	$("#tahun").val($(e).data('tahun'));
	$("#jenis_skp").val($(e).data('jenis_skp'));
	$("#nilai_capaian").val($(e).data('nilai_capaian'));
	$("#orientasi_pelayanan").val($(e).data('orientasi_pelayanan'));
	$("#integritas").val($(e).data('integritas'));
	$("#komitmen").val($(e).data('komitmen'));
	$("#disiplin").val($(e).data('disiplin'));
	$("#kerjasama").val($(e).data('kerjasama'));
	$("#kepemimpinan").val($(e).data('kepemimpinan'));
	$("#tahun").datepicker({
		autoclose: true,
		format: "yyyy",
		viewMode: "years", 
		minViewMode: "years"
	});
	hideKepemimpinan();
	$("#jenis_skp").select2({
		placeholder: 'Pilih SKP'
	}).change(function() {
		hideKepemimpinan();
	});
	$("#nilai_capaian,#orientasi_pelayanan,#integritas,#komitmen,#disiplin,#kerjasama,#kepemimpinan").keydown(function (e) {
		var key   = e.keyCode ? e.keyCode : e.which;
		if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 || 
			(key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
			(key >= 35 && key <= 40) || 
			(key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) || 
			(key >= 96 && key <= 105))) 
			e.preventDefault();
	});
}
</script>
