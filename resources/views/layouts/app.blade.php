<?php
	use App\Model\Pegawai2;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>SIMPEG BDG</title>
		<meta name="csrf_token" content="{{ csrf_token() }}" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/bower_components/datatables/media/css/jquery.dataTables.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" />
		<link href="{{ asset('/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet"/>
		<!-- text fonts -->
		<link rel="stylesheet" href="{{ asset('/css/ace-fonts.min.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ asset('/css/ace.min.css') }}" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="{{ asset('/css/ace-rtl.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/parsley.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/jquery.fileupload.css') }}" />
		<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-datepicker3.css') }}">
		<style type="text/css">
				.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
					font-size: 12px;
					padding: 4px;
				}
				td>label, th>label {
					font-size: 12px;
				}
				.user-info {
   					max-width: 500px;
   				}
   				.tooltip-inner {
					white-space:pre-wrap;
				}
				.table-responsive {
				    width: 100%;
				    margin-bottom: 15px;
				    overflow-y: hidden;
				    overflow-x: scroll;
				    -ms-overflow-style: -ms-autohiding-scrollbar;
				    border: 1px solid #DDD;
				    -webkit-overflow-scrolling: touch;
				}
				.mobile-header{
						margin-top: -10px;
					}
				@media(max-width: 500px){

					.responsive{
						max-width: 100%;
					}
					.tampil{
						display: inline-block;
					}
					.pull-right>.dropdown-menu {
					    right: 0;
					    left: 1px;
					}
				}
				@media(min-width: 500px){
					.tampil{
						display: none;
					}
				}
		</style>
		 @yield('styles')
	</head>

	<body class="no-skin">
	<header>
	  <div class="head-block">
		<div class="container">
		  <div class="line1"></div>
		  <div class="middle-block">
			<div class="row-eq-height">
			  <!-- <div class="col-md-2"> -->
				<div>
				  <img src="{{ asset('images/logo.png') }}" class="logo"/>
				</div>
			  <!-- </div> -->
			  <div class="col-lg-12 bg">
				<div class="row">
				  <div class="col-md-9 head-left">
				  	@if(!isset($title))
					<h3><span class="hidden-xs" id="simpeg_title">Sistem Informasi Manajemen Kepegawaian (SIMPEG)</span><span class="visible-xs">SIMPEG</span></h3>
					@else
					<h3><span class="hidden-xs" id="kgb_title">Kenaikan Gaji Berkala (KGB)</span><span class="visible-xs">KGB</span></h3>
					@endif
					<h4><span class="hidden-xs">Badan Kepegawaian, Pendidikan dan Pelatihan Kota Bandung</span><span class="visible-xs">BKPP</span></h4>
				  </div>
				  <div class="col-md-3 head-right">
					  <p class="text-right" style="word-break: break-all;">Selamat Datang <b>{{ Auth::user() ? Auth::user()->nama : '' }}</b></p>
					  <div id="navbar" class="navbar navbar-default visible-s pull-right">
					  	<div class="navbar-container" id="navbar-container">
					  		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					  			<span class="sr-only">Toggle sidebar</span>

					  			<span class="icon-bar"></span>

					  			<span class="icon-bar"></span>

					  			<span class="icon-bar"></span>
					  		</button>
					  	</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	</header>
		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="{{Request::is('home')?'active':''}}">
						<a href="{{url('/home')}}">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>
					@if(Auth::user()->id == 298)
					<li class="{{Request::is('upload-assessment')?'active':''}}">
						<a href="{{url('/upload-assessment')}}"	>
							<i class="menu-icon fa fa-paste"></i>
							<span class="menu-text"> Upload Assessment </span>
						</a>
						<b class="arrow"></b>
					</li>
					@endif
					@if(Auth::user()->role_id == 1)
					<li class="{{Request::is('rekon-sapk/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list-alt"></i>
							<span class="menu-text"> Rekon SAPK</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							@if(Auth::user()->id == 298)
							<li class="{{Request::is('rekon-sapk/upload')?'active':''}}">
								<a href="{{url('rekon-sapk/upload')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Upload Rekon
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekon-sapk/sapk')?'active':''}}">
								<a href="{{url('#')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Tidak Ada di SAPK
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekon-sapk/simpeg')?'active':''}}">
								<a href="{{url('#')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Tidak Ada di SIMPEG
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekon-sapk/beda-gol')?'active':''}}">
								<a href="{{url('#')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Data Beda Golongan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							@endif
							<li class="{{Request::is('rekon-sapk/update-kp')?'active':''}}">
								<a href="{{url('rekon-sapk/update-kp')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Update KP Kolektif
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					@endif
					@if(Auth::user()->role_id == 4)
						<?php
							$nip = Auth::user()->username;
							$pegawai = Pegawai2::where('peg_nip', $nip)->first();

						?>
						<li class="{{Request::is('pegawai/profile/*') ? 'active' :''}}">
							<a href="{{url('/pegawai/profile/'.$pegawai->peg_id)}}" >
								<i class="menu-icon glyphicon glyphicon-user"></i>
								<span class="menu-text">
									Data Pegawai
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('kgb/*')?'active open':''}}">
							<a href="#" class="dropdown-toggle">
								<i class="menu-icon fa fa-money"></i>
								<span class="menu-text"> KGB </span>
								<b class="arrow fa fa-angle-down"></b>
							</a>
							<b class="arrow"></b>

							<ul class="submenu">
								<li class="{{Request::is('kgb/ttd-digital')?'active':''}}">
									<a href="{{url('/kgb/ttd-digital')}}" >
										<i class="menu-icon fa fa-caret-right"></i>
										<span class="menu-text">
											Tanda Tangan Digital
										</span>
									</a>
									<b class="arrow"></b>
								</li>
							</ul>
						</li>
					@endif
					@if(Auth::user()->role_id != 4)

					@if(Auth::user()->role_id != 5 && Auth::user()->role_id != 8 && Auth::user()->role_id != 9)
					<li class="{{Request::is('list-unit')?'active':''}}">
						<a href="{{url('/list-unit/')}}" >
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text">
								List Pegawai
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@endif

					@if(Auth::user()->role_id == 3 || Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
					<li class="{{Request::is('penjagaan/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-book"></i>
							<span class="menu-text"> Penjagaan </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							@if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
							<li class="{{Request::is('penjagaan/kpo*')?'active':''}}">
								<a href="{{url('/penjagaan/kpo')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										KPO Per OPD
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('penjagaan/kp-struktural')?'active':''}}">
								<a href="{{url('#')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										KP Struktural <span class="small">(Alpha)</span>
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							@elseif(Auth::user()->role_id == 2)
							<li class="{{Request::is('penjagaan/kpo-skpd')?'active':''}}">
								<a href="{{url('/penjagaan/kpo-skpd')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										KPO
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							@endif
						</ul>
					</li>
					@endif
					
					@if(Auth::user()->role_id != 5 && Auth::user()->role_id != 8 && Auth::user()->role_id != 9)
					<li class="{{Request::is('kgb/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-money"></i>
							<span class="menu-text"> KGB </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							<li class="{{Request::is('kgb/dashboard')?'active':''}}">
								<a href="{{url('/kgb/dashboard')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Dashboard KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('kgb/rekomendasi-kgb')?'active':''}}">
								<a href="{{url('/kgb/rekomendasi-kgb')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Prediksi KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('kgb/usulan-kgb')?'active':''}}">
								<a href="{{url('/kgb/usulan-kgb')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Usulan KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
							<li class="{{Request::is('kgb/inbox-kgb')?'active':''}}">
								<a href="{{url('/kgb/inbox-kgb')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Inbox KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('kgb/proses-kgb')?'active':''}}">
								<a href="{{url('/kgb/proses-kgb/get-data')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Proses Data KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('kgb/ttd-digital')?'active':''}}">
								<a href="{{url('/kgb/ttd-digital')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Tanda Tangan Digital
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('kgb/laporan-kgb')?'active':''}}">
								<a href="{{url('/kgb/laporan-kgb')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Laporan KGB
									</span>
								</a>
								<b class="arrow"></b>
							</li>
								<li class="{{Request::is('kgb/*')?'active open':''}}">
								<a href="#" class="dropdown-toggle">
									<i class="menu-icon fa fa-hdd-o"></i>
									<span class="menu-text">Setting</span>
									<b class="arrow fa fa-angle-down"></b>
								</a>
								<b class="arrow"></b>
								<ul class="submenu">
									<li class="{{Request::is('kgb/ttd-kgb')?'active':''}}">	
										<a href="{{url('/kgb/ttd-kgb')}}"  >
											<i class="menu-icon fa fa-caret-right"></i>
											<span class="menu-text">
												Setting Penandatangan
											</span>
										</a>
										<b class="arrow"></b>
									</li>
									<li class="{{Request::is('kgb/ttd-kgb')?'active':''}}">	
										<a href="{{url('/kgb/setting-template-srt')}}" >
											<i class="menu-icon fa fa-caret-right"></i>
											<span class="menu-text">
												Setting Template Surat
											</span>
										</a>
										<b class="arrow"></b>
									</li>
								</ul>
							</li>
							@endif
						</ul>
					</li>
					@endif

					@if (Auth::user()->role_id != '6')
					<li class="{{Request::is('list-all-pegawai/'.Auth::user()->satuan_kerja_id)?'active':''}}">
						<a href="{{url('/list-all-pegawai/'.Auth::user()->satuan_kerja_id)}}" >
							<i class="menu-icon fa fa-search"></i>
							<span class="menu-text">
								Cari Pegawai
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@if (Auth::user()->role_id != '2' && Auth::user()->role_id != '3' && Auth::user()->role_id != '7' && Auth::user()->role_id != '5' && Auth::user()->role_id != '8' && Auth::user()->role_id != '9')
					<li class="{{Request::is('list-mutasi')?'active':''}}">
						<a href="{{url('/list-mutasi/')}}" >
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text">
								List Pegawai Mutasi
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="{{Request::is('mutasi')?'active':''}}">
						<a href="{{url('/mutasi/')}}" >
							<i class="menu-icon glyphicon glyphicon-download"></i>
							<span class="menu-text">
								Mutasi Masuk
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@endif
					@endif					
					@endif
					@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
					<li class="{{Request::is('pegawai-inaktif/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> List Pegawai Inaktif </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							<li class="{{Request::is('pegawai-inaktif/pensiun')?'active':''}}">
								<a href="{{url('/pegawai-inaktif/pensiun')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Pensiun
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('pegawai-inaktif/meninggal')?'active':''}}">
								<a href="{{url('/pegawai-inaktif/meninggal')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Meninggal Dunia
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('pegawai-inaktif/pindah')?'active':''}}">
								<a href="{{url('/pegawai-inaktif/pindah')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Pindah ke Luar
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="{{Request::is('evjab/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-exchange"></i>
							<span class="menu-text"> Konversi Jabatan </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="{{Request::is('evjab/jfu')?'active':''}}">
								<a href="{{url('/evjab/jfu')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Master Pelaksana
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<!--<li class="{{Request::is('evjab/jfu/terisi')?'active':''}}">
								<a href="{{url('/evjab/jfu/terisi')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jabatan Terisi
									</span>
								</a>
								<b class="arrow"></b>
							</li>-->
							<li class="{{Request::is('evjab/search')?'active':''}}">
								<a href="{{url('/evjab/search')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Cari Pegawai
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-unit*')?'active':''}}">
								<a href="{{url('/evjab/list-unit')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										List Pegawai
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-clear-unit*')?'active':''}}">
								<a href="{{url('/evjab/list-clear-unit')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										List Pegawai Bersih
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-pegawai-naik')?'active':''}}">
								<a href="{{url('/evjab/list-pegawai-naik')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Pegawai Naik Kelas
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-pegawai-turun')?'active':''}}">
								<a href="{{url('/evjab/list-pegawai-turun')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Pegawai Turun Kelas
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-pegawai-belum')?'active':''}}">
								<a href="{{url('/evjab/list-pegawai-belum')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Belum Punya Jabatan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('evjab/list-pegawai-ujikom')?'active':''}}">
								<a href="{{url('/evjab/list-pegawai-ujikom')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Lulus Uji Kompetensi
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					@endif
					@if(Auth::user()->role_id == 1)									
					<li class="{{Request::is('notifikasi/*')?'active':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-bell-o"></i>
							<span class="menu-text">
								Notifikasi
							</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
						<li class="{{Request::is('notifikasi/pegawai-struktural')?'active':''}}">
								<a href="{{url('/notifikasi/pegawai-struktural/')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jabatan > 5 Tahun
									</span>
								</a>

								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('notifikasi/prediksi')?'active':''}}">
								<a href="{{url('/notifikasi/prediksi')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Prediksi Pensiun
									</span>
								</a>

								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('notifikasi/lebih')?'active':''}}">
								<a href="{{url('/notifikasi/lebih')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Melebihi Umur Pensiun
									</span>
								</a>

								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('notifikasi/prediksipangkat')?'active':''}}">
								<a href="{{url('/notifikasi/prediksipangkat')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Prediksi Kenaikan Pangkat
									</span>
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="{{Request::is('data/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-hdd-o"></i>
							<span class="menu-text"> Data Master </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
						<li class="{{Request::is('data/keahlian')?'active':''}}">
							<a href="{{url('/data/keahlian')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Keahlian
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/bahasa')?'active':''}}">
							<a href="{{url('/data/bahasa')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Bahasa
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/satker')?'active':''}}">
							<a href="{{url('/data/satker')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Satuan Kerja
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/uker')?'active':''}}">
							<a href="{{url('/data/uker')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Unit Kerja
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/jabatan')?'active':''}}">
							<a href="{{url('/data/jabatan')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Jabatan
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/jft')?'active':''}}">
							<a href="{{url('/data/jft')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									JFT
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/jfu')?'active':''}}">
							<a href="{{url('/data/jfu')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									JFU
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						<li class="{{Request::is('data/pengajuan')?'active':''}}">
							<a href="{{url('/data/pengajuan')}}" >
								<i class="menu-icon fa fa-caret-right"></i>
								<span class="menu-text">
									Pengajuan
								</span>
							</a>

							<b class="arrow"></b>
						</li>
						</ul>
					</li>
					<li class="{{Request::is('pushdata/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-cloud-upload"></i>
							<span class="menu-text"> Push Data ke ERK </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="{{Request::is('pushdata/jabatan')?'active':''}}">
								<a href="{{url('/pushdata/jabatan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Perubahan Jabatan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('pushdata/pensiun')?'active':''}}">
								<a href="{{url('/pushdata/pensiun')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Status Pensiun
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					{{-- <li class="{{Request::is('import/pegawai/jfu')?'active':''}}">
						<a href="{{url('import/pegawai/jfu')}}" >
							<i class="menu-icon fa fa-table"></i>
							<span class="menu-text">
								Import Data ASN
							</span>
						</a>

						<b class="arrow"></b>
					</li> --}}
					@endif
					@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->role_id == 6 || Auth::user()->role_id == 7)
					<li class="{{Request::is('pns/search')?'active':''}}">
						<a href="{{url('pns/search')}}" >
							<i class="menu-icon fa fa-table"></i>
							<span class="menu-text">
								Semua Pegawai
							</span>
						</a>
					</li>
					<li class="{{Request::is('search')?'active':''}}">
						<a href="{{url('search')}}" >
							<i class="menu-icon fa fa-search"></i>
							<span class="menu-text">
								Pencarian Dinamis
							</span>
						</a>
					</li>
					@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3 || Auth::user()->role_id == 6)
					<li class="{{Request::is('ulang-tahun*')?'active':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-calendar"></i>
							<span class="menu-text">Ulang Tahun</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="{{Request::is('ulang-tahun/harian*')?'active':''}}">
								<a href="{{url('/ulang-tahun/harian')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Harian
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('ulang-tahun/bulanan*')?'active':''}}">
								<a href="{{url('/ulang-tahun/bulanan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Bulanan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					@endif
					@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 6)
					<li class="{{Request::is('search/jabatan-kosong')?'active':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-star-half-full"></i>
							<span class="menu-text">
								Jabatan Kosong
							</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							<li class="{{Request::is('search/jabatan-kosong')?'active':''}}">
								<a href="{{url('/search/jabatan-kosong')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Eselon
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('search/jabatan-kosong/non-eselon')?'active':''}}">
								<a href="{{url('/search/jabatan-kosong/non-eselon')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Non-eselon
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					@endif

					<li class="{{Request::is('rekap/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-table"></i>
							<span class="menu-text"> Rekap </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							<li class="{{Request::is('rekap/skpd')?'active':''}}">
								<a href="{{url('/rekap/skpd')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										SKPD
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/jabatan')?'active':''}}">
								<a href="{{url('/rekap/jabatan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jabatan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/golongan')?'active':''}}">
								<a href="{{url('/rekap/golongan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Golongan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/jenis-kelamin')?'active':''}}">
								<a href="{{url('/rekap/jenis-kelamin')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jenis Kelamin
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/pendidikan')?'active':''}}">
								<a href="{{url('/rekap/pendidikan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Tingkat Pendidikan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/usia')?'active':''}}">
								<a href="{{url('/rekap/usia')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Kelompok Usia
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('rekap/eselon-jk')?'active':''}}">
								<a href="{{url('/rekap/eselon-jk')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Eselon Jenis Kelamin
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="{{Request::is('grafik/*')?'active open':''}}">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-bar-chart"></i>
							<span class="menu-text"> Grafik </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>

						<ul class="submenu">
							<li class="{{Request::is('grafik/jabatan')?'active':''}}">
								<a href="{{url('/grafik/jabatan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jabatan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('grafik/golongan')?'active':''}}">
								<a href="{{url('/grafik/golongan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Golongan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('grafik/jenis-kelamin')?'active':''}}">
								<a href="{{url('/grafik/jenis-kelamin')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Jenis Kelamin
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('grafik/eselon-jk')?'active':''}}">
								<a href="{{url('/grafik/eselon-jk')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Eselon Jenis Kelamin
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('grafik/pendidikan')?'active':''}}">
								<a href="{{url('/grafik/pendidikan')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Tingkat Pendidikan
									</span>
								</a>
								<b class="arrow"></b>
							</li>
							<li class="{{Request::is('grafik/usia')?'active':''}}">
								<a href="{{url('/grafik/usia')}}" >
									<i class="menu-icon fa fa-caret-right"></i>
									<span class="menu-text">
										Kelompok Usia
									</span>
								</a>
								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					@endif
					@if(Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 6 && Auth::user()->role_id != 8 && Auth::user()->role_id != 5 && Auth::user()->role_id != 9)
					<li class="{{Request::is('walikota')?'active':''}}">
						<a href="{{url('/walikota/')}}" >
							<i class="menu-icon fa fa-male"></i>
							<span class="menu-text">
								Profil Walikota
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@endif
					@if(Auth::user()->role_id == 1)
					<li class="{{Request::is('daftar-hadir')?'active':''}}">
						<a href="{{ url('/daftar-hadir') }}" >
							<i class="menu-icon fa fa-table"></i>
							<span class="menu-text">
								Absen / Presensi
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@endif
					@if(Auth::user()->role_id == 4)
						<li class="{{Request::is('pengajuan/*')?'active open':''}}">
							<a href="#" class="dropdown-toggle">
								<i class="menu-icon fa fa-archive"></i>
								<span class="menu-text"> Pengajuan </span>
								<b class="arrow fa fa-angle-down"></b>
							</a>
							<b class="arrow"></b>

							<ul class="submenu">
								<li class="{{Request::is('pengajuan/create')?'active':''}}">
									<a href="{{url('/pengajuan/create')}}" >
										<i class="menu-icon fa fa-caret-right"></i>
										<span class="menu-text">
											Buat Pengajuan
										</span>
									</a>
									<b class="arrow"></b>
								</li>
								<li class="{{Request::is('pengajuan/pengajuan-saya')?'active':''}}">
									<a href="{{url('/pengajuan/pengajuan-saya')}}" >
										<i class="menu-icon fa fa-caret-right"></i>
										<span class="menu-text">
											Pengajuan Saya
										</span>
									</a>
									<b class="arrow"></b>
								</li>
							</ul>
						</li>
					@endif
					@if(Auth::user()->role_id == 1)
					<li class="{{Request::is('pengajuan/*')?'active':''}}">
						<a href="{{ url('/pengajuan/daftar-pengajuan') }}" >
							<i class="menu-icon fa fa-table"></i>
							<span class="menu-text">
								Daftar Pengajuan
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="{{Request::is('user/all')?'active':''}}">
						<a href="{{url('/user/all/')}}" >
							<i class="menu-icon fa fa-group"></i>
							<span class="menu-text">
								Data User
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					@endif
					<li class="{{Request::is('user/edit')?'active':''}}">
						<a href="{{ url('/user/edit') }}" >
							<i class="menu-icon fa fa-key"></i>
							<span class="menu-text">
								Ganti Password
							</span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="{{ url('/auth/logout') }}" >
							<i class="menu-icon fa fa-power-off"></i>
							<span class="menu-text">
								Logout
							</span>
						</a>

						<b class="arrow"></b>
					</li>


				</ul><!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs mobile-header" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<div class="navbar-buttons navbar-header pull-right" role="navigation">
								<span class="user-info">
									Selamat Datang, {{ Auth::user()->nama }}
								</span>
								<a href="{{ url('/auth/logout') }}" class="btn btn-danger">
									<i class="ace-icon fa fa-power-off"></i>
								</a>
						</div>
						<div class="navbar-buttons navbar-header pull-right dropdown" role="navigation">
							<?php
								$cek = App\Model\NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_deleted',1)->lists('notification_id')->toArray();
								$read = App\Model\NotificationUserStatus::where('user_id',Auth::user()->id)->where('is_readed',1)->where('is_deleted','<>',1)->lists('notification_id')->toArray();
								$jum = App\Model\Notification::whereNotIn('id',array_merge($cek,$read))->whereRaw("exists (select * from jsonb_array_elements_text(role_ids) where value = '".Auth::user()->role_id."')")->count();
								$data = App\Model\Notification::whereNotIn('id',$cek)->whereRaw("exists (select * from jsonb_array_elements_text(role_ids) where value = '".Auth::user()->role_id."')")->get();
							?>
								<a class="btn btn-info dropdown-toggle" data-toggle="dropdown">
									<i class="ace-icon fa fa-bell icon-animated-bell"></i>
									@if($jum > 0)
									<span class="badge jumlah-notif" style="">{{$jum}}</span>
									@endif
								</a>
								<ul class="dropdown-menu dropdown-menu-right" style="max-height: 300px;overflow-y: auto">
								@if(count($data) > 0)
								@foreach($data as $j)
									<?php
										$read = App\Model\NotificationUserStatus::where('notification_id',$j->id)->where('is_readed',1)->exists();
									?>
								    <li <?php if(!$read) echo "style='background:#f5f5f5'"; ?> id="notif-{{$j->id}}"><button type="button" aria-hidden="true" data-id="{{$j->id}}" class="close delete-notif" style="position: absolute; right: 5px;">×</button><a href="#"  data-toggle="modal" data-target="#modal-notif" class="notif-read" data-id="{{$j->id}}" data-title="{{$j->title}}" data-sender="{{$j->sender}}" data-konten="{{$j->content}}">

								    	<b>{{$j->sender}}</b><br>
								    	<p>{{$j->title}}</p>

								    </a></li>
								@endforeach
								@else
								<center><i>Tidak Ada Notifikasi</i></center>
								@endif
								  </ul>
						</div>
						<!-- /section:basics/content.searchbox -->
					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						@yield ('content')
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<div id="modal-notif" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title notif-title"></h4>
		      </div>
		      <div class="modal-body">
		        <p class="notif-isi"></p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{ asset('/js/jquery.min.js')}}'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
		</script>
		<script src="{{ asset('/js/bootstrap.min.js')}}"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="{{ asset('/js/ace.min.js')}}"></script>
		<script src="{{ asset('/js/ace-extra.min.js')}}"></script>
		<script src="{{ asset('/js/jquery.maskedinput.js')}}"></script>
		<script src="{{ asset('/js/select2.min.js')}}"></script>
		<script src="{{ asset('/js/parsley.js')}}"></script>
		<script src="{{ asset('/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('js/vue.min.js')}}"></script>
		<script src="{{asset('js/jquery.knob.js')}}"></script>
		<script src="{{asset('js/jquery.ui.widget.js')}}"></script>
		<script src="{{asset('js/jquery.iframe-transport.js')}}"></script>
		<script src="{{asset('js/jquery.fileupload.js')}}"></script>
		<script src="{{asset('js/sweetalert.min.js')}}"></script>
		<script src="{{asset('/moment/js/moment.min.js')}}"></script>
		<script src="{{asset('/daterangepicker/daterangepicker.js')}}"></script>
		<script type="text/javascript" src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>
		<script src="https://cdn.ravenjs.com/3.26.2/raven.min.js" crossorigin="anonymous"></script>
		<script>Raven.config('https://37bfb8986c5544efb1a8e5c7b695bd1f@sentry.io/1238373').install();</script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>   

		<script type="text/javascript">
			function change_title_kgb(val) {
				if(val) {
					$('kgb_title').show();
					$('simpeg_title').hide();
				} else {
					$('kgb_title').hide();
					$('simpeg_title').show();
				}
			}

			$('.notif-read').click(function(){
				$('.notif-title').html($(this).data('title')+"<br><small>Oleh : "+$(this).data('sender')+"</small>");
				$('.notif-isi').html($(this).data('konten'));
				var id = $(this).data('id');
				$.ajax({
					type: "GET",
					cache: false,
					url: "{{ URL::to('home/read-notif') }}",
					data: {"id":id},
					success: function(response) {
						if(response.sisa > 0){
							$('.jumlah-notif').html(response.sisa);
						}else{
							$('.jumlah-notif').remove();
						}
						$('#notif-'+id).css("background-color","white");
					}
				});
			});
			$('.delete-notif').click(function(){
				var id = $(this).data('id');
				$.ajax({
					type: "GET",
					cache: false,
					url: "{{ URL::to('home/delete-notif') }}",
					data: {"id":id},
					success: function(response) {
						if(response.sisa > 0){
							$('.jumlah-notif').html(response.sisa);
						}else{
							$('.jumlah-notif').remove();
						}
						$('#notif-'+id).remove();
					}
				});
			});
			var monthtext=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

			Vue.filter('formatDate', function (value) {
				if(value){
					var date = value.split("-");
					if (date.length < 3) return value;
					var d = parseInt(date[2]);
					var m = parseInt(date[1]);
					var y = date[0];
					return d+" "+monthtext[m-1]+" "+y;
				}else{
					return '-';
				}

			});
		</script>
		<!-- inline scripts related to this page -->
		 @yield('scripts')
	</body><!-- &copy; BKD Kota Bandung 2016 -->
</html>
