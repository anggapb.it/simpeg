<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Page</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ asset('/css/ace-fonts.min.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ asset('/css/ace.min.css') }}" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="{{ asset('/css/ace-rtl.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/sso/style.css') }}" />

		<style type="text/css">
			.head-block {
			  background-color: #3B5874;
			  color: #ffffff;
			  margin-bottom: 10px;
			}
			.head-block {
			  background-color: #3B5874;
			  color: #ffffff;
			  margin-bottom: 10px;
			}
			.head-left {
			  padding: 18px 0 0 25px;
			}
			.head-left h4, .head-left h3 {
			  margin: 0;
			}
			.head-right {
			  padding-top: 20px;
			}
			.head-right p {
			  margin: 0;
			  font-size: 11px;
			}
			.bg {
			  width: 100% !important;
			}

			@media (max-width: 992px) {
			  .head-right {
			    float: right;
			  }
			  .head-left {
			    float: left;
			  }
			}
			@media (min-width: 992px) {
			.middle-block {
			  background-color: #0674b8;
			    /*line-height: 0%;*/
			    /*width: 0px;*/
			/*    border-bottom: 100px solid transparent;
			    border-right: 100px solid #3b5874;*/
			}
			.bg {
			  background: url("{{asset('images/bg.png')}}") no-repeat left center #3b5874;
			}
			}
			.row-eq-height {
			  display: -webkit-box;
			  display: -webkit-flex;
			  display: -ms-flexbox;
			  display:         flex;
			}
		</style>
	</head>

	<body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content" style="padding: 0">
				@yield('content')
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{ asset('/js/jquery.min.js')}}'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
		</script>


		@yield('scripts');
	</body>
</html>
