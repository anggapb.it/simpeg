
 <?php 
    $satuan_kerja = App\Model\SatuanKerja::where('satuan_kerja_id', $satker)->first();
    $unit_kerja = App\Model\UnitKerja::where('unit_kerja_id', Auth::user()->unit_kerja_id)->first();
 ?>
@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
    <tr>
    @include('form.select2',['label'=>'Organisasi','required'=>false,'name'=>'satuan_kerja_id','vue'=>'v-model="params.satuan_kerja_id"','data'=>
        App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>'Pilih Organisasi','value'=>$satker])
    </tr>
@elseif(Auth::user()->role_id == 2)
    <input type="hidden" name="satuan_kerja_id" id="satuan_kerja_id" value="{{$satker}}">
	 <tr>
    @include('form.view',['label'=>'Organisasi','required'=>false,'name'=>'satuan_kerja_id','vue'=>'v-model="params.satuan_kerja_id"','value'=>$satuan_kerja->satuan_kerja_nama])
    </tr>
@elseif(Auth::user()->role_id == 5)
    <input type="hidden" name="satuan_kerja_id" id="satuan_kerja_id" value="{{$satker}}">
     <tr>
    @include('form.view',['label'=>'Organisasi','required'=>false,'name'=>'satuan_kerja_id','vue'=>'v-model="params.satuan_kerja_id"','value'=>$satuan_kerja->satuan_kerja_nama])
    </tr>
@endif
@if(Auth::user()->role_id < 4)
<tr>
    <td>Satuan Organisasi</td>
    <td>:</td>
    <td>
        <select id="satuan_organisasi_id" name="satuan_organisasi_id" style="width:300px" v-model="params.satuan_organisasi_id">
        <?php 
            $sat_ker = App\Model\SatuanKerja::where('satuan_kerja_id', $satker)->lists('satuan_kerja_nama', 'satuan_kerja_id'); 
        ?>
         <option value="">-Pilih-</option>
        @foreach($sat_ker as $id_st => $nama_st)
            <?php
                $uk = App\Model\UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 1)->lists('unit_kerja_nama', 'unit_kerja_id');
            ?>
            @foreach ($uk as $id_unit => $nama_unit)
                 <option value="{{$id_unit}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit}}</option>
                 <?php 
                    $uk2 = App\Model\UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $id_unit)->lists('unit_kerja_nama', 'unit_kerja_id');
                 ?>
            @endforeach
        @endforeach
        </select>
    </td>
</tr>
@elseif(Auth::user()->role_id == 5)
    <input type="hidden" name="satuan_organisasi_id" id="satuan_organisasi_id" value="{{$unit_kerja->unit_kerja_id}}">
     @include('form.view',['label'=>'Satuan Organisasi','required'=>false,'name'=>'satuan_organisasi_id','vue'=>'','value'=>$unit_kerja->unit_kerja_nama])
@endif
@if(Auth::user()->role_id < 4)
<tr>
	<td>Unit Kerja</td>
	<td>:</td>
	<td>
		<select id="unit_kerja_id" name="unit_kerja_id" style="width:300px" v-model="params.unit_kerja_id">
			<option value="">-Pilih-</option>
        </select>
	</td>
</tr>

@elseif(Auth::user()->role_id == 5)
<tr>
     @include('form.select2',['label'=>'Unit Kerja','required'=>true,'name'=>'unit_kerja_id','vue'=>'','data'=>
        App\Model\UnitKerja::where('satuan_kerja_id', $satker)->whereIn('unit_kerja_level',[3,4])->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id'),'empty'=>'Pilih Unit Kerja','value'=>$unit_kerja->unit_kerja_id])
</tr>
@endif

