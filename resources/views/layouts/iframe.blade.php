<?php
	use App\Model\Pegawai2;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>SIMPEG BDG</title>
		<meta name="csrf_token" content="{{ csrf_token() }}" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/bower_components/datatables/media/css/jquery.dataTables.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ asset('/css/ace-fonts.min.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ asset('/css/ace.min.css') }}" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="{{ asset('/css/ace-rtl.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/parsley.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/jquery.fileupload.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-datepicker3.css') }}">
		<style type="text/css">
				.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
					font-size: 12px;
					padding: 4px;
				}
				td>label, th>label {
					font-size: 12px;
				}
				.user-info {
   					max-width: 500px;
   				}
   				.tooltip-inner {
					white-space:pre-wrap;
				}
				.table-responsive {
				    width: 100%;
				    margin-bottom: 15px;
				    overflow-y: hidden;
				    overflow-x: scroll;
				    -ms-overflow-style: -ms-autohiding-scrollbar;
				    border: 1px solid #DDD;
				    -webkit-overflow-scrolling: touch;
				}
				.mobile-header{
						margin-top: -10px;
					}
				@media(max-width: 500px){
					
					.responsive{
						max-width: 100%;
					}
					.tampil{
						display: inline-block;
					}
				}
				@media(min-width: 500px){
					.tampil{
						display: none;
					}
				}
		</style>
		 @yield('styles')
	</head>

	<body class="no-skin">
		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						@yield ('content')
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{ asset('/js/jquery.min.js')}}'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
		</script>
		<script src="{{ asset('/js/bootstrap.min.js')}}"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="{{ asset('/js/ace.min.js')}}"></script>
		<script src="{{ asset('/js/ace-extra.min.js')}}"></script>
		<script src="{{ asset('/js/jquery.maskedinput.js')}}"></script>
		<script src="{{ asset('/js/select2.min.js')}}"></script>
		<script src="{{ asset('/js/parsley.js')}}"></script>
		<script src="{{ asset('/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('js/vue.min.js')}}"></script>
		<script src="{{asset('js/jquery.knob.js')}}"></script>
		<script src="{{asset('js/jquery.ui.widget.js')}}"></script>
		<script src="{{asset('js/jquery.iframe-transport.js')}}"></script>
		<script src="{{asset('js/jquery.fileupload.js')}}"></script>
		<script type="text/javascript" src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>

		<script type="text/javascript">
			var monthtext=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

			Vue.filter('formatDate', function (value) {
				if(value){
					var date = value.split("-");
					if (date.length < 3) return value;
					var d = parseInt(date[2]);
					var m = parseInt(date[1]);
					var y = date[0];
					return d+" "+monthtext[m-1]+" "+y;
				}else{
					return '-';
				}
				
			});
		</script>
		<!-- inline scripts related to this page -->
		 @yield('scripts')
	</body><!-- &copy; BKD Kota Bandung 2016 -->
</html>
