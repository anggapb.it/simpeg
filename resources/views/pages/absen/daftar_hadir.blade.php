@extends('layouts.app')
<?php
  $satker = Auth::user()->satuan_kerja_id;
?>
@section('content')
<center><h3>Daftar Kehadiran Pegawai</h3><br></center>
<div id="vm" class="row">
    <div class="col-xs-12">
    @if(Auth::user()->role_id == '3' || Auth::user()->role_id == '1' || Auth::user()->role_id == '6')
      <center>
       @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
                App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
            </center>
    @endif
    <div class="form-horizontal"> 
      <br>
      <table class="table no-footer">
        @include('layouts.head')
          <tr>
            @include('form.txt2',['label'=>'Tanggal','required'=>false,'name'=>'tanggal','vue'=>'v-model="params.tanggal_from"','value'=>date('Y-m-d')])
          </tr>
        </table>
       <a class="btn btn-default btn-xs" v-on:click="doSearch" style="margin-bottom:20px;">Tampilkan</a>
       <a class="btn btn-primary btn-xs" onclick="cetakDaftarHadir()" style="margin-bottom:20px;">Print</a>
    </div>
  </div>
    <div class="col-xs-12">
      <div id="isi" class="table-responsive">
        <table class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
            <thead>
                <tr>
                    <th rowspan="3" valign="absmiddle">No.</th>
                    <th rowspan="3" valign="absmiddle">NAMA /<br>NIP /<br>USERID</th>
                    <th colspan="12">TINGKAT PRESENSI</th>
                    <th colspan="5">TIDAK HADIR SEHARI PENUH</th>
                    <th rowspan="3">JAM MASUK</th>
                    <th rowspan="3">JAM PULANG</th>
                    {{-- <th rowspan="3">TERLAMBAT</th> --}}
                </tr>
                <tr>
                    <th rowspan="2">DATANG<br>
                    TEPAT<br>
                    WAKTU</th>
                    <th rowspan="2">D</th>
                    <th rowspan="2">S</th>
                    <th rowspan="2">I</th>
                    <th colspan="2">TK</th>
                    <th rowspan="2">PULANG<br>
                    TEPAT<br>
                    WAKTU</th>
                    <th rowspan="2">D</th>
                    <th rowspan="2">S</th>
                    <th rowspan="2">I</th>
                    <th colspan="2">TK</th>
                    <th rowspan="2">D</th>
                    <th rowspan="2">S</th>
                    <th rowspan="2">I</th>
                    <th rowspan="2">C</th>
                    <th rowspan="2">TK</th>
                </tr>
                <tr>
                    <th>K</th>
                    <th>WAKTU</th>
                    <th>K</th>
                    <th>WAKTU</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    {{-- <th>22</th> --}}
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items">
                    <td v-text="item[0]"></td>
                    <td><span v-text="item[1]"></span><br><span v-text="item[22]"></span><br><span v-text="item[23]"></span></td>
                    <td v-text="item[2] | formatCross"></td>
                    <td v-text="item[3] | formatCross"></td>
                    <td v-text="item[4] | formatCross"></td>
                    <td v-text="item[5] | formatCross"></td>
                    <td v-text="item[6] | formatCross"></td>
                    <td v-text="item[7] | formatTime"></td>
                    <td v-text="item[8] | formatCross"></td>
                    <td v-text="item[9] | formatCross"></td>
                    <td v-text="item[10] | formatCross"></td>
                    <td v-text="item[11] | formatCross"></td>
                    <td v-text="item[12] | formatCross"></td>
                    <td v-text="item[13] | formatTime"></td>
                    <td v-text="item[14] | formatCross"></td>
                    <td v-text="item[15] | formatCross"></td>
                    <td v-text="item[16] | formatCross"></td>
                    <td v-text="item[17] | formatCross"></td>
                    <td v-text="item[18] | formatCross"></td>
                    <td v-text="item[20] | formatTime"></td>
                    <td v-text="item[21] | formatTime"></td>
                    {{-- <td v-text="item[26] | formatYesNo"></td> --}}
                </tr>
            </tbody>
        </table>
        <div class="form-inline pull-left">
          Tampilkan
            <select v-model="pagination.perpage" v-on:change="changePage(1)">
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="200">200</option>
                <option value="1000">1000</option>
            </select>
          data
        </div>
        <div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
        <div class="form-inline pull-right">
          <div class="form-group">
            Halaman &nbsp;
            <!--first page button-->
            <a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
            <a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
            <input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
            <a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
            <a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
            &nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
            &nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
          </div>
        </div>
        <br><br><br><br>
      <table width="100%" class="nocetak">
          <tbody>
              <tr>
                  <td colspan="4"><u>Keterangan</u></td>
              </tr>
              <tr>
                  <td width="30">1.</td>
                  <td width="50">D</td>
                  <td width="10">:</td>
                  <td>Dinas Luar (<span v-text="total.persen_hari_d | formatDec"></span> %)</td>
              </tr>
              <tr>
                  <td width="30">2.</td>
                  <td width="50">S</td>
                  <td width="10">:</td>
                  <td>Sakit (<span v-text="total.persen_hari_s | formatDec"></span> %)</td>
              </tr>
              <tr>
                  <td width="30">3.</td>
                  <td width="50">I</td>
                  <td width="10">:</td>
                  <td>Ijin (<span v-text="total.persen_hari_i | formatDec"></span> %)</td>
              </tr>
              <tr>
                  <td width="30">4.</td>
                  <td width="50">TK</td>
                  <td width="10">:</td>
                  <td>Tanpa Keterangan (<span v-text="total.persen_hari_total_tk | formatDec"></span> %)</td>
              </tr>
              <tr>
                  <td width="30">5.</td>
                  <td width="50">K</td>
                  <td width="10">:</td>
                  <td>Berapa Kali Terlambat / Pulang Cepat (<span v-text="total.persen_k | formatDec"></span> %)</td>
              </tr>
              <tr>
                  <td width="30">6.</td>
                  <td width="50">C</td>
                  <td width="10">:</td>
                  <td>Cuti (<span v-text="total.persen_hari_c | formatDec"></span> %)</td>
              </tr>
          </tbody>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0" class="nocetak">
          <tr>
            <td>&nbsp;</td>
            <td align="center"><font face="Arial" size="2"><br>Badan Kepegawaian Daerah<br>Kota Bandung</font></td>
            <td></td>
          </tr>
          <tr>
            <td width="120">&nbsp;</td>
            <td align="center">
          <table border="1" width="80%" cellspacing="0" cellpadding="0">
            </tr>
              <td width="100" align="center"><font face="Arial" size="1">Validasi</font></td>
              <td width="100" align="center"><font face="Arial" size="1">Verifikasi</font></td>
            </tr>
            <tr>
              <td  align="center"><br><br><br><br><br><br><u><font face="Arial" size="1"></font></td>

              <td  align="center"><br><br><br><br><br><br><u></td>
            </tr>
          </table>
          <tr>
            <td width=120>&nbsp;</td>
          </tr>      
        </table>
      </div>
</div>
@endsection
@section('styles')
<style>
  thead {
      text-align: center;
  }
</style>
@endsection

@section('scripts')
<script src="{{asset('js/vue1.0.min.js')}}"></script>
<script type="text/javascript">
Vue.filter('formatCross', function (value) {
  return value ? 'x' : '';
});
Vue.filter('formatTime', function (value,clean) {
  if (!value && clean) return '';
  if (!value && !clean) return '-';
  var h = Math.floor(value/3600) % 24;
  var m = Math.floor((value % 3600) /60);
  var s = value % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  return h+':'+m+':'+s;
});

$('#tanggal').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = yyyy+'-'+mm+'-'+dd;

  var role = "{{Auth::user()->role_id}}";

  function satkerCallback(data){
      $("#satuan_kerja_id").empty();
      $("#satuan_kerja_id").select2("val", "");
      $('#satuan_kerja_id').append($('<option></option>').attr('value', '').text('-Pilih-')); 
       for (row in data) {
          $('#satuan_kerja_id').append($('<option></option>').attr('value', row).text(data[row]));
          $("#satuan_kerja_id").select2({ width: '500px' }); 
      }
  }

  if(role==1 || role ==3 || role == 6){
    $('#satuan_kerja_id').select2({ width: '400px' }).on('change', function (e){
      var id=$("#satuan_kerja_id").val();
      if(id!=0||id!=''){
        $.ajax({
              url: 'https://simpeg.bandung.go.id/siap2/api/get-satuan-organisasi',
              data: {satuan_kerja_id: id},
              dataType: 'jsonp',
              jsonp: 'callback',
              jsonpCallback: 'satorgCallback',
              success: function(){
                //setSatorg();
              }
          });
      }
    });
  }

  function satorgCallback(data){
      $("#satuan_organisasi_id").empty();
      $("#satuan_organisasi_id").select2("val", "");
      $('#satuan_organisasi_id').append($('<option></option>').attr('value', '').text('-Pilih-')); 
      for (row in data) {
          $('#satuan_organisasi_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
          $("#satuan_organisasi_id").select2({ width: '500px' }); 
      }
  }

  $('#satuan_organisasi_id').select2({ width: '400px' }).on('change', function (e){
    var id=$("#satuan_kerja_id").val();
    var parent=$("#satuan_organisasi_id").val();
    if((id!=0||id!='') && (parent!=0||parent!='')){
        $.ajax({
          url: 'https://simpeg.bandung.go.id/siap2/api/get-unit-kerja',
          data: {satuan_kerja_id:id,unit_kerja_parent: parent},
          dataType: 'jsonp',
          jsonp: 'callback',
          jsonpCallback: 'unitkerjaCallback',
          success: function(){
            //setUker();
          }
        });
    }
  });

  function unitkerjaCallback(data){
      $("#unit_kerja_id").empty();
      $("#unit_kerja_id").select2("val", "");
      $('#unit_kerja_id').append($('<option></option>').attr('value', '').text('-Pilih-')); 
      for (row in data) {
          $('#unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
          $("#unit_kerja_id").select2({ width: '500px' }); 
      }
  }

  function setSatker(){
      $("#satuan_kerja_id").select2("val",vm.params.satuan_kerja_id);
      $("#satuan_kerja_id").change();
      vm.doSearch();
  }

  function setSatorg(){
      if(vm.params.satuan_organisasi_id != null || vm.params.satuan_organisasi_id != 0){
        $("#satuan_organisasi_id").select2("val",vm.params.satuan_organisasi_id);
        $("#satuan_organisasi_id").change();
        vm.doSearch();
      }
  }

  function setUker(){
      if((vm.params.satuan_organisasi_id != null && vm.params.unit_kerja_id != null) || (vm.params.satuan_organisasi_id != '' && vm.params.unit_kerja_id != '')){
        $("#unit_kerja_id").select2("val",vm.params.unit_kerja_id);
        $("#unit_kerja_id").change();
        vm.doSearch();
      }
  }

  var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: false,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 200,
      count: 0,
    },
    params: {
      satuan_kerja_id: '',
      satuan_organisasi_id: 0,
      unit_kerja_id: 0,
    },
    tanggal_from:'',
    tanggal_to:'',
    search: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      if (page) this.pagination.page = page;
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_input = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      //console.log("{{Auth::user()->satuan_kerja_id}}");
      //if (!$('#satuan_kerja_id').val() || $('#satuan_kerja_id').val()=='' ) return;
      this.params.satuan_kerja_id = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : "{{Auth::user()->satuan_kerja_id}}";
      this.params.satuan_organisasi_id = $('#satuan_organisasi_id').val();
      this.params.unit_kerja_id = $('#unit_kerja_id').val();
      this.tanggal_from = $('#tanggal').val();
      this.tanggal_to = $('#tanggal').val();
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    }
  }
});
function getData(page) {
  if (!vm.tanggal_from || !vm.tanggal_to) {
    vm.isLoading = false;
    return;
  }
  vm.isLoading = true;
  $.ajax({
        url: "https://simpeg.bandung.go.id/siap2/api/get-daftar-hadir/"+vm.tanggal_from+"/"+vm.tanggal_to,
        data: {
          page: page,
          perpage: vm.pagination.perpage,
          params: vm.params,
          search: vm.search,
          order: vm.sortColumn,
          order_direction: vm.sortDir
    },
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'daftarhadirCallback',
        success: function(data) {
        vm.items = data.data;
        vm.isLoading = false;
        vm.pagination.count = data.count;
        if(data.count==0) {
          alert('Data Tidak Ditemukan!')
        }
        saveState();
      }
  });
}

function saveState() {
  var stored = {
    params : JSON.stringify(vm.params),
    search : vm.search,
    search_input : vm.search_input,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem('daftar_hadir_state',JSON.stringify(stored));
}

function loadState() {
  var stored = JSON.parse(localStorage.getItem('daftar_hadir_state'));
  vm.tanggal_from = today;
  $("#tanggal").val(vm.tanggal_from);
  if (stored) {
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.search_input = stored.search_input;
    vm.sortColumn = stored.sortColumn;
    vm.sortDir = stored.sortDir;
  }
}
loadState();

function cetakDaftarHadir(){
    var satker = $("#satuan_kerja_id option:selected").text() ? $("#satuan_kerja_id option:selected").text() : '{{getSatuanKerjaNama(Auth::user()->satuan_kerja_id)}}';
    var satorg = $('#satuan_organisasi_id option:selected').val() ? $('#satuan_organisasi_id option:selected').text() : '' ;
    var uker = $('#unit_kerja_id option:selected').val() ? $('#unit_kerja_id option:selected').text() : '';
    var tanggal = $('#tanggal').val();
    var printWindow= window.open ('', 'mywindow', 'location=0,status=0,scrollbars=1,width=1000,height=600');
    var strContent = '<html><head><title>Print Preview</title>';
    strContent = strContent +'<style>.cetak_table{ width:100%}' +
    '.cetak_table table {    clear: both;margin-bottom: 0 !important;max-width: none !important;position: relative;}.table-bordered { border-top-width: 0;}' +
    '.table-bordered {'+
    '-moz-border-bottom-colors: none;'+
    '-moz-border-left-colors: none;'+
    '-moz-border-right-colors: none;'+
    '-moz-border-top-colors: none;'+
    'border-collapse: separate;'+
    'border-color: #ddd #ddd #ddd #ddd;'+
    'border-image: none;'+
    'border-style: solid solid solid solid;'+
    'border-width: 1px 1px 1px 1px;'+
    '}'+
    '.table {'+
    'margin-bottom: 20px;'+
    'width: 100%;'+
    'border-spacing: 0;'+
    '}'+
    'table tr td{'+
    'background-color: transparent;'+
    'border: 1px solid #ddd;'+
    'border-collapse: collapse;'+
    'border-spacing: 0;'+
    'max-width: 100%;'+
    '}'+
    'table thead th{'+
    'background-color: transparent;'+
    'border: 1px solid #ddd;'+
    'border-collapse: collapse;'+
    'border-spacing: 0;'+
    'max-width: 100%;'+
    '}'+
    '.nocetak tr td{'+
    'background-color: transparent;'+
    'border: none;'+
    'border-collapse: collapse;'+
    'border-spacing: 0;'+
    'max-width: 100%;'+
    '}'+
    '</style>';
    strContent = strContent + '</head><body>';
    strContent = strContent + '<h2><center>Daftar Kehadiran Pegawai</center></h2>';
    strContent = strContent + '<h3><center>'+satker+'</center></h3>';
    strContent = strContent + '<h3><center>'+satorg+'</center></h3>';
    strContent = strContent + '<h3><center>'+uker+'</center></h3>';
    strContent = strContent + '<h3><center>Tanggal '+tanggal+'</center></h3>';
    strContent = strContent + '<div class=cetak_table>';
    strContent = strContent + document.getElementById('isi').innerHTML;
    strContent = strContent + '</div>';
    strContent = strContent + '</body>';
    printWindow.document.write(strContent);
    printWindow.document.close();
    printWindow.focus();        
    printWindow.print();
}

  </script>
@endsection
