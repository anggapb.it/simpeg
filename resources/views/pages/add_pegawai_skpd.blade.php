@extends('layouts.app')
<?php 
    use App\Model\TingkatPendidikan;
    use App\Model\Pendidikan;
    use App\Model\Jabatan;
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
    use App\Model\KategoriPendidikan;
    use App\Model\Kabupaten;
    use App\Model\Kecamatan;
?>
@section('content')
@if ($message)
    <div class="alert alert-warning"  id="messageFlash">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ $message }}
        <br>
        <a href="{{url('/mutasi-masuk/'.$cek->peg_id)}}" class="btn btn-success btn-xs">
            Mutasi Masuk Pegawai
        </a>
    </div>
@endif
<div class="bs-callout bs-callout-warning hidden">
  <h4>Tambah Pegawai Gagal!</h4>
  <p>Data Harus Lengkap!</p>
</div>
<div class="row">
	<div class="col-md-12">
		<center><h5 class="title"><b>TAMBAH PEGAWAI<br>SATUAN KERJA {{ $satker->satuan_kerja_nama }}</b></h5></center>
        <br>
		<br>
	</div>
</div>
<form action="{{url('/store-pegawai-skpd',$satker->satuan_kerja_id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="form-edit" data-parsley-validate="" onsubmit="parent.scrollTo(0, 0); return true">
	{!! csrf_field() !!}
    <div class="table-responsive">
    <table class="table no-footer">
         <tr>
            <td>Unit Kerja</td>
            <td>:</td>
            <td>
            <select id="unit_kerja_id" name="unit_kerja_id" style="width:300px">
            <?php 
                $sat_ker = SatuanKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('status',1)->lists('satuan_kerja_nama', 'satuan_kerja_id'); 
            ?>
             <option value=""></option>
            @foreach($sat_ker as $id_st => $nama_st)
                 <option value="">{{$nama_st}}</option>
                <?php
                    $uk = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 1)->where('status',1)->lists('unit_kerja_nama', 'unit_kerja_id');
                ?>
                @foreach ($uk as $id_unit => $nama_unit)
                     <option value="{{$id_unit}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit}}</option>
                     <?php 
                        $uk2 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 2)->where('status',1)->where('unit_kerja_parent', $id_unit)->lists('unit_kerja_nama', 'unit_kerja_id');
                     ?>
                    @foreach ($uk2 as $id_unit2 => $nama_unit2)
                        <option value="{{$id_unit2}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit2}}</option>
                        <?php 
                            $uk3 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 3)->where('status',1)->where('unit_kerja_parent', $id_unit2)->lists('unit_kerja_nama', 'unit_kerja_id');
                        ?>
                        @foreach ($uk3 as $id_unit3 => $nama_unit3)
                            <option value="{{$id_unit3}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit3}}</option>
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
            </select>
            </td>
        </tr>
        <tr>
            @include('form.number2',['label'=>'NIP','required'=>true,'name'=>'peg_nip', 'id'=>'txt_peg_nip','placeholder'=>'', 'maxlength'=>'18'])
            @include('form.number',['label'=>'NIP Lama','required'=>false,'name'=>'peg_nip_lama', 'maxlength'=>'9'])
        </tr>
        <tr>
            @include('form.txt',['label'=>'Nama Lengkap','required'=>true,'name'=>'peg_nama', 'id'=>'txt_peg_nama','placeholder'=>'', 'maxlength'=>'200'])
        </tr>
        <tr>
           <td></td>
            <td></td>
            <td>Gelar Depan : <input type="text" name="peg_gelar_depan" value=""/></td>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td><input type="text" name="peg_gelar_belakang" value=""/></td>
        </tr>
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td colspan="7">
                <input type="text" name="peg_lahir_tempat"  value="">,
                <input id="tt1" type="text" name="peg_lahir_tanggal" value="" placeholder="dd-mm-yyyy">
            </td>
        </tr>
        <tr>
             @include('form.file', ['label'=>'Foto','required'=>false,'name'=>'peg_foto', 'value'=>''])
        </tr>
        <tr>
            @include('form.radio2',['label'=>'Jenis Kelamin','required'=>false,'name'=>'peg_jenis_kelamin','data'=>[
            'L' => 'Laki Laki','P' => 'Perempuan' ]])
        </tr>
        <tr>
             @include('form.radio2',['label'=>'Status Perkawinan','required'=>false,'name'=>'peg_status_perkawinan','data'=>[
            '1' => 'Kawin','2' => 'Belum Kawin', '3'=>'Janda', '4'=>'Duda' ]])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Agama','required'=>false,'name'=>'id_agama','data'=>
                App\Model\Agama::lists('nm_agama','id_agama')
            ,'empty'=>''])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Jenis ASN','required'=>false,'name'=>'peg_jenis_asn','data'=>
                App\Model\JenisAsn::lists('nama_jenis_asn','id_jen_asn'),'empty'=>''])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Status Calon','required'=>false,'name'=>'peg_status_calon','data'=>
                App\Model\StatusCalon::lists('status','id_stat_calon'),'empty'=>''])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Jenis PNS','required'=>false,'name'=>'peg_jenis_pns','data'=>
                App\Model\JenisPns::lists('nama_jenis','id_jen_pns'),'empty'=>''])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Status TKD','required'=>false,'name'=>'peg_status_tkd','data'=>
                App\Model\StatusTKD::lists('status_tkd','id'),'empty'=>''])
        </tr>
    <tr>
        @include('form.radio2',['label'=>'Status Pegawai','required'=>false,'name'=>'peg_status_kepegawaian','data'=>[
            '2' => 'CPNS','1' => 'PNS','3' => 'PPPK']])
    <td>TMT CPNS</td>
    <td>:</td>
    <td>
        <input id="tt2" type="text" id="txt_peg_cpns_tmt" name="peg_cpns_tmt" onblur="" onkeyup="" value="" size="12" maxlength="10">
            &nbsp; <font color="red">format : dd-mm-yyyy</font>
    </td>
    </tr>
    <tr style="display:none;" id="tmt_pns">
        <td colspan="3"></td>
        <td>TMT PNS</td>
        <td>:</td>
        <td>
            <input id="tt3" type="text" name="peg_pns_tmt" value=""  size="12" maxlength="10">
             &nbsp; <font color="red">format : dd-mm-yyyy</font>
        </td>
    </tr>
    <tr>
    <td>Pendidikan Awal</td>
    <td>:</td>
    <td>
       <input type="text" id="_tingpend_awal" name="txt_tingpend_awal" value="" size="8" readonly />
        <select id="id_pend_awal" name="id_pend_awal" style="width:300px">
        <?php
            $tingpend = TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id');
        ?>
        @foreach($tingpend as $id_tp => $nama_tp )
            <optgroup label="{{$nama_tp}}">
            <?php 
                $katpend = KategoriPendidikan::where('tingpend_id', $id_tp)->lists('kat_nama', 'kat_pend_id');
            ?>
            @foreach($katpend as $id_kp => $nama_kp)
                <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_kp}}">
                <?php 
                    $pend = Pendidikan::where('kat_pend_id', $id_kp)->orderBy('nm_pend')->lists('nm_pend', 'id_pend');
                ?>
                @foreach ($pend as $id_p => $nama_p)
                    <option value="{{$id_p}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_p}}</option>
                @endforeach
            @endforeach        
        @endforeach
        </select>
      <br/>(<font color="red"><i>Pendidikan waktu diangkat menjadi CPNS</i></font>)
    </td>
    <td>Tahun Pendidikan Awal</td>
    <td>:</td>
    <td><input type="text" name="peg_pend_awal_th"  value="" maxlength="4" size="4"/></td>
</tr>
<tr>
    <td>Pendidikan Akhir</td>
    <td>:</td>
    <td>
        <input type="text" id="_tingpend_akhir" name="txt_tingpend_akhir" value="" size="8" readonly />
          <select id="id_pend_akhir" name="id_pend_akhir" style="width:300px">
        <?php
            $tingpend = TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id');
        ?>
        @foreach($tingpend as $id_tp => $nama_tp )
            <optgroup label="{{$nama_tp}}">
            <?php 
                $katpend = KategoriPendidikan::where('tingpend_id', $id_tp)->lists('kat_nama', 'kat_pend_id');
            ?>
            @foreach($katpend as $id_kp => $nama_kp)
                <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_kp}}">
                <?php 
                    $pend = Pendidikan::where('kat_pend_id', $id_kp)->orderBy('nm_pend')->lists('nm_pend', 'id_pend');
                ?>
                @foreach ($pend as $id_p => $nama_p)
                    <option value="{{$id_p}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_p}}</option>
                @endforeach
            @endforeach        
        @endforeach
        </select>
        <br/>(<font color="red"><i>Pendidikan sesuai ijazah terakhir</i></font>)
    </td>
    <td>Tahun Pendidikan Akhir</td>
    <td>:</td>
    <td><input type="text" name="peg_pend_akhir_th"  value=""  value="" maxlength="4" size="4"></td>
</tr>
<tr>
    @include('form.txt', ['label'=>'Angka Kredit', 'required'=>false, 'name'=>'peg_ak', 'maxlength'=>'50'])
</tr>
<tr>
     @include('form.select2',['label'=>'Jenis Jabatan','required'=>true,'name'=>'jenis_jabatan','data'=>
     ['2' => 'Struktural','3' => 'Fungsional Tertentu', '4'=>'Fungsional Umum' ],'empty'=>'-Pilih-'])
</tr>
<tr>
    @include('form.select2',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_id','data'=>
             [''=>'-Pilih-'],'empty'=>'-Pilih-'])
</tr>
<tr>
    <td>TMT Jabatan</td>
    <td>:</td>
    <td>
        <input id="tt7" type="text" name="peg_jabatan_tmt" value="" size="12" maxlength="10">
        &nbsp; <font color="red">format : dd-mm-yyyy</font>
    </td>
</tr>
<tr>
   @include('form.txt', ['label'=>'Instansi (jika dipekerjakan)', 'required'=>false, 'name'=>'peg_instansi_dpk', 'maxlength'=>'200'])
</tr>
<tr>
   @include('form.select2', ['label'=>'Status Gaji', 'required'=>false, 'name'=>'peg_status_gaji','data'=>['1'=>'Pemkot', '2'=>'Luar Pemkot'] ,'empty'=>'-Pilih-'])
</tr>
<tr>
   @include('form.select2', ['label'=>'Kedudukan Pegawai', 'required'=>false, 'name'=>'id_status_kepegawaian',
   'data'=>App\Model\StatusKepegawaian::lists('status','id_status_kepegawaian') ,'empty'=>'-Pilih-'])
</tr>
<tr>
   @include('form.select2',['label'=>'Golongan Awal','required'=>true,'name'=>'gol_id_awal','data'=>
                App\Model\Golongan::orderBy('gol_id')->lists('nm_gol','gol_id')
            ,'empty'=>'-Pilih-'])
<td>TMT Golongan Awal</td>
<td>:</td>
<td>
    <input id="tt4" type="text" name="peg_gol_awal_tmt" value="" size="12" maxlength="10">
</td>
</tr>
<tr id='cpns2'>
  @include('form.select2',['label'=>'Golongan Akhir','required'=>true,'name'=>'gol_id_akhir','data'=>
                App\Model\Golongan::orderBy('gol_id')->lists('nm_gol','gol_id')
            ,'empty'=>'-Pilih-'])
    <td>TMT Golongan Akhir</td>
    <td>:</td>
    <td>
        <input id="tt5" type="text" name="peg_gol_akhir_tmt" value="" size="12" maxlength="10">
    </td>
</tr>
<tr>
    <td>Masa Kerja Golongan</td>
    <td>:</td>
    <td colspan="7">
        <input type="text" class="quantity" id="txt_peg_kerja_tahun" name="peg_kerja_tahun" value="" size='2' maxlength='2' autocomplete="off" style="text-align:center" /> Tahun
        <input type="text" class="quantity" name="peg_kerja_bulan" value="" size='2' maxlength='2' autocomplete="off" style="text-align:center" /> Bulan
        
    </td>
</tr>
<tr id="karsuis">
    @include('form.txt', ['label'=>'No. KARPEG', 'required'=>false, 'name'=>'peg_karpeg', 'maxlength'=>'50'])
    @include('form.txt', ['label'=>'No. Karis/Karsu', 'required'=>false, 'name'=>'peg_karsutri', 'maxlength'=>'50'])
</tr>
<tr>
    @include('form.txt',['label'=>'No. Askes','required'=>false,'name'=>'peg_no_askes', 'maxlength'=>'50'])
</tr>
<tr>
    @include('form.txt',['label'=>'No. KTP','required'=>false,'name'=>'peg_ktp', 'maxlength'=>'50'])
   @include('form.txt',['label'=>'NPWP','required'=>false,'name'=>'peg_npwp', 'maxlength'=>'50'])
</tr>
<tr>
    @include('form.select2',['label'=>'Golongan Darah','required'=>false,'name'=>'id_goldar','data'=>
    App\Model\GolonganDarah::orderBy('nm_goldar')->lists('nm_goldar','id_goldar') ,'empty'=>'-Pilih-'])
</tr>
<tr>
     @include('form.select',['label'=>'BAPETARUM','required'=>false,'name'=>'peg_bapertarum','data'=>
    [''=>'-Pilih-','1' => 'Sudah Diambil','2' => 'Belum Diambil' ]])
</tr>
<tr>
    <td>TMT Gaji Berkala Terakhir</td>
    <td>:</td>
    <td colspan="7">
        <input id="tt6" type="text" name="peg_tmt_kgb"  value="" size="12" maxlength="10">
    </td>
</tr>
<tr>
    <td>Alamat Rumah</td>
    <td>:</td>
    <td colspan="7">
        <input type="text" name="peg_rumah_alamat" value="" maxlength="255" size="136" autocomplete="off">
    </td>
</tr>
<tr>
    <td colspan="2"></td>
    <td colspan="7">
        <table width="100%"  border="0" bordercolor="red" cellspacing='0'>
            <tr>
                @include('form.txt',['label'=>'Kel./Desa', 'required'=>false, 'name'=>'peg_kel_desa', 'value'=>'', 'maxlength'=>'200'] )
                @include('form.select2',['label'=>'Kec.', 'required'=>false, 'name'=>'kecamatan_id','data'=>
    App\Model\Kecamatan::orderBy('kecamatan_id')->lists('kecamatan_nm','kecamatan_id') ,'empty'=>'-Pilih-', 'value'=>''])
            </tr>
            <tr>
                <td>Kab./Kota</td>
                <td>:</td>
                <td>
                    <input type="text" name="kabupaten_nm" id="kabupaten_nm" value="" readonly size="39"/>
                    &nbsp; <font color="red">(Pilih Kecamatan Terlebih Dahulu!)</font>
                    <input type="hidden" name="kabupaten_id" id="kabupaten_id" value="">
                </td>
                @include('form.txt',['label'=>'Kode Pos', 'required'=>false, 'name'=>'peg_kodepos', 'value'=>'', 'maxlength'=>'50'] )
            </tr>
            <tr>
                @include('form.txt',['label'=>'Telp', 'required'=>false, 'name'=>'peg_telp', 'value'=>'', 'maxlength'=>'20'] )
                @include('form.txt',['label'=>'HP', 'required'=>false, 'name'=>'peg_telp_hp', 'value'=>'', 'maxlength'=>'20'] )
            </tr>
            <tr>
                @include('form.txt',['label'=>'Email', 'required'=>false, 'name'=>'peg_email', 'value'=>'', 'maxlength'=>'50'] )
            </tr>
        </table>
    </td>
</td>
<tr>
    <td colspan="9" align="center">
        <input type="submit" value="Simpan" name="simpan">
    </td>
</tr>
</table>
</div>
</form>
<div class="modal fade" id="modal-jabatan-fu" tabindex="-1" role="dialog" aria-labelledby="#modal-jabatan-fu" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Tambah Jabatan Fungsional Umum</h4>
        </div>
       
        <div class="modal-body" id="modal-detail-content">
            @include('form.select2',['label'=>'Nama Jabatan', 'required'=>false, 'name'=>'jfu_id','data'=>
    App\Model\JabatanFungsionalUmum::orderBy('jfu_nama')->lists('jfu_nama','jfu_id') ,'empty'=>'-Pilih-'])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitNewJabatanFu" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$('#form-edit').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
})

jQuery(function($){
     $("#tt1").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt2").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt3").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt4").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt5").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt6").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt7").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
});

// $('#peg_nip').on('focusout', function() {
//     var v = $('#peg_nip').val();
//     $.ajax({
//         type: "GET",
//         cache: false,
//         url: "{{ URL::to('pegawai/cek-nip') }}",
//         beforeSend: function (xhr) {
//             var token = $('meta[name="csrf_token"]').attr('content');

//             if (token) {
//                   return xhr.setRequestHeader('X-CSRF-TOKEN', token);
//             }
//         },
//         data: {"peg_nip":v},
//         dataType: 'json',
//         success: function(response) { 
//             if(response == "false"){
//                 alert("NIP sudah terdaftar! Silahkan ke menu Mutasi Masuk.");
//                 $('#peg_nip').val("");
//             }
//         }
//     }).error(function (e){
//         console.log(e);
//         alert("ERROR!");
//     });
// });

$('#unit_kerja_id').select2({width: '500px'}).on('change', function (e){
    var unit_kerja_id = $('#unit_kerja_id').val();
});

$('#id_pend_awal').select2({ width: 'resolve' }).on('change', function (e) {
    var v = $('#id_pend_awal').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/tingkat-pendidikan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kat_pend_id":v},
        dataType: 'json',
        success: function(response) {
            $('#_tingpend_awal').val(response);
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
    
});

$('#id_pend_akhir').select2({ width: 'resolve' }).on('change', function (e) {
    var v = $('#id_pend_akhir').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/tingkat-pendidikan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kat_pend_id":v},
        dataType: 'json',
        success: function(response) {
            $('#_tingpend_akhir').val(response);
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
    
});

$('#jenis_jabatan').select2({ width: '350px' }).on('change', function (e){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatanNew') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":'{{$satker->satuan_kerja_id}}'},
        dataType: 'json',
        success: function(data) {
            $('#jabatan_id').empty();
            for (row in data) {
                 //var jab = data[row].jabatan_id;
                //var jab = data['jabatan'].jabatan_id;

                if(data['jenis'] == 3){
                    $('#jabatan_id').empty();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else if(data['jenis'] == 4){
                    $('#jabatan_id').empty();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else if(data['jenis']==2){
                    $('#jabatan_id').empty();
                    for(r in data['jabatan']){

                    //console.log(data['jabatan'][r].jabatan_nama);
                    $('#jabatan_id').append($('<option></option>').attr('value', data['jabatan'][r].jabatan_id).text(data['jabatan'][r].jabatan_nama));
                    }
                }
            }
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
});

$('#jabatan_id').select2({ width: '350px' });

$('#kecamatan_id').select2({ width: '250px' }).on('change', function (e) {
    var v = $('#kecamatan_id').val();
    if(v != ''){
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/kabupaten') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kecamatan_id":v},
        dataType: 'json',
        success: function(data) {
            $('#kabupaten_nm').val(data.nama);
            $('#kabupaten_id').val(data.id);
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
    }else{
        $('#kabupaten_nm').val('');
        $('#kabupaten_id').val('');
    }
});

$('#peg_status_kepegawaian2').on('change', function (e) {
     $('#tmt_pns').show();
});

$('#peg_status_kepegawaian1').on('change', function (e) {
     $('#tmt_pns').hide();
});

function getJabatanAwal(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":'{{$satker->satuan_kerja_id}}'},
        dataType: 'json',
        success: function(data) {
            $("#jabatan_id").empty();
            for (row in data) {
                var jab = data[row].jabatan_id;
                if(data[row].jabatan_jenis == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jf_nama));
                            }
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else if(data[row].jabatan_jenis == 4){
                    $('#tampilJenisButton').show();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else{
                    $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jabatan_nama));
                }
            }
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
}
$("#tampilJenisButton").click(function(e){
     e.preventDefault();
     $('#jfu_id').select2({ width: '600px' });
     $("#submitNewJabatanFu").click(function() {
        var jfu_id = $("#jfu_id").val();
        var unit_kerja_id = $('#unit_kerja_id').val();
        var satuan_kerja_id = $('#satuan_kerja_id').val();
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('pegawai/new_jfu') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"jfu_id":jfu_id, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":'{{$satker->satuan_kerja_id}}'},
            
            success: function(response) {
                alert("Data berhasil ditambahkan");
                $('#modal-jabatan-fu').modal('toggle');
                var op, index, select, option;

                // Get the raw DOM object for the select box
                select = document.getElementById('jabatan_id');

                // Clear the old options
                select.length = 0;

                // Load the new options
                op = response; // Or whatever source information you're working with
                for (index = 0; index < op.length; ++index) {
                  option = op[index];
                  select.add(new Option(option.jfu_nama, option.jfu_id));
                }
                getJabatanAwal();

            }
        }).error(function (e){
            console.log(e);
            alert("ERROR!");
        });
    });
});
$(".quantity").on("keydown", function (e) {
    var key   = e.keyCode ? e.keyCode : e.which;

    if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
         (key == 65 && ( e.ctrlKey || e.metaKey  ) ) || 
         (key >= 35 && key <= 40) ||
         (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
         (key >= 96 && key <= 105)
       )) e.preventDefault();
});
$('#peg_foto').bind('change', function() {
    var size = this.files[0].size;
    var max = 2097152;
    if(size>max){
        alert('Ukuran foto harus lebih kecil dari 2 MB');
        $('#peg_foto').val('');
    }
});
var vm = new Vue({
  el: '#form-edit',
  data: {
    pegnip:'',
  },
  methods: {
    doSearch: function(){
        $.getJSON("{{url('pegawai/add-cek')}}", {peg_nip: this.pegnip}, function(data) {
          if(data.pegawai != 0){
            var url = "{{url('/pegawai/profile/edit/')}}/"+data.pegawai.peg_id;
            // $('.error').html('<div class="alert alert-danger">Nip Sudah Ada, '+data.pegawai.peg_nama+' <a href="'+url+'" class="btn btn-danger btn-xs">Lihat Pegawai</a></div>');
            var html = '';
            html += '<div class="alert alert-danger">Nip Sudah Ada, '+data.pegawai.peg_nama;
            @if(Auth::user()->role_id == 1)
            html += '<a href="'+url+'" class="btn btn-danger btn-xs">Lihat Pegawai</a>';
            @endif
            html += '</div>';

            $('.error').html(html);    
          }else{
            $('.error').html("");
          }

          if(!data.valid){
            $('.valid').html('<div class="alert alert-danger">NIP Tidak Valid</div>');
          }else{
            $('.valid').html("");
          }
        });
    }
  }
});
</script>
@endsection