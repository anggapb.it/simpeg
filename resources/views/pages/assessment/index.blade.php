@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
  <div class="row">
    <h2>
        Upload Hasil Assessment
    </h2>
    <hr />
    <span class="small">Petunjuk:</i></span>
    <br />
    <span class="small">File berbentuk Spreadsheet dan berekstensi *.xls <i>(Microsoft Excel 97-2003 Workbook)</i></span>
    <br />
    <span class="small">File hanya boleh memiliki 1 (satu) sheet dan data harus dimulai dari baris ke-7 (tujuh)</i></span>
    <br />
    <span class="small">Contoh File dapat didownload <a href="{{url('/uploads-resize/expo/tp.xlsx')}}">disini.</a></i></span>
    <hr />
    @if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
            <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
            </button>
            {{ session('message') }}
        </div>
	@endif
    <form method="post" action="{{url('/upload-assessment')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="table">
        <table class="table no-footer">
            <tr>
            @include('form.file2', ['label'=>'File','required'=>true,'name'=>'excelfile', 'value'=>''])                            
            <br />
            @include('form.text4',['label'=>'Tanggal','required'=>true,'name'=>'tanggal','placeholder'=>'yyyy-mm-dd'])
            <br />                           
            <input type="submit" value="Upload" class="btn btn-primary btn-xs" />
            </tr>
        </table>
    </div>
    </form>    
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(".tanggal").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
</script>
@endsection