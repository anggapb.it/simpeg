@extends('layouts.app')
<?php
    use App\Model\TingkatPendidikan;
    use App\Model\Pendidikan;
    use App\Model\Jabatan;
    use App\Model\JabatanFungsional;
    use App\Model\JabatanFungsionalUmum;
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
    use App\Model\CalonJft;
    use App\Model\KategoriPendidikan;
    use App\Model\Kabupaten;
    use App\Model\Kecamatan;
   // dd($pegawai);
    $calonjft = CalonJft::where('peg_id',$pegawai->peg_id)->first();
    $pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
    $pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
    if($pend_awal){
        $kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first();
        if($kat_pend_awal){
            $tingpend_awal = TingkatPendidikan::where('tingpend_id', $kat_pend_awal->tingpend_id)->first();
        }
    }
    if($pend_akhir){
        $kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first();
        if($kat_pend_akhir){
            $tingpend_akhir = TingkatPendidikan::where('tingpend_id', $kat_pend_akhir->tingpend_id)->first();
        }
    }
    $jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id)->first();
    if($jabatan){
        $jabatan_jenis= $jabatan->jabatan_jenis;
        $jabatan_id = $pegawai->jabatan_id;
        if($jabatan_jenis == 3){
            $jf = $jabatan->jf_id;
        }elseif ($jabatan_jenis == 4) {
            $jfu = $jabatan->jfu_id;
        }
        $satuan_kerja =SatuanKerja::where('satuan_kerja_id', $jabatan->satuan_kerja_id)->first();
    }else{
        $jabatan_nama ='';
        $jabatan_id =0;
        $jabatan_jenis =0;
    }
    //$jabatan_nama = $jabatan->jabatan_nama;

    $sopd_nama = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();

    $kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
    if($kecamatan){
        $kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
    }else{
        $kab = null;
    }
    //$uk = [''=>''] + App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id')->all();
    $uk = App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('unit_kerja_id', $pegawai->unit_kerja_id)->where('status',1)->select('unit_kerja_nama')->first();
    $nama_unit_kerja = $uk ? $uk->unit_kerja_nama : '';
?>
@section('content')
<div class="bs-callout bs-callout-warning hidden">
  <h4>Edit Data Gagal!</h4>
  <p>Data Harus Lengkap!</p>
</div>

<div class="row">
    <div class="col-md-12">
        <center><h5 class="title"><b>EDIT PEGAWAI</b></h5></center>
        <br>
        <small class="tampil">*Slide Ke Kiri Untuk Mengisi Semua Form</small>
    </div>
    @if (session('message'))
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
    @endif
</div>
<form action="{{url('/update-pegawai',$pegawai->peg_id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="form-edit" data-parsley-validate="" onsubmit="parent.scrollTo(0, 0); return true">
    {!! csrf_field() !!}
    <div class="table-responsive">
    <table class="table no-footer">
        <tr>
            <?php
            $satker = SatuanKerja::where('satuan_kerja_id',$pegawai->satuan_kerja_id)->first();
            ?>
            @include('form.txt',['label'=>'Satuan Kerja','required'=>true,'name'=>'satuan_kerja', 'id'=>'satuan_kerja','placeholder'=>'', 'value'=>$satker ? $satker->satuan_kerja_nama : '-','readonly'=>true,'maxlength'=>500])
            <td>
            <input type="hidden" name="satuan_kerja_id" id="satuan_kerja_id" value="{{$pegawai->satuan_kerja_id}}">
            </td>
        </tr>
        <tr>
            <?php
            $uker = UnitKerja::where('unit_kerja_id',$pegawai->unit_kerja_id)->first();
            ?>
            @include('form.txt',['label'=>'Unit Kerja','required'=>false,'name'=>'unit_kerja', 'id'=>'unit_kerja','placeholder'=>'', 'value'=>$uker ? $uker->unit_kerja_nama : '','readonly'=>true,'maxlength'=>500])
            <td>
            <input type="hidden" name="unit_kerja_id" id="unit_kerja_id" value="{{$pegawai->unit_kerja_id}}">
            @if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,2,5] : [1]))
            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-jabatan">Update Jabatan</button>
            @else
            <button type="button" class="btn btn-info btn-xs" disabled>Update Jabatan (Fitur Disabled)</button>
            @endif
            </td>
        </tr>
        @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 8)
        <tr>
            @include('form.number2',['label'=>'NIP','required'=>true,'name'=>'peg_nip', 'id'=>'txt_peg_nip','placeholder'=>'', 'maxlength'=>'18', 'value'=>$pegawai->peg_nip])
            @include('form.number',['label'=>'NIP Lama','required'=>false,'name'=>'peg_nip_lama', 'maxlength'=>'9', 'value'=>$pegawai->peg_nip_lama])
        </tr>
        @else
        <tr>
            <td>NIP</td>
            <td>:</td>
            <td>{{$pegawai->peg_nip}}
            <input type="hidden" name="peg_nip" id="txt_peg_nip" value="{{$pegawai->peg_nip}}">
            </td>
            <td>NIP Lama</td>
            <td>:</td>
            <td>{{$pegawai->peg_nip_lama}}
            <input type="hidden" name="peg_nip_lama" id="peg_nip_lama" value="{{$pegawai->peg_nip_lama}}">
            </td>
        </tr>
        @endif
        @if(Auth::user()->role_id == 1)
        <tr>
            @include('form.txt',['label'=>'Nama Lengkap','required'=>true,'name'=>'peg_nama', 'id'=>'txt_peg_nama','placeholder'=>'', 'value'=>$pegawai->peg_nama, 'maxlength'=>'100'])
        </tr>
        @else
            <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td><?php echo $pegawai->peg_nama; ?><input type="hidden" name="peg_nama" value="<?php echo $pegawai->peg_nama; ?>"></td>            
            </tr>
        @endif
        @if(Auth::user()->role_id == 1 || Auth::user()->id == 2745)
        <tr>
           <td></td>
            <td></td>
            <td>Gelar Depan : <input type="text" name="peg_gelar_depan" value="{{$pegawai->peg_gelar_depan or ''}}"/></td>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td><input type="text" name="peg_gelar_belakang" value="{{$pegawai->peg_gelar_belakang or ''}}"/></td>
        </tr>
        @else
        <tr>
           <td></td>
            <td></td>
            <td>Gelar Depan : {{$pegawai->peg_gelar_depan or '-'}} </td>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td>{{$pegawai->peg_gelar_belakang or ''}}</td>
        </tr>
        <input type="hidden" name="peg_gelar_depan" id="peg_gelar_depan" value="{{$pegawai->peg_gelar_depan or ''}}" />
        <input type="hidden" name="peg_gelar_belakang" id="peg_gelar_belakang" value="{{$pegawai->peg_gelar_belakang or ''}}" />
        @endif
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td colspan="7">
                <input type="text" name="peg_lahir_tempat"  value="{{$pegawai->peg_lahir_tempat or ''}}">,
                <input id="tt1" type="text" name="peg_lahir_tanggal" value="{{editDate($pegawai->peg_lahir_tanggal)}}" placeholder="dd-mm-yyyy">
            </td>
        </tr>
        <tr>
             @include('form.file', ['label'=>'Foto','required'=>false,'name'=>'peg_foto', 'value'=>$pegawai->peg_foto, 'placeholder'=>$pegawai->peg_foto])
        </tr>
        <tr>
            @include('form.radio2',['label'=>'Jenis Kelamin','required'=>false,'name'=>'peg_jenis_kelamin','data'=>[
            'L' => 'Laki Laki','P' => 'Perempuan' ], 'value'=>$pegawai->peg_jenis_kelamin])
        </tr>
        <tr>
             @include('form.radio2',['label'=>'Status Perkawinan','required'=>false,'name'=>'peg_status_perkawinan','data'=>[
            '1' => 'Kawin','2' => 'Belum Kawin', '3'=>'Janda', '4'=>'Duda' ], 'value'=>$pegawai->peg_status_perkawinan])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Agama','required'=>false,'name'=>'id_agama','data'=>
                App\Model\Agama::lists('nm_agama','id_agama'),'empty'=>'','value'=>$pegawai->id_agama])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Jenis ASN','required'=>false,'name'=>'peg_jenis_asn','data'=>
                App\Model\JenisAsn::lists('nama_jenis_asn','id_jen_asn'),'empty'=>'','value'=>$pegawai->peg_jenis_asn])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Status Calon','required'=>false,'name'=>'peg_status_calon','data'=>
                App\Model\StatusCalon::lists('status','id_stat_calon'),'empty'=>'','value'=>$pegawai->peg_status_calon])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Jenis PNS','required'=>false,'name'=>'peg_jenis_pns','data'=>
                App\Model\JenisPns::lists('nama_jenis','id_jen_pns'),'empty'=>'','value'=>$pegawai->peg_jenis_pns])
        </tr>
        <tr>
            @include('form.select2',['label'=>'Status TKD','required'=>false,'name'=>'peg_status_tkd','data'=>
                App\Model\StatusTKD::lists('status_tkd','id'),'empty'=>'','value'=>$pegawai->status_tkd])
        </tr>
    <tr>
        @include('form.radio2',['label'=>'Status Pegawai','required'=>false,'name'=>'peg_status_kepegawaian','data'=>[
            '2' => 'CPNS','1' => 'PNS','3' => 'PPPK'], 'value'=>$pegawai->peg_status_kepegawaian])
    <td>TMT CPNS</td>
    <td>:</td>
    <td>
        <input id="tt2" type="text" id="txt_peg_cpns_tmt" name="peg_cpns_tmt" onblur="" onkeyup="" value="{{editDate($pegawai->peg_cpns_tmt)}}" size="12" maxlength="10">
            &nbsp; <font color="red">format : dd-mm-yyyy</font>
    </td>
    </tr>
    <tr id="tmt_pns">
        <td colspan="3"></td>
        <td>TMT PNS</td>
        <td>:</td>
        <td>
            <input id="tt3" type="text" name="peg_pns_tmt" value="{{editDate($pegawai->peg_pns_tmt)}}"  size="12" maxlength="10">
             &nbsp; <font color="red">format : dd-mm-yyyy</font>
        </td>
    </tr>
    @if(Auth::user()->role_id == 1 || Auth::user()->id == 2745)
    <tr>
    <td>Pendidikan Awal</td>
    <td>:</td>
    <td>
        <input type="text" id="_tingpend_awal" name="txt_tingpend_awal" value="{{$tingpend_awal->nm_tingpend or ''}}" size="8" readonly />
        <select id="id_pend_awal" name="id_pend_awal" style="width:300px">
        <?php
            $tingpend = TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id');
        ?>
        @foreach($tingpend as $id_tp => $nama_tp )
            <optgroup label="{{$nama_tp}}">
            <?php
                $katpend = KategoriPendidikan::where('tingpend_id', $id_tp)->lists('kat_nama', 'kat_pend_id');
            ?>
            @foreach($katpend as $id_kp => $nama_kp)
                <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_kp}}">
                <?php
                    $pend = Pendidikan::where('kat_pend_id', $id_kp)->orderBy('nm_pend')->lists('nm_pend', 'id_pend');
                ?>
                @foreach ($pend as $id_p => $nama_p)
                    <option value="{{$id_p}}"{{$pegawai->id_pend_awal===$id_p?' selected':''}}>&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_p}}</option>
                @endforeach
            @endforeach
        @endforeach
        </select>
      <br/>(<font color="red"><i>Pendidikan waktu diangkat menjadi CPNS</i></font>)
    </td>
    <td>Tahun Pendidikan Awal</td>
    <td>:</td>
    <td><input type="text" name="peg_pend_awal_th"  value="{{$pegawai->peg_pend_awal_th}}" maxlength="4" size="4"/></td>
    </tr>
    <tr>
    <td>Pendidikan Akhir</td>
    <td>:</td>
    <td>
        <input type="text" id="_tingpend_akhir" name="txt_tingpend_akhir" value="{{$tingpend_akhir->nm_tingpend or ''}}" size="8" readonly />
          <select id="id_pend_akhir" name="id_pend_akhir" style="width:300px">
        <?php
            $tingpend = TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id');
        ?>
        @foreach($tingpend as $id_tp => $nama_tp )
            <optgroup label="{{$nama_tp}}">
            <?php
                $katpend = KategoriPendidikan::where('tingpend_id', $id_tp)->lists('kat_nama', 'kat_pend_id');
            ?>
            @foreach($katpend as $id_kp => $nama_kp)
                <optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_kp}}">
                <?php
                    $pend = Pendidikan::where('kat_pend_id', $id_kp)->orderBy('nm_pend')->lists('nm_pend', 'id_pend');
                ?>
                @foreach ($pend as $id_p => $nama_p)
                    <option value="{{$id_p}}"{{$pegawai->id_pend_akhir===$id_p?' selected':''}}>&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_p}}</option>
                @endforeach
            @endforeach
        @endforeach
        </select>
        <br/>(<font color="red"><i>Pendidikan sesuai ijazah terakhir</i></font>)
    </td>
    <td>Tahun Pendidikan Akhir</td>
    <td>:</td>
    <td><input type="text" name="peg_pend_akhir_th" value="{{$pegawai->peg_pend_akhir_th}}" maxlength="4" size="4"></td>
    </tr>
    @else
    <input type="hidden" name="id_pend_awal" id="id_pend_awal" value="{{$pegawai->id_pend_awal}}" />
    <input type="hidden" name="peg_pend_awal_th" id="peg_pend_awal_th" value="{{$pegawai->peg_pend_awal_th}}" />
    <input type="hidden" name="id_pend_akhir" id="id_pend_akhir" value="{{$pegawai->id_pend_akhir}}" />
    <input type="hidden" name="peg_pend_akhir_th" id="peg_pend_akhir_th" value="{{$pegawai->peg_pend_akhir_th}}" />
    @endif
<tr>
    @if($jabatan_jenis == 2)
        <?php $jenjab = 'Struktural'; ?>
    @elseif($jabatan_jenis == 3)
        <?php $jenjab = 'Fungsional Tertentu'; ?>
    @else
        <?php $jenjab = 'Fungsional Umum'; ?>
    @endif
     @include('form.txt',['label'=>'Jenis Jabatan','required'=>true,'name'=>'jenjab','empty'=>'-Pilih-', 'value'=>$jenjab,'readonly' => true,'maxlength'=>500])
     <td>
     <input type="hidden" name="jenis_jabatan" id="jenis_jabatan" value="{{$jabatan_jenis}}">
     </td>
</tr>
<tr>
    @if($jabatan_jenis == 2)
    <?php
        $struk = App\Model\Jabatan::where('jabatan_id',$pegawai->jabatan_id)->first();
    ?>
        @include('form.txt',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$struk->jabatan_nama,'readonly' => true,'maxlength'=>500])
    @elseif($jabatan_jenis == 3)
        <?php
            $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jf_id')->get(['jf_id']);
            $id = [];
            $jafung = JabatanFungsional::where('jf_id',$jf)->first();
            foreach ($jab as $j) {
                $id[] = $j->jf_id;
            }
        ?>
        @include('form.txt',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$jafung?$jafung->jf_nama:'','readonly' => true,'maxlength'=>500])

    @elseif($jabatan_jenis == 4)
    <?php
        $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jfu_id')->get(['jfu_id']);
        $id = [];
        $jafungumum = JabatanFungsionalUmum::where('jfu_id',$jfu)->first();
        foreach ($jab as $j) {
            $id[] = $j->jfu_id;
        }
    ?>
        @include('form.txt',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$jafungumum ? $jafungumum->jfu_nama : 'Pelaksana','readonly' => true,'maxlength'=>500])
    @endif
    <td>
    <input type="hidden" name="jabatan_id" id="jabatan_id" value="{{$pegawai->jabatan_id}}">
    @if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,2,5] : [1]))
    <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-jabatan">Update Jabatan</button>
    @else
    <button type="button" class="btn btn-info btn-xs" disabled>Update Jabatan (Fitur Disabled)</button>
    @endif
    </td>
</tr>


@if($jabatan_jenis == 3)
<?php
    $jafung = JabatanFungsional::where('jf_id',$jf)->first();
?>
@if($jafung->rumpun_id == 17)
<tr>
    @include('form.select2', ['label'=>'Jenis Guru', 'required'=>false, 'name'=>'jenis_guru','data'=>['Guru Kelas'=>'Guru Kelas','Guru Agama Islam'=>'Guru Agama Islam','Guru Penjasorkes'=>'Guru Penjasorkes','Guru PPKN'=>'Guru PPKN','Guru Bahasa Indonesia'=>'Guru Bahasa Indonesia','Guru Bahasa Inggris'=>'Guru Bahasa Inggris','Guru Matematika'=>'Guru Matematika','Guru IPA'=>'Guru IPA','Guru IPS'=>'Guru IPS','Guru Seni Budaya'=>'Guru Seni Budaya','Guru TIK'=>'Guru TIK','Guru Muatan Lokal'=>'Guru Muatan Lokal','Guru Prakarya dan Kewirausahaan'=>'Guru Prakarya dan Kewirausahaan','Guru Bimbingan dan Konseling'=>'Guru Bimbingan dan Konseling'] ,'empty'=>'-Pilih-' ,'value'=>$pegawai->jenis_guru])
</tr>

@endif
@endif

@if($jabatan_jenis == 4)
<?php
    $jafungumum2 = JabatanFungsionalUmum::where('jfu_id',$jfu)->first();
?>
@if($jafungumum2->jfu_id == 580)
<tr>
    @include('form.select2', ['label'=>'Jenis Guru', 'required'=>false, 'name'=>'jenis_guru','data'=>['Guru Kelas'=>'Guru Kelas','Guru Agama Islam'=>'Guru Agama Islam','Guru Penjasorkes'=>'Guru Penjasorkes','Guru PPKN'=>'Guru PPKN','Guru Bahasa Indonesia'=>'Guru Bahasa Indonesia','Guru Bahasa Inggris'=>'Guru Bahasa Inggris','Guru Matematika'=>'Guru Matematika','Guru IPA'=>'Guru IPA','Guru IPS'=>'Guru IPS','Guru Seni Budaya'=>'Guru Seni Budaya','Guru TIK'=>'Guru TIK','Guru Muatan Lokal'=>'Guru Muatan Lokal','Guru Prakarya dan Kewirausahaan'=>'Guru Prakarya dan Kewirausahaan','Guru Bimbingan dan Konseling'=>'Guru Bimbingan dan Konseling'] ,'empty'=>'-Pilih-' ,'value'=>$pegawai->jenis_guru])
</tr>

@endif
@endif

<tr>
    <td>TMT Jabatan</td>
    <td>:</td>
    <td>
        <input id="tt7" type="text" name="peg_jabatan_tmt" value="{{editDate($pegawai->peg_jabatan_tmt)}}" size="12" maxlength="10" readonly="">
        &nbsp; <font color="red">format : dd-mm-yyyy</font>
        <input type="hidden" name="no_sk_jabatan" id="no_sk_jabatan">
        <input type="hidden" name="tanggal_sk_jabatan" id="tanggal_sk_jabatan">
        <input type="hidden" name="jabatan_penandatangan_jab" id="jabatan_penandatangan_jab">
        <input type="hidden" name="jabatan_is_calon_jft" id="jabatan_is_calon_jft">
        <input type="hidden" name="jabatan_calon_jft_id" id="jabatan_calon_jft_id">
    </td>
    <td>
    </td>

</tr>
<tr>
   @include('form.txt', ['label'=>'Instansi (jika dipekerjakan)', 'required'=>false, 'name'=>'peg_instansi_dpk', 'value'=>$pegawai->peg_instansi_dpk, 'maxlength'=>'200'])
</tr>
<tr>
   @include('form.select2', ['label'=>'Status Gaji', 'required'=>false, 'name'=>'peg_status_gaji','data'=>['1'=>'Pemkot', '2'=>'Luar Pemkot'] ,'empty'=>'-Pilih-' ,'value'=>$pegawai->peg_status_gaji])
</tr>
@if(Auth::user()->role_id != 2 && Auth::user()->role_id != 8)
<tr>
    @include('form.select2', ['label'=>'Kedudukan Pegawai', 'required'=>false, 'name'=>'id_status_kepegawaian',
   'data'=>App\Model\StatusKepegawaian::lists('status','id_status_kepegawaian') ,'empty'=>'-Pilih-' ,'value'=>$pegawai->id_status_kepegawaian])
</tr>
@else
<tr>
    <td>Kedudukan Pegawai</td>
    <td>:</td>
    <td>
        <?php
        $status_kepegawaian = App\Model\StatusKepegawaian::where('id_status_kepegawaian',$pegawai->id_status_kepegawaian)->first();
        ?>
        {{$status_kepegawaian ? $status_kepegawaian->status : ''}}
    <input type="hidden" name="id_status_kepegawaian" id="id_status_kepegawaian" value="{{$pegawai->id_status_kepegawaian}}">
    </td>
</tr>
@endif
<tr>
   @include('form.select2',['label'=>'Golongan Awal','required'=>true,'name'=>'gol_id_awal','data'=>
                App\Model\Golongan::orderBy('gol_id')->lists('nm_gol','gol_id')
            ,'empty'=>'-Pilih-', 'value'=>$pegawai->gol_id_awal])
<td>TMT Golongan Awal</td>
<td>:</td>
<td>
    <input id="tt4" type="text" name="peg_gol_awal_tmt" value="{{editDate($pegawai->peg_gol_awal_tmt)}}" size="12" maxlength="10">
</td>
</tr>
<tr id='cpns2'>
    <?php
        $gol = App\Model\Golongan::where('gol_id',$pegawai->gol_id_akhir)->first();
    ?>
  @include('form.txt',['label'=>'Golongan Akhir','required'=>true,'name'=>'gol_akhir'
            ,'empty'=>'-Pilih-', 'value'=>$gol ? $gol->nm_gol : '','readonly' => true,'maxlength'=>'10'])
    <td>TMT Golongan Akhir</td>
    <td>:</td>
    <td>
        <input type="hidden" name="gol_id_akhir" id="gol_id_akhir" value="{{$pegawai->gol_id_akhir}}">
        <input id="tt5" type="text" name="peg_gol_akhir_tmt" readonly="" value="{{editDate($pegawai->peg_gol_akhir_tmt)}}" size="12" maxlength="10">
    </td>
</tr>
<tr>
    <td>Masa Kerja Golongan</td>
    <td>:</td>
    <td>
        <input type="text" id="txt_peg_kerja_tahun" class="quantity" readonly="" name="peg_kerja_tahun" value="{{$pegawai->peg_kerja_tahun}}" size='2' maxlength='2' onkeydown="return jsOnlyNumber(event); " onfocus="return get_gapok()" autocomplete="off" style="text-align:center" /> Tahun
        <input type="text" id="txt_peg_kerja_bulan" name="peg_kerja_bulan" class="quantity" readonly="" value="{{$pegawai->peg_kerja_bulan}}" size='2' maxlength='2' onkeydown="return jsOnlyNumber(event)" autocomplete="off" style="text-align:center" /> Bulan
    </td>
    <td colspan="6">
        @if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,2,5] : [1]))
        <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-pangkat" onclick="addPangkat()">Update Golongan Akhir</button>
        @else
        <button type="button" class="btn btn-info btn-xs" disabled>Update Golongan (Fitur Disabled)</button>
        @endif
        <input type="hidden" name="no_sk" id="no_sk">
        <input type="hidden" name="tanggal_sk" id="tanggal_sk">
        <input type="hidden" name="jabatan_penandatangan" id="jabatan_penandatangan">
        <input type="hidden" name="unit_kerja_gol" id="unit_kerja_gol">
    </td>
</tr>
<tr id="karsuis">
    @include('form.txt', ['label'=>'No. KARPEG', 'required'=>false, 'name'=>'peg_karpeg', 'value'=>$pegawai->peg_karpeg, 'maxlength'=>'50'])
    @include('form.txt', ['label'=>'No. Karis/Karsu', 'required'=>false, 'name'=>'peg_karsutri', 'value'=>$pegawai->peg_karsutri, 'maxlength'=>'50'])
</tr>
<tr>
    @include('form.txt',['label'=>'No. Askes','required'=>false,'name'=>'peg_no_askes', 'value'=>$pegawai->peg_no_askes,'maxlength'=>'50'])
</tr>
<tr>
    @include('form.txt',['label'=>'No. KTP','required'=>false,'name'=>'peg_ktp', 'value'=>$pegawai->peg_ktp, 'maxlength'=>'50'])
   @include('form.txt',['label'=>'NPWP','required'=>false,'name'=>'peg_npwp', 'value'=>$pegawai->peg_npwp, 'maxlength'=>'50'])
</tr>
<tr>
    @include('form.select2',['label'=>'Golongan Darah','required'=>false,'name'=>'id_goldar','data'=>
    App\Model\GolonganDarah::orderBy('nm_goldar')->lists('nm_goldar','id_goldar') ,'empty'=>'-Pilih-', 'value'=>$pegawai->id_goldar])
</tr>
<tr>
     @include('form.select2',['label'=>'BAPETARUM','required'=>false,'name'=>'peg_bapertarum','data'=>
    [1 => 'Sudah Diambil',2 => 'Belum Diambil' ],'empty'=>'-Pilih-', 'value'=>$pegawai->peg_bapertarum])
</tr>
<tr>
    <td>TMT Gaji Berkala Terakhir</td>
    <td>:</td>
    <td colspan="7">
        <input id="tt6" type="text" name="peg_tmt_kgb"  value="{{editDate($pegawai->peg_tmt_kgb)}}" size="12" maxlength="10">
    </td>
</tr>
<tr>
    <td>Alamat Rumah</td>
    <td>:</td>
    <td colspan="7">
        <input type="text" name="peg_rumah_alamat" value="{{$pegawai->peg_rumah_alamat}}" maxlength="255" size="136" autocomplete="off">
    </td>
</tr>
<tr>
    <td colspan="2"></td>
    <td colspan="7">
        <table width="100%"  border="0" bordercolor="red" cellspacing='0'>
            <tr>
                @include('form.txt',['label'=>'Kel./Desa', 'required'=>false, 'name'=>'peg_kel_desa', 'value'=>$pegawai->peg_kel_desa, 'maxlength'=>'100'] )
                @include('form.select2',['label'=>'Kec.', 'required'=>false, 'name'=>'kecamatan_id','data'=>
    App\Model\Kecamatan::orderBy('kecamatan_id')->lists('kecamatan_nm','kecamatan_id') ,'empty'=>'-Pilih-', 'value'=>$pegawai->kecamatan_id])
            </tr>
            <tr>
                <td>Kab./Kota</td>
                <td>:</td>
                <td>
                    @if($kab != null)
                    <input type="text" name="kabupaten_nm" id="kabupaten_nm" value="{{$kab->kabupaten_nm}}" readonly size="39"/>
                    &nbsp; <font color="red">(Pilih Kecamatan Terlebih Dahulu!)</font>
                    <input type="hidden" name="kabupaten_id" id="kabupaten_id" value="{{$kab->kabupaten_id}}">
                    @else
                    <input type="text" name="kabupaten_nm" id="kabupaten_nm" value="" readonly size="39"/>
                    &nbsp; <font color="red">(Pilih Kecamatan Terlebih Dahulu!)</font>
                    <input type="hidden" name="kabupaten_id" id="kabupaten_id" value="">
                    @endif
                </td>
                @include('form.txt',['label'=>'Kode Pos', 'required'=>false, 'name'=>'peg_kodepos', 'value'=>$pegawai->peg_kodepos, 'maxlength'=>'50'] )
            </tr>
            <tr>
                @include('form.txt',['label'=>'Telp', 'required'=>false, 'name'=>'peg_telp', 'value'=>$pegawai->peg_telp, 'maxlength'=>'20'] )
                @include('form.txt',['label'=>'HP', 'required'=>false, 'name'=>'peg_telp_hp', 'value'=>$pegawai->peg_telp_hp, 'maxlength'=>'20'] )
            </tr>
            <tr>
                @include('form.txt',['label'=>'Email', 'required'=>false, 'name'=>'peg_email', 'value'=>$pegawai->peg_email, 'maxlength'=>'50'] )
            </tr>
        </table>
    </td>
</td>
<tr>
    <td colspan="9" align="center">
        <input type="submit" value="Simpan" name="simpan" id="simpan_data" class="btn btn-xs">

        <a href="{{url('/pegawai/profile/edit',$pegawai->peg_id)}}" class="btn btn-xs">Batal</a>
    </td>
</tr>
</table>
</div>
</form>

<div class="modal fade" id="modal-jabatan-fu" tabindex="-1" role="dialog" aria-labelledby="#modal-jabatan-fu" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Tambah Jabatan Fungsional Umum</h4>
        </div>

        <div class="modal-body" id="modal-detail-content">
            @include('form.select2',['label'=>'Nama Jabatan', 'required'=>true, 'name'=>'jfu_id','data'=>
    App\Model\JabatanFungsionalUmum::orderBy('jfu_nama')->lists('jfu_nama','jfu_id') ,'empty'=>'-Pilih-'])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitNewJabatanFu" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </div>
    </div>
</div>
<div class="modal fade" id="modal-pangkat" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelPangkat"><div id="modal-button-edit"></div></h4>
        </div>
        <form id="pangkatForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <div class="bs-callout bs-callout-warning hidden" id="gol-error">
              <h4>Edit Data Gagal!</h4>
              <p>Data Harus Lengkap!</p>
            </div>
            @include('form.select2_modal',['label'=>'Gol. Ruang','required'=>true,'name'=>'gol_ruang_id','data'=>
            App\Model\Golongan::orderBy('nm_gol')->lists('nm_gol','gol_id'),'empty'=>'-Pilih-'])
            <input type="hidden" id="gol_ruang_nama">
            <br>
            @include('form.text2',['label'=>'Masa Kerja Tahun','required'=>false,'name'=>'riw_pangkat_thn','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Masa Kerja Bulan','required'=>false,'name'=>'riw_pangkat_bln','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Nomor SK','required'=>false,'name'=>'riw_pangkat_sk','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_pangkat_sktgl','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'riw_pangkat_pejabat','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'TMT','required'=>false,'name'=>'riw_pangkat_tmt','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Unit Kerja','required'=>false,'name'=>'riw_pangkat_unit_kerja','placeholder'=>''])
        </div>
        <div class="modal-footer">
            <input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Batal</button>
            <button type="button" id="submitPenghargaan" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>
<div class="modal fade" id="modal-jabatan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelPangkat"><div id="modal-button-edit">Update Jabatan</div></h4>
        </div>
        <form id="pangkatForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <div class="bs-callout bs-callout-warning hidden">
              <h4>Edit Data Gagal!</h4>
              <p>Data Harus Lengkap!</p>
            </div>
            @include('form.select2_modal',['label'=>'SOPD','required'=>true,'name'=>'riw_jabatan_satker','data'=>
                App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>'','value'=>$pegawai->satuan_kerja_id])
            <input type="hidden" id="gol_ruang_nama">
            <br>
            <div class="row" id="riw_jabatan_uker_container">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Unit Kerja <span class="required" aria-required="true">*</span></label>
                    <div class="col-md-8">
                        <select id="riw_jabatan_uker" name="riw_jabatan_uker" style="width:300px">
                        <?php
                            $sat_ker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('status',1)->lists('satuan_kerja_nama', 'satuan_kerja_id');
                        ?>
                         <option value="{{$pegawai->unit_kerja_id}}" selected="selected"></option>
                        @foreach($sat_ker as $id_st => $nama_st)
                             <option value="">{{$nama_st}}</option>
                            <?php
                                $uk = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 1)->where('status',1)->lists('unit_kerja_nama', 'unit_kerja_id');
                            ?>
                            @foreach ($uk as $id_unit => $nama_unit)
                                 <option value="{{$id_unit}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit}}</option>
                                 <?php
                                    $uk2 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 2)->where('status',1)->where('unit_kerja_parent', $id_unit)->lists('unit_kerja_nama', 'unit_kerja_id');
                                 ?>
                                @foreach ($uk2 as $id_unit2 => $nama_unit2)
                                    <option value="{{$id_unit2}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit2}}</option>

                                    <?php
                                        $uk3 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 3)->where('status',1)->where('unit_kerja_parent', $id_unit2)->lists('unit_kerja_nama', 'unit_kerja_id');
                                    ?>
                                    @foreach ($uk3 as $id_unit3 => $nama_unit3)
                                        <option value="{{$id_unit3}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit3}}</option>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            </div>
            <br>
            @include('form.select2_modal',['label'=>'Jenis Jabatan','required'=>true,'name'=>'riw_jabatan_jenjab','data'=>
            ['2' => 'Struktural','3' => 'Fungsional Tertentu', '4'=>'Fungsional Umum' ],'empty'=>'-Pilih-', 'value'=>$jabatan_jenis])
            <br>
            @if($jabatan_jenis == 2)
                @include('form.select2_modal',['label'=>'Nama Jabatan','required'=>true,'name'=>'riw_jabatan_jab','data'=>App\Model\Jabatan::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('unit_kerja_id', $pegawai->unit_kerja_id)->where('jabatan_jenis', 2)->lists('jabatan_nama','jabatan_id')
                         ,'empty'=>'-Pilih-', 'value'=>$pegawai->jabatan_id])
            @elseif($jabatan_jenis == 3)
                <?php
                    $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jf_id')->get(['jf_id']);
                    $id = [];
                    foreach ($jab as $j) {
                        $id[] = $j->jf_id;
                    }
                ?>
                @include('form.select2_modal',['label'=>'Nama Jabatan','required'=>true,'name'=>'riw_jabatan_jab','data'=>App\Model\JabatanFungsional::whereIn('jf_id',$id)->lists('jf_nama','jf_id')
                         ,'empty'=>'-Pilih-', 'value'=>$jf])
            @elseif($jabatan_jenis == 4)
            <?php
                $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jfu_id')->get(['jfu_id']);
                $id = [];
                foreach ($jab as $j) {
                    $id[] = $j->jfu_id;
                }
            ?>
                @include('form.select2_modal',['label'=>'Nama Jabatan','required'=>true,'name'=>'riw_jabatan_jab','data'=>App\Model\JabatanFungsionalUmum::whereIn('jfu_id',$jab)->lists('jfu_nama','jfu_id')
                         ,'empty'=>'-Pilih-', 'value'=>$jfu])
            @else
                 @include('form.select2_modal',['label'=>'Nama Jabatan','required'=>true,'name'=>'riw_jabatan_jab','data'=>App\Model\Jabatan::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('unit_kerja_id', $pegawai->unit_kerja_id)->lists('jabatan_nama','jabatan_id')
                         ,'empty'=>'-Pilih-', 'value'=>$pegawai->jabatan_id])
            @endif
            <br>
            <div id="calon_jft" style="{{$jabatan_jenis != 4 ? 'display:none' : ''}}" >
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Calon JFT</label>
                        <div class="col-md-8">
                            <div class="radio-list">
                                <input type="checkbox" name="is_calon" id="is_calon" value="1" {{$calonjft ? ($calonjft->is_calon ? 'checked=""' : '') : ''}}>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div id="jft_calon" style="{{$calonjft ? ($calonjft->is_calon ? '' : 'display:none') : 'display:none'}}">
            @include('form.select2_modal',['label'=>'Jabatan Fungsional','required'=>true,'name'=>'jft_calon_id','data'=>App\Model\JabatanFungsional::lists('jf_nama','jf_id')
                     ,'empty'=>'-Pilih-','value' => $calonjft ? $calonjft->jf_id : ''])
            </div>
            </div>
            <br>
            @include('form.text2',['label'=>'TMT Jabatan','required'=>false,'name'=>'riw_jabatan_tmt','placeholder'=>'','value'=>editDate($pegawai->peg_jabatan_tmt)])
            <br>
            @include('form.text2',['label'=>'No. SK','required'=>false,'name'=>'riw_jabatan_nosk','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Tanggal SK','required'=>false,'name'=>'riw_jabatan_tglsk','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Jabatan Penandatangan SK','required'=>false,'name'=>'riw_jabatan_jabttd','placeholder'=>''])
        </div>
        <div class="modal-footer">
            <input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
            <button type="button" id="submit_jabatan" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
var addPangkat = function(){
    $("#labelPangkat").text("Update Golongan Ruang Akhir");
    $("#gol_ruang_id").val("").select2();
    $(".riw_pangkat_thn").val("");
    $(".riw_pangkat_bln").val("");
    $(".riw_pangkat_gapok").val("");
    $(".riw_pangkat_sktgl").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
    $(".riw_pangkat_tmt").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
    $(".riw_pangkat_sk").val("");
    $(".riw_pangkat_pejabat").val("");
    $(".riw_pangkat_unit_kerja").val("{{$sopd_nama ? $sopd_nama->satuan_kerja_nama : ''}}");
    $('#gol-error').addClass("hidden");

    $(".riw_pangkat_thn").on("keydown", function (e) {
        numberOnly(e);
    });
    $(".riw_pangkat_bln").on("keydown", function (e) {
        numberOnly(e);
    });
}
$("#gol_ruang_id").change(function(){
    $('#gol-error').addClass("hidden");
});
$("#riw_jabatan_tglsk").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#riw_jabatan_tmt").val("{{editDate($pegawai->peg_jabatan_tmt)}}").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#submitPenghargaan").click(function(){
    var gol = $("#gol_ruang_id").val();
    if(gol == ''){
        $('#gol-error').removeClass("hidden");
        return 0;
    }
    $("#gol_akhir").val($("#gol_ruang_id option[value='"+gol+"']").text());
    $("#gol_id_akhir").val($("#gol_ruang_id").val());
    $("#tt5").val($("#riw_pangkat_tmt").val());
    $("#txt_peg_kerja_tahun").val($("#riw_pangkat_thn").val());
    $("#txt_peg_kerja_bulan").val($("#riw_pangkat_bln").val());
    $("#no_sk").val($("#riw_pangkat_sk").val());
    $("#tanggal_sk").val($("#riw_pangkat_sktgl").val());
    $("#jabatan_penandatangan").val($("#riw_pangkat_pejabat").val());
    $("#unit_kerja_gol").val($("#riw_pangkat_unit_kerja").val());
    $('#modal-pangkat').modal('hide');
});


$("#submit_jabatan").click(function(){
    var satker = $("#riw_jabatan_satker").val();
    var uker = $("#riw_jabatan_uker").val();
    var jenjab = $("#riw_jabatan_jenjab").val();
    var jab = $("#riw_jabatan_jab").val();
    $("#satuan_kerja").val($("#riw_jabatan_satker option[value='"+satker+"']").text());
    $("#satuan_kerja_id").val(satker);
    $("#unit_kerja").val($("#riw_jabatan_uker option[value='"+uker+"']").text());
    $("#unit_kerja_id").val(uker);
    $("#jenjab").val($("#riw_jabatan_jenjab option[value='"+jenjab+"']").text());
    $("#jenis_jabatan").val(jenjab);
    $("#jabatan_nama").val($("#riw_jabatan_jab option[value='"+jab+"']").text());
    $("#jabatan_id").val(jab);
    $("#no_sk_jabatan").val($("#riw_jabatan_nosk").val());
    $("#tanggal_sk_jabatan").val($("#riw_jabatan_tglsk").val());
    $("#jabatan_penandatangan_jab").val($("#riw_jabatan_jabttd").val());
    $("#jabatan_calon_jft_id").val($("#jft_calon_id").val());
    $("#jabatan_is_calon_jft").val($("#is_calon").is(':checked'));
    $("#tt7").val($("#riw_jabatan_tmt").val());

    $('#modal-jabatan').modal('hide');

});

jQuery(function($){
     $("#tt1").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt2").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt3").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt4").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt5").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt6").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt7").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});

     //var jab = $("#jabatan_id").val();
    // getJabatanAwal();
    // $("#jabatan_id").select2("val", jab);
});
$('#jft_calon_id').select2({width:'500px'});
$('#form-edit').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })

// $('#unit_kerja_id').select2({ width: '500px' }).select2('data', {id: "{{$pegawai->unit_kerja_id}}", text: "{{$nama_unit_kerja}}"}).on('change', function (e){
//     var unit_kerja_id = $('#unit_kerja_id').val();
//     getJabatan();
// });

$('#riw_jabatan_uker').select2({ width: '500px' }).select2('data', {id: "{{$pegawai->unit_kerja_id}}", text: "{{$nama_unit_kerja}}"}).on('change', function (e){
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    getJabatanRiw();
});

// $('#satuan_kerja_id').select2({ width: '400px' }).on('change', function (e){
//     var v = $('#satuan_kerja_id').val();
//     $.ajax({
//         type: "GET",
//         cache: false,
//         url: "{{ URL::to('pegawai/getUnitKerja') }}",
//         beforeSend: function (xhr) {
//             var token = $('meta[name="csrf_token"]').attr('content');

//             if (token) {
//                   return xhr.setRequestHeader('X-CSRF-TOKEN', token);
//             }
//         },
//         data: {"satuan_kerja_id":v},
//         dataType: 'json',
//         success: function(data) {
//             $("#unit_kerja_id").empty();
//             $("#unit_kerja_id").select2("val", "");
//             $('#unit_kerja_id').append($('<option></option>').attr('value', '').text(''));
//             for (row in data) {
//                 $('#unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
//             }
//             getJabatan();
//         }
//     }).error(function (e){
//         console.log(e);
//         alert("ERROR!");
//     });
// });

$('#riw_jabatan_satker').select2({ width: '400px' }).on('change', function (e){
    var v = $('#riw_jabatan_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#riw_jabatan_uker").empty();
            $("#riw_jabatan_uker").select2("val", "");
            $('#riw_jabatan_uker').append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#riw_jabatan_uker').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            getJabatanRiw();
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
});

$('#id_pend_awal').select2().on('change', function (e) {
    var v = $('#id_pend_awal').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/tingkat-pendidikan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kat_pend_id":v},
        dataType: 'json',
        success: function(response) {
            $('#_tingpend_awal').val(response);
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });

});

$('#id_pend_akhir').select2().on('change', function (e) {
    var v = $('#id_pend_akhir').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/tingkat-pendidikan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kat_pend_id":v},
        dataType: 'json',
        success: function(response) {
            $('#_tingpend_akhir').val(response);
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });

});

// $('#jabatan_id').select2({ width: '370px' });

// $('#jenis_jabatan').select2({ width: '370px' }).on('change', function (e){
//    $("#jabatan_id").empty();
//    $("#jabatan_id").select2("val", "");
//    getJabatan();
// });

$('#riw_jabatan_jab').select2({ width: '370px' });

$('#riw_jabatan_jenjab').select2({ width: '370px' }).on('change', function (e){
   $("#jabatan_id").empty();
   $("#jabatan_id").select2("val", "");
   var val = $(this).val();
   if(val == 4){
        $('#calon_jft').show();
        $('#is_calon').prop('checked',true);
   }else{
        $('#calon_jft').hide();
        $('#is_calon').prop('checked',false);
   }
   getJabatanRiw();
});

function getJabatan(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            //$("#jabatan_id").select2("val", "");
                var unit_kerja_id = $('#unit_kerja_id').val();
                var satuan_kerja_id = $('#satuan_kerja_id').val();
                var v = $('#jenis_jabatan').val();
                //console.log(v);
                if(v == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            //$("#jabatan_id").empty();
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v== 4){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            //$("#jabatan_id").empty();
                            for (row in data) {
                                //console.log(data);
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v==2){
                    //$("#jabatan_id").empty();
                    for (row in data) {
                        $('#jabatan_id').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
                    }
                }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

$('#riw_jabatan_uker').on('change',function(){
    $('#riw_jabatan_jab').empty();
    $('#riw_jabatan_jab').val('').trigger('change');
    $('#riw_jabatan_jenjab').val('').trigger('change');
});

$('#riw_jabatan_jenjab').on('change',function(){
    $('#riw_jabatan_jab').empty();
    $('#riw_jabatan_jab').val('').trigger('change');
});

function getJabatanRiw(){
    var v = $('#riw_jabatan_jenjab').val();
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    var satuan_kerja_id = $('#riw_jabatan_satker').val();
    
    $("#riw_jabatan_jab").select2("val", "");
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            //$("#jabatan_id").select2("val", "");
                var unit_kerja_id = $('#riw_jabatan_uker').val();
                var satuan_kerja_id = $('#riw_jabatan_satker').val();
                var v = $('#riw_jabatan_jenjab').val();
                //console.log(v);
                if(v == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#riw_jabatan_jab").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                            $('#riw_jabatan_jab').val('').trigger('change');
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v== 4){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#riw_jabatan_jab").empty();
                            //$("#jabatan_id").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));

                            $('#riw_jabatan_jab').val('').trigger('change');
                            for (row in data) {
                                //console.log(data);
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v==2){
                    //$("#jabatan_id").empty();
                            $("#riw_jabatan_jab").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));

                            $('#riw_jabatan_jab').val('').trigger('change');
                    for (row in data) {
                        $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
                    }
                }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

function getJabatanAwal(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            $("#jabatan_id").empty();
            for (row in data) {
                var jab = data[row].jabatan_id;
                if(data[row].jabatan_jenis == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(data[row].jabatan_jenis == 4){
                    $('#tampilJenisButton').show();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jfu_nama));
                            }

                            console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else{
                    $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jabatan_nama));
                }
            }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

function getJabatanAwalRiw(){
    var v = $('#riw_jabatan_jenjab').val();
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    var satuan_kerja_id = $('#riw_jabatan_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            $("#riw_jabatan_jab").empty();
            for (row in data) {
                var jab = data[row].jabatan_id;
                if(data[row].jabatan_jenis == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(data[row].jabatan_jenis == 4){
                    $('#tampilJenisButton').show();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jfu_nama));
                            }

                            console.log($('#riw_jabatan_jab').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else{
                    $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jabatan_nama));
                }
            }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

$('#kecamatan_id').select2({ width: '250px' }).on('change', function (e) {
    var v = $('#kecamatan_id').val();
    if(v != ''){
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/kabupaten') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"kecamatan_id":v},
        dataType: 'json',
        success: function(data) {
            $('#kabupaten_nm').val(data.nama);
            $('#kabupaten_id').val(data.id);
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
    }else{
        $('#kabupaten_nm').val('');
        $('#kabupaten_id').val('');
    }
});

$('#peg_status_kepegawaian2').on('change', function (e) {
     $('#tmt_pns').show();
});

$('#peg_status_kepegawaian1').on('change', function (e) {
     $('#tmt_pns').hide();
});

$(".quantity").on("keydown", function (e) {
var key   = e.keyCode ? e.keyCode : e.which;

if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
     (key >= 35 && key <= 40) ||
     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
     (key >= 96 && key <= 105)
   )) e.preventDefault();
});


$('#peg_foto').bind('change', function() {
    var size = this.files[0].size;
    var max = 2097152;
    if(size>max){
        alert('Ukuran foto harus lebih kecil dari 2 MB');
        $('#peg_foto').val('');
    }
});

var vm = new Vue({
  el: '#form-edit',
  data: {
    pegnip:'',
  },
  methods: {
    doSearch: function(){
        var id = "{{$pegawai->peg_id}}";
        $.getJSON("{{url('pegawai/edit-cek')}}", {peg_nip: this.pegnip,peg_id:id}, function(data) {
          if(data.pegawai != 0){
            var url = "{{url('/pegawai/profile/edit/')}}/"+data.pegawai.peg_id;
            var html = '';
            html += '<div class="alert alert-danger">Nip Sudah Ada, '+data.pegawai.peg_nama;
            @if(Auth::user()->role_id == 1)
            html += '<a href="'+url+'" class="btn btn-danger btn-xs">Lihat Pegawai</a>';
            @endif
            html += '</div>';

            $('.error').html(html);    
            $('#simpan_data').prop('disabled',true);
          }else{
            $('#simpan_data').prop('disabled',false);
            $('.error').html("");
          }

          if(!data.valid){
            $('.valid').html('<div class="alert alert-danger">NIP Tidak Valid</div>');
            $('#simpan_data').prop('disabled',true);
          }else{
            $('.valid').html("");
            $('#simpan_data').prop('disabled',false);
          }
        });
    }
  }
});

$('#is_calon').on('change',function(){
    var val = $(this).is(':checked');
    if(val){
        $('#jft_calon').show();
        $('#jft_calon_id').val('').trigger('change');
    }else{
        $('#jft_calon').hide();
        $('#jft_calon_id').val('').trigger('change');
    }
});
</script>
@endsection