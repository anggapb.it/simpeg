@extends('layouts.app')
<?php
    use App\Model\SatuanKerja;
    use App\Model\Role;
?>
@section('content')
<div class="row">
	<center><h3>Edit Profile</h2></center>
	<br>
	<hr>
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	@if (session('message'))
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
	@endif
	<form class="form-horizontal" role="form" method="POST" action="{{ url('user/update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<div class="col-md-9">
				<?php /*
			@include('form.text2',['label'=>'Nama','required'=>false,'name'=>'nama','placeholder'=>'', 'value'=>$user->nama])
	        <br>
	        
	       	@if(Auth::user()->role_id == 4)
	       		@include('form.text2',['label'=>'Username','required'=>false,'name'=>'username','placeholder'=>'', 'value'=>$user->username, 'readonly'=>TRUE])
	       	@else
	        	@include('form.text2',['label'=>'Username','required'=>false,'name'=>'username','placeholder'=>'', 'value'=>$user->username])
	        @endif
	        */ ?>
	        <br>
	        @include('form.password',['label'=>'Password Lama','required'=>false,'name'=>'password_lama','placeholder'=>''])
	        <br>
	        @include('form.password',['label'=>'Password Baru','required'=>false,'name'=>'password_baru','placeholder'=>''])
	        <br>
	        <?php /*
	        @include('form.select2_modal',['label'=>'Role','required'=>false,'name'=>'role_id','data'=>
	        Role::lists('role_name','id') ,'empty'=>'-Pilih-', 'value'=>$user->role_id])
	        <br>
	    	@include('form.select2_modal',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
	        SatuanKerja::orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id') ,'empty'=>'-Pilih-', 'value'=>$user->satuan_kerja_id])
	        */ ?>
		</div>
	    </div>
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
					Save
				</button>
			</div>
		</div>
	</form>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$("#role_id").select2().attr("disabled", false);
	$("#satuan_kerja_id").select2().attr("disabled", false);
</script>
@endsection
