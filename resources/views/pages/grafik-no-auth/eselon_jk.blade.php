@extends('layouts.app-no-auth')

@section('content')
<div class="row">
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/jabatan')}}" class="btn btn-primary">Jabatan</a>
    </div>
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/golongan')}}" class="btn btn-primary">Golongan</a>
    </div>
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/jenis-kelamin')}}" class="btn btn-primary">Jenis Kelamin</a>
    </div>
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/eselon-jk')}}" class="btn btn-primary" disabled>Eselon Jenis Kelamin</a>
    </div>
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/pendidikan')}}" class="btn btn-primary">Tingkat Pendidikan</a>
    </div>
    <div class="col-md-2">
        <a href="{{url('grafik-no-auth/usia')}}" class="btn btn-primary">Kelompok Usia</a>
    </div>
</div><br><br>
<div class="form-group ">
    <div class="col-lg-4"></div>
  <label for="cname" class="control-label col-lg-1">Satuan Kerja</label>
  <div class="col-lg-7">
    <select name="psearch" id="satuan_kerja_id">
          <option value="">Please Select</option>
          @foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
          @endforeach
    </select> 
  </div>
</div>
<br><br>
<br><br>
<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

@endsection
@section('styles')
<style>
</style>
@endsection
@section('scripts')
<script src="{{asset('js/highcharts.js')}}"></script>
<script src="{{asset('js/exporting.js')}}"></script>
<script type="text/javascript">
function fillData(data){
chart = new Highcharts.Chart({
    chart: {
            renderTo:'container',
            type: 'column'
        },
        title: {
            text: 'Grafik Jenis Kelamin Per Eselon'
        },
        xAxis: {
            categories: [
                'II.A',
                'II.B',
                'III.A',
                'III.B',
                'IV.A',
                'IV.B',
                'V.A'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Pegawai'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} pegawai</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
            dataLabels: {
                    align: 'center',
                    enabled: true
                }
            }
        },
        series: data
    });
}
$('#satuan_kerja_id').select2({ width: '300px' }).on('change', function (e){
    var v = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : 0;
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('grafik-no-auth/get-data-eselon-jk/')}}/"+v,
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        dataType: 'json',
        success: function(out) {
            fillData(out.d);
        }
    });
});
$('#satuan_kerja_id').change();

</script>
@endsection