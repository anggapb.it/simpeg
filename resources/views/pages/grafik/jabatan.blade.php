@extends('layouts.app')

@section('content')
<center><h4>GRAFIK PEGAWAI NEGERI SIPIL DAERAH</h4>
        <h4>DI LINGKUNGAN PEMERINTAH KOTA BANDUNG</h4>
        <h4>TAHUN <?php echo (date('Y'));?></h4><br>
</center>
<hr><br>
<div class="form-group ">
  <label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
  <div class="col-lg-9">
    <select name="psearch" id="satuan_kerja_id">
          <option value="">Please Select</option>
          @foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
          @endforeach
    </select> 
  </div>
</div>
<br><br>
<br><br>
<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

@endsection
@section('styles')
<style>
</style>
@endsection
@section('scripts')
<script src="{{asset('js/highcharts.js')}}"></script>
<script src="{{asset('js/exporting.js')}}"></script>
<script type="text/javascript">
var options = {
    chart: {
        renderTo: 'container',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    },
    title: {
        text: 'Grafik Jabatan'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
            showInLegend: true
        }
    },
    series: [{
        type: 'pie',
        name: 'Grafik Jabatan',
        data: []
    }]
}
$('#satuan_kerja_id').select2({ width: '300px' }).on('change', function (e){
    // var v = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : '{{Auth::user()->satuan_kerja_id}}';
    var v = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : 0;
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('grafik/get-data-jabatan/')}}/"+v,
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        dataType: 'json',
        success: function(data) {
            options.series[0].data = data.data;
            chart = new Highcharts.Chart(options);
        }
    });
});
$('#satuan_kerja_id').change();

</script>
@endsection