@extends('layouts.app')
<?php
    use App\Model\SatuanKerja;
?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Import Data Jabatan Fungsional Pegawai</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {!! session('message') !!}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <form style="margin: 0 0 10px 0;" action="{{url('import/pegawai/jfu')}}" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <?php /*
    @include('form.select2_modal',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
    SatuanKerja::orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id') ,'empty'=>'-Pilih-'])
    */ ?>
    Masukkan file: 
    <input type="file" name="excelfile" id="excelfile">
    <input type="submit" value="Import" name="submit" class="btn btn-sm btn-primary">
  </form>
</div>
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    $('#posts-table').DataTable({
    });
});
</script>
@endsection
