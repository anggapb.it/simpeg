@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
	<h3>Tambah Jabatan</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('data/jabatan/store') }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <table class="table no-footer">
                  <tr>
                    <td>Unit Kerja</td>
                    <td>:</td>
                    <td>
                      @if($uker == null)
                        {{$satker->satuan_kerja_nama}}
                        <?php
                        $unit = "null";
                        ?>
                      @else
                        {{$uker->unit_kerja_nama}}
                        <?php
                        $unit = $uker->unit_kerja_id;
                        ?>
                      @endif
                    </td>
                  </tr>
                  @include('form.hidden',['name'=>'satker','value'=>$satker->satuan_kerja_id])
                  @include('form.hidden',['name'=>'uker','value'=>$unit])
                  <tr>
                      <td>Jenis Jabatan </td>
                      <td>:</td>
                      <td>
                          <select id="jenjab" name="jenjab" style="max-width:500px;">
                              <option value="">-Pilih-</option>
                              @if($jenjab == 2)
                              <option value="2" selected="">Struktural</option>
                              <option value="3">Jabatan Fungsional Tertentu</option>
                              <option value="4">Jabatan Fungsional Umum</option>
                              @elseif($jenjab == 3)
                              <option value="2">Struktural</option>
                              <option value="3" selected="">Jabatan Fungsional Tertentu</option>
                              <option value="4">Jabatan Fungsional Umum</option>
                              @elseif($jenjab == 4)
                              <option value="2">Struktural</option>
                              <option value="3">Jabatan Fungsional Tertentu</option>
                              <option value="4" selected="">Jabatan Fungsional Umum</option>
                              @else
                              <option value="2">Struktural</option>
                              <option value="3">Jabatan Fungsional Tertentu</option>
                              <option value="4">Jabatan Fungsional Umum</option>
                              @endif
                          </select>

                      </td>
                  </tr>
                  @if($jenjab == "2")
                    <tr>
                      @include('form.txt',['label' => 'Nama Jabatan','required'=>true,'name'=>'namajab'])
                    </tr>
                    <tr>
                      @include('form.select2',['label'=>'Eselon','required'=>false,'name'=>'eselon','vue'=>'', 'data'=>App\Model\Eselon::lists('eselon_nm','eselon_id'),'empty'=>"Non Eselon"])
                    </tr>
                    <tr>
                      @include('form.number',['label' => 'Batas Usia Pensiun','required'=>true,'name'=>'bup','maxlength'=>3])
                    </tr>
                    <tr>
                      @include('form.txt',['label' => 'Kode Jabatan','required'=>false,'name'=>'kodejab'])
                    </tr>
                  @elseif($jenjab == "3")
                    <tr>
                      @include('form.select2',['label'=>'Nama Jabatan Fungsional Tertentu','required'=>true,'name'=>'namajf','vue'=>'', 'data'=>App\Model\JabatanFungsional::whereNotIn('jf_id',App\Model\Jabatan::where('jabatan_jenis',3)->where('satuan_kerja_id',$satuan_kerja_id)->where('unit_kerja_id',$unit_kerja_id == 'null' ? null : $unit_kerja_id)->get(['jf_id']))->lists('jf_nama','jf_id')])
                    </tr>
                  @elseif($jenjab == "4")
                    <tr>
                      @include('form.select2',['label'=>'Nama Jabatan Fungsional Umum','required'=>true,'name'=>'namajfu','vue'=>'', 'data'=>App\Model\JabatanFungsionalUmum::whereNotIn('jfu_id',App\Model\Jabatan::where('jabatan_jenis',4)->where('satuan_kerja_id',$satuan_kerja_id)->where('unit_kerja_id',$unit_kerja_id == 'null' ? null : $unit_kerja_id)->get(['jfu_id']))->lists('jfu_nama','jfu_id')])
                    </tr>
                  @endif
                </table>

                 <button class="btn btn-primary btn-xs" type="submit">Tambah</button>

                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  $('#jenjab').change(function() {
      var val = $("#jenjab option:selected").val();
      var satker = $("#satker").val();
      var uker = $("#uker").val();

      var url = "{{url('data/jabatan/tambah')}}?satuan_kerja_id="+satker+"&unit_kerja_id="+uker+"&jenjab="+val;
      window.location.href= url;
  });
  $('#eselon').select2({ width: '500px' });
  $('#namajfu').select2({ width: '500px' });
  $('#namajf').select2({ width: '500px' });
  </script>
@endsection