@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
  <h3>Edit Jabatan</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('data/jabatan/update/'.$model->jabatan_id) }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                <table class="table no-footer">
                  <tr>
                    <td>Unit Kerja</td>
                    <td>:</td>
                    <td>
                      @if(empty($uker))
                        {{$satker->satuan_kerja_nama}}
                      @else
                        {{$uker->unit_kerja_nama}}
                      @endif
                    </td>
                  </tr>
                    <tr>
                      @include('form.txt',['label' => 'Nama Jabatan','required'=>true,'name'=>'namajab','value' => $model->jabatan_nama])
                    </tr>
                    <tr>
                      @include('form.select2',['label'=>'Eselon','required'=>false,'name'=>'eselon_id','vue'=>'', 'data'=>App\Model\Eselon::lists('eselon_nm','eselon_id'),'value'=>$model->eselon_id,'empty'=>"Non Eselon"])
                    </tr>
                    <tr>
                      @include('form.number',['label' => 'Batas Usia Pensiun','required'=>true,'name'=>'bup','maxlength'=>3,'value' => $model->jabatan_bup])
                    </tr>
                    <tr>
                      @include('form.txt',['label' => 'Kode Jabatan','required'=>false,'name'=>'kodejab','value' => $model->kode_jabatan])
                    </tr>
                </table>
                </div>
                 <button class="btn btn-primary btn-xs" type="submit">Simpan</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  </script>
@endsection