@extends('layouts.app')

@section('content')
<h2 align="center">
  Daftar Jabatan @if($satker)<br>{{ $satker->satuan_kerja_nama }} @endif
</h2>
<center>
  @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
            App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
</center>
  @if ($satker)
<?php
  $uk_parent = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', '1')->orderBy('unit_kerja_left')->get();
  $uker= App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->select('unit_kerja_id')->orderBy('unit_kerja_left')->get();
  $unit = array();
  foreach ($uker as $key => $value) {
    $unit[] = $value->unit_kerja_id;
  }
  $peg = 0;
  if($jabatan){
    $peg = App\Model\Pegawai::where('jabatan_id',$jabatan->jabatan_id)->where('peg_status', 'TRUE')->count();
  }
?>
                <div class="table-responsive">
    <table id="list-unit" class="table table-striped table-bordered table-hover">
       <thead>
       <tr class="bg-info">
           <th>Unit Kerja</th>
           <th>Jabatan</th>
           <th>Jumlah Pegawai</th>
           <th>Pilihan</th>
       </tr>
       </thead>
       <tbody>
       <tr>
         <td>{{ $satker->satuan_kerja_nama }}</td>
         <td>{{$jabatan ? $jabatan->jabatan_nama : '-'}} ({{$jabatan? (empty($jabatan->kode_jabatan) ? '-' : $jabatan->kode_jabatan) : '-'}})</td>
         <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jabatan ? $jabatan->jabatan_id : 0}}">{{$peg}}</a></td>
         <td>
         @if($jabatan)
         <a href="{{url('data/jabatan/edit/'.($jabatan ? $jabatan->jabatan_id : 0))}}"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <a href="{{url('data/jabatan/delete/'.($jabatan ? $jabatan->jabatan_id : 0))}}" class="text-danger" data-jumlah="{{$peg}}" onclick="notifyConfirm(event,{{$peg}})"><i class="fa fa-trash-o"></i> Hapus</a>
         </td>
         @endif
       </tr>
      <?php
      $jf_parent = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',null)->where('jabatan_jenis',3)->get();
      $jfu_parent = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',null)->where('jabatan_jenis',4)->get();
      ?>
      @if(count($jf_parent) > 0)
        @foreach($jf_parent as $jfp)
        <?php
        $jumjfp = App\Model\Pegawai::where('jabatan_id',$jfp->jabatan_id)->where('peg_status', 'TRUE')->count();
        ?>
          <tr>
            <td></td>
            <td style="padding-left:50px">{{$jfp->jabatan_fungsional ? $jfp->jabatan_fungsional->jf_nama : ''}}</td>
            <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfp->jabatan_id}}">{{$jumjfp}}</a></td>
            <td>
       <a href="{{url('data/jabatan/delete/'.$jfp->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfp}}" onclick="notifyConfirm(event,{{$jumjfp}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
          </tr>
        @endforeach
      @endif
      @if(count($jfu_parent) > 0)
        @foreach($jfu_parent as $jfup)
        <?php
        $jumjfup = App\Model\Pegawai::where('jabatan_id',$jfup->jabatan_id)->where('peg_status', 'TRUE')->count();
        ?>
          <tr>
            <td></td>
            <td style="padding-left:50px">
              {{$jfup->jabatan_fungsional_umum ? $jfup->jabatan_fungsional_umum->jfu_nama : ''}}
            </td>
            <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfup->jabatan_id}}">{{$jumjfup}}</a></td>
            <td>
       <a href="{{url('data/jabatan/delete/'.$jfup->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfup}}" onclick="notifyConfirm(event,{{$jumjfup}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
          </tr>
        @endforeach
      @endif
       <tr>
         <td></td>
         <td><a href="{{url('data/jabatan/tambah/?satuan_kerja_id='.$satker->satuan_kerja_id).'&unit_kerja_id=null'}}" class="btn btn-info btn-circle btn-xs"><i class="fa fa-plus"></i></a></td>
         <td></td>
         <td></td>
       </tr>
      @foreach($uk_parent as $up)
        <?php
        $peg2 = 0;
        $uk_child = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', '2')->where('unit_kerja_parent', $up->unit_kerja_id)->orderBy('unit_kerja_left')->get();
        $jab_parent = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$up->unit_kerja_id)->where('jabatan_jenis',2)->get();
        ?>
        @if(count($jab_parent) > 0)
        @foreach($jab_parent as $key => $jp)
        <?php
        $peg2 = App\Model\Pegawai::where('jabatan_id',$jp->jabatan_id)->where('peg_status', 'TRUE')->count();
        ?>
        <tr>
          @if($key == 0)
          <td style="padding-left:25px">{{$up->unit_kerja_nama}}</td>
          @else
          <td></td>
          @endif

          <td>
          @if($jp->jabatan_nama == "")
            Tidak ada nama
          @else
              {{$jp->jabatan_nama}} ({{empty($jp->kode_jabatan) ? '-' : $jp->kode_jabatan}})
          @endif
          </td>
          <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jp->jabatan_id}}">{{$peg2}}</a></td>
          <td><a href="{{url('data/jabatan/edit/'.$jp->jabatan_id)}}"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <a href="{{url('data/jabatan/delete/'.$jp->jabatan_id)}}" class="text-danger" data-jumlah="{{$peg2}}" onclick="notifyConfirm(event,{{$peg2}})"><i class="fa fa-trash-o"></i> Hapus</a>
          </td>
        </tr>
        @endforeach
        @else
          <tr>
            <td style="padding-left:50px">{{$up->unit_kerja_nama}}</td>
            <td><i>&lt;Tidak ada Jabatan Struktural&gt;</i>
            </td>
            <td>0</td>
            <td>
            </td>
          </tr>
        @endif
        <?php
        $jf_parent = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$up->unit_kerja_id)->where('jabatan_jenis',3)->get();
        $jfu_parent = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$up->unit_kerja_id)->where('jabatan_jenis',4)->get();
        ?>
        @if(count($jf_parent) > 0)
          @foreach($jf_parent as $jfp)
          <?php
          $jumjfp = App\Model\Pegawai::where('jabatan_id',$jfp->jabatan_id)->where('peg_status', 'TRUE')->count();
          ?>
            <tr>
              <td></td>
              <td style="padding-left:50px">{{$jfp->jabatan_fungsional ? $jfp->jabatan_fungsional->jf_nama : ''}}</td>
              <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfp->jabatan_id}}">{{$jumjfp}}</a></td>
              <td>
         <a href="{{url('data/jabatan/delete/'.$jfp->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfp}}" onclick="notifyConfirm(event,{{$jumjfp}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
            </tr>
          @endforeach
        @endif
        @if(count($jfu_parent) > 0)
          @foreach($jfu_parent as $jfup)
          <?php
          $jumjfup = App\Model\Pegawai::where('jabatan_id',$jfup->jabatan_id)->where('peg_status', 'TRUE')->count();
          ?>
            @if($jfup->jabatan_id < 34038)
            <tr>
              <td></td>
              <td style="padding-left:50px">
                {{$jfup->jabatan_fungsional_umum ? $jfup->jabatan_fungsional_umum->jfu_nama : ''}}
              </td>
              <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfup->jabatan_id}}">{{$jumjfup}}</a></td>
              <td>
         <a href="{{url('data/jabatan/delete/'.$jfup->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfup}}" onclick="notifyConfirm(event,{{$jumjfup}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
            </tr>
            @endif
          @endforeach
        @endif
        <tr>
          <td></td>
          <td><a href="{{url('data/jabatan/tambah?satuan_kerja_id='.$satker->satuan_kerja_id.'&unit_kerja_id='.$up->unit_kerja_id)}}" class="btn btn-info btn-circle btn-xs"><i class="fa fa-plus"></i></a></td>
          <td></td>
          <td></td>
        </tr>
          @foreach($uk_child as $uc)
            <?php

              $uk_child2 = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', '3')->where('unit_kerja_parent', $uc->unit_kerja_id)->orderBy('unit_kerja_left')->get();
              $jab_child = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc->unit_kerja_id)->where('jabatan_jenis',2)->get();
              ?>
              @forelse($jab_child as $key => $jc)
            <tr>
              @if($key == 0)
              <td style="padding-left:75px">{{$uc->unit_kerja_nama}}</td>
              @else
              <td></td>
              @endif
              <?php
              $peg3 = App\Model\Pegawai::where('jabatan_id',$jc->jabatan_id)->where('peg_status', 'TRUE')->count();
              ?>
              <td>
                  @if($jc->jabatan_nama == "")
                    <i>Tidak ada nama</i>
                  @else
                      {{$jc->jabatan_nama}} ({{empty($jc->kode_jabatan) ? '-' : $jc->kode_jabatan}})
                  @endif
              </td>
              <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jc->jabatan_id}}">{{$peg3}}</a></td>
              <td><a href="{{url('data/jabatan/edit/'.$jc->jabatan_id)}}"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <a href="{{url('data/jabatan/delete/'.$jc->jabatan_id)}}" class="text-danger" data-jumlah="{{$peg3}}" onclick="notifyConfirm(event,{{$peg3}})"><i class="fa fa-trash-o"></i> Hapus</a>
              </td>
            </tr>
              @empty
            <tr>
              <td style="padding-left:75px">{{$uc->unit_kerja_nama}}</td>
              <td><em>Belum ada jabatan struktural</em></td>
            </tr>
              @endforelse
            <?php
            $jf_child = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc->unit_kerja_id)->where('jabatan_jenis',3)->get();
            $jfu_child = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc->unit_kerja_id)->where('jabatan_jenis',4)->get();
            ?>
            @if(count($jf_child) > 0)
              @foreach($jf_child as $jfc)
              <?php
              $jumjfc = App\Model\Pegawai::where('jabatan_id',$jfc->jabatan_id)->where('peg_status', 'TRUE')->count();
              ?>
                <tr>
                  <td></td>
                  <td style="padding-left:50px">{{$jfc->jabatan_fungsional ? $jfc->jabatan_fungsional->jf_nama : ''}}</td>
                  <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfc->jabatan_id}}">{{$jumjfc}}</a></td>
                  <td><a href="{{url('data/jabatan/delete/'.$jfc->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfc}}" onclick="notifyConfirm(event,{{$jumjfc}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
                </tr>
              @endforeach
            @endif
            @if(count($jfu_child) > 0)
              @foreach($jfu_child as $jfuc)
              <?php
              $jumjfuc = App\Model\Pegawai::where('jabatan_id',$jfuc->jabatan_id)->where('peg_status', 'TRUE')->count();
              ?>
                @if($jfuc->jabatan_id < 34038)
                <tr>
                  <td></td>
                  <td style="padding-left:50px">{{$jfuc->jabatan_fungsional_umum ? $jfuc->jabatan_fungsional_umum->jfu_nama : ''}}</td>
                  <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfuc->jabatan_id}}">{{$jumjfuc}}</a></td>
                  <td><a href="{{url('data/jabatan/delete/'.$jfuc->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfuc}}" onclick="notifyConfirm(event,{{$jumjfuc}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
                </tr>
                @endif
              @endforeach
            @endif
            <tr>
              <td></td>
              <td><a href="{{url('data/jabatan/tambah?satuan_kerja_id='.$satker->satuan_kerja_id.'&unit_kerja_id='.$uc->unit_kerja_id)}}" class="btn btn-info btn-circle btn-xs"><i class="fa fa-plus"></i></a></td>
              <td></td>
              <td></td>
            </tr>
            @foreach($uk_child2 as $uc2)
              <?php
              $jab_child2 = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc2->unit_kerja_id)->where('jabatan_jenis',2)->get();
              ?>
              @forelse($jab_child2 as $key => $jc2)
              <?php
                $peg4 = App\Model\Pegawai::where('jabatan_id', $jc2->jabatan_id)->where('peg_status', 'TRUE')->count();

              ?>
              <tr>
                @if($key == 0)
                <td style="padding-left:100px">{{$uc2->unit_kerja_nama}}</td>
                @else
                <td></td>
                @endif
                <td>
                  @if($jc2->jabatan_nama == "")
                    <i>Tidak ada nama</i>
                  @else
                      {{$jc2->jabatan_nama}} ({{empty($jc2->kode_jabatan) ? '-' : $jc2->kode_jabatan}})
                  @endif
                </td>
                <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jc2->jabatan_id}}">{{$peg4}}</a></td>
                <td><a href="{{url('data/jabatan/edit/'.$jc2->jabatan_id)}}"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <a href="{{url('data/jabatan/delete/'.$jc2->jabatan_id)}}" class="text-danger" data-jumlah="{{$peg4}}" onclick="notifyConfirm(event,{{$peg4}})"><i class="fa fa-trash-o"></i> Hapus</a>
                </td>
              </tr>
              @empty
            <tr>
              <td style="padding-left:100px">{{$uc->unit_kerja_nama}}</td>
              <td><em>Belum ada jabatan struktural</em></td>
            </tr>
              @endforelse
              <?php
              $jf_child2 = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc2->unit_kerja_id)->where('jabatan_jenis',3)->get();
              $jfu_child2 = App\Model\Jabatan::where('satuan_kerja_id',$satker->satuan_kerja_id)->where('unit_kerja_id',$uc2->unit_kerja_id)->where('jabatan_jenis',4)->get();
              ?>
              @if(count($jf_child2) > 0)
                @foreach($jf_child2 as $jfc2)
                <?php
                $jumjfc2 = App\Model\Pegawai::where('jabatan_id',$jfc2->jabatan_id)->where('peg_status', 'TRUE')->count();
                ?>
                  <tr>
                    <td></td>
                    <td style="padding-left:50px">
                      @if(!$jfc2->jabatan_fungsional || empty($jfc2->jabatan_fungsional->jf_nama))
                        Tidak Ada Nama Fungsional
                      @else
                        {{$jfc2->jabatan_fungsional->jf_nama}}
                      @endif
                    </td>
                    <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfc2->jabatan_id}}">{{$jumjfc2}}</a></td>
                    <td><a href="{{url('data/jabatan/delete/'.$jfc2->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfc2}}" onclick="notifyConfirm(event,{{$jumjfc2}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
                  </tr>
                @endforeach
              @endif
              @if(count($jfu_child2) > 0)
                @foreach($jfu_child2 as $jfucd)
                <?php
                $jumjfuc2 = App\Model\Pegawai::where('jabatan_id',$jfucd->jabatan_id)->where('peg_status', 'TRUE')->count();
                ?>
                  @if($jfucd->jabatan_id < 34038)
                  <tr>
                    <td></td>
                    <td style="padding-left:50px">
                      @if(!$jfucd->jabatan_fungsional_umum || empty($jfucd->jabatan_fungsional_umum->jfu_nama))
                        Tidak Ada Nama Fungsional Umum
                      @else
                        {{$jfucd->jabatan_fungsional_umum->jfu_nama}}
                      @endif
                    </td>
                    <td><a data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$jfucd->jabatan_id}}">{{$jumjfuc2}}</a></td>
                    <td><a href="{{url('data/jabatan/delete/'.$jfucd->jabatan_id)}}" class="text-danger" data-jumlah="{{$jumjfuc2}}" onclick="notifyConfirm(event,{{$jumjfuc2}})"><i class="fa fa-trash-o"></i> Hapus</a></td>
                  </tr>
                  @endif
                @endforeach
              @endif

              <tr>
                <td></td>
                <td><a href="{{url('data/jabatan/tambah?satuan_kerja_id='.$satker->satuan_kerja_id.'&unit_kerja_id='.$uc2->unit_kerja_id)}}" class="btn btn-info btn-circle btn-xs"><i class="fa fa-plus"></i></a></td>
                <td></td>
                <td></td>
              </tr>
            @endforeach
          @endforeach
        @endforeach
       </tbody>
   </table>
   </div>
  @endif
<br>
<br>
@include('includes/modal-jumlah-pegawai')
@endsection
@section('styles')
<style type="text/css">
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
  }
  .btn-circle.btn-lg {
    width: 50px;
    height: 50px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
  }
  .btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    font-size: 24px;
    line-height: 1.33;
    border-radius: 35px;
  }
  .jumpeg{
    cursor:pointer;
  }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
  $('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
      var val = $("#satuan_kerja_id option:selected").val();
      var url = "{{url('data/jabatan')}}?satuan_kerja_id="+val;
      window.location.href= url;
  });
function notifyConfirm(e,jum) {
  console.log(jum);
  if (!confirm("Apakah anda yakin?")) {
    e.preventDefault();
    prevent = true;
    return false;
  }
  if(jum != 0){
    alert('Jumlah Pegawai Harus 0 untuk bisa Menghapus Jabatan');
    e.preventDefault();
    prevent = true;
    return false;
  }else{
    if (!confirm("Apakah anda yakin untuk menghapus Jabatan Ini?")) {
      e.preventDefault();
      prevent = true;
      return false;
    }
    return true;
  }
}

$('.peg').dataTable({
  "ordering": false,
  "info":     false
});
$('body').on('click','.jumpeg',function(){
  var id = $(this).attr('data-id');
  $.get("{{action('JabatanController@getPegawai')}}",{id:id},function(data){
    if(data.length > 0){
    $.each(data,function(key,value){
      if(key == 0){
        $('.listpeg').html("<tr><td>"+value.peg_nip+"</td><td>"+value.peg_nama+"</td><td><a class='view-pegawai' href='{{url('pegawai/profile/edit')}}/"+value.peg_id+"' title='View Pegawai'><span class='fa fa-search'></span></a> <a class='edit-pegawai' href='{{url('edit-pegawai')}}/"+value.peg_id+"' title='Edit Pegawai'><span class='ace-icon glyphicon glyphicon-pencil'></span></a></td></tr>");
      }else{
        $('.listpeg').append("<tr><td>"+value.peg_nip+"</td><td>"+value.peg_nama+"</td><td><a class='view-pegawai' href='{{url('pegawai/profile/edit')}}/"+value.peg_id+"' title='View Pegawai'><span class='fa fa-search'></span></a> <a class='edit-pegawai' href='{{url('edit-pegawai')}}/"+value.peg_id+"' title='Edit Pegawai'><span class='ace-icon glyphicon glyphicon-pencil'></span></a></td></tr>");
      }
    });
    }else{
      $('.listpeg').html("<tr><td colspan='3'>Tidak Ada Data Pegawai</td></tr>");
    }
  });
});
</script>
@endsection
