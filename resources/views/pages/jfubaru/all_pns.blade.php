@extends('layouts.app')

@section('content')
<h2 align="center">
  Data Seluruh Pelaksana Kota Bandung
</h2>
<br>
<br>
<div id="vm">
      <div class="col-lg-6">
          <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                          <h4 class="panel-title">
                              Search
                          </h4>
                      </a>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <div class="form-inline">
                          <div class="form-group">
                            <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input" v-on="keyup: doSearch | key 'enter'">
                            <button class="btn btn-primary ewButton" id="btnsubmit" name="btnsubmit" v-on="click: doSearch">Search</button>
                            <div class="btn-group ewButtonGroup">
                              <a class="btn btn-default" v-on="click: clearFilter">Show all</a>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
    </div>
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
  @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
   <div class="col-lg-12">
  <div class="loading panel" v-show="isLoading">
    <h3>Sedang memuat data...</h3>
  </div>
  <a class="btn btn-success btn-xs" v-on="click: exportData">Export All Pegawai</a>
  </div>
  <section v-show="!isLoading" style="display: none">
  <div class="table-responsive">
    <table cellspacing="0" class="table table-striped table-advanced">
      <tbody>
        <tr>
          <td class="ewGridContent">
            <div class="ewGridMiddlePanel" id="gmp_pegawai">
              <table class="table table-bordered table-striped table-condensed" id="tbl_pegawailist">
                <thead>
                  <!-- Table header -->
                  <tr class="ewTableHeader">
                    @foreach ($columns as $column_id => $column)
                    <th>
                      <div class="ewPointer" v-on="click: sort('{{$column_id}}')">
                        <div class="pegawai_{{$column_id}}" id="elh_pegawai_{{$column_id}}">
                          <div class="ewTableHeaderBtn">
                            <span class="ewTableHeaderCaption">{{$column[0]}}</span>
                            <span class="ewTableHeaderSort">
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'desc'" class="caret"></span>
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'asc' " class="caret ewSortUp"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </th>
                    @endforeach
                    <th class="ewListOptionHeader" data-name="view" style="white-space: nowrap;"><span>&nbsp;</span></th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="@{{$index % 2 ? 'ewTableAltRow' : 'ewTableRow'}}" data-rowindex="@{{$index + 1}}" data-rowtype="1" v-repeat="items">
                    @foreach ($columns as $column)
                    <td>
                      @if($column[1] == 'peg_status')
                        <span class="@{{peg_status == 'Aktif - PNS' || peg_status == 'Aktif' ? 'label label-primary' : (peg_status == 'Aktif - CPNS' ? 'label label-info' : (peg_status == 'Pegawai Pensiun' ? 'label label-danger' : 'label label-warning'))}}" v-text="{{$column[1]}}"></span>
                      @else
                        <span v-text="{{$column[1]}}"></span>
                      @endif
                    </td>
                    @endforeach
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{url('evjab/search/profile-ujikom/edit')}}/@{{peg_id}}" data-toggle="tooltip" title="Lihat Detail" target="_blank">Lihat Detail
                        </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="ewGridLowerPanel">
              <form class="form-inline">
                <div class="form-group">
                  Page &nbsp;
                  <!--first page button-->
                  <a class="btn btn-default btn-xs" v-on="click: paginate('first')"><i class="glyphicon glyphicon-step-backward"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('previous')"><i class="glyphicon glyphicon-chevron-left"></i></a>
                  <input class="form-control input-sm" type="text" v-model="pagination.page" v-on="change: changePage, keyup: changePage | key 'enter'">
                  <a class="btn btn-default btn-xs" v-on="click: paginate('next')"><i class="glyphicon glyphicon-chevron-right"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('last')"><i class="glyphicon glyphicon-step-forward"></i></a>
                  &nbsp;of&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                  &nbsp;&nbsp;&nbsp;&nbsp; Records&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;to&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;of&nbsp;@{{ pagination.count }}</td>
                </div>
              </form>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  </section>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var page = 'pns';
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: true,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 20,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_input = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    },
    exportData: function () {
      this.search = this.search_input;
      exportExcel();
    }
  }
});
loadState();
vm.changePage();
function getData(page) {
  $.getJSON("{{$url}}?page="+page+'&perpage='+vm.pagination.perpage,
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
    saveState();
  });
}
function saveState() {
  var stored = {
    pagination : JSON.stringify(vm.pagination),
    params : JSON.stringify(vm.params),
    search : vm.search,
    search_input : vm.search_input,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem(page + '_state',JSON.stringify(stored));
}
function loadState() {
  var stored = JSON.parse(localStorage.getItem(page + '_state'));
  if (stored) {
    vm.pagination = JSON.parse(stored.pagination);
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.search_input = stored.search_input;
    vm.sortColumn = stored.sortColumn;
    vm.sortDir = stored.sortDir;
  }
}
function exportExcel(){
  var url = "{{url('pns-evjab/export')}}";
  var params = '?search=' + vm.search;
        
  window.location.href = url+params;
}
</script>
@endsection
