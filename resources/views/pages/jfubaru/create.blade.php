@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
  <h3>Tambah Nama Jabatan Pelaksana 2019</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('evjab/jfu/store') }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <table class="table no-footer">
                  <tr>
                    @include('form.txt',['label' => 'Nama Jabatan','required'=>true,'name'=>'namajab'])
                  </tr>
                  <tr>
                    @include('form.select2',['label'=>'Golongan Awal','required'=>false,'name'=>'gol_awal','vue'=>'', 'data'=>App\Model\Golongan::lists('nm_gol','gol_id')])
                  </tr>
                  <tr>
                    @include('form.select2',['label'=>'Golongan Akhir','required'=>false,'name'=>'gol_akhir','vue'=>'', 'data'=>App\Model\Golongan::lists('nm_gol','gol_id')])
                  </tr>
                  <tr>
                    @include('form.number',['label' => 'Batas Usia Pensiun','required'=>true,'name'=>'bup','maxlength'=>3])
                  </tr>
                  <tr>
                    @include('form.txt',['label' => 'Syarat','required'=>false,'name'=>'syarat'])
                  </tr>
                  <tr>
                    <td>Jabatan Kelas</td>
                    <td>:</td>
                    <td>
                      <select name="jabatan_kelas" id="jabkelas" style="width: 100px">
                        <option value="">Pilih Kelas</option>
                        @foreach($jfkelas as $jk)
                        <option value="{{$jk->kelas}}">{{$jk->kelas}} (Nilai Jabatan : {{$jk->nilai_jabatan}})</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td>Kode Jabatan</td>
                    <td>:</td>
                    <td>
                      <input type="text" name="kojab" class="form-control" id="kojab" style="width: 200px" readonly="">
                    </td>
                  </tr>
                </table>

                 <button class="btn btn-primary btn-xs" type="submit">Simpan</button>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  $('#jabkelas').select2({ width: '300px' });
  $('#kategori').select2({ width: '500px' });
  $('#rumpun').select2({ width: '400px',placeholder:'Pilih Rumpun' });

  $(".quantity").on("keydown", function (e) {
      var key   = e.keyCode ? e.keyCode : e.which;

      if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
           (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
           (key >= 35 && key <= 40) ||
           (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
           (key >= 96 && key <= 105)
         )) e.preventDefault();
  });

  $('#jabkelas').on('change',function(){
    var val = $(this).val();

    if(val == ''){
    }

    $.ajax({
      url:"{{url('evjab/jfu/searchkojab')}}",
      type:'GET',
      data:{"jabatan_kelas":val},
      success:function(data){
        $('#kojab').val(data);
      }

    });
  });
  </script>
@endsection
