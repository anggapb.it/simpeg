@extends('layouts.app')
<?php
    use App\Model\TingkatPendidikan;
    use App\Model\Pendidikan;
    use App\Model\Jabatan;
    use App\Model\JfuBaru;
    use App\Model\JabatanFungsional;
    use App\Model\JabatanFungsionalUmum;
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
    use App\Model\KategoriPendidikan;
    use App\Model\Kabupaten;
    use App\Model\Kecamatan;
    $pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
    $pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
    if($pend_awal){
        $kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first();
        if($kat_pend_awal){
            $tingpend_awal = TingkatPendidikan::where('tingpend_id', $kat_pend_awal->tingpend_id)->first();
        }
    }
    if($pend_akhir){
        $kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first();
        if($kat_pend_akhir){
            $tingpend_akhir = TingkatPendidikan::where('tingpend_id', $kat_pend_akhir->tingpend_id)->first();
        }
    }
    $jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id_evjab)->first();
    if($jabatan){
        $jabatan_jenis= $jabatan->jabatan_jenis;
        $jabatan_id = $pegawai->jabatan_id;
        if($jabatan_jenis == 3){
            $jf = $jabatan->jf_id;
        }elseif ($jabatan_jenis == 4) {
            $jfu = $jabatan->jfu_id;
        }
        $satuan_kerja =SatuanKerja::where('satuan_kerja_id', $jabatan->satuan_kerja_id)->first();
    }else{
        $jabatan_nama ='';
        $jabatan_id =0;
        $jabatan_jenis =4;
        $jfu = 0;
    }
    //$jabatan_nama = $jabatan->jabatan_nama;

    $sopd_nama = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();

    $kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
    if($kecamatan){
        $kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
    }else{
        $kab = null;
    }
    //$uk = [''=>''] + App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id')->all();
    $uk = App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('unit_kerja_id', $pegawai->unit_kerja_id)->where('status',1)->select('unit_kerja_nama')->first();
    $nama_unit_kerja = $uk ? $uk->unit_kerja_nama : '';
?>
@section('content')
<div class="bs-callout bs-callout-warning hidden">
  <h4>Edit Data Gagal!</h4>
  <p>Data Harus Lengkap!</p>
</div>

<div class="row">
    <div class="col-md-12">
        <center><h5 class="title"><b>EDIT PEGAWAI</b></h5></center>
        <br>
        <small class="tampil">*Slide Ke Kiri Untuk Mengisi Semua Form</small>
    </div>
    @if (session('message'))
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
    @endif
</div>
<form action="{{url('evjab/update-pegawai',$pegawai->peg_id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="form-edit" data-parsley-validate="" onsubmit="parent.scrollTo(0, 0); return true">
    {!! csrf_field() !!}
    <div class="table-responsive">
    <table class="table no-footer">
        <tr>
            <?php
            $satker = SatuanKerja::where('satuan_kerja_id',$pegawai->satuan_kerja_id)->first();
            ?>
            <td >Satuan Kerja</td>
            <td>:</td>
            <td>{{$satker->satuan_kerja_nama}}
              <input type="hidden" id="satuan_kerja_id" name="satuan_kerja_id" value="{{$satker->satuan_kerja_id}}" />
            </td>
        </tr>
        <tr>
            <?php
            $uker = UnitKerja::where('unit_kerja_id',$pegawai->unit_kerja_id)->first();
            ?>
            <td>Unit Kerja</td>
            <td>:</td>
            <td>{{$uker->unit_kerja_nama}}
              <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{$uker->unit_kerja_id}}" />
            </td>
        </tr>
        <tr>
          <td>NIP</td>
          <td>:</td>
          <td>{{$pegawai->peg_nip}}
            <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{$uker->unit_kerja_id}}" />
          </td>
        </tr>
        <tr>
          <td>Nama</td>
          <td>:</td>
          <td>{{$pegawai->peg_nama}}</td>
        </tr>
        <tr>
          <?php $jenjab = 'Fungsional Umum'; ?>
          <td>Jenis Jabatan</td>
          <td>:</td>
          <td>{{$jenjab}}</td>
          <td>
            <input type="hidden" name="jenis_jabatan" id="jenis_jabatan" value="{{$jabatan_jenis}}">
          </td>
        </tr>
        <tr>
            <td>Nama Jabatan Lama</td>
            <td>:</td>
            <td>{{$pegawai->jfu_nama}}</td>
        </tr>
<tr>
    <?php
        $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jfu_id')->get(['jfu_id']);
        $id = [];
        $jafungumum = JfuBaru::where('jfu_id',$jfu)->first();
        foreach ($jab as $j) {
            $id[] = $j->jfu_id;
        }
    ?>
        @include('form.txt',['label'=>'Nama Jabatan 2019','required'=>true,'name'=>'jabatan_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$jafungumum ? $jafungumum->jfu_nama : '(belum dipilih)','readonly' => true,'maxlength'=>500])
    <td>
      <input type="hidden" name="jabatan_id" id="jabatan_id" value="{{$pegawai->jabatan_id_evjab}}">
      <input type="hidden" name="peg_jabatan_tmt" id="peg_jabatan_tmt" value="01-01-2019">
      <input type="hidden" name="gol_id_akhir" id="gol_id_akhir" value="{{$pegawai->gol_id_akhir}}">
    </td>
</tr>


@if($jabatan_jenis == 3)
<?php
    $jafung = JabatanFungsional::where('jf_id',$jf)->first();
?>
@if($jafung->rumpun_id == 17 && !in_array($jafung->jf_nama,['Pengawas Madya','Pengawas Muda','Pengawas Utama']))
<tr>
    @include('form.select2', ['label'=>'Jenis Guru', 'required'=>false, 'name'=>'jenis_guru','data'=>['Guru Kelas'=>'Guru Kelas','Guru Agama Islam'=>'Guru Agama Islam','Guru Penjasorkes'=>'Guru Penjasorkes','Guru PPKN'=>'Guru PPKN','Guru Bahasa Indonesia'=>'Guru Bahasa Indonesia','Guru Bahasa Inggris'=>'Guru Bahasa Inggris','Guru Matematika'=>'Guru Matematika','Guru IPA'=>'Guru IPA','Guru IPS'=>'Guru IPS','Guru Seni Budaya'=>'Guru Seni Budaya','Guru TIK'=>'Guru TIK','Guru Muatan Lokal'=>'Guru Muatan Lokal','Guru Prakarya dan Kewirausahaan'=>'Guru Prakarya dan Kewirausahaan','Guru Bimbingan dan Konseling'=>'Guru Bimbingan dan Konseling'] ,'empty'=>'-Pilih-' ,'value'=>$pegawai->jenis_guru])
</tr>

@endif
@endif

<tr>
    <td>Kelas Jabatan</td>
    <td>:</td>
    <td>
        <input id="info_kelas" type="text" name="info_kelas" value="{{$kelas}}" size="12" maxlength="10" readonly="">
    </td>
    <td>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-jabatan">Update Jabatan</button></td>
</tr>
<tr>
    <td colspan="9" align="center">
        <input type="submit" value="Simpan" name="simpan" id="simpan_data" class="btn btn-xs">
        <a href="{{url('/evjab/search/profile/edit',$pegawai->peg_id)}}" class="btn btn-xs">Batal</a>
    </td>
</tr>
</table>
</div>
</form>

<div class="modal fade" id="modal-jabatan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelPangkat"><div id="modal-button-edit">Update Jabatan</div></h4>
        </div>
        <form id="pangkatForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <div class="bs-callout bs-callout-warning hidden">
              <h4>Edit Data Gagal!</h4>
              <p>Data Harus Lengkap!</p>
            </div>
            <div class="row" id="riw_jabatan_satker_container">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4">SOPD <span class="required" aria-required="true">*</span></label>
                  <div class="col-md-8">
                    {{$satker->satuan_kerja_nama}}
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row" id="riw_jabatan_uker_container">
              <div class="col-md-12">
                  <div class="form-group">
                      <label class="control-label col-md-4">Unit Kerja <span class="required" aria-required="true">*</span></label>
                      <div class="col-md-8">
                        {{$uker->unit_kerja_nama}}
                      </div>
                  </div>
              </div>
            </div>
            <br>
            <div class="row" id="riw_jabatan_jenjab_container">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4">Jenis Jabatan <span class="required" aria-required="true">*</span></label>
                  <div class="col-md-8">
                    Fungsional Umum
                  </div>
                </div>
              </div>
            </div>
            <br>
            @include('form.select2_modal',[
            'label'=>'Nama Jabatan 2019',
            'required'=>true,
            'name'=>'riw_jabatan_jab',
            'data'=>App\Model\JfuBaru::select(DB::raw("(jfu_nama ||' | Kelas: '|| jfu_kelas) AS jfu_full, jfu_id"))->where('is_deleted', false)->orderBy('jfu_full')->lists('jfu_full','jfu_id'),
            'empty'=>'-Pilih-',
            'value'=>$jfu
            ])
        </div>
        <div class="modal-footer">
            <input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
            <button type="button" id="submit_jabatan" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
$("#riw_jabatan_tglsk").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#riw_jabatan_tmt").val("{{editDate($pegawai->peg_jabatan_tmt)}}").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#submit_jabatan").click(function(){
    var jab = $("#riw_jabatan_jab").val();
    var tempNama = $("#riw_jabatan_jab option[value='"+jab+"']").text();
    var namaJab = tempNama.substr(0, tempNama.indexOf('|')-1);
    var tempKelas = tempNama.substr(tempNama.indexOf('|')+9);
    $("#jabatan_nama").val(namaJab);
      $("#jabatan_id").val(jab);
    $("#no_sk_jabatan").val($("#riw_jabatan_nosk").val());
    $("#tanggal_sk_jabatan").val($("#riw_jabatan_tglsk").val());
    $("#jabatan_penandatangan_jab").val($("#riw_jabatan_jabttd").val());
    $("#tt7").val($("#riw_jabatan_tmt").val());
    $('#modal-jabatan').modal('hide');
    $('#info_kelas').val(tempKelas);
    $("#jabatan_id").val(jab);
});

jQuery(function($){
     $("#tt1").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt2").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt3").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt4").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt5").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt6").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt7").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
});

$('#riw_jabatan_uker').select2({ width: '500px' }).select2('data', {id: "{{$pegawai->unit_kerja_id}}", text: "{{$nama_unit_kerja}}"}).on('change', function (e){
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    getJabatanRiw();
});

$('#riw_jabatan_satker').select2({ width: '400px' }).on('change', function (e){
    var v = $('#riw_jabatan_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#riw_jabatan_uker").empty();
            $("#riw_jabatan_uker").select2("val", "");
            $('#riw_jabatan_uker').append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#riw_jabatan_uker').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            getJabatanRiw();
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
});

$('#riw_jabatan_jab').select2({ width: '370px' });

$('#riw_jabatan_jenjab').select2({ width: '370px' }).on('change', function (e){
   $("#jabatan_id").empty();
   $("#jabatan_id").select2("val", "");
   var val = $(this).val();
   getJabatanRiw();
});

function getJabatan(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            //$("#jabatan_id").select2("val", "");
                var unit_kerja_id = $('#unit_kerja_id').val();
                var satuan_kerja_id = $('#satuan_kerja_id').val();
                var v = $('#jenis_jabatan').val();
                //console.log(v);
                if(v == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            //$("#jabatan_id").empty();
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v== 4){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            //$("#jabatan_id").empty();
                            for (row in data) {
                                //console.log(data);
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v==2){
                    //$("#jabatan_id").empty();
                    for (row in data) {
                        $('#jabatan_id').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
                    }
                }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

$('#riw_jabatan_uker').on('change',function(){
    $('#riw_jabatan_jab').empty();
    $('#riw_jabatan_jab').val('').trigger('change');
    $('#riw_jabatan_jenjab').val('').trigger('change');
});

$('#riw_jabatan_jenjab').on('change',function(){
    $('#riw_jabatan_jab').empty();
    $('#riw_jabatan_jab').val('').trigger('change');
});

function getJabatanRiw(){
    var v = $('#riw_jabatan_jenjab').val();
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    var satuan_kerja_id = $('#riw_jabatan_satker').val();

    $("#riw_jabatan_jab").select2("val", "");
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            //$("#jabatan_id").select2("val", "");
                var unit_kerja_id = $('#riw_jabatan_uker').val();
                var satuan_kerja_id = $('#riw_jabatan_satker').val();
                var v = $('#riw_jabatan_jenjab').val();
                //console.log(v);
                if(v == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#riw_jabatan_jab").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                            $('#riw_jabatan_jab').val('').trigger('change');
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v== 4){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        data:{"unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#riw_jabatan_jab").empty();
                            //$("#jabatan_id").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));

                            $('#riw_jabatan_jab').val('').trigger('change');
                            for (row in data) {
                                //console.log(data);
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                            //console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(v==2){
                    //$("#jabatan_id").empty();
                            $("#riw_jabatan_jab").empty();
                            $('#riw_jabatan_jab').append($('<option></option>').attr('value', '').text('-- Pilih --'));

                            $('#riw_jabatan_jab').val('').trigger('change');
                    for (row in data) {
                        $('#riw_jabatan_jab').append($('<option></option>').attr('value', data[row].jabatan_id).text(data[row].jabatan_nama));
                    }
                }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

function getJabatanAwal(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            $("#jabatan_id").empty();
            for (row in data) {
                var jab = data[row].jabatan_id;
                if(data[row].jabatan_jenis == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(data[row].jabatan_jenis == 4){
                    $('#tampilJenisButton').show();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jfu_nama));
                            }

                            console.log($('#jabatan_id').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else{
                    $('#jabatan_id').append($('<option></option>').attr('value', jab).text(data[row].jabatan_nama));
                }
            }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

function getJabatanAwalRiw(){
    var v = $('#riw_jabatan_jenjab').val();
    var unit_kerja_id = $('#riw_jabatan_uker').val();
    var satuan_kerja_id = $('#riw_jabatan_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatan') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            $("#riw_jabatan_jab").empty();
            for (row in data) {
                var jab = data[row].jabatan_id;
                if(data[row].jabatan_jenis == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jf_nama));
                            }
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else if(data[row].jabatan_jenis == 4){
                    $('#tampilJenisButton').show();
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jfu_nama));
                            }

                            console.log($('#riw_jabatan_jab').val());
                        },
                        error: function (e){
                            console.log(e);
                            // alert("ERROR!");
                        },
                    });
                }else{
                    $('#riw_jabatan_jab').append($('<option></option>').attr('value', jab).text(data[row].jabatan_nama));
                }
            }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
}

$(".quantity").on("keydown", function (e) {
var key   = e.keyCode ? e.keyCode : e.which;

if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
     (key >= 35 && key <= 40) ||
     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
     (key >= 96 && key <= 105)
   )) e.preventDefault();
});
</script>
@endsection
