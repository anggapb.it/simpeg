@extends('layouts.app')
<?php
    use App\Model\TingkatPendidikan;
    use App\Model\Pendidikan;
    use App\Model\Jabatan;
    use App\Model\JfuBaru;
    use App\Model\JabatanFungsional;
    use App\Model\JabatanFungsionalUmum;
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
    use App\Model\KategoriPendidikan;
    use App\Model\Kabupaten;
    use App\Model\Kecamatan;
    $pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
    $pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
    if($pend_awal){
        $kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first();
        if($kat_pend_awal){
            $tingpend_awal = TingkatPendidikan::where('tingpend_id', $kat_pend_awal->tingpend_id)->first();
        }
    }
    if($pend_akhir){
        $kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first();
        if($kat_pend_akhir){
            $tingpend_akhir = TingkatPendidikan::where('tingpend_id', $kat_pend_akhir->tingpend_id)->first();
        }
    }
    $jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id_evjab)->first();
    if($jabatan){
        $jabatan_jenis= $jabatan->jabatan_jenis;
        $jabatan_id = $pegawai->jabatan_id;
        if($jabatan_jenis == 3){
            $jf = $jabatan->jf_id;
        }elseif ($jabatan_jenis == 4) {
            $jfu = $jabatan->jfu_id;
        }
        $satuan_kerja =SatuanKerja::where('satuan_kerja_id', $jabatan->satuan_kerja_id)->first();
    }else{
        $jabatan_nama ='';
        $jabatan_id =0;
        $jabatan_jenis =4;
        $jfu = 0;
    }
    //$jabatan_nama = $jabatan->jabatan_nama;

    $sopd_nama = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->first();

    $kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
    if($kecamatan){
        $kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
    }else{
        $kab = null;
    }
    //$uk = [''=>''] + App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id')->all();
    $uk = App\Model\UnitKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('unit_kerja_id', $pegawai->unit_kerja_id)->where('status',1)->select('unit_kerja_nama')->first();
    $nama_unit_kerja = $uk ? $uk->unit_kerja_nama : '';
?>
@section('content')
<div class="bs-callout bs-callout-warning hidden">
  <h4>Edit Data Gagal!</h4>
  <p>Data Harus Lengkap!</p>
</div>

<div class="row">
    <div class="col-md-12">
        <center><h5 class="title"><b>EDIT PEGAWAI BERDASARKAN HASIL UJI KOMPETENSI</b></h5></center>
        <br>
        <small class="tampil">*Slide Ke Kiri Untuk Mengisi Semua Form</small>
    </div>
    @if (session('message'))
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
    @endif
</div>
<form action="{{url('evjab/update-pegawai-ujikom',$pegawai->peg_id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="form-edit" data-parsley-validate="" onsubmit="parent.scrollTo(0, 0); return true">
    {!! csrf_field() !!}
    <div class="table-responsive">
    <table class="table no-footer">
        <!-- tinggal part yg ini -->
        <!--
        1. ubah pop up agar ada pilihan edit unit kerja terkecil (done)
        2. hasil pop up disimpan di UI, jadi input type hidden gitu (done)
        3. pas di submit, selain nge save jabatan id baru... pastikan nge save unit kerja id baru juga dan redirec ke halaman ujikom
        -->
        <tr>
          <td>NIP</td>
          <td>:</td>
          <td>{{$pegawai->peg_nip}}
            <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{$uker->unit_kerja_id}}" />
          </td>
        </tr>
        <tr>
          <td>Nama</td>
          <td>:</td>
          <td>{{$pegawai->peg_nama}}</td>
        </tr>
        <tr>
            <?php
            $satker = SatuanKerja::where('satuan_kerja_id',$pegawai->satuan_kerja_id)->first();
            ?>
            <td >Satuan Kerja</td>
            <td>:</td>
            <td>{{$satker->satuan_kerja_nama}}
              <input type="hidden" id="satuan_kerja_id" name="satuan_kerja_id" value="{{$satker->satuan_kerja_id}}" />
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <?php
            $uker = UnitKerja::where('unit_kerja_id',$pegawai->unit_kerja_id)->first();
            ?>
            <td>Unit Kerja Lama</td>
            <td>:</td>
            <td>{{$uker->unit_kerja_nama}}
              <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{$uker->unit_kerja_id}}" />
              <input type="hidden" name="jenis_jabatan" id="jenis_jabatan" value="{{$jabatan_jenis}}">
            </td>
        </tr>                
        <tr>
            <td>Nama Jabatan Lama</td>
            <td>:</td>
            <td>{{$pegawai->jfu_nama}}</td>
        </tr>
        <tr>
            <td>Kelas Jabatan Lama</td>
            <td>:</td>
            <td>{{$pegawai->jfu_kelas}}</td>
        </tr>
        <tr>                 
        <?php
            $ukerEvjab = UnitKerja::where('unit_kerja_id',$pegawai->unit_kerja_id_evjab)->first();            
        ?>
        @include('form.txt',['label'=>'Unit Kerja 2019','required'=>true,'name'=>'unit_kerja_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$ukerEvjab ? $ukerEvjab->unit_kerja_nama : '-','readonly' => true,'maxlength'=>500])                 
        </tr>
        <tr>
        <?php
            $jab = Jabatan::where('unit_kerja_id',$pegawai->unit_kerja_id)->where('satuan_kerja_id',$pegawai->satuan_kerja_id)->whereNotNull('jfu_id')->get(['jfu_id']);
            $id = [];
            $jafungumum = JfuBaru::where('jfu_id',$jfu)->first();
            foreach ($jab as $j) {
                $id[] = $j->jfu_id;
            }
        ?>
        @include('form.txt',['label'=>'Nama Jabatan 2019','required'=>true,'name'=>'jabatan_nama'
                 ,'empty'=>'-Pilih-', 'value'=>$jafungumum ? $jafungumum->jfu_nama : '(belum dipilih)','readonly' => true,'maxlength'=>500])
            
                <input type="hidden" name="jabatan_id" id="jabatan_id" value="{{$pegawai->jabatan_id_evjab}}">
                <input type="hidden" name="peg_jabatan_tmt" id="peg_jabatan_tmt" value="01-01-2019">
                <input type="hidden" name="gol_id_akhir" id="gol_id_akhir" value="{{$pegawai->gol_id_akhir}}">                
                <input type="hidden" name="unit_kerja_id_evjab" id="unit_kerja_id_evjab" value="{{$pegawai->unit_kerja_id_evjab}}">
        </tr>

<tr>
    <td>Kelas Jabatan 2019 *</td>
    <td>:</td>
    <td>
        <input id="info_kelas" type="text" name="info_kelas" value="{{$kelas}}" size="12" maxlength="10" readonly="">
    </td>
    <td>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-jabatan">Update Jabatan</button></td>
</tr>
<tr>
    <td colspan="9" align="center">
        <input type="submit" value="Simpan" name="simpan" id="simpan_data" class="btn btn-xs">
        <a href="{{url('/evjab/search/profile/edit',$pegawai->peg_id)}}" class="btn btn-xs">Batal</a>
    </td>
</tr>
</table>
</div>
</form>

<div class="modal fade" id="modal-jabatan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelPangkat"><div id="modal-button-edit">Update Jabatan</div></h4>
        </div>
        <form id="pangkatForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <div class="bs-callout bs-callout-warning hidden">
              <h4>Edit Data Gagal!</h4>
              <p>Data Harus Lengkap!</p>
            </div>
            <div class="row" id="riw_jabatan_satker_container">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-4">SOPD <span class="required" aria-required="true">*</span></label>
                  <div class="col-md-8">
                    {{$satker->satuan_kerja_nama}}
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row" id="riw_jabatan_uker_container">
              <div class="col-md-12">
                  <div class="form-group">
                        <label class="control-label col-md-4">Unit Kerja <span class="required" aria-required="true">*</span></label>
                        <div class="col-md-8">
                        <select id="riw_jabatan_uker" name="riw_jabatan_uker" style="width:300px">
                        <?php
                            $sat_ker = SatuanKerja::where('satuan_kerja_id', $pegawai->satuan_kerja_id)->where('status',1)->lists('satuan_kerja_nama', 'satuan_kerja_id');
                        ?>
                         <option value="{{$pegawai->unit_kerja_id}}" selected="selected"></option>
                        @foreach($sat_ker as $id_st => $nama_st)
                             <option value="">{{$nama_st}}</option>
                            <?php
                                $uk = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 1)->where('status',1)->lists('unit_kerja_nama', 'unit_kerja_id');
                            ?>
                            @foreach ($uk as $id_unit => $nama_unit)
                                 <option value="{{$id_unit}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit}}</option>
                                 <?php
                                    $uk2 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 2)->where('status',1)->where('unit_kerja_parent', $id_unit)->lists('unit_kerja_nama', 'unit_kerja_id');
                                 ?>
                                @foreach ($uk2 as $id_unit2 => $nama_unit2)
                                    <option value="{{$id_unit2}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit2}}</option>

                                    <?php
                                        $uk3 = UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 3)->where('status',1)->where('unit_kerja_parent', $id_unit2)->lists('unit_kerja_nama', 'unit_kerja_id');
                                    ?>
                                    @foreach ($uk3 as $id_unit3 => $nama_unit3)
                                        <option value="{{$id_unit3}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit3}}</option>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </select>
                        </div>
                  </div>
              </div>
            </div>                        
            <br>
            @include('form.select2_modal',[
            'label'=>'Nama Jabatan 2019',
            'required'=>true,
            'name'=>'riw_jabatan_jab',
            'data'=>App\Model\JfuBaru::select(DB::raw("(jfu_nama ||' | Kelas: '|| jfu_kelas) AS jfu_full, jfu_id"))->where('is_deleted', false)->orderBy('jfu_full')->lists('jfu_full','jfu_id'),
            'empty'=>'-Pilih-',
            'value'=>$jfu
            ])
        </div>
        <div class="modal-footer">
            <input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}">
            <button type="button" id="submit_jabatan" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
$("#riw_jabatan_tglsk").val("").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#riw_jabatan_tmt").val("{{editDate($pegawai->peg_jabatan_tmt)}}").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
$("#submit_jabatan").click(function(){
    var jab = $("#riw_jabatan_jab").val();
    var tempNama = $("#riw_jabatan_jab option[value='"+jab+"']").text();
    var namaJab = tempNama.substr(0, tempNama.indexOf('|')-1);
    var tempKelas = tempNama.substr(tempNama.indexOf('|')+9);
    var uker = $("#riw_jabatan_uker").val();
    var namaUker = $("#riw_jabatan_uker option[value='"+uker+"']").text();
    $("#jabatan_nama").val(namaJab);
      $("#jabatan_id").val(jab);
    $("#no_sk_jabatan").val($("#riw_jabatan_nosk").val());
    $("#tanggal_sk_jabatan").val($("#riw_jabatan_tglsk").val());
    $("#jabatan_penandatangan_jab").val($("#riw_jabatan_jabttd").val());
    $("#tt7").val($("#riw_jabatan_tmt").val());
    $('#modal-jabatan').modal('hide');
    $('#info_kelas').val(tempKelas);
    $("#jabatan_id").val(jab);    
    $('#unit_kerja_nama').val(namaUker.trim());    
    $('#unit_kerja_id_evjab').val(uker);
});

jQuery(function($){
     $("#tt1").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt2").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt3").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt4").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt5").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt6").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#tt7").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
});

$('#riw_jabatan_uker').select2({ width: '500px' }).select2('data', {id: "{{$pegawai->unit_kerja_id_evjab}}", text: "{{$nama_unit_kerja}}"}).on('change', function (e){
    var unit_kerja_id = $('#riw_jabatan_uker').val();    
});

$('#riw_jabatan_satker').select2({ width: '400px' }).on('change', function (e){
    var v = $('#riw_jabatan_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#riw_jabatan_uker").empty();
            $("#riw_jabatan_uker").select2("val", "");
            $('#riw_jabatan_uker').append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#riw_jabatan_uker').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
});

$('#riw_jabatan_jab').select2({ width: '370px' });

$('#riw_jabatan_jenjab').select2({ width: '370px' }).on('change', function (e){
   $("#jabatan_id").empty();
   $("#jabatan_id").select2("val", "");
   var val = $(this).val();
   
});


$('#riw_jabatan_jenjab').on('change',function(){
    $('#riw_jabatan_jab').empty();
    $('#riw_jabatan_jab').val('').trigger('change');
});

$(".quantity").on("keydown", function (e) {
var key   = e.keyCode ? e.keyCode : e.which;

if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
     (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
     (key >= 35 && key <= 40) ||
     (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
     (key >= 96 && key <= 105)
   )) e.preventDefault();
});
</script>
@endsection
