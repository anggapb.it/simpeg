@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
	<div class="row">
    <center><h3 class="title">Rekap Jabatan Pelaksana yang Terisi</h3></center>
    @if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
    @endif
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <hr>
    <br><br>
    <table id="bahasa-table" class="table table-striped table-bordered table-hover">
      <thead>
        <tr class="bg-info">
          <th>No</th>
          <th>Nama Jabatan</th>
          <th>Kelas Jabatan</th>
          <th>Harga Jabatan</th>
        </tr>
      </thead>
      <tbody>
        <?php $nourut = 0; ?>
        @foreach($jfuterisi as $jt)
        <tr>
          <?php $nourut++; ?>
          <td>{{$nourut}}</td>
          <td>{{$jt->jfu_nama}}</td>
          <td>{{$jt->jfu_kelas}}</td>
          <td>{{$jt->nilai_jabatan}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#bahasa-table').DataTable({
              "lengthMenu": [[50, 100, 200, -1], [50, 200, 300, "All"]]
        });
    });
</script>
@endsection
