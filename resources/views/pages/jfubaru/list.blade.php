@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Data Master Jabatan Pelaksana 2019</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<a class="btn btn-primary btn-xs" href="{{url('evjab/jfu/tambah')}}"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Nama Jabatan Pelaksana</a>
<br><br>
  <table id="bahasa-table" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Nama Jabatan</th>
         <th>Jumlah Pegawai</th>
         <th>Action</th>
     </tr>
       </thead>
    <tbody>
    @foreach($jabatan as $k)
    <tr>
        <td>{{$k->jfu_nama}}</td>
        <td>
            <?php
                $jabatan = App\Model\Jabatan::where('jfu_id',$k->jfu_id)->lists('jabatan_id');
                $jumpeg = App\Model\PegawaiEvjab::whereIn('jabatan_id_evjab',$jabatan)->where('peg_status',true)->count();
            ?>
            <a href="#" data-toggle="modal" class="jumpeg" data-target="#myModal" data-id="{{$k->jfu_id}}">{{$jumpeg}}</a>
        </td>
        <td>
            <a href="{{url('evjab/jfu/edit/'.$k->jfu_id)}}" class="btn btn-warning btn-xs">
                <i class="ace-icon fa fa-edit bigger-120"></i> Edit
            </a>
            <a href="{{url('/evjab/jfu/delete',$k->jfu_id)}}"
                class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    Delete
            </a>

        </td>
    </tr>
    @endforeach
   </tbody>
 </table>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pegawai</h4>
      </div>
      <div class="modal-body">
        <table class="table peg table-bordered">
          <thead>
            <tr>
              <th>NIP</th>
              <th>Nama</th>
              <th>Satuan Kerja</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody class="listpeg">

          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#bahasa-table').DataTable({
            "lengthMenu": [[50, 100, 200, -1], [50, 200, 300, "All"]]
        });
    });

    $(document).on('click','.jumpeg',function(){
        var jfu_id = $(this).data('id');
        $.ajax({
            url : "{{url('evjab/jfu/get-pegawai')}}",
            data: {'jfu_id':jfu_id},
            type:'GET',
            success:function(data){
                if(data.length > 0){
                $.each(data,function(key,value){
                  if(key == 0){
                    $('.listpeg').html("<tr><td>"+value.peg_nip+"</td><td>"+value.peg_nama+"</td><td>"+value.satuan_kerja_nama+"</td><td><a class='view-pegawai' href='{{url('evjab/search/profile/edit')}}/"+value.peg_id+"' title='View Pegawai'><span class='fa fa-search'></span></a> <a class='edit-pegawai' href='{{url('evjab/search/edit-pegawai')}}/"+value.peg_id+"' title='Edit Pegawai'><span class='ace-icon glyphicon glyphicon-pencil'></span></a></td></tr>");
                  }else{
                    $('.listpeg').append("<tr><td>"+value.peg_nip+"</td><td>"+value.peg_nama+"</td><td>"+value.satuan_kerja_nama+"</td><td><a class='view-pegawai' href='{{url('evjab/search/profile/edit')}}/"+value.peg_id+"' title='View Pegawai'><span class='fa fa-search'></span></a> <a class='edit-pegawai' href='{{url('evjab/search/edit-pegawai')}}/"+value.peg_id+"' title='Edit Pegawai'><span class='ace-icon glyphicon glyphicon-pencil'></span></a></td></tr>");
                  }
                });
                }else{
                  $('.listpeg').html("<tr><td colspan='4'>Tidak Ada Data Pegawai</td></tr>");
                }
            }
        });
    });
</script>
@endsection
