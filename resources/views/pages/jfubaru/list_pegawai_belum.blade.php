@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JfuBaru;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
	use App\Model\SatuanKerja;
?>
<div class="row">
	<div class="col-xs-12">

		<center><h3 class="title">DAFTAR PEGAWAI<br>BELUM PUNYA JABATAN BARU</h3></center>		
		@if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
	    	<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
	        {{ session('message') }}
	    </div>
		@endif
	 	<hr>
	 	<div class="pull-right tableTools-container">

		<br>
		<br>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
			<th>NAMA/TEMPAT TGL LAHIR</th>
			<th>NIP</th>
			<th>SATUAN KERJA</th>
			<th>JABATAN / KELAS</th>
			<th>JABATAN 2019 / KELAS</th>
			<th>PILIHAN</th>
	     </tr>

	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php
            $satuanKerja = SatuanKerja::where('satuan_kerja_id', $p->satuan_kerja_id)->first();

						$jabatanLama = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
						if($jabatanLama){
							$jfuLama = JabatanFungsionalUmum::where('jfu_id', $jabatanLama['jfu_id'])->first();
							$jabatanLama['jabatan_nama'] = $jfuLama['jfu_nama'];
							$jabatanLamaKelas =  $jfuLama['jfu_kelas'];
	     		  }else{
							$jabatanLama['jabatan_nama'] = 'Pelaksana';
							$jabatanLamaKelas = 5;
						}
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}</a>
	     			@endif
	     			<br>
	     			{{$p->peg_lahir_tempat}},{{$p->peg_lahir_tanggal ? transformDate($p->peg_lahir_tanggal) : '' }}
	     		</td>
	     		<td>
	     			{{$p->peg_nip}}
	     		</td>
					<td>@if($satuanKerja)
						{{$satuanKerja['satuan_kerja_nama']}}
						@endif
					</td>

	     		<td>@if($jabatanLama)
	     			{{$jabatanLama['jabatan_nama']}} ({{$jabatanLamaKelas}})
	     			@endif
	     		</td>

					<td>
							<span style="color:#FF0000; font-weight:bold;">Belum Dipilih (0)</span>
	     		</td>



	     		<td>
		    		<span><a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}" target="_blank" class="view-pegawai"><i class="fa fa-search"></i>View</a></span> <br>
	     			<span><a href="{{url('/evjab/search/edit-pegawai',$p->peg_id)}}" target="_blank" class="edit-pegawai"><i class="ace-icon glyphicon glyphicon-pencil"></i> Edit</a></span> <br>
					<span><a href="{{url('/evjab/search/delete-pegawai',$p->peg_id)}}" onclick="return confirm('Apa Anda Yakin Akan Menghapus Pegawai Ini?')" class="delete-pegawai"><i class="fa fa-trash"></i> Hapus</a></span>
				</td>
	     	</tr>
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 5 }
		  	]
    	});
	});
	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
	    var url = "{{url('evjab/list-unit')}}/"+val;
	    window.location.href= url;
	});
</script>

<script type="text/javascript">
	 setTimeout(function(){
        $("#messageFlash").fadeOut();
    },2000);
</script>

@endsection
