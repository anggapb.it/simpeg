@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JfuBaru;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
	use App\Model\SatuanKerja;
?>
<div class="row">
	<div class="col-xs-12">

		<center><h3 class="title">DAFTAR PEGAWAI<br>TURUN KELAS JABATAN</h3></center>

	 	<hr>
	 	<div class="pull-right tableTools-container">

		<br>
		<br>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
			<th>NAMA/TEMPAT TGL LAHIR</th>
			<th>NIP</th>
			<th>SATUAN KERJA</th>
			<th>JABATAN / KELAS</th>
			<th>JABATAN 2019 / KELAS</th>
			<th>PILIHAN</th>
	     </tr>

	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php
            $satuanKerja = SatuanKerja::where('satuan_kerja_id', $p->satuan_kerja_id)->first();
	     		  $jabatan = Jabatan::where('jabatan_id', $p->jabatan_id_evjab)->first();
	     		  if($jabatan){
							$jfu = JfuBaru::where('jfu_id', $jabatan['jfu_id'])->first();
							$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
							$jabatanKelas = $jfu['jfu_kelas'];
	     		  }else{
							$jabatan['jabatan_nama'] = 'Belum Dipilih';
							$jabatanKelas = 0;
						}

						$jabatanLama = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
						if($jabatanLama){
							$jfuLama = JabatanFungsionalUmum::where('jfu_id', $jabatanLama['jfu_id'])->first();
							$jabatanLama['jabatan_nama'] = $jfuLama['jfu_nama'];
							$jabatanLamaKelas =  $jfuLama['jfu_kelas'];
	     		  }else{
							$jabatanLama['jabatan_nama'] = 'Pelaksana';
							$jabatanLamaKelas = 5;
						}
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}</a>
	     			@endif
	     			<br>
	     			{{$p->peg_lahir_tempat}},{{$p->peg_lahir_tanggal ? transformDate($p->peg_lahir_tanggal) : '' }}
	     		</td>
	     		<td>
	     			{{$p->peg_nip}}
	     		</td>
					<td>@if($satuanKerja)
						{{$satuanKerja['satuan_kerja_nama']}}
						@endif
					</td>

	     		<td>@if($jabatanLama)
	     			{{$jabatanLama['jabatan_nama']}} ({{$jabatanLamaKelas}})
	     			@endif
	     		</td>

					<td>
							<span style="color:#ff8000; font-weight:bold;">{{$jabatan['jabatan_nama']}} ({{$jabatanKelas}})</span>
	     		</td>



	     		<td>
		    		<span><a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}" target="_blank" class="view-pegawai"><i class="fa fa-search"></i>View</a></span> <br>
	     			<span><a href="{{url('/evjab/search/edit-pegawai',$p->peg_id)}}" target="_blank" class="edit-pegawai"><i class="ace-icon glyphicon glyphicon-pencil"></i> Edit</a></span> <br>
				</td>
	     	</tr>
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 5 }
		  	]
    	});
	});
	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
	    var url = "{{url('evjab/list-unit')}}/"+val;
	    window.location.href= url;
	});
</script>
@endsection
