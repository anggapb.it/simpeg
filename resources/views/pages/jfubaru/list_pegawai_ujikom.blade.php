@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JfuBaru;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
?>
<div class="row">
	<div class="col-xs-12">

		<center><h3 class="title">DAFTAR PEGAWAI<br>LULUS UJI KOMPETENSI</h3></center>

	 	<hr>
	 	<div class="pull-right tableTools-container">
		<a href="/evjab/list-pegawai-ujikom/download" class="btn btn-success btn-xs"><i class="ace-icon fa fa-file-excel-o"></i> Export to Excel</a>
		<br>
		<br>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
			<th>NAMA/TEMPAT TGL LAHIR</th>
			<th>NIP</th>
			<th>SATUAN KERJA</th>
			<th>JABATAN (KELAS) / UNIT KERJA</th>
			<th>JABATAN 2019 (KELAS) / UNIT KERJA BARU</th>
			<th>PILIHAN</th>
	     </tr>

	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php
            $satuanKerja = SatuanKerja::where('satuan_kerja_id', $p->satuan_kerja_id)->first();
                   $jabatan = Jabatan::where('jabatan_id', $p->jabatan_id_evjab)->first();
                   $unitKerja = UnitKerja::where('unit_kerja_id', $p->unit_kerja_id)->first();
                   $unitKerjaBaru = UnitKerja::where('unit_kerja_id', $p->unit_kerja_id_evjab)->first();
	     		  if($jabatan){
							$jfu = JfuBaru::where('jfu_id', $jabatan['jfu_id'])->first();
							$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
							$jabatanKelas = $jfu['jfu_kelas'];
	     		  }else{
							$jabatan['jabatan_nama'] = 'Belum Dipilih';
							$jabatanKelas = 0;
						}

						$jabatanLama = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
						if($jabatanLama){
							$jfuLama = JabatanFungsionalUmum::where('jfu_id', $jabatanLama['jfu_id'])->first();
							$jabatanLama['jabatan_nama'] = $jfuLama['jfu_nama'];
							$jabatanLamaKelas =  $jfuLama['jfu_kelas'];
	     		  }else{
							$jabatanLama['jabatan_nama'] = 'Pelaksana';
							$jabatanLamaKelas = 5;
						}
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/evjab/search/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}</a>
	     			@endif
	     		</td>
	     		<td>
	     			{{$p->peg_nip}}
	     		</td>
					<td>@if($satuanKerja)
						{{$satuanKerja['satuan_kerja_nama']}}
						@endif
					</td>

	     		<td>@if($jabatanLama)
	     			{{$jabatanLama['jabatan_nama']}} ({{$jabatanLamaKelas}})
	     			@endif
                    @if($unitKerja)
					<br>pada<br>{{$unitKerja['unit_kerja_nama']}}
					@endif
	     		</td>

				<td>
                    @if($jabatan['jabatan_nama'] == 'Belum Dipilih')
	     			<span style="color:red; font-weight:bold;">{{$jabatan['jabatan_nama']}} ({{$jabatanKelas}})</span>
						@else
							@if($jabatanKelas > $jabatanLamaKelas)
							<span style="color:#008800; font-weight:bold;">{{$jabatan['jabatan_nama']}} ({{$jabatanKelas}})</span>
							@elseif($jabatanKelas == $jabatanLamaKelas)
							<span style="color:#0000ff; font-weight:bold;">{{$jabatan['jabatan_nama']}} ({{$jabatanKelas}})</span>
							@else
							<span style="color:#ff8000; font-weight:bold;">{{$jabatan['jabatan_nama']}} ({{$jabatanKelas}})</span>
							@endif
	     			@endif
                    @if($unitKerjaBaru)
					<br>pada<br>{{$unitKerjaBaru['unit_kerja_nama']}}
					@endif
	     		</td>



	     		<td>
		    		<span><a href="{{url('/evjab/search/profile-ujikom/edit',$p->peg_id)}}" target="_blank" class="view-pegawai"><i class="fa fa-search"></i>View</a></span> <br>
	     			<span><a href="{{url('/evjab/search/edit-pegawai-ujikom',$p->peg_id)}}" target="_blank" class="edit-pegawai"><i class="ace-icon glyphicon glyphicon-pencil"></i> Edit</a></span> <br>
				</td>
	     	</tr>
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 5 }
		  	]
    	});
	});
	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
	    var url = "{{url('evjab/list-unit')}}/"+val;
	    window.location.href= url;
	});
</script>
@endsection
