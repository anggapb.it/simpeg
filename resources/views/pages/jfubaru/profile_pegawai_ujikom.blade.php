@extends('layouts.app')

@section('content')
<?php
	use App\Model\Agama;
	use App\Model\Kecamatan;
	use App\Model\Kabupaten;
	use App\Model\Propinsi;
	use App\Model\GolonganDarah;
	use App\Model\KategoriPendidikan;
	use App\Model\Pendidikan;
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\JfuBaru;
	use App\Model\Eselon;
	use App\Model\Gaji;
	use App\Model\GajiTahun;
	use App\Model\StatusEditPegawai;
	use App\Model\StatusEditPegawaiLog;
	use App\Model\StatusKepegawaian;
	use App\Model\PegawaiEvjab;
	use App\Model\PegawaiPensiun;
	use App\Model\SatuanKerja;
	use App\Model\UnitKerja;
	use App\Model\Kedudukan;

	$kedudukan = Kedudukan::all();
	$submit = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 2)->orderBy('id','desc')->first();
	$revisi = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 3)->orderBy('id','desc')->first();
	$draft = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 1)->orderBy('id','desc')->first();
	$terima = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 4)->orderBy('id','desc')->first();
	$usulskpd = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 5)->orderBy('id','desc')->first();
	$revisiskpd = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 6)->orderBy('id','desc')->first();
	$pensiun = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id','desc')->first();
	$st_pensiun = null;
	$st_pensiun_batal = null;

	if($pensiun && $pensiun['action'] == 'pensiun_pegawai'){
		$st_pensiun = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pensiun_pegawai')->first();
	}
	if($pensiun && $pensiun['action'] == 'undo_pensiun_pegawai'){
		$st_pensiun_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pensiun_pegawai')->first();
	}


	$st_meninggal = null;
	$st_meninggal_batal = null;
	$st_pindah = null;
	$st_pindah_batal = null;

	if($pensiun && $pensiun['action'] == 'pegawai_meninggal'){
		$st_meninggal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pegawai_meninggal')->first();
	}
	if ($pensiun && $pensiun['action'] == 'undo_pegawai_meninggal') {
		$st_meninggal_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pegawai_meninggal')->first();
	}
	if($pensiun && $pensiun['action'] == 'pindah_pegawai'){
		$st_pindah = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pindah_pegawai')->first();
	}
	if ($pensiun && $pensiun['action'] == 'undo_pindah_pegawai') {
		$st_pindah_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pindah_pegawai')->first();
	}

	//dd($st_pensiun_batal);
	//DB SKPD
	if($st_pensiun){
		$peg_pensiun = PegawaiPensiun::where('peg_id',$pegawai->peg_id)->where('pensiun_id',1)->first();
	}
	if($pegawai->unit_kerja_id){
		$uker=UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id)->first();
	}else{
		$uker =null;
    }
    if($pegawai->unit_kerja_id){
        $uker_evjab = UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id)->first();
    }else{
        $uker_evjab = null;
    }
	$agama = Agama::where('id_agama', $pegawai->id_agama)->first();
	$goldar = GolonganDarah::where('id_goldar', $pegawai->id_goldar)->first();
	$kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
		if($kecamatan){
			$kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
		}else{
			$kab = null;
		}

	$pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
	$pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
		if($pend_awal){
			$kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first();
		}else{
			$kat_pend_awal = null;
		}
		if($pend_akhir){
			$kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first();
		}else{
			$kat_pend_akhir = null;
		}

	$golongan_awal = Golongan::where('gol_id', $pegawai->gol_id_awal)->first();
	$golongan_akhir = Golongan::where('gol_id', $pegawai->gol_id_akhir)->first();

	$jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id)->first();
	if($jabatan){
		$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
		$jabatanKelasLama = 5;
		if($jfu){
			$jabatan->jabatan_nama = $jfu['jfu_nama'];
			$jabatanKelasLama = $jfu['jfu_kelas'];
		}else{
			$jabatan->jabatan_nama = 'Pelaksana';
		}
	}

	$jabatanEvjab = Jabatan::where('jabatan_id', $pegawai->jabatan_id_evjab)->first();
	if($jabatanEvjab){
		$jfuBaru = JfuBaru::where('jfu_id', $jabatanEvjab->jfu_id)->first();
		$jfuBaruNama = $jfuBaru['jfu_nama'];
		$jabatanKelasBaru = $jfuBaru['jfu_kelas'];
	}else{
		$jfuBaruNama = '(belum dipilih)';
		$jabatanKelasBaru = '0';
	}
	$unitKerjaEvjab = UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id_evjab)->first();
	if($unitKerjaEvjab){
		$unitKerjaBaruNama = $unitKerjaEvjab['unit_kerja_nama'];
	} else {
		$unitKerjaBaruNama = '-';
	}

	$eselon = 0;
	if($pegawai->peg_kerja_tahun == '-'){
			$pegawai->peg_kerja_tahun == 0;
	}
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $pegawai->gol_id_akhir)->where('gaji_masakerja', $pegawai->peg_kerja_tahun)->first();

	$id = $pegawai->peg_id;
	//dd($pegawai);
	if($pegawai->peg_nama == null){
		$pegawai->peg_nama = '';
	}

	if($pegawai->peg_gelar_depan == ""){
		$pegawai->peg_gelar_depan = '';
	}

	if($pegawai->peg_gelar_belakang == ""){
		$pegawai->peg_gelar_belakang = '';
	}

	if($pegawai->peg_nip == null){
		$pegawai->peg_nip = '';
	}

	if($pegawai->peg_nip_lama == ""){
		$pegawai->peg_nip_lama = '';
	}

	if($pegawai->peg_lahir_tempat == ""){
		$pegawai->peg_lahir_tempat = '';
	}

	if($pegawai->peg_lahir_tanggal == null){
		$pegawai->peg_lahir_tanggal = '';
	}

	if($pegawai->peg_jenis_kelamin == null){
		$pegawai->peg_jenis_kelamin = '';
	}
	if($pegawai->id_agama == null){
		$pegawai->id_agama = '';
	}

	if($pegawai->peg_status_perkawinan == null){
		$pegawai->peg_status_perkawinan = '';
	}

	if($pegawai->peg_status_kepegawaian == null){
		$pegawai->peg_status_kepegawaian = '';
	}
	if($pegawai->peg_cpns_tmt == null){
		$pegawai->peg_cpns_tmt = '';
	}
	if($pegawai->peg_pns_tmt == null){
		$pegawai->peg_pns_tmt = '';
	}
	if($pegawai->id_pend_awal == null){
		$pegawai->id_pend_awal = '';
	}
	if($pegawai->peg_pend_awal_th == null){
		$pegawai->peg_pend_awal_th = '';
	}
	if($pegawai->id_pend_akhir == null){
		$pegawai->id_pend_akhir = '';
	}
	if($pegawai->peg_pend_akhir_th == null){
		$pegawai->peg_pend_akhir_th = '';
	}
	if($pegawai->jabatan_id == null){
		$pegawai->jabatan_id = '';
	}
	if($pegawai->peg_jabatan_tmt == null){
		$pegawai->peg_jabatan_tmt = '';
	}
	if($pegawai->peg_instansi_dpk == null){
		$pegawai->peg_instansi_dpk = '';
	}
	if($pegawai->gol_id_awal == null){
		$pegawai->gol_id_awal = '';
	}
	if($pegawai->peg_gol_awal_tmt == null){
		$pegawai->peg_gol_awal_tmt = '';
	}
	if($pegawai->gol_id_akhir == null){
		$pegawai->gol_id_akhir = '';
	}
	if($pegawai->peg_gol_akhir_tmt == null){
		$pegawai->peg_gol_akhir_tmt = '';
	}
	if($pegawai->peg_kerja_tahun == null){
		$pegawai->peg_kerja_tahun = 0;
	}
	if($pegawai->peg_kerja_bulan == null){
		$pegawai->peg_kerja_bulan = '';
	}
	if($pegawai->peg_karpeg == ""){
		$pegawai->peg_karpeg = '';
	}
	if($pegawai->peg_karsutri == ""){
		$pegawai->peg_karsutri = '';
	}
	if($pegawai->peg_no_askes == null){
		$pegawai->peg_no_askes = '';
	}
	if($pegawai->peg_ktp == null){
		$pegawai->peg_ktp = '';
	}
	if($pegawai->peg_npwp == null){
		$pegawai->peg_npwp = '';
	}
	if($pegawai->id_goldar == null){
		$pegawai->id_goldar = '';
	}
	if($pegawai->peg_bapertarum == null){
		$pegawai->peg_bapertarum = '';
	}
	if($pegawai->peg_tmt_kgb == null){
		$pegawai->peg_tmt_kgb = '';
	}
	if($pegawai->peg_rumah_alamat == null){
		$pegawai->peg_rumah_alamat = '';
	}
	if($pegawai->peg_kel_desa == ""){
		$pegawai->peg_kel_desa = '';
	}
	if($pegawai->peg_kodepos == ""){
		$pegawai->peg_kodepos = '';
	}
	if($pegawai->peg_telp == ""){
		$pegawai->peg_telp = '';
	}
	if($pegawai->peg_telp_hp == ""){
		$pegawai->peg_telp_hp = '';
	}
	if($pegawai->peg_email == ""){
		$pegawai->peg_email = '';
	}
	if($pegawai->satuan_kerja_id == null){
		$pegawai->satuan_kerja_id = '';
	}
	if($pegawai->kecamatan_id == null){
		$pegawai->kecamatan_id = '';
	}

	if($pegawai->peg_status_asn == null){
		$pegawai->peg_status_asn = '';
	}

	if($pegawai->peg_status_gaji == null){
		$pegawai->peg_status_gaji = '';
	}

	if($pegawai->id_status_kepegawaian == null){
		$pegawai->id_status_kepegawaian = '';
	}


	//db bkd
	$pegawai_bkd= PegawaiEvjab::where('peg_id', $pegawai->peg_id)->first();

	if($pegawai_bkd){
		$agama_bkd = Agama::where('id_agama', $pegawai_bkd->id_agama)->first();
		$goldar_bkd = GolonganDarah::where('id_goldar', $pegawai_bkd->id_goldar)->first();
		if($pegawai_bkd->unit_kerja_id){
			$uker_bkd=UnitKerja::where('unit_kerja_id', $pegawai_bkd->unit_kerja_id)->first();
		}else{
			$uker_bkd =null;
        }
        if($pegawai_bkd->unit_kerja_id_evjab){
            $uker_bkd_evjab = UnitKerja::where('unit_kerja_id', $pegawai_bkd->unit_kerja_id_evjab)->first();
        }else{
            $uker_bkd_evjab = null;
        }

		$kecamatan_bkd = Kecamatan::where("kecamatan_id", $pegawai_bkd->kecamatan_id)->first();
			if($kecamatan_bkd){
				$kab_bkd = Kabupaten::where('kabupaten_id', $kecamatan_bkd->kabupaten_id)->first();
			}else{
				$kab_bkd = null;
			}

		$pend_awal_bkd = Pendidikan::where('id_pend', $pegawai_bkd->id_pend_awal)->first();
		$pend_akhir_bkd = Pendidikan::where('id_pend', $pegawai_bkd->id_pend_akhir)->first();
			if($pend_awal_bkd){
				$kat_pend_awal_bkd = KategoriPendidikan::where('kat_pend_id', $pend_awal_bkd->kat_pend_id)->first();
			}else{
				$kat_pend_awal_bkd= null;
			}
			if($pend_akhir_bkd){
				$kat_pend_akhir_bkd = KategoriPendidikan::where('kat_pend_id', $pend_akhir_bkd->kat_pend_id)->first();
			}else{
				$kat_pend_akhir_bkd = null;
			}

		$golongan_awal_bkd = Golongan::where('gol_id', $pegawai_bkd->gol_id_awal)->first();
		$golongan_akhir_bkd = Golongan::where('gol_id', $pegawai_bkd->gol_id_akhir)->first();

		$jabatan_bkd = Jabatan::where('jabatan_id', $pegawai_bkd->jabatan_id)->first();
			if($jabatan_bkd){
				$eselon_bkd = Eselon::where('eselon_id', $jabatan_bkd['eselon_id'])->first();
				if($jabatan_bkd->jabatan_jenis == 3){
					$jf = JabatanFungsional::where('jf_id', $jabatan_bkd['jf_id'])->first();
					$jabatan_bkd->jabatan_nama = $jf['jf_nama'];
				}elseif($jabatan_bkd->jabatan_jenis == 4){
					$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan_bkd['jfu_id'])->first();
					if($jfu){
						$jabatan_bkd->jabatan_nama = $jfu['jfu_nama'];
					}else{
						$jabatan_bkd->jabatan_nama = 'Pelaksana';
					}
				}
			}else{
				$eselon_bkd = null;
			}
		if($pegawai_bkd->peg_kerja_tahun == '-'){
			$pegawai_bkd->peg_kerja_tahun == 0;
		}
		$gaji_bkd = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $pegawai_bkd->gol_id_akhir)->where('gaji_masakerja', $pegawai_bkd->peg_kerja_tahun)->first();
		$satker_bkd = SatuanKerja::where('satuan_kerja_id', $pegawai_bkd->satuan_kerja_id)->first();
		if($pegawai_bkd->peg_nama == null){
			$pegawai_bkd->peg_nama = '';
		}

		if($pegawai_bkd->peg_gelar_depan == ''){
			$pegawai_bkd->peg_gelar_depan = '';
		}

		if($pegawai_bkd->peg_gelar_belakang == ''){
			$pegawai_bkd->peg_gelar_belakang = '';
		}

		if($pegawai_bkd->peg_nip == null){
			$pegawai_bkd->peg_nip = '';
		}

		if($pegawai_bkd->peg_nip_lama == null){
			$pegawai_bkd->peg_nip_lama = '';
		}

		if($pegawai_bkd->peg_lahir_tempat == null){
			$pegawai_bkd->peg_lahir_tempat = '';
		}

		if($pegawai_bkd->peg_lahir_tanggal == null){
			$pegawai_bkd->peg_lahir_tanggal = '';
		}

		if($pegawai_bkd->peg_jenis_kelamin == null){
			$pegawai_bkd->peg_jenis_kelamin = '';
		}

		if($pegawai_bkd->peg_status_perkawinan == null){
			$pegawai_bkd->peg_status_perkawinan = '';
		}
		if($pegawai_bkd->id_agama == null){
			$pegawai_bkd->id_agama = '';
		}

		if($pegawai_bkd->peg_status_kepegawaian == null){
			$pegawai_bkd->peg_status_kepegawaian = '';
		}
		if($pegawai_bkd->peg_cpns_tmt == null){
			$pegawai_bkd->peg_cpns_tmt = '';
		}
		if($pegawai_bkd->peg_pns_tmt == null){
			$pegawai_bkd->peg_pns_tmt = '';
		}
		if($pegawai_bkd->id_pend_awal == null){
			$pegawai_bkd->id_pend_awal = '';
		}
		if($pegawai_bkd->peg_pend_awal_th == null){
			$pegawai_bkd->peg_pend_awal_th = '';
		}
		if($pegawai_bkd->id_pend_akhir == null){
			$pegawai_bkd->id_pend_akhir = '';
		}
		if($pegawai_bkd->peg_pend_akhir_th == null){
			$pegawai_bkd->peg_pend_akhir_th = '';
		}
		if($pegawai_bkd->jabatan_id == null){
			$pegawai_bkd->jabatan_id = '';
		}
		if($pegawai_bkd->peg_jabatan_tmt == null){
			$pegawai_bkd->peg_jabatan_tmt = '';
		}
		if($pegawai_bkd->peg_instansi_dpk == null){
			$pegawai_bkd->peg_instansi_dpk = '';
		}
		if($pegawai_bkd->gol_id_awal == null){
			$pegawai_bkd->gol_id_awal = '';
		}
		if($pegawai_bkd->peg_gol_awal_tmt == null){
			$pegawai_bkd->peg_gol_awal_tmt = '';
		}
		if($pegawai_bkd->gol_id_akhir == null){
			$pegawai_bkd->gol_id_akhir = '';
		}
		if($pegawai_bkd->peg_gol_akhir_tmt == null){
			$pegawai_bkd->peg_gol_akhir_tmt = '';
		}
		if($pegawai_bkd->peg_kerja_tahun == null){
			$pegawai_bkd->peg_kerja_tahun = 0;
		}
		if($pegawai_bkd->peg_kerja_bulan == null){
			$pegawai_bkd->peg_kerja_bulan = '';
		}
		if($pegawai_bkd->peg_karpeg == null){
			$pegawai_bkd->peg_karpeg = '';
		}
		if($pegawai_bkd->peg_karsutri == null){
			$pegawai_bkd->peg_karsutri = '';
		}
		if($pegawai_bkd->peg_no_askes == null){
			$pegawai_bkd->peg_no_askes = '';
		}
		if($pegawai_bkd->peg_ktp == null){
			$pegawai_bkd->peg_ktp = '';
		}
		if($pegawai_bkd->peg_npwp == null){
			$pegawai_bkd->peg_npwp = '';
		}
		if($pegawai_bkd->id_goldar == null){
			$pegawai_bkd->id_goldar = '';
		}
		if($pegawai_bkd->peg_bapertarum == null){
			$pegawai_bkd->peg_bapertarum = '';
		}
		if($pegawai_bkd->peg_tmt_kgb == null){
			$pegawai_bkd->peg_tmt_kgb = '';
		}
		if($pegawai_bkd->peg_rumah_alamat == null){
			$pegawai_bkd->peg_rumah_alamat = '';
		}
		if($pegawai_bkd->peg_kel_desa == null){
			$pegawai_bkd->peg_kel_desa = '';
		}
		if($pegawai_bkd->peg_kodepos == null){
			$pegawai_bkd->peg_kodepos = '';
		}
		if($pegawai_bkd->peg_telp == null){
			$pegawai_bkd->peg_telp = '';
		}
		if($pegawai_bkd->peg_telp_hp == null){
			$pegawai_bkd->peg_telp_hp = '';
		}
		if($pegawai_bkd->peg_email == null){
			$pegawai_bkd->peg_email = '';
		}
		if($pegawai_bkd->satuan_kerja_id == null){
			$pegawai_bkd->satuan_kerja_id = '';
		}
		if($pegawai_bkd->kecamatan_id == null){
			$pegawai_bkd->kecamatan_id = '';
		}

		if($pegawai_bkd->peg_status_asn == null){
			$pegawai_bkd->peg_status_asn = '';
		}

		if($pegawai_bkd->peg_status_gaji == null){
			$pegawai_bkd->peg_status_gaji = '';
		}

		if($pegawai_bkd->id_status_kepegawaian == null){
			$pegawai_bkd->id_status_kepegawaian = '';
		}

	}

	$kepsek = false;
	if($jabatan){
		if (strpos(strtolower($jabatan->jabatan_nama), 'kepala sekolah') !== false) {
	    	$kepsek = true;
		}
	}elseif($jabatan_bkd){
		if (strpos(strtolower($jabatan_bkd->jabatan_nama), 'kepala sekolah') !== false) {
	    	$kepsek = true;
		}
	}

	$field = ["peg_status","peg_ketstatus"];

	$sama = true;
	if($pegawai && $pegawai_bkd){
		foreach ($field as $key) {
			if($pegawai->{$key} != $pegawai_bkd->{$key}){
				$sama = false;
			}
		}
	}
?>
<!-- #section:elements.tab.option -->
<div class="tabbable">
	<ul class="nav nav-tabs" id="myTab">
		@include('includes.nav_pegawai_evjab')
	</ul>

	<div class="tab-content">
		<!--DATA PRIBADI-->
		<div id="data-pribadi" class="tab-pane in active">
			<div id="user-profile" class="user-profile row">
				@include('includes.profile')
				<br><br><br>
				<div class="col-xs-12">
				<div class="pull-left tableTools-container">
					<span style="padding-left:10px;">
					<a href="{{url('evjab/search/edit-pegawai-ujikom',$pegawai['peg_id'])}}" class="btn btn-warning btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Edit</a>
					</span>
				</div>
				</div>
				@include('pages.jfubaru.data_pribadi_evjab')
			</div>
		</div>
	</div>
</div>

@endsection
