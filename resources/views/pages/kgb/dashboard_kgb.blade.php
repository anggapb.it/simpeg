@extends('layouts.app')

@section('content')
<?php
	/* use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai; */
?>
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app">
	<div class="col-xs-12">
		@if(Auth::user()->role_id == '3' || Auth::user()->role_id == '1' || Auth::user()->role_id == '6')
		<center>
			<div class="form-group row">
			
				<div class="col-lg-12">
					<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker_dashkgb" v-on:change="filterSatker()">
						<option value="">Pilih Satuan Kerja</option>
						@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
				</div>
		</center>
		@endif
		<center><h5 class="title">DAFTAR STATUS USULAN KGB <br> SATUAN KERJA : <span id="satuan_kerja_nama">{{ $satker->satuan_kerja_nama }}</span></h5></center>
	
		<hr>
	 
		
		
	  <table id="tabel-list-pegawai" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
	     <thead>
	     <tr class="bg-info">
			<th>STATUS</th>
			<th>JUMLAH</th>
			<th>AKSI</th>
	     </tr>
	     </thead>
	     <tbody>
			
		 	
	     	<tr v-for="(index, item) in items">
			 	{{-- <td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td> --}}
	     		<td>
	     			@{{item.nama_status}}
	     		</td>
	     		<td>
					<center>@{{item.jumlah}}</center>
				 </td>	
				 
				<td v-if="item.jumlah > 0"><a href="{{url('/kgb/inbox-kgb','')}}">Lihat Detail</a></td>
				<td v-else="item.jumlah = 0"></td>
				 
	     	</tr>
	     </tbody>
	 </table>
	
		<div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
	</div>
</div>
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
        </div>
		<div class="modal-body">
			<div id="profile" class="user-profile row">
			</div>
			<div id="content_result" class="user-profile row">
			</div>
		</div>
        <div class="modal-footer">
            <button id="submitBahasa" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
	// 	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	//     var val = $("#satuan_kerja_id option:selected").val();
	//     var url = "{{url('kgb/dashboard')}}/"+val;
	//     window.location.href= url;
	// });
		
		loadState();
		getData();

		// $('#tanggal_from').datepicker({
		// 	format: "MM-yyyy",
		// 	startView: "months", 
		// 	minViewMode: "months",
		// 	autoclose: true
		// });

		// $('#tanggal_to').datepicker({
		// 	format: "MM-yyyy",
		// 	startView: "months", 
		// 	minViewMode: "months",
		// 	autoclose: true
		// });

		// $('#tanggal_to').on('changeDate', function (ev) {
		// 	var tgl_from = $('#tanggal_from').val();
		// 	var tgl_to = $('#tanggal_to').val();

		// 	vm.periode_awal = tgl_from;
		// 	vm.periode_akhir = tgl_to;

		// 	if(tgl_from != null && tgl_from != ""){
		// 	if(tgl_to<tgl_from){
		// 		alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
		// 		$('#tanggal_to').val("");
		// 	}
		// 	}
		// });

		// $('#tanggal_from').on('changeDate', function (ev) {
		// 	var tgl_from = $('#tanggal_from').val();
		// 	var tgl_to = $('#tanggal_to').val();
			
		// 	vm.periode_awal = tgl_from;
		// 	vm.periode_akhir = tgl_to;

		// 	if(tgl_to != null && tgl_to != ""){
		// 	if(tgl_to<tgl_from){
		// 		alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
		// 		$('#tanggal_from').val("");
		// 	}
		// 	}
		// });
		
	});

	function check_all() {
        var table= $('#checked_all').closest('#tabel-list-pegawai');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function proses()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_proses"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan Ceklist terlebih dahulu pegawai yang akan KGB!", "error");
			return false;
		}

		swal({
			title: "Apakah akan proses KGB pada periode ini?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$('#prosesKGB').button('loading');

				vm.isLoading = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					// alert($('#peg_id'+no).val()+'='+no);
					promise = $.ajax({
								type: "POST",
								cache: false,
								url: "{{ URL::to('kgb/proses-kgb') }}/",
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {peg_id:$('#peg_id'+no).val()},
								dataType: 'json',
								success: function(data) {
									
								}
							})/* .error(function (e){
								console.log(e);return false;
								swal("Error!", "", "error");
							}); */

							promise.done(function(msg)
							{
								console.log("successfully updated"); 
								successCounter++;
							});

							promise.fail(function(jqXHR) { console.log(jqXHR); });

						promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vm.isLoading = false;
					$('#prosesKGB').button('reset');
					swal("Success!", "Berhasil Proses KGB!", "success");
					$('#checked_all').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		
	}

	var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			search_satker_dashkgb: '',
			search_uker: '',
			search_golongan: [],
			search_input_golongan: [],
			search_eselon: [],
			search_input_eselon: [],
			search_kelas_jabatan: '',
			search_jenjab: '',
		},
		methods: {
			clearFilter: function () {
				this.search_satker_dashkgb = '';
				this.search_uker = '';
				this.search_golongan = [];
				this.search_eselon = [];
				this.search_kelas_jabatan = '';
      			this.search_jenjab = '';
				this.search_input_golongan = [];
				this.search_input_eselon = [];
				
				$('#tanggal_from').val('');
				$('#tanggal_to').val('');
				/* $('#m_eselon').select2('val','');
				$('#m_golongan').select2('val','');
				$('#m_kelas_jabatan').select2('val','');
				$('#m_uker').select2('val',''); */
				
				this.changePage();
			},
			filterSatker : function() {
				this.search_satker_dashkgb = $('select[name=m_satker] option:selected').val();
				getData();
			}
		}
		});
	
	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/dashboard-kgb')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				search_satker_dashkgb: vm.search_satker_dashkgb,
				search_uker: vm.search_uker,
				search_golongan: vm.search_golongan,
				search_eselon: vm.search_eselon,
				search_kelas_jabatan: vm.search_kelas_jabatan,
				search_jenjab: vm.search_jenjab
			},
			function(data) {
				vm.items = data.list_status_usulan;
				vm.isLoading = false;
				// console.log(data.count[0].count);
				saveState();
				if(vm.search_satker_dashkgb)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	

	var page = 'prediksi_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			search_satker_dashkgb: vm.search_satker_dashkgb,
			search_uker: vm.search_uker,
			search_golongan: vm.search_golongan,
			search_input_golongan: vm.search_input_golongan,
			search_eselon: vm.search_eselon,
			search_input_eselon: vm.search_input_eselon,
			search_kelas_jabatan: vm.search_kelas_jabatan,
			search_jenjab: vm.search_jenjab
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal) {
                stored.periode_awal = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
			vm.search_satker_dashkgb = stored.search_satker_dashkgb;
			vm.search_uker = stored.search_uker;
			vm.search_golongan = stored.search_golongan;
			vm.search_input_golongan = stored.search_input_golongan;
			vm.search_eselon = stored.search_eselon;
			vm.search_input_eselon = stored.search_input_eselon;
			vm.search_kelas_jabatan = stored.search_kelas_jabatan;
			vm.search_jenjab = stored.search_jenjab;
		}
	}

	/* function getSKP(id) {
			$.ajax({
				type: "GET",
				cache: false,
				url: "{{ URL::to('skp-kgb') }}/"+id,
				beforeSend: function (xhr) {
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				data: {"id":id},
				dataType: 'json',
				success: function(data) {
					$('#profile').html(data.view_1);
					$('#content_result').html(data.view_2);
				}
			}).error(function (e){
				console.log(e);
				alert("ERROR!");
			});		
	} */

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});
</script>
@endsection