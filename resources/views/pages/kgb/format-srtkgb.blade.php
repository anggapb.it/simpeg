@extends('layouts.app')
<?php
  $satker = Auth::user()->satuan_kerja_id;
  $satuan_kerja = App\Model\SatuanKerja::where('satuan_kerja_id', $satker)->first();
?>
@section('content')
<center><h3>Format Surat KGB</h3><br></center>
@if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
@endif
<div id="vm" class="row">
   
    <div class="col-xs-12">
        <div class="animated fadeIn pull-right" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
        <div id="isi">
        <table class="table table-striped table-bordered responsive" id="data-list">
            <thead>
                <tr>
                    <th valign="absmiddle">Template Surat KGB</th>
                    <th valign="absmiddle">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items">
                    <td >
                      <center><a v-text="item[1]" 
                         v-bind:href="'{{url('kgb/download-srt-kgb')}}/'+item[1]">
                      </a></center>
                    </td>
                    <td>
                      <a class="btn btn-xs btn-success" 
                         v-bind:href="'{{url('kgb/download-srt-kgb')}}/'+item[1]">
                        <i class="fa fa-download"></i>
                      </a>
                      <a href=""
                          data-toggle="modal" data-target="#modal-upload-srt"
                          v-bind:data-id="item[0]"
                          v-bind:data-template="item[1]"
                          onclick="editUploadTemp(this)" class="btn btn-xs btn-warning">
                         <i class="fa fa-pencil"></i>
                      </a>
                    </td>
                </tr>
                <tr v-if="items.length <= 0">
                    <td colspan="8">Data tidak tersedia</td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
</div>
<div class="modal fade" id="modal-upload-srt" tabindex="-1" role="dialog" aria-labelledby="modal-upload-srt" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title">Form Upload Format Surat KGB</h4>
    </div>
    <form action="{{url('/kgb/update-template-srt')}}" id="uploadSrtKgbForm" method="POST" class="form-horizontal"  enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-body" id="modal-detail-content">
        <input type="hidden" name="id" id="format_srt_id">
        <div class="row" >
        <div class="col-md-12">
          <div class="form-group">
            <label class="control-label col-md-4">Upload Template Surat KGB (max 2 MB) <span class="required" aria-required="true">*</span></label>
            <div class="col-md-8">
              <input type="file" name="template" id="template" class="form-control" style="font-size:14px;">
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-xs">Simpan</button>
      </div>
    </form>
  </div>
  </div>
</div>

@endsection
@section('styles')
<style>
thead {
    text-align: center;
}
</style>
@endsection
@section('scripts')
@include('scripts.head')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">

var vm = new Vue({
  el: '#vm',
  data: {
    tanggal: {
        from: "",
        to: ""
    },
    isLoading: false,
    items: [],
    pegawai: {},
    userid:{},
    tpp: '',
    total:'',
    search: '',
    search_input: '',
    bulan: '',
    satuan_kerja: {{ Auth::user()->satuan_kerja_id }},
    satuan_organisasi: '',
    unit_kerja: '',
    hari_kerja: 0,
  },
  methods: {
    deleteData: function(id) {
      if (!confirm("Apakah anda yakin?")) return;
      $.getJSON('{{url('manual/pengajuan/destroy')}}?id='+id,
        function(result) {
          if (result.success) {
            notify(result.message);
            getData();
          }
      });
    },
    approve: function(id) {
      if (!confirm("Apakah anda yakin?")) return;
      $.getJSON('{{url('manual/pengajuan/update-status')}}?id='+id+'&status=1',
        function(result) {
          if (result.success) {
            notify(result.message);
            getData();
          }
      });
    },
    doSearch: function () {
      getData();
    }
  }
});

@if (Auth::user()->role_id == 1)
  $('#m_satuan_kerja_id,#m_satuan_organisasi_id,#m_unit_kerja_id,#m_peg_nip').select2({ width: '100%' });
@else
  $('#m_satuan_organisasi_id,#m_unit_kerja_id,#m_peg_nip').select2({ width: '100%' });
@endif

$(document).on('change','#satuan_kerja_id',function(){
  vm.satuan_kerja = $(this).val();
  vm.satuan_organisasi = '';
  vm.unit_kerja = '';
  getData();
});
$(document).on('change','#satuan_organisasi_id',function(){
  vm.satuan_organisasi = $(this).val();
  vm.unit_kerja = '';
  getData();
});
$(document).on('change','#unit_kerja_id',function(){
  vm.unit_kerja = $(this).val();
  getData();
});


getData();
function getData() {
  vm.isLoading = true;
  $.getJSON("{{url('kgb/get-data')}}",
//   {
//     satuan_kerja: vm.satuan_kerja,
//     satuan_organisasi: vm.satuan_organisasi,
//     unit_kerja: vm.unit_kerja,
//     bulan: vm.bulan,
//   },
  function(data) {
    vm.items = data;
    vm.isLoading = false;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
  });
}
function create() {
  $("#format_srt_id").val(null);
  $("#modal-upload-srt").modal('show');
  $("#tanggal_from").val('');
  $("#tanggal_to").val('');
  $("#lampiran").attr('required','true');
}

var editUploadTemp = function(e){
    var id= $(e).data('id');
  
    $("#uploadSrtKgbForm").attr("action","{{ URL::to('kgb/update-template-srt') }}");
    $("#format_srt_id").val(id);
    $("#lampiran").removeAttr('required');
}


var dateNow = new Date;
$('#tanggal_from').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: dateNow,
    endDate: dateNow,
}).on("change", function() {
  var dateSama = this.value.split("-");
  var dSama = parseInt(dateSama[2])+1;
  var mSama = parseInt(dateSama[1])-1;
  var ySama = dateSama[0];
  dateSama = new Date(ySama, mSama, dSama - 1);
  /*
  var endDate = new Date(ySama, dateSama.getMonth() + 1, 0);
  var endMonth = endDate.getMonth();
  if(endMonth != mSama) {
    endMonth -= 1;
    endDate = new Date(ySama, endMonth, 0);
  }
  */
  $('#tanggal_to').val('');
  $('#tanggal_to').datepicker('destroy');
  $('#tanggal_to').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: dateSama,
    endDate: dateNow,
  });
});
</script>
@endsection