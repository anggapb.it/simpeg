@extends('layouts.app')

@section('content')
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app">
	<div class="col-xs-12">
		<!-- <center>
		 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
							App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
		</center> -->
		<center><h3 class="title">INBOX KGB<!-- <br>SATUAN KERJA <span id="satuan_kerja_nama">{{ $satker->satuan_kerja_nama }}</span> --></h3></center>

	 	<hr>
		<div class="form-group row">
			<label for="cname" class="control-label col-lg-3"></label>
			<div class="col-lg-6">
				@include('form.date-range',['label'=>'Periode','required'=>false,'name'=>'tanggal_from','name2'=>'tanggal_to'])
			</div>
			<div class="col-lg-3">
				<button class="btn btn-primary btn-sm" id="btnsubmit" name="btnsubmit" v-on:click="doSearch"><i class="fa fa-search"></i> Search</button>					
			</div>
		</div>
		<!-- <section style="display:none" v-show="showAdvancedSearch">
			<div class="form-group row">
			<label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
			<div class="col-lg-9">
				<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker">
					<option value="">Please Select</option>
					@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
						<option value="{{$key}}">{{$value}}</option>
					@endforeach
				</select> 
			</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Unit Kerja</label>
				<div class="col-lg-9">
					<select name="psearch" id="m_uker" v-model="search_uker">
						<option value="">Please Select</option>
						@foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Eselon</label>
				<?php 
				$arrayno = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
					<div class="col-md-1">
						<label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
				<div class="col-lg-9">
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_jenjab">Struktural
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_jenjab">Fungsional Tertentu
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_jenjab">Fungsional Umum
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
				<div class="col-lg-9">
					<select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
						<option value="">Please Select</option>
						@foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
							<option value="{{$key}}">{{$key}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Golongan</label>
				<?php
					$nogol = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
					<div class="col-lg-1">
						<label>
						<input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
		</section> -->
		<hr>
	 	<div class="tableTools-container">
			<div class="btn-group ewButtonGroup">
				<!-- <a class="btn btn-info btn-sm" v-on:click="showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a> -->
				<!-- <a class="btn btn-primary btn-xs" href="{{url('kgb/usulan-kgb/create')}}"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah usulan</a> -->
				<!-- <a class="btn btn-default btn-sm" v-on:click="clearFilter">Show all</a> -->
			</div>
			<!-- <button type="button" class="btn btn-success btn-sm pull-right" id='prosesKGB' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="proses()">Proses KGB</button> -->
			<!-- <br>
			<br> -->
		</div>
		<div class="form-inline pull-left">
			Show
			<select v-model="pagination.perpage" v-on:change="changePage(1)">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="200">200</option>
				<option value="1000">1000</option>
			</select>
			entries
		</div>
		<div class="form-inline pull-right">
			<div class="form-group">
				Search &nbsp;
				<input class="form-control input-sm" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
			</div>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
	     <thead>
	     <tr class="bg-info">
			<th>NO</th>
			<th>No Usulan</th>
			<th>Tgl Usulan</th>
			<th>Sat. Kerja</th>
			<th>Total Pegawai</th>
			<th>Surat pengantar</th>
			<th>Status</th>
			<!--th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th-->
			<th>*</th>
	     </tr>
	     </thead>
	     <tbody>
	     	<tr v-for="(index, item) in items">
			 	<td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td>
	     		<td>
	     			@{{item.no_usulan}}
	     		</td>
	     		<td>
	     			@{{item.tgl_usulan | formatDate}}
	     		</td>

	     		<td>
					@{{item.satuan_kerja_nama}}
	     		</td>
	     		<td class='text-center'>
				 	<span class="badge progress-bar-danger">@{{item.total_pegawai ? item.total_pegawai : 0 }}</span>
	     		</td>
	     		<td>
				 	<span v-if="item.surat_pengantar">
				 		<i class='fa fa-file-pdf-o red'></i> <a href="javascript:void(0)" onclick="window.open('{{ url('api/usulan-kgb/get_pdf/') }}/@{{item.usulan_id}}')">@{{item.surat_pengantar}}</a>
					</span>
				</td>
	     		<td>
				 	@{{item.nama_status}}
				</td>
				<!--td class='text-center'>
					<input type='hidden' id="peg_id@{{index}}" value="@{{item.peg_id }}">
					<span v-if="item.kgb_status === null">
						<input type='checkbox'  class='bigCheckbox' id='check_proses@{{index}}' value='@{{index}}'>
					</span>
					<span v-else>
						<i class="fa fa-check"  title="Data telah di proses"></i>
					</span>
				</td-->
				<td class='text-center'>
					<span v-if="item.status == 10 || item.status == 20">
						<a class="btn btn-warning btn-xs" v-on:click="changeStatusUsulan(item.usulan_id, 20, item.status)"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Detail</a>
						<a class="btn btn-danger btn-xs" v-show="item.status == 10" v-on:click="changeStatusUsulan(item.usulan_id, -10, item.status)"><i class="fa fa-trash"></i>&nbsp;Tolak</a>
					</span>
					<span v-else>
					<a class="btn btn-success btn-xs" v-bind:href="'inbox-kgb/detail'+'/'+item.usulan_id"><i class="glyphicon glyphicon-search"></i>&nbsp;View</a>
					</span>
				</td>
	     	</tr>
	     </tbody>
	 </table>
	</div>
		<div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
		<div class="form-inline pull-right">
			<div class="form-group">
			Halaman &nbsp;
			<!--first page button-->
			<a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
			<input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
			<a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
			&nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
			&nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* var t = $('#tabel-list').DataTable({
			"bSort" : false,
			"iDisplayLength": 21,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 8 }
			  ]
		}); */
		
		loadState();
		getData();

		$('#tanggal_from').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();

			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			/* if(tgl_from != null && tgl_from != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_to').val("");
			}
			} */
		});

		$('#tanggal_from').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();
			
			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			/* if(tgl_to != null && tgl_to != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_from').val("");
			}
			} */
		});
		
	});

	var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			clearFilter: function () {				
				$('#tanggal_from').val('');
				$('#tanggal_to').val('');
				/* $('#m_eselon').select2('val','');
				$('#m_golongan').select2('val','');
				$('#m_kelas_jabatan').select2('val','');
				$('#m_uker').select2('val',''); */
				this.pagination.page = 1;
				this.changePage();
			},
			changePage: function (page) {
			if (isNaN(parseInt(this.pagination.page))) {
				this.pagination.page = 1;
			}
			if (page) this.pagination.page = page;
			getData(this.pagination.page);
			},
			doSearch : function () {
				getData();
			},
			changeStatusUsulan : function (usulan_id, status, status_usulan_real) {
				if(status_usulan_real == 20) {
					window.location = "{{url('kgb/inbox-kgb/detail/')}}/"+usulan_id;
				} else {
					var message = 'reject';
					if(status == 20)
						message = 'cek';

					swal({
						title: "Apakah akan "+ message +" usulan data pegawai ini?",
						text: "",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					}).then((willDelete) => {
						if (willDelete) {
							vm.isLoading = true;                            
							$.getJSON("{{url('kgb/inbox-kgb-ubah-status/').'/'}}",
								{                                
									usulan_id : usulan_id,
									status : status
								},
								function(data) {
									if(data.message) {
										getData();

										swal({
											title : "Success!", 
											text : data.message, 
											type : "success"}).then(
										function() {
											if(status == 20)
											window.location = "{{url('kgb/inbox-kgb/detail/')}}/"+data.usulan_id;
										});
									}
								}
							);
						} else {
							// swal("Your imaginary file is safe!");
						}
					});
        		}
        	}
		}
		});
	
	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/usulan-kgb')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
				par : 'inbox_kgb'
			},
			function(data) {
				vm.items = data.list;
				vm.isLoading = false;
				vm.pagination.count = data.count;
				if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				}
				saveState();
				if(vm.search_satker)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	var page = 'inbox_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal) {
                stored.periode_awal = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
		}
	}

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});
</script>
@endsection