@extends('layouts.app')

@section('content')
<?php
	/* use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai; */
?>
 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="row" id="app">
    <div class="col-lg-12">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('kgb/inbox-kgb') }}" id="usulan" method="post" name="usulan">
            <section class="panel panel-primary">
                <div class="panel-heading">Header Usulan</div>
                <div class="panel-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- <input type="hidden" id="par" name="par" value="0"> -->
                    <input type="hidden" id="usulan_id" name="usulan_id" value="{{ isset($usulan->usulan_id) ? $usulan->usulan_id : '' }}">
                    <table class="table no-footer">
                        <tr>
                            @include('form.txt',['label' => 'No Usulan','required'=>true,'name'=>'no_usulan', 'value' => $usulan->no_usulan, 'readonly' => 'readonly'])
                        </tr>
                        <tr>
                            @include('form.txt',['label' => 'Tgl Usulan','required'=>true,'name'=>'tgl_usulan', 'value' => editDate($usulan->tgl_usulan), 'readonly' => 'readonly'])
                        </tr>
                        <tr>
                        @include('form.txt',['label' => 'Status','required'=>false,'name'=>'nama_status', 'value' => $usulan->statusUsulan[0]->nama_status, 'readonly' => 'readonly'])
                        </tr>
                    </table>
                </div>
                </section>
                <section class="panel panel-primary">
                    <div class="panel-heading">Detail Usulan</div>
                    <div class="panel-body">
                        <div class="form-inline pull-left">
                            Show
                            <select v-model="pagination.perpage" v-on:change="changePage(1)">
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="200">200</option>
                                <option value="1000">1000</option>
                            </select>
                            entries
                        </div>
                        <div class="form-inline pull-right">
                            <div class="form-group">
                                Search &nbsp;
                                <input class="form-control input-sm" id="lookup_key" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
                            </div>
                        </div>
                        <table id="tabel-list-pegawai" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
                            <thead>
                            <tr class="bg-info">
                                <th rowspan="2">NO</th>
                                <th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
                                <th rowspan="2">NIP</th>
                                <th rowspan="2">Golongan</th>
                                <th rowspan="2">JABATAN</th>
                                <!-- <th colspan="2">KGB</th>
                                <th colspan="2">MASA KERJA</th> -->
                                <th rowspan="2">Doc. Digital</th>
                                <th rowspan="2">*</th>
                                <th rowspan="2">Keterangan</th>
                                <th rowspan="2"><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
                            </tr>
                            <tr class="bg-info">
                                <!--th>GOL</th>
                                <!--th>TMT GOL</th-->
                                <!--th>Nama</th>
                                <!--th>TMT</th-->
                                <!-- <th>Terakhir</th>
                                <th>Selanjutnya</th>
                                <th>THN</th>
                                <th>BLN</th> -->
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(index, item) in items">
                                    <td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td>
                                    <td>
                                        <a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</a>
                                        <a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</a>
                                        <br>
                                        @{{item.peg_lahir_tempat}},<span v-if="item.peg_lahir_tanggal">@{{item.peg_lahir_tanggal | formatDate}}</span>
                                    </td>
                                    <td>
                                        @{{item.peg_nip}}
                                    </td>

                                    <td>
                                        @{{item.nm_gol}}
                                    </td>
                                    <td>
                                        <div v-if="item.jf_nama">
                                        @{{item.jf_nama}}
                                        </div>
                                        <div v-else-if="item.jfu_nama">
                                        @{{item.jfu_nama}}
                                        </div> 
                                        <div v-else>
                                        @{{item.jabatan_nama}}
                                        </div>
                                    </td>
                                    <!-- <td>@{{item.peg_tmt_kgb | formatDate}}</td>
                                    <td>@{{item.tgl_kenaikan | formatDate}}</td>
                                    <td>@{{item.peg_kerja_tahun}}</td>
                                    <td>@{{item.peg_kerja_bulan}} </td> -->
                                    <td class='text-center'>
                                        <div class="btn-group" role="group" aria-label="...">
                                            <a class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-detail" onclick="viewDMS();getDMS(@{{item.peg_id}})">DMS</a>
                                        </div>	
                                        
                                    </td>
                                    <td class='text-center'>
                                        <span v-if="item.status_usulan_pegawai <= 10">
                                            <select class="form-control" id="status_usulan_pegawai@{{index}}" name="status_usulan_pegawai[]" onchange="hideKeterangan(@{{index}}, this.value)">
                                                <option value="10" :selected="item.status_usulan_pegawai==10">Proses</option>
                                                <option value="-10" :selected="item.status_usulan_pegawai==-10">Ditolak</option>
                                            </select>
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == 50">
                                            <strong>Done</strong>
                                        </span>
                                    </td>
                                    <td>
                                        <span v-if="item.status_usulan_pegawai==-10">
                                            <textarea class='form-control' id="keterangan@{{index}}" name="keterangan@{{index}}" rows="2" cols="4">@{{item.keterangan}}</textarea>
                                        </span>
                                        <span v-else>
                                            <textarea class='form-control' style="display:none" id="keterangan@{{index}}" name="keterangan@{{index}}" rows="2" cols="4">@{{item.keterangan}}</textarea>
                                        </span>
                                    </td>
                                    <td class='text-center'>
                                        <input type='hidden' id="peg_id@{{index}}" value="@{{item.peg_id }}">
                                        <input type='hidden' id="usulan_id_detail@{{index}}" value="@{{item.usulan_id_detail }}">
                                        <span v-if="item.status_usulan_pegawai == 10">
                                            <i class="fa fa-check"  title="Data pegawai sedang di proses KGB"></i>
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == 50">
                                            <i class="fa fa-check"  title="Data pegawai telah selesai diKGB"></i>
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == -10">
                                            <i class="fa fa-remove"  title="Data pegawai telah direject"></i>
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == 0">
                                            <input type='checkbox'  class='bigCheckbox' id='check_proses@{{index}}' value='@{{index}}'>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                    <td>
                                        @if((isset($usulan->status) && $usulan->status >= 10 && $usulan->status <= 20))
                                            <button type="button" class="btn btn-success btn-sm pull-right" id='prosesKGB' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="proses()">Proses KGB</button>
                                        @endif
                                    </td>    
                                </tr>
                            </tbody>
                        </table>
                        <div class="animated fadeIn pull-left" v-show="isLoading">
                            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
                        </div>
                        <div class="form-inline pull-right">
                            <div class="form-group">
                            Halaman &nbsp;
                            <!--first page button-->
                            <a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
                            <a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
                            <input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
                            <a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
                            <a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
                            &nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                            &nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel panel-primary">
                    <div class="panel-body">
                        @if((isset($usulan->status) && $usulan->status >= 10 && $usulan->status <= 20))
                            <button id="save_usulan" class="btn btn-primary btn-xs" type="submit" onclick="">Simpan</button>
                        @endif
                        <a class="btn btn-default btn-xs" href="{{url('kgb/inbox-kgb')}}">Kembali</a>
                    </div>
                </section>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
        </div>
		<div class="modal-body">
			<div id="profile" class="user-profile row">
			</div>
			<div id="content_result" class="user-profile row">
			</div>
		</div>
        <div class="modal-footer">
            <button id="submitBahasa" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
  <script type="text/javascript">
  $.fn.ready(function(){    
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });

        loadState();
		getData();
  })

  var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			clearFilter: function () {
				
				this.pagination.page = 1;
				this.changePage();
			},
			changePage: function (page) {
                if (isNaN(parseInt(this.pagination.page))) {
                    this.pagination.page = 1;
                }
                if (page) this.pagination.page = page;
                getData(this.pagination.page);
			},
			doSearch : function () {
				getData();
			}
		}
    });

    function hideKeterangan(index, val) {
        if(val == -10) {
            $('#keterangan'+index).show();
            $('#keterangan'+index).focus();
        } else {
            $('#keterangan'+index).hide();

        }
    }

    function check_all() {
        var table= $('#checked_all').closest('#tabel-list-pegawai');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function proses()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_proses"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan Ceklist terlebih dahulu pegawai yang akan KGB!", "error");
			return false;
		}

		swal({
			title: "Apakah akan proses KGB pada periode ini?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$('#prosesKGB').button('loading');

				vm.isLoading = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					// alert($('#peg_id'+no).val()+'='+no);
					promise = $.ajax({
								type: "POST",
								cache: false,
								url: "{{ URL::to('kgb/proses-kgb') }}/",
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {peg_id:$('#peg_id'+no).val(),
                                    status_usulan_pegawai : $('#status_usulan_pegawai'+no).val(),
                                    keterangan : $('#keterangan'+no).val(),
                                        usulan_id_detail : $('#usulan_id_detail'+no).val()},
								dataType: 'json',
								success: function(data) {
									
								}
							})/* .error(function (e){
								console.log(e);return false;
								swal("Error!", "", "error");
							}); */

							promise.done(function(msg)
							{
								console.log("successfully updated"); 
								successCounter++;
							});

							promise.fail(function(jqXHR) { console.log(jqXHR); });

						promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vm.isLoading = false;
					$('#prosesKGB').button('reset');
					swal("Success!", "Berhasil Proses KGB!", "success");
					$('#checked_all').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		
	}

	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/usulan-detail-kgb/').'/' }}"+$('#usulan_id').val(),
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
			},
			function(data) {
				vm.items = data.list;
				vm.isLoading = false;
				vm.pagination.count = data.count;
				/* if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				} */
				saveState();
			}
		);
	}

	var page = 'detail_inbox_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
		}
	}

    var viewDMS = function(){
        // $("#bahasaForm").attr("action","{{ URL::to('data/bahasa/add/') }}");
        $("#labelBahasa").text("Detail DMS");

        // $(".keahlian_nama").val("");
        // $(".keahlian_deskripsi").val("");
	}
	
	function getDMS(id) {
			$.ajax({
				type: "GET",
				cache: false,
				url: "{{ URL::to('dms-kgb') }}/"+id,
				beforeSend: function (xhr) {
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				data: {"id":id},
				dataType: 'json',
				success: function(data) {
					$('#profile').html(data.view_1);
					$('#content_result').html(data.view_2);
				}
			}).error(function (e){
				console.log(e);
				alert("ERROR!");
			});		
	}

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});
  </script>
@endsection
