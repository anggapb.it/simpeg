@extends('layouts.app')

@section('content')
<?php
	/* use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai; */
?>
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app">
	<div class="col-xs-12">
		<!-- <center>
		 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
							App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
		</center> -->
		<center><h3 class="title">Prediksi KGB<br>SATUAN KERJA <span id="satuan_kerja_nama">{{ $satker->satuan_kerja_nama }}</span></h3></center>

	 	<hr>
		<div class="form-group row">
			<label for="cname" class="control-label col-lg-3"></label>
			<div class="col-lg-6">
				@include('form.date-range',['label'=>'Periode','required'=>false,'name'=>'tanggal_from','name2'=>'tanggal_to'])
			</div>
		</div>
		<section style="display:none" v-show="showAdvancedSearch">
			<!--div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Nama</label>
				<div class="col-lg-9">
					<input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_nama" v-on="keyup:doSearch | key 'enter'">              
				</div>
			</div-->
			<div class="form-group row">
			<label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
			<div class="col-lg-9">
				<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker">
					<option value="">Please Select</option>
					@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
						<option value="{{$key}}">{{$value}}</option>
					@endforeach
				</select> 
			</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Unit Kerja</label>
				<div class="col-lg-9">
					<select name="psearch" id="m_uker" v-model="search_uker">
						<option value="">Please Select</option>
						@foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Eselon</label>
				<?php 
				$arrayno = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
					<div class="col-md-1">
						<label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
				<div class="col-lg-9">
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_jenjab">Struktural
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_jenjab">Fungsional Tertentu
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_jenjab">Fungsional Umum
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
				<div class="col-lg-9">
					<select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
						<option value="">Please Select</option>
						@foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
							<option value="{{$key}}">{{$key}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Golongan</label>
				<?php
					$nogol = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
					<div class="col-lg-1">
						<label>
						<input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
		</section>
		<hr>
	 	<div class="tableTools-container">
		<div class="btn-group ewButtonGroup">
            <a class="btn btn-info btn-sm" v-on:click="showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a>
			<button class="btn btn-primary btn-sm" id="btnsubmit" name="btnsubmit" v-on:click="doSearch"><i class="fa fa-search"></i> Search</button>					
            <a class="btn btn-default btn-sm" v-on:click="clearFilter">Show all</a>
		</div>
			<!-- <button type="button" class="btn btn-success btn-sm pull-right" id='prosesKGB' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="proses()">Proses KGB</button> -->
			<br>
			<br>
		</div>
		<div class="form-inline pull-left">
			Show
			<select v-model="pagination.perpage" v-on:change="changePage(1)">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="200">200</option>
				<option value="1000">1000</option>
			</select>
			entries
		</div>
		<div class="form-inline pull-right">
			<div class="form-group">
				Search &nbsp;
				<input class="form-control input-sm" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
			</div>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
	     <thead>
	     <tr class="bg-info">
			<th rowspan="2">NO</th>
			<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
			<th rowspan="2">NIP</th>
			<th rowspan="2">Golongan</th>
			<th rowspan="2">JABATAN</th>
			<th colspan="2">KGB</th>
			<th colspan="2">MASA KERJA</th>
			<th rowspan="2">Status KGB</th>
			<th rowspan="2">Ket.</th>
			<!-- <th rowspan="2"><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th> -->
	     </tr>
	     <tr class="bg-info">
			<!--th>GOL</th>
			<!--th>TMT GOL</th-->
			<!--th>Nama</th>
			<!--th>TMT</th-->
			<th>Terakhir</th>
			<th>Selanjutnya</th>
			<th>THN</th>
			<th>BLN</th>
		</tr>
	     </thead>
	     <tbody>
		 	<?php $no = 1 ?>
	     	<tr v-for="(index, item) in items" v-bind:class="{'text-danger': (item.mhukum_id == 20 || item.total_dms == 0)}">
			 	<td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td>
	     		<td>
	     			<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</a>
	     			<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</a>
	     			<br>
	     			@{{item.peg_lahir_tempat}},<span v-if="item.peg_lahir_tanggal">@{{item.peg_lahir_tanggal | formatDate}}</span>
	     		</td>
	     		<td>
	     			@{{item.peg_nip}}
	     		</td>

	     		<td>
					@{{item.gol_akhir}}
	     		</td>
	     		<td>
				 	<div v-if="item.jf_nama">
					 @{{item.jf_nama}}
					</div>
					<div v-else-if="item.jfu_nama">
					 @{{item.jfu_nama}}
					</div> 
					<div v-else>
					 @{{item.jabatan_nama}}
	     			</div>
	     		</td>
	     		<td>@{{item.peg_tmt_kgb | formatDate}}</td>
	     		<td>@{{item.tgl_kenaikan | formatDate}}</td>
	     		<td>@{{item.tahun_lama_kerja}}</td>
	     		<td>@{{item.bulan_lama_kerja}} </td>
	     		<!-- <td>
				 <div class="btn-group" role="group" aria-label="...">
				    <button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-detail" onclick="viewDMS();getDMS(@{{item.peg_id}})">DMS</button>
				 </div>	
	 			
				</td> -->
				<td class="text-center">
					<input type='hidden' id="peg_id@{{index}}" value="@{{item.peg_id }}">
					<span v-if="item.kgb_status > 0 && item.kgb_status != 50">
						<i class="fa fa-check"  title="Data telah di verifikasi"></i>
						<!-- <input type='checkbox'  class='bigCheckbox' id='check_proses@{{index}}' value='@{{index}}'> -->
					</span>
					<span v-if="item.kgb_status == 0">
						<i class="fa fa-remove"  title="Data telah belum diverifikasi"></i>
					</span>
				</td>
	     		<td>
				 	<span v-if="item.mhukum_id">
					Masih terkena hukdis.<br>
					</span>
					<span v-if="item.total_dms == 0">
					DMS masih kosong
					</span>
				</td>
	     	</tr>
	     </tbody>
	 </table>
	</div>
		<div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
		<div class="form-inline pull-right">
			<div class="form-group">
			Halaman &nbsp;
			<!--first page button-->
			<a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
			<input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
			<a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
			&nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
			&nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
        </div>
		<div class="modal-body">
			<div id="profile" class="user-profile row">
			</div>
			<div id="content_result" class="user-profile row">
			</div>
		</div>
        <div class="modal-footer">
            <button id="submitBahasa" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* var t = $('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 21,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 8 }
			  ]
		}); */
		
		loadState();
		getData();

		$('#tanggal_from').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();

			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;


		});

		$('#tanggal_from').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();
			
			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;


		});
		
	});

	function check_all() {
        var table= $('#checked_all').closest('#tabel-list-pegawai');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function proses()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_proses"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan Ceklist terlebih dahulu pegawai yang akan KGB!", "error");
			return false;
		}

		swal({
			title: "Apakah akan proses KGB pada periode ini?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$('#prosesKGB').button('loading');

				vm.isLoading = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					// alert($('#peg_id'+no).val()+'='+no);
					promise = $.ajax({
								type: "POST",
								cache: false,
								url: "{{ URL::to('kgb/proses-kgb') }}/",
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {peg_id:$('#peg_id'+no).val()},
								dataType: 'json',
								success: function(data) {
									
								}
							})/* .error(function (e){
								console.log(e);return false;
								swal("Error!", "", "error");
							}); */

							promise.done(function(msg)
							{
								console.log("successfully updated"); 
								successCounter++;
							});

							promise.fail(function(jqXHR) { console.log(jqXHR); });

						promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vm.isLoading = false;
					$('#prosesKGB').button('reset');
					swal("Success!", "Berhasil Proses KGB!", "success");
					$('#checked_all').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		
	}

	var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			search_satker: '',
			search_uker: '',
			search_golongan: [],
			search_input_golongan: [],
			search_eselon: [],
			search_input_eselon: [],
			search_kelas_jabatan: '',
			search_jenjab: '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			clearFilter: function () {
				this.search_satker = '';
				this.search_uker = '';
				this.search_golongan = [];
				this.search_eselon = [];
				this.search_kelas_jabatan = '';
      			this.search_jenjab = '';
				this.search_input_golongan = [];
				this.search_input_eselon = [];
				
				$('#tanggal_from').val('');
				$('#tanggal_to').val('');
				/* $('#m_eselon').select2('val','');
				$('#m_golongan').select2('val','');
				$('#m_kelas_jabatan').select2('val','');
				$('#m_uker').select2('val',''); */
				this.pagination.page = 1;
				this.changePage();
			},
			changePage: function (page) {
			if (isNaN(parseInt(this.pagination.page))) {
				this.pagination.page = 1;
			}
			if (page) this.pagination.page = page;
			getData(this.pagination.page);
			},
			doSearch : function () {
				var eselon = [];
				$.each($("input[name='m_eselon']:checked"), function(){            
					eselon.push($(this).val());
				});
				var golongan = [];
				$.each($("input[name='m_golongan']:checked"), function(){            
					golongan.push($(this).val());
				});
				this.search_golongan = golongan;
      			this.search_eselon = eselon;
				getData();
			}
		}
		});
	
	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/kgb')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
				search_satker: vm.search_satker,
				search_uker: vm.search_uker,
				search_golongan: vm.search_golongan,
				search_eselon: vm.search_eselon,
				search_kelas_jabatan: vm.search_kelas_jabatan,
				search_jenjab: vm.search_jenjab
			},
			function(data) {
				vm.items = data.list_pegawai_kgb;
				vm.isLoading = false;
				// console.log(data.count[0].count);
				vm.pagination.count = data.count[0].count;
				if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				}
				saveState();
				if(vm.search_satker)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	/* $('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
			var url = "";			
			url = "{{url('kgb/rekomendasi-kgb')}}/"+val; */
			/* if ("{{Request::is('evjab/list-clear-unit*')}}"){
				url = "{{url('evjab/list-clear-unit')}}/"+val;
			} else {
				url = "{{url('evjab/list-unit')}}/"+val;
			} */
	  /*   window.location.href= url;
	}); */
	
	var viewDMS = function(){
        // $("#bahasaForm").attr("action","{{ URL::to('data/bahasa/add/') }}");
        $("#labelBahasa").text("Detail DMS");

        // $(".keahlian_nama").val("");
        // $(".keahlian_deskripsi").val("");
	}
	
	/* var viewSKP = function(){
        // $("#bahasaForm").attr("action","{{ URL::to('data/bahasa/add/') }}");
        $("#labelBahasa").text("Detail SKP");

        // $(".keahlian_nama").val("");
        // $(".keahlian_deskripsi").val("");
	} */
	
	function getDMS(id) {
			$.ajax({
				type: "GET",
				cache: false,
				url: "{{ URL::to('dms-kgb') }}/"+id,
				beforeSend: function (xhr) {
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				data: {"id":id},
				dataType: 'json',
				success: function(data) {
					$('#profile').html(data.view_1);
					$('#content_result').html(data.view_2);
				}
			}).error(function (e){
				console.log(e);
				alert("ERROR!");
			});		
	}
	var page = 'prediksi_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage,
			search_satker: vm.search_satker,
			search_uker: vm.search_uker,
			search_golongan: vm.search_golongan,
			search_input_golongan: vm.search_input_golongan,
			search_eselon: vm.search_eselon,
			search_input_eselon: vm.search_input_eselon,
			search_kelas_jabatan: vm.search_kelas_jabatan,
			search_jenjab: vm.search_jenjab
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal) {
                stored.periode_awal = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
			vm.search_satker = stored.search_satker;
			vm.search_uker = stored.search_uker;
			vm.search_golongan = stored.search_golongan;
			vm.search_input_golongan = stored.search_input_golongan;
			vm.search_eselon = stored.search_eselon;
			vm.search_input_eselon = stored.search_input_eselon;
			vm.search_kelas_jabatan = stored.search_kelas_jabatan;
			vm.search_jenjab = stored.search_jenjab;
		}
	}

	/* function getSKP(id) {
			$.ajax({
				type: "GET",
				cache: false,
				url: "{{ URL::to('skp-kgb') }}/"+id,
				beforeSend: function (xhr) {
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token) {
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				data: {"id":id},
				dataType: 'json',
				success: function(data) {
					$('#profile').html(data.view_1);
					$('#content_result').html(data.view_2);
				}
			}).error(function (e){
				console.log(e);
				alert("ERROR!");
			});		
	} */

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});
</script>
@endsection