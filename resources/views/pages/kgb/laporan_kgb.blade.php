@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
?>
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app">
	<div class="col-xs-12">
		<!-- <center>
		 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
							App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
		</center> -->
		<center><h3 class="title">Laporan KGB<!-- <br>SATUAN KERJA <span id="satuan_kerja_nama">{{ $satker->satuan_kerja_nama }}</span> --></h3></center>

	 	<hr>
		<div class="form-group row">
			<label for="cname" class="control-label col-lg-3"></label>
			<div class="col-lg-6">
				@include('form.date-range',['label'=>'Periode','required'=>false,'name'=>'tanggal_from','name2'=>'tanggal_to'])
			</div>
		</div>
		<section style="display:none" v-show="showAdvancedSearch">
			<!--div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Nama</label>
				<div class="col-lg-9">
					<input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_nama" v-on="keyup:doSearch | key 'enter'">              
				</div>
			</div-->
			<div class="form-group row">
			<label for="cname" class="control-label col-lg-3">Penandatangan</label>
			<div class="col-lg-9">
				<select class="psearch" id="m_ttd" name="m_ttd" v-model="search_ttd">
				
					<option selected>Please Select</option>
                        @foreach($ttd_kgb as $item)
                        <option value="{{ $item->peg_id }}">{{ $item->peg_gelar_depan.''.$item->peg_nama.', '.$item->peg_gelar_belakang }}</option>
                        @endforeach
				</select> 
			</div>
			</div>
			<div class="form-group row">
			<label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
			<div class="col-lg-9">
				<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker">
					<option value="">Please Select</option>
					@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
						<option value="{{$key}}">{{$value}}</option>
					@endforeach
				</select> 
			</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Unit Kerja</label>
				<div class="col-lg-9">
					<select name="psearch" id="m_uker" v-model="search_uker">
						<option value="">Please Select</option>
						@foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Eselon</label>
				<?php 
				$arrayno = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
					<div class="col-md-1">
						<label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
				<div class="col-lg-9">
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_jenjab">Struktural
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_jenjab">Fungsional Tertentu
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_jenjab">Fungsional Umum
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
				<div class="col-lg-9">
					<select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
						<option value="">Please Select</option>
						@foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
							<option value="{{$key}}">{{$key}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Golongan</label>
				<?php
					$nogol = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
					<div class="col-lg-1">
						<label>
						<input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
		</section>
		<hr>
	 	<div class="tableTools-container">
		<div class="btn-group ewButtonGroup">
            <a class="btn btn-info btn-sm" v-on:click="showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a>
			<button class="btn btn-primary btn-sm" id="btnsubmit" name="btnsubmit" v-on:click="doSearch"><i class="fa fa-search"></i> Search</button>					
            <a class="btn btn-default btn-sm" v-on:click="clearFilter">Show all</a>
            <!--<a class="btn btn-success btn-sm" v-on="click: exportData">Export Excel</a> -->
		</div>
		<div class="btn-group ewButtonGroup pull-right">
			<button type="button" class="btn btn-danger btn-sm" id='downloadPDF' onclick="exportPdf()">PDF</button>
			<button type="button" class="btn btn-success btn-sm" id='downloadExcel' onclick="exportExcel()">Excel</button>
		</div>
			<br>
			<br>
		</div>
		<div class="form-inline pull-left">
			Show
			<select v-model="pagination.perpage" v-on:change="changePage(1)">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="200">200</option>
				<option value="1000">1000</option>
			</select>
			entries
		</div>
		<div class="form-inline pull-right">
			<div class="form-group">
				Search &nbsp;
				<input class="form-control input-sm" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
			</div>
		</div>
		<div class="table-responsive">
			<table id="table-proses-kgb"
				class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0" v-bind:class="{ 'dimmed': isLoading }">
				<thead>
					<tr class="bg-info">
						<th rowspan="2">NO</th>
						<th rowspan="2">NIP / NAMA</th>
						{{-- <th rowspan="2">NIP</th> --}}
						<th rowspan="2">SATUAN KERJA</th>
						<th rowspan="2">UNIT KERJA</th>		
						<th rowspan="2">JABATAN</th>
						<th rowspan="2">GOLONGAN</th>
						<th rowspan="2">TMT Gaji Berkala</th>
						<th colspan="2">MASA KERJA</th>
						<th colspan="2">GAJI POKOK</th>
						<th rowspan="2">STATUS</th>
					
					</tr>
					<tr>
						<th>THN</th>
						<th>BLN</th>
						<th>Lama</th>
						<th>Baru</th>
					</tr>

				</thead>
				<tbody>
					
					<?php $no = 1 ?>
					<tr v-for="(index, item) in items">
						<td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td>
							<!-- <td>
									<input type='hidden' id="kgb_id@{{index}}" value="@{{item.kgb_id }}">
									<span v-if="item.kgb_status == 0">
										<input type='checkbox'  class='bigCheckbox' id='check_validasi@{{index}}' value='@{{index}}'>
									</span>
									<span v-if="item.kgb_status == 1">
										<span class="glyphicon glyphicon-check"></span>
									</span>
							</td> -->
						
						{{-- <td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td> --}}
						<td>
							<span v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</span>
							<span v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</span>
							<br>
							NIP : @{{item.peg_nip}}
						</td>
						{{-- <td>
							@{{item.peg_nip}}
						</td> --}}
	   
						<td>
						   @{{item.satuan_kerja_nama}}
						</td>
						<td>
							@{{item.unit_kerja_nama}}
						</td>
						<td>
							<div v-if="item.jf_nama">
							@{{item.jf_nama}}
						   </div>
						   <div v-else-if="item.jfu_nama">
							@{{item.jfu_nama}}
						   </div> 
						   <div v-else>
							@{{item.jabatan_nama}}
							</div>
						</td>
						<td>
							@{{item.nm_gol}}
						</td>
						
						<!-- <td>@{{item.peg_tmt_kgb | formatDate}}</td> -->
						<td>@{{item.tgl_kenaikan | formatDate}}</td>
						<td>@{{item.kgb_kerja_tahun}}</td>
						<td>@{{item.kgb_kerja_bulan}}</td>
						<td>@{{formatPrice(item.gaji_pokok)}}</td>
						<td>@{{item.kgb_gapok}}</td>
						<td>
							<span style="color:red" v-if="item.kgb_status == 0">
							 Belum Diverifikasi
						   </span>
						   <span style="color:green" v-if="item.kgb_status == 1">
								Sudah Diverifikasi
							</span>
						</td>
				
					</tr>
				</tbody>
			</table>
		</div>
		<div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
		<div class="form-inline pull-right">
			<div class="form-group">
			Halaman &nbsp;
			<!--first page button-->
			<a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
			<input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
			<a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
			&nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
			&nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
        </div>
		<div class="modal-body">
			<div id="profile" class="user-profile row">
			</div>
			<div id="content_result" class="user-profile row">
			</div>
		</div>
        <div class="modal-footer">
            <button id="submitBahasa" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* var t = $('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 21,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 8 }
			  ]
		}); */
		
		loadState();
		getData(); 

		$('#tanggal_from').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();

			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			/* if(tgl_from != null && tgl_from != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_to').val("");
			}
			} */
		});

		$('#tanggal_from').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();
			
			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			/* if(tgl_to != null && tgl_to != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_from').val("");
			}
			} */
		});
		
	});

	
	function exportExcel() {
		window.open("{{url('kgb/laporan-kgb-excel/xlsx')}}", '_blank');
	}
	
	var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			search_satker: '',
			search_uker: '',
			search_ttd: '',
			search_golongan: [],
			search_input_golongan: [],
			search_eselon: [],
			search_input_eselon: [],
			search_kelas_jabatan: '',
			search_jenjab: '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			clearFilter: function () {
				this.search_ttd = '';
				this.search_satker = '';
				this.search_uker = '';
				this.search_golongan = [];
				this.search_eselon = [];
				this.search_kelas_jabatan = '';
      			this.search_jenjab = '';
				this.search_input_golongan = [];
				this.search_input_eselon = [];
				
				$('#tanggal_from').val('');
				$('#tanggal_to').val('');
				/* $('#m_eselon').select2('val','');
				$('#m_golongan').select2('val','');
				$('#m_kelas_jabatan').select2('val','');
				$('#m_uker').select2('val',''); */
				this.pagination.page = 1;
				this.changePage();
			},
			changePage: function (page) {
			if (isNaN(parseInt(this.pagination.page))) {
				this.pagination.page = 1;
			}
			if (page) this.pagination.page = page;
			getData(this.pagination.page);
			},
			doSearch : function () {
				var eselon = [];
				$.each($("input[name='m_eselon']:checked"), function(){            
					eselon.push($(this).val());
				});
				var golongan = [];
				$.each($("input[name='m_golongan']:checked"), function(){            
					golongan.push($(this).val());
				});
				this.search_golongan = golongan;
      			this.search_eselon = eselon;
				getData();
			},
			formatPrice: function (value){
				let val = (value/1).toFixed(0).replace('.', ',')
				return 'Rp'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			}
		}
		});
	
	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/proses-kgb')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
				search_ttd: vm.search_ttd,
				search_satker: vm.search_satker,
				search_uker: vm.search_uker,
				search_golongan: vm.search_golongan,
				search_eselon: vm.search_eselon,
				search_kelas_jabatan: vm.search_kelas_jabatan,
				search_jenjab: vm.search_jenjab,
				par : 'laporan_kgb'
			},
			function(data) {
				vm.items = data.list_proses_kgb;
				vm.isLoading = false;
				vm.pagination.count = data.count;
				if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				}
				saveState();
				if(vm.search_satker)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	var page = 'laporan_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage,
			search_ttd: vm.search_ttd,
			search_satker: vm.search_satker,
			search_uker: vm.search_uker,
			search_golongan: vm.search_golongan,
			search_input_golongan: vm.search_input_golongan,
			search_eselon: vm.search_eselon,
			search_input_eselon: vm.search_input_eselon,
			search_kelas_jabatan: vm.search_kelas_jabatan,
			search_jenjab: vm.search_jenjab
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal) {
                stored.periode_awal = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
			vm.search_ttd = stored.search_ttd;
			vm.search_satker = stored.search_satker;
			vm.search_uker = stored.search_uker;
			vm.search_golongan = stored.search_golongan;
			vm.search_input_golongan = stored.search_input_golongan;
			vm.search_eselon = stored.search_eselon;
			vm.search_input_eselon = stored.search_input_eselon;
			vm.search_kelas_jabatan = stored.search_kelas_jabatan;
			vm.search_jenjab = stored.search_jenjab;
		}
	}

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});

	function exportPdf() {
		
		var periode_awal = vm.periode_awal;
		var periode_akhir = vm.periode_akhir;
		var satker = vm.search_satker;
		var ttd = vm.search_ttd;
		var encrypt_ttd = btoa(ttd);
		var uker = vm.search_uker;
		var gol = [];
		gol = vm.search_golongan;
		var encrypt_gol = btoa(gol);
		var encrypt_periode_awal = btoa(periode_awal);
		var encrypt_periode_akhir = btoa(periode_akhir);
		if(satker == '') satker = 'all';
		if(uker == '') uker = 'all';
		if(encrypt_gol == '') encrypt_gol = 'all';
		if(encrypt_ttd == '') encrypt_ttd = 'all';
		var url = "{{url('kgb/listing-kgb-pdf')}}"+'/'+encrypt_periode_awal+'/'+encrypt_periode_akhir+'/'+satker+'/'+uker+'/'+encrypt_gol+'/'+encrypt_ttd;
			if(periode_awal == '' || periode_akhir == ''){
				alert("Tentukan periodenya...");
			} else{
				window.open(url, '_blank');
			}	 	
		}
	
</script>
@endsection