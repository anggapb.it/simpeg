@extends('layouts.app')
<?php

?>
@section('content')
<div id="app" class="container-fluid">
	<div class="row">
<h1>Master Tanda tangan KGB</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-ttd-kgb" onclick="addTTDKGB()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah</button>
<br><br>
  <table id="list-table" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>No</th>
         <th>Pejabat Penandatangan</th>
         <th>Gol Awal</th>
         <th>Gol Akhir</th>
         <th>tanda tangan digital</th>
         <th>Jenis jabatan</th>
         <th>Status</th>
         <th>*</th>
     </tr>
       </thead>
    <tbody>
    <?php $no = 1 ?>
    @foreach($list as $k)
    <tr>
        <td>{{$no}}</td>
        <td>{{$k->peg_gelar_depan.''.$k->peg_nama.', '.$k->peg_gelar_belakang}}</td>
        <td>{{$k->nm_gol_awal}}</td>
        <td>{{$k->nm_gol_akhir}}</td>
        <td><?php echo $k->photo_ttd ? '<i class="fa fa-check"  title="Scan ttd digital telah terupload"></i>Sudah diupload' : '' ?></td>
        <td>@if($k->jenis_jabatan == 1) 
            {{ 'Fungsional'}}
            @elseif($k->jenis_jabatan == 2)
             {{'Pelaksana'}}
            @endif
        </td>
        <td>{{$k->status == 1 ? 'Aktif' : 'Non Aktif'}}</td>
        <td>
            <button class="btn btn-warning btn-xs"
                data-toggle="modal" data-target="#modal-ttd-kgb" 
                data-id="{{$k->id}}" 
                data-peg_id_ttd="{{$k->peg_id_ttd}}" 
                data-gol_awal_id="{{$k->gol_awal_id}}" 
                data-gol_akhir_id="{{$k->gol_akhir_id}}" 
                data-satuan_kerja_id="{{$k->satuan_kerja_id}}" 
                data-unit_kerja_parent="{{$k->unit_kerja_parent}}" 
                data-unit_kerja_id="{{$k->unit_kerja_id}}" 
                data-jenis_jabatan="{{$k->jenis_jabatan}}" 
                data-status="{{$k->status}}" 
                data-photo_ttd="{{$k->photo_ttd}}" 
                onclick="editTTDKGB(this)">
                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                Edit</button>
            <a href='javascript:void(0)'
                class="btn btn-danger btn-xs" v-on="click: deleteData({{$k->id}})">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    Delete
            </a>

        </td>
    </tr>
    <?php $no++ ?>
    @endforeach
   </tbody>
 </table>
</div>
</div>

<div class="modal fade" id="modal-ttd-kgb" tabindex="-1" role="dialog" aria-labelledby="modal-ttd-kgb" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelTitle"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="{{url('/ttd-kgb')}}" id="ttdkgbForm" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <?php $satker = Auth::user()->satuan_kerja_id; ?>
            @include('form.select2_modal',['label'=>'Organisasi','name'=>'m_satuan_kerja_id','data'=>App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'required'=>false,'empty'=>'','value'=>$satker])
            @include('form.select2_modal',['label'=>'Satuan Organisasi','name'=>'m_satuan_organisasi_id','data'=>App\Model\UnitKerja::where('satuan_kerja_id',$satker)->where('unit_kerja_level',1)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id'),'required'=>false,'empty'=>''])
            @include('form.select2_modal',['label'=>'Unit Kerja','name'=>'m_unit_kerja_id','data'=>[],'required'=>false,'empty'=>''])
            @include('form.select2_modal',['label'=>'Tanda tangan pegawai','name'=>'m_peg_nip','data'=>[],'required'=>false,'empty'=>''])
            <!-- @include('form.text2',['label'=>'Tanda tangan','required'=>true,'name'=>'peg_id_ttd','placeholder'=>'']) -->
            <input type='hidden' id="peg_id_ttd" name="peg_id_ttd">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Batas Golongan Awal</label>
                        <div class="col-md-8">                            
                            @include('form.select2_edit',['class'=>'form-control','required'=>true,'name'=>'gol_awal_id','data'=>
                                $list_golongan
                            ,'empty'=>'-Pilih-'])
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Batas Golongan Akhir</label>
                        <div class="col-md-8">                            
                            @include('form.select2_edit',['class'=>'form-control','required'=>true,'name'=>'gol_akhir_id','data'=>
                                $list_golongan
                            ,'empty'=>'-Pilih-'])
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Jenis Jabatan</label>
                        <div class="col-md-8">
                            <select class="form-control" id="jenis_jabatan" name="jenis_jabatan"  style="max-width:150px;">
                                <option value="1">Fungsional</option>
                                <option value="2">Pelaksana</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Status</label>
                        <div class="col-md-8">
                            <select class="form-control" id="status" name="status"  style="max-width:150px;">
                                <option value="1">Aktif</option>
                                <option value="2">Non Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            @include('form.password',['label' => 'Pin','required'=>true,'name'=>'pin', 'value' => '', 'width' => true])
            <br>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Upload ttd digital (max 2 MB) <span class="required" aria-required="true">*</span></label>
                        <div class="col-md-8">
                            <input type="file" name="photo_ttd" id="photo_ttd" class="form-control" style="font-size:14px;">
                            <!-- <img id="show_photo_ttd" src="" alt="ttd digital image" class="img-thumbnail"> -->
                            <span id="show_photo_ttd" style="display:none"><i class="fa fa-check"  title="Scan ttd digital telah terupload"></i>Sudah diupload</span>
                        </div>
                    </div>
                </div>
            </div>
         </div>
        <div class="modal-footer">
            <button id="submit" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>

@endsection

@section('scripts')
@include('scripts.head')
<script>
    $(document).ready(function() {
        $('#list-table').DataTable({
        });
        
        $('.select2').select2({ width: '100%',allowClear: true });
    });

    var vm = new Vue({
    el: '#app',
    data: {
        tanggal: {
            from: "",
            to: ""
        },
        isLoading: false,
        items: [],
        satuan_kerja: {{ Auth::user()->satuan_kerja_id }},
        satuan_organisasi: '',
        unit_kerja: '',
        hari_kerja: 0,
    },
    methods: {
        doSearch: function () {
        // getData();
        },
        deleteData : function (id) { 
            swal({
                title: "Apakah akan menghapus ttd KGB ini?",
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    /* swal("Poof! Your imaginary file has been deleted!", {
                    icon: "success",
                    }); */
                    window.location = "{{ URL::to('kgb/delete-ttd-kgb') }}/"+id
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
        }
    }
    });

    $(document).on('change','#m_satuan_kerja_id',function(){
        vm.satuan_kerja = $(this).val();
        vm.satuan_organisasi = '';
        vm.unit_kerja = '';
        // getData();
    });

    $(document).on('change','#m_satuan_organisasi_id',function(){
        vm.satuan_organisasi = $(this).val();
        vm.unit_kerja = '';
        // getData();
    });
    
    $(document).on('change','#m_unit_kerja_id',function(){
        vm.unit_kerja = $(this).val();
        // getData();
    });

    $(document).on('change','#m_peg_nip',function(){
        $('#peg_id_ttd').val($('#m_peg_nip').val());
        // vm.unit_kerja = $(this).val();
        // getData();
    });


/*     getData();
    function getData() {
        vm.isLoading = true;
        $.getJSON("{{url('manual/pengajuan/get-data')}}",
        {
            satuan_kerja: vm.satuan_kerja,
            satuan_organisasi: vm.satuan_organisasi,
            unit_kerja: vm.unit_kerja,
            bulan: vm.bulan,
        },
        function(data) {
            vm.items = data;
            vm.isLoading = false;
            if(data.count==0) {
            alert('Data Tidak Ditemukan!')
            }
        });
    } */

    var addTTDKGB = function(){
        $("#ttdkgbForm").attr("action","{{ URL::to('kgb/ttd-kgb') }}");
        $("#labelTitle").text("Tambah Data");

        $("#peg_id_ttd").val("");
        $("#gol_awal_id").val("");
        $("#gol_akhir_id").val("");

        $('#show_photo_ttd').hide();
        
        $("#m_satuan_kerja_id").val(0).trigger('change');
        $("#m_satuan_organisasi_id").val(0).trigger('change');
        $("#m_unit_kerja_id").val(0).trigger('change');
        $("#m_peg_nip").val(0).trigger('change');
        
        getSatuanOrganisasi();
        getPegawai();
    }
    
    var editTTDKGB = function(e) {
        $("#ttdkgbForm").attr("action","{{ URL::to('kgb/ttd-kgb/') }}/"+$(e).data('id'));
        $("#labelTitle").text("Edit Data");
        
        $('#m_satuan_kerja_id').val($(e).data('satuan_kerja_id')).trigger('change');
        satuan_organisasi_id = $(e).data('unit_kerja_parent');
        unit_kerja_id = $(e).data('unit_kerja_id');
        peg_nip = $(e).data('peg_id_ttd');

        if($(e).data('photo_ttd'))
            $('#show_photo_ttd').show();

        $("#peg_id_ttd").val($(e).data('peg_id_ttd'));
        $("#gol_awal_id").val($(e).data('gol_awal_id'));
        $("#gol_akhir_id").val($(e).data('gol_akhir_id'));
        $("#status").val($(e).data('status'));
        $("#jenis_jabatan").val($(e).data('jenis_jabatan'));
        // $("#show_photo_ttd").prop('src', '<?php echo URL::asset('storage/ttd_digital').'/' ?>'+$(e).data('photo_ttd'));

        getSatuanOrganisasi();

        if(satuan_organisasi_id == "" || unit_kerja_id == "") {
            getUnitKerja();
            // setTimeout(function(){ getPegawai(); }, 2000);

        }
    }
</script>
@endsection
