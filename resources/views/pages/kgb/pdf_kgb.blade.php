<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>SIMPEG BDG</title>
		<meta name="csrf_token" content="{{ csrf_token() }}" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
	</head>
<body>	
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
?>
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PEGAWAI<br>SATUAN KERJA <?php echo strtoupper($satker->satuan_kerja_nama) ?></h3></center>

	 	<hr>
	  <table id="tabel-list-pegawai">
	     <thead>
	     <tr class="bg-info">
			<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
			<th rowspan="2">NIP</th>
			<th rowspan="2">Golongan</th>
			<th rowspan="2">JABATAN</th>
			<th colspan="2">KGB</th>
			<th colspan="2">MASA KERJA</th>
	     </tr>
	     <tr class="bg-info">
			<!--th>GOL</th>
			<!--th>TMT GOL</th-->
			<!--th>Nama</th>
			<!--th>TMT</th-->
			<th>Terakhir</th>
			<th>Selanjutnya</th>
			<th>THN</th>
			<th>BLN</th>
		</tr>
	     </thead>
	     <tbody>
		 	<?php $no = 1 ?>
	     	@foreach ($pegawai as $p)
	     	<?php $golongan = Golongan::where('gol_id', $p->gol_id_akhir)->first();
	     		  $jabatan = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
	     		  if($jabatan){
	     		  	if($jabatan['jabatan_jenis'] == 3){
						$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
						$jabatan['jabatan_nama'] = $jf['jf_nama'];
					}elseif($jabatan['jabatan_jenis'] == 4){
						$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
						$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
					}
	     		  }
	     		  $submit = StatusEditPegawai::where('peg_id', $p->peg_id)->where('status_id', 2)->first();
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
					  {{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}
	     			@else
	     			  {{ $p->peg_gelar_depan}}{{$p->peg_nama}}
	     			@endif
	     			<br>
	     			{{$p->peg_lahir_tempat}},{{$p->peg_lahir_tanggal ? $p->peg_lahir_tanggal : '' }}
	     		</td>
	     		<td>
	     			{{$p->peg_nip}}
	     		</td>

	     		<td>@if($golongan)
	     				{{$golongan->nm_gol}}
	     			@endif
	     		</td>
	     		<td>@if($jabatan)
	     			{{$jabatan['jabatan_nama']}}
	     			@endif
	     		</td>
	     		<td>{{$p->peg_tmt_kgb}}</td>
	     		<td>{{date('d-m-Y', strtotime('+2 years', strtotime($p->peg_tmt_kgb)))}}</td>
	     		<td>{{$p->peg_kerja_tahun}}</td>
	     		<td>{{$p->peg_kerja_bulan}} </td>
	     	</tr>
			 <?php $no++ ?>
	     	@endforeach
	     </tbody>
	 </table>
	</div>
</div>
</body>
</html>
