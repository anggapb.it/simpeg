@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JfuBaru;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
	use App\Model\SatuanKerja;
?>
<center>
	<h3 class="title">PROSES DATA KGB</h3>
</center>
<div id="app" class="row">
	<div class="col-xs-12">
		<div class="form-horizontal">
			<br>
			<table class="table no-footer">
				<tr>
					@include('form.date-range',['label'=>'Periode','required'=>false,'name'=>'tanggal_from','name2'=>'tanggal_to'])
				</tr>
			</table>
			<section style="display:none" v-show="showAdvancedSearch">
			
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Penandatangan</label>
					<div class="col-lg-9">
						<select class="psearch" id="m_ttd" name="m_ttd" v-model="search_ttd">
						
							<option value="" selected>Please Select</option>
								@foreach($ttd_kgb as $item)
								<option value="{{ $item->peg_id }}">{{ $item->peg_gelar_depan.''.$item->peg_nama.', '.$item->peg_gelar_belakang }}</option>
								@endforeach
						</select> 
					</div>
					</div>
				<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
				<div class="col-lg-9">
					<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker">
						<option value="">Please Select</option>
						@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
				</div>
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Unit Kerja</label>
					<div class="col-lg-9">
						<select name="psearch" id="m_uker" v-model="search_uker">
							<option value="">Please Select</option>
							@foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
								<option value="{{$key}}">{{$value}}</option>
							@endforeach
						</select> 
					</div>
				</div>
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">No. Usulan</label>
					<div class="col-lg-9">
						<input class="form-control" id="psearch" name="psearch" placeholder="" type="text" v-model="search_nousulan">
					</div>
					</div>
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Eselon</label>
					<?php 
					$arrayno = 0;
					?>
					<div class="col-lg-9"> 
						@foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
						<div class="col-md-1">
							<label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
						</div>
						@endforeach
					</div>
				</div>
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
					<div class="col-lg-9">
						<div class="col-md-2">
							<label>
								<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_jenjab">Struktural
							</label>
						</div>
						<div class="col-md-2">
							<label>
								<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_jenjab">Fungsional Tertentu
							</label>
						</div>
						<div class="col-md-2">
							<label>
								<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_jenjab">Fungsional Umum
							</label>
						</div>
					</div>
				</div>
				{{-- <div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
					<div class="col-lg-9">
						<select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
							<option value="">Please Select</option>
							@foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
								<option value="{{$key}}">{{$key}}</option>
							@endforeach
						</select> 
					</div>
				</div> --}}
				<div class="form-group row">
					<label for="cname" class="control-label col-lg-3">Golongan</label>
					<?php
						$nogol = 0;
					?>
					<div class="col-lg-9"> 
						@foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
						<div class="col-lg-1">
							<label>
							<input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
						</div>
						@endforeach
					</div>
				</div>
			</section>
			<a class="btn btn-info btn-xs" style="margin-bottom:20px;" v-on:click="showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a>
			<a class="btn btn-default btn-xs" v-on:click="doSearch" style="margin-bottom:20px;">Search</a>
			<a class="btn btn-warning btn-xs" id='btn_verifikasi' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="verifikasi()" style="margin-bottom:20px;">Verifikasi</a>
			<a class="btn btn-danger btn-xs" id='btn_cancel' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="cancel()" style="margin-bottom:20px;">Reject</a>
			<div class="btn-group ewButtonGroup pull-right">
				<a class="btn btn-primary btn-xs"  id='btn_generate_srt' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="generateSrt()" style="margin-bottom:20px;">Create PDF Surat KGB</a>
				<a class="btn btn-success btn-xs"  id='btn_ctk_srt' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..."  onclick="ctksrt()" style="margin-bottom:20px;">Cetak Surat KGB</a>
			</div>

		</div>
	
	</div>

	<div class="col-xs-12">


		@if (session('message'))
		<div class="alert alert-success" id="messageFlash">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			{{ session('message') }}
		</div>
		@endif
		<hr>
		<div class="form-inline pull-left">
			Show
			<select v-model="pagination.perpage" v-on:change="changePage(1)">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="200">200</option>
				<option value="1000">1000</option>
			</select>
			entries
		</div>
		<div class="form-inline pull-right">
			<div class="form-group">
				Search &nbsp;
				<input class="form-control input-sm" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
			</div>
		</div>
		
		<div class="table-responsive">
			<table id="table-proses-kgb"
				class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0" v-bind:class="{ 'dimmed': isLoading }">
				<thead>
					<tr class="bg-info">
							<th rowspan="2"><input type='checkbox' class='bigCheckbox' id='checked_all'
								onchange='check_all()'></th>
						<th rowspan="2">NIP / NAMA</th>
						{{-- <th rowspan="2">NIP</th> --}}
						<th rowspan="2">SATUAN KERJA</th>
						<th rowspan="2">UNIT KERJA</th>		
						{{-- <th rowspan="2">JABATAN</th> --}}
						
						<th rowspan="2">GOL</th>
						<th colspan="2">KGB</th>
						<th colspan="2">MASA KERJA</th>
						<th colspan="2">GAJI POKOK</th>
						{{-- <th rowspan="2">STATUS</th> --}}
						{{-- <th rowspan="2">NO. SURAT</th> --}}
						<th rowspan="2">NO USULAN</th>
						<th rowspan="2">STATUS</th>
						
						<th rowspan="2"></th>
					
					</tr>
					<tr>
						
						<th>Terakhir</th>
						<th>Selanjutnya</th>
						<th>THN</th>
						<th>BLN</th>
						<th>Lama</th>
						<th>Baru</th>
					</tr>

				</thead>
				<tbody>
					
					<?php $no = 1 ?>
					<tr v-for="(index, item) in items">
							<td>
									<input type='hidden' id="kgb_id@{{index}}" value="@{{item.kgb_id }}">
									{{-- <span v-if="item.kgb_status == 0"> --}}
										<input type='checkbox'  class='bigCheckbox' id='check_validasi@{{index}}' value='@{{index}}'>
									{{-- </span> --}}
									{{-- <span v-if="item.kgb_status == 1">
										<span class="glyphicon glyphicon-check"></span>
									</span> --}}
							</td>
						
						{{-- <td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td> --}}
						<td>
							<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</a>
							<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</a>
							<br>
							NIP : @{{item.peg_nip}}
						</td>
						{{-- <td>
							@{{item.peg_nip}}
						</td> --}}
	   
						<td>
						   @{{item.satuan_kerja_nama}}
						</td>
						<td>
							<input class="form-control input-sm" type='hidden' id="nm_unit_kerja@{{index}}" value="@{{item.unit_kerja_nama}}">
							@{{item.unit_kerja_nama}}
						</td>
					
						<td>
							@{{item.nm_gol}}
						</td>
						{{-- <td>		
							@{{item.peg_kerja_tahun}}
						</td>
						<td>@{{item.peg_kerja_bulan}}</td> --}}
						<td>@{{item.peg_tmt_kgb | formatDate}}</td>
						<td>@{{item.kgb_tmt | formatDate}}</td>
						<td><input class="form-control input-sm" type='hidden' id="kgb_kerja_tahun@{{index}}" value="@{{item.kgb_kerja_tahun}}">
							<span>@{{item.kgb_kerja_tahun}}</span>
						</td>
						<td><input class="form-control input-sm" type='hidden' id="kgb_kerja_bulan@{{index}}" value="@{{item.kgb_kerja_bulan}}">
							<span>@{{item.kgb_kerja_bulan}}</span>
						</td>
					
						{{-- <td><input type="text" class="form-control input-sm" id="tgl_kenaikan@{{index}}" autocomplete="off" value="@{{item.tgl_kenaikan}}"/></td> --}}
						{{-- <td>
							<span style="color:red" v-if="item.kgb_status == 0">
									<input class="form-control input-sm" type='text' id="kgb_kerja_tahun@{{index}}" value="@{{item.tahun_lama_kerja}}">
							</span>	
							<span style="color:red" v-if="item.kgb_status == 1">
								<input class="form-control input-sm" type='text' id="kgb_kerja_tahun@{{index}}" value="@{{item.tahun_lama_kerja}}">
							</span>
						</td> --}}
						{{-- <td><input class="form-control input-sm" type='text' id="kgb_kerja_bulan@{{index}}" value="@{{item.bulan_lama_kerja}}"></td>  --}}
						{{-- <td>@{{item.kgb_kerja_bulan}}></td> --}}
						<td>@{{formatPrice(item.gaji_pokok)}}</td>
						<td>@{{formatPrice(item.kgb_gapok)}}</td>
						<td>@{{item.no_usulan}}</td>
						<td>
							<span style="color:red" v-if="item.kgb_status == 0">
							 Belum Diverifikasi
							 <input type="checkbox" id="c_kgb_status@{{index}}" value="@{{index}}" hidden="true"/>
							 <input type="checkbox" id="c_kgb_status_pdf@{{index}}" value="@{{index}}" hidden="true"/>
						   </span>
						   <span style="color:green" v-if="item.kgb_status == 1 && item.kgb_status_pdf != 1">
								Sudah Diverifikasi
								<input type="checkbox" id="c_kgb_status@{{index}}" value="@{{index}}" checked hidden="true"/>
								<input type="checkbox" id="c_kgb_status_pdf@{{index}}" value="@{{index}}" hidden="true"/>
							</span>
							<span style="color:green" v-if="item.kgb_status == 1 && item.kgb_status_pdf == 1">
								Sudah Diverifikasi & Surat siap di cetak 
								<input type="checkbox" id="c_kgb_status@{{index}}" value="@{{index}}" hidden="true"/>
								<input type="checkbox" id="c_kgb_status_pdf@{{index}}" value="@{{index}}" checked hidden="true"/>
							</span>
							
						</td>
						{{-- <td>
							<input type='text' id="kgb_nosrt@{{index}}" value="@{{item.kgb_nosurat}}">
						</td> --}}
						<td>
							<span v-if="item.kgb_status_pdf == 1">
								<a class="btn btn-primary btn-xs" href="{{url('/kgb/srt-kgb')}}/@{{item.kgb_id}}" style="margin-bottom:20px;"><i class="fa fa-download"></i> Surat</a>
							</span>
						</td>
				
					</tr>
				</tbody>
			</table>
		</div>
		<div class="animated fadeIn pull-left" v-show="isLoading">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
		<div class="form-inline pull-right">
			<div class="form-group">
			Halaman &nbsp;
			<!--first page button-->
			<a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
			<input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
			<a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
			&nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
			&nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-kgb" tabindex="-1" role="dialog" aria-labelledby="modal-kgb" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
						class="sr-only">Close</span></button>
				<h4 class="modal-title">Form Input Data KGB Manual</h4>
			</div>
			<form action="{{url('/manual/pengajuan/update')}}" id="pengajuanForm" method="POST" class="form-horizontal"
				enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-body" id="modal-detail-content">
					<input type="hidden" name="id" id="pengajuan_id">
					<?php $satker = Auth::user()->satuan_kerja_id; ?>
					@include('form.select2_modal',['label'=>'Organisasi','name'=>'m_satuan_kerja_id','data'=>App\Model\SatuanKerja::where('satuan_kerja_nama','not
					like',
					'%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'required'=>false,'empty'=>'','value'=>$satker])
					@include('form.select2_modal',['label'=>'Satuan
					Organisasi','name'=>'m_satuan_organisasi_id','data'=>App\Model\UnitKerja::where('satuan_kerja_id',$satker)->where('unit_kerja_level',1)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id'),'required'=>false,'empty'=>''])
					@include('form.select2_modal',['label'=>'Unit
					Kerja','name'=>'m_unit_kerja_id','data'=>[],'required'=>false,'empty'=>''])
					@include('form.select2_modal',['label'=>'Pegawai','name'=>'m_peg_nip','data'=>[],'required'=>false,'empty'=>''])
					<input type='hidden' id="peg_id" name="peg_id">
					<div class="row">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary btn-xs">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
@include('scripts.head')
<script src="{{asset('js/vue2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		loadState();
		getData();
		$('#tanggal_from').datepicker({
			format: "MM-yyyy",
			startView: "months",
			minViewMode: "months",
			autoclose: true
		});


		$('#tanggal_to').datepicker({
			format: "MM-yyyy",
			startView: "months",
			minViewMode: "months",
			autoclose: true
		});

		$('.select2').select2({
			width: '100%',
			allowClear: true
		});
		

		$('#satuan_kerja_id').select2({
			width: '500px'
		}).change(function () {
			var val = $("#satuan_kerja_id option:selected").val();
			var url = "{{url('evjab/list-unit')}}/" + val;
			window.location.href = url;
		});

		$('#tanggal_to').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();

			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			if(tgl_from != null && tgl_from != ""){
			// if(tgl_to<tgl_from){
			// 	alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
			// 	$('#tanggal_to').val("");
			// }
			}
		});

		$('#tanggal_from').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();
			
			vm.periode_awal = tgl_from;
			vm.periode_akhir = tgl_to;

			if(tgl_to != null && tgl_to != ""){
			// if(tgl_to<tgl_from){
			// 	alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
			// 	$('#tanggal_from').val("");
			// }
			}
		});

	});


	function generateSrt(){
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		var searchIDs = $('input:checkbox[id^="c_kgb_status"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		// console.log(searchIDs);
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Tidak ada data yang sudah di verifikasi", "error");
			return false;
		}				

		swal({
			title: "Generate PDF Surat KGB?",
			text: "(Hanya status yang sudah di verifikasi)",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willUpdate) => {
			if (willUpdate) { 
				$('#btn_generate_srt').button('loading');
				vm.isLoading = true;
					$.ajax({
								type: "GET",
								url: "{{ url('kgb/srt-kgb-multiple') }}",
								data: {
									periode_awal : $('#tanggal_from').val(),
									periode_akhir : $('#tanggal_to').val()
									},
								dataType: 'json',
								success: function(data) {
									if(data.message) {
										getData();
										$('#btn_generate_srt').button('reset');
										swal({
											title : "Informasi!", 
											text : "Berhasil!", 
											type : "success"}).then(
										function() {
											//if(status == 20)
											window.location.reload;
										});
									}
								}
							})				

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
	}

	function verifikasi()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_validasi"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan pilih pegawai!", "error");
			return false;
		}

		swal({
			title: "Apakah Anda Yakin?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willUpdate) => {
			if (willUpdate) {
				$('#btn_verifikasi').button('loading');

				vm.isLoading = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					// alert($('#peg_id'+no).val()+'='+no);
					promise = $.ajax({
								type: "POST",
								cache: false,
								url: "{{ URL::to('kgb/proses-kgb/verifikasi') }}/",
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {
									kgb_id:$('#kgb_id'+no).val(),
									kgb_kerja_tahun : $('#kgb_kerja_tahun'+no).val(),
									kgb_kerja_bulan : $('#kgb_kerja_bulan'+no).val(),
									kgb_nosurat : $('#kgb_nosrt'+no).val(),
									nm_unit_kerja : $('#nm_unit_kerja'+no).val(),
									kgb_status : 1,
									},
								dataType: 'json',
								success: function(data) {
								
								}
							})

							// promise.done(function(msg)
							// {
							// 	console.log("successfully updated"); 
							// 	successCounter++;
							// });

							// promise.fail(function(jqXHR) { console.log(jqXHR); });

						promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vm.isLoading = false;
					$('#btn_verifikasi').button('reset');
					swal("Success!", "Berhasil!", "success");
					$('#check_validasi').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});
			
				

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		
	}

	function cancel()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_validasi"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan pilih pegawai!", "error");
			return false;
		}

		swal({
			title: "Apakah Anda Yakin Akan?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willUpdate) => {
			if (willUpdate) {
				$('#btn_cancel').button('loading');

				vm.isLoading = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					// alert($('#peg_id'+no).val()+'='+no);
				promise =	$.ajax({
								type: "POST",
								cache: false,
								url: "{{ URL::to('kgb/proses-kgb/cancel') }}/",
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {
									kgb_id:$('#kgb_id'+no).val(),
									kgb_kerja_tahun : $('#kgb_kerja_tahun'+no).val(),
									kgb_kerja_bulan : $('#kgb_kerja_bulan'+no).val(),
									kgb_nosurat : $('#kgb_nosrt'+no).val(),
									kgb_status : '',
									},
								dataType: 'json',
								success: function(data) {
									
								}
							})

							promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vm.isLoading = false;
					$('#btn_cancel').button('reset');
					swal("Success!", "Berhasil!", "success");
					$('#btn_cancel').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		window.location.reload;
	}

	function ctksrt()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;
		var periode_awal = vm.periode_awal;
		var periode_akhir = vm.periode_akhir;
		var satker = vm.search_satker;
		var ttd = vm.search_ttd;
		var encrypt_satker = btoa(satker);
		
		var encrypt_ttd = btoa(ttd);
		var uker = vm.search_uker;
		var encrypt_uker = btoa(uker);
		var gol = [];
		gol = vm.search_golongan;
		var encrypt_gol = btoa(gol);
		var encrypt_periode_awal = btoa(periode_awal);
		var encrypt_periode_akhir = btoa(periode_akhir);
		if(satker == '') satker = btoa('undefined');
		if(uker == '') uker = btoa('undefined');
		if(encrypt_gol == '') encrypt_gol = btoa('undefined');
		if(encrypt_satker == '') encrypt_satker = btoa('undefined');
		if(encrypt_uker == '') encrypt_uker = btoa('undefined');
		if(encrypt_ttd == '') encrypt_ttd = btoa('undefined');

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="c_kgb_status_pdf"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		// console.log(searchIDs);
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Tidak ada data yang siap di cetak", "error");
			return false;
		}				
		swal({
			title: "Cetak Surat?",
			text: "Hanya Status yang sudah diverifikasi & surat siap di cetak",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willCtk) => {
			if (willCtk) {
				if(periode_awal == '' || periode_akhir == ''){
				alert("Tentukan periodenya...");
				} else{
					window.open("{{ url('kgb/pdf-merger') }}"+'/'+encrypt_periode_awal+'/'+encrypt_periode_akhir+'/'+encrypt_satker+'/'+encrypt_uker+'/'+encrypt_gol+'/'+encrypt_ttd, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=200,width=900,height=600");
				}	 	
				
			} else {
				// swal("Your imaginary file is safe!");
			}
		});
	}

	function check_all() {
		var table = $('#checked_all').closest('#table-proses-kgb');
		if ($('#checked_all').is(':checked')) {
			$('td input:checkbox', table).prop('checked', true);
		} else {
			$('td input:checkbox', table).prop('checked', false);
		}
	}

	var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			search_satker: '',
			search_uker: '',
			search_ttd: '',
			search_golongan: [],
			lookup_key : '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			changePage: function (page) {
			if (isNaN(parseInt(this.pagination.page))) {
				this.pagination.page = 1;
			}
			if (page) this.pagination.page = page;
			getData(this.pagination.page);
			},
			doSearch : function () {
				var eselon = [];
				$.each($("input[name='m_eselon']:checked"), function(){            
					eselon.push($(this).val());
				});
				var golongan = [];
				$.each($("input[name='m_golongan']:checked"), function(){            
					golongan.push($(this).val());
				});
				this.search_golongan = golongan;
      			this.search_eselon = eselon;
				getData();
			},
			doCtkSrtMultiple : function () {
				ctkSrtKgbMultiple();
			},
			formatPrice: function (value){
				let val = (value/1).toFixed(0).replace('.', ',')
				return 'Rp'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			}
		}
		});

	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/proses-kgb')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
				search_satker: vm.search_satker,
				search_ttd: vm.search_ttd,
				search_uker: vm.search_uker,
				search_golongan: vm.search_golongan,
				search_eselon: vm.search_eselon,
				search_kelas_jabatan: vm.search_kelas_jabatan,
				search_jenjab: vm.search_jenjab,
				search_nousulan: vm.search_nousulan
			},
			function(data) {
				vm.items = data.list_proses_kgb;
				vm.isLoading = false;
				vm.pagination.count = data.count;
				// if(data.count==0) {
				// 	alert('Data Tidak Ditemukan!')
				// }
				saveState();
				if(vm.search_satker)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	var page = 'proses_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage,
			search_satker: vm.search_satker,
			search_ttd: vm.search_ttd,
			search_uker: vm.search_uker,
			search_golongan: vm.search_golongan,
			search_input_golongan: vm.search_input_golongan,
			search_eselon: vm.search_eselon,
			search_input_eselon: vm.search_input_eselon,
			search_kelas_jabatan: vm.search_kelas_jabatan,
			search_jenjab: vm.search_jenjab,
			search_nousulan: vm.search_nousulan
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}

	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal) {
                stored.periode_awal = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
			vm.search_satker = stored.search_satker;
			vm.search_ttd = stored.search_ttd;
			vm.search_uker = stored.search_uker;
			vm.search_golongan = stored.search_golongan;
			vm.search_input_golongan = stored.search_input_golongan;
			vm.search_eselon = stored.search_eselon;
			vm.search_input_eselon = stored.search_input_eselon;
			vm.search_kelas_jabatan = stored.search_kelas_jabatan;
			vm.search_jenjab = stored.search_jenjab;
			vm.search_nousulan = stored.search_nousulan;
		}
	}
	
	function ctkSrtKgbMultiple() {
		vm.isLoading = true;
		$.getJSON("{{url('kgb/srt-kgb-multiple')}}",
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage
			},
			function(data) {
				alert("Data PDF Berhasil Disimpan");
				vm.isLoading = false;
			}
		);
	}
</script>



@endsection