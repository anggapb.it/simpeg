@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
    <div class="row">
        <h3>Setting Template Surat KGB</h3>
        @if (session('message'))
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                {{ session('message') }}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <hr>
        <form class="cmxform form-horizontal tasi-form" action="{{ url('kgb/update-template-srt') }}" id="fUpTempSrt" method="post" name="fUpTempSrt">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button class="btn btn-primary btn-xs">
            <i class="ace-icon glyphicon glyphicon-pencil"></i>Update</button>
        <br><br>
        <div class="form-group">
        <textarea class="form-control rounded-0 summernote" name="template_srt" id="template_srt" rows="30">{{$settemplate->template}}</textarea>
        </div>
    </form>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('.summernote').summernote({
         height: 900,   //set editable area's height
            codemirror: { // codemirror options
          theme: 'monokai'
        }
    });
  });

 </script> 

@endsection