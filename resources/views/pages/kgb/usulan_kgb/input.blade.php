@extends('layouts.app')

@section('content')
<?php
	/* use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai; */
?>
 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="row" id="app">
    <div class="col-lg-12">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('kgb/usulan-kgb') }}" id="usulan" method="post" name="usulan" enctype="multipart/form-data">
            <section class="panel panel-primary">
                <div class="panel-heading">Tambah Usulan</div>
                <div class="panel-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="par" name="par" value="0">
                    <input type="hidden" id="usulan_id" name="usulan_id" value="{{ isset($usulan->usulan_id) ? $usulan->usulan_id : '' }}">
                    <!-- <input type="text" id="usulan_id_prediksi" name="usulan_id_prediksi"> -->
                    <table class="table no-footer">
                    <tr>
                        @include('form.txt',['label' => 'No Usulan','required'=>true,'name'=>'no_usulan', 'value' => (isset($usulan->no_usulan) ? $usulan->no_usulan : '') ])
                    </tr>
                    <tr>
                        @include('form.txt',['label' => 'Tgl Usulan','required'=>true,'name'=>'tgl_usulan', 'value' => (isset($usulan->tgl_usulan) ? editDate($usulan->tgl_usulan) : '') ])
                    </tr>
					@if((isset($usulan->statusUsulan[0]->nama_status)))
                    <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td>
                        <input type="text" name="status" id="status" readonly class='form-control' value="{{ (isset($usulan->statusUsulan[0]->nama_status) ? $usulan->statusUsulan[0]->nama_status : '') }}" style="width:250px;">
                        </td>
                    </tr>
					@endif
                    <tr>
                        <td>Surat pengantar * <br> <span>(Maksimal 2 Mb)</span></td>
                        <td>:</td>
                        <td>

						@if (isset($usulan->surat_pengantar) && $usulan->surat_pengantar)
							<i class='fa fa-file-pdf-o red'></i> <a href="javascript:void(0)" onclick="window.open('{{ url('api/usulan-kgb/get_pdf/'.$usulan->usulan_id) }}')">{{$usulan->surat_pengantar}}</a>
						@endif
                        	<input type="file" name="surat_pengantar" id="surat_pengantar" class="form-control" style="font-size:14px;width:250px" value="<?php echo (isset($usulan->surat_pengantar) ? editDate($usulan->surat_pengantar) : '') ?>">
                        </td>
                    </tr>
                    </table>
                </div>
                </section>
                <section class="panel panel-primary">
                    <div class="panel-heading">Detail Usulan</div>
                    <div class="panel-body">
                        @if((isset($usulan->status) && $usulan->status == 0) || !isset($usulan->status))
                        <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-pegawai" onclick="addUsulanKgb()"><i class="ace-icon glyphicon glyphicon-plus"></i>Add Pegawai</a>  
                        <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-prediksi-kgb" onclick='getDataPrediksi()'><i class="ace-icon glyphicon glyphicon-plus"></i>Prediksi KGB</a>  
                        <br><br>
                        @endif
                        <div class="form-inline pull-left">
                            Show
                            <select v-model="pagination.perpage" v-on:change="changePage(1)">
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="200">200</option>
                                <option value="1000">1000</option>
                            </select>
                            entries
                        </div>
                        <div class="form-inline pull-right">
                            <div class="form-group">
                                Search &nbsp;
                                <input class="form-control input-sm" id="lookup_key" type="text" v-model="lookup_key" v-on:keyup.enter="doSearch()">
                            </div>
                        </div>
                        <table id="tabel-list-detail-usulan" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
                            <thead>
                            <tr class="bg-info">
                                <th rowspan="2">NO</th>
                                <th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
                                <th rowspan="2">NIP</th>
                                <th rowspan="2">Golongan</th>
                                <th rowspan="2">JABATAN</th>
                                <!-- <<th colspan="1">KGB</th>
                                <th colspan="2">MASA KERJA</th> -->
                                <th rowspan="2">Status Usulan</th>
                                <th rowspan="2">Keterangan</th>
                                <th rowspan="2">*</th>
                            </tr>
                            <tr class="bg-info">
                                <!--th>GOL</th>
                                <!--th>TMT GOL</th-->
                                <!--th>Nama</th>
                                <!--th>TMT</th-->
                                <!-- <th>Terakhir</th> -->
                                <!-- <th>Selanjutnya</th>
                                <th>THN</th>
                                <th>BLN</th> -->
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(index, item) in items">
                                    <td>@{{ ((pagination.page-1)*pagination.perpage)+(index+1) }}</td>
                                    <td>
                                        <a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</a>
                                        <a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</a>
                                        <br>
                                        @{{item.peg_lahir_tempat}},<span v-if="item.peg_lahir_tanggal">@{{item.peg_lahir_tanggal | formatDate}}</span>
                                    </td>
                                    <td>
                                        @{{item.peg_nip}}
                                    </td>

                                    <td>
                                        @{{item.nm_gol}}
                                    </td>
                                    <td>
                                        <div v-if="item.jf_nama">
                                        @{{item.jf_nama}}
                                        </div>
                                        <div v-else-if="item.jfu_nama">
                                        @{{item.jfu_nama}}
                                        </div> 
                                        <div v-else>
                                        @{{item.jabatan_nama}}
                                        </div>
                                    </td>
                                    <!-- <td>@{{item.peg_tmt_kgb | formatDate}}</td-->
                                    <!-- <td>@{{item.tgl_kenaikan | formatDate}}</td>
                                    <td>@{{item.peg_kerja_tahun}}</td>
                                    <td>@{{item.peg_kerja_bulan}} </td> -->
                                    <td class="text-center">
										<span v-if="item.status_usulan_pegawai == 10">
                                            <i class="fa fa-check"  title="Data pegawai sedang di proses KGB"></i> Proses
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == 50">
                                            <i class="fa fa-check"  title="Data pegawai telah selesai diKGB"></i> Finish
                                        </span>
                                        <span v-if="item.status_usulan_pegawai == -10">
                                            <i class="fa fa-remove"  title="Data pegawai telah direject"></i> Reject
                                        </span> 
									</td>
									<td>@{{item.keterangan}}
									</td>
                                    <td>
                                    @if((isset($usulan->status) && $usulan->status == 0) || !isset($usulan->status))
                                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" v-on:click="deleteData(item.usulan_id_detail)"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                                    @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="animated fadeIn pull-left" v-show="isLoading">
                            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
                        </div>
                        <div class="form-inline pull-right">
                            <div class="form-group">
                            Halaman &nbsp;
                            <!--first page button-->
                            <a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
                            <a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
                            <input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
                            <a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
                            <a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
                            &nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                            &nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel panel-primary">
                    <div class="panel-body">
                        @if((isset($usulan->status) && $usulan->status == 0) || !isset($usulan->status))
                            <button id="save_usulan" class="btn btn-primary btn-xs" type="button" onclick="simpanData()">Simpan</button>
                            <button id="save_usulan" class="btn btn-success btn-xs pull-right" type="button" onclick="submitData()">Kirim ke BKPP</button>
                        @endif
                        <a class="btn btn-default btn-xs" href="{{url('kgb/usulan-kgb')}}">Kembali</a>
                    </div>
                </section>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="modal-pegawai" tabindex="-1" role="dialog" aria-labelledby="modal-pegawai" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelTitle">Prediksi KGB</h4>
        </div>
        <form method="POST" action="" id="usulanPrediksiKgbForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            <?php $satker = Auth::user()->satuan_kerja_id; ?>
            <!-- @include('form.select2_modal',['label'=>'Organisasi','name'=>'m_satuan_kerja_id','data'=>App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'required'=>false,'empty'=>'','value'=>$satker]) -->
            <input type="hidden" id="m_satuan_kerja_id" value="{{$satker}}">
            <input type="hidden" id="peg_id" name="peg_id">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Organisasi</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="satuan_kerja_nama" name="satuan_kerja_nama" value="{{$list_satker->satuan_kerja_nama}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            @include('form.select2_modal',['label'=>'Satuan Organisasi','name'=>'m_satuan_organisasi_id','data'=>App\Model\UnitKerja::where('satuan_kerja_id',$satker)->where('unit_kerja_level',1)->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id'),'required'=>false,'empty'=>''])
            @include('form.select2_modal',['label'=>'Unit Kerja','name'=>'m_unit_kerja_id','data'=>[],'required'=>false,'empty'=>''])
            @include('form.select2_modal',['label'=>'Pegawai','name'=>'m_peg_nip','data'=>[],'required'=>false,'empty'=>''])
            <!-- @include('form.text2',['label'=>'Tanda tangan','required'=>true,'name'=>'peg_id_ttd','placeholder'=>'']) -->
            <!-- <input type='hidden' id="peg_id_ttd" name="peg_id_ttd"> -->
            
         </div>
        <div class="modal-footer">
            <a id="submit" onclick="tambahData()" data-dismiss="modal" class="btn btn-primary btn-xs">Simpan</a>
        </div>
    </form>
    </div>
    </div>
</div>
<div class="modal fade" id="modal-prediksi-kgb"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modal-prediksi-kgb" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelTitle"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="" id="usulanKgbForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body">            
            @include('pages.kgb.usulan_kgb.modal-prediksi',[])            
         </div>
        <div class="modal-footer">
            <a id="submitPrediksiKgb" onclick="tambahDataPrediksi()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." class="btn btn-primary btn-xs">Simpan</a>
            <a id="submit" data-dismiss="modal" class="btn btn-default btn-xs">Close</a>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/vue2.min.js')}}"></script>
@include('scripts.head')
  <script type="text/javascript">
  $.fn.ready(function(){
    $('.select2').select2({ width: '100%',allowClear: true });
    $('#tgl_usulan').datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
			autoclose: true
        });
    
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });

    /* $("#usulanKgbForm").submit(function() {
        $("#submit").get(0);
        return false;
    });     */
   /*  var t = $('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 10,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 7 }
			  ]
		}); */

        loadState();
		getData();
  })

  var vm = new Vue({
		el: '#app',
		data: {
			isLoading: false,
			// showAdvancedSearch: false,
			items: [],
			periode_awal : '',
			periode_akhir : '',			
			lookup_key : '',
			pagination: {
				page: 1,
				previous: false,
				next: false,
				perpage: 20,
				count: 0,
			},		
		},
		methods: {
			paginate: function (direction) {
			if (direction === 'previous') {
				if (this.pagination.page > 1) {
				--this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
				++this.pagination.page;
				this.changePage();
				}
			} else if (direction === 'first') {
				this.pagination.page = 1;
				this.changePage();
			} else if (direction === 'last') {
				this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
				this.changePage();
			}
			},
			/* clearFilter: function () {
				
				this.pagination.page = 1;
				this.changePage();
			}, */
			changePage: function (page) {
                if (isNaN(parseInt(this.pagination.page))) {
                    this.pagination.page = 1;
                }
                if (page) this.pagination.page = page;
                getData(this.pagination.page);
			},
			doSearch : function () {
				getData();
			},
			deleteData : function (id) {
				swal({
					title: "Apakah akan menghapus usulan data pegawai ini?",
					text: "",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				}).then((willDelete) => {
					if (willDelete) {
						vm.isLoading = true;                            
                        $.getJSON("{{url('kgb/usulan-kgb-delete-detail/').'/'}}"+id,
                            {                                
                            },
                            function(data) {
                                // if(data.message) {
                                    swal("Success!", data.message, "success");
                                    getData();
                                // }
                            }
                        );
					} else {
						// swal("Your imaginary file is safe!");
					}
				});
        	}
		}
    });
	
	var simpanData = function(){
		$("#usulan").attr("action","{{ URL::to('kgb/usulan-kgb/0') }}");
		$("#usulan").submit();
	}
	
	var submitData = function(){
		swal({
			title: "Apakah anda akan kirim usulan ke BKPP?",
			text: "Data yang sudah dikirim tidak dapat diubah lagi!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$("#usulan").attr("action","{{ URL::to('kgb/usulan-kgb/10') }}");
				$("#usulan").submit();
			} else {
				// swal("Your imaginary file is safe!");
			}
		});
	}
    
    var tambahData = function () {
                    vm.isLoading = true;
                    $.ajax({
                            type: "POST",
                            cache: false,
                            url: "{{url('kgb/usulan-kgb-detail/').'/' }}"+$('#usulan_id').val(),
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf_token"]').attr('content');

                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            },
                            data: {peg_id : $('#peg_id').val(), no_usulan : $('#no_usulan').val(), tgl_usulan : $('#tgl_usulan').val(), status : $('#status').val()},
                            dataType: 'json',
                            success: function(data) {
                                if(data.message) {
                                    swal("Success!", data.message, "success");
                                    $('#usulan_id').val(data.usulan_id);
                                    getData();
                                } else {
                                    swal("Error!", data.error, "error");
                                    vm.isLoading = false;
                                }
                            }
                        }).error(function (e){
                            console.log(e);return false;
                            swal("Error!", "", "error");
                        });
        }
            
	function getData() {
		vm.isLoading = true;
		$.getJSON("{{url('api/usulan-detail-kgb/').'/' }}"+$('#usulan_id').val(),
			{
				periode_awal: vm.periode_awal,
				periode_akhir: vm.periode_akhir,
				lookup_key: vm.lookup_key,
				page: vm.pagination.page,
				perpage: vm.pagination.perpage,
			},
			function(data) {
				vm.items = data.list;
				vm.isLoading = false;
				vm.pagination.count = data.count;
				/* if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				} */
				saveState();
			}
		);
	}

	var page = 'detail_usulan_kgb';
	function saveState() {
		var stored = {
			periode_awal: vm.periode_awal,
			periode_akhir: vm.periode_akhir,
			lookup_key: vm.lookup_key,
			page: vm.pagination.page,
			perpage: vm.pagination.perpage
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadState() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			$('#tanggal_from').val(stored.periode_awal);
			$('#tanggal_to').val(stored.periode_akhir);
			vm.periode_awal = stored.periode_awal;
			vm.periode_akhir = stored.periode_akhir;
			vm.lookup_key = stored.lookup_key;
			vm.page = stored.page;
			vm.perpage = stored.perpage;
		}
	}

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});

  var addUsulanKgb = function(){
        // $("#usulanKgbForm").attr("action","{{ URL::to('kgb/ttd-kgb') }}");
        $("#labelTitle").text("Tambah Data");

        $("#peg_id").val("");
        /* $("#gol_awal_id").val("");
        $("#gol_akhir_id").val(""); */
        // $("#status").val("");
        getSatuanOrganisasi();
        getUnitKerja()
        getPegawai();
    }
    
  $(document).on('change','#m_satuan_kerja_id',function(){
        vm.satuan_kerja = $(this).val();
        vm.satuan_organisasi = '';
        vm.unit_kerja = '';
        // getData();
    });

    $(document).on('change','#m_satuan_organisasi_id',function(){
        vm.satuan_organisasi = $(this).val();
        vm.unit_kerja = '';
        // getData();
    });
    
    $(document).on('change','#m_unit_kerja_id',function(){
        vm.unit_kerja = $(this).val();
        // getData();
    });

    $(document).on('change','#m_peg_nip',function(){
        $('#peg_id').val($('#m_peg_nip').val());
        // vm.unit_kerja = $(this).val();
        // getData();
    });
  </script>
  
<script type="text/javascript">
	$(document).ready(function() {
		/* var t = $('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 21,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 8 }
			  ]
		}); */
		
		loadStatePrediksi();
		getDataPrediksi();

		$('#tanggal_from').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').datepicker({
			format: "MM-yyyy",
			startView: "months", 
			minViewMode: "months",
			autoclose: true
		});

		$('#tanggal_to').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();

			vms.periode_awal_prediksi = tgl_from;
			vms.periode_akhir_prediksi = tgl_to;

			/* if(tgl_from != null && tgl_from != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_to').val("");
			}
			} */
		});

		$('#tanggal_from').on('changeDate', function (ev) {
			var tgl_from = $('#tanggal_from').val();
			var tgl_to = $('#tanggal_to').val();
			
			vms.periode_awal_prediksi = tgl_from;
			vms.periode_akhir_prediksi = tgl_to;

			/* if(tgl_to != null && tgl_to != ""){
			if(tgl_to<tgl_from){
				alert('Tanggal to tidak boleh lebih kecil dari tanggal from');
				$('#tanggal_from').val("");
			}
			} */
		});
		
	});

	function check_all() {
        var table= $('#checked_all').closest('#tabel-list-pegawai');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function tambahDataPrediksi()
	{
		var promisesArray = [];
		var successCounter = 0;
		var promise;

		event.preventDefault();
		var searchIDs = $('input:checkbox[id^="check_proses"]:checked').map(function(){
						  return $(this).val();
						}).get(); // <----
		
		if(searchIDs.length == 0) {
			// toastr.error('Silahkan Ceklist terlebih dahulu pegawai yang akan KGB','Error');
			swal("Warning!", "Silahkan Ceklist terlebih dahulu data usulan pegawai!", "error");
			return false;
		}
		
		swal({
			title: "Apakah akan simpan data usulan pegawai ini?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$('#submitPrediksiKgb').button('loading');
				
				vms.isLoadingPrediksi = true;
				for(var i=0;i<searchIDs.length;i++) {
					no = searchIDs[i];
					
					promise = $.ajax({
								type: "POST",
								cache: false,
								async:false,
								url: "{{url('kgb/usulan-kgb-detail/').'/' }}"+$('#usulan_id').val(),
								beforeSend: function (xhr) {
									var token = $('meta[name="csrf_token"]').attr('content');

									if (token) {
										return xhr.setRequestHeader('X-CSRF-TOKEN', token);
									}
								},
								data: {peg_id : $('#peg_id'+no).val()
										, kgb_kerja_tahun : $('#kgb_kerja_tahun'+no).val()
										, kgb_kerja_bulan : $('#kgb_kerja_bulan'+no).val()
										, kgb_tmt : $('#kgb_tmt'+no).val()
										, no_usulan : $('#no_usulan').val(), tgl_usulan : $('#tgl_usulan').val(), status : $('#status').val()},
								dataType: 'json',
								success: function(data) {
									
								},
								error : function (e){
									console.log(e.responseJSON.error);
									alert(e.responseJSON.error);
									// return false;
								}
							})

							promise.done(function(msg)
							{
								$('#usulan_id').attr('value', msg.usulan_id);
								// $('#usulan_id_prediksi').trigger('change')
								console.log(msg.message); 
								successCounter++;
							});

							promise.fail(function(jqXHR) { 
								// console.log(jqXHR);
								// console.log(jqXHR.responseJSON.error);
								// swal("Error!", jqXHR.responseJSON.error, "error");

								$('#submitPrediksiKgb').button('reset');
								$('#checked_all').prop('checked', false);
							});

						promisesArray.push(promise);
				}

				$.when.apply($, promisesArray).done(function()
				{
					getData();
					vms.isLoadingPrediksi = false;
					$('#submitPrediksiKgb').button('reset');
					$('#modal-prediksi-kgb').modal('toggle');
					swal("Success!", "Berhasil simpan usulan data pegawai!", "success");
					$('#checked_all').prop('checked', false);
					// console.log("WAITED FOR " + successCounter + " OF " + testarray.length);
				});
				$.when.apply($, promisesArray).fail(function()
				{
					getData();
					vms.isLoadingPrediksi = false;
					$('#submitPrediksiKgb').button('reset');
					$('#modal-prediksi-kgb').modal('toggle');
					$('#checked_all').prop('checked', false);
				});

			} else {
				// swal("Your imaginary file is safe!");
			}
		});
		
	}

	var vms = new Vue({
		el: '#app_prediksi',
		data: {
			isLoadingPrediksi: false,
			showAdvancedSearch: false,
			datas: [],
			periode_awal_prediksi : '',
			periode_akhir_prediksi : '',			
			lookup_key_prediksi : '',
			search_satker: '',
			search_uker: '',
			search_golongan: [],
			search_input_golongan: [],
			search_eselon: [],
			search_input_eselon: [],
			search_kelas_jabatan: '',
			search_jenjab: '',
			pagination_prediksi: {
				page: 1,
				previous: false,
				next: false,
				perpage: 10,
				count: 0,
			},		
		},
		methods: {
			paginate_prediksi: function (direction) {
			if (direction === 'previous') {
				if (this.pagination_prediksi.page > 1) {
				--this.pagination_prediksi.page;
				this.changePagePrediksi();
				}
			} else if (direction === 'next') {
				if (Math.floor(this.pagination_prediksi.count / this.pagination_prediksi.perpage) != (this.pagination_prediksi.page - 1)) {
				++this.pagination_prediksi.page;
				this.changePagePrediksi();
				}
			} else if (direction === 'first') {
				this.pagination_prediksi.page = 1;
				this.changePagePrediksi();
			} else if (direction === 'last') {
				this.pagination_prediksi.page = Math.floor(this.pagination_prediksi.count / this.pagination_prediksi.perpage) + 1;
				this.changePagePrediksi();
			}
			},
			clearFilter: function () {
				this.search_satker = '';
				this.search_uker = '';
				this.search_golongan = [];
				this.search_eselon = [];
				this.search_kelas_jabatan = '';
      			this.search_jenjab = '';
				this.search_input_golongan = [];
				this.search_input_eselon = [];
				
				$('#tanggal_from').val('');
				$('#tanggal_to').val('');
				/* $('#m_eselon').select2('val','');
				$('#m_golongan').select2('val','');
				$('#m_kelas_jabatan').select2('val','');
				$('#m_uker').select2('val',''); */
				this.pagination_prediksi.page = 1;
				this.changePagePrediksi();
			},
			changePagePrediksi: function (page) {
			if (isNaN(parseInt(this.pagination_prediksi.page))) {
				this.pagination_prediksi.page = 1;
			}
			if (page) this.pagination_prediksi.page = page;
			getDataPrediksi(this.pagination_prediksi.page);
			},
			doSearchPrediksi : function () {
				var eselon = [];
				$.each($("input[name='m_eselon']:checked"), function(){            
					eselon.push($(this).val());
				});
				var golongan = [];
				$.each($("input[name='m_golongan']:checked"), function(){            
					golongan.push($(this).val());
				});
				this.search_golongan = golongan;
      			this.search_eselon = eselon;
				getDataPrediksi();
			}
		}
		});
	
	function getDataPrediksi() {
		vms.isLoadingPrediksi = true;
		$.getJSON("{{url('api/kgb')}}",
			{
				periode_awal: vms.periode_awal_prediksi,
				periode_akhir: vms.periode_akhir_prediksi,
				lookup_key: vms.lookup_key_prediksi,
				page: vms.pagination_prediksi.page,
				perpage: vms.pagination_prediksi.perpage,
				search_satker: vms.search_satker,
				search_uker: vms.search_uker,
				search_golongan: vms.search_golongan,
				search_eselon: vms.search_eselon,
				search_kelas_jabatan: vms.search_kelas_jabatan,
				search_jenjab: vms.search_jenjab
			},
			function(data) {
				vms.datas = data.list_pegawai_kgb;
				vms.isLoadingPrediksi = false;
				vms.pagination_prediksi.count = data.count[0].count;;
				if(data.count==0) {
					alert('Data Tidak Ditemukan!')
				}
				saveState();
				if(vms.search_satker)
					$('#satuan_kerja_nama').html($('select[name=m_satker] option:selected').text());
			}
		);
	}

	var page = 'modal_prediksi_kgb';
	function saveState() {
		var stored = {
			periode_awal_prediksi: vms.periode_awal_prediksi,
			periode_akhir_prediksi: vms.periode_akhir_prediksi,
			lookup_key_prediksi: vms.lookup_key_prediksi,
			page: vms.pagination_prediksi.page,
			perpage: vms.pagination_prediksi.perpage,
			search_satker: vms.search_satker,
			search_uker: vms.search_uker,
			search_golongan: vms.search_golongan,
			search_input_golongan: vms.search_input_golongan,
			search_eselon: vms.search_eselon,
			search_input_eselon: vms.search_input_eselon,
			search_kelas_jabatan: vms.search_kelas_jabatan,
			search_jenjab: vms.search_jenjab
		};
		localStorage.setItem(page + '_state',JSON.stringify(stored));
	}
	function loadStatePrediksi() {
		var stored = JSON.parse(localStorage.getItem(page + '_state'));
		if (stored) {
			var today = new Date();
            
            if(!stored.periode_awal_prediksi) {
                stored.periode_awal_prediksi = moment(today).format('MMMM-YYYY');
            }
			
			$('#tanggal_from').val(stored.periode_awal_prediksi);
			$('#tanggal_to').val(stored.periode_akhir_prediksi);
			vms.periode_awal_prediksi = stored.periode_awal_prediksi;
			vms.periode_akhir_prediksi = stored.periode_akhir_prediksi;
			vms.lookup_key_prediksi = stored.lookup_key_prediksi;
			vms.page = stored.page;
			vms.perpage = stored.perpage;
			vms.search_satker = stored.search_satker;
			vms.search_uker = stored.search_uker;
			vms.search_golongan = stored.search_golongan;
			vms.search_input_golongan = stored.search_input_golongan;
			vms.search_eselon = stored.search_eselon;
			vms.search_input_eselon = stored.search_input_eselon;
			vms.search_kelas_jabatan = stored.search_kelas_jabatan;
			vms.search_jenjab = stored.search_jenjab;
		}
	}

	Vue.filter('formatDate', function(value) {
		if (value) {
			return moment(String(value)).format('DD-MM-YYYY')
		}
	});
</script>
@endsection
