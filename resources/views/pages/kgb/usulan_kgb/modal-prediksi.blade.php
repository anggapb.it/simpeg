
<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<div class="row" id="app_prediksi">
	<div class="col-xs-12">
		<!-- <center>
		 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
							App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
		</center> -->
		<center><h3 class="title">Prediksi KGB<br>SATUAN KERJA <span id="satuan_kerja_nama">{{ $list_satker->satuan_kerja_nama }}</span></h3></center>

	 	<hr>
		<section style="display:none" v-show="showAdvancedSearch">
            <div class="form-group row">
                <label for="cname" class="control-label col-lg-3">Periode</label>
                <div class="col-lg-6">
                    @include('form.date-range',['label'=>'','required'=>false,'name'=>'tanggal_from','name2'=>'tanggal_to'])
                </div>
            </div>
			<div class="form-group row">
			<label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
			<div class="col-lg-9">
				<select class="psearch" id="m_satker" name="m_satker" v-model="search_satker">
					<option value="">Please Select</option>
					@foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
						<option value="{{$key}}">{{$value}}</option>
					@endforeach
				</select> 
			</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Unit Kerja</label>
				<div class="col-lg-9">
					<select name="psearch" id="m_uker" v-model="search_uker">
						<option value="">Please Select</option>
						@foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Eselon</label>
				<?php 
				$arrayno = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
					<div class="col-md-1">
						<label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
				<div class="col-lg-9">
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_jenjab">Struktural
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_jenjab">Fungsional Tertentu
						</label>
					</div>
					<div class="col-md-2">
						<label>
							<input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_jenjab">Fungsional Umum
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
				<div class="col-lg-9">
					<select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
						<option value="">Please Select</option>
						@foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
							<option value="{{$key}}">{{$key}}</option>
						@endforeach
					</select> 
				</div>
			</div>
			<div class="form-group row">
				<label for="cname" class="control-label col-lg-3">Golongan</label>
				<?php
					$nogol = 0;
				?>
				<div class="col-lg-9"> 
					@foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
					<div class="col-lg-1">
						<label>
						<input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
					</div>
					@endforeach
				</div>
			</div>
		</section>
		<hr>
	 	<div class="tableTools-container">
		<div class="btn-group ewButtonGroup">
            <a class="btn btn-info btn-sm" v-on:click="showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a>
			<a class="btn btn-primary btn-sm" id="btnsubmit" name="btnsubmit" v-on:click="doSearchPrediksi"><i class="fa fa-search"></i> Search</a>					
            <a class="btn btn-default btn-sm" v-on:click="clearFilter">Show all</a>
		</div>
			<!-- <button type="button" class="btn btn-success btn-sm pull-right" id='prosesKGB' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..." onclick="proses()">Proses KGB</button> -->
			<br>
			<br>
		</div>
		<div class="form-inline pull-left">
			Show
			<select v-model="pagination_prediksi.perpage" v-on:change="changePagePrediksi(1)">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="200">200</option>
				<option value="1000">1000</option>
			</select>
			entries
		</div>
		<div class="form-inline pull-right">
			<div class="form-group">
				Search &nbsp;
				<input class="form-control input-sm" type="text" v-model="lookup_key_prediksi" v-on:keyup.enter="doSearchPrediksi()">
			</div>
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai" class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoadingPrediksi }">
	     <thead>
	     <tr class="bg-info">
			<th rowspan="2">NO</th>
			<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
			<th rowspan="2">NIP</th>
			<th rowspan="2">Golongan</th>
			<th rowspan="2">JABATAN</th>
			<th colspan="2">KGB</th>
			<th colspan="2">MASA KERJA</th>
			<th rowspan="2">Status KGB</th>
			<th rowspan="2">Ket.</th>
			<th rowspan="2"><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
	     </tr>
	     <tr class="bg-info">
			<!--th>GOL</th>
			<!--th>TMT GOL</th-->
			<!--th>Nama</th>
			<!--th>TMT</th-->
			<th>Terakhir</th>
			<th>Selanjutnya</th>
			<th>THN</th>
			<th>BLN</th>
		</tr>
	     </thead>
	     <tbody>
		 	<?php $no = 1 ?>
	     	<tr v-for="(index, item) in datas" v-bind:class="{'text-danger': (item.mhukum_id == 20 || item.total_dms == 0)}">
			 	<td>@{{ ((pagination_prediksi.page-1)*pagination_prediksi.perpage)+(index+1) }}</td>
	     		<td>
	     			<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-if="item.peg_gelar_belakang != null">@{{ item.peg_gelar_depan}}@{{item.peg_nama}}, @{{item.peg_gelar_belakang}}</a>
	     			<a v-bind:href="'/pegawai/profile/edit/'+item.peg_id" v-else>@{{ item.peg_gelar_depan}}@{{item.peg_nama}}</a>
	     			<br>
	     			@{{item.peg_lahir_tempat}},<span v-if="item.peg_lahir_tanggal">@{{item.peg_lahir_tanggal | formatDate}}</span>
	     		</td>
	     		<td>
	     			@{{item.peg_nip}}
	     		</td>

	     		<td>
					@{{item.nm_gol}}
	     		</td>
	     		<td>
				 	<div v-if="item.jf_nama">
					 @{{item.jf_nama}}
					</div>
					<div v-else-if="item.jfu_nama">
					 @{{item.jfu_nama}}
					</div> 
					<div v-else>
					 @{{item.jabatan_nama}}
	     			</div>
	     		</td>
	     		<td>@{{item.peg_tmt_kgb | formatDate}}</td>
	     		<td>@{{item.tgl_kenaikan | formatDate}}</td>
				<td>@{{item.tahun_lama_kerja}}</td>
	     		<td>@{{item.bulan_lama_kerja}} </td>
	     		<!-- <td>@{{item.peg_kerja_tahun}}</td>
	     		<td>@{{item.peg_kerja_bulan}} </td> -->
	     		<!-- <td>
				 <div class="btn-group" role="group" aria-label="...">
				    <button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-detail" onclick="viewDMS();getDMS(@{{item.peg_id}})">DMS</button>
				 </div>	
	 			
				</td> -->
				<td class="text-center">
					<span v-if="item.kgb_status > 0 && item.kgb_status != 50">
						<i class="fa fa-check"  title="Data telah di verifikasi"></i>
						<!-- <input type='checkbox'  class='bigCheckbox' id='check_proses@{{index}}' value='@{{index}}'> -->
					</span>
					<span v-if="item.kgb_status == 0">
						<i class="fa fa-remove"  title="Data belum diverifikasi"></i>
					</span>
				</td>
	     		<td>
				 	<span v-if="item.mhukum_id">
					Masih terkena hukdis.<br>
					</span>
					<span v-if="item.total_dms == 0">
					DMS masih kosong
					</span>
				</td>
				<td class='text-center'>
					<input type='hidden' id="peg_id@{{index}}" value="@{{item.peg_id }}">
					<input type='hidden' id="kgb_kerja_tahun@{{index}}" value="@{{item.tahun_lama_kerja }}">
					<input type='hidden' id="kgb_kerja_bulan@{{index}}" value="@{{item.bulan_lama_kerja }}">
					<input type='hidden' id="kgb_tmt@{{index}}" value="@{{item.tgl_kenaikan }}">
					<span v-if="(item.kgb_status == null||item.kgb_status == 50) && (item.mhukum_id == null && item.total_dms > 0)">
					<input type='checkbox'  class='bigCheckbox' id='check_proses@{{index}}' value='@{{index}}'>
					</span>
				</td>
	     	</tr>
	     </tbody>
	 </table>
	</div>
		<div class="animated fadeIn pull-left" v-show="isLoadingPrediksi">
            <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
        </div>
		<div class="form-inline pull-right">
			<div class="form-group">
			Halaman &nbsp;
			<!--first page button-->
			<a class="btn btn-default btn-xs" v-on:click="paginate_prediksi('first')"><i class="fa fa-step-backward"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate_prediksi('previous')"><i class="fa fa-chevron-left"></i></a>
			<input class="form-control input-sm" type="text" v-model="pagination_prediksi.page" v-on:change="changePagePrediksi()">
			<a class="btn btn-default btn-xs" v-on:click="paginate_prediksi('next')"><i class="fa fa-chevron-right"></i></a>
			<a class="btn btn-default btn-xs" v-on:click="paginate_prediksi('last')"><i class="fa fa-step-forward"></i></a>
			&nbsp;dari&nbsp;@{{ Math.floor(pagination_prediksi.count / pagination_prediksi.perpage) + 1 }}
			&nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination_prediksi.page - 1) * pagination_prediksi.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination_prediksi.count, pagination_prediksi.page  * pagination_prediksi.perpage) }}&nbsp;dari&nbsp;@{{ pagination_prediksi.count }}</td>
			</div>
		</div>
	</div>
</div>
