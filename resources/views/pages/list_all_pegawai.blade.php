@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
?>
<div class="row">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PEGAWAI<br>SATUAN KERJA {{ $satker->satuan_kerja_nama }}</h3></center>

	 	<hr>
	 	<div class="pull-right tableTools-container">
	 	@if (Auth::user()->role_id != 6 && Auth::user()->role_id != 2 && Auth::user()->role_id != 5 && Auth::user()->role_id != 8)
			<a href="{{url('/add-pegawai-skpd',$satker->satuan_kerja_id)}}" class="btn btn-primary btn-xs"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Pegawai</a>
		@endif
		@if(Auth::user()->satuan_kerja_id == 21 && Auth::user()->role_id != 8 && Auth::user()->role_id != 5)
			<a href="{{url('/add-pegawai-skpd',$satker->satuan_kerja_id)}}" class="btn btn-primary btn-xs"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Pegawai</a>
		@endif
		@if(Auth::user()->role_id == 5 || Auth::user()->role_id == 8)
			<a href="{{url('/list-all-pegawai/download',[$satker->satuan_kerja_id,Auth::user()->unit_kerja_id])}}" class="btn btn-success btn-xs"><i class="ace-icon fa fa-download"></i>Download Data Pegawai</a>
		@else
			<a href="{{url('/list-all-pegawai/download',[$satker->satuan_kerja_id,'all'])}}" class="btn btn-success btn-xs"><i class="ace-icon fa fa-download"></i>Download Data Pegawai</a>
			<a href="{{url('/list-all-pegawai/downloadduk',$satker->satuan_kerja_id)}}" class="btn btn-warning btn-xs"><i class="ace-icon fa fa-download"></i>Download DUK</a>			
		@endif
		</div>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
			<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
			<th rowspan="2">NIP</th>
			<th colspan="2">PANGKAT</th>
			<th colspan="2">JABATAN</th>
			<th colspan="2">PEGAWAI</th>
			<th colspan="2">MASA KERJA</th>
			<th rowspan="2">PILIHAN</th>
	     </tr>
	     <tr class="bg-info">
			<th>GOL</th>
			<th>TMT GOL</th>
			<th>Nama</th>
			<th>TMT</th>
			<th>STATUS</th>
			<th>TMT</th>
			<th>THN</th>
			<th>BLN</th>
		</tr>
	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php $golongan = Golongan::where('gol_id', $p->gol_id_akhir)->first();
	     		  $jabatan = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
	     		  if($jabatan){
	     		  	if($jabatan['jabatan_jenis'] == 3){
						$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
						$jabatan['jabatan_nama'] = $jf['jf_nama'];
					}elseif($jabatan['jabatan_jenis'] == 4){
						$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
						$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
					}
	     		  }
	     		  $submit = StatusEditPegawai::where('peg_id', $p->peg_id)->where('status_id', 2)->first();
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}</a>
	     			@endif
	     			<br>
	     			{{$p->peg_lahir_tempat}},{{$p->peg_lahir_tanggal ? transformDate($p->peg_lahir_tanggal) : '' }}
	     		</td>
	     		<td>
	     			{{$p->peg_nip}}
	     		</td>

	     		<td>@if($golongan)
	     				{{$golongan->nm_gol}}
	     			@endif
	     		</td>
	     		<td>{{transformDate($p->peg_gol_akhir_tmt) ? transformDate($p->peg_gol_akhir_tmt) : '-'}}</td>
	     		<td>@if($jabatan)
	     			{{$jabatan['jabatan_nama']}}
	     			@endif
	     		</td>
	     		<td>{{transformDate($p->peg_jabatan_tmt)}}</td>
	     		<td>
		     		@if($p->peg_status_kepegawaian == '1')
						PNS
					@elseif($p->peg_status_kepegawaian == '2')
						CPNS
					@else
						-
					@endif
				</td>
	     		<td>
	     			@if($p->peg_status_kepegawaian == '1')
						{{ transformDate($p->peg_pns_tmt)}}
					@elseif($p->peg_status_kepegawaian == '2')
						{{ transformDate($p->peg_cpns_tmt) }}
					@else
						-
					@endif
	     		</td>
	     		<td>{{$p->peg_kerja_tahun}}</td>
	     		<td>{{$p->peg_kerja_bulan}} </td>
	     		<td>
		    		<span><a href="{{url('/pegawai/profile/edit',$p->peg_id)}}" class="view-pegawai"><i class="fa fa-search"></i>
	     				View</a></span> <br>
	 			@if (in_array(Auth::user()->role_id, canSkpdEdit() ? [1,3,2,8,9] : [1,3]))
	     			@if($submit)
	     			<span class="edit-pegawai gray"><i class="ace-icon glyphicon glyphicon-pencil"></i> Edit (Sudah diusulkan)</span> <br>
	     			@else
	     			<span><a href="{{url('/edit-pegawai',$p->peg_id)}}"  class="edit-pegawai"><i class="ace-icon glyphicon glyphicon-pencil"></i> Edit</a></span> <br>
	     			@endif
	     			@if(Auth::user()->role_id == '1')
		    			<span class="delete-pegawai"><a href="{{url('/delete-pegawai',$p->peg_id)}}" class="delete-pegawai" onclick="return confirm('Apa Anda Yakin Akan Menghapus Pegawai Ini?')"><i class="fa fa-trash"></i> Delete</a></span>
		    		@endif
		    	@endif
				</td>
	     	</tr>
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 10 }
		  	]
    	});
	});
</script>
@endsection