@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Data Master Bahasa</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-bahasa" onclick="addBahasa()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Bahasa</button>
<br><br>
  <table id="bahasa-table" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Nama</th>
         <th>Deskripsi</th>
         <th>Actions</th>
     </tr>
       </thead>
    <tbody>
    @foreach($bahasa as $k)
    <tr>
        <td>{{$k->keahlian_nama}}</td>
        <td>{{$k->keahlian_deskripsi}}</td>
        <td>
            <button class="btn btn-warning btn-xs"
                data-toggle="modal" data-target="#modal-bahasa" 
                data-nama="{{$k->keahlian_nama}}" 
                data-id="{{$k->keahlian_id}}" 
                data-deskripsi="{{$k->keahlian_deskripsi}}" 
                onclick="editBahasa(this)">
                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                Edit</button>
            <a href="{{url('/data/bahasa/delete',$k->keahlian_id)}}" 
                class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    Delete
            </a>

        </td>
    </tr>
    @endforeach
   </tbody>
 </table>
</div>
</div>

<div class="modal fade" id="modal-bahasa" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelBahasa"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="{{url('/bahasa/add')}}" id="bahasaForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            @include('form.text2',['label'=>'Nama','required'=>false,'name'=>'keahlian_nama','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Deskripsi','required'=>false,'name'=>'keahlian_deskripsi','placeholder'=>''])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitBahasa" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#bahasa-table').DataTable({
        });
    });

    var addBahasa = function(){
        $("#bahasaForm").attr("action","{{ URL::to('data/bahasa/add/') }}");
        $("#labelBahasa").text("Tambah Data Bahasa");

        $(".keahlian_nama").val("");
        $(".keahlian_deskripsi").val("");
    }
    var editBahasa = function(e){
        $("#bahasaForm").attr("action","{{ URL::to('data/bahasa/update/') }}/"+$(e).data('id'));
        $("#labelBahasa").text("Edit Data Bahasa");

        $(".keahlian_nama").val($(e).data('nama'));
        $(".keahlian_deskripsi").val($(e).data('deskripsi'));
    }
</script>
@endsection
