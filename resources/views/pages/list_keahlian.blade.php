@extends('layouts.app')
<?php
    use App\Model\Keahlian;
?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Data Keahlian</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-keahlian" onclick="addKeahlian()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Keahlian</button>
<br><br>
<div class="table-responsive">
  <table id="keahlian-table" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Nama</th>
         <th>Deskripsi</th>
         <th>Tipe</th>
         <th>Actions</th>
     </tr>
       </thead>
    <tbody>
    @foreach($keahlian as $k)
        @include('includes.keahlian_view',['k'=>$k,'i'=>0])
    @endforeach
   </tbody>
 </table>
 </div>
</div>
</div>

<div class="modal fade" id="modal-keahlian" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelKeahlian"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="{{url('/keahlian/add')}}" id="keahlianForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            @include('form.text2',['label'=>'Nama Keahlian','required'=>false,'name'=>'keahlian_nama','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Deskripsi Keahlian','required'=>false,'name'=>'keahlian_deskripsi','placeholder'=>''])
            <br>
            @include('form.select2_modal',['label'=>'Tipe Keahlian','required'=>false,'name'=>'keahlian_tipe','data'=>
            ['default'=>'default', 'angka'=>'angka','level'=>'level'] ,'empty'=>'-Pilih-'])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitKeahlian" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#keahlian-table').DataTable({
            "bSort" : false
        });
    });

    var addKeahlian = function(){
        $("#keahlianForm").attr("action","{{ URL::to('data/keahlian/add/') }}");
        $("#labelKeahlian").text("Tambah Data Keahlian");

        $(".keahlian_nama").val("");
        $(".keahlian_deskripsi").val("");
        $("#keahlian_tipe").val("").select2();
    }

     var addKeahlianChild = function(e){
        console.log('hah');
        $("#keahlianForm").attr("action","{{ URL::to('data/keahlian/addchild/') }}/"+$(e).data('parent'));
        $("#labelKeahlian").text("Tambah Data Keahlian");

        $(".keahlian_nama").val("");
        $(".keahlian_deskripsi").val("");
        $("#keahlian_tipe").val("").select2();
    }
    var editKeahlian = function(e){
        $("#keahlianForm").attr("action","{{ URL::to('data/keahlian/update/') }}/"+$(e).data('id'));
        $("#labelKeahlian").text("Edit Data Keahlian");

        $(".keahlian_nama").val($(e).data('nama'));
        $(".keahlian_deskripsi").val($(e).data('deskripsi'));
        $("#keahlian_tipe").val($(e).data('tipe')).select2();
    }
</script>
@endsection
