@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
?>
<div class="row">
    <div class="col-xs-12">
        <center><h3>Nominatif KPO @if($periode)Periode {{$periode}}@endif tahun <?php echo date("Y"); ?></h3></center>		
        @if($satker)
        <h3 style="text-align:center">Pada {{ $satker->satuan_kerja_nama }}</h3>
        @endif
        @if(Auth::user()->role_id != 2)
            @if(date("n") <= 6)           
            Periode : <select id="periode_id" name="periode_id" class="responsive"> 
            <option value="0">April 2019</option>
            </select>        
            @elseif(date("n") >= 8)
            Periode : <select id="periode_id" name="periode_id" class="responsive"> 
            <option value="1">Oktober 2019</option>
            </select>
            @else
                @if(date("j") <=15)
                Periode : <select id="periode_id" name="periode_id" class="responsive"> 
                <option value="0">April 2019</option>
                </select>        
                @else
                Periode : <select id="periode_id" name="periode_id" class="responsive"> 
                <option value="1">Oktober 2019</option>
                </select>
                @endif
            @endif
        @endif
        <br />
        <br />
        @if(Auth::user()->role_id == '3' || Auth::user()->role_id == '1' || Auth::user()->role_id == '6')			
			 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
                App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])            
        <br />
        <br />
        @endif    
    <div class="table-responsive">
        <table id="list-pengajuan" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">        
            <thead>
                <tr class="ewTableHeader">
                    <th>No</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Pangkat/Gol. Ruang</th>
                    <th>TMT</th>                    
                    <th>Masa Kerja</th>                                        
                    <th>Pend. Akhir</th>
                </tr>                                    
            </thead>
            <tbody>
            @if($daftarkpo)
            <?php $nourut = 1; ?>
            @foreach($daftarkpo as $p)
            <tr>
                <td>{{$nourut}}</td>
                <td>{{$p->peg_nip}}</td>
                <td>
                    @if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}</a>
	     			@endif                    
                </td>
                <td>{{$p->golruang}}</td>
                <td>{{transformDate($p->peg_gol_akhir_tmt) ? transformDate($p->peg_gol_akhir_tmt) : '-'}}</td>                
                <td>{{$p->peg_kerja_tahun}} tahun {{$p->peg_kerja_bulan}} bulan</td>                
                <td>{{$p->kat_nama}}</td>
            </tr>
            <?php $nourut++; ?>
            @endforeach
            @endif
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
		$('#list-pengajuan').DataTable({
			"bSort" : false,	
            "iDisplayLength": 20,		
			"autoWidth": false,
            "columnDefs": [
            { "width": "25px", "targets": 0 },
            { "width": "125px", "targets": 1 },            
            { "width": "125px", "targets": 5 },
            { "width": "80px", "targets": 6 }
		  	]
    	});
	});
    $('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
        var val2 = $("#periode_id option:selected").val();
        var url = "{{url('penjagaan/kpo')}}/"+val+"/"+val2;
	    window.location.href= url;
	}); 
</script>
@endsection