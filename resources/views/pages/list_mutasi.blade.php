@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-xs-12">
		<h3 style="text-align:center">List Pegawai Mutasi</h3>
	 	<hr>
		<br><br>
	  <table id="list-unit" class="table table-striped table-bordered table-hover">
	     <thead>
	     <tr class="bg-info">
	     	 <th>Nama Pegawai</th>
	     	 <th>NIP Pegawai</th>
	         <th>Pilihan</th>
	     </tr>
	     </thead>
	     <tbody>
	     	@foreach($pegawai as $p)
	     	<tr>
	     		<td>{{ $p->peg_nama }}</td>
	     		<td>{{ $p->peg_nip }}</td>
	     		<td>
		     		<a href="{{url('/mutasi-masuk',$p->peg_id)}}" class="btn btn-info btn-xs">
						Mutasi Masuk Pegawai
					</a>
				</td>
	     	</tr>	
			@endforeach
	     </tbody>
	 </table>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#list-unit').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
    	});
	});
</script>
@endsection
