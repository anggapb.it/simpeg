@extends('layouts.app')

@section('content')
<?php
    ini_set('max_execution_time', 6000);
    ini_set('memory_limit','2048M');
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\UnitKerja;
	use App\Model\SatuanKerja;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\JabatanFungsional;
	use App\Model\StatusEditPegawai;
?>
<div class="row">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PEGAWAI STRUKTURAL DAN TMT JABATAN LEBIH DARI 5 TAHUN
		</h3></center>

	 	<hr>
	 	<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
			<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
			<th rowspan="2">NIP</th>
			<th rowspan="2">SATUAN KERJA</th>
			<th colspan="3">JABATAN</th>
			<th colspan="2">PEGAWAI</th>
			<th colspan="2">MASA KERJA</th>
			<th rowspan="2">PILIHAN</th>
	     </tr>
	     <tr class="bg-info">
			<th>Nama</th>
			<th>TMT</th>
			<th>TAHUN MASA JABATAN</th>
			<th>STATUS</th>
			<th>TMT</th>
			<th>THN</th>
			<th>BLN</th>
		</tr>
	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php $golongan = Golongan::where('gol_id', $p->gol_id_akhir)->first();
	     			$jabatan = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
	     			if($jabatan){
		     		  	if($jabatan['jabatan_jenis'] == 3){
							$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
							$jabatan['jabatan_nama'] = $jf['jf_nama'];
						}elseif($jabatan['jabatan_jenis'] == 4){
							$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
							$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
						}
	     		  	}
	     			$submit = StatusEditPegawai::where('peg_id', $p->peg_id)->where('status_id', 2)->first();
	     			$uk = SatuanKerja::where('satuan_kerja_id',$p->satuan_kerja_id)->first();
	     	?>
	     	<tr>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan}}{{$p->peg_nama}}, {{$p->peg_gelar_belakang}}</a>
	     			@else
	     			<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->peg_gelar_depan ? $p->peg_gelar_depan : ''}}{{$p->peg_nama ? $p->peg_nama : ''}}</a>
	     			@endif
	     			<br>
	     			{{$p->peg_lahir_tempat ? $p->peg_lahir_tempat : ''}}, {{$p->peg_lahir_tanggal ? transformDate($p->peg_lahir_tanggal) : '' }}
	     		</td>
	     		<td>
	     			{{$p->peg_nip ? $p->peg_nip : ''}}
	     		</td>
	     		<td>{{$uk ? $uk->satuan_kerja_nama : '-'}}</td>
	     		<td>{{$jabatan['jabatan_nama'] ? $jabatan['jabatan_nama'] : 'Pelaksana'}}</td>
	     		<td>{{$p->peg_jabatan_tmt ? ($p->peg_jabatan_tmt < '1970-01-01' ? 'Tanggal Tidak Valid' : transformDate($p->peg_jabatan_tmt)) : ''}}</td>
	     		<td>{{$p->usia_jabatan}}</td>
	     		<td>
	     			@if($p->peg_status_kepegawaian)
			     		@if($p->peg_status_kepegawaian == '1')
							PNS
						@elseif($p->peg_status_kepegawaian == '2')
							CPNS
						@else
							-
						@endif
					@else
						-
					@endif
				</td>
	     		<td>
	     			@if($p->peg_status_kepegawaian)
		     			@if($p->peg_status_kepegawaian == '1')
							{{ transformDate($p->peg_pns_tmt)}}
						@elseif($p->peg_status_kepegawaian == '2')
							{{ transformDate($p->peg_cpns_tmt) }}
						@else
							-
						@endif
					@else
						-
					@endif
	     		</td>
	     		<td>{{$p->peg_kerja_tahun ? $p->peg_kerja_tahun : ''}}</td>
	     		<td>{{$p->peg_kerja_bulan ? $p->peg_kerja_bulan : ''}} </td>
	     		<td>
	     			<span><a href="{{url('/pegawai/profile/edit',$p->peg_id)}}" class="view-pegawai"><i class="fa fa-search"></i>
	     				View</a></span> <br>
	     			@if(Auth::user()->role_id == '1')
		    			<span class="delete-pegawai"><a href="{{url('/delete-pegawai',$p->peg_id)}}" class="delete-pegawai" onclick="return confirm('Apa Anda Yakin Akan Menghapus Pegawai Ini?')"><i class="fa fa-trash"></i> Delete</a></span>
		    		@endif
				</td>
	     	</tr>
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"iDisplayLength": 20,
        	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    	});
	});
</script>
@endsection