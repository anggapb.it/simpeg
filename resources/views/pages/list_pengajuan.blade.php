@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Data Master Pengajuan</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-pengajuan" onclick="addPengajuan()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah Pengajuan</button>
<br><br>
    <div class="table-responsive">
  <table id="pengajuan-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr class="bg-info">
                <th>No</th>
                <th>Pengajuan</th>
                <th>Deskripsi</th>
                <th>Actions</th>
            </tr>
        </thead>
    <tbody>
    @foreach($pengajuan as $k => $p)
    <tr>
        <td>{{ $k + 1 }}</td>
        <td>{{$p->nama_pengajuan}}</td>
        <td>{{$p->deskripsi}}</td>
        <td>
            <button class="btn btn-warning btn-xs"
                data-toggle="modal" data-target="#modal-pengajuan" 
                data-pengajuan="{{$p->nama_pengajuan}}" 
                data-id="{{$p->id}}" 
                data-deskripsi="{{$p->deskripsi}}" 
                onclick="editPengajuan(this)">
                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                Edit</button>
            <a href="{{url('/data/pengajuan/delete',$p->id)}}" 
                class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    Delete
            </a>

        </td>
    </tr>
    @endforeach
   </tbody>
 </table>
 </div>
</div>
</div>

<div class="modal fade" id="modal-pengajuan" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelPengajuan"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="{{url('/pengajuan/add')}}" id="pengajuanForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body" id="modal-detail-content">
            @include('form.text2',['label'=>'Pengajuan','required'=>false,'name'=>'pengajuan','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Deskripsi','required'=>false,'name'=>'deskripsi','placeholder'=>''])
            <br>
         </div>
        <div class="modal-footer">
            <button id="submitPengajuan" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#pengajuan-table').DataTable({
        });
    });

    var addPengajuan = function(){
        $("#pengajuanForm").attr("action","{{ URL::to('data/pengajuan/add/') }}");
        $("#labelPengajuan").text("Tambah Data Pengajuan");

        $(".pengajuan").val("");
        $(".deskripsi").val("");
    }
    var editPengajuan = function(e){
        $("#pengajuanForm").attr("action","{{ URL::to('data/pengajuan/update/') }}/"+$(e).data('id'));
        $("#labelPengajuan").text("Edit Data Pengajuan");

        $(".pengajuan").val($(e).data('pengajuan'));
        $(".deskripsi").val($(e).data('deskripsi'));
    }
</script>
@endsection
