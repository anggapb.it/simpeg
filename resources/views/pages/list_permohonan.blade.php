@extends('layouts.app')
@section('content')
<div class="row" id="vm">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PEMOHON DENGAN STATUS {{$nama->status}}</h3></center><br>

			@if($nama->id==2 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 3) )

	 		{{--	<a href="{{url('/pegawai/approve/all',$query)}}" class="btn btn-xs btn-primary">Approve All</a> --}}
	 		@endif
	 	<hr>

	 	 <div class="col-lg-12">
	 	<div class="loading panel" v-show="isLoading">
	 	  <h3>Sedang memuat data...</h3>
	 	</div>
	 	</div>
	 	<section v-show="!isLoading" style="display: none; width: 100%;">
	 		<div class="form-group row pull-right">
	 		  <label for="cname" class="control-label col-lg-2">Cari</label>
	 		  <div class="col-lg-9">
	 		     <input class="form-control" id="psearch" name="psearch" placeholder="Masukkan kata kunci" type="text" value="" v-model="search_input" v-on="keyup:doSearch | debounce 500">
	 		  </div>
	 		</div>
	 		<div class="table-responsive">
	 	   <table cellspacing="0" class="table table-striped table-advanced">
	 	    <tbody>
	 	      <tr>
	 	        <td class="ewGridContent">
	 	          <div class="ewGridMiddlePanel" id="gmp_pegawai">
	 	            <table class="table table-bordered table-striped table-condensed" id="tbl_pegawailist">
	 	              <thead>
	 	                <!-- Table header -->
	 	                <tr class="ewTableHeader">
	 	                  @foreach ($columns as $column_id => $column)
	 	                  	<th>
	 	                  	  <div class="ewPointer" v-on="click: sort('{{$column_id}}')">
	 	                  	    <div class="pegawai_{{$column_id}}" id="elh_pegawai_{{$column_id}}">
	 	                  	      <div class="ewTableHeaderBtn">
	 	                  	        <span class="ewTableHeaderCaption">{{$column[0]}}</span>
	 	                  	        <span class="ewTableHeaderSort">
	 	                  	          <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'desc'" class="caret"></span>
	 	                  	          <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'asc' " class="caret ewSortUp"></span>
	 	                  	        </span>
	 	                  	      </div>
	 	                  	    </div>
	 	                  	  </div>
	 	                  	</th>
	 	                  @endforeach
	 	                  @if($nama->id==2 && Auth::user()->role_id == '1')
	 	                  <th class="ewListOptionHeader" data-name="view" style="white-space: nowrap;"><span>Aksi</span></th>
	 	                  @endif
	 	                </tr>
	 	              </thead>
	 	              <tbody>
	 	                <tr class="@{{$index % 2 ? 'ewTableAltRow' : 'ewTableRow'}}" data-rowindex="@{{$index + 1}}" data-rowtype="1" v-repeat="items">
	 	                  <td>
	 	                    <span>@{{updated_at}}</span>
	 	                  </td>
	 	                  <template v-if="stat_length > 0">
	 	                  	<template v-if="stat_cek">
					     		<template v-if="stat == 'pensiun_pegawai'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:red"><b> (Pegawai Dipensiunkan)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'pegawai_meninggal'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:red"><b> (Pegawai Meninggal)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'mutasi_keluar'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:red"><b> (Pegawai Pindah Keluar)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'pindah_pegawai'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:red"><b> (Pegawai Pindah Keluar)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'mutasi_masuk_skpd'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:red"><b> (Pegawai Pindah Masuk)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'tambah_pegawai'">
					     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:green"><b> (Pegawai Baru)</b></span></td>
					     		</template>
					     		<template v-if="stat == 'update_pegawai'">
					     			<template v-if="pegawai_bkd > 0">
					     				<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:orange"><b> (Pegawai Edit)</b></span></td>
					     			</template>
					     			<template v-if="pegawai_bkd <= 0">
					     				<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a> <span style="color:green"><b> (Pegawai Baru)</b></span></td>
					     			</template>
					     		</template>
					     	</template>
				     		<template v-if=!stat_cek>
				     			<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a><span style="color:orange"><b> (Pegawai Edit)</b></span></td>
				     		</template>
					     </template>
					     <template v-if="stat_length <= 0">
					     	<td><a href="{{url('/pegawai/profile/edit')}}/@{{peg_id}}">@{{peg_nama}}</a></td>
					     </template>

					     <td>@{{nama_editor}}</td>
					     @if($nama->id==3 || $nama->id==6)
					     <td>
					     	@{{keterangan}}
					     </td>
					     @endif
	 	                  @if($nama->id==2 && Auth::user()->role_id == '1')
	 	                  <td>
	 	                      <a class="btn btn-xs btn-warning" href="{{url('batal-usulan')}}/@{{peg_id}}" data-toggle="tooltip" title="Batal Usulan">Batal Usulan
	 	                      </a>
	 	                  </td>
	 	                  @endif
	 	                </tr>
	 	              </tbody>
	 	            </table>
	 	          </div>
	 	          <div class="ewGridLowerPanel">
	 	            <form class="form-inline">
	 	              <div class="form-group">
	 	                Page &nbsp;
	 	                <!--first page button-->
	 	                <a class="btn btn-default btn-xs" v-on="click: paginate('first')"><i class="glyphicon glyphicon-step-backward"></i></a>
	 	                <a class="btn btn-default btn-xs" v-on="click: paginate('previous')"><i class="glyphicon glyphicon-chevron-left"></i></a>
	 	                <input class="form-control input-sm" type="text" v-model="pagination.page" v-on="change: changePage, keyup: changePage | key 'enter'">
	 	                <a class="btn btn-default btn-xs" v-on="click: paginate('next')"><i class="glyphicon glyphicon-chevron-right"></i></a>
	 	                <a class="btn btn-default btn-xs" v-on="click: paginate('last')"><i class="glyphicon glyphicon-step-forward"></i></a>
	 	                &nbsp;of&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
	 	                &nbsp;&nbsp;&nbsp;&nbsp; Records&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;to&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;of&nbsp;@{{ pagination.count }}</td>
	 	              </div>
	 	            </form>
	 	          </div>
	 	        </td>
	 	      </tr>
	 	    </tbody>
	 	  </table>
	 	  </div>
	 	</section>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
var page = 'list-permohonan';
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: true,
    showAdvancedSearch: false,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 20,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    sortColumn: 'peg_nama',
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.sortColumn = null;
      this.sortDir = null;
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    }
  }
});

loadState();
vm.changePage();
function getData(page) {
	vm.params.id = "{{$nama->id}}";
  $.getJSON("{{url('list-permohonan/get-data')}}?page="+page+'&perpage='+vm.pagination.perpage,
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
    saveState();
  });
}

function saveState() {
  var stored = {
    pagination : JSON.stringify(vm.pagination),
    params : JSON.stringify(vm.params),
    search : vm.search,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem(page + '_state',JSON.stringify(stored));
}
function loadState() {
  var stored = JSON.parse(localStorage.getItem(page + '_state'));
  if (stored) {
    vm.pagination = JSON.parse(stored.pagination);
    vm.pagination.page = 1;
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.sortColumn = '';
    vm.sortDir = stored.sortDir;
  }
  $('#psearch').val(vm.search);
}
</script>
@endsection
