@extends('layouts.app')
<?php
	use App\Model\Pegawai2;
	use App\Model\Pegawai;
	use App\Model\User;
	use App\Model\StatusEditPegawai;
	use App\Model\StatusEditPegawaiLog;
	use App\Model\LogEditPegawai;

	
	$arr = array();
	foreach($status as $key => $s){
		$arr[] = $s->peg_id;
	}
	$query = http_build_query(array('peg_id' => $arr));
?>
@section('content')
<div class="row">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PEMOHON DENGAN STATUS {{$nama->status}}</h3></center><br>

			@if($nama->id==2 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 3) )

	 		<!--	<a href="{{url('/pegawai/approve/all',$query)}}" class="btn btn-xs btn-primary">Approve All</a> -->
	 		@endif
	 	<hr>
	  <table id="list-permohonan" class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     	<tr>
	     		<th>Waktu</th>
	     		<th>Nama Pegawai</th>
	     		@if($nama->id==4)
	     		<th>Nama Verifikator</th>
	     		@else
	     		<th>Nama Editor</th>
	     		@endif
	     		@if($nama->id==3)
	     		<th>Keterangan Revisi</th>
	     		@endif
	     		@if($nama->id==2 && Auth::user()->role_id == '1')
	     		<th>Aksi</th>
	     		@endif
	     	</tr>
	     </thead>
	     <tbody>
	     	@foreach($status as $key => $s)
	     	<?php 
	     		if($nama->id == 4){
	     			$pegawai = Pegawai::where('peg_id', $s->peg_id)->where('peg_nip', '!=', '')->first();
	     		}else{
	     			$pegawai = Pegawai2::where('peg_id', $s->peg_id)->where('peg_nip', '!=', '')->first();
	     		}
	     		$pegawai_bkd = Pegawai::where('peg_id', $s->peg_id)->where('peg_nip', '!=', '')->first();

	     		$id_editor = App\Model\StatusEditPegawaiLog::where('sed_id', $s->id)->orderBy('id', 'desc')->first();

	     		$ket = LogEditPegawai::where('peg_id', $s->peg_id)->where('status_id', $nama->id)->orderBy('id', 'desc')->first();
	     		$a = StatusEditPegawai::where('peg_id', $s->peg_id)->where('status_id', $nama->id)->whereHas('log', function($query) use ($id_editor){
	     					$query->where('editor_id', $id_editor->editor_id);
	     				})->orderBy('id', 'desc')->first();
	     		$st = StatusEditPegawaiLog::where('sed_id', $a->id)->where('action','!=','usul_perubahan')->where('action','!=','verifikasi_data')->where('action','!=','revisi_data')->where('action','!=','batal_usulan')->orderBy('id', 'desc')->first();
	     		$editor = User::where('id', $id_editor->editor_id)->first();
	     	?>
	     	@if($pegawai)
	     	<tr>
	     		<td>{{transformDateTime($a['updated_at'])}}</td>
	     		@if($st)
		     		@if($st->action=="pensiun_pegawai")
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:red"><b>(Pegawai Dipensiunkan)</b></span></td>
		     		@elseif($st->action=="mutasi_keluar")
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:red"><b>(Pegawai Pindah Keluar)</b></span></td>
		     		@elseif($st->action=="mutasi_masuk_skpd")
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:red"><b>(Pegawai Pindah Masuk)</b></span></td>
		     		@elseif($st->action=="tambah_pegawai")
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:green"><b>(Pegawai Baru)</b></span></td>
		     		@elseif($st->action=="update_pegawai")
		     			@if($pegawai_bkd)
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:orange"><b>(Pegawai Edit)</b></span></td>
		     			@else
		     			<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai->peg_gelar_depan}} {{$pegawai->peg_nama}}, {{$pegawai->peg_gelar_belakang}}</a> <span style="color:green"><b>(Pegawai Baru)</b></span></td>
		     			@endif
		     		@else
		     		<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai['peg_gelar_depan']}} {{$pegawai['peg_nama']}}, {{$pegawai['peg_gelar_belakang']}}</a><span style="color:orange"><b>(Pegawai Edit)</b></span></td>
		     		@endif
		     	@else
		     		<td><a href="{{url('/pegawai/profile/edit',$s->peg_id)}}">{{$pegawai['peg_gelar_depan']}} {{$pegawai['peg_nama']}}, {{$pegawai['peg_gelar_belakang']}}</a></td>
		     	@endif

	     		<td>{{$editor['nama']}}</td>
	     		@if($nama->id==3)
	     		<td>
	     			{{$ket->keterangan}}
	     		</td>
	     		@endif
	     		@if(Auth::user()->role_id == '1' && $nama->id==2)
	     		<td>
	     				<a href="{{url('/batal-usulan',$s->peg_id)}}" class="btn btn-xs btn-warning">Batalkan Usulan</a>
	     		</td>
	     		@endif
	     	</tr>
	     	@endif
	     	@endforeach
	     </tbody>
	   	</table>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#list-permohonan').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
    	});
	});
</script>
@endsection
