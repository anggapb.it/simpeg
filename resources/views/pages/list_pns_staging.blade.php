@extends('layouts.app')

@section('content')
<?php
	use App\Model\Jabatan;
	use App\Model\SatuanKerja;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
?>
<div class="row">
	<div class="col-xs-12">
		<center><h3 class="title">DAFTAR PERUBAHAN JABATAN</h3></center>
		<hr>
	 	@if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
	    	<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
	        {{ session('message') }}
	    </div>
		@endif
	 	<div class="pull-right tableTools-container">
	 		<a href="/pushdata/jabatan-all" class="btn btn-warning btn-xs" onclick="return confirm('Perhatian! Anda akan mem-Push seluruh Perubahan Data Pegawai. Apa anda yakin?')">Push All Pegawai</a>
		</div>
		<div class="table-responsive">
			<table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
		    	<thead>
		     		<tr class="bg-info">
						<th rowspan="2">NAMA/TEMPAT TGL LAHIR</th>
						<th rowspan="2">NIP</th>
						<th colspan="2">DATA SIMPEG</th>
						<th rowspan="2">OPTION</th>
						<th colspan="2">DATA ERK</th>
		     		</tr>
		     		<tr class="bg-info">
						<th>Jabatan / OPD</th>
						<th>TMT</th>
						<th>Jabatan / OPD</th>
						<th>TMT</th>
					</tr>
		     	</thead>
		     	<tbody>
		     		@foreach ($listPegawai as $p)
		     		<?php
	     		  		$jabatan = Jabatan::where('jabatan_id', $p->pegawaiSimpeg->jabatan_id)->first();
	     		  		if($jabatan){
	     		  			if($jabatan['jabatan_jenis'] == 3){
								$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
								$jabatan['jabatan_nama'] = $jf['jf_nama'];
							}elseif($jabatan['jabatan_jenis'] == 4){
								$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
								$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
							}
	     		  		}
	     		  		$jabatanErk = Jabatan::where('jabatan_id', $p->pegawaiErk->jabatan_id)->first();
	     		  		if($jabatanErk){
	     		  			if($jabatanErk['jabatan_jenis'] == 3){
								$jf = JabatanFungsional::where('jf_id', $jabatanErk['jf_id'])->first();
								$jabatan['jabatan_nama'] = $jf['jf_nama'];
							}elseif($jabatanErk['jabatan_jenis'] == 4){
								$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatanErk['jfu_id'])->first();
								$jabatanErk['jabatan_nama'] = $jfu['jfu_nama'];
							}
	     		  		}
	     		  		$skpd = SatuanKerja::where('satuan_kerja_id', $p->pegawaiSimpeg->satuan_kerja_id)->first();
	     		  		$skpdErk = SatuanKerja::where('satuan_kerja_id', $p->pegawaiErk->satuan_kerja_id)->first();
	     			?>
		     		<tr>
		     			<td>
		     				@if($p->pegawaiSimpeg->peg_gelar_belakang != null)
	     					<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->pegawaiSimpeg->peg_gelar_depan}} {{$p->pegawaiSimpeg->peg_nama}}, {{$p->pegawaiSimpeg->peg_gelar_belakang}}</a>
	     					@else
	     					<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}">{{ $p->pegawaiSimpeg->peg_gelar_depan}} {{$p->pegawaiSimpeg->peg_nama}}</a>
	     					@endif
	     					<br>
	     					{{$p->pegawaiSimpeg->peg_lahir_tempat}}, {{$p->pegawaiSimpeg->peg_lahir_tanggal ? transformDate($p->pegawaiSimpeg->peg_lahir_tanggal) : '' }}
		     			</td>
		     			<td>{{$p->pegawaiSimpeg->peg_nip}}</td>
		     			<td>@if($jabatan)
	     						{{$jabatan['jabatan_nama']}} <br /> {{$skpd->satuan_kerja_nama}}
	     					@endif
	     				</td>
	     				<td>
	     					{{transformDate($p->pegawaiSimpeg->peg_jabatan_tmt)}}
	     				</td>
	     				<td align="center">
	     					<a href="{{url('/pushdata/jabatan',$p->peg_id)}}" onclick="return confirm('Apa anda yakin akan mem-Push Data Pegawai ini?')" class="btn btn-primary btn-xs">Push <i class="fa fa-arrow-circle-right"></i></a>
	     				</td>
	     				<td>@if($jabatanErk)
	     						{{$jabatanErk['jabatan_nama']}} <br /> {{$skpdErk->satuan_kerja_nama}}
	     					@endif
	     				</td>
	     				<td>
	     					{{transformDate($p->pegawaiErk->peg_jabatan_tmt)}}
	     				</td>
		     		</tr>
		     		@endforeach
		     	</tbody>
		 	</table>
	 	</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 10,
			"autoWidth": false
    	});
	});
</script>
@endsection
