@extends('layouts.app')

@section('content')
<?php
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\StatusEditPegawai;
?>
<div class="row">
	<div class="col-xs-12">
		<center><h3>Daftar Pegawai Yang Berulang Tahun @if($satuan == 'harian') Hari @else Bulan @endif Ini pada <br>{{ $satker->satuan_kerja_nama }}</h3></center>
		<br>
		<center>
		 @include('form.select2',['label'=>'Pilih Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
							App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
		</center>
		<br><br>
	 	<hr>
		<div class="table-responsive">
	  <table id="tabel-list-pegawai"  class="table table-bordered dataTable no-footer DTTT_selectable dataTables_info" cellspacing="0">
	     <thead>
	     <tr class="bg-info">
				 <th>FOTO</th>
				 <th>BIODATA</th>
				 <th>PILIHAN</th>
	     </tr>
	     </thead>
	     <tbody>
	     	@foreach ($pegawai as $p)
	     	<?php $golongan = Golongan::where('gol_id', $p->gol_id_akhir)->first();
	     		  $jabatan = Jabatan::where('jabatan_id', $p->jabatan_id)->first();
	     		  if($jabatan){
	     		  	if($jabatan['jabatan_jenis'] == 3){
						$jf = JabatanFungsional::where('jf_id', $jabatan['jf_id'])->first();
						$jabatan['jabatan_nama'] = $jf['jf_nama'];
					}elseif($jabatan['jabatan_jenis'] == 4){
						$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan['jfu_id'])->first();
						$jabatan['jabatan_nama'] = $jfu['jfu_nama'];
					}
	     		  }
	     		  $submit = StatusEditPegawai::where('peg_id', $p->peg_id)->where('status_id', 2)->first();
	     	?>
	     	<tr>
					<td>
					@if(file_exists('uploads/'.$p->peg_foto) && $p->peg_foto != null)
						<img id="avatar" class="editable img-responsive" src="{{asset('/uploads/'.$p->peg_foto) }}" />
					@else
						<img id="avatar" class="editable img-responsive" src="{{asset('/uploads/none.png') }}" />
					@endif
					</td>
	     		<td>
	     			@if($p->peg_gelar_belakang != null)
	     			<b>Nama</b>: {{ $p->peg_gelar_depan}} {{$p->peg_nama}}, {{$p->peg_gelar_belakang}}
	     			@else
	     			<b>Nama</b>: {{ $p->peg_gelar_depan}} {{$p->peg_nama}}
	     			@endif
	     			<br>
						<b>NIP</b>: {{$p->peg_nip}}
						<br>
	     			<b>Tempat Lahir</b>: {{$p->peg_lahir_tempat}}
						<br>
						<b>Tanggal Lahir</b>: {{$p->peg_lahir_tanggal ? transformDate($p->peg_lahir_tanggal) : '' }}
						<br>
						<b>Pangkat/Gol.Ruang</b>: {{$golongan->nm_pkt}}, {{$golongan->nm_gol}}
						<br><b>Jabatan</b>: @if($jabatan)
		     			{{$jabatan['jabatan_nama']}}
		     			@endif
	     		</td>
	     		<td>
						<a href="{{url('/pegawai/profile/edit',$p->peg_id)}}" class="btn btn-primary btn-xs"><i class="fa fa-search"></i> Lihat Profil</a>
					</td>
	     	</tr>
				@if($satuan)
				<input type="hidden" name="satuan" value="{{$satuan}}" />
				@endif
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabel-list-pegawai').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
			"autoWidth": false,
			"columnDefs": [
		    { "width": "50px", "targets": 2 },
				{ "width": "50px", "targets": 0 }
		  	]
    	});
	});
</script>
<script>
	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
	    var url = "{{url('ulang-tahun/')}}/"+"{{$satuan}}/"+val;
	    window.location.href= url;
	});
</script>
@endsection
