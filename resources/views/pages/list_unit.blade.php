<?php
	use App\Model\UnitKerja;
	use App\Model\Pegawai;
	$uk_parent = UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', '1')->where('status',1)->orderBy('unit_kerja_left')->get();
	$uker=UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->select('unit_kerja_id')->where('status',1)->orderBy('unit_kerja_left')->get();
	$unit = array();
	foreach ($uker as $key => $value) {
		$unit[] = $value->unit_kerja_id; 
	}

	//$peg = Pegawai::WhereIn('unit_kerja_id', $unit)->orWhere('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count();
		
	$peg = Pegawai::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count();
	$non_unit = Pegawai::where('satuan_kerja_id', $satker->satuan_kerja_id)->whereNull('unit_kerja_id')->where('peg_status', 'TRUE')->count();
?>

@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-xs-12">
		@if(Auth::user()->role_id == '3' || Auth::user()->role_id == '1' || Auth::user()->role_id == '6')
			<center>
			 @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
                App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
            </center>
		@endif
			@include('form.radio_padded',['label'=>'','required'=>false,'name'=>'non','data'=>[
            'non_unit_kerja' => 'Tanpa Unit Kerja','non_satuan_kerja' => 'Tanpa Satuan Kerja' ]])
		<br><br>
		<h3 style="text-align:center">List Unit Kerja di {{ $satker->satuan_kerja_nama }}</h3>
	 	<hr>
	 	<a href="{{url('/list-all-pegawai',$satker->satuan_kerja_id)}}" class="btn btn-primary btn-xs pull-right">
			Lihat Semua Pegawai
		</a>
		<br><br>
		<div class="table-responsive">
	  <table id="list-unit" class="table table-striped table-bordered table-hover">
	     <thead>
	     <tr class="bg-info">
	     	 <th>SOPD</th>
	         <th>Unit Kerja</th>
	         <th>Jumlah Pegawai</th>
	         <th>Pilihan</th>
	     </tr>
	     </thead>
	     <tbody>
	     	<tr>
	     		<td>{{ $satker->satuan_kerja_nama }}</td>
	     		<td></td>
	     		<td>{{ $peg }}</td>
	     		<td>
		     		<a href="{{url('/list-all-pegawai',$satker->satuan_kerja_id)}}" class="btn btn-warning btn-xs">
						Lihat Data Pegawai
					</a>
				</td>
	     	</tr>
	     	<tr>
	     		<td>Tidak Ada Unit Kerja</td>
	     		<td></td>
	     		<td>{{ $non_unit }}</td>
	     		<td>
		     		<a href="{{url('list-pegawai/non-unit',$satker->satuan_kerja_id)}}" class="btn btn-warning btn-xs">
						Lihat Data Pegawai
					</a>
				</td>
	     	</tr>		
			@foreach($uk_parent as $up)
				<?php 
				$peg2 = Pegawai::where('unit_kerja_id', $up->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count();
				$uk_child = UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', '2')->where('status',1)->where('unit_kerja_parent', $up->unit_kerja_id)->orderBy('unit_kerja_left')->get(); ?>
	     	<tr>
	     		<td></td>
	     		<td>{{$up->unit_kerja_nama}}</td>
	     		<td>{{ $peg2 }}</td>
	     		<td>
	     			<a href="{{url('/list-pegawai/all',$up->unit_kerja_id)}}" class="btn btn-warning btn-xs">
						Lihat Data Pegawai
					</a>
				</td>
	     	</tr>
	     		@foreach($uk_child as $uc)
	     			<?php
	     				$peg3 = Pegawai::where('unit_kerja_id', $uc->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count(); 
	     				$uk_child2 = UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('status',1)->where('unit_kerja_level', '3')->where('unit_kerja_parent', $uc->unit_kerja_id)->orderBy('unit_kerja_left')->get(); ?>
	     			<tr>
	     				<td></td>
			     		<td style="padding-left:50px">{{$uc->unit_kerja_nama}}</td>
			     		<td>{{ $peg3 }}</td>
			     		<td>
			     		<a href="{{url('/list-pegawai/all',$uc->unit_kerja_id)}}" class="btn btn-warning btn-xs">
							Lihat Data Pegawai
						</a>
						</td>
			     	</tr>
			     	@foreach($uk_child2 as $uc2)
			     		<?php
			     			$peg4 = Pegawai::where('unit_kerja_id', $uc2->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count(); 
			     		?>
			     		<tr>
			     			<td></td>
				     		<td style="padding-left:100px">{{$uc2->unit_kerja_nama}}</td>
				     		<td>{{$peg4}}</td>
				     		<td>
					     		<a href="{{url('/list-pegawai/all',$uc2->unit_kerja_id)}}" class="btn btn-warning btn-xs">
									Lihat Data Pegawai
								</a>
							</td>
				     	</tr>
			     	@endforeach
	     		@endforeach
	     	@endforeach
	     </tbody>
	 </table>
	 </div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
	    var val = $("#satuan_kerja_id option:selected").val();
	    var url = "{{url('list-unit')}}/"+val;
	    window.location.href= url;
	});

	$('#non_non_unit_kerja').change(function() {
	    var url = "{{url('list-pegawai/non-unit',$satker->satuan_kerja_id)}}";
	    window.location.href= url;
	});

	$('#non_non_satuan_kerja').change(function() {
	    var url = "{{url('list-pegawai/non-satuan-kerja/all')}}";
	    window.location.href= url;
	});
</script>
@endsection