@extends('layouts.app')
<?php
    use App\Model\SatuanKerja;
    use App\Model\UnitKerja;
    use App\Model\Role;
?>
@section('content')
<div class="container-fluid">
	<div class="row">
<h1>Data User</h1>
@if (session('message'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <strong>Whoops!</strong> Terjadi Kesalahan Input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <hr>
<button class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#modal-user" onclick="addUser()"><i class="ace-icon glyphicon glyphicon-plus"></i>Tambah User</button>
<br><br>
<div class="table-responsive">
  <table id="posts-table" class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Nama</th>
         <th>Username</th>
         <th>Role</th>
         <th>Satuan Kerja</th>
         <th>Status Aktif</th>
         <th>Actions</th>
     </tr>
       </thead>
    <tbody>
    @foreach($user as $u)
    <?php 
        $satker = SatuanKerja::where('satuan_kerja_id', $u->satuan_kerja_id)->first(); 
        $role = Role::where('id', $u->role_id)->first();
        $uker = UnitKerja::where('unit_kerja_id',$u->unit_kerja_id)->first();
    ?>
    <tr>
        <td>{{$u->nama}}</td>
        <td>{{$u->username}}</td>
        <td>{{$role ? $role->role_name : 'ERROR'}}</td>
        <td>{{$satker ? $satker['satuan_kerja_nama'] : '-'}}{{$u->role_id == 5 ? ($uker ? " - ".$uker->unit_kerja_nama : "") : ""}}</td>
        <td>{{$u->status_aktif == 1 ? 'Aktif' : 'Tidak Aktif'}}</td>
        <td>
            <button class="btn btn-warning btn-xs"
                data-toggle="modal" data-target="#modal-user" 
                data-username="{{$u->username}}" 
                data-id="{{$u->id}}" 
                data-nama="{{$u->nama}}" 
                data-satuan-kerja="{{$u->satuan_kerja_id}}"
                data-unit-kerja="{{$u->unit_kerja_id}}"
                data-role="{{$u->role_id}}"
                data-status="{{$u->status_aktif}}"
                onclick="editUser(this)">
                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                Edit</button>
            <a href="{{url('/user/delete',$u->id)}}" 
                class="btn btn-danger btn-xs" onclick="return confirm('Apa anda yakin?')">
                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    Delete
            </a>

        </td>
    </tr>
    @endforeach
   </tbody>
 </table>
 </div>
</div>
</div>

<div class="modal fade" id="modal-user" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="labelUser"><div id="modal-button-edit"></div></h4>
        </div>
        <form method="POST" action="{{url('/user/add')}}" id="userForm">
            {!! csrf_field() !!}
        <div class="modal-body" id="modal-detail-content">
            @include('form.text2',['label'=>'Nama','required'=>false,'name'=>'nama','placeholder'=>''])
            <br>
            @include('form.text2',['label'=>'Username','required'=>false,'name'=>'username','placeholder'=>''])
            <br>
            <div id="add_pass" style="display:none">
                @include('form.password',['label'=>'Password','required'=>false,'name'=>'password','placeholder'=>''])
                <br>
            </div>
            <div id="edit_pass" style="display:none">
                <?php /*
                @include('form.password',['label'=>'Password Lama','required'=>false,'name'=>'password_lama','placeholder'=>''])
                <br>
                */ ?>
                @include('form.password',['label'=>'Password Baru','required'=>false,'name'=>'password_baru','placeholder'=>''])
                <br>
            </div>
            @include('form.select2_modal',['label'=>'Role','required'=>false,'name'=>'role_id','data'=>
            Role::lists('role_name','id') ,'empty'=>'-Pilih-'])
            <br>
            @include('form.select2_modal',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
            SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id') ,'empty'=>'-Pilih-'])
            <br id="uker_br">
            <div class="row" id="uker_id">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Unit Kerja</label>
                    <div class="col-md-8">
                        <select id="unit_kerja_id" name="unit_kerja_id" style="width:300px">
                        </select>
                    </div>
                </div>
            </div>
            </div>
            <br>
            @include('form.select2_modal',['label'=>'Status Aktif','required'=>false,'name'=>'status_aktif','data'=>
            ['1'=>'Aktif', '0'=>'Tidak Aktif']])
        </div>
        <div class="modal-footer">
            <button id="submitUser" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    $('#posts-table').DataTable({
    });
    $('#uker_id').hide();
    $('#uker_br').hide();
});

$('#role_id').on('change',function(){
    var val = $(this).val();
    if(val == 5){
        $('#uker_id').show();
        $('#uker_br').show();
        $('#unit_kerja_id').select2({
            width: '100%'
        });
    }else if(val == 8){
        $('#uker_id').show();
        $('#uker_br').show();
        $('#satuan_kerja_id').val(21).trigger('change');
        $('#unit_kerja_id').select2({
            width: '100%'
        });
    }else if(val == 9){
        $('#uker_id').show();
        $('#uker_br').show();
        $('#satuan_kerja_id').val(29).trigger('change');
        $('#unit_kerja_id').select2({
            width: '100%'
        });
    }else{
        $('#uker_id').hide();
        $('#uker_br').hide();
        $('#unit_kerja_id').val('').trigger('change');
    }
});
$('#satuan_kerja_id').on('change', function (e){
    var v = $(this).val();
    var role = $('#role_id').val();

    if(role == 8 && v != 21){
        $('#role_id').val('').trigger('change');
    }

    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#unit_kerja_id").empty();
            $("#unit_kerja_id").select2("val", "");
            $('#unit_kerja_id').append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
        },
        error: function (e){
            console.log(e);
            // alert("ERROR!");
        },
    });
});

var addUser = function(){
    $("#userForm").attr("action","{{ URL::to('user/add/') }}");
    $("#labelUser").text("Tambah Data User");
    $("#edit_pass").hide();
    $("#add_pass").show();

    $(".nama").val("");
    $(".username").val("");
    $("#password").val("");
    $("#role_id").val("").select2();
    $('#unit_kerja_id').val('').trigger('change');
    $("#satuan_kerja_id").val("").select2();
    $("#status_aktif").val("").select2();
}
var editUser = function(e){
    $("#userForm").attr("action","{{ URL::to('user/update/') }}/"+$(e).data('id'));
    $("#labelUser").text("Edit Data User");
    $("#edit_pass").show();
    $("#add_pass").hide();

    $(".nama").val($(e).data('nama'));
    $(".username").val($(e).data('username'));
    $("#role_id").val($(e).data('role')).select2();
    $("#satuan_kerja_id").val($(e).data('satuan-kerja')).trigger('change').select2();
    if($(e).data('role') == 5 || $(e).data('role') == 8){
        $('#uker_id').show();
        $('#uker_br').show();
        $('#unit_kerja_id').val($(e).data('unit-kerja'));
        $('#unit_kerja_id').select2({
            width: '100%'
        });
    }else{
        $('#uker_id').hide();
        $('#uker_br').hide();
        $('#unit_kerja_id').val('').trigger('change');
    }
    $("#status_aktif").val($(e).data('status')).select2();
}
</script>
@endsection
