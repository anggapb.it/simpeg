@extends('layouts.app')

@section('content')
@if($count==0)
	<p>Data Tidak Ditemukan!</p>
@endif
@foreach($pegawai as $p)
<?php
	$satker = App\Model\SatuanKerja::where('satuan_kerja_id', $p['satuan_kerja_id'])->first();
	
?>	
<div class="row">

	<div class="col-xs-12">
	 	<hr>
		<br><br>
		
		<div class="profile-user-info profile-user-info-striped">
			<div class="profile-info-row">
				<div class="profile-info-name"> Nama Pegawai </div>

				<div class="profile-info-value">
					<span class="editable" id="username">{{ $p['peg_nama'] }}</span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> NIP </div>

				<div class="profile-info-value">
					<span class="editable" id="username">{{ $p['peg_nip'] }}</span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> Satuan Kerja Asal</div>

				<div class="profile-info-value">
					<span class="editable" id="age">{{ $satker['satuan_kerja_nama']}}</span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> Status Pegawai </div>
				@if($p['peg_status'] == 'TRUE')
				<div class="profile-info-value">
					<span class="editable" id="username">Pegawai Aktif</span>
				</div>
				@else
				<div class="profile-info-value">
					<span class="editable" id="username">Pegawai Pensiun/Meninggal</span>
				</div>
				@endif
			</div>
		</div>
		<br>
		<a href="{{url('/mutasi-masuk',$p['peg_id'])}}" class="btn btn-success btn-xs">Mutasi Masuk</a>
	</div>
</div>
@endforeach
@endsection