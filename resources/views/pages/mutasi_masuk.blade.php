@extends('layouts.app')
<?php
	$uk = App\Model\UnitKerja::where('satuan_kerja_id', $satker['satuan_kerja_id'])->orderBy('unit_kerja_nama')->lists('unit_kerja_nama','unit_kerja_id')->all();
	$id_satker =$satker['satuan_kerja_id'];
?>	
@section('content')
<div class="bs-callout bs-callout-warning hidden">
  <h4>Edit Data Gagal!</h4>
  <p>Data Harus Lengkap!</p>
</div>
<div class="row">
	<div class="col-xs-12">
        @if(Auth::user()->role_id == 2)
		<h3 style="text-align:center">Mutasi Masuk Pegawai ke {{ $satker['satuan_kerja_nama']}}</h3>
        @else
        <h3 style="text-align:center">Mutasi Masuk Pegawai</h3>
        @endif
	 	<hr>
		<br><br>
		<div class="profile-user-info profile-user-info-striped">
			<div class="profile-info-row">
				<div class="profile-info-name"> Nama Pegawai </div>

				<div class="profile-info-value">
					<span class="editable" id="username">{{ $pegawai['peg_nama'] }}</span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> NIP </div>

				<div class="profile-info-value">
					<span class="editable" id="username">{{ $pegawai['peg_nip'] }}</span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> Satuan Kerja Asal</div>

				<div class="profile-info-value">
					<span class="editable" id="age">{{ $satker_asal['satuan_kerja_nama']}}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('mutasi-masuk-skpd/'.$pegawai['peg_id']) }}" id="form-mutasi"> 
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="satuan_kerja_id" value="{{ $id_satker }}">
		<br>
		<table class="table no-footer">
            @if(Auth::user()->role_id != 2)
            <tr>
            @include('form.select2',['label'=>'SOPD','required'=>true,'name'=>'satuan_kerja_id','data'=>
                App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>'','value'=>$satker['satuan_kerja_id']])
            </tr>
            @endif
			<tr>
                <td>Unit Kerja</td>
                <td>:</td>
                <td>
                    <select id="unit_kerja_id" name="unit_kerja_id" style="width:300px">
                    <?php 
                        $sat_ker = App\Model\SatuanKerja::where('satuan_kerja_id', $satker['satuan_kerja_id'])->lists('satuan_kerja_nama', 'satuan_kerja_id'); 
                    ?>
                     <option value="">-Pilih-</option>
                    @foreach($sat_ker as $id_st => $nama_st)
                         <option value="">{{$nama_st}}</option>
                        <?php
                            $uk = App\Model\UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 1)->lists('unit_kerja_nama', 'unit_kerja_id');
                        ?>
                        @foreach ($uk as $id_unit => $nama_unit)
                             <option value="{{$id_unit}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit}}</option>
                             <?php 
                                $uk2 = App\Model\UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 2)->where('unit_kerja_parent', $id_unit)->lists('unit_kerja_nama', 'unit_kerja_id');
                             ?>
                            @foreach ($uk2 as $id_unit2 => $nama_unit2)
                                <option value="{{$id_unit2}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit2}}</option>
                                <?php 
                                    $uk3 = App\Model\UnitKerja::where('satuan_kerja_id', $id_st)->where('unit_kerja_level', 3)->where('unit_kerja_parent', $id_unit2)->lists('unit_kerja_nama', 'unit_kerja_id');
                                ?>
                                @foreach ($uk3 as $id_unit3 => $nama_unit3)
                                    <option value="{{$id_unit3}}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$nama_unit3}}</option>
                                @endforeach
                            @endforeach
                        @endforeach
                    @endforeach
                    </select>
                </td>
            </tr>
	    	<tr>
			     @include('form.select2',['label'=>'Jenis Jabatan','required'=>true,'name'=>'jenis_jabatan','data'=>
			     ['2' => 'Struktural','3' => 'Fungsional Tertentu', '4'=>'Fungsional Umum' ],'empty'=>'-Pilih-', 'value'=>'', 'required'=>true])
			</tr>
			<tr>
		    	@include('form.select2',['label'=>'Nama Jabatan','required'=>true,'name'=>'jabatan_id','data'=>
		             [''=>'-Pilih-'],'empty'=>'-Pilih-', 'required'=>true])
			    <td> <div id="tampilJenisButton" style="display:none">
			            <button data-toggle="modal" data-target="#modal-jabatan-fu">Tambah Jabatan Fungsional Umum</button>
			        </div>
			    </td>
		    </tr>
		    <tr>
			    <td colspan="9" align="center">
			        <input type="submit" value="Simpan" name="simpan">
			    </td>
			</tr>
	     </table>
		</form>
	</div>
</div>
@endsection

@section('scripts')
<script>
$('#form-mutasi').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })

$('#satuan_kerja_id').select2({ width: '400px' }).on('change', function (e){
    var v = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#unit_kerja_id").empty();
            $("#unit_kerja_id").select2("val", ""); 
            $('#unit_kerja_id').append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            getJabatan();
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
});

$('#unit_kerja_id').select2({ width: '500px' }).on('change', function(e){
    getJabatan();
});

$('#jenis_jabatan').select2({ width: '370px' }).on('change', function (e){
   getJabatan();
});

function getJabatan(){
    var v = $('#jenis_jabatan').val();
    var unit_kerja_id = $('#unit_kerja_id').val();
    // var satuan_kerja_id = "{{$id_satker}}";
    var satuan_kerja_id = $('#satuan_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getJabatanNew') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"jenis_jabatan":v, "unit_kerja_id":unit_kerja_id, "satuan_kerja_id":satuan_kerja_id},
        dataType: 'json',
        success: function(data) {
            $("#jabatan_id").select2("val", "");
            $("#jabatan_id").empty();
            for (row in data) {
               // console.log(data['jenis']);
                var jab = data['jabatan'].jabatan_id;
                if(data['jenis'] == 3){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsional') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jf_id":data[row].jf_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jf_id).text(data[row].jf_nama));
                            }
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else if(data['jenis'] == 4){
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "{{ URL::to('pegawai/jabatanFungsionalUmum') }}",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        data: {"jfu_id":data[row].jfu_id},
                        dataType: 'json',
                        success: function(data) {
                            for (row in data) {
                                //console.log(data);
                                $('#jabatan_id').append($('<option></option>').attr('value', data[row].jfu_id).text(data[row].jfu_nama));
                            }

                           // console.log($('#jabatan_id').val());
                        }
                    }).error(function (e){
                        console.log(e);
                        alert("ERROR!");
                    });
                }else if(data['jenis'] == 2){
                   $('#jabatan_id').empty();
                    for(r in data['jabatan']){

                   // console.log(data['jabatan'][r].jabatan_nama);
                    $('#jabatan_id').append($('<option></option>').attr('value', data['jabatan'][r].jabatan_id).text(data['jabatan'][r].jabatan_nama));
                    }
                }
            }
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
}

$('#jabatan_id').select2({ width: '350px' });
</script>
@endsection