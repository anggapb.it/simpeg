@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-xs-12">
		<h3 style="text-align:center">Mutasi Masuk Pegawai</h3>
	 	<hr>
		<br><br>
	  	<form action="{{url('/search-pegawai')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		{!! csrf_field() !!}
		  <table class="table no-footer">
	        <tr>
	            @include('form.txt',['label'=>'NIP/Nama Pegawai','required'=>false,'name'=>'search','empty'=>'','value'=>''])
	        </tr>
	        <tr>
			    <td colspan="9" align="center">
			        <input type="submit" value="Cari" name="simpan" class="btn btn-xs">
			    </td>
			</tr>
    	</table>
    	</form>
	</div>
</div>
@endsection

@section('scripts')
<script>

</script>
@endsection