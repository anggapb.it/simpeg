@extends('layouts.app')

@section('content')
<?php
	use App\Model\Agama;
	use App\Model\Kecamatan;
	use App\Model\Kabupaten;
	use App\Model\Propinsi;
	use App\Model\GolonganDarah;
	use App\Model\KategoriPendidikan;
	use App\Model\Pendidikan;
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;
	use App\Model\Eselon;
	use App\Model\Gaji;
	use App\Model\GajiTahun;
	use App\Model\StatusEditPegawai;
	use App\Model\StatusEditPegawaiLog;
	use App\Model\StatusKepegawaian;
	use App\Model\Pegawai;
	use App\Model\PegawaiPensiun;
	use App\Model\SatuanKerja;
	use App\Model\UnitKerja;
	use App\Model\Kedudukan;

	$kedudukan = Kedudukan::all();
	$submit = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 2)->orderBy('id','desc')->first();
	$revisi = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 3)->orderBy('id','desc')->first();
	$draft = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 1)->orderBy('id','desc')->first();
	$terima = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 4)->orderBy('id','desc')->first();
	$usulskpd = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 5)->orderBy('id','desc')->first();
	$revisiskpd = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->where('status_id', 6)->orderBy('id','desc')->first();
	$pensiun = StatusEditPegawai::where('peg_id', $pegawai->peg_id)->orderBy('id','desc')->first();
	$st_pensiun = null;
	$st_pensiun_batal = null;

	if($pensiun && $pensiun['action'] == 'pensiun_pegawai'){
		$st_pensiun = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pensiun_pegawai')->first();
	}
	if($pensiun && $pensiun['action'] == 'undo_pensiun_pegawai'){
		$st_pensiun_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pensiun_pegawai')->first();
	}


	$st_meninggal = null;
	$st_meninggal_batal = null;
	$st_pindah = null;
	$st_pindah_batal = null;

	if($pensiun && $pensiun['action'] == 'pegawai_meninggal'){
		$st_meninggal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pegawai_meninggal')->first();
	}
	if ($pensiun && $pensiun['action'] == 'undo_pegawai_meninggal') {
		$st_meninggal_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pegawai_meninggal')->first();
	}
	if($pensiun && $pensiun['action'] == 'pindah_pegawai'){
		$st_pindah = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'pindah_pegawai')->first();
	}
	if ($pensiun && $pensiun['action'] == 'undo_pindah_pegawai') {
		$st_pindah_batal = StatusEditPegawaiLog::where('sed_id', $pensiun['id'])->where('action', 'undo_pindah_pegawai')->first();
	}
	
	//dd($st_pensiun_batal);
	//DB SKPD
	if($st_pensiun){
		$peg_pensiun = PegawaiPensiun::where('peg_id',$pegawai->peg_id)->where('pensiun_id',1)->first();
	}
	if($pegawai->unit_kerja_id){
		$uker=UnitKerja::where('unit_kerja_id', $pegawai->unit_kerja_id)->first();
	}else{
		$uker =null;
	}
	$agama = Agama::where('id_agama', $pegawai->id_agama)->first();	
	$goldar = GolonganDarah::where('id_goldar', $pegawai->id_goldar)->first();
	$kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
		if($kecamatan){
			$kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
		}else{
			$kab = null;
		}

	$pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
	$pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
		if($pend_awal){
			$kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first(); 
		}else{
			$kat_pend_awal = null;
		}
		if($pend_akhir){
			$kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first(); 
		}else{
			$kat_pend_akhir = null;
		}

	$golongan_awal = Golongan::where('gol_id', $pegawai->gol_id_awal)->first();
	$golongan_akhir = Golongan::where('gol_id', $pegawai->gol_id_akhir)->first();

	$jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id)->first();
		if($jabatan){
			$eselon = Eselon::where('eselon_id', $jabatan->eselon_id)->first();
			if($jabatan->jabatan_jenis == 3){
				$jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
				if($jf){
					$jabatan->jabatan_nama = $jf['jf_nama'];
				}else{
					$jabatan->jabatan_nama = 'Pelaksana';
				}
			}elseif($jabatan->jabatan_jenis == 4){
				$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
				if($jfu){
					$jabatan->jabatan_nama = $jfu['jfu_nama'];
				}else{
					$jabatan->jabatan_nama = 'Pelaksana';
				}

			}
		}else{
			$eselon = null;
		}
	if($pegawai->peg_kerja_tahun == '-'){
			$pegawai->peg_kerja_tahun == 0;
	}
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $pegawai->gol_id_akhir)->where('gaji_masakerja', $pegawai->peg_kerja_tahun)->first();
	
	$id = $pegawai->peg_id;
	//dd($pegawai);
	if($pegawai->peg_nama == null){
		$pegawai->peg_nama = '';
	}

	if($pegawai->peg_gelar_depan == ""){
		$pegawai->peg_gelar_depan = '';
	}

	if($pegawai->peg_gelar_belakang == ""){
		$pegawai->peg_gelar_belakang = '';
	}

	if($pegawai->peg_nip == null){
		$pegawai->peg_nip = '';
	}

	if($pegawai->peg_nip_lama == ""){
		$pegawai->peg_nip_lama = '';
	}

	if($pegawai->peg_lahir_tempat == ""){
		$pegawai->peg_lahir_tempat = '';
	}

	if($pegawai->peg_lahir_tanggal == null){
		$pegawai->peg_lahir_tanggal = '';
	}

	if($pegawai->peg_jenis_kelamin == null){
		$pegawai->peg_jenis_kelamin = '';
	}
	if($pegawai->id_agama == null){
		$pegawai->id_agama = '';
	}

	if($pegawai->peg_status_perkawinan == null){
		$pegawai->peg_status_perkawinan = '';
	}

	if($pegawai->peg_status_kepegawaian == null){
		$pegawai->peg_status_kepegawaian = '';
	}
	if($pegawai->peg_cpns_tmt == null){
		$pegawai->peg_cpns_tmt = '';
	}
	if($pegawai->peg_pns_tmt == null){
		$pegawai->peg_pns_tmt = '';
	}
	if($pegawai->id_pend_awal == null){
		$pegawai->id_pend_awal = '';
	}
	if($pegawai->peg_pend_awal_th == null){
		$pegawai->peg_pend_awal_th = '';
	}
	if($pegawai->id_pend_akhir == null){
		$pegawai->id_pend_akhir = '';
	}
	if($pegawai->peg_pend_akhir_th == null){
		$pegawai->peg_pend_akhir_th = '';
	}
	if($pegawai->jabatan_id == null){
		$pegawai->jabatan_id = '';
	}
	if($pegawai->peg_jabatan_tmt == null){
		$pegawai->peg_jabatan_tmt = '';
	}
	if($pegawai->peg_instansi_dpk == null){
		$pegawai->peg_instansi_dpk = '';
	}
	if($pegawai->gol_id_awal == null){
		$pegawai->gol_id_awal = '';
	}
	if($pegawai->peg_gol_awal_tmt == null){
		$pegawai->peg_gol_awal_tmt = '';
	}
	if($pegawai->gol_id_akhir == null){
		$pegawai->gol_id_akhir = '';
	}
	if($pegawai->peg_gol_akhir_tmt == null){
		$pegawai->peg_gol_akhir_tmt = '';
	}
	if($pegawai->peg_kerja_tahun == null){
		$pegawai->peg_kerja_tahun = 0;
	}
	if($pegawai->peg_kerja_bulan == null){
		$pegawai->peg_kerja_bulan = '';
	}
	if($pegawai->peg_karpeg == ""){
		$pegawai->peg_karpeg = '';
	}
	if($pegawai->peg_karsutri == ""){
		$pegawai->peg_karsutri = '';
	}
	if($pegawai->peg_no_askes == null){
		$pegawai->peg_no_askes = '';
	}
	if($pegawai->peg_ktp == null){
		$pegawai->peg_ktp = '';
	}
	if($pegawai->peg_npwp == null){
		$pegawai->peg_npwp = '';
	}
	if($pegawai->id_goldar == null){
		$pegawai->id_goldar = '';
	}
	if($pegawai->peg_bapertarum == null){
		$pegawai->peg_bapertarum = '';
	}
	if($pegawai->peg_tmt_kgb == null){
		$pegawai->peg_tmt_kgb = '';
	}
	if($pegawai->peg_rumah_alamat == null){
		$pegawai->peg_rumah_alamat = '';
	}
	if($pegawai->peg_kel_desa == ""){
		$pegawai->peg_kel_desa = '';
	}
	if($pegawai->peg_kodepos == ""){
		$pegawai->peg_kodepos = '';
	}
	if($pegawai->peg_telp == ""){
		$pegawai->peg_telp = '';
	}
	if($pegawai->peg_telp_hp == ""){
		$pegawai->peg_telp_hp = '';
	}
	if($pegawai->peg_email == ""){
		$pegawai->peg_email = '';
	}
	if($pegawai->satuan_kerja_id == null){
		$pegawai->satuan_kerja_id = '';
	}
	if($pegawai->kecamatan_id == null){
		$pegawai->kecamatan_id = '';
	}

	if($pegawai->peg_status_asn == null){
		$pegawai->peg_status_asn = '';
	}

	if($pegawai->peg_status_gaji == null){
		$pegawai->peg_status_gaji = '';
	}

	if($pegawai->id_status_kepegawaian == null){
		$pegawai->id_status_kepegawaian = '';
	}


	//db bkd
	$pegawai_bkd= Pegawai::where('peg_id', $pegawai->peg_id)->first();

	if($pegawai_bkd){
		$agama_bkd = Agama::where('id_agama', $pegawai_bkd->id_agama)->first();	
		$goldar_bkd = GolonganDarah::where('id_goldar', $pegawai_bkd->id_goldar)->first();
		if($pegawai_bkd->unit_kerja_id){
			$uker_bkd=UnitKerja::where('unit_kerja_id', $pegawai_bkd->unit_kerja_id)->first();
		}else{
			$uker_bkd =null;
		}

		$kecamatan_bkd = Kecamatan::where("kecamatan_id", $pegawai_bkd->kecamatan_id)->first();
			if($kecamatan_bkd){
				$kab_bkd = Kabupaten::where('kabupaten_id', $kecamatan_bkd->kabupaten_id)->first();
			}else{
				$kab_bkd = null;
			}

		$pend_awal_bkd = Pendidikan::where('id_pend', $pegawai_bkd->id_pend_awal)->first();
		$pend_akhir_bkd = Pendidikan::where('id_pend', $pegawai_bkd->id_pend_akhir)->first();
			if($pend_awal_bkd){
				$kat_pend_awal_bkd = KategoriPendidikan::where('kat_pend_id', $pend_awal_bkd->kat_pend_id)->first(); 
			}else{
				$kat_pend_awal_bkd= null;
			}
			if($pend_akhir_bkd){
				$kat_pend_akhir_bkd = KategoriPendidikan::where('kat_pend_id', $pend_akhir_bkd->kat_pend_id)->first(); 
			}else{
				$kat_pend_akhir_bkd = null;
			}

		$golongan_awal_bkd = Golongan::where('gol_id', $pegawai_bkd->gol_id_awal)->first();
		$golongan_akhir_bkd = Golongan::where('gol_id', $pegawai_bkd->gol_id_akhir)->first();

		$jabatan_bkd = Jabatan::where('jabatan_id', $pegawai_bkd->jabatan_id)->first();
			if($jabatan_bkd){
				$eselon_bkd = Eselon::where('eselon_id', $jabatan_bkd['eselon_id'])->first();
				if($jabatan_bkd->jabatan_jenis == 3){
					$jf = JabatanFungsional::where('jf_id', $jabatan_bkd['jf_id'])->first();
					$jabatan_bkd->jabatan_nama = $jf['jf_nama'];
				}elseif($jabatan_bkd->jabatan_jenis == 4){
					$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan_bkd['jfu_id'])->first();
					if($jfu){
						$jabatan_bkd->jabatan_nama = $jfu['jfu_nama'];
					}else{
						$jabatan_bkd->jabatan_nama = 'Pelaksana';
					}
				}
			}else{
				$eselon_bkd = null;
			}
		if($pegawai_bkd->peg_kerja_tahun == '-'){
			$pegawai_bkd->peg_kerja_tahun == 0;
		}
		$gaji_bkd = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $pegawai_bkd->gol_id_akhir)->where('gaji_masakerja', $pegawai_bkd->peg_kerja_tahun)->first();
		$satker_bkd = SatuanKerja::where('satuan_kerja_id', $pegawai_bkd->satuan_kerja_id)->first();
		if($pegawai_bkd->peg_nama == null){
			$pegawai_bkd->peg_nama = '';
		}

		if($pegawai_bkd->peg_gelar_depan == ''){
			$pegawai_bkd->peg_gelar_depan = '';
		}

		if($pegawai_bkd->peg_gelar_belakang == ''){
			$pegawai_bkd->peg_gelar_belakang = '';
		}

		if($pegawai_bkd->peg_nip == null){
			$pegawai_bkd->peg_nip = '';
		}

		if($pegawai_bkd->peg_nip_lama == null){
			$pegawai_bkd->peg_nip_lama = '';
		}

		if($pegawai_bkd->peg_lahir_tempat == null){
			$pegawai_bkd->peg_lahir_tempat = '';
		}

		if($pegawai_bkd->peg_lahir_tanggal == null){
			$pegawai_bkd->peg_lahir_tanggal = '';
		}

		if($pegawai_bkd->peg_jenis_kelamin == null){
			$pegawai_bkd->peg_jenis_kelamin = '';
		}

		if($pegawai_bkd->peg_status_perkawinan == null){
			$pegawai_bkd->peg_status_perkawinan = '';
		}
		if($pegawai_bkd->id_agama == null){
			$pegawai_bkd->id_agama = '';
		}

		if($pegawai_bkd->peg_status_kepegawaian == null){
			$pegawai_bkd->peg_status_kepegawaian = '';
		}
		if($pegawai_bkd->peg_cpns_tmt == null){
			$pegawai_bkd->peg_cpns_tmt = '';
		}
		if($pegawai_bkd->peg_pns_tmt == null){
			$pegawai_bkd->peg_pns_tmt = '';
		}
		if($pegawai_bkd->id_pend_awal == null){
			$pegawai_bkd->id_pend_awal = '';
		}
		if($pegawai_bkd->peg_pend_awal_th == null){
			$pegawai_bkd->peg_pend_awal_th = '';
		}
		if($pegawai_bkd->id_pend_akhir == null){
			$pegawai_bkd->id_pend_akhir = '';
		}
		if($pegawai_bkd->peg_pend_akhir_th == null){
			$pegawai_bkd->peg_pend_akhir_th = '';
		}
		if($pegawai_bkd->jabatan_id == null){
			$pegawai_bkd->jabatan_id = '';
		}
		if($pegawai_bkd->peg_jabatan_tmt == null){
			$pegawai_bkd->peg_jabatan_tmt = '';
		}
		if($pegawai_bkd->peg_instansi_dpk == null){
			$pegawai_bkd->peg_instansi_dpk = '';
		}
		if($pegawai_bkd->gol_id_awal == null){
			$pegawai_bkd->gol_id_awal = '';
		}
		if($pegawai_bkd->peg_gol_awal_tmt == null){
			$pegawai_bkd->peg_gol_awal_tmt = '';
		}
		if($pegawai_bkd->gol_id_akhir == null){
			$pegawai_bkd->gol_id_akhir = '';
		}
		if($pegawai_bkd->peg_gol_akhir_tmt == null){
			$pegawai_bkd->peg_gol_akhir_tmt = '';
		}
		if($pegawai_bkd->peg_kerja_tahun == null){
			$pegawai_bkd->peg_kerja_tahun = 0;
		}
		if($pegawai_bkd->peg_kerja_bulan == null){
			$pegawai_bkd->peg_kerja_bulan = '';
		}
		if($pegawai_bkd->peg_karpeg == null){
			$pegawai_bkd->peg_karpeg = '';
		}
		if($pegawai_bkd->peg_karsutri == null){
			$pegawai_bkd->peg_karsutri = '';
		}
		if($pegawai_bkd->peg_no_askes == null){
			$pegawai_bkd->peg_no_askes = '';
		}
		if($pegawai_bkd->peg_ktp == null){
			$pegawai_bkd->peg_ktp = '';
		}
		if($pegawai_bkd->peg_npwp == null){
			$pegawai_bkd->peg_npwp = '';
		}
		if($pegawai_bkd->id_goldar == null){
			$pegawai_bkd->id_goldar = '';
		}
		if($pegawai_bkd->peg_bapertarum == null){
			$pegawai_bkd->peg_bapertarum = '';
		}
		if($pegawai_bkd->peg_tmt_kgb == null){
			$pegawai_bkd->peg_tmt_kgb = '';
		}
		if($pegawai_bkd->peg_rumah_alamat == null){
			$pegawai_bkd->peg_rumah_alamat = '';
		}
		if($pegawai_bkd->peg_kel_desa == null){
			$pegawai_bkd->peg_kel_desa = '';
		}
		if($pegawai_bkd->peg_kodepos == null){
			$pegawai_bkd->peg_kodepos = '';
		}
		if($pegawai_bkd->peg_telp == null){
			$pegawai_bkd->peg_telp = '';
		}
		if($pegawai_bkd->peg_telp_hp == null){
			$pegawai_bkd->peg_telp_hp = '';
		}
		if($pegawai_bkd->peg_email == null){
			$pegawai_bkd->peg_email = '';
		}
		if($pegawai_bkd->satuan_kerja_id == null){
			$pegawai_bkd->satuan_kerja_id = '';
		}
		if($pegawai_bkd->kecamatan_id == null){
			$pegawai_bkd->kecamatan_id = '';
		}

		if($pegawai_bkd->peg_status_asn == null){
			$pegawai_bkd->peg_status_asn = '';
		}

		if($pegawai_bkd->peg_status_gaji == null){
			$pegawai_bkd->peg_status_gaji = '';
		}

		if($pegawai_bkd->id_status_kepegawaian == null){
			$pegawai_bkd->id_status_kepegawaian = '';
		}
		
	}

	$kepsek = false;
	if($jabatan){
		if (strpos(strtolower($jabatan->jabatan_nama), 'kepala sekolah') !== false) {
	    	$kepsek = true;
		}
	}elseif($jabatan_bkd){
		if (strpos(strtolower($jabatan_bkd->jabatan_nama), 'kepala sekolah') !== false) {
	    	$kepsek = true;
		}
	}

	$field = ["peg_status","peg_ketstatus"];

	$sama = true;
	if($pegawai && $pegawai_bkd){
		foreach ($field as $key) {
			if($pegawai->{$key} != $pegawai_bkd->{$key}){
				$sama = false;
			}
		}
	}
?>
<!-- #section:elements.tab.option -->
<div class="tabbable"> 
	<ul class="nav nav-tabs" id="myTab">
		@include('includes.nav_pegawai')
	</ul>

	<div class="tab-content">
		<!--DATA PRIBADI-->
		<div id="data-pribadi" class="tab-pane in active">
			<span class="pull-right">Terakhir Di Update : {{getFullDateTime($pegawai_bkd ? $pegawai_bkd->updated_at : $pegawai->updated_at)}}</span>
			@if($st_pensiun)
			<br><span class="pull-right">No SK Pensiun : {{$peg_pensiun ? $peg_pensiun->rpensiun_nosk : null}}</span><br>
			<span class="pull-right">Tanggal SK Pensiun : {{getFullDateTime($peg_pensiun ? $peg_pensiun->rpensiun_tglsk : null)}}</span>
			@endif
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				<br><br><br>
				<div class="col-xs-12">
				<div class="pull-right tableTools-container">
				@if (in_array(Auth::user()->role_id, canSkpdEdit() ? [1,3,2,8,9] : [1,3]))								
					@if($pegawai->peg_status)
						@if($usulskpd && isDisdik() && Auth::user()->role_id==2 || $revisiskpd && isDisdik() && Auth::user()->role_id==2 )
							<a href="{{url('/edit-pegawai',$pegawai['peg_id'])}}" class="btn btn-warning btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Edit</a>
							<button type="button" class="btn btn-danger btn-xs update-kedudukan" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#modal-kedudukan" data-status="pensiun" data-pegawai="{{$pegawai->peg_nama}}">
						   		<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i> Update Kedudukan Pegawai 
						  	</button>
						@endif
						@if(!$submit)
						@if(!$usulskpd)
							<a href="{{url('/edit-pegawai',$pegawai['peg_id'])}}" class="btn btn-warning btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Edit</a>
							<button type="button" class="btn btn-danger btn-xs update-kedudukan" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#modal-kedudukan" data-status="pensiun" data-pegawai="{{$pegawai->peg_nama}}">
						   		<i class="ace-icon fa fa-exclamation-triangle bigger-120"></i> Update Kedudukan Pegawai 
						  	</button>
							@if(($pegawai_bkd) && (($st_pensiun == null) || ($st_pensiun_batal!=null)) && (($st_meninggal == null) || ($st_meninggal_batal!=null)) && (($st_pindah == null) || ($st_pindah_batal!=null )) && Auth::user()->role_id != 8)
								<div class="btn-group">
						  			<button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						   			 <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i> Update Status Kepegawaian <i class="ace-icon fa fa-sort-desc bigger-120"></i>
						  			</button>
						  			<ul class="dropdown-menu">
						    			<li><a href="#" class="update-status" data-toggle="modal" data-target="#modal-stat" data-status="pensiun" data-pegawai="{{$pegawai->peg_nama}}">Set Pensiun</a></li>
						    			<li><a href="#" class="update-status" data-toggle="modal" data-target="#modal-stat" data-status="meninggal" data-pegawai="{{$pegawai->peg_nama}}">Set Meninggal Dunia</a></li>
						    			<li><a href="#" class="update-status" data-toggle="modal" data-target="#modal-stat" data-status="pindah" data-pegawai="{{$pegawai->peg_nama}}">Set Pindah ke Luar Pemkot Bandung</a></li>
						    			<li><a href="#" class="update-status" data-toggle="modal" data-target="#modal-stat" data-status="cuti" data-pegawai="{{$pegawai->peg_nama}}">Set CTLN / Tugas Belajar</a></li>
						  			</ul>
								</div>
							@endif
							@if((Auth::user()->role_id == 1 || Auth::user()->satuan_kerja_id == 21) && Auth::user()->role_id != 8 )
								<a href="{{url('/pegawai/mutasi-keluar',$pegawai->peg_id)}}" class="btn btn-inverse btn-xs" onclick="return confirm('Apa Anda Yakin?')"><i class="ace-icon fa fa-exchange bigger-120"></i>Mutasi Keluar</a>
							@endif
						@endif
						@endif
						@if($pegawai_bkd && (($st_pensiun && ($st_pensiun_batal==null)) || $pegawai_bkd->peg_ketstatus == 'Pensiun'))
							<a href="{{url('/pegawai/pensiun/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Pensiun</a>
						@endif
						@if($pegawai_bkd && $st_meninggal && ($st_meninggal_batal==null))
							<a href="{{url('/pegawai/update/meninggal/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Meninggal</a>
						@endif
						@if($pegawai_bkd && $st_pindah && ($st_pindah_batal==null))
							<a href="{{url('/pegawai/update/pindah/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Pindah Pegawai</a>
						@endif
						@if(Auth::user()->role_id == 1)
							@if($submit || $revisi)
								<button id="modal-revisi-show" class="btn btn-danger btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Revisi</button>
								<a href="{{url('/pegawai/approve',$pegawai->peg_id)}}" class="btn btn-primary btn-xs" onclick="return confirm('Apakah Anda yakin?')"><i class="ace-icon glyphicon glyphicon-ok"></i>Approve</a>
							@endif
						@endif
						@if(Auth::user()->role_id == 2 && isDisdik())
							@if($usulskpd || $revisiskpd)
								<button id="modal-revisi-show" class="btn btn-danger btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Revisi</button>
								<a href="{{url('/pegawai/approve',$pegawai->peg_id)}}" class="btn btn-primary btn-xs" onclick="return confirm('Apakah Anda yakin?')"><i class="ace-icon glyphicon glyphicon-ok"></i>Approve</a>
							@endif
						@endif

							@if(!$submit )
								@if(!$usulskpd)
									<a href="{{url('/submit-pegawai',$pegawai->peg_id)}}" class="btn btn-primary btn-xs" onclick="return confirm('Apa Anda yakin?')"><i class="ace-icon fa fa-floppy-o"></i>Submit</a>
								@endif
							@endif
					@else
						@if($st_pensiun)
							<button class="btn btn-warning btn-xs" id="edit-status" data-toggle="modal" data-target="#modal-stat" data-sk="{{$peg_pensiun ? $peg_pensiun->rpensiun_nosk : null}}" data-tanggal="{{$peg_pensiun ? $peg_pensiun->rpensiun_tglsk : null}}" data-status="pensiun" data-pegawai="{{$pegawai->peg_nama}}"><i class="ace-icon fa fa-edit"></i>Edit Data Pensiun</button>
						@endif
						@if($pegawai_bkd && (($st_pensiun && ($st_pensiun_batal==null)) || $pegawai_bkd->peg_ketstatus == 'Pensiun'))
							<a href="{{url('/pegawai/pensiun/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Pensiun</a>
						@endif
						@if($pegawai_bkd && $st_meninggal && ($st_meninggal_batal==null))
							<a href="{{url('/pegawai/update/meninggal/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Meninggal</a>
						@endif
						@if($pegawai_bkd && $st_pindah && ($st_pindah_batal==null))
							<a href="{{url('/pegawai/update/pindah/batal',$pegawai->peg_id)}}" class="btn btn-info btn-xs"><i class="ace-icon glyphicon glyphicon-remove"></i>Batalkan Status Pindah Pegawai</a>
						@endif
						@if(Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
							@if($submit || $revisi)
								<button id="modal-revisi-show" class="btn btn-danger btn-xs"><i class="ace-icon glyphicon glyphicon-pencil"></i>Revisi</button>
								<a href="{{url('/pegawai/approve',$pegawai->peg_id)}}" class="btn btn-primary btn-xs" onclick="return confirm('Apakah Anda yakin?')"><i class="ace-icon glyphicon glyphicon-ok"></i>Approve</a>
							@endif
						@endif
					@endif
				@endif
				@if(!$submit && Auth::user()->role_id != 8 && $draft && !$sama)
					<a href="{{url('/submit-pegawai',$pegawai->peg_id)}}" class="btn btn-primary btn-xs" onclick="return confirm('Apa Anda yakin?')"><i class="ace-icon fa fa-floppy-o"></i>Submit</a>
				@endif
					<a href="{{url('/pegawai/download',$pegawai->peg_id)}}" class="btn btn-xs" target="_blank"><i class="ace-icon fa fa-download"></i>Download Data Pegawai</a>
				</div>
				</div>
				@if($pegawai_bkd)
					@include('includes.data_pribadi')
				@else
					@include('includes.data_pribadi_baru')
				@endif
				
			</div>
		</div>
		<!--RIWAYAT PENDIDIKAN-->
		<div id="riwayat-pendidikan-formal" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_pendidikan_formal2')
			</div>
		</div>
		<!--RIWAYAT PENDIDIKAN NON FORMAL-->
		<div id="riwayat-pendidikan-nonformal" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_pendidikan_nonformal')
			</div>
		</div>
			<!--RIWAYAT KEPANGKATAN-->
		<div id="riwayat-kepangkatan" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_kepangkatan2')
			</div>
		</div>
		<!--RIWAYAT JABATAN-->
		<div id="riwayat-jabatan" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_jabatan2')
			</div>
		</div>
		<!--RIWAYAT KGB-->
		<div id="riwayat-kgb" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_kgb')
			</div>
		</div>
		<!--PMK-->
		<div id="riwayat-pmk" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_pmk')
			</div>
		</div>
		<!--RIWAYAT Kedhuk-->
		<div id="riwayat-kedhuk" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_kedhuk')
			</div>
		</div>
		<!--Kompetensi-->
		<div id="kompetensi" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.kompetensi')
			</div>
		</div>
		<!--Angka Kredit-->
		<div id="angka-kredit" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.angka_kredit')
			</div>
		</div>		
		<!--SKP-->
		<div id="skp" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.skp')
			</div>
		</div>
		<!--RIWAYAT DIKLAT STRUKTURAL-->
		<div id="riwayat-diklat-struktural" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_diklat_struktural2')
			</div>
		</div>
		<!--RIWAYAT DIKLAT FUNGSIONAL-->
		<div id="riwayat-diklat-fungsional" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_diklat_fungsional2')
			</div>
		</div>
		<!--RIWAYAT DIKLAT TEKNIS-->
		<div id="riwayat-diklat-teknis" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_diklat_teknis2')
			</div>
		</div>
		<!--RIWAYAT PENGHARGAAN-->
		<div id="riwayat-penghargaan" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_penghargaan2')
			</div>
		</div>
		<!--RIWAYAT PASANGAN-->
		<div id="riwayat-pasutri" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_pasutri2')
			</div>
		</div>
		<!--RIWAYAT PASANGAN-->
		<div id="riwayat-anak" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_anak2')
			</div>
		</div>
		<!--RIWAYAT ORANG TUA-->
		<div id="riwayat-orang-tua" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_orang_tua2')
			</div>
		</div>
		<!--RIWAYAT SAUDARA-->
		<div id="riwayat-saudara" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_saudara2')
			</div>
		</div>
        @if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,3,2,5] : [1,3]))
		<!--RIWAYAT HUKUMAN-->
		<div id="riwayat-hukuman" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_hukuman2')
			</div>
		</div>
		@endif
		@if(Auth::user()->id == 298)
		<!--RIWAYAT ASSESSMENT -->
		<div id="riwayat-assessment" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_assessment')
			</div>
		</div>
		@endif		
		<!--RIWAYAT TAMBAHAN-->
		<div id="riwayat-tambahan" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				<div class="col-md-8" id="peg_kpe">
				@include('form.radio_modal',['label'=>'Kepemilikan KPE','required'=>false,'name'=>'peg_punya_kpe','data'=>[
            	'1' => 'Ya','0' => 'Tidak' ], 'value'=>$pegawai->peg_punya_kpe])
            	</div>
				@include('includes.riwayat_keahlian2')
				@include('includes.riwayat_bahasa2')
			</div>
		</div>
		<div id="file-pegawai" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.file_pegawai2')
			</div>
		</div>
		@if($kepsek)
		<div id="riwayat-kepsek" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_kepsek')
			</div>
		</div>
		@endif
		<div id="riwayat-cuti" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_cuti')
			</div>
		</div>
		<div id="riwayat-plt" class="tab-pane">
			<div id="user-profile" class="user-profile row">
				@if($pegawai_bkd)
					@include('includes.profile')
				@else
					@include('includes.profile_baru')
				@endif
				@include('includes.riwayat_plt')
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-stat" tabindex="-1" role="dialog" aria-labelledby="modal-stat" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelFile"><div id="modal-button-edit">Update Status Kepegawaian - <span class="nama-peg"></span> (<span class="status-peg"></span>)</div></h4>
		</div>
		<form method="POST" action="{{url('/pegawai/update-status',$pegawai->peg_id)}}" id="fileForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="status" id="status-ganti">
		<div class="modal-body" id="modal-detail-content">
		<div class="row">
			<div class="col-md-12" id="pensiun">
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">No. SK Pensiun<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" name="noskpensiun" class="form-control" id="rpensiun_nosk">
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">TMT SK Pensiun<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="tmt_pensiun" name="tmt_pensiun" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="cuti">
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Jenis Cuti<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<select class="form-control" name="jeniscuti_id" id="jeniscuti_id">
							<option value="">-- Pilih --</option>
							<option value="5">Cuti Di Luar Tanggungan Negaara</option>
							<option value="6">Tugas Belajar</option>
							<option value="8">Tugas Belajar dengan pembiayaan dari APBD</option>
						</select>
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">No. SK Cuti<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" name="cuti_no" class="form-control" id="cuti_no">
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Tanggal Cuti Mulai<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="cuti_mulai" name="cuti_mulai" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Tanggal Cuti Selesai<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="cuti_selesai" name="cuti_selesai" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="meninggal">
				<div class="col-md-2"></div>
				<label class="control-label col-md-3 label-form">No. SK Pensiun<span class="required" aria-required="true">*</span></label>
				<div class="col-md-6">
					<input type="text" name="noskmeninggal" class="form-control">
				</div>
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">TMT SK Pensiun<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="tanggal_meninggal" name="tanggal_meninggal" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="pindah">
				<div class="col-md-2"></div>
				<label class="control-label col-md-3 label-form">No. SK Pindah<span class="required" aria-required="true">*</span></label>
				<div class="col-md-6">
					<input type="text" name="noskpindah" class="form-control">
				</div>
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">TMT Pindah<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="tmt_pindah" name="tmt_pindah" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Pindah Ke<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input class="form-control" type="text" id="ket_pindah" name="ket_pindah" size="25">
					</div>
				</div>
			</div>
		</div>

		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan" onclick="return confirm('Apa Anda yakin akan merubah status pegawai ini?')">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<!-- /section:elements.tab.option -->

<div class="modal fade" id="modal-kedudukan" tabindex="-1" role="dialog" aria-labelledby="modal-kedudukan" aria-hidden="true" enctype="multipart/form-data">
 	<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="labelFile"><div id="modal-button-edit">Update Kedudukan Pegawai - <span class="nama-peg"></span> </div></h4>
		</div>
		<form method="POST" action="{{url('/pegawai/update-kedudukan',$pegawai->peg_id)}}" id="fileForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-body" id="modal-detail-content">
		<div class="row">
			<div class="col-md-12" id="pensiun">
				<div class="form-group">
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Kedudukan Pegawai<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<select class="form-control" name="kedudukan_id" id="kedudukan_id">
							<option value="">-- Pilih --</option>
							@foreach($kedudukan as $ked)
							<option value="{{$ked->id}}">{{$ked->kedudukan_pns}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Nomor SK<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" name="nosk" class="form-control" id="nosk">
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">Tanggal SK<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="tanggal_sk" name="tanggal_sk" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
					<br>
					<div class="col-md-2"></div>
					<label class="control-label col-md-3 label-form">TMT Kedudukan<span class="required" aria-required="true">*</span></label>
					<div class="col-md-6">
						<input type="text" id="tmt_kedudukan" name="tmt_kedudukan" onblur="" onkeyup="" value="" size="20" maxlength="10">
						    &nbsp; <font color="red">format : yyyy-mm-dd</font>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="modal-footer">
			<input type="hidden" name="peg_id" class="peg_id" value="{{$pegawai->peg_id}}" >
			<button id="submitFile" class="btn btn-primary btn-xs file-simpan" onclick="return confirm('Apa Anda yakin akan merubah status pegawai ini?')">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">

	$(document).ready(function(){
		$("#upload1").attr('disabled',true);
		$("#file_nama").change(function(){
		    if($("#file_nama").val()!== null || $("#file_nama").val() !=='undefined')
		    {
		    	$("#upload1").attr('disabled',false);
		    }
		    else{
		    	$("#upload1").attr('disabled',true);
		    }
		});
	});

	$('#filename').change(function() {
	   $('.file-simpan').prop('disabled', false);
	});

	$('#modal-revisi-show').click(function(){
		console.log('masuk');
		$('#modal-revisi').modal('toggle');
	});
	$('.update-status').click(function(){
		$('.nama-peg').html($(this).data('pegawai'));
		$('.status-peg').html($(this).data('status'));
		var status = $(this).data('status');
		$('#status-ganti').val(status);
		if(status == 'meninggal'){
			$('#meninggal').show();
			$('#pindah').hide();
			$('#pensiun').hide();
			$('#cuti').hide();
		}else if(status == 'pensiun'){
			$('#meninggal').hide();
			$('#pindah').hide();
			$('#cuti').hide();
			$('#pensiun').show();
		}else if(status == 'cuti'){
			$('#meninggal').hide();
			$('#pindah').hide();
			$('#pensiun').hide();
			$('#cuti').show();
		}
		else{
			$('#meninggal').hide();
			$('#pindah').show();
			$('#pensiun').hide();
			$('#cuti').hide();
		}
		$("#tmt_pensiun").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#tmt_pindah").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#cuti_mulai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#cuti_selesai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#tanggal_meninggal").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$('#modal-status').modal('toggle');
	});
	$('.update-kedudukan').click(function(){
		$('.nama-peg').html($(this).data('pegawai'));
		$("#tmt_kedudukan").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#tanggal_sk").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
	});

	$('#edit-status').click(function(){
		$('.nama-peg').html($(this).data('pegawai'));
		$('.status-peg').html($(this).data('status'));
		var status = $(this).data('status');
		$('#status-ganti').val(status);
		if(status == 'meninggal'){
			$('#meninggal').show();
			$('#pindah').hide();
			$('#pensiun').hide();
			$('#cuti').hide();
		}else if(status == 'pensiun'){
			$('#meninggal').hide();
			$('#pindah').hide();
			$('#cuti').hide();
			$('#pensiun').show();
		}else if(status == 'cuti'){
			$('#meninggal').hide();
			$('#pindah').hide();
			$('#pensiun').hide();
			$('#cuti').show();
		}
		else{
			$('#meninggal').hide();
			$('#pindah').show();
			$('#pensiun').hide();
			$('#cuti').hide();
		}
		$('#rpensiun_nosk').val($(this).data('sk'));
		$("#tmt_pensiun").val($(this).data('tanggal')).mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#tmt_pindah").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#cuti_mulai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#cuti_selesai").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$("#tanggal_meninggal").val("").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
		$('#modal-status').modal('toggle');
	});
	 $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
   	$('#myTab a[href="' + hash + '"]').tab('show');

  	$( ".show-option" ).tooltip({
		show: {
			effect: "slideDown",
			delay: 250
		}
	});

	$(function(){
	 	$('#peg_punya_kpe1').click(function(){
		    if ($(this).is(':checked')){
		      	var val = $(this).val();
		      	$.ajax({
			        type: "GET",
			        cache: false,
			        url: "{{ URL::to('pegawai/updateKPE',$pegawai->peg_id) }}",
			        beforeSend: function (xhr) {
			            var token = $('meta[name="csrf_token"]').attr('content');

			            if (token) {
			                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
			            }
			        },
			        data: {"peg_punya_kpe":val},
			        dataType: 'json',
			        success: function(data) {
			        	alert('telah diupdate');
			           $("#peg_kpe").load(window.location + " #peg_kpe");
			        }
			    }).error(function (e){
			        alert('telah diupdate');
			    });
		    }
	  	});
	  	$('#peg_punya_kpe2').click(function(){
		    if ($(this).is(':checked')){
		      	var val = $(this).val();
		      	$.ajax({
			        type: "GET",
			        cache: false,
			        url: "{{ URL::to('pegawai/updateKPE',$pegawai->peg_id) }}",
			        beforeSend: function (xhr) {
			            var token = $('meta[name="csrf_token"]').attr('content');

			            if (token) {
			                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
			            }
			        },
			        data: {"peg_punya_kpe":val},
			        dataType: 'json',
			        success: function(data) {
			        	alert('telah diupdate');
			           $("#peg_kpe").load(window.location + " #peg_kpe");
			        }
			    }).error(function (e){
			        alert('telah diupdate');
			    });
		    }
	  	});
	});
	var vm = new Vue({
	  el: '#modal-file',
	  data: {
	    file_nama:'',
	  },
	  methods: {
	    doSearch: function(){
	    	var peg_id = "{{$pegawai->peg_id}}";
	        $.getJSON("{{url('pegawai/file-cek')}}", {peg_id:peg_id,file_nama: this.file_nama}, function(data) {
	          if(data > 0){
	          	$('.error-file').html('<div class="alert alert-danger">Nama FIle Sudah Terdaftar, Silahkan Gunakan Nama Yang Lain.</div>');
	          }else{
	          	$('.error-file').html('');
	          }
	        });
	    }
	  }
	});
</script>
@include('scripts.file_upload')
@endsection
