@extends('layouts.app')

@section('content')
@if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
  @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif

<div id="vm">
	<h3 style="text-align:center">Daftar Pengajuan</h3>

	<div class="col-lg-9">
        <div class="form-group row">
          	<label for="cname" class="control-label col-lg-3">Cari</label>
          	<div class="col-lg-9">
             	<input class="form-control" id="psearch" name="psearch" placeholder="Masukkan kata kunci" type="text" value="" v-model="search_input" v-on="keyup:doSearch | key 'enter'">
          	</div>
    	</div>

    	<div class="form-group row">
	      	<label for="cname" class="control-label col-lg-3">Pengajuan</label>
	      	<div class="col-lg-9">
	            <select class="psearch form-control" id="m_satker" v-model="search_input_pengajuan" v-on="change:doSearch">
	                  <option value="" selected="">Pilih Pengajuan</option>
	                  @foreach ($data=App\Model\MasterPengajuan::lists('nama_pengajuan','nama_pengajuan') as $key => $opt)
	                    <option value="{{$key}}">{{$opt}}</option>
	                  @endforeach
	            </select> 
	      	</div>
	    </div>
	</div>

	<div class="col-lg-12">
  		<div class="loading panel" v-show="isLoading">
    		<h3>Sedang memuat data...</h3>
  		</div>
  	</div>
  	<section v-show="!isLoading" style="display: none; width: 100%;">
    <div id="isi" class="table-responsive">
    	<table cellspacing="0" class="table table-striped table-advanced">
      		<tbody>
        		<tr>
          			<td class="ewGridContent">
            			<div class="ewGridMiddlePanel" id="gmp_pegawai">
              				<table class="table table-bordered table-striped table-condensed" id="tbl_pegawailist">
                				<thead>
                  					<tr class="ewTableHeader">
                  						<th class="ewListOptionHeader" data-name="view" style="white-space: nowrap;">No</th>
                    					@foreach ($columns as $column_id => $column)
                    						<th>
                      							<div class="ewPointer" v-on="click: sort('{{$column_id}}')">
                        							<div class="pegawai_{{$column_id}}" id="elh_pegawai_{{$column_id}}">
                          								<div class="ewTableHeaderBtn">
                            								<span class="ewTableHeaderCaption">{{$column[0]}}</span>
                            								<span class="ewTableHeaderSort">
                          										<span v-show="sortColumn == '{{$column_id}}' && sortDir == 'desc'" class="caret"></span>
                              									<span v-show="sortColumn == '{{$column_id}}' && sortDir == 'asc' " class="caret ewSortUp"></span>
                            								</span>
                          								</div>
                        							</div>
                      							</div>
                    						</th>
                    					@endforeach
                    					<th class="ewListOptionHeader" data-name="view" style="white-space: nowrap;"><span>&nbsp;</span></th>
                  					</tr>
                				</thead>

                				<tbody>
                  					<tr class="@{{$index % 2 ? 'ewTableAltRow' : 'ewTableRow'}}" data-rowindex="@{{$index + 1}}" data-rowtype="1" v-repeat="items">
                  						<td>@{{ $index + 1 }}</td>
                    					@foreach ($columns as $column)
                    						<td>
                      							<span v-text="{{$column[1]}}"></span>
                    						</td>
                    					@endforeach
                    					<td>
                        					<a href="{{ url('pengajuan/detail/') }}/@{{ pengajuan_id }}" class="btn btn-info btn-xs">
												<i class="fa fa-search"></i> Lihat
											</a>
											<button class="btn btn-primary btn-xs"  data-toggle="modal" id="ubah-stat" data-target="#modal-user" data-keterangan="@{{keterangan}}" data-status="@{{status}}" onclick="ubahStatus(@{{ pengajuan_id }})">
												<i class="fa fa-pencil"></i> Ubah Status
											</button>
                    					</td>
                  					</tr>
                				</tbody>
              				</table>
            			</div>

            			<div class="ewGridLowerPanel">
              				<form class="form-inline">
                				<div class="form-group">
                  					Page &nbsp;
                  
                  					<a class="btn btn-default btn-xs" v-on="click: paginate('first')"><i class="glyphicon glyphicon-step-backward"></i></a>
                  					<a class="btn btn-default btn-xs" v-on="click: paginate('previous')"><i class="glyphicon glyphicon-chevron-left"></i></a>
                  					<input class="form-control input-sm" type="text" v-model="pagination.page" v-on="change: changePage, keyup: changePage | key 'enter'">
                  					<a class="btn btn-default btn-xs" v-on="click: paginate('next')"><i class="glyphicon glyphicon-chevron-right"></i></a>
                  					<a class="btn btn-default btn-xs" v-on="click: paginate('last')"><i class="glyphicon glyphicon-step-forward"></i></a>
              						&nbsp;of&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                  					&nbsp;&nbsp;&nbsp;&nbsp; Records&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;to&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;of&nbsp;@{{ pagination.count }}</td>
                				</div>
              				</form>
            			</div>
      				</td>
        		</tr>
      		</tbody>
    	</table>
      </div>
  	</section>
</div>

<div class="modal fade" id="modal-user" tabindex="-1" role="dialog" aria-labelledby="modal-revisi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Ubah Status</h4>
        </div>
        <form method="POST" action="{{url('/')}}" id="userForm">
            {!! csrf_field() !!}
        <div class="modal-body" id="modal-detail-content">
            @include('form.radio_modal',['label'=>'Status','required'=>false,'name'=>'status','data'=>
            ['verifikasi' => 'Verifikasi', 'tolak' => 'Tolak', 'revisi' => 'Revisi', 'terima' => 'Terima']])
            <br>

            @include('form.textarea',['label' => 'Keterangan', 'required' => false, 'name' => 'keterangan'])
        </div>
        <div class="modal-footer">
            <button id="submitUser" class="btn btn-primary btn-xs">Simpan</button>
        </div>
    </form>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#list-pengajuan').DataTable({
			"bSort" : false,
			"iDisplayLength": 20,
    	});
	});

	var ubahStatus = function(id){
	    $("#userForm").attr("action","{{ url('pengajuan/ubah-status/') }}/"+id);
	    $("#labelUser").text("Ubah Status");
      $("#keterangan").val($("#ubah-stat").data('keterangan'));

      var stat = $("#ubah-stat").data('status');
	    $(".radio-button[value='"+stat+"']").prop('checked', true);
	}

	var page = 'daftar-pengajuan';
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: true,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 20,
      count: 0,
    },
    params: {
    },
    search: '',
    search_pengajuan: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      	if (direction === 'previous') {
    		if (this.pagination.page > 1) {
          		--this.pagination.page;
          		this.changePage();
        	}
      	} else if (direction === 'next') {
        	if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          		++this.pagination.page;
          		this.changePage();
        	}
      	} else if (direction === 'first') {
        	this.pagination.page = 1;
        	this.changePage();
      	} else if (direction === 'last') {
        	this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        	this.changePage();
      	}
    },
    changePage: function (page) {
      	if (isNaN(parseInt(this.pagination.page))) {
        	this.pagination.page = 1;
      	}
      	getData(this.pagination.page);
    },
    clearFilter: function () {
      	for (var key in this.params) {
        	if (this.params.hasOwnProperty(key)) {
          		this.params[key] = '';
        	}
      	}
      	this.search = '';
      	this.search_input = '';
      	this.search_pengajuan = '';
      	this.sortColumn = null;
      	this.sortDir = null;
      	this.pagination.page = 1;
      	this.changePage();
    },
    sort: function (col) {
      	if (this.sortColumn == col) {
        	this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      	} else {
        	this.sortColumn = col;
        	this.sortDir = 'asc';
      	}
      	this.pagination.page = 1;
      	this.changePage();
    },
    doSearch: function () {
      	this.search = this.search_input;
      	this.search_pengajuan = this.search_input_pengajuan;
      	this.pagination.page = 1;
      	this.changePage();
    },
  }
});
loadState();
vm.changePage();
function getData(page) {
  $.getJSON("{{url('pengajuan/get-data')}}?page="+page+'&perpage='+vm.pagination.perpage,
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    search_pengajuan: vm.search_pengajuan,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
    saveState();
  });
}

function saveState() {
  var stored = {
    pagination : JSON.stringify(vm.pagination),
    params : JSON.stringify(vm.params),
    search : vm.search,
    search_pengajuan : vm.search_pengajuan,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem(page + '_state',JSON.stringify(stored));
}
function loadState() {
  var stored = JSON.parse(localStorage.getItem(page + '_state'));
  if (stored) {
    vm.pagination = JSON.parse(stored.pagination);
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.search_input = stored.search;
    vm.search_pengajuan = stored.search_pengajuan;
    vm.search_input_pengajuan = stored.search_pengajuan;
    vm.sortColumn = stored.sortColumn;
    vm.sortDir = stored.sortDir;
  }
}
</script>
@endsection
