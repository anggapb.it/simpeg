@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<h3 style="text-align:center">Detail Pengajuan</h3>
		 	<hr>
			<br><br>

			<div class="col-xs-6">
				<div class="profile-user-info profile-user-info-striped">
					<div class="profile-info-row">
						<div class="profile-info-name"> Nip</div>
						<div class="profile-info-value">
							<span class="editable">{{ $data->pegawai ? $data->pegawai->peg_nip : '' }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Nama</div>
						<div class="profile-info-value">
							<span class="editable">{{ $data->pegawai ? $data->pegawai->peg_nama : '' }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Pengajuan</div>
						<div class="profile-info-value">
							<span class="editable">{{ $data->nama_pengajuan }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Lampiran</div>
						<div class="profile-info-value">
							@foreach($lampiran as $file)
								<a class="grey show-option" href="{{ asset('uploads/file/'.$file->file_lokasi) }}" title="">
									<span class="editable">{{ $file->file_nama }}</span>
								</a>
								<br>
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-6 col-sm-3 center">
				<div>
					<!-- #section:pages/profile.picture -->
					<span class="profile-picture">
						<a href="{{asset('/uploads/'.$data->pegawai ? $data->pegawai->peg_foto : '') }}" target="_blank">
							<img id="avatar" class="editable img-responsive" src="{{asset('/uploads/'.$data->pegawai ? $data->pegawai->peg_foto : '') }}" />
						</a>
					</span>

					<!-- /section:pages/profile.picture -->
					<div class="space-4"></div>
				</div>
			</div>
		</div>
	</div>
@endsection