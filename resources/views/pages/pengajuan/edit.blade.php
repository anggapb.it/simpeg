@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
  <h3>Buat Pengajuan</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('pengajuan/update') }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                {!! method_field('patch') !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" name="pengajuan_id" value="{{ $data->pengajuan_id }}">

                <table class="table no-footer">
                  <tr>
                    @include('form.select2',['label'=>'Nama Pengajuan','required'=>true,'name'=>'nama_pengajuan','vue'=>'', 'data'=> $pengajuan,'empty'=>'Pilih Nama Pengajuan', 'value' => $data->nama_pengajuan])
                  </tr>
                  <tr>
                    @include('form.checkbox2',['label'=>'File Lampiran','required'=>true,'name'=>'lampiran','vue'=>'', 'data' => $file, 'value' => json_encode($data->lampiran)])
                  </tr>
                </table>
                
                 <button class="btn btn-primary btn-xs" type="submit">Tambah</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
      $('#nama_pengajuan').select2({
        width:"500px"
      });
  </script>
@endsection