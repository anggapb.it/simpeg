@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
	<h3>Tambah Walikota</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('walikota/store') }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <table class="table no-footer">
                  <tr>
                    @include('form.select2',['label'=>'Pilih Nama','required'=>false,'name'=>'walikota','vue'=>'', 'data'=>App\Model\Walikota::lists('walikota_nama','walikota_id'),'empty'=>'Pilih Nama Pimpinan'])
                  </tr>
                  <tr><td>Atau</td></tr>
                  <tr>
                    @include('form.txt',['label'=>'Tulis Nama baru','required'=>false,'name'=>'walikota_new','vue'=>''])
                  </tr>
                	<tr>
                    @include('form.txt2',['label'=>'Tanggal Mulai Menjabat','required'=>false,'name'=>'tgl_mulai','vue'=>''])
                  </tr>

                  <tr>
                    @include('form.txt2',['label'=>'Tanggal Selesai Menjabat','required'=>false,'name'=>'tgl_selesai','vue'=>''])
                  </tr>
                  <tr>
                   @include('form.radio2',['label'=>'Jabatan','required'=>false,'name'=>'jenis','data'=>['Walikota' => 'Walikota','Wakil Walikota' => 'Wakil Walikota' ], 'value'=>''])
                  </tr>
                </table>
                
                 <button class="btn btn-primary btn-xs" type="submit">Tambah</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
      $('#walikota').select2({
        width:"500px"
      });
      
jQuery(function($){
     $("#tgl_selesai").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
     $("#tgl_mulai").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
});
  </script>
@endsection