@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
	<h3>Edit Walikota</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('walikota/update/'.$model->id) }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <table class="table no-footer">
                  <tr>
                    @include('form.txt',['label'=>'Nama','required'=>false,'name'=>'walikota_nama','vue'=>'','value'=>$model->walikota->walikota_nama])
                  </tr>
                	<tr>
                    @include('form.txt2',['label'=>'Tanggal Mulai Menjabat','required'=>false,'name'=>'tgl_mulai','vue'=>'','value'=>$model->tgl_mulai])
                  </tr>

                  <tr>
                    @include('form.txt2',['label'=>'Tanggal Selesai Menjabat','required'=>false,'name'=>'tgl_selesai','vue'=>'', 'value'=>$model->tgl_selesai])
                  </tr>
                  <tr>
                   @include('form.radio2',['label'=>'Jabatan','required'=>false,'name'=>'jenis','data'=>['Walikota' => 'Walikota','Wakil Walikota' => 'Wakil Walikota' ], 'value'=>$model->jenis])
                  </tr>
                </table>
                
                 <button class="btn btn-primary btn-xs" type="submit">Simpan</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
      $('#walikota').select2({
        width:"500px"
      });
      
jQuery(function($){
     $("#tgl_selesai").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
     $("#tgl_mulai").mask("9999-99-99",{placeholder:"yyyy-mm-dd"});
});
  </script>
@endsection