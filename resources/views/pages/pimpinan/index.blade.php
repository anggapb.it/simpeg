@extends('layouts.app')

@section('content')
<h2 align="center">
  Profil Pimpinan Kota Bandung
</h2>
<br>
<br>
<div id="vm">
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
  @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
   <div class="col-lg-12">
  <div class="loading panel" v-show="isLoading">
    <h3>Sedang memuat data...</h3>
  </div>
  </div>
  @if(Auth::user()->role_id==1)
   <a class="btn btn-xs btn-primary" href="{{url('walikota/add')}}/" data-toggle="tooltip" title="Tambah Data" >Tambah Data</a>
  @endif
  <section v-show="!isLoading" style="display: none">
    <div class="table-responsive">
    <table cellspacing="0" class="table table-striped table-advanced">
      <tbody>
        <tr>
          <td class="ewGridContent">
            <div class="ewGridMiddlePanel" id="gmp_pegawai">
              <table class="table table-bordered table-striped table-condensed" id="tbl_pegawailist">
                <thead>
                  <!-- Table header -->
                  <tr class="ewTableHeader">
                    @foreach ($columns as $column_id => $column)
                    <th>
                      <div class="ewPointer" v-on="click: sort('{{$column_id}}')">
                        <div class="pegawai_{{$column_id}}" id="elh_pegawai_{{$column_id}}">
                          <div class="ewTableHeaderBtn">
                            <span class="ewTableHeaderCaption">{{$column[0]}}</span>
                            <span class="ewTableHeaderSort">
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'desc'" class="caret"></span>
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'asc' " class="caret ewSortUp"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </th>
                    @endforeach
                    @if(Auth::user()->role_id==1)
                    <td></td>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  <tr class="@{{$index % 2 ? 'ewTableAltRow' : 'ewTableRow'}}" data-rowindex="@{{$index + 1}}" data-rowtype="1" v-repeat="items">
                    @foreach ($columns as $column)
                    <td>
                      <span v-text="{{$column[1]}}"></span>
                    </td>
                    @endforeach
                    @if(Auth::user()->role_id==1)
                    <td>
                       <a class="btn btn-xs btn-info" href="{{url('pegawai/profile/edit')}}/@{{peg_id}}" data-toggle="tooltip" title="Lihat">Lihat
                        </a>
                        <a class="btn btn-xs btn-warning" href="{{url('walikota/edit')}}/@{{peg_id}}/@{{id}}" data-toggle="tooltip" title="Edit">Edit
                        </a>
                    </td>
                    @endif
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="ewGridLowerPanel">
              <form class="form-inline">
                <div class="form-group">
                  Page &nbsp;
                  <!--first page button-->
                  <a class="btn btn-default btn-xs" v-on="click: paginate('first')"><i class="glyphicon glyphicon-step-backward"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('previous')"><i class="glyphicon glyphicon-chevron-left"></i></a>
                  <input class="form-control input-sm" type="text" v-model="pagination.page" v-on="change: changePage, keyup: changePage | key 'enter'">
                  <a class="btn btn-default btn-xs" v-on="click: paginate('next')"><i class="glyphicon glyphicon-chevron-right"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('last')"><i class="glyphicon glyphicon-step-forward"></i></a>
                  &nbsp;of&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                  &nbsp;&nbsp;&nbsp;&nbsp; Records&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;to&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;of&nbsp;@{{ pagination.count }}</td>
                </div>
              </form>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    </div>
  </section>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var page = 'walikota';
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: true,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 20,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_input = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    },
    exportData: function () {
      this.search = this.search_input;
      exportExcel();
    }
  }
});
loadState();
vm.changePage();
function getData(page) {
  $.getJSON("{{$url}}?page="+page+'&perpage='+vm.pagination.perpage,
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
    saveState();
  });
}
function saveState() {
  var stored = {
    pagination : JSON.stringify(vm.pagination),
    params : JSON.stringify(vm.params),
    search : vm.search,
    search_input : vm.search_input,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem(page + 'pimpinan_state',JSON.stringify(stored));
}
function loadState() {
  var stored = JSON.parse(localStorage.getItem(page + 'pimpinan_state'));
  if (stored) {
    vm.pagination = JSON.parse(stored.pagination);
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.search_input = stored.search_input;
    vm.sortColumn = stored.sortColumn;
    vm.sortDir = stored.sortDir;
  }
}
</script>
@endsection
