<?php
	use App\Model\Agama;
	use App\Model\Kecamatan;
	use App\Model\Kabupaten;
	use App\Model\Propinsi;
	use App\Model\GolonganDarah;
	use App\Model\KategoriPendidikan;
	use App\Model\Pendidikan;
	use App\Model\Golongan;
	use App\Model\Jabatan;
	use App\Model\Eselon;
	use App\Model\Gaji;
	use App\Model\GajiTahun;
	use App\Model\StatusEditPegawai;
	use App\Model\Pegawai;
	use App\Model\PegawaiPensiun;
	use App\Model\TingkatPendidikan;
	use App\Model\Universitas;
	use App\Model\Jurusan;
	use App\Model\Fakultas;
	use App\Model\DiklatStruktural;
	use App\Model\DiklatFungsional;
	use App\Model\DiklatTeknis;
	use App\Model\Penghargaan;
	use App\Model\Keahlian;
	use App\Model\KeahlianLevel;
	use App\Model\RiwayatPendidikan2;
	use App\Model\RiwayatNonFormal2;
	use App\Model\RiwayatDiklat2;
	use App\Model\RiwayatKeahlianRel2;
	use App\Model\RiwayatKeahlian2;
	use App\Model\Hukum;
	use App\Model\HukumKategori;
	use App\Model\JabatanFungsional;
	use App\Model\JabatanFungsionalUmum;

	$agama = Agama::where('id_agama', $pegawai->id_agama)->first();	
	$goldar = GolonganDarah::where('id_goldar', $pegawai->id_goldar)->first();
	$kecamatan = Kecamatan::where("kecamatan_id", $pegawai->kecamatan_id)->first();
	if($kecamatan){
		$kab = Kabupaten::where('kabupaten_id', $kecamatan->kabupaten_id)->first();
	}else{
		$kab = null;
	}
	$pend_awal = Pendidikan::where('id_pend', $pegawai->id_pend_awal)->first();
	$pend_akhir = Pendidikan::where('id_pend', $pegawai->id_pend_akhir)->first();
	if($pend_awal){
		$kat_pend_awal = KategoriPendidikan::where('kat_pend_id', $pend_awal->kat_pend_id)->first(); 
	}else{
		$kat_pend_awal = null;
	}

	if($pend_akhir){
		$kat_pend_akhir = KategoriPendidikan::where('kat_pend_id', $pend_akhir->kat_pend_id)->first(); 
	}else{
		$kat_pend_akhir = null;
	}
	$golongan_awal = Golongan::where('gol_id', $pegawai->gol_id_awal)->first();
	$golongan_akhir = Golongan::where('gol_id', $pegawai->gol_id_akhir)->first();
	$jabatan = Jabatan::where('jabatan_id', $pegawai->jabatan_id)->first();
	if($jabatan){
		$eselon = Eselon::where('eselon_id', $jabatan->eselon_id)->first();
	}else{
		$eselon = null;
	}
	$gaji_thn = GajiTahun::where('mgaji_status', 'TRUE')->first();
	$gaji = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $pegawai->gol_id_akhir)->where('gaji_masakerja', $pegawai->peg_kerja_tahun)->first();
	$id = $pegawai->peg_id;

	if($pegawai->peg_jenis_kelamin == 'L'){
		$jk = 'Laki-laki';
	}else{
		$jk = 'Perempuan';
	}

	if($pegawai->peg_status_perkawinan == '1'){
		$sp='Kawin';
	}elseif($pegawai->peg_status_perkawinan == '2'){
		$sp='Belum Kawin';
	}elseif($pegawai->peg_status_perkawinan == '3'){
		$sp='Janda';
	}elseif($pegawai->peg_status_perkawinan == '4'){
		$sp='Duda';
	}else{
		$sp='-';
	}

	if($pegawai->peg_status_kepegawaian == '1'){
		$peg_stat = 'PNS';
	}else{
		$peg_stat = 'CPNS';
	}

	if($pend_awal){
		if (intval($kat_pend_awal->kat_pend_id) <= 6){
			$pendawal = $pend_awal->nm_pend;
		}else{
			$pendawal = $kat_pend_awal->kat_nama.'-'.$pend_awal->nm_pend;
		}
	}

	if($pend_akhir){
		if (intval($kat_pend_akhir->kat_pend_id) <= 6){
			$pendakhir = $pend_akhir->nm_pend;
		}else{
			$pendakhir = $kat_pend_akhir->kat_nama.'-'.$pend_akhir->nm_pend;
		}
	}

	if($jabatan){
		if($jabatan->jabatan_jenis == 2){
			$jab_jenis = 'Struktural';
			$nama_jabatan = $jabatan->jabatan_nama;
		}elseif($jabatan->jabatan_jenis == 3){
			$jab_jenis = 'Fungsional Tertentu';
			$jf = JabatanFungsional::where('jf_id', $jabatan->jf_id)->first();
			$nama_jabatan = $jf->jf_nama;
		}elseif($jabatan->jabatan_jenis == 4){
			$jab_jenis ='Fungsional Umum';
			$jfu = JabatanFungsionalUmum::where('jfu_id', $jabatan->jfu_id)->first();
			if($jfu){
				$nama_jabatan = $jfu['jfu_nama'];
			}else{
				$nama_jabatan = 'Pelaksana';
			}
		}
	}else{
		$jab_jenis ='-';
		$nama_jabatan='-';
	}

	if($pegawai->peg_bapertarum == 1){
		$bap = 'Sudah Diambil';
	}elseif($pegawai->peg_bapertarum == 2){
		$bap = 'Belum Diambil';
	}else{
		$bap = '-';
	}

	if($pegawai->peg_punya_kpe == 1){
		$kpe = 'Ya';
	}else{
		$kpe = 'Tidak';
	}

	$keahlian = Keahlian::where('spesifik_dari_keahlian_id', '<>', 1)->where('keahlian_id','<>',1)->get();
	$keahlian_bahasa = Keahlian::where('spesifik_dari_keahlian_id', 1)->get();

	if(!$pegawai->peg_status){
		$peg_pensiun = PegawaiPensiun::where('peg_id',$pegawai->peg_id)->first();
	}

	$i=1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
@page {
    margin: 1.5cm;
}

body {
  font-family: sans-serif;
    margin: 0.5cm 0;
    text-align: justify;
    font-size: 11px;
}
table thead th{
    
}
thead {
    display: table-header-group;
}
.tabel {
	/*page-break-inside: avoid;*/
    background-color: transparent;
    border: 1px solid black;
    border-spacing: 0;
    max-width: 100%;
}
.tabel-td{
	border: 1px solid black;
}
#header td,
#footer td {
  padding: 0;
    width: 50%;
}

.boxed {
  border: 1px solid black;
}

.title {
  font-size: 13px;
}
.subtitle {
  font-size: 14pt;
}

p {
  text-align: left;
  font-size: 1em;
  margin: 0 0 2pt 0;
  padding: 0;
}
.padded {
  padding: 10pt;
}

.warna-tabel{
	color:#336199;background-color:#EDF3F4;
	border-top:1px solid #F7FBFF;
	text-align:left;
	padding:6px 4px 6px 10px;
}

.isi-tabel{
	display:table-cell;padding:6px 4px 6px 6px;border-top:1px dotted #336199;
	margin-left:1px;margin-right:3px;color:#666;border-bottom:1px solid #FFF;
}
.page-break {
    page-break-after: always;
}
.foto{
	max-width: 100px; 
}
.header { 
	position: fixed;
	 left: 0px; top: -50px; right: 0px;height: 500px;
}
.header-warna{
	background-color:lightgrey;
}

</style>
</head>
<body>
<!--DATA PRIBADI -->
	<div class="header">
		<img src="bdg.png"></img>
		<hr>
	</div>
	<p align="center" class="title">
   		<strong>DAFTAR RIWAYAT HIDUP</strong>
	</p>
	<br/>
	<br/>
	<table border="0" cellspacing="3" cellpadding="3" align="center">
	    <tbody>
	    	<tr>
	    		<td colspan="3"><center>
	    			@if(file_exists('uploads/'.$pegawai->peg_foto) && $pegawai->peg_foto != null)
	    			<img src="{{$foto}}" class="foto"/>
	    			@else
	    			<img src="uploads/none.png" class="foto"/>
	    			@endif
	    		</img></center></td>
	    	</tr>
	    </tbody>
	</table>
	<br/>
	<br/>
	<table cellspacing="5" cellpadding="5">
	    <tbody>
	    	<tr>
	    		<td>NIP</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_nip}}</td>
				
				@if(!$pegawai->peg_status){
	    		<td>No. SK Pensiun</td>
	    		<td>:</td>
	    		<td>{{$peg_pensiun ? $peg_pensiun->rpensiun_nosk : null}}</td>
	    		@endif
	    	</tr>
	    	<tr>
	    		<td>NIP Lama</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_nip_lama}}</td>

	    		@if(!$pegawai->peg_status){
	    		<td>Tangal SK Pensiun</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($peg_pensiun ? $peg_pensiun->rpensiun_tglsk : null)}}</td>
	    		@endif
	    	</tr>
	    	<tr>
	    		<td>Nama Lengkap</td>
	    		<td>:</td>
	    		<td>{{ $pegawai->peg_gelar_depan}}{{$pegawai->peg_nama}} {{$pegawai->peg_gelar_belakang}}</td>
	    	</tr>
	    	<tr>
	    		<td>Tempat, Tanggal Lahir</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_lahir_tempat}}, {{getIndonesianDate($pegawai->peg_lahir_tanggal)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Jenis Kelamin</td>
	    		<td>:</td>
	    		<td>{{$jk}}</td>
	    	</tr>
	    	<tr>
	    		<td>Status Perkawinan</td>
	    		<td>:</td>
	    		<td>{{$sp}}</td>
	    	</tr>
	    	<tr>
	    		<td>Agama</td>
	    		<td>:</td>
	    		@if($agama)
	    		<td>{{$agama->nm_agama}}</td>
	    		@else
	    		<td></td>
	    		@endif
	    	</tr>
	    	<tr>
	    		<td>Status Kepegawaian</td>
	    		<td>:</td>
	    		<td>{{$peg_stat}}</td>
	
	    		<td>TMT CPNS</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_cpns_tmt)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Kedudukan Kepegawaian</td>
	    		<td>:</td>
	    		<td>{{$kedudukan_pegawai}}</td>
	    		<td>TMT PNS</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_pns_tmt)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Pendidikan Awal</td>
	    		<td>:</td>
	    		@if($pend_awal)
	    		<td>{{$pendawal}}</td>
	    		@else
	    		<td></td>
	    		@endif
	    		<td>Tahun Pendidikan Awal</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_pend_awal_th}}</td>
	    	</tr>
	    	<tr>
	    		<td>Pendidikan Akhir</td>
	    		<td>:</td>
	    		@if($pendakhir)
	    		<td>{{$pendakhir}}</td>
	    		@else
	    		<td></td>
	    		@endif
	    		<td>Tahun Pendidikan Akhir</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_pend_akhir_th}}</td>
	    	</tr>
	    	<tr>
	    		<td>Jenis Jabatan</td>
	    		<td>:</td>
	    		<td>{{$jab_jenis}}</td>
	    	</tr>
	    	<tr>
	    		<td>Eselon</td>
	    		<td>:</td>
	    		<td>{{$eselon['eselon_nm']}}</td>
	    	</tr>
	    	<tr>
	    		<td>Nama Jabatan</td>
	    		<td>:</td>
	    		<td>{{$nama_jabatan}}</td>
	    		<td>TMT Jabatan</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_jabatan_tmt)}}</td>
	    	</tr>
	    	<tr>
	    		<td>SOPD</td>
	    		<td>:</td>
	    		<td>{{$satker['satuan_kerja_nama']}}</td>
	    	</tr>
	    	<tr>
	    		<td>Instansi (Jika Dipekerjakan)</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_instansi_dpk}}</td>
	    	</tr>
	    	<tr>
	    		<td>Golongan Awal</td>
	    		<td>:</td>
	    		<td>{{$golongan_awal ? $golongan_awal->nm_gol : ''}}, {{$golongan_awal ? $golongan_awal->nm_pkt : ''}}</td>
	    		<td>TMT Golongan Awal</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_gol_awal_tmt)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Golongan Akhir</td>
	    		<td>:</td>
	    		<td>{{$golongan_akhir ? $golongan_akhir->nm_gol : ''}}, {{$golongan_akhir ? $golongan_akhir->nm_pkt : ''}}</td>
	    		<td>TMT Golongan Akhir</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_gol_akhir_tmt)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Masa Kerja Golongan</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_kerja_tahun}} Tahun {{$pegawai->peg_kerja_bulan}} Bulan</td>
	    		<td>Gaji Pokok</td>
	    		<td>:</td>
	    		<td>Rp {{$gaji['gaji_pokok']}}</td>
	    	</tr>
	    	<tr>
	    		<td>No. KARPEG</td>
	    		<td>:</td>
	    		<td>{{ $pegawai->peg_karpeg}}</td>
	    		<td>No. Karis/Karsu</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_karsutri}}</td>
	    	</tr>
	    	<tr>
	    		<td>No. ASKES</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_no_askes}}</td>
	    	</tr>
	    	<tr>
	    		<td>No. KTP</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_ktp}}</td>
	    		<td>No. NPWP</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_npwp}}</td>
	    	</tr>
	    	<tr>
	    		<td>Golongan Darah</td>
	    		<td>:</td>
	    		<td>{{$goldar['nm_goldar']}}</td>
	    	</tr>
	    	<tr>
	    		<td>BAPETARUM</td>
	    		<td>:</td>
	    		<td>{{$bap}}</td>
	    	</tr>
	    	<tr>
	    		<td>TMT Gaji Berkala Terbaru</td>
	    		<td>:</td>
	    		<td>{{getIndonesianDate($pegawai->peg_tmt_kgb)}}</td>
	    	</tr>
	    	<tr>
	    		<td>Alamat Rumah</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_rumah_alamat}}</td>
	    	</tr>
	    	<tr>
	    		<td>Kel./Desa</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_kel_desa}}</td>
	    		<td>Kec.</td>
	    		<td>:</td>
	    		@if($kecamatan)
	    		<td>{{$kecamatan->kecamatan_nm}}</td>
	    		@else
	    		<td></td>
	    		@endif
	    	</tr>
	    	<tr>
	    		<td>Kab./Kota</td>
	    		<td>:</td>
	    		@if($kab)
	    		<td>{{$kab->kabupaten_nm}}</td>
	    		@else
	    		<td></td>
	    		@endif
	    		<td>Kode Pos</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_kodepos}}</td>
	    	</tr>
	    	<tr>
	    		<td>Telp.</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_telp}}</td>
	    		<td>HP</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_telp_hp}}</td>
	    	</tr>
	    	<tr>
	    		<td>Email</td>
	    		<td>:</td>
	    		<td>{{$pegawai->peg_email}}</td>
	    		<td>Status Kepemilikan KPE</td>
	    		<td>:</td>
	    		<td>{{$kpe}}</td>
	    	</tr>
	    </tbody>
	</table>
	<br/><br/>
<!--RIWAYAT PENDIDIKAN -->
	<p align="center" class="title">
   		<strong>RIWAYAT PENDIDIKAN FORMAL</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center" class="tabel">
		<thead class="header-warna">
	     <tr>
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Tingkat Pendidikan</th>
		    <th rowspan="2" class="tabel-td">Fakultas</th>
		    <th rowspan="2" class="tabel-td">Jurusan</th>
			<th colspan="3" class="tabel-td">STTB/Ijazah</th>
			<th colspan="2" class="tabel-td">Sekolah/Perguruan Tinggi</th>
		</tr>
		<tr>
		    <th class="tabel-td">Nomor</th>
		    <th class="tabel-td">Tanggal</th>
		    <th class="tabel-td">Nama Kepala Sekolah/Rektor</th>
		    <th class="tabel-td">Nama</th>
		    <th class="tabel-td">Lokasi (Kab./Kota)</th>
		</tr>
	     </thead>
	     <tbody>
	     	<?php 
	     	foreach($riwayat_pend as $rp){
	     		$tp= TingkatPendidikan::where('tingpend_id', $rp->tingpend_id)->first(); 
	     		$univ = Universitas::where('univ_id', $rp->univ_id)->first();
	     		$jurusan = Jurusan::where('jurusan_id', $rp->jurusan_id)->first();
	     		$fakultas = Fakultas::where('fakultas_id', $rp->fakultas_id)->first();

	     		if($tp->kd_tingpend == 2){
	     		  $lokasi_sekolah = $univ['univ_kota'];
	     		}elseif($tp->kd_tingpend ==1){
	     			$lokasi_sekolah = $rp->riw_pendidikan_lokasi;
	     		}

	     	?>
	     	<tr>
	     		<td class="tabel-td">{{ $i }}</td>
	     		<td class="tabel-td">{{ $tp->nm_tingpend }}</td>
	     		<td class="tabel-td">{{$fakultas['fakultas_nm']}}</td>
	     		<td class="tabel-td">{{$jurusan['jurusan_nm']}}</td>
	     		<td class="tabel-td">{{ $rp->riw_pendidikan_sttb_ijazah }}</td>
	     		<td class="tabel-td">{{getIndonesianDate($rp->riw_pendidikan_tgl)}}</td>
	     		<td class="tabel-td">{{$rp->riw_pendidikan_pejabat}}</td>
	     		<td class="tabel-td">{{$rp->riw_pendidikan_nm}}{{$univ['univ_nmpti']}}</td>
	     		<td class="tabel-td">{{$lokasi_sekolah}}</td>
	     		
	     	</tr>
	     	<?php $i++;  }  ?>
	     </tbody>
 	</table>
<br><br><br><br>
<!--RIWAYAT PENDIDIKAN NON FORMAL -->
	<p align="center" class="title">
   		<strong>RIWAYAT PENDIDIKAN NON FORMAL</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
	     <tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Nama Kursus/Seminar/LokaKarya</th>
		  	<th colspan="2" class="tabel-td">Tanggal</th>
			<th colspan="3" class="tabel-td">Ijazah/Tanda Lulus/Surat Keterangan</th>
			<th rowspan="2" class="tabel-td">Instansi<br/>Penyelenggara</th>
			<th rowspan="2" class="tabel-td">Tempat</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Mulai</th>
			<th class="tabel-td">selesai</th>
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Nama Pejabat</th>
		</tr>
	     </thead>
	    <tbody>
	    	<?php $i=1; foreach($pend_non as $pn){ ?>
	    	<tr>	
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$pn->non_nama}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($pn->non_tgl_mulai)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($pn->non_tgl_selesai)}}</td>
	    		<td class="tabel-td">{{$pn->non_sttp}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($pn->non_sttp_tanggal)}}</td>
	    		<td class="tabel-td">{{$pn->non_sttp_pejabat}}</td>
	    		<td class="tabel-td">{{$pn->non_penyelenggara}}</td>
	    		<td class="tabel-td">{{$pn->non_tempat}}</td>
	    	</tr>	
	    	<?php $i++; }?>
	    </tbody>	
	</table>
<br><br><br><br>
<!--RIWAYAT KEPANGKATAN -->
	<p align="center" class="title">
   		<strong>RIWAYAT KEPANGKATAN</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Gol.<br/>Ruang</th>
			<th colspan="2" class="tabel-td">Masa Kerja</th>
		  	<th rowspan="2" class="tabel-td">Gaji Pokok</th>
		  	<th colspan="3" class="tabel-td">Surat Keputusan</th>
			<th rowspan="2" class="tabel-td">TMT</th>
			<th rowspan="2" class="tabel-td">Unit Kerja</th>
		</tr>
		<tr align="center">
		    <th class="tabel-td">Thn</th>
		    <th class="tabel-td">Bln</th>
		    <th class="tabel-td">Nomor</th>
		    <th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan<br/>Penandatangan</th>
		</tr>
	     </thead>
	    <tbody>
    		<?php $i=1; 
    		foreach($riwayat_pangkat as $rp){ 
    			$gol_ = Golongan::where('gol_id', $rp->gol_id)->first(); 
				$gaji_ = Gaji::where('mgaji_id', $gaji_thn->mgaji_id)->where('gol_id', $rp->gol_id)->where('gaji_masakerja', $rp->riw_pangkat_thn)->first();
    		?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$gol_ ? $gol_->nm_gol : ''}}</td>
	    		<td class="tabel-td">{{$rp->riw_pangkat_thn}}</td>
	    		<td class="tabel-td">{{$rp->riw_pangkat_bln}}</td>
	    		<td class="tabel-td">Rp {{$gaji_ ? $gaji_['gaji_pokok'] : 0}}</td>
	    		<td class="tabel-td">{{$rp->riw_pangkat_sk}}</td>
	    		<td class="tabel-td">{{ getIndonesianDate($rp->riw_pangkat_sktgl)}}</td>
	    		<td class="tabel-td">{{$rp->riw_pangkat_pejabat}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rp->riw_pangkat_tmt)}}</td>
	    		<td class="tabel-td">{{$rp->riw_pangkat_unit_kerja}}</td>
	    	</tr>
	    	<?php $i++;  } ?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT JABATAN -->
	<p align="center" class="title">
   		<strong>RIWAYAT JABATAN</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Nama Jabatan</th>
			<th rowspan="2" class="tabel-td">Gol.<br/>Ruang</th>
			<th colspan="3" class="tabel-td">Surat Keputusan</th>
			<th rowspan="2" class="tabel-td">TMT</th>
			<th rowspan="2" class="tabel-td">Unit Kerja</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan Penandatangan</th>
		</tr>
	     </thead>
	    <tbody>
	    	<?php $i=1; 
	    	foreach($riwayat_jabatan as $rj){
	    		$gol_jab = Golongan::where('gol_id', $rj->gol_id)->first(); ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$rj->riw_jabatan_nm}}</td>
	    		<td class="tabel-td">{{$gol_jab ? $gol_jab['nm_gol'] : ''}}</td>
	    		<td class="tabel-td">{{$rj->riw_jabatan_no}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rj->riw_jabatan_tgl)}}</td>
	    		<td class="tabel-td">{{$rj->riw_jabatan_pejabat}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rj->riw_jabatan_tmt)}}</td>
	    		<td class="tabel-td">{{$rj->riw_jabatan_unit}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT DIKLAT STRUKTURAL -->
	<p align="center" class="title">
   		<strong>RIWAYAT DIKLAT STRUKTURAL</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center" class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th colspan="2" class="tabel-td">Diklat Struktural</th>
			<th colspan="2" class="tabel-td">Tanggal</th>
		    <th rowspan="2" class="tabel-td">Jmlh Jam</th>
		    <th colspan="3" class="tabel-td">STTP</th>
		    <th colspan="2" class="tabel-td">Instansi Penyelengggara</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Kategori</th>
			<th class="tabel-td">Nama</th>
			<th class="tabel-td">Mulai</th>
			<th class="tabel-td">Selesai</th>
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan<br/>Penandatangan</th>
		    <th class="tabel-td">Instansi</th>
		    <th class="tabel-td">Lokasi</th>
		</tr>
	    </thead>
	    <tbody>
	    	<?php 
	    		$i=1;
	    		foreach ($riwayat_diklat_s as $rd){
	    		$diklat_kat = DiklatStruktural::where('kategori_id', $rd->kategori_id)->first();
	    		if($diklat_kat){
		    		$kategori = DiklatStruktural::where('kategori_id', $diklat_kat['kategori_parent'])->first();	
	    		}else{
		    		$kategori = collect();	
	    		}
	    	 ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$kategori ? $kategori['kategori_nama'] : ''}}</td>
	    		<td class="tabel-td">{{$diklat_kat ? $diklat_kat['kategori_nama'] : ''}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rd->diklat_mulai)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rd->diklat_selesai)}}</td>
	    		<td class="tabel-td">{{$rd->diklat_jumlah_jam}}</td>
	    		<td class="tabel-td">{{preg_replace('/\//','$0 ',$rd->diklat_sttp_no)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rd->diklat_sttp_tgl)}}</td>
	    		<td class="tabel-td">{{$rd->diklat_sttp_pej}}</td>
	    		<td class="tabel-td">{{$rd->diklat_penyelenggara}}</td>
	    		<td class="tabel-td">{{$rd->diklat_tempat}}</td>
	    	</tr>	
	    	<?php $i++; }?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT DIKLAT FUNGSIONAL -->
	<p align="center" class="title">
   		<strong>RIWAYAT DIKLAT FUNGSIONAL</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna"> 
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Nama Diklat Fungsional</th>
			<th colspan="2" class="tabel-td">Tanggal</th>
		    <th rowspan="2" class="tabel-td">Jmlh Jam</th>
		    <th colspan="3" class="tabel-td">STTP</th>
		    <th colspan="2" class="tabel-td">Instansi Penyelengggara</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Mulai</th>
			<th class="tabel-td">Selesai</th>
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan<br/>Penandatangan</th>
		    <th class="tabel-td">Instansi</th>
		    <th class="tabel-td">Lokasi</th>
		</tr>
	     </thead>
	    <tbody>
	    	<?php $i=1;
		    	foreach ($riwayat_diklat_f as $rdf) {
		    	$nama_diklat = DiklatFungsional::where('diklat_fungsional_id', $rdf->diklat_fungsional_id)->first() ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$nama_diklat ? $nama_diklat['diklat_fungsional_nm'] : ''}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdf->diklat_mulai)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdf->diklat_selesai)}}</td>
	    		<td class="tabel-td">{{$rdf->diklat_jumlah_jam}}</td>
	    		<td class="tabel-td">{{preg_replace('/\//','$0 ',$rdf->diklat_sttp_no)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdf->diklat_sttp_tgl)}}</td>
	    		<td class="tabel-td">{{$rdf->diklat_sttp_pej}}</td>
	    		<td class="tabel-td">{{$rdf->diklat_penyelenggara}}</td>
	    		<td class="tabel-td">{{$rdf->diklat_tempat}}</td>
	    	</tr>
	    	<?php $i++; } ?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT DIKLAT TEKNIS -->
	<p align="center" class="title">
   		<strong>RIWAYAT DIKLAT TEKNIS</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Nama Diklat Teknis</th>
			<th colspan="2" class="tabel-td">Tanggal</th>
		    <th rowspan="2" class="tabel-td">Jmlh Jam</th>
		    <th colspan="3" class="tabel-td">STTP</th>
		    <th colspan="2" class="tabel-td">Instansi Penyelengggara</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Mulai</th>
			<th class="tabel-td">Selesai</th>
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan<br/>Penandatangan</th>
		    <th class="tabel-td">Instansi</th>
		    <th class="tabel-td">Lokasi</th>
		</tr>
	     </thead>
	      <tbody>
	    	<?php $i=1;
	    	foreach ($riwayat_diklat_t as $rdt){
	    	$nama_diklat = DiklatTeknis::where('diklat_teknis_id', $rdt->diklat_teknis_id)->first() ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$nama_diklat ? $nama_diklat->diklat_teknis_nm : ''}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdt->diklat_mulai)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdt->diklat_selesai)}}</td>
	    		<td class="tabel-td">{{$rdt->diklat_jumlah_jam}}</td>
	    		<td class="tabel-td">{{preg_replace('/\//','$0 ',$rdt->diklat_sttp_no)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($rdt->diklat_sttp_tgl)}}</td>
	    		<td class="tabel-td">{{$rdt->diklat_sttp_pej}}</td>
	    		<td class="tabel-td">{{$rdt->diklat_penyelenggara}}</td>
	    		<td class="tabel-td">{{$rdt->diklat_tempat}}</td>
	    	</tr>	
	    	<?php $i++; } ?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT PENGHARGAAN -->
	<p align="center" class="title">
   		<strong>RIWAYAT PENGHARGAAN</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Nama Penghargaan</th>
			<th colspan="3" class="tabel-td">Surat Keputusan</th>
			<th colspan="2" class="tabel-td">Instansi Penyelenggara</th>
		</tr>
		<tr align="center">
			<th class="tabel-td">Nomor</th>
			<th class="tabel-td">Tanggal</th>
			<th class="tabel-td">Jabatan Penandatangan</th>
			<th class="tabel-td">Instansi</th>
			<th class="tabel-td">Lokasi</th>
		</tr>
	    </thead>
	    <tbody>
	    	<?php $i=1;
	    	foreach ($penghargaan as $p){
	    	$np= Penghargaan::where('penghargaan_id', $p->penghargaan_id)->first(); ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$np ? $np['penghargaan_nm'] : ''}}</td>
	    		<td class="tabel-td">{{$p->riw_penghargaan_sk}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($p->riw_penghargaan_tglsk)}}</td>
	    		<td class="tabel-td">{{$p->riw_penghargaan_jabatan}}</td>
	    		<td class="tabel-td">{{$p->riw_penghargaan_instansi}}</td>
	    		<td class="tabel-td">{{$p->riw_penghargaan_lokasi}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT PASUTRI -->
	<?php
		if($pegawai->peg_jenis_kelamin == 'L'){
			$jkp = 'ISTRI';
			$jdl = 'Istri';
		}else{
			$jkp = 'SUAMI';
			$jdl = 'Suami';
		}
	?>
	<p align="center" class="title">
   		<strong>RIWAYAT {{$jkp}}</strong>
	</p>
	<br/><br/>
	<table border="1" cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
		<tr align="center">
			<th class="tabel-td">No.</th>
			<th class="tabel-td">Nama {{$jdl}}</th>
			<th class="tabel-td">Tempat dan Tanggal Lahir</th>
			<th class="tabel-td">Tanggal Nikah</th>
			<th class="tabel-td">Memperoleh<br/>Tunjangan</th>
			<th class="tabel-td">Pendidikan</th>
		    <th class="tabel-td">Pekerjaan</th>
		    <th class="tabel-td">Keterangan</th>
		</tr>
	    </thead>
	     <tbody>
	    	<?php $i=1;
	    		foreach ($kel_p as $k){ 
		    		if($k->riw_status_tunj == 'TRUE'){
		    			$tunj = 'YA';
		    		}else{
		    			$tunj = 'TIDAK';
		    		}
	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$k->riw_nama}}</td>
	    		<td class="tabel-td">{{$k->riw_tempat_lahir}} {{getIndonesianDate($k->riw_tgl_lahir)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($k->riw_tgl_ket)}}</td>
	    		<td class="tabel-td">{{$tunj}}</td>
	    		<td class="tabel-td">{{$k->riw_pendidikan}}</td>
	    		<td class="tabel-td">{{$k->riw_pekerjaan}}</td>
	    		<td class="tabel-td">{{$k->riw_ket}}</td>
	    	</tr>
	    	<?php $i++; } ?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT ANAK -->
	<p align="center" class="title">
   		<strong>RIWAYAT ANAK</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th class="tabel-td">No.</th>
			<th class="tabel-td">Nama Anak</th>
			<th class="tabel-td">Jenis<br/>Kelamin</th>
			<th class="tabel-td">Tempat dan Tanggal Lahir</th>
		    <th class="tabel-td">Status <br/>Perkawinan</th>
		    <th class="tabel-td">Memperoleh<br/>Tunjangan</th>
		    <th class="tabel-td">Pendidikan</th>
		    <th class="tabel-td">Pekerjaan</th>
		    <th class="tabel-td">Keterangan</th>
		</tr>
	     </thead>
	      <tbody>
	    	<?php $i=1;
	    		foreach ($kel_a as $k){ 
	    			$riw_kel = '-';
		    		if($k->riw_kelamin == 'L'){
	    				$riw_kel = 'Laki-laki';
	    			}elseif($k->riw_kelamin == 'P'){
	    				$riw_kel = 'Perempuan';
	    			}

	    			if($k->riw_status_perkawinan==1){
	    				$riw_kawin = 'Kawin';
	    			}elseif($k->riw_status_perkawinan==2){
	    				$riw_kawin = 'Belum Kawin';
	    			}elseif($k->riw_status_perkawinan==3){
	    				$riw_kawin = 'Janda';
	    			}elseif($k->riw_status_perkawinan==4){
	    				$riw_kawin = 'Duda';
	    			}else{
	    				$riw_kawin = '-';
	    			}

	    			if($k->riw_status_tunj == 'TRUE'){
		    			$tunj = 'YA';
		    		}else{
		    			$tunj = 'TIDAK';
		    		}
	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$k->riw_nama}}</td>
	    		<td class="tabel-td">{{$riw_kel}}</td>
	    		<td class="tabel-td">{{$k->riw_tempat_lahir}} {{(getIndonesianDate($k->riw_tgl_lahir))}}</td>
	    		<td class="tabel-td">{{$riw_kawin}}</td>
	    		<td class="tabel-td">{{$tunj}}</td>
	    		<td class="tabel-td">{{$k->riw_pendidikan}}</td>
	    		<td class="tabel-td">{{$k->riw_pekerjaan}}</td>
	    		<td class="tabel-td">{{$k->riw_ket}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT ORANG TUA -->
	<p align="center" class="title">
   		<strong>RIWAYAT ORANG TUA</strong>
	</p>
	<br/><br/>
	<table border="1" cellspacing="3" cellpadding="3" align="center" class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th class="tabel-td">No.</th>
			<th class="tabel-td">Nama Orang Tua</th>
			<th class="tabel-td">Jenis Kelamin</th>
			<th class="tabel-td">Tempat dan Tanggal Lahir</th>
			<th class="tabel-td">Pendidikan</th>
			<th class="tabel-td">Pekerjaan</th>
			<th class="tabel-td">Keterangan</th>
		</tr>
	     </thead>
	       <tbody>
	    	 <?php 
	    	 	$i =1;
	    	 	foreach ($kel_o as $k){
	    	 		$riw_kel = '-';
		    		if($k->riw_kelamin == 'L'){
	    				$riw_kel = 'Laki-laki';
	    			}elseif($k->riw_kelamin == 'P'){
	    				$riw_kel = 'Perempuan';
	    			}
	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$k->riw_nama}}</td>
	    		<td class="tabel-td">{{$riw_kel}}</td>
	    		<td class="tabel-td">{{$k->riw_tempat_lahir}} {{getIndonesianDate($k->riw_tgl_lahir)}}</td>
	    		<td class="tabel-td">{{$k->riw_pendidikan}}</td>
	    		<td class="tabel-td">{{$k->riw_pekerjaan}}</td>
	    		<td class="tabel-td">{{$k->riw_ket}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
<br><br><br><br>
<!--RIWAYAT SAUDARA-->
	<p align="center" class="title">
   		<strong>RIWAYAT SAUDARA</strong>
	</p>
	<br/><br/>
	<table border="1" cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th class="tabel-td">No.</th>
			<th class="tabel-td">Nama Saudara</th>
			<th class="tabel-td">Jenis Kelamin</th>
			<th class="tabel-td">Tempat dan Tanggal Lahir</th>
			<th class="tabel-td">Pendidikan</th>
			<th class="tabel-td">Pekerjaan</th>
			<th class="tabel-td">Keterangan</th>
		</tr>
	     </thead>
	     <tbody>
	    	<?php $i=1;
		    	foreach ($kel_s as $k){ 
		    		$riw_kel = '-';
		    		if($k->riw_kelamin == 'L'){
	    				$riw_kel = 'Laki-laki';
	    			}elseif($k->riw_kelamin == 'P'){
	    				$riw_kel = 'Perempuan';
	    			}
	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$k->riw_nama}}</td>
	    		<td class="tabel-td">{{$riw_kel}}</td>
	    		<td class="tabel-td">{{$k->riw_tempat_lahir}} {{ getIndonesianDate($k->riw_tgl_lahir)}}</td>
	    		<td class="tabel-td">{{$k->riw_pendidikan}}</td>
	    		<td class="tabel-td">{{$k->riw_pekerjaan}}</td>
	    		<td class="tabel-td">{{$k->riw_ket}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
	<br><br><br><br>
	<p align="center" class="title">
   		<strong>RIWAYAT KEAHLIAN</strong>
	</p>
	<br/><br/>
	<table border="1" cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna">
			<tr align="center">
				<th rowspan="2" class="tabel-td">No.</th>
				<th rowspan="2" class="tabel-td">Nama Keahlian</th>
				<th rowspan="2" class="tabel-td">Level Keahlian</th>
			    <th rowspan="2" class="tabel-td">Tanggal Mulai Keahlian</th>
			    <th colspan="3" class="tabel-td">Pendidikan Penunjang</th>
			    <th rowspan="2" class="tabel-td">Predikat</th>
			</tr>
			<tr align="center">
				<th class="tabel-td">Formal</th>
				<th class="tabel-td">Non Formal</th>
				<th class="tabel-td">Diklat</th>
			</tr>
	     </thead>
	     <tbody>
	    	<?php $i=1;
		    	foreach ($keahlian as $k){ 
		    		$riw_keahlian = RiwayatKeahlian2::where('peg_id', $pegawai->peg_id)->where('keahlian_id',$k->keahlian_id)->get();
			    	foreach ($riw_keahlian as $rk){ 
			    		$keahlian = Keahlian::where('keahlian_id',$rk->keahlian_id)->first();
			    		$level = KeahlianLevel::where('keahlian_level_id', $rk->keahlian_level_id)->first();
		      			$rel = RiwayatKeahlianRel2::where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
		      			$pend = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel['riw_pendidikan_id'])->first();
		      			$univ = Universitas::where('univ_id', $pend['univ_id'])->first();
		      			$pend_non = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->where('non_id', $rel['non_id'])->first();
		      			$diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel['diklat_id'])->first();
		      			if($diklat){
			      			if($diklat['diklat_jenis']==1){
								$jns = DiklatStruktural::where('kategori_id', $diklat->kategori_id)->first();
								$nm = $jns['kategori_nama'];
							}elseif($diklat['diklat_jenis']==2){
								$jns = DiklatFungsional::where('diklat_fungsional_id', $diklat->diklat_fungsional_id)->first();
								$nm = $jns['diklat_fungsional_nm'];
							}elseif($diklat['diklat_jenis']==3){
								$jns = DiklatTeknis::where('diklat_teknis_id', $diklat->diklat_teknis_id)->first();
								$nm = $jns['diklat_teknis_nm'];
							}else{
								$nm = '-';
							}
						}else{
							$nm = "-";
						}
	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$keahlian ? $keahlian['keahlian_nama'] : ''}}</td>
      			<td class="tabel-td">{{$level ? $level['keahlian_level_nama'] : ''}}</td>
      			<td class="tabel-td">{{$rk->riw_keahlian_sejak}}</td>
      			<td class="tabel-td">{{$pend ? $pend['riw_pendidikan_nm'] : ''}}{{$univ ? $univ['univ_nmpti'] : ''}}</td>
      			<td class="tabel-td">{{$pend_non ? $pend_non['non_nama'] : ''}}</td>
      			<td class="tabel-td">{{$nm}}</td>
      			<td class="tabel-td">{{$rel ? $rel['predikat'] : ''}}</td>
	    	</tr>
	    	<?php $i++; }}?>
	    </tbody>
	</table>
	<br><br><br><br>
<!--RIWAYAT BAHASA-->
	<p align="center" class="title">
   		<strong>RIWAYAT KEMAMPUAN BAHASA</strong>
	</p>
	<br/><br/>
	<table border="1" cellspacing="3" cellpadding="3" align="center"  class="tabel">
		 <thead class="header-warna">
		<tr align="center">
			<th rowspan="2" class="tabel-td">No.</th>
			<th rowspan="2" class="tabel-td">Bahasa</th>
			<th colspan="4" class="tabel-td">Level Keahlian</th>
		    <th rowspan="2" class="tabel-td">Tanggal Mulai Keahlian</th>
		    <th colspan="3" class="tabel-td">Pendidikan Penunjang</th>
		    <th rowspan="2" class="tabel-td">Predikat</th>
		</tr>
		<tr align="center">
			<th>Membaca</th>
			<th>Mendengarkan</th>
			<th>Menulis</th>
			<th>Berbicara</th>
			<th>Formal</th>
			<th>Non Formal</th>
			<th>Diklat</th>
		</tr>
	     </thead>
	     <tbody>
	    	<?php $i=1;
		    	foreach ($keahlian_bahasa as $kb){ 
		    		$riw_keahlian_bahasa = RiwayatKeahlian2::where('peg_id', $pegawai->peg_id)->where('keahlian_id',$kb->keahlian_id)->get();
	    			foreach($riw_keahlian_bahasa as $rk){
	    				$array = str_split($rk->keahlian_level_id);
		      			if($array[0]!=0){
		      				$lv1 = $array[0].$array[1];
		      			}elseif($array[0] ==0 && $array[1] == 0){
		      				$lv1 = null;
		      			}else{
		      				$lv1 = $array[1];
		      			}

		      			if($array[2]!=0){
		      				$lv2 = $array[2].$array[3];
		      			}elseif($array[2] ==0 && $array[3] == 0){
		      				$lv2 = null;
		      			}else{
		      				$lv2 = $array[3];
		      			}

		      			if($array[4]!=0){
		      				$lv3 = $array[4].$array[5];
		      			}elseif($array[4] ==0 && $array[5] == 0){
		      				$lv3 = null;
		      			}else{
		      				$lv3 = $array[5];
		      			}

		      			if($array[6]!=0){
		      				$lv4 = $array[6].$array[7];
		      			}elseif($array[6] ==0 && $array[7] == 0){
		      				$lv4 = null;
		      			}else{
		      				$lv4 = $array[7];
		      			}

		      			$level1 = KeahlianLevel::where('keahlian_level_id', $lv1)->first();
		      			$level2 = KeahlianLevel::where('keahlian_level_id', $lv2)->first();
		      			$level3 = KeahlianLevel::where('keahlian_level_id', $lv3)->first();
		      			$level4 = KeahlianLevel::where('keahlian_level_id', $lv4)->first();
		      			$rel = RiwayatKeahlianRel2::where('riw_keahlian_id', $rk->riw_keahlian_id)->first();
		      			$pend = RiwayatPendidikan2::where('peg_id', $pegawai->peg_id)->where('riw_pendidikan_id', $rel['riw_pendidikan_id'])->first();
		      			$univ = Universitas::where('univ_id', $pend['univ_id'])->first();
		      			$pend_non = RiwayatNonFormal2::where('peg_id', $pegawai->peg_id)->where('non_id', $rel['non_id'])->first();
		      			$diklat = RiwayatDiklat2::where('peg_id', $pegawai->peg_id)->where('diklat_id', $rel['diklat_id'])->first();
		      			if($diklat){
			      			if($diklat['diklat_jenis']==1){
								$jns = DiklatStruktural::where('kategori_id', $diklat->kategori_id)->first();
								$nm = $jns['kategori_nama'];
							}elseif($diklat['diklat_jenis']==2){
								$jns = DiklatFungsional::where('diklat_fungsional_id', $diklat->diklat_fungsional_id)->first();
								$nm = $jns['diklat_fungsional_nm'];
							}elseif($diklat['diklat_jenis']==3){
								$jns = DiklatTeknis::where('diklat_teknis_id', $diklat->diklat_teknis_id)->first();
								$nm = $jns['diklat_teknis_nm'];
							}
						}else{
							$nm = "";
						}

	    	?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$kb->keahlian_nama}}</td>
      			<td class="tabel-td">{{$level1['keahlian_level_nama']}}</td>
      			<td class="tabel-td">{{$level2['keahlian_level_nama']}}</td>
      			<td class="tabel-td">{{$level3['keahlian_level_nama']}}</td>
      			<td class="tabel-td">{{$level4['keahlian_level_nama']}}</td>
      			<td class="tabel-td">{{$rk->riw_keahlian_sejak}}</td>
      			<td class="tabel-td">{{$pend['riw_pendidikan_nm']}}{{$univ['univ_nmpti']}}</td>
      			<td class="tabel-td">{{$pend_non['non_nama']}}</td>
      			<td class="tabel-td">{{$nm}}</td>
      			<td class="tabel-td">{{$rel['predikat']}}</td>
	    	</tr>
	    	<?php $i++; }}?>
	    </tbody>
	</table>
	<br><br><br><br>
    @if (in_array(Auth::user()->role_id, canSkpdEditJabatan() ? [1,3,2,5] : [1,3]))

	<!--CATATAN HUKDIS -->
	<p align="center" class="title">
   		<strong>CATATAN HUKUMAN DISIPLIN</strong>
	</p>
	<br/><br/>
	<table cellspacing="3" cellpadding="3" align="center"  class="tabel">
		<thead class="header-warna">
			<tr align="center">
				<th rowspan="2" class="tabel-td">No.</th>
				<th rowspan="2" class="tabel-td">Kategori Hukuman</th>
				<th rowspan="2" class="tabel-td">Nama Hukuman</th>
			    <th colspan="2" class="tabel-td">SK</th>
			    <th colspan="2" class="tabel-td">Lama</th>
			    <th rowspan="2" class="tabel-td">Keterangan Pelanggaran</th>
			</tr>
			<tr align="center">
				<th class="tabel-td">No SK</th>
				<th class="tabel-td">Tanggal SK</th>
				<th class="tabel-td">Tanggal Mulai</th>
				<th class="tabel-td">Tanggal Selesai</th>
			</tr>
	    </thead>
	    <tbody>
	    	<?php $i=1;
	    	foreach ($hukuman as $hk){
	    		$hkm= Hukum::where('mhukum_id', $hk->mhukum_id)->first();

	    		if($hkm){
		    		$kat_hukuman = HukumKategori::where('mhukum_cat_id', $hkm->mhukum_cat_id)->first();
	    		}else{
	    			$kat_hukuman = collect();
	    		}
	    	 ?>
	    	<tr>
	    		<td class="tabel-td">{{$i}}</td>
	    		<td class="tabel-td">{{$kat_hukuman ? $kat_hukuman['mhukum_cat_nm'] : ''}}</td>
	    		<td class="tabel-td">{{$hkm ? $hkm->mhukum_hukuman : ''}}</td>
	    		<td class="tabel-td">{{$hk->riw_hukum_sk}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($hk->riw_hukum_tgl)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($hk->riw_hukum_tmt)}}</td>
	    		<td class="tabel-td">{{getIndonesianDate($hk->riw_hukum_sd)}}</td>
	    		<td class="tabel-td">{{$hk->riw_hukum_ket}}</td>
	    	</tr>
	    	<?php $i++; }?>
	    </tbody>
	</table>
	@endif
</body>
</html>