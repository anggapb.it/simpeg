@extends('layouts.app')

@section('content')
<center><h4>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH</h4>
        <h4>DI LINGKUNGAN PEMERINTAH KOTA BANDUNG</h4>
        <h4>TAHUN <?php echo (date('Y'));?></h4><br>
</center>

<div id="vm" class="row">
    <div class="col-xs-12">
        <br>
        <a href="{{url('rekap/golongan/export')}}/" class="btn btn-xs btn-primary">Export Excel</a><br><br>

               <div id="isi" class="table-responsive">
                  <table class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
                      <thead>
                          <tr>
                              <th rowspan="2">No.</th>
                              <th v-on:click="orderTable(1)" rowspan="2">NAMA SKPD</th>
                              <th v-on:click="orderTable(2)" colspan="17">GOLONGAN</th>
                              <th v-on:click="orderTable(4)" rowspan="2">JUMLAH SELURUH</th>
                          </tr>
                          <tr>
                              <th>I/a</th>
                              <th>I/b</th>
                              <th>I/c</th>
                              <th>I/d</th>
                              <th>II/a</th>
                              <th>II/b</th>
                              <th>II/c</th>
                              <th>II/d</th>
                              <th>III/a</th>
                              <th>III/b</th>
                              <th>III/c</th>
                              <th>III/d</th>
                              <th>IV/a</th>
                              <th>IV/b</th>
                              <th>IV/c</th>
                              <th>IV/d</th>
                              <th>IV/e</th>
                          </tr>
                          <tr>
                              <th>1</th>
                              <th>2</th>
                              <th>3</th>
                              <th>4</th>
                              <th>5</th>
                              <th>6</th>
                              <th>7</th>
                              <th>8</th>
                              <th>9</th>
                              <th>10</th>
                              <th>11</th>
                              <th>12</th>
                              <th>13</th>
                              <th>14</th>
                              <th>15</th>
                              <th>16</th>
                              <th>17</th>
                              <th>18</th>
                              <th>19</th>
                              <th>20</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr v-for="item in items">
                              <td v-text="item[0]"></td>
                              <td v-html="item[1]"></td>
                              <td><span v-text="item[2]"></span></td>
                              <td><span v-text="item[3]"></span></td>
                              <td><span v-text="item[4]"></span></td>
                              <td><span v-text="item[5]"></span></td>
                              <td><span v-text="item[6]"></span></td>
                              <td><span v-text="item[7]"></span></td>
                              <td><span v-text="item[8]"></span></td>
                              <td><span v-text="item[9]"></span></td>
                              <td><span v-text="item[10]"></span></td>
                              <td><span v-text="item[11]"></span></td>
                              <td><span v-text="item[12]"></span></td>
                              <td><span v-text="item[13]"></span></td>
                              <td><span v-text="item[14]"></span></td>
                              <td><span v-text="item[15]"></span></td>
                              <td><span v-text="item[16]"></span></td>
                              <td><span v-text="item[17]"></span></td>
                              <td><span v-text="item[18]"></span></td>
                              <td><span v-text="item[19]"></span></td>
                          </tr>
                      </tbody>
                      <tfoot>
                          <tr>
                            <td colspan="2" style="text-align: right"><b>Total</b></td>
                            <td v-text="jumlah.a1"></td>
                            <td v-text="jumlah.b1"></td>
                            <td v-text="jumlah.c1"></td>
                            <td v-text="jumlah.d1"></td>
                            <td v-text="jumlah.a2"></td>
                            <td v-text="jumlah.b2"></td>
                            <td v-text="jumlah.c2"></td>
                            <td v-text="jumlah.d2"></td>
                            <td v-text="jumlah.a3"></td>
                            <td v-text="jumlah.b3"></td>
                            <td v-text="jumlah.c3"></td>
                            <td v-text="jumlah.d3"></td>
                            <td v-text="jumlah.a4"></td>
                            <td v-text="jumlah.b4"></td>
                            <td v-text="jumlah.c4"></td>
                            <td v-text="jumlah.d4"></td>
                            <td v-text="jumlah.e4"></td>
                            <td v-text="jumlah.total"></td>
                          </tr>
                      </tfoot>
                  </table>
                    <div class="form-inline pull-left">
                      Tampilkan
                        <select v-model="pagination.perpage" v-on:change="changePage(1)">
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="200">200</option>
                            <option value="1000">1000</option>
                        </select>
                      data
                      <div class="animated fadeIn pull-left" v-show="isLoading">
                          <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
                      </div>
                    </div>
                  <div class="form-inline pull-right">
                    <div class="form-group">
                      Halaman &nbsp;
                      <!--first page button-->
                      <a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
                      <a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
                      <input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
                      <a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
                      <a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
                      &nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                      </div>
                      &nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
                  </div>
                </div>
            </div>
    </div>
</div>

@endsection
@section('styles')
<style>
  thead {
      text-align: center;
  }
  .tab-content {
    border:none;
  }
</style>
@endsection
@section('scripts')
<script src="{{asset('js/vue1.0.min.js')}}"></script>
<script type="text/javascript">
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: false,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 200,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
    tanggal_from : '',
    tanggal_to : '',
    jumlah:{},
    total: {
      persen_hari_d: 0,
      persen_hari_s: 0,
      persen_hari_i: 0,
      persen_hari_total_tk: 0,
      persen_k: 0,
      persen_hari_c: 0,
    },
    order: 1,
    dir: 1,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      if (page) this.pagination.page = page;
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_input = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    },
    orderTable: function(col) {
      if (col != this.order) {
        if (col == 1) {
          this.dir = 1;
        } else {
          this.dir = -1;
        }
      } else {
        this.dir = -this.dir;
      }
      this.order = col;
    },
    compareItem: function(a,b) {
      return this.order == 1 ?
        (a[this.order] > b[this.order] ? 1 : -1)
        :
        (parseFloat(a[this.order]) - parseFloat(b[this.order]));
    }
  }
});
vm.changePage();
function getData(page) {
  vm.isLoading = true;
  $.getJSON("{{url('rekap/get-data-golongan')}}",
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.total = data.total;
    vm.jumlah = data.jumlah;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
  });
}



</script>
@endsection
