@extends('layouts.app')

@section('content')
<center><h4>REKAPITULASI PEGAWAI NEGERI SIPIL DAERAH</h4>
        <h4>DI LINGKUNGAN PEMERINTAH KOTA BANDUNG</h4>
        <h4>TAHUN <?php echo (date('Y'));?></h4><br>
</center>

<div id="vm" class="row">
    <div class="col-xs-12">
        <br>
        <a href="{{url('rekap/pendidikan/export')}}" class="btn btn-xs btn-primary">Export Excel</a><br>
        <div id="isi" class="table-responsive">
        <table class="table table-striped table-bordered responsive" v-bind:class="{ 'dimmed': isLoading }">
            <thead>
                <tr>
                    <th rowspan="2">No.</th>
                    <th v-on:click="orderTable(1)" rowspan="2">NAMA SKPD</th>
                    <th v-on:click="orderTable(2)" colspan="10">TINGKAT PENDIDIKAN</th>
                    <th v-on:click="orderTable(4)" rowspan="2">JUMLAH SELURUH</th>
                </tr>
                <tr>
                    <th>SD</th>
                    <th>SMP</th>
                    <th>SMA</th>
                    <th>D1</th>
                    <th>D2</th>
                    <th>D3</th>
                    <th>D4</th>
                    <th>S1</th>
                    <th>S2</th>
                    <th>S3</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in items">
                    <td v-text="item[0]"></td>
                    <td v-html="item[1]"></td>
                    <td v-html="item[2]"></td>
                    <td v-html="item[3]"></td>
                    <td v-html="item[4]"></td>
                    <td v-html="item[5]"></td>
                    <td v-html="item[6]"></td>
                    <td v-html="item[7]"></td>
                    <td v-html="item[8]"></td>
                    <td v-html="item[9]"></td>
                    <td v-html="item[10]"></td>
                    <td v-html="item[11]"></td>
                    <td v-html="item[12]"></td>
                </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2" style="text-align: right"><b>Total</b></td>
                <td v-text="jumlah.sd"></td>
                <td v-text="jumlah.smp"></td>
                <td v-text="jumlah.sma"></td>
                <td v-text="jumlah.d1"></td>
                <td v-text="jumlah.d2"></td>
                <td v-text="jumlah.d3"></td>
                <td v-text="jumlah.d4"></td>
                <td v-text="jumlah.s1"></td>
                <td v-text="jumlah.s2"></td>
                <td v-text="jumlah.s3"></td>
                <td v-text="jumlah.total"></td>
              </tr>
            </tfoot>
        </table>
          <div class="form-inline pull-left">
            Tampilkan
              <select v-model="pagination.perpage" v-on:change="changePage(1)">
                  <option value="20">20</option>
                  <option value="50">50</option>
                  <option value="200">200</option>
                  <option value="1000">1000</option>
              </select>
            data
          <div class="animated fadeIn pull-left" v-show="isLoading">
              <i class="fa fa-refresh fa-spin"></i> Sedang memuat data
          </div>
        </div>
        <div class="form-inline pull-right">
          <div class="form-group">
            Halaman &nbsp;
            <!--first page button-->
            <a class="btn btn-default btn-xs" v-on:click="paginate('first')"><i class="fa fa-step-backward"></i></a>
            <a class="btn btn-default btn-xs" v-on:click="paginate('previous')"><i class="fa fa-chevron-left"></i></a>
            <input class="form-control input-sm" type="text" v-model="pagination.page" v-on:change="changePage()">
            <a class="btn btn-default btn-xs" v-on:click="paginate('next')"><i class="fa fa-chevron-right"></i></a>
            <a class="btn btn-default btn-xs" v-on:click="paginate('last')"><i class="fa fa-step-forward"></i></a>
            &nbsp;dari&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp; Entri&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;sampai&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;dari&nbsp;@{{ pagination.count }}</td>
      </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<style>
  thead {
      text-align: center;
  }
</style>
@endsection
@section('scripts')
<script src="{{asset('js/vue1.0.min.js')}}"></script>
<script type="text/javascript">
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: false,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 200,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    sortColumn: null,
    sortDir: null,
    tanggal_from : '',
    tanggal_to : '',
    jumlah : {},
    total: {
      persen_hari_d: 0,
      persen_hari_s: 0,
      persen_hari_i: 0,
      persen_hari_total_tk: 0,
      persen_k: 0,
      persen_hari_c: 0,
    },
    order: 1,
    dir: 1,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      if (page) this.pagination.page = page;
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_input = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      this.search = this.search_input;
      this.pagination.page = 1;
      this.changePage();
    },
    orderTable: function(col) {
      if (col != this.order) {
        if (col == 1) {
          this.dir = 1;
        } else {
          this.dir = -1;
        }
      } else {
        this.dir = -this.dir;
      }
      this.order = col;
    },
    compareItem: function(a,b) {
      return this.order == 1 ?
        (a[this.order] > b[this.order] ? 1 : -1)
        :
        (parseFloat(a[this.order]) - parseFloat(b[this.order]));
    }
  }
});
vm.changePage();
function getData(page) {
  vm.isLoading = true;
  $.getJSON("{{url('rekap/get-data-pendidikan')}}",
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.total = data.total;
    vm.jumlah = data.jumlah;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
  });
}



</script>
@endsection
