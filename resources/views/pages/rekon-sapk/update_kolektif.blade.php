@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
  <div class="row">
    <h3>Update Data Kenaikan Pangkat Kolektif</h3>
    <span class="small">File diperoleh dari <a href="https://ncsisadmin.bkn.go.id/" target="_blank">NCSIS Admin BKN</a> dan berekstensi *.xls <i>(Microsoft Excel 97-2003 Workbook)</i></span>
    <br />
    <span class="small">File hanya boleh memiliki 1 (satu) sheet dan data harus dimulai dari baris ke-2 (dua)</i></span>
    <hr />
    @if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
            <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
            </button>
            {{ session('message') }}
        </div>
	@endif
    <form method="post" action="{{url('/rekon-sapk/update-kp')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="table-responsive">
        <table class="table no-footer">
            <tr>
            @include('form.file', ['label'=>'File','required'=>true,'name'=>'excelfile', 'value'=>''])                
            </tr>
            <tr>
            @include('form.radio2',['label'=>'Riwayat Pangkat','required'=>true,'name'=>'riwayat_kp','value'=>'1', 'data'=>[
            '1' => 'Mutakhirkan','2' => 'Abaikan' ]])        
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input name="update_pokok" type="checkbox" value="update"> Update Data Pribadi <span style="color:#FF0000; font-weight:bold;">(Hati-hati)</span></td>                           
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" value="Upload" class="btn btn-primary btn-xs" /></td>                                
            </tr>
        </table>
    </div>
    </form>    
  </div>
</div>
@endsection