@extends('layouts.app')
<?php

?>
@section('content')
<div class="container-fluid">
  <div class="row">
    <h1>Upload File Data Pegawai versi BKN</h1>
    <h6>File diperoleh dari <a href="https://ncsisadmin.bkn.go.id/" target="_blank">BKN</a> dan berekstensi *.xls <i>(Microsoft Excel 97-2003 Workbook)</i></h6>
    <hr>
    @if (session('message'))
	    <div class="alert alert-success"  id="messageFlash">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        {{ session('message') }}
      </div>
		@endif
    <form method="post" action="{{url('/rekon-sapk/upload')}}" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="col-lg-9">
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-1">File</label>
          <div class="col-lg-9">
             <input class="form-control" id="file" name="excelfile" type="file"/>
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-1">&nbsp;</label>
          <div class="col-lg-9">
              <input type="submit" value="Upload" class="btn btn-primary btn-sm" />
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
