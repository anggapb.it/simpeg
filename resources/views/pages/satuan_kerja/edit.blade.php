@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
	<h3>Tambah Satuan Kerja</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('data/satker/update/'.$model->satuan_kerja_id)}}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                <table class="table no-footer">
                  <tr>
                    @include('form.txt',['label'=>'Satuan Kerja Nama','required'=>false,'name'=>'satuan_kerja_nama','vue'=>'','value'=>$model->satuan_kerja_nama])
                  </tr>
                	<tr>
                    @include('form.txt',['label'=>'Satuan Kerja Alamat','required'=>false,'name'=>'satuan_kerja_alamat','vue'=>'','value'=>$model->satuan_kerja_alamat])
                  </tr>
                	<tr>
                   @include('form.radio2',['label'=>'Status','required'=>false,'name'=>'status','data'=>['1' => 'Aktif','0' => 'Tidak Aktif' ], 'value'=>$model->status])
                  </tr>
                </table>
                </div>
                 <button class="btn btn-primary btn-xs" type="submit">Simpan</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  </script>
@endsection