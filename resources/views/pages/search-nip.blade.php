@extends('layouts.app-no-auth')

@section('content')
<div class="col-sm-10 col-sm-offset-1">
	<div class="login-container" style="width: 700px">

		<div class="space-6"></div>
		<div class="position-relative">
			<div id="login-box" class="login-box visible widget-box no-border">
				<div class="widget-body">
					<div class="widget-main">
						<div class="space-6"></div>
							<fieldset>
								<div class="row">
									<div class="col-md-10">
										<input type="text" name="nip" class="form-control" id="nip_cari" placeholder="NIP Pegawai">
									</div>
									<div class="col-md-2"><button class="btn btn-primary btn-xs" id="cari" type="button"><span class="fa fa-search"></span> Cari</button></div>
								</div>
								<hr>
								<div class="row" id="data-pegawai">
									<div class="col-sm-12">
										<div class="profile-user-info profile-user-info-striped">
											<div class="profile-info-row">
												<div class="profile-info-name"> Nama </div>
												<div class="profile-info-value">
													<span id="nama"></span>&nbsp;
												</div>
											</div>
											<div class="profile-info-row">
												<div class="profile-info-name">Jenis Jabatan </div>
												<div class="profile-info-value">
													<span id="jenjab"></span>&nbsp;
												</div>
											</div>
											<div class="profile-info-row">
												<div class="profile-info-name"> Jabatan </div>
												<div class="profile-info-value">
													<span id="jabatan"></span>&nbsp;
												</div>
											</div>																						
											<div class="profile-info-row">
												<div class="profile-info-name"> Jenis Kelamin </div>
												<div class="profile-info-value">
													<span id="jenkel"></span>&nbsp;
												</div>
											</div>											
											<div class="profile-info-row">
												<div class="profile-info-name"> Pangkat / Gol.Ruang </div>
												<div class="profile-info-value">
													<span id="golongan"></span>&nbsp;
												</div>
											</div>
											<div class="profile-info-row">
												<div class="profile-info-name"> Pendidikan Terakhir </div>
												<div class="profile-info-value">
													<span id="pendidikan_terakhir"></span>&nbsp;
												</div>
											</div>
											<div class="profile-info-row">
												<div class="profile-info-name"> Unit Kerja </div>
												<div class="profile-info-value">
													<span id="sopd"></span>&nbsp;
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
					</div><!-- /.widget-main -->
				</div><!-- /.widget-body -->
			</div><!-- /.login-box -->
		</div><!-- /.position-relative -->

	</div>
</div><!-- /.col -->
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		// $('#data-pegawai').hide();
	});

	$(document).on('click','#cari',function(){
		var nip = $('#nip_cari').val();

		if(nip == ''){
			alert('NIP Tidak Boleh Kosong');
			return;
		}

		$.post("{{url('search-nip/search')}}",{_token:"{{csrf_token()}}",nip:nip},function(data){
			if(!data){
				alert('Pegawai Tidak Ditemukan!');
				return;
			}

			var form = ['nama','jenjab','jabatan','jenkel','golongan','pendidikan_terakhir','sopd'];
			for (var i = 0; i < data.length; i++) {
				if(i == 7) continue;
				if(i == 6){
					var html = '';
					for (var a = 0; a < data[i].length; a++) {
						if(a == 0){
							html += data[i][a].nama+'<br>';
						}else if(a == 1){
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+data[i][a].nama+'<br>';
						}else if(a == 2){
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+data[i][a].nama+'<br>';
						}
					}
					if(data[i].length == 0){
						html += "<b>"+data[7]+"</b>";
					}else if(data[i].length == 1){
						html += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+data[7]+"</b>";
					}else if(data[i].length == 2){
						html += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+data[7]+"</b>";
					}else if(data[i].length == 3){
						html += "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+data[7]+"</b>";
					}

					$('#'+form[i]).html(html);
				}else{
					$('#'+form[i]).html(data[i]);
				}
			}
		});
	});
</script>
@endsection