@extends('layouts.app')

@section('content')
<h2 align="center">
  Search Pegawai
</h2>
<br>
<br>
<div id="vm">
      <div class="col-lg-9">
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Cari</label>
          <div class="col-lg-9">
             <input class="form-control" id="psearch" name="psearch" placeholder="Masukkan kata kunci" type="text" value="" v-model="search_input" v-on="keyup:doSearch | key 'enter'">
          </div>
        </div>
        <section style="display: none" v-show="showAdvancedSearch">
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Nama</label>
          <div class="col-lg-9">
             <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_nama" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Jabatan</label>
          <div class="col-lg-9">
              <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_jabatan" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">NIP</label>
          <div class="col-lg-9">
              <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_nip" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Nomor HP</label>
          <div class="col-lg-9">
              <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_hp" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Instansi (Dipekerjakan)</label>
          <div class="col-lg-9">
              <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_instansi" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Satuan Kerja</label>
          <div class="col-lg-9">
            <select class="psearch" id="m_satker" v-model="search_input_satker">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\SatuanKerja::where('satuan_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('satuan_kerja_nama','asc')->lists('satuan_kerja_nama','satuan_kerja_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
            </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Unit Kerja</label>
          <div class="col-lg-9">
            <select name="psearch" id="m_uker" v-model="search_input_uker">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\UnitKerja::where('unit_kerja_nama', 'not like', '%-%')->where('status', 1)->orderBy('unit_kerja_nama','asc')->lists('unit_kerja_nama','unit_kerja_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Eselon</label>
          <?php 
          $arrayno = 0;
          ?>
          <div class="col-lg-9"> 
          @foreach ($data=App\Model\Eselon::lists('eselon_nm','eselon_id') as $key => $value)
          <div class="col-md-1">
            <label><input id="m_eselon" type="checkbox" name="m_eselon" v-model="search_input_eselon[{{$arrayno++}}]" value="{{$key}}">{{$value}}</label>
          </div>
          @endforeach
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Jenis Jabatan</label>
          <div class="col-lg-9">
            <div class="col-md-2">
              <label>
                <input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="2" v-model="search_input_jenjab">Struktural
              </label>
            </div>
            <div class="col-md-2">
              <label>
                <input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="3" v-model="search_input_jenjab">Fungsional Tertentu
              </label>
            </div>
            <div class="col-md-2">
              <label>
                <input type="radio" name="m_jenis_jabatan" id="m_jenis_jabatan" value="4" v-model="search_input_jenjab">Fungsional Umum
              </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Kelas Jabatan</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_kelas_jabatan" v-model="search_kelas_jabatan">
                  <option value="">Please Select</option>
                  @foreach ($data=DB::connection('pgsql2')->table('m_spg_jabatan_kelas')->lists('kelas','kelas') as $key => $value)
                    <option value="{{$key}}">{{$key}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Golongan</label>
          <?php
            $nogol = 0;
          ?>
          <div class="col-lg-9"> 
          @foreach ($data=App\Model\Golongan::lists('nm_gol','gol_id') as $key => $value)
          <div class="col-lg-1">
          <label>
              <input type="checkbox" name="m_golongan" id="m_golongan" value="{{$key}}" v-model="search_input_golongan[{{$nogol++}}]">{{$value}}</label>
          </div>
          @endforeach
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Agama</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_agama" v-model="search_input_agama">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\Agama::lists('nm_agama','id_agama') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Status Pegawai</label>
          <div class="col-lg-9">
              <select  name="psearch" id="status_pegawai" v-model="search_input_status_pegawai">
                  <option value="">Please Select</option>
                  @foreach ($data=['1'=>'PNS','2'=>'CPNS'] as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Tingkat Pendidikan</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_tingpend" v-model="search_input_tingpend">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\TingkatPendidikan::orderBy('kode_urut_pend')->lists('nm_tingpend','tingpend_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Universitas</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_universitas" v-model="search_input_universitas">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\Universitas::orderBy('univ_nmpti')->lists('univ_nmpti','univ_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Jurusan</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_jurusan" v-model="search_input_jurusan">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\Jurusan::orderBy('jurusan_nm')->lists('jurusan_nm','jurusan_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Riwayat Pendidikan</label>
          <div class="col-lg-9">
              <input class="form-control" id="psearch" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_riwayat" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Jenis Kelamin</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_jk" v-model="search_input_jk">
                  <option value="">Please Select</option>
                  @foreach ($data=['L'=>'Laki-laki','P'=>'Perempuan'] as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Status Perkawinan</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_perkawinan" v-model="search_input_perkawinan">
                  <option value="">Please Select</option>
                  @foreach ($data=['1' => 'Kawin','2' => 'Belum Kawin', '3'=>'Janda', '4'=>'Duda' ] as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Golongan Darah</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_goldar" v-model="search_input_goldar">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\GolonganDarah::lists('nm_goldar','id_goldar') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Kelompok Usia</label>
          <div class="col-lg-9">
              <select  name="psearch" id="m_usia" v-model="search_input_usia">
                  <option value="">Please Select</option>
                  @foreach ($data=['1' => '16-25','2' => '26-35', '3'=>'36-45', '4'=>'46-55', '5'=>'56-65', '6'=>'>65' ] as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Alamat</label>
          <div class="col-lg-9">
              <input class="form-control" id="m_alamat" name="psearch" placeholder="Search" type="text" value="" v-model="search_input_alamat" v-on="keyup:doSearch | key 'enter'">              
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Diklat Struktural</label>
          <div class="col-lg-9">
            <select class="psearch" id="m_diklat_struktural" v-model="search_input_diklat_struktural">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\DiklatStruktural::lists('kategori_nama','kategori_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
            </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Diklat Fungsional</label>
          <div class="col-lg-9">
            <select class="psearch" id="m_diklat_fungsional" v-model="search_input_diklat_fungsional">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\DiklatFungsional::lists('diklat_fungsional_nm','diklat_fungsional_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
            </select> 
          </div>
        </div>
        <div class="form-group row">
          <label for="cname" class="control-label col-lg-3">Diklat Teknis</label>
          <div class="col-lg-9">
            <select class="psearch" id="m_diklat_teknis" v-model="search_input_diklat_teknis">
                  <option value="">Please Select</option>
                  @foreach ($data=App\Model\DiklatTeknis::lists('diklat_teknis_nm','diklat_teknis_id') as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                  @endforeach
            </select> 
          </div>
        </div>
        </section><div>&nbsp;</div>
          <div class="btn-group ewButtonGroup">
            <button class="btn btn-primary btn-sm" id="btnsubmit" name="btnsubmit" v-on="click: doSearch"><i class="fa fa-search"></i> Search</button>
            <a class="btn btn-info btn-sm" v-on="click: showAdvancedSearch = !showAdvancedSearch" v-text="showAdvancedSearch ? 'Hide Advanced Search' : 'Advanced Search'">Advanced Search</a>
            <a class="btn btn-default btn-sm" v-on="click: clearFilter">Show all</a>
            <a class="btn btn-success btn-sm" v-on="click: exportData">Export Excel</a>
          </div>
    </div>
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
  @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
   <div class="col-lg-12">
  <div class="loading panel" v-show="isLoading">
    <h3>Sedang memuat data...</h3>
  </div>
  </div>
  <section v-show="!isLoading" style="display: none; width: 100%;">
  <div class="table-responsive">
     <table cellspacing="0" class="table table-striped table-advanced">
      <tbody>
        <tr>
          <td class="ewGridContent">
            <div class="ewGridMiddlePanel" id="gmp_pegawai">
              <table class="table table-bordered table-striped table-condensed" id="tbl_pegawailist">
                <thead>
                  <!-- Table header -->
                  <tr class="ewTableHeader">
                    @foreach ($columns as $column_id => $column)
                    <th>
                      <div class="ewPointer" v-on="click: sort('{{$column_id}}')">
                        <div class="pegawai_{{$column_id}}" id="elh_pegawai_{{$column_id}}">
                          <div class="ewTableHeaderBtn">
                            <span class="ewTableHeaderCaption">{{$column[0]}}</span>
                            <span class="ewTableHeaderSort">
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'desc'" class="caret"></span>
                              <span v-show="sortColumn == '{{$column_id}}' && sortDir == 'asc' " class="caret ewSortUp"></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </th>
                    @endforeach
                    <th class="ewListOptionHeader" data-name="view" style="white-space: nowrap;"><span>&nbsp;</span></th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="@{{$index % 2 ? 'ewTableAltRow' : 'ewTableRow'}}" data-rowindex="@{{$index + 1}}" data-rowtype="1" v-repeat="items">
                    @foreach ($columns as $column)
                    <td>
                      <span v-text="{{$column[1]}}"></span>
                    </td>
                    @endforeach
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{url('pegawai/profile/edit')}}/@{{peg_id}}" data-toggle="tooltip" title="Lihat Detail" target="_blank">Lihat Detail
                        </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="ewGridLowerPanel">
              <form class="form-inline">
                <div class="form-group">
                  Page &nbsp;
                  <!--first page button-->
                  <a class="btn btn-default btn-xs" v-on="click: paginate('first')"><i class="glyphicon glyphicon-step-backward"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('previous')"><i class="glyphicon glyphicon-chevron-left"></i></a>
                  <input class="form-control input-sm" type="text" v-model="pagination.page" v-on="change: changePage, keyup: changePage | key 'enter'">
                  <a class="btn btn-default btn-xs" v-on="click: paginate('next')"><i class="glyphicon glyphicon-chevron-right"></i></a>
                  <a class="btn btn-default btn-xs" v-on="click: paginate('last')"><i class="glyphicon glyphicon-step-forward"></i></a>
                  &nbsp;of&nbsp;@{{ Math.floor(pagination.count / pagination.perpage) + 1 }}
                  &nbsp;&nbsp;&nbsp;&nbsp; Records&nbsp;@{{ (pagination.page - 1) * pagination.perpage + 1 }}&nbsp;to&nbsp;@{{ Math.min(pagination.count, pagination.page  * pagination.perpage) }}&nbsp;of&nbsp;@{{ pagination.count }}</td>
                </div>
              </form>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    </div>
  </section>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$('#m_satker').select2({ width: '100%' }).on('change', function (e){
    var v = $('#m_satker').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('pegawai/getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v},
        dataType: 'json',
        success: function(data) {
            $("#m_uker").empty();
            $("#m_uker").select2("val", ""); 
            $('#m_uker').append($('<option></option>').attr('value', '').text('-Pilih-'));
            for (row in data) {
                $('#m_uker').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
        }
    }).error(function (e){
        console.log(e);
        alert("ERROR!");
    });
});
// $('#m_golongan').select2({ width: '100%' });
// $('#m_eselon').select2({ width: '100%' });
$('#m_uker').select2({ width: '100%' });
$('#m_agama').select2({ width: '100%' });
$('#m_tingpend').select2({ width: '100%' });
$('#m_perkawinan').select2({ width: '100%' });
$('#m_kelas_jabatan').select2({ width: '100%' });
$('#m_goldar').select2({ width: '100%' });
$('#m_jurusan').select2({ width: '100%' });
$('#m_universitas').select2({ width: '100%' });
$('#m_usia').select2({ width: '100%' });
$('#m_jk').select2({ width: '100%' });
$('#m_diklat_struktural').select2({ width: '100%' });
$('#m_diklat_fungsional').select2({ width: '100%' });
$('#m_diklat_teknis').select2({ width: '100%' });
$('#status_pegawai').select2({ width: '100%' });
var page = 'pns';
var vm = new Vue({
  el: '#vm',
  data: {
    isLoading: true,
    showAdvancedSearch: false,
    items: [],
    pagination: {
      page: 1,
      previous: false,
      next: false,
      perpage: 20,
      count: 0,
    },
    params: {
    },
    search: '',
    search_input: '',
    search_nama: '',
    search_input_nama: '',
    search_nip: '',
    search_input_nip: '',
    search_jabatan: '',
    search_input_jabatan: '',
    search_satker: '',
    search_input_satker: '',
    search_uker: '',
    search_input_uker: '',
    search_golongan: '',
    search_input_golongan: '',
    search_eselon: [],
    search_kelas_jabatan: '',
    search_input_kelas_jabatan: '',
    search_input_eselon: [],
    search_agama: '',
    search_input_agama: '',
    search_status_pegawai: '',
    search_input_status_pegawai: '',
    search_tingpend: '',
    search_input_tingpend: '',
    search_jurusan: '',
    search_input_jurusan: '',
    search_jk: '',
    search_input_jk: '',
    search_perkawinan: '',
    search_input_perkawinan: '',
    search_goldar: '',
    search_input_goldar: '',
    search_usia: '',
    search_input_usia: '',
    search_alamat: '',
    search_input_alamat: '',
    search_hp: '',
    search_input_hp: '',
    search_instansi: '',
    search_input_instansi: '',
    search_riwayat: '',
    search_input_riwayat: '',
    search_jenjab: '',
    search_input_jenjab: '',
    search_universitas: '',
    search_input_universitas: '',
    search_diklat_struktural: '',
    search_input_diklat_struktural: '',
    search_diklat_fungsional: '',
    search_input_diklat_fungsional: '',
    search_diklat_teknis: '',
    search_input_diklat_teknis: '',
    sortColumn: null,
    sortDir: null,
  },
  methods: {
    paginate: function (direction) {
      if (direction === 'previous') {
        if (this.pagination.page > 1) {
          --this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'next') {
        if (Math.floor(this.pagination.count / this.pagination.perpage) != (this.pagination.page - 1)) {
          ++this.pagination.page;
          this.changePage();
        }
      } else if (direction === 'first') {
        this.pagination.page = 1;
        this.changePage();
      } else if (direction === 'last') {
        this.pagination.page = Math.floor(this.pagination.count / this.pagination.perpage) + 1;
        this.changePage();
      }
    },
    changePage: function (page) {
      if (isNaN(parseInt(this.pagination.page))) {
        this.pagination.page = 1;
      }
      getData(this.pagination.page);
    },
    clearFilter: function () {
      for (var key in this.params) {
        if (this.params.hasOwnProperty(key)) {
          this.params[key] = '';
        }
      }
      this.search = '';
      this.search_nama = '';
      this.search_nip = '';
      this.search_jabatan = '';
      this.search_satker = '';
      this.search_uker = '';
      this.search_golongan = [];
      this.search_eselon = [];
      this.search_kelas_jabatan = '';
      this.search_agama = '';
      this.search_status_pegawai = '';
      this.search_tingpend = '';
      this.search_jurusan = '';
      this.search_jk = '';
      this.search_perkawinan = '';
      this.search_goldar = '';
      this.search_usia = '';
      this.search_alamat = '';
      this.search_hp = '';
      this.search_riwayat = '';
      this.search_jenjab = '';
      this.search_universitas = '';
      this.search_diklat_struktural = '';
      this.search_diklat_fungsional = '';
      this.search_diklat_teknis = '';
      this.search_input = '';
      this.search_input_nama = '';
      this.search_input_nip = '';
      this.search_input_jabatan = '';
      this.search_input_satker = '';
      this.search_input_uker = '';
      this.search_input_golongan = [];
      this.search_input_eselon = [];
      this.search_input_kelas_jabatan = '';
      this.search_input_agama = '';
      this.search_input_status_pegawai = '';
      this.search_input_tingpend = '';
      this.search_input_jurusan = '';
      this.search_input_jk = '';
      this.search_input_perkawinan = '';
      this.search_input_goldar = '';
      this.search_input_usia = '';
      this.search_input_alamat = '';
      this.search_input_hp = '';
      this.search_input_instansi = '';
      this.search_input_riwayat = '';
      this.search_input_jenjab = '';
      this.search_input_universitas = '';
      this.search_input_diklat_struktural = '';
      this.search_input_diklat_fungsional = '';
      this.search_input_diklat_teknis = '';
      this.sortColumn = null;
      this.sortDir = null;
      $('.date-picker').val('');
      $('#m_satker').select2('val','');
      $('#m_eselon').select2('val','');
      $('#m_golongan').select2('val','');
      $('#m_agama').select2('val','');
      $('#m_tingpend').select2('val','');
      $('#m_jk').select2('val','');
      $('#m_kelas_jabatan').select2('val','');
      $('#m_perkawinan').select2('val','');
      $('#m_goldar').select2('val','');
      $('#m_usia').select2('val','');
      $('#m_uker').select2('val','');
      $('#m_diklat_struktural').select2('val','');
      $('#m_diklat_fungsional').select2('val','');
      $('#m_diklat_teknis').select2('val','');
      $('#status_pegawai').select2('val','');
      this.pagination.page = 1;
      this.changePage();
    },
    sort: function (col) {
      if (this.sortColumn == col) {
        this.sortDir = (this.sortDir == 'asc') ? 'desc' : 'asc';
      } else {
        this.sortColumn = col;
        this.sortDir = 'asc';
      }
      this.pagination.page = 1;
      this.changePage();
    },
    doSearch: function () {
      var eselon = [];
      $.each($("input[name='m_eselon']:checked"), function(){            
          eselon.push($(this).val());
      });
      var golongan = [];
      $.each($("input[name='m_golongan']:checked"), function(){            
          golongan.push($(this).val());
      });
      this.search = this.search_input;
      this.search_nama = this.search_input_nama;
      this.search_nip = this.search_input_nip;
      this.search_jabatan = this.search_input_jabatan;
      this.search_satker = this.search_input_satker ? this.search_input_satker: $('#m_satker').val();
      this.search_uker = this.search_input_uker ? this.search_input_uker : $('#m_uker').val();
      this.search_golongan = golongan;
      this.search_eselon = eselon;
      this.search_kelas_jabatan = this.search_input_kelas_jabatan ? this.search_input_kelas_jabatan : $('#m_kelas_jabatan').val();
      this.search_agama = this.search_input_agama ? this.search_input_agama: $('#m_agama').val();
      this.search_status_pegawai = this.search_input_status_pegawai ? this.search_input_status_pegawai: $('#status_pegawai').val();
      this.search_tingpend = this.search_input_tingpend ? this.search_input_tingpend: $('#m_tingpend').val();
      this.search_jurusan = this.search_input_jurusan ? this.search_input_jurusan: $('#m_jurusan').val();
      this.search_universitas = this.search_input_universitas ? this.search_input_universitas: $('#m_universitas').val();
      this.search_jk = this.search_input_jk ? this.search_input_jk: $('#m_jk').val();
      this.search_perkawinan = this.search_input_perkawinan ? this.search_input_perkawinan: $('#m_perkawinan').val();
      this.search_goldar = this.search_input_goldar ? this.search_input_goldar: $('#m_goldar').val();
      this.search_usia = this.search_input_usia ? this.search_input_usia: $('#m_usia').val();
      this.search_diklat_struktural = this.search_input_diklat_struktural ? this.search_input_diklat_struktural: $('#m_diklat_struktural').val();
      this.search_diklat_fungsional = this.search_input_diklat_fungsional ? this.search_input_diklat_fungsional: $('#m_diklat_fungsional').val();
      this.search_diklat_teknis = this.search_input_diklat_teknis ? this.search_input_diklat_teknis: $('#m_diklat_teknis').val();
      this.search_alamat = this.search_input_alamat;
      this.search_hp = this.search_input_hp;
      this.search_instansi = this.search_input_instansi;
      this.search_riwayat = this.search_input_riwayat;
      this.search_jenjab = this.search_input_jenjab;
      this.pagination.page = 1;
      this.changePage();
    },
    exportData: function () {
      var eselon = [];
      $.each($("input[name='m_eselon']:checked"), function(){            
          eselon.push($(this).val());
      });
      var golongan = [];
      $.each($("input[name='m_golongan']:checked"), function(){            
          golongan.push($(this).val());
      });
      this.search = this.search_input;
      this.search_nama = this.search_input_nama;
      this.search_nip = this.search_input_nip;
      this.search_jabatan = this.search_input_jabatan;
      this.search_satker = this.search_input_satker ? this.search_input_satker: $('#m_satker').val();
      this.search_uker = this.search_input_uker ? this.search_input_uker : $('#m_uker').val();
      this.search_golongan = golongan;
      this.search_eselon = eselon;
      this.search_kelas_jabatan = this.search_input_kelas_jabatan ? this.search_input_kelas_jabatan : $('#m_kelas_jabatan').val();
      this.search_agama = this.search_input_agama ? this.search_input_agama: $('#m_agama').val();
      this.search_status_pegawai = this.search_input_status_pegawai ? this.search_input_status_pegawai: $('#status_pegawai').val();
      this.search_tingpend = this.search_input_tingpend ? this.search_input_tingpend: $('#m_tingpend').val();
      this.search_jurusan = this.search_input_jurusan ? this.search_input_jurusan: $('#m_jurusan').val();
      this.search_universitas = this.search_input_universitas ? this.search_input_universitas: $('#m_universitas').val();
      this.search_jk = this.search_input_jk ? this.search_input_jk: $('#m_jk').val();
      this.search_perkawinan = this.search_input_perkawinan ? this.search_input_perkawinan: $('#m_perkawinan').val();
      this.search_goldar = this.search_input_goldar ? this.search_input_goldar: $('#m_goldar').val();
      this.search_usia = this.search_input_usia ? this.search_input_usia: $('#m_usia').val();
      this.search_diklat_struktural = this.search_input_diklat_struktural ? this.search_input_diklat_struktural: $('#m_diklat_struktural').val();
      this.search_diklat_fungsional = this.search_input_diklat_fungsional ? this.search_input_diklat_fungsional: $('#m_diklat_fungsional').val();
      this.search_diklat_teknis = this.search_input_diklat_teknis ? this.search_input_diklat_teknis: $('#m_diklat_teknis').val();
      this.search_alamat = this.search_input_alamat;
      this.search_hp = this.search_input_hp;
      this.search_instansi = this.search_input_instansi;
      this.search_riwayat = this.search_input_riwayat;
      this.search_jenjab = this.search_input_jenjab;
      this.pagination.page = 1;
      exportExcel();
    }
  }
});
loadState();
vm.changePage();
function getData(page) {
  $.getJSON("{{url('search/get-data')}}?page="+page+'&perpage='+vm.pagination.perpage,
  {
    page: page,
    perpage: vm.pagination.perpage,
    params: vm.params,
    search: vm.search,
    search_nama: vm.search_nama,
    search_nip: vm.search_nip,
    search_jabatan: vm.search_jabatan,
    search_satker: vm.search_satker,
    search_uker: vm.search_uker,
    search_golongan: vm.search_golongan,
    search_kelas_jabatan: vm.search_kelas_jabatan,
    search_eselon: vm.search_eselon,
    search_agama: vm.search_agama,
    search_status_pegawai: vm.search_status_pegawai,
    search_tingpend: vm.search_tingpend,
    search_jurusan: vm.search_jurusan,
    search_universitas: vm.search_universitas,
    search_jk: vm.search_jk,
    search_perkawinan: vm.search_perkawinan,
    search_goldar: vm.search_goldar,
    search_usia: vm.search_usia,
    search_alamat: vm.search_alamat,
    search_hp: vm.search_hp,
    search_riwayat: vm.search_riwayat,
    search_jenjab: vm.search_jenjab,
    search_diklat_struktural: vm.search_diklat_struktural,
    search_diklat_fungsional: vm.search_diklat_fungsional,
    search_diklat_teknis: vm.search_diklat_teknis,
    order: vm.sortColumn,
    order_direction: vm.sortDir,
  },
  function(data) {
    vm.items = data.data;
    vm.isLoading = false;
    vm.pagination.count = data.count;
    if(data.count==0) {
      alert('Data Tidak Ditemukan!')
    }
    saveState();
  });
}

function exportExcel(){
  var url = "{{url('search/export')}}";
  var params = $.param({
    search : vm.search,
    search_nama : vm.search_nama,
    search_input_nama : vm.search_input_nama,
    search_nip : vm.search_nip,
    search_input_nip : vm.search_input_nip,
    search_jabatan : vm.search_jabatan,
    search_input_jabatan : vm.search_input_jabatan,
    search_satker : vm.search_satker,
    search_input_satker : vm.search_input_satker,
    search_uker : vm.search_uker,
    search_input_uker : vm.search_input_uker,
    search_golongan : vm.search_golongan,
    search_input_golongan : vm.search_input_golongan,
    search_eselon : vm.search_eselon,
    search_input_eselon : vm.search_input_eselon,
    search_agama : vm.search_agama,
    search_input_agama : vm.search_input_agama,
    search_status_pegawai : vm.search_status_pegawai,
    search_input_status_pegawai : vm.search_input_status_pegawai,
    search_tingpend : vm.search_tingpend,
    search_input_tingpend : vm.search_input_tingpend,
    search_jurusan : vm.search_jurusan,
    search_input_jurusan : vm.search_input_jurusan,
    search_universitas : vm.search_universitas,
    search_input_universitas : vm.search_input_universitas,
    search_jk : vm.search_jk,
    search_input_jk : vm.search_input_jk,
    search_perkawinan : vm.search_perkawinan,
    search_input_perkawinan : vm.search_input_perkawinan,
    search_goldar : vm.search_goldar,
    search_input_goldar : vm.search_input_goldar,
    search_usia : vm.search_usia,
    search_input_usia : vm.search_input_usia,
    search_alamat : vm.search_alamat,
    search_input_alamat : vm.search_input_alamat,
    search_hp : vm.search_hp,
    search_input_hp : vm.search_input_hp,
    search_instansi : vm.search_instansi,
    search_input_instansi : vm.search_input_instansi,
    search_riwayat : vm.search_riwayat,
    search_input_riwayat : vm.search_input_riwayat,
    search_kelas_jabatan: vm.search_kelas_jabatan,
    search_input_kelas_jabatan: vm.search_input_kelas_jabatan,
    search_jenjab : vm.search_jenjab,
    search_input_jenjab : vm.search_input_jenjab,
    search_input_diklat_teknis : vm.search_input_diklat_teknis,
    search_input_diklat_fungsional : vm.search_input_diklat_fungsional,
    search_input_diklat_struktural : vm.search_input_diklat_struktural,
  });
        
  window.location.href = url+'?'+params;
}
function saveState() {
  var stored = {
    pagination : JSON.stringify(vm.pagination),
    params : JSON.stringify(vm.params),
    search : vm.search,
    search_nama : vm.search_nama,
    search_input_nama : vm.search_input_nama,
    search_nip : vm.search_nip,
    search_input_nip : vm.search_input_nip,
    search_jabatan : vm.search_jabatan,
    search_input_jabatan : vm.search_input_jabatan,
    search_satker : vm.search_satker,
    search_input_satker : vm.search_input_satker,
    search_uker : vm.search_uker,
    search_input_uker : vm.search_input_uker,
    search_golongan : vm.search_golongan,
    search_input_golongan : vm.search_input_golongan,
    search_eselon : vm.search_eselon,
    search_input_eselon : vm.search_input_eselon,
    search_agama : vm.search_agama,
    search_input_agama : vm.search_input_agama,
    search_status_pegawai : vm.search_status_pegawai,
    search_input_status_pegawai : vm.search_input_status_pegawai,
    search_tingpend : vm.search_tingpend,
    search_input_tingpend : vm.search_input_tingpend,
    search_jurusan : vm.search_jurusan,
    search_input_jurusan : vm.search_input_jurusan,
    search_universitas : vm.search_universitas,
    search_input_universitas : vm.search_input_universitas,
    search_jk : vm.search_jk,
    search_input_jk : vm.search_input_jk,
    search_perkawinan : vm.search_perkawinan,
    search_input_perkawinan : vm.search_input_perkawinan,
    search_goldar : vm.search_goldar,
    search_input_goldar : vm.search_input_goldar,
    search_usia : vm.search_usia,
    search_input_usia : vm.search_input_usia,
    search_alamat : vm.search_alamat,
    search_input_alamat : vm.search_input_alamat,
    search_hp : vm.search_hp,
    search_input_hp : vm.search_input_hp,
    search_instansi : vm.search_instansi,
    search_input_instansi : vm.search_input_instansi,
    search_riwayat : vm.search_riwayat,
    search_input_riwayat : vm.search_input_riwayat,
    search_kelas_jabatan: vm.search_kelas_jabatan,
    search_input_kelas_jabatan: vm.search_input_kelas_jabatan,
    search_diklat_struktural: vm.search_diklat_struktural,
    search_input_diklat_struktural: vm.search_input_diklat_struktural,
    search_diklat_fungsional: vm.search_diklat_fungsional,
    search_input_diklat_fungsional: vm.search_input_diklat_fungsional,
    search_diklat_teknis: vm.search_diklat_teknis,
    search_input_diklat_teknis: vm.search_input_diklat_teknis,
    search_jenjab: vm.search_jenjab,
    search_input_jenjab: vm.search_input_jenjab,
    sortColumn : vm.sortColumn,
    sortDir : vm.sortDir,
  };
  localStorage.setItem(page + '_state',JSON.stringify(stored));
}
function loadState() {
  var stored = JSON.parse(localStorage.getItem(page + '_state'));
  if (stored) {
    vm.pagination = JSON.parse(stored.pagination);
    vm.params = JSON.parse(stored.params);
    vm.search = stored.search;
    vm.search_nama = stored.search_nama;
    vm.search_input_nama = stored.search_input_nama;
    vm.search_nip = stored.search_nip;
    vm.search_input_nip = stored.search_input_nip;
    vm.search_jabatan = stored.search_jabatan;
    vm.search_input_jabatan = stored.search_input_jabatan;
    vm.search_satker = stored.search_satker;
    vm.search_input_satker = stored.search_input_satker;
    vm.search_uker = stored.search_uker;
    vm.search_input_uker = stored.search_input_uker;
    vm.search_golongan = stored.search_golongan;
    vm.search_input_golongan = stored.search_input_golongan;
    vm.search_eselon = stored.search_eselon;
    vm.search_input_eselon = stored.search_input_eselon;
    vm.search_agama = stored.search_agama;
    vm.search_input_agama = stored.search_input_agama;
    vm.search_status_pegawai = stored.search_status_pegawai;
    vm.search_input_status_pegawai = stored.search_input_status_pegawai;
    vm.search_tingpend = stored.search_tingpend;
    vm.search_input_tingpend = stored.search_input_tingpend;
    vm.search_jurusan = stored.search_jurusan;
    vm.search_input_jurusan = stored.search_input_jurusan;
    vm.search_universitas = stored.search_universitas;
    vm.search_input_universitas = stored.search_input_universitas;
    vm.search_jk = stored.search_jk;
    vm.search_input_jk = stored.search_input_jk;
    vm.search_perkawinan= stored.search_perkawinan;
    vm.search_input_perkawinan = stored.search_input_perkawinan;
    vm.search_goldar = stored.search_goldar;
    vm.search_input_goldar = stored.search_input_goldar;
    vm.search_usia = stored.search_usia;
    vm.search_input_usia = stored.search_input_usia;
    vm.search_alamat = stored.search_alamat;
    vm.search_input_alamat = stored.search_input_alamat;
    vm.search_hp = stored.search_hp;
    vm.search_input_hp = stored.search_input_hp;
    vm.search_instansi = stored.search_instansi;
    vm.search_input_instansi = stored.search_input_instansi;
    vm.search_riwayat = stored.search_riwayat;
    vm.search_input_riwayat = stored.search_input_riwayat;
    vm.search_kelas_jabatan = stored.search_kelas_jabatan;
    vm.search_input_kelas_jabatan = stored.search_input_kelas_jabatan;
    vm.search_jenjab = stored.search_jenjab;
    vm.search_input_jenjab = stored.search_input_jenjab;
    vm.search_diklat_struktural = stored.search_diklat_struktural;
    vm.search_input_diklat_struktural = stored.search_input_diklat_struktural;
    vm.search_diklat_fungsional = stored.search_diklat_fungsional;
    vm.search_input_diklat_fungsional = stored.search_input_diklat_fungsional;
    vm.search_diklat_teknis = stored.search_diklat_teknis;
    vm.search_input_diklat_teknis = stored.search_input_diklat_teknis;
    vm.sortColumn = stored.sortColumn;
    vm.sortDir = stored.sortDir;
  }
}
</script>
@endsection
