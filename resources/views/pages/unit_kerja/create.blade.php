@extends('layouts.app')

@section('content')

 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
	<h3>Tambah Unit Kerja</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('data/uker/store') }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                <table class="table no-footer">
                  <tr>
                    <td>Organisasi</td>
                    <td>:</td>
                    <td>{{$satker->satuan_kerja_nama}}</td>
                  </tr>
                	<tr>
                    @include('form.txt',['label'=>'Unit Kerja Nama','required'=>false,'name'=>'unit_kerja_nama','vue'=>''])
                  </tr>
                   @include('form.hidden',['name'=>'satuan_kerja_id','value'=>Request::get('satuan_kerja_id')])
                   @include('form.hidden',['name'=>'unit_kerja_level','value'=>Request::get('level')])
                  <?php $unit_kerja_parent = Request::get('unit_kerja_parent') == 'null' ? null : Request::get('unit_kerja_parent',null); ?>
                    @include('form.hidden',['name'=>'unit_kerja_parent','value'=>$unit_kerja_parent])
                  <tr>
                    @include('form.select2',['label'=>'Posisi Unit Kerja Setelah','required'=>false,'name'=>'unit_kerja_after','vue'=>'', 'data'=>App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_level', Request::get('level'))->where('unit_kerja_parent', $unit_kerja_parent)->orderBy('unit_kerja_left')->lists('unit_kerja_nama','unit_kerja_id'),'empty'=>'Taruh di urutan pertama'])
                  </tr>
                  <tr>
                   @include('form.radio2',['label'=>'Status','required'=>false,'name'=>'status','data'=>['1' => 'Aktif','0' => 'Tidak Aktif' ], 'value'=>'1'])
                  </tr>
                </table>
                
                 <button class="btn btn-primary btn-xs" type="submit">Tambah</button>
             
                </div>
            </form>
        </div>
</div>
    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  </script>
@endsection