@extends('layouts.app')

@section('content')
<?php
  $uk_options = [];
  $uk_parent = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->whereNull('unit_kerja_parent')->orderBy('unit_kerja_left')->get();
  foreach($uk_parent as $up) {
    $uk_options[$up->unit_kerja_id] = $up->unit_kerja_nama;
    $uk_child = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_parent', $up->unit_kerja_id)->orderBy('unit_kerja_left')->get();
    foreach($uk_child as $uc) {
      $uk_options[$uc->unit_kerja_id] = '--- '.$uc->unit_kerja_nama;
      $uk_child2 = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('unit_kerja_parent', $uc->unit_kerja_id)->orderBy('unit_kerja_left')->get();
      foreach($uk_child2 as $uc2) {
        $uk_options[$uc2->unit_kerja_id] = '------ '.$uc2->unit_kerja_nama;
      }
    }
  }
?>
 @if (count($errors) > 0)
   <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>Whoops!</strong> Ada Kesalahan!<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
@endif
  @if (Session::has('message'))
  <div class="col-lg-12">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
        <strong>{{ session('message') }}</strong>
    </div>
  </div>
  @endif
<div class="col-lg-12">
  <h3>Edit Unit Kerja</h3>
<section class="panel">
    <div class="panel-body">
        <div class=" form">
            <form class="cmxform form-horizontal tasi-form" action="{{ url('data/uker/update/'.$model->unit_kerja_id) }}" id="fpegawaiadd" method="post" name="fpegawaiadd">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="table-responsive">
                <table class="table no-footer">
                  <tr>
                    <td>Organisasi</td>
                    <td>:</td>
                    <td>{{$satker->satuan_kerja_nama}}</td>
                  </tr>
                	<tr>
                    @include('form.txt',['label'=>'Unit Kerja Nama','required'=>false,'name'=>'unit_kerja_nama','vue'=>'','value'=>$model->unit_kerja_nama])
                  </tr>
                  <?php
                    $data = App\Model\UnitKerja::where('satuan_kerja_id', $model->satuan_kerja_id)->where('unit_kerja_level', $model->unit_kerja_level)->where('unit_kerja_parent', $model->unit_kerja_parent)->orderBy('unit_kerja_left')->lists('unit_kerja_nama','unit_kerja_id');
                  ?>
                  <tr>
                    <td>Posisi Unit Kerja Setelah </td>
                    <td>:</td>
                    <td>
                        <select id="unit_kerja_after" name="unit_kerja_after" style="max-width:500px;">
                            <option value="">Tidak Diubah</option>
                            <option value="0">Taruh di urutan pertama</option>
                            @foreach ($data as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                  </tr>
                  <!-- <tr>

                    @include('form.select2',['label'=>'Posisi Unit Kerja Setelah','required'=>false,'name'=>'unit_kerja_after','vue'=>'', 'data'=>$data,'empty'=>'Tidak Diubah'])
                  </tr> -->
                  <tr>
                    @include('form.select2',['label'=>'Unit Kerja Atas','required'=>false,'name'=>'unit_kerja_parent','vue'=>'', 'data'=>$uk_options,'empty'=>App\Model\SatuanKerja::find($model->satuan_kerja_id)->satuan_kerja_nama,'value'=>$model->unit_kerja_parent])
                  </tr>
                  <tr>
                   @include('form.radio2',['label'=>'Status','required'=>false,'name'=>'status','data'=>['1' => 'Aktif','0' => 'Tidak Aktif' ], 'value'=>$model->status])
                  </tr>
                </table>
                
                 <button class="btn btn-primary btn-xs" type="submit">Simpan</button>
             
                </div>
            </form>
        </div>

    </div>
</section>
</div>
@endsection
@section('scripts')
  <script type="text/javascript">
  </script>
@endsection