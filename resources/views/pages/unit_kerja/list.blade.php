@extends('layouts.app')

@section('content')
<h2 align="center">
  Daftar Unit Kerja @if($satker)<br>{{ $satker->satuan_kerja_nama }} @endif
</h2>
<center>
  @include('form.select2',['label'=>'Satuan Kerja','required'=>false,'name'=>'satuan_kerja_id','data'=>
            App\Model\SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->where('status',1)->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id'),'empty'=>''])
</center>
  @if ($satker)
<?php
  $uk_parent = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('is_deleted', false)->where('status',1)->whereNull('unit_kerja_parent')->orderBy('unit_kerja_left')->get();
  $uker= App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('is_deleted', false)->where('status',1)->select('unit_kerja_id')->orderBy('unit_kerja_left')->get();
  $unit = array();
  foreach ($uker as $key => $value) {
    $unit[] = $value->unit_kerja_id; 
  }
  $peg = App\Model\Pegawai::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count();
?>
<div class="table-responsive">
    <table id="list-unit" class="table table-striped table-bordered table-hover">
       <thead>
       <tr class="bg-info">
         <th>SOPD</th>
           <th>Unit Kerja</th>
           <th>Jumlah Pegawai</th>
           <th>Pilihan</th>
       </tr>
       </thead>
       <tbody>
        <tr>
          <td>{{ $satker->satuan_kerja_nama }}</td>
          <td></td>
          <td>{{ $peg }}</td>
          <td>
            <a href="{{url('data/uker/create')}}?satuan_kerja_id={{$satker->satuan_kerja_id}}&unit_kerja_parent=null&level=1" class="btn btn-info btn-xs">
              Tambah Unit Kerja Bawahan
            </a>
          </td>
        </tr> 
      @foreach($uk_parent as $up)
        <?php 
        $peg2 = App\Model\Pegawai::where('unit_kerja_id', $up->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count();
        $uk_child = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('is_deleted', false)->where('status',1)->where('unit_kerja_parent', $up->unit_kerja_id)->orderBy('unit_kerja_left')->get(); ?>
        <tr>
          <td></td>
          <td>{{$up->unit_kerja_nama}}</td>
          <td>{{ $peg2 }}</td>
          <td>
            <a href="{{url('data/uker/edit')}}/{{$up->unit_kerja_id}}" class="btn btn-success btn-xs">
              Edit
            </a>
            <a href="{{url('data/uker/create')}}?satuan_kerja_id={{$satker->satuan_kerja_id}}&unit_kerja_parent={{$up->unit_kerja_id}}&level=2" class="btn btn-info btn-xs">
              Tambah Unit Kerja Bawahan
            </a>
            <a href="{{url('data/uker/destroy')}}/{{$up->unit_kerja_id}}" class="btn btn-danger btn-xs pull-right" onclick="notifyConfirm(event)">
              Hapus
            </a>
          </td>
        </tr>
          @foreach($uk_child as $uc)
            <?php
              $peg3 = App\Model\Pegawai::where('unit_kerja_id', $uc->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count(); 
              $uk_child2 = App\Model\UnitKerja::where('satuan_kerja_id', $satker->satuan_kerja_id)->where('is_deleted', false)->where('status',1)->where('unit_kerja_parent', $uc->unit_kerja_id)->orderBy('unit_kerja_left')->get(); ?>
            <tr>
              <td></td>
              <td style="padding-left:50px">{{$uc->unit_kerja_nama}}</td>
              <td>{{ $peg3 }}</td>
              <td>
                <a href="{{url('data/uker/edit')}}/{{$uc->unit_kerja_id}}" class="btn btn-success btn-xs">
                  Edit
                </a>
                <a href="{{url('data/uker/create')}}?satuan_kerja_id={{$satker->satuan_kerja_id}}&unit_kerja_parent={{$uc->unit_kerja_id}}&level=3" class="btn btn-info btn-xs">
                  Tambah Unit Kerja Bawahan
                </a>
                <a href="{{url('data/uker/destroy')}}/{{$uc->unit_kerja_id}}" class="btn btn-danger btn-xs pull-right" onclick="notifyConfirm(event)">
                  Hapus
                </a>
              </td>
            </tr>
            @foreach($uk_child2 as $uc2)
              <?php
                $peg4 = App\Model\Pegawai::where('unit_kerja_id', $uc2->unit_kerja_id)->where('satuan_kerja_id', $satker->satuan_kerja_id)->where('peg_status', 'TRUE')->count(); 
              ?>
              <tr>
                <td></td>
                <td style="padding-left:100px">{{$uc2->unit_kerja_nama}}</td>
                <td>{{$peg4}}</td>
                <td>
                  <a href="{{url('data/uker/edit')}}/{{$uc2->unit_kerja_id}}" class="btn btn-success btn-xs">
                    Edit
                  </a>
                  <a href="{{url('data/uker/destroy')}}/{{$uc2->unit_kerja_id}}" class="btn btn-danger btn-xs pull-right" onclick="notifyConfirm(event)">
                    Hapus
                  </a>
                </td>
              </tr>
            @endforeach
          @endforeach
        @endforeach
       </tbody>
   </table>
</div>
  @endif
<br>
<br>

@endsection
@section('scripts')
<script type="text/javascript">
  $('#satuan_kerja_id').select2({ width: '500px' }).change(function() {
      var val = $("#satuan_kerja_id option:selected").val();
      var url = "{{url('data/uker')}}?satuan_kerja_id="+val;
      window.location.href= url;
  });
function notifyConfirm(e) {
  if (!confirm("Apakah anda yakin?")) {
    e.preventDefault();
    prevent = true;
    return false;
  }
  if (!confirm("Apakah anda yakin untuk menghapus Unit Kerja Ini?")) {
    e.preventDefault();
    prevent = true;
    return false;
  }
  return true;
}
</script>
@endsection
