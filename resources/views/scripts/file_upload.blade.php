<script type="text/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url ="{{ URL::to('upload-file/') }}";
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(jpg|gif|jpe?g|png|docx|doc|xls|pdf|xlsx)$/i,
        maxFileSize: 999000,
        done: function (e, data) {
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        success: function(data) {
            $('#files').append('<p/>').text(data.data);
            $('.filename').val(data.data);
        } 
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>