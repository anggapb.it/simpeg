<script type="text/javascript">
var role = "{{Auth::user()->role_id}}";

if(role==1 || role ==3){
    $('#satuan_kerja_id').select2({ width: '400px' }).on('change', function (e){
        var v = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : '{{Auth::user()->satuan_kerja_id}}';
        $.ajax({
            type: "GET",
            cache: false,
            url: "{{ URL::to('getSatuanOrganisasi') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"satuan_kerja_id":v},
            dataType: 'json',
            success: function(data) {
                $("#satuan_organisasi_id").empty();
                $("#satuan_organisasi_id").select2("val", ""); 
                $("#unit_kerja_id").empty();
                $("#unit_kerja_id").select2("val", ""); 
                $('#satuan_organisasi_id').append($('<option></option>').attr('value', '').text(''));
                for (row in data) {
                    $('#satuan_organisasi_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
                }
            }
        }).error(function (e){
            console.log(e);
            //alert("ERROR!");
        });
    });
}

$('#satuan_organisasi_id').select2({ width: '400px' }).on('change', function (e){
    var v = $('#satuan_kerja_id').val() ? $('#satuan_kerja_id').val() : '{{Auth::user()->satuan_kerja_id}}';
    var satorg = $('#satuan_organisasi_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id":v, "unit_kerja_parent":satorg},
        dataType: 'json',
        success: function(data) {
            $("#unit_kerja_id").empty();
            $("#unit_kerja_id").select2("val", ""); 
            $('#unit_kerja_id').select2({ width: '400px' }).append($('<option></option>').attr('value', '').text(''));
            for (row in data) {
                $('#unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
        }
    }).error(function (e){
        console.log(e);
        //alert("ERROR!");
    });
});


var satuan_organisasi_id = '';
var unit_kerja_id = '';
var peg_nip = '';
$('#m_satuan_kerja_id').on('change', function (e) {
    getSatuanOrganisasi();
    getPegawai();
});
$('#m_satuan_organisasi_id').on('change', function (e) {
    getUnitKerja();
    getPegawai();
});

$('#m_unit_kerja_id').on('change', function (e) {
    getPegawai();
});

function getSatuanOrganisasi() {
    $("#m_satuan_organisasi_id,#m_unit_kerja_id,#m_peg_nip").empty();
    $("#m_satuan_organisasi_id,#m_unit_kerja_id,#m_peg_nip").select2({ width: '100%' });
    var val = (role == 1) ? $('#m_satuan_kerja_id').val() : {{ $satker }};
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('getSatuanOrganisasi') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if(token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id": val},
        dataType: 'json',
        success: function(data) {
            $('#m_satuan_organisasi_id').append($('<option></option>').attr('value','').text(''));
            for(row in data) {
                $('#m_satuan_organisasi_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            if(satuan_organisasi_id != '') {
                $("#m_satuan_organisasi_id").val(satuan_organisasi_id).trigger('change');
            }
        }
    }).error(function (e) {
        console.log(e);
    });
}

function getUnitKerja() {
    $("#m_unit_kerja_id,#m_peg_nip").empty();
    $("#m_unit_kerja_id,#m_peg_nip").select2({ width: '100%' }); 
    var val = (role == 1) ? $('#m_satuan_kerja_id').val() : {{ $satker }};
    var satorg = $('#m_satuan_organisasi_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('getUnitKerja') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if(token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id": val, "unit_kerja_parent": satorg},
        dataType: 'json',
        success: function(data) {
            $('#m_unit_kerja_id').append($('<option></option>').attr('value','').text(''));
            for(row in data) {
                $('#m_unit_kerja_id').append($('<option></option>').attr('value', data[row].unit_kerja_id).text(data[row].unit_kerja_nama));
            }
            if(unit_kerja_id != '') {
                $("#m_unit_kerja_id").val(unit_kerja_id).trigger('change');
            }
        }
    }).error(function (e) {
        console.log(e);
    });
}

function getPegawai() {
    $("#m_peg_nip").empty();
    $("#m_peg_nip").select2({ width: '100%' }); 
    var val = (role == 1) ? $('#m_satuan_kerja_id').val() : {{ $satker }};
    var satorg = $('#m_satuan_organisasi_id').val();
    var uker = $('#m_unit_kerja_id').val();
    $.ajax({
        type: "GET",
        cache: false,
        url: "{{ URL::to('getPegawai') }}",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: {"satuan_kerja_id": val, "unit_kerja_parent": satorg, "unit_kerja_id": uker},
        dataType: 'json',
        success: function(data) {
            $('#m_peg_nip').append($('<option></option>').attr('value','').text(''));
            for(row in data) {
                $('#m_peg_nip').append($('<option></option>').attr('value', data[row].peg_id).text('['+data[row].peg_nip+'] '+data[row].peg_nama));
            }
            if(peg_nip != '') {
                $("#m_peg_nip").val(peg_nip).trigger('change');
            }
        }
    }).error(function (e) {
        console.log(e);
    });    
}
</script>