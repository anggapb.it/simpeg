<script type="text/javascript">
	function showEditDiklat(id){
		$(".editDiklat"+id).show();
		$(".aktifDiklat"+id).hide();
	}

	function hideEditDiklat(id){
		$(".editDiklat"+id).hide();
		$(".aktifDiklat"+id).show();
	}


    function showTambahDiklat(){
    	$(".tambahDiklat").show();
    }

    function cancelAddDiklat(){
        $(".tambahDiklat").hide();
    }

    function submitRiwayatDiklat(id) {
        var token = $(".token").val();
        var diklat_teknis_id = $(".diklat_teknis_id"+id).val();
        var id_riwayat = id;
        var peg_id = $(".peg_id"+id).val();
        var diklat_mulai = $(".diklat_mulai"+id).val();
        var diklat_selesai = $(".diklat_selesai"+id).val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam"+id).val();
        var diklat_sttp_no = $(".diklat_sttp_no"+id).val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl"+id).val();
        var diklat_sttp_pej = $(".diklat_sttp_pej"+id).val();
        var diklat_penyelenggara = $(".diklat_penyelenggara"+id).val();
        var diklat_tempat = $(".diklat_tempat"+id).val();
        var diklat_jenis = $(".diklat_jenis"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-diklat-teknis') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"diklat_teknis_id":diklat_teknis_id, "diklat_id":id_riwayat, "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

    function tambahRiwayatDiklat() {
        var token = $(".token").val();
        var diklat_teknis_id = $(".diklat_teknis_id").val();
        var peg_id = $(".peg_id").val();
        var diklat_mulai = $(".diklat_mulai").val();
        var diklat_selesai = $(".diklat_selesai").val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam").val();
        var diklat_sttp_no = $(".diklat_sttp_no").val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl").val();
        var diklat_sttp_pej = $(".diklat_sttp_pej").val();
        var diklat_penyelenggara = $(".diklat_penyelenggara").val();
        var diklat_tempat = $(".diklat_tempat").val();
        var diklat_jenis = $(".diklat_jenis").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-diklat-teknis') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"diklat_teknis_id":diklat_teknis_id,  "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
               alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }
    
     function submitRiwayatDiklatFungsional(id) {
        var token = $(".token").val();
        var diklat_fungsional_id = $(".diklat_fungsional_id"+id).val();
        var id_riwayat = id;
        var peg_id = $(".peg_id"+id).val();
        var diklat_mulai = $(".diklat_mulai"+id).val();
        var diklat_selesai = $(".diklat_selesai"+id).val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam"+id).val();
        var diklat_sttp_no = $(".diklat_sttp_no"+id).val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl"+id).val();
        var diklat_sttp_pej = $(".diklat_sttp_pej"+id).val();
        var diklat_penyelenggara = $(".diklat_penyelenggara"+id).val();
        var diklat_tempat = $(".diklat_tempat"+id).val();
        var diklat_jenis = $(".diklat_jenis"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-diklat-fungsional') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"diklat_fungsional_id":diklat_fungsional_id, "diklat_id":id_riwayat, "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
               alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

    function tambahRiwayatDiklatFungsional() {
        var token = $(".token").val();
        var diklat_fungsional_id = $(".diklat_fungsional_id").val();
        var peg_id = $(".peg_id").val();
        var diklat_mulai = $(".diklat_mulai").val();
        var diklat_selesai = $(".diklat_selesai").val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam").val();
        var diklat_sttp_no = $(".diklat_sttp_no").val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl").val();
        var diklat_sttp_pej = $(".diklat_sttp_pej").val();
        var diklat_penyelenggara = $(".diklat_penyelenggara").val();
        var diklat_tempat = $(".diklat_tempat").val();
        var diklat_jenis = $(".diklat_jenis").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-diklat-fungsional') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"diklat_fungsional_id":diklat_fungsional_id,  "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }

    function submitRiwayatDiklatStruktural(id) {
        var token = $(".token").val();
        var kategori_id = $(".kategori_id"+id).val();
        var id_riwayat = id;
        var peg_id = $(".peg_id"+id).val();
        var diklat_mulai = $(".diklat_mulai"+id).val();
        var diklat_selesai = $(".diklat_selesai"+id).val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam"+id).val();
        var diklat_sttp_no = $(".diklat_sttp_no"+id).val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl"+id).val();
        var diklat_sttp_pej = $(".diklat_sttp_pej"+id).val();
        var diklat_penyelenggara = $(".diklat_penyelenggara"+id).val();
        var diklat_tempat = $(".diklat_tempat"+id).val();
        var diklat_jenis = $(".diklat_jenis"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-diklat-struktural') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"kategori_id":kategori_id, "diklat_id":id_riwayat, "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

    function tambahRiwayatDiklatStruktural() {
        var token = $(".token").val();
        var kategori_id = $(".kategori_id").val();
        var peg_id = $(".peg_id").val();
        var diklat_mulai = $(".diklat_mulai").val();
        var diklat_selesai = $(".diklat_selesai").val();
        var diklat_jumlah_jam = $(".diklat_jumlah_jam").val();
        var diklat_sttp_no = $(".diklat_sttp_no").val();
        var diklat_sttp_tgl = $(".diklat_sttp_tgl").val();
        var diklat_sttp_pej = $(".diklat_sttp_pej").val();
        var diklat_penyelenggara = $(".diklat_penyelenggara").val();
        var diklat_tempat = $(".diklat_tempat").val();
        var diklat_jenis = $(".diklat_jenis").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-diklat-struktural') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"kategori_id":kategori_id,  "peg_id":peg_id, "diklat_mulai":diklat_mulai,
                "diklat_selesai":diklat_selesai, "diklat_jumlah_jam":diklat_jumlah_jam, "diklat_sttp_no":diklat_sttp_no,
                "diklat_sttp_tgl":diklat_sttp_tgl, "diklat_sttp_pej":diklat_sttp_pej, "diklat_penyelenggara":diklat_penyelenggara,
                "diklat_tempat":diklat_tempat,"diklat_jenis":diklat_jenis},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }
</script>