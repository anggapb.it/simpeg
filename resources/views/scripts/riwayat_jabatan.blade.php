<script type="text/javascript">
	function showEditJabatan(id){
		$(".editJabatan"+id).show();
		$(".aktifJabatan"+id).hide();
	}

	function hideEditJabatan(id){
		$(".editJabatan"+id).hide();
		$(".aktifJabatan"+id).show();
	}


    function showTambahJabatan(){
    	$(".tambahJabatan").show();
    }

    function cancelAddJabatan(){
        $(".tambahJabatan").hide();
    }

    function submitRiwayatJabatan(id) {
        var token = $(".token").val();
        var riw_jabatan_id = id;
        var peg_id = $(".peg_id"+id).val();
        var riw_jabatan_nm = $(".riw_jabatan_nm"+id).val();
        var riw_jabatan_no = $(".riw_jabatan_no"+id).val();
        var riw_jabatan_tgl = $(".riw_jabatan_tgl"+id).val();
        var riw_jabatan_pejabat = $(".riw_jabatan_pejabat"+id).val();
        var riw_jabatan_tmt = $(".riw_jabatan_tmt"+id).val();
        var riw_jabatan_unit = $(".riw_jabatan_unit"+id).val();
        var gol_id = $(".gol_id"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-jabatan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"riw_jabatan_id":riw_jabatan_id, "peg_id":peg_id, "riw_jabatan_nm":riw_jabatan_nm,
                "riw_jabatan_no":riw_jabatan_no, "riw_jabatan_tgl":riw_jabatan_tgl, "riw_jabatan_pejabat":riw_jabatan_pejabat,
                "riw_jabatan_tmt":riw_jabatan_tmt, "riw_jabatan_unit":riw_jabatan_unit, "gol_id":gol_id},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

     function tambahRiwayatJabatan() {
        var token = $(".token").val();
        var peg_id = $(".peg_id").val();
        var riw_jabatan_nm = $(".riw_jabatan_nm").val();
        var riw_jabatan_no = $(".riw_jabatan_no").val();
        var riw_jabatan_tgl = $(".riw_jabatan_tgl").val();
        var riw_jabatan_pejabat = $(".riw_jabatan_pejabat").val();
        var riw_jabatan_tmt = $(".riw_jabatan_tmt").val();
        var riw_jabatan_unit = $(".riw_jabatan_unit").val();
        var gol_id = $(".gol_id").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-jabatan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"peg_id":peg_id, "riw_jabatan_nm":riw_jabatan_nm,
                "riw_jabatan_no":riw_jabatan_no, "riw_jabatan_tgl":riw_jabatan_tgl, "riw_jabatan_pejabat":riw_jabatan_pejabat,
                "riw_jabatan_tmt":riw_jabatan_tmt, "riw_jabatan_unit":riw_jabatan_unit, "gol_id":gol_id},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }
</script>