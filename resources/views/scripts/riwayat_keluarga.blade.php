<script type="text/javascript">
	function showEditKeluarga(id){
		$(".editKeluarga"+id).show();
		$(".aktifKeluarga"+id).hide();
	}

	function hideEditKeluarga(id){
		$(".editKeluarga"+id).hide();
		$(".aktifKeluarga"+id).show();
	}


    function showTambahKeluarga(){
    	$(".tambahKeluarga").show();
    }

    function cancelAddKeluarga(){
        $(".tambahKeluarga").hide();
    }

    function submitRiwayatKeluarga(id) {
    	var token = $(".token").val();
    	var nama = $(".riw_nama"+id).val();
    	var id_riwayat = id;
    	var peg_id = $(".peg_id"+id).val();
    	var riw_kelamin = $(".riw_kelamin"+id).val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir"+id).val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir"+id).val();
    	var riw_pendidikan = $(".riw_pendidikan"+id).val();
    	var riw_pekerjaan = $(".riw_pekerjaan"+id).val();
    	var riw_ket = $(".riw_ket"+id).val();
    	var riw_status = $(".riw_status"+id).val();
    	
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('riwayat-saudara') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "riw_id":id_riwayat, "peg_id":peg_id, "riw_kelamin":riw_kelamin,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir, "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
	}

	function tambahRiwayatKeluarga(){
		var token = $(".token").val();
    	var nama = $(".riw_nama").val();
    	var peg_id = $(".peg_id").val();
    	var riw_kelamin = $(".riw_kelamin").val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir").val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir").val();
    	var riw_pendidikan = $(".riw_pendidikan").val();
    	var riw_pekerjaan = $(".riw_pekerjaan").val();
    	var riw_ket = $(".riw_ket").val();
    	var riw_status = $(".riw_status").val();
    	
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('add-riwayat-saudara') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "peg_id":peg_id, "riw_kelamin":riw_kelamin,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir, "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
	}


	 function submitRiwayatAnak(id) {
    	var token = $(".token").val();
    	var nama = $(".riw_nama"+id).val();
    	var id_riwayat = id;
    	var peg_id = $(".peg_id"+id).val();
    	var riw_kelamin = $(".riw_kelamin"+id).val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir"+id).val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir"+id).val();
    	var riw_status_perkawinan = $(".riw_status_perkawinan"+id).val();
    	var riw_status_tunj = $(".riw_status_tunj"+id).val();
    	var riw_pendidikan = $(".riw_pendidikan"+id).val();
    	var riw_pekerjaan = $(".riw_pekerjaan"+id).val();
    	var riw_ket = $(".riw_ket"+id).val();
    	var riw_status = $(".riw_status"+id).val();
    	
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('riwayat-anak') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "riw_id":id_riwayat, "peg_id":peg_id, "riw_kelamin":riw_kelamin,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir, "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status, "riw_status_perkawinan":riw_status, "riw_status_tunj":riw_status_tunj},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
	}

	function tambahRiwayatAnak(){
		var token = $(".token").val();
    	var nama = $(".riw_nama").val();
    	var peg_id = $(".peg_id").val();
    	var riw_kelamin = $(".riw_kelamin").val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir").val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir").val();
    	var riw_pendidikan = $(".riw_pendidikan").val();
    	var riw_pekerjaan = $(".riw_pekerjaan").val();
    	var riw_ket = $(".riw_ket").val();
    	var riw_status = $(".riw_status").val();
    	var riw_status_perkawinan = $(".riw_status_perkawinan").val();
    	var riw_status_tunj = $(".riw_status_tunj").val();
    	console.log(riw_status_tunj);
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('add-riwayat-anak') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "peg_id":peg_id, "riw_kelamin":riw_kelamin,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir, "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status,"riw_status_perkawinan":riw_status, "riw_status_tunj":riw_status_tunj},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
	}

	function submitRiwayatPasutri(id) {
    	var token = $(".token").val();
    	var nama = $(".riw_nama"+id).val();
    	var id_riwayat = id;
    	var peg_id = $(".peg_id"+id).val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir"+id).val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir"+id).val();
    	var riw_tgl_ket= $(".riw_tgl_ket"+id).val();
    	var riw_status_tunj = $(".riw_status_tunj"+id).val();
    	var riw_pendidikan = $(".riw_pendidikan"+id).val();
    	var riw_pekerjaan = $(".riw_pekerjaan"+id).val();
    	var riw_ket = $(".riw_ket"+id).val();
    	var riw_status = $(".riw_status"+id).val();
    	
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('riwayat-pasutri') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "riw_id":id_riwayat, "peg_id":peg_id,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir,"riw_tgl_ket":riw_tgl_ket , "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status,  "riw_status_tunj":riw_status_tunj},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
	}

	function tambahRiwayatPasutri(){
		var token = $(".token").val();
    	var nama = $(".riw_nama").val();
    	var peg_id = $(".peg_id").val();
    	var riw_tempat_lahir = $(".riw_tempat_lahir").val();
    	var riw_tgl_lahir = $(".riw_tgl_lahir").val();
    	var riw_tgl_ket = $(".riw_tgl_ket").val();
    	var riw_pendidikan = $(".riw_pendidikan").val();
    	var riw_pekerjaan = $(".riw_pekerjaan").val();
    	var riw_ket = $(".riw_ket").val();
    	var riw_status = $(".riw_status").val();
    	var riw_status_tunj = $(".riw_status_tunj").val();
    	console.log(riw_status_tunj);
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('add-riwayat-pasutri') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"riw_nama":nama, "peg_id":peg_id, "riw_tgl_ket":riw_tgl_ket,
	        	"riw_tempat_lahir":riw_tempat_lahir, "riw_tgl_lahir":riw_tgl_lahir, "riw_pendidikan":riw_pendidikan,
	        	"riw_pekerjaan":riw_pekerjaan, "riw_ket":riw_ket, "riw_status":riw_status, "riw_status_tunj":riw_status_tunj},
	        success: function(data) {
	            alert("Data telah disimpan.");
                window.location.reload();
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
	}
</script>