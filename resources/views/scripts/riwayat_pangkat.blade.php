<script type="text/javascript">
	function showEditPangkat(id){
		$(".editPangkat"+id).show();
		$(".aktifPangkat"+id).hide();
	}

	function hideEditPangkat(id){
		$(".editPangkat"+id).hide();
		$(".aktifPangkat"+id).show();
	}


    function showTambahPangkat(){
    	$(".tambahPangkat").show();
    }

    function cancelAddPangkat(){
        $(".tambahPangkat").hide();
    }

    function submitRiwayatPangkat(id) {
        var token = $(".token").val();
        var riw_pangkat_id = id;
        var peg_id = $(".peg_id"+id).val();
        var riw_pangkat_thn = $(".riw_pangkat_thn"+id).val();
        var riw_pangkat_bln = $(".riw_pangkat_bln"+id).val();
        var riw_pangkat_gapok = $(".riw_pangkat_gapok"+id).val();
        var riw_pangkat_sk = $(".riw_pangkat_sk"+id).val();
        var riw_pangkat_sktgl = $(".riw_pangkat_sktgl"+id).val();
        var riw_pangkat_pejabat = $(".riw_pangkat_pejabat"+id).val();
        var riw_pangkat_tmt = $(".riw_pangkat_tmt"+id).val();
        var riw_pangkat_unit_kerja = $(".riw_pangkat_unit_kerja"+id).val();
        var gol_id = $(".gol_id"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-pangkat') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"riw_pangkat_id":riw_pangkat_id, "peg_id":peg_id, "riw_pangkat_thn":riw_pangkat_thn,
                "riw_pangkat_bln":riw_pangkat_bln, "riw_pangkat_gapok":riw_pangkat_gapok, "riw_pangkat_sk":riw_pangkat_sk,
                "riw_pangkat_sktgl":riw_pangkat_sktgl, "riw_pangkat_pejabat":riw_pangkat_pejabat, "riw_pangkat_tmt":riw_pangkat_tmt, "riw_pangkat_unit_kerja":riw_pangkat_unit_kerja, "gol_id":gol_id},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

     function tambahRiwayatPangkat() {
        var token = $(".token").val();
        var peg_id = $(".peg_id").val();
        var riw_pangkat_thn = $(".riw_pangkat_thn").val();
        var riw_pangkat_bln = $(".riw_pangkat_bln").val();
        var riw_pangkat_gapok = $(".riw_pangkat_gapok").val();
        var riw_pangkat_sk = $(".riw_pangkat_sk").val();
        var riw_pangkat_sktgl = $(".riw_pangkat_sktgl").val();
        var riw_pangkat_pejabat = $(".riw_pangkat_pejabat").val();
        var riw_pangkat_tmt = $(".riw_pangkat_tmt").val();
        var riw_pangkat_unit_kerja = $(".riw_pangkat_unit_kerja").val();
        var gol_id = $(".gol_id").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-pangkat') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"peg_id":peg_id, "riw_pangkat_thn":riw_pangkat_thn,
                "riw_pangkat_bln":riw_pangkat_bln, "riw_pangkat_gapok":riw_pangkat_gapok, "riw_pangkat_sk":riw_pangkat_sk,
                "riw_pangkat_sktgl":riw_pangkat_sktgl, "riw_pangkat_pejabat":riw_pangkat_pejabat, "riw_pangkat_tmt":riw_pangkat_tmt, 
                "riw_pangkat_unit_kerja":riw_pangkat_unit_kerja, "gol_id":gol_id},
            success: function(data) {
               alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }
</script>