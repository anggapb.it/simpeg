<script type="text/javascript">
	function showEditPendidikan(id){
		$(".editPendidikan"+id).show();
		$(".aktifPendidikan"+id).hide();
	}

	function hideEditPendidikan(id){
		$(".editPendidikan"+id).hide();
		$(".aktifPendidikan"+id).show();
	}


    function showTambahPendidikan(){
    	$(".tambahPendidikan").show();
    }

    function cancelAddPendidikan(){
        $(".tambahPendidikan").hide();
    }

    function submitRiwayatPendidikanNon(id) {
        var token = $(".token").val();
        var non_id = id;
        var peg_id = $(".peg_id"+id).val();
        var non_nama = $(".non_nama"+id).val();
        var non_tgl_mulai = $(".non_tgl_mulai"+id).val();
        var non_tgl_selesai = $(".non_tgl_selesai"+id).val();
        var non_sttp = $(".non_sttp"+id).val();
        var non_sttp_tanggal = $(".non_sttp_tanggal"+id).val();
        var non_sttp_pejabat = $(".non_sttp_pejabat"+id).val();
        var non_penyelenggara = $(".non_penyelenggara"+id).val();
        var non_tempat = $(".non_tempat"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-pendidikan-non') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"non_id":non_id, "peg_id":peg_id, "non_nama":non_nama,
                "non_tgl_mulai":non_tgl_mulai, "non_tgl_selesai":non_tgl_selesai, "non_sttp":non_sttp,
                "non_sttp_tanggal":non_sttp_tanggal, "non_sttp_pejabat":non_sttp_pejabat, "non_penyelenggara":non_penyelenggara, "non_tempat":non_tempat},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

     function tambahRiwayatPendidikanNon() {
        var token = $(".token").val();
        var peg_id = $(".peg_id").val();
        var non_nama = $(".non_nama").val();
        var non_tgl_mulai = $(".non_tgl_mulai").val();
        var non_tgl_selesai = $(".non_tgl_selesai").val();
        var non_sttp = $(".non_sttp").val();
        var non_sttp_tanggal = $(".non_sttp_tanggal").val();
        var non_sttp_pejabat = $(".non_sttp_pejabat").val();
        var non_penyelenggara = $(".non_penyelenggara").val();
        var non_tempat = $(".non_tempat").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-pendidikan-non') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"peg_id":peg_id, "non_nama":non_nama, "non_tgl_mulai":non_tgl_mulai, "non_tgl_selesai":non_tgl_selesai, "non_sttp":non_sttp,
                "non_sttp_tanggal":non_sttp_tanggal, "non_sttp_pejabat":non_sttp_pejabat, "non_penyelenggara":non_penyelenggara, "non_tempat":non_tempat},
            success: function(data) {
                alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }

    function submitRiwayatPendidikan(id) {
        var token = $(".token").val();
        var riw_pendidikan_id = id;
        var peg_id = $(".peg_id"+id).val();
        var tingpend_id = $(".tingpend_id"+id).val();
        var fakultas_id = $(".fakultas_id"+id).val();
        var jurusan_id = $(".jurusan_id"+id).val();
        var riw_pendidikan_sttb_ijazah = $(".riw_pendidikan_sttb_ijazah"+id).val();
        var riw_pendidikan_tgl = $(".riw_pendidikan_tgl"+id).val();
        var riw_pendidikan_pejabat = $(".riw_pendidikan_pejabat"+id).val();
        var riw_pendidikan_nm = $(".riw_pendidikan_nm"+id).val();
        var riw_pendidikan_lokasi = $(".riw_pendidikan_lokasi"+id).val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('riwayat-pendidikan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"riw_pendidikan_id":riw_pendidikan_id, "peg_id":peg_id, "tingpend_id":tingpend_id,
                "fakultas_id":fakultas_id, "jurusan_id":jurusan_id, "riw_pendidikan_sttb_ijazah":riw_pendidikan_sttb_ijazah,
                "riw_pendidikan_tgl":riw_pendidikan_tgl, "riw_pendidikan_pejabat":riw_pendidikan_pejabat, 
                "riw_pendidikan_nm":riw_pendidikan_nm, "riw_pendidikan_lokasi":riw_pendidikan_lokasi},
            success: function(data) {
                alert("Data telah disimpan.");
                    window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
    }

    function tambahRiwayatPendidikan() {
        var token = $(".token").val();
        var peg_id = $(".peg_id").val();
        var tingpend_id = $(".tingpend_id").val();
        var fakultas_id = $(".fakultas_id").val();
        var jurusan_id = $(".jurusan_id").val();
        var riw_pendidikan_sttb_ijazah = $(".riw_pendidikan_sttb_ijazah").val();
        var riw_pendidikan_tgl = $(".riw_pendidikan_tgl").val();
        var riw_pendidikan_pejabat = $(".riw_pendidikan_pejabat").val();
        var riw_pendidikan_nm = $(".riw_pendidikan_nm").val();
        var riw_pendidikan_lokasi = $(".riw_pendidikan_lokasi").val();
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-pendidikan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"peg_id":peg_id, "tingpend_id":tingpend_id,
                "fakultas_id":fakultas_id, "jurusan_id":jurusan_id, "riw_pendidikan_sttb_ijazah":riw_pendidikan_sttb_ijazah,
                "riw_pendidikan_tgl":riw_pendidikan_tgl, "riw_pendidikan_pejabat":riw_pendidikan_pejabat, 
                "riw_pendidikan_nm":riw_pendidikan_nm, "riw_pendidikan_lokasi":riw_pendidikan_lokasi},
            success: function(data) {
                alert("Data telah disimpan.");
                    window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }

</script>