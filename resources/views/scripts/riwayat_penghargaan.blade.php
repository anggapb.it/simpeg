<script type="text/javascript">

    function showMe(id){
		$(".edit"+id).show();
		$(".aktif"+id).hide();
	}

	function hideMe(id){
		$(".edit"+id).hide();
		$(".aktif"+id).show();
	}


    function showTambahPenghargaan(){
    	$(".tambah").show();
    }

    function cancelAdd(){
        $(".tambah").hide();
    }

    function submitRiwayatPenghargaan(id) {
    	var token = $(".token").val();
    	var nama = $("#nama").val();
    	var id_riwayat = id;
    	var peg_id = $(".peg_id").val();
    	var riw_penghargaan_sk = $(".riw_penghargaan_sk").val();
    	var riw_penghargaan_tglsk = $(".riw_penghargaan_tglsk").val();
    	var riw_penghargaan_jabatan = $(".riw_penghargaan_jabatan").val();
    	var riw_penghargaan_instansi = $(".riw_penghargaan_instansi").val();
    	var riw_penghargaan_lokasi = $(".riw_penghargaan_lokasi").val();
    	
   		$.ajax({
   			type: "POST",
            cache: false,
	        url: "{{ URL::to('riwayat-penghargaan') }}",
	        beforeSend: function (xhr) {
	            var token = $('meta[name="csrf_token"]').attr('content');

	            if (token) {
	                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
	            }
        	},
	        data: {"penghargaan_id":nama, "riw_penghargaan_id":id_riwayat, "peg_id":peg_id, "riw_penghargaan_sk":riw_penghargaan_sk,
	        	"riw_penghargaan_tglsk":riw_penghargaan_tglsk, "riw_penghargaan_jabatan":riw_penghargaan_jabatan, "riw_penghargaan_instansi":riw_penghargaan_instansi,
	        	"riw_penghargaan_lokasi":riw_penghargaan_lokasi},
	        success: function(data) {
                $(document).ajaxStop(function(){
                    alert("Data telah disimpan.");
                    window.location.reload();
                });
	        }
    	}).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil diupdate!");
        });
	}

    function tambahRiwayatPenghargaan(){
        var token = $(".token").val();
        var nama = $(".riw_penghargaan_nm").val();
        var peg_id = $(".peg_id").val();
        var riw_penghargaan_sk = $(".riw_penghargaan_sk").val();
        var riw_penghargaan_tglsk = $(".riw_penghargaan_tglsk").val();
        var riw_penghargaan_jabatan = $(".riw_penghargaan_jabatan").val();
        var riw_penghargaan_instansi = $(".riw_penghargaan_instansi").val();
        var riw_penghargaan_lokasi = $(".riw_penghargaan_lokasi").val();
        
        console.log(peg_id);
        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ URL::to('add-riwayat-penghargaan') }}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {"riw_penghargaan_nm":nama, "peg_id":peg_id, "riw_penghargaan_sk":riw_penghargaan_sk,
                "riw_penghargaan_tglsk":riw_penghargaan_tglsk, "riw_penghargaan_jabatan":riw_penghargaan_jabatan, "riw_penghargaan_instansi":riw_penghargaan_instansi,
                "riw_penghargaan_lokasi":riw_penghargaan_lokasi},
            success: function(data) {
               alert("Data telah disimpan.");
                window.location.reload();
            }
        }).error(function (e){
            console.log(e);
            alert("ERROR! data tidak berhasil ditambahkan!");
        });
    }


</script>