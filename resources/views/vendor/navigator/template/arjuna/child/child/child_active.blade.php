<li class="treeview active">
    <a href="">
        {!! $icon or '<i class="fa fa-bar-chart-o"></i>' !!}
        <span>{{ $text }}</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    
    {!! $child !!}

</li>