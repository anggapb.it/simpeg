<li>
	<a href="{{ $url }}">
		<i class="fa fa-circle-o"></i> {{ $text }}
	</a>
</li>