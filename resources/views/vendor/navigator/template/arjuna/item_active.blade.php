<li class="active">
	<a href="{{ $item->url }}">{!! $item->iconFa() !!} {{ $item->text }}</a>
</li>