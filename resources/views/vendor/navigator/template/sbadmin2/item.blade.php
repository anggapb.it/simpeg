<li>
    <a href="{{ $item->url }}">{!! $item->iconFa() !!} {{ $item->text }}</a>
</li>