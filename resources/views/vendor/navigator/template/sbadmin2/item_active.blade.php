<li>
    <a href="{{ $item->url }}" class="active">{!! $item->iconFa() !!} {{ $item->text }}</a>
</li>